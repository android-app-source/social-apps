.class public Lcom/facebook/video/player/plugins/Video360Plugin;
.super Lcom/facebook/video/player/plugins/VideoPlugin;
.source ""

# interfaces
.implements LX/3II;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final s:LX/19o;


# instance fields
.field public A:Landroid/os/AsyncTask;

.field public B:Landroid/os/Handler;

.field public C:Landroid/os/AsyncTask;

.field public D:LX/7E5;

.field private E:Z

.field public F:I

.field public G:Z

.field public H:Z

.field public I:Z

.field public J:Z

.field public final K:Ljava/lang/Runnable;

.field public final L:Ljava/lang/Runnable;

.field public M:LX/3IO;

.field public N:Z

.field public O:LX/2pa;

.field public P:Lcom/facebook/spherical/model/SphericalVideoParams;

.field public Q:LX/3IP;

.field public a:Z

.field public b:Z

.field public c:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:I

.field public u:F

.field public v:F

.field public w:F

.field public x:F

.field public y:F

.field public z:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 545929
    sget-object v0, LX/19o;->CUBEMAP:LX/19o;

    sput-object v0, Lcom/facebook/video/player/plugins/Video360Plugin;->s:LX/19o;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 545930
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 545931
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 545932
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545933
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 545934
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545935
    new-instance v0, Lcom/facebook/video/player/plugins/Video360Plugin$1;

    invoke-direct {v0, p0}, Lcom/facebook/video/player/plugins/Video360Plugin$1;-><init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->K:Ljava/lang/Runnable;

    .line 545936
    new-instance v0, Lcom/facebook/video/player/plugins/Video360Plugin$2;

    invoke-direct {v0, p0}, Lcom/facebook/video/player/plugins/Video360Plugin$2;-><init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->L:Ljava/lang/Runnable;

    .line 545937
    const-class v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-static {v0, p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 545938
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3IJ;

    invoke-direct {v1, p0}, LX/3IJ;-><init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545939
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3IK;

    invoke-direct {v1, p0}, LX/3IK;-><init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545940
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3IL;

    invoke-direct {v1, p0}, LX/3IL;-><init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545941
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/2sC;

    invoke-direct {v1, p0}, LX/2sC;-><init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545942
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3IM;

    invoke-direct {v1, p0}, LX/3IM;-><init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545943
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3IN;

    invoke-direct {v1, p0}, LX/3IN;-><init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545944
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->B:Landroid/os/Handler;

    .line 545945
    new-instance v0, LX/3IO;

    invoke-direct {v0}, LX/3IO;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->M:LX/3IO;

    .line 545946
    new-instance v0, LX/3IP;

    invoke-direct {v0}, LX/3IP;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->Q:LX/3IP;

    .line 545947
    return-void
.end method

.method public static A(Lcom/facebook/video/player/plugins/Video360Plugin;)V
    .locals 4

    .prologue
    .line 545948
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 545949
    sget-object v0, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    iget-object v0, v0, LX/09L;->value:Ljava/lang/String;

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 545950
    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    if-nez v1, :cond_1

    .line 545951
    if-eqz v0, :cond_1

    .line 545952
    sget-object v0, Lcom/facebook/video/player/plugins/Video360Plugin;->s:LX/19o;

    .line 545953
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2qW;->setProjectionType(LX/19o;)V

    .line 545954
    iget-object v1, p0, LX/2oy;->i:LX/2oj;

    if-eqz v1, :cond_0

    .line 545955
    iget-object v1, p0, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/2ok;

    sget-object v3, LX/3np;->PROJECTION_TYPE:LX/3np;

    iget-object v3, v3, LX/3np;->value:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/19o;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v3, v0}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 545956
    :cond_0
    return-void

    .line 545957
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    iget-object v0, v0, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    goto :goto_0

    .line 545958
    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public static G(Lcom/facebook/video/player/plugins/Video360Plugin;)V
    .locals 14

    .prologue
    const/4 v6, 0x1

    .line 545994
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545995
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/2qW;->setGuideActive(Z)V

    .line 545996
    :cond_0
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_1

    .line 545997
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lm;

    sget-object v2, LX/7Ll;->GUIDE_ON:LX/7Ll;

    invoke-direct {v1, v2}, LX/7Lm;-><init>(LX/7Ll;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 545998
    :cond_1
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->E:Z

    if-nez v0, :cond_2

    .line 545999
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    .line 546000
    iget-object v2, v1, LX/2pb;->D:LX/04G;

    move-object v1, v2

    .line 546001
    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->getCurrentPlaybackPositionMs(Lcom/facebook/video/player/plugins/Video360Plugin;)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v3, v3, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v4}, LX/2pb;->s()LX/04D;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v5, v5, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    .line 546002
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v8, LX/04A;->VIDEO_GUIDE_ENTERED:LX/04A;

    iget-object v8, v8, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v8, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v8, v8, LX/04F;->value:Ljava/lang/String;

    int-to-float v9, v2

    const/high16 v10, 0x447a0000    # 1000.0f

    div-float/2addr v9, v10

    float-to-double v9, v9

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 546003
    const/4 v11, 0x0

    move-object v7, v0

    move-object v9, v3

    move-object v10, v5

    move-object v12, v4

    move-object v13, v1

    invoke-static/range {v7 .. v13}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 546004
    :cond_2
    iput-boolean v6, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->E:Z

    .line 546005
    return-void
.end method

.method public static H(Lcom/facebook/video/player/plugins/Video360Plugin;)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 545959
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->E:Z

    if-eqz v0, :cond_2

    .line 545960
    iput-boolean v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->E:Z

    .line 545961
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545962
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/2qW;->setGuideActive(Z)V

    .line 545963
    :cond_0
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_1

    .line 545964
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lm;

    sget-object v2, LX/7Ll;->GUIDE_OFF:LX/7Ll;

    invoke-direct {v1, v2}, LX/7Lm;-><init>(LX/7Ll;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 545965
    :cond_1
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_2

    .line 545966
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    .line 545967
    iget-object v2, v1, LX/2pb;->D:LX/04G;

    move-object v1, v2

    .line 545968
    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->getCurrentPlaybackPositionMs(Lcom/facebook/video/player/plugins/Video360Plugin;)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v3, v3, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v4}, LX/2pb;->s()LX/04D;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v5, v5, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    .line 545969
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v7, LX/04A;->VIDEO_GUIDE_EXITED:LX/04A;

    iget-object v7, v7, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v7, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v7, v7, LX/04F;->value:Ljava/lang/String;

    int-to-float v8, v2

    const/high16 v9, 0x447a0000    # 1000.0f

    div-float/2addr v8, v9

    float-to-double v8, v8

    invoke-virtual {v6, v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 545970
    const/4 v10, 0x0

    move-object v6, v0

    move-object v8, v3

    move-object v9, v5

    move-object v11, v4

    move-object v12, v1

    invoke-static/range {v6 .. v12}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 545971
    :cond_2
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-static {p0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v1

    check-cast v1, LX/1C2;

    invoke-static {p0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object p0

    check-cast p0, LX/19m;

    iput-object v1, p1, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iput-object p0, p1, Lcom/facebook/video/player/plugins/Video360Plugin;->d:LX/19m;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/player/plugins/Video360Plugin;FF)V
    .locals 8

    .prologue
    const/high16 v7, 0x42700000    # 60.0f

    .line 545972
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->getCurrentPlaybackPositionMs(Lcom/facebook/video/player/plugins/Video360Plugin;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/7E5;->b(I)I

    move-result v2

    .line 545973
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    iget v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->F:I

    invoke-virtual {v0, v1}, LX/7E5;->a(I)Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v0

    iget v0, v0, Lcom/facebook/spherical/model/KeyframeParams;->b:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 545974
    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    iget v3, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->F:I

    invoke-virtual {v1, v3}, LX/7E5;->a(I)Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v1

    iget v1, v1, Lcom/facebook/spherical/model/KeyframeParams;->c:I

    int-to-float v1, v1

    invoke-static {p2, v1}, LX/7Cq;->a(FF)F

    move-result v1

    .line 545975
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 545976
    iget v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->F:I

    if-eq v1, v2, :cond_3

    .line 545977
    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    invoke-virtual {v1, v2}, LX/7E5;->a(I)Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v1

    .line 545978
    iget v3, v1, Lcom/facebook/spherical/model/KeyframeParams;->b:I

    neg-int v3, v3

    int-to-float v3, v3

    .line 545979
    iget v4, v1, Lcom/facebook/spherical/model/KeyframeParams;->c:I

    int-to-float v4, v4

    .line 545980
    iget-object v5, p0, LX/2oy;->i:LX/2oj;

    new-instance v6, LX/7Ln;

    invoke-direct {v6, v1}, LX/7Ln;-><init>(Lcom/facebook/spherical/model/KeyframeParams;)V

    invoke-virtual {v5, v6}, LX/2oj;->a(LX/2ol;)V

    .line 545981
    iget-boolean v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->E:Z

    if-eqz v1, :cond_0

    .line 545982
    cmpl-float v0, v0, v7

    if-lez v0, :cond_2

    .line 545983
    sub-float v1, v3, p1

    .line 545984
    const/4 v0, 0x0

    invoke-static {p2, v0}, LX/7Cq;->a(FZ)F

    move-result v0

    invoke-static {v0, v4}, LX/7Cq;->a(FF)F

    move-result v0

    .line 545985
    :goto_0
    iget-object v3, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->Q:LX/3IP;

    const/16 v4, 0x7d0

    invoke-virtual {v3, v1, v0, v4, p0}, LX/3IP;->b(FFILX/3II;)V

    .line 545986
    :cond_0
    iput v2, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->F:I

    .line 545987
    :cond_1
    :goto_1
    return-void

    .line 545988
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    iget v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->F:I

    invoke-virtual {v0, v1}, LX/7E5;->a(I)Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v0

    iget v0, v0, Lcom/facebook/spherical/model/KeyframeParams;->b:I

    int-to-float v0, v0

    add-float v1, v3, v0

    .line 545989
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    iget v3, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->F:I

    invoke-virtual {v0, v3}, LX/7E5;->a(I)Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v0

    iget v0, v0, Lcom/facebook/spherical/model/KeyframeParams;->c:I

    int-to-float v0, v0

    invoke-static {v0, v4}, LX/7Cq;->a(FF)F

    move-result v0

    goto :goto_0

    .line 545990
    :cond_3
    iget-boolean v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->E:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->Q:LX/3IP;

    invoke-virtual {v1}, LX/3IP;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 545991
    cmpl-float v0, v0, v7

    if-lez v0, :cond_1

    .line 545992
    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->H(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    goto :goto_1
.end method

.method public static getCurrentPlaybackPositionMs(Lcom/facebook/video/player/plugins/Video360Plugin;)I
    .locals 1

    .prologue
    .line 545993
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getDefaultFov(Lcom/facebook/video/player/plugins/Video360Plugin;)F
    .locals 2

    .prologue
    .line 545918
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_1

    .line 545919
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 545920
    invoke-virtual {v0}, Lcom/facebook/spherical/model/SphericalVideoParams;->b()F

    move-result v1

    .line 545921
    const/4 p0, 0x0

    cmpl-float p0, v1, p0

    if-lez p0, :cond_0

    .line 545922
    const/high16 p0, 0x42f00000    # 120.0f

    invoke-static {v1, p0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 545923
    const/high16 p0, 0x42200000    # 40.0f

    invoke-static {v1, p0}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 545924
    :cond_0
    move v0, v1

    .line 545925
    :goto_0
    return v0

    :cond_1
    const/high16 v0, 0x428c0000    # 70.0f

    goto :goto_0
.end method

.method public static x(Lcom/facebook/video/player/plugins/Video360Plugin;)V
    .locals 2

    .prologue
    .line 545926
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545927
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2qW;->setTiltInteractionEnabled(Z)V

    .line 545928
    :cond_0
    return-void
.end method

.method private z()V
    .locals 3

    .prologue
    .line 545781
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 545782
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->x()I

    move-result v0

    .line 545783
    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->y()I

    move-result v1

    .line 545784
    if-lez v0, :cond_0

    if-lez v1, :cond_0

    .line 545785
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v2

    .line 545786
    iget-object p0, v2, LX/2qW;->c:LX/7Cy;

    if-eqz p0, :cond_0

    iget-object p0, v2, LX/2qW;->c:LX/7Cy;

    iget-object p0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz p0, :cond_0

    .line 545787
    iget-object p0, v2, LX/2qW;->c:LX/7Cy;

    iget-object p0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 545788
    iget-object v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {v2, v0}, LX/7D0;->a(I)V

    .line 545789
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 545790
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 545791
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/VideoPlugin;->v()V

    .line 545792
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->z()V

    .line 545793
    :cond_0
    return-void
.end method

.method public final a(LX/2pa;)V
    .locals 1

    .prologue
    .line 545794
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->d()V

    .line 545795
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a(LX/2pa;Z)V

    .line 545796
    return-void
.end method

.method public a(LX/2pa;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 545797
    if-eqz p1, :cond_0

    .line 545798
    iput-object p1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    .line 545799
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 545800
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    invoke-virtual {v0}, LX/2pa;->e()Z

    const/16 v0, 0x19

    iput v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->t:I

    .line 545801
    :cond_0
    if-eqz p2, :cond_1

    .line 545802
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->z:F

    .line 545803
    iput-boolean v2, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->E:Z

    .line 545804
    iput-boolean v2, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->b:Z

    .line 545805
    iput v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->w:F

    .line 545806
    iput v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->x:F

    .line 545807
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/facebook/video/player/plugins/VideoPlugin;->a(LX/2pa;Z)V

    .line 545808
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 545809
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/3IO;I)Z
    .locals 10

    .prologue
    .line 545810
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 545811
    :cond_0
    const/4 v0, 0x0

    .line 545812
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    .line 545813
    iget-object v1, v0, LX/2qW;->c:LX/7Cy;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/2qW;->c:LX/7Cy;

    iget-object v1, v1, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v1, :cond_2

    if-nez p1, :cond_3

    .line 545814
    :cond_2
    const/4 v1, 0x0

    .line 545815
    :goto_1
    move v0, v1

    .line 545816
    goto :goto_0

    :cond_3
    iget-object v1, v0, LX/2qW;->c:LX/7Cy;

    iget-object v1, v1, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    const/4 v2, 0x0

    .line 545817
    if-nez p1, :cond_4

    .line 545818
    :goto_2
    move v1, v2

    .line 545819
    goto :goto_1

    .line 545820
    :cond_4
    iget-object v3, v1, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget-object v3, v3, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 545821
    if-lez p2, :cond_5

    iget-object v3, v1, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    .line 545822
    int-to-float v5, p2

    .line 545823
    iget v7, v3, LX/3IO;->h:F

    iget v8, v3, LX/3IO;->d:F

    sub-float/2addr v7, v8

    .line 545824
    iget v8, v3, LX/3IO;->i:F

    iget v9, v3, LX/3IO;->e:F

    sub-float/2addr v8, v9

    .line 545825
    mul-float/2addr v7, v7

    mul-float/2addr v8, v8

    add-float/2addr v7, v8

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    double-to-float v7, v7

    move v6, v7

    .line 545826
    cmpg-float v5, v5, v6

    if-gtz v5, :cond_6

    const/4 v5, 0x1

    :goto_3
    move v3, v5

    .line 545827
    if-eqz v3, :cond_5

    .line 545828
    const/4 v2, 0x1

    .line 545829
    iget-object v3, v1, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    .line 545830
    iget v5, v3, LX/3IO;->b:F

    iput v5, p1, LX/3IO;->b:F

    .line 545831
    iget v5, v3, LX/3IO;->c:F

    iput v5, p1, LX/3IO;->c:F

    .line 545832
    iget v5, v3, LX/3IO;->f:F

    iput v5, p1, LX/3IO;->f:F

    .line 545833
    iget v5, v3, LX/3IO;->d:F

    iput v5, p1, LX/3IO;->d:F

    .line 545834
    iget v5, v3, LX/3IO;->e:F

    iput v5, p1, LX/3IO;->e:F

    .line 545835
    iget v5, v3, LX/3IO;->h:F

    iput v5, p1, LX/3IO;->h:F

    .line 545836
    iget v5, v3, LX/3IO;->i:F

    iput v5, p1, LX/3IO;->i:F

    .line 545837
    iget-wide v5, v3, LX/3IO;->g:J

    iput-wide v5, p1, LX/3IO;->g:J

    .line 545838
    iget-object v3, v1, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget-object v4, v1, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget v4, v4, LX/3IO;->d:F

    iput v4, v3, LX/3IO;->h:F

    .line 545839
    iget-object v3, v1, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget-object v4, v1, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget v4, v4, LX/3IO;->e:F

    iput v4, v3, LX/3IO;->i:F

    .line 545840
    :cond_5
    iget-object v3, v1, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget-object v3, v3, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2

    :cond_6
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public d()V
    .locals 2

    .prologue
    .line 545841
    invoke-super {p0}, Lcom/facebook/video/player/plugins/VideoPlugin;->d()V

    .line 545842
    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->x(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    .line 545843
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 545844
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->B:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->K:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 545845
    return-void
.end method

.method public get360TextureView()LX/2qW;
    .locals 1

    .prologue
    .line 545846
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->g()LX/2qW;

    move-result-object v0

    return-object v0
.end method

.method public getViewportState()LX/7DJ;
    .locals 4

    .prologue
    .line 545847
    new-instance v0, LX/7DJ;

    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1}, LX/2qW;->getPitch()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v2

    invoke-virtual {v2}, LX/2qW;->getYaw()F

    move-result v2

    iget-boolean v3, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->E:Z

    invoke-direct {v0, v1, v2, v3}, LX/7DJ;-><init>(FFZ)V

    return-object v0
.end method

.method public final i()LX/2p8;
    .locals 2

    .prologue
    .line 545848
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->e:LX/19d;

    sget-object v1, LX/2p7;->SPHERICAL:LX/2p7;

    invoke-virtual {v0, v1}, LX/19d;->a(LX/2p7;)LX/2p8;

    move-result-object v0

    return-object v0
.end method

.method public final j()V
    .locals 5

    .prologue
    .line 545849
    invoke-super {p0}, Lcom/facebook/video/player/plugins/VideoPlugin;->j()V

    .line 545850
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545851
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->getDefaultFov(Lcom/facebook/video/player/plugins/Video360Plugin;)F

    move-result v1

    invoke-virtual {v0, v1}, LX/2qW;->setPreferredVerticalFOV(F)V

    .line 545852
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_2

    .line 545853
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v0, v0, LX/2pa;->b:LX/0P1;

    const-string v1, "SphericalViewportStateKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7DJ;

    .line 545854
    if-eqz v0, :cond_5

    .line 545855
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    iget v2, v0, LX/7DJ;->a:F

    iget v0, v0, LX/7DJ;->b:F

    .line 545856
    iget-object v3, v1, LX/2qW;->c:LX/7Cy;

    if-eqz v3, :cond_1

    iget-object v3, v1, LX/2qW;->c:LX/7Cy;

    iget-object v3, v3, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v3, :cond_6

    .line 545857
    :cond_1
    invoke-virtual {v1}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    iput-object v4, v3, LX/7Cz;->j:Ljava/lang/Float;

    .line 545858
    invoke-virtual {v1}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    iput-object v4, v3, LX/7Cz;->i:Ljava/lang/Float;

    .line 545859
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->d:LX/19m;

    .line 545860
    iget-boolean v1, v0, LX/19m;->f:Z

    move v0, v1

    .line 545861
    if-eqz v0, :cond_2

    .line 545862
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->J:Z

    .line 545863
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 545864
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    iget-boolean v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->a:Z

    invoke-virtual {v0, v1}, LX/2qW;->setShouldUseFullScreenControl(Z)V

    .line 545865
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 545866
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    new-instance v1, LX/7DF;

    invoke-direct {v1}, LX/7DF;-><init>()V

    invoke-virtual {v1}, LX/7DF;->b()LX/7DF;

    move-result-object v1

    const/high16 v2, 0x42b40000    # 90.0f

    .line 545867
    iput v2, v1, LX/7DF;->d:F

    .line 545868
    move-object v1, v1

    .line 545869
    const/high16 v2, -0x3d4c0000    # -90.0f

    .line 545870
    iput v2, v1, LX/7DF;->c:F

    .line 545871
    move-object v1, v1

    .line 545872
    invoke-virtual {v1}, LX/7DF;->c()LX/7DG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2qW;->setRendererBounds(LX/7DG;)V

    .line 545873
    :cond_4
    return-void

    .line 545874
    :cond_5
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    iget v0, v0, Lcom/facebook/spherical/model/SphericalVideoParams;->c:I

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->u:F

    .line 545875
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    iget v0, v0, Lcom/facebook/spherical/model/SphericalVideoParams;->d:I

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->v:F

    .line 545876
    iget v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->u:F

    const/high16 v1, -0x3ccc0000    # -180.0f

    const/high16 v2, 0x43340000    # 180.0f

    invoke-static {v0, v1, v2}, LX/7DG;->a(FFF)F

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->u:F

    .line 545877
    iget v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->v:F

    const/high16 v1, -0x3d4c0000    # -90.0f

    const/high16 v2, 0x42b40000    # 90.0f

    invoke-static {v0, v1, v2}, LX/7DG;->a(FFF)F

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->v:F

    .line 545878
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    iget v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->v:F

    iget v2, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->u:F

    invoke-virtual {v0, v1, v2}, LX/2qW;->a(FF)V

    goto :goto_0

    .line 545879
    :cond_6
    iget-object v3, v1, LX/2qW;->c:LX/7Cy;

    iget-object v3, v3, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 545880
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/facebook/spherical/GlMediaRenderThread;->N:Z

    .line 545881
    invoke-virtual {v3, v2, v0}, Lcom/facebook/spherical/GlMediaRenderThread;->a(FF)V

    .line 545882
    goto :goto_0
.end method

.method public k()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 545883
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 545884
    const-string v0, "V360"

    const-string v1, "Video360Plugin id:%d beginRendering() has no textureview"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 545885
    :goto_0
    return-void

    .line 545886
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    invoke-virtual {v0}, LX/2qW;->a()V

    .line 545887
    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->A(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    .line 545888
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->z()V

    .line 545889
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v5, :cond_1

    iget-object v5, p0, LX/2oy;->j:LX/2pb;

    if-eqz v5, :cond_1

    .line 545890
    iget-object v5, p0, LX/2oy;->j:LX/2pb;

    new-instance v6, LX/0H2;

    invoke-direct {v6}, LX/0H2;-><init>()V

    iget-object v7, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    iget-boolean v7, v7, Lcom/facebook/spherical/model/SphericalVideoParams;->j:Z

    .line 545891
    iput-boolean v7, v6, LX/0H2;->a:Z

    .line 545892
    move-object v6, v6

    .line 545893
    iget-object v7, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    iget-wide v7, v7, Lcom/facebook/spherical/model/SphericalVideoParams;->k:D

    .line 545894
    iput-wide v7, v6, LX/0H2;->b:D

    .line 545895
    move-object v6, v6

    .line 545896
    iget-object v7, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    iget-wide v7, v7, Lcom/facebook/spherical/model/SphericalVideoParams;->l:D

    .line 545897
    iput-wide v7, v6, LX/0H2;->c:D

    .line 545898
    move-object v6, v6

    .line 545899
    new-instance v7, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;

    invoke-direct {v7, v6}, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;-><init>(LX/0H2;)V

    move-object v6, v7

    .line 545900
    iget-object v7, v5, LX/2pb;->E:LX/2q7;

    invoke-interface {v7, v6}, LX/2q7;->a(Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V

    .line 545901
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 545902
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2qW;->setTiltInteractionEnabled(Z)V

    .line 545903
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_4

    .line 545904
    new-instance v0, LX/7E5;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    invoke-direct {v0, v1}, LX/7E5;-><init>(Lcom/facebook/spherical/model/SphericalVideoParams;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    .line 545905
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    .line 545906
    iget-boolean v1, v0, LX/7E5;->b:Z

    move v0, v1

    .line 545907
    if-eqz v0, :cond_4

    .line 545908
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->getCurrentPlaybackPositionMs(Lcom/facebook/video/player/plugins/Video360Plugin;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/7E5;->b(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->F:I

    .line 545909
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v0, v0, LX/2pa;->b:LX/0P1;

    const-string v1, "SphericalViewportStateKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7DJ;

    .line 545910
    if-eqz v0, :cond_3

    iget-boolean v0, v0, LX/7DJ;->c:Z

    if-eqz v0, :cond_5

    .line 545911
    :cond_3
    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->G(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    .line 545912
    :goto_1
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_4

    .line 545913
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Ln;

    iget-object v3, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    iget v4, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->F:I

    invoke-virtual {v3, v4}, LX/7E5;->a(I)Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v3

    invoke-direct {v1, v3}, LX/7Ln;-><init>(Lcom/facebook/spherical/model/KeyframeParams;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 545914
    :cond_4
    iput-boolean v2, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->G:Z

    .line 545915
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->B:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->L:Ljava/lang/Runnable;

    const-wide/16 v2, 0x96

    const v4, 0x5b31ab62    # 5.0009508E16f

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 545916
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->B:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360Plugin;->K:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    const v4, 0xd4c820c

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto/16 :goto_0

    .line 545917
    :cond_5
    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->H(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    goto :goto_1
.end method
