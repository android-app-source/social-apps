.class public Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;
.super LX/2oy;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final a:LX/2pS;

.field private final b:Landroid/widget/FrameLayout;

.field public final c:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

.field public final d:Landroid/widget/ProgressBar;

.field private final e:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;

.field private final f:Z

.field public final n:J

.field public final o:I

.field private final p:Z

.field public q:LX/2pR;

.field public r:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 468178
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 468179
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 468124
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 468125
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 468156
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 468157
    new-instance v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;

    invoke-direct {v0, p0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;-><init>(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->e:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;

    .line 468158
    sget-object v0, LX/2pR;->DEFAULT:LX/2pR;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->q:LX/2pR;

    .line 468159
    const-class v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-static {v0, p0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 468160
    const v0, 0x7f030a3e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 468161
    const v0, 0x7f0d19d5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->b:Landroid/widget/FrameLayout;

    .line 468162
    const v0, 0x7f0d19d6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->c:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    .line 468163
    const v0, 0x7f0d19d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->d:Landroid/widget/ProgressBar;

    .line 468164
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->r:LX/0ad;

    sget-short v1, LX/0ws;->dD:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->f:Z

    .line 468165
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->r:LX/0ad;

    sget v1, LX/0ws;->dE:I

    const/16 v2, 0x1388

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->o:I

    .line 468166
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->r:LX/0ad;

    sget-wide v2, LX/0ws;->dF:J

    const-wide/16 v4, 0x64

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->n:J

    .line 468167
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->u:LX/0Uh;

    const/16 v1, 0xe7

    invoke-virtual {v0, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->p:Z

    .line 468168
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->f:Z

    if-eqz v0, :cond_0

    .line 468169
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->d:Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->getInvisibleConst()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 468170
    :goto_0
    new-instance v0, LX/2pS;

    invoke-direct {v0, p0}, LX/2pS;-><init>(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a:LX/2pS;

    .line 468171
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    .line 468172
    new-instance v1, LX/2pT;

    invoke-direct {v1, p0}, LX/2pT;-><init>(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V

    move-object v1, v1

    .line 468173
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468174
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/2pU;

    invoke-direct {v1, p0}, LX/2pU;-><init>(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468175
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/2pV;

    invoke-direct {v1, p0, p0}, LX/2pV;-><init>(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468176
    return-void

    .line 468177
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->c:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->getInvisibleConst()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object v1, p1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->r:LX/0ad;

    iput-object v2, p1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->s:LX/0Sh;

    iput-object v3, p1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->t:LX/0So;

    iput-object p0, p1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->u:LX/0Uh;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;Z)V
    .locals 4

    .prologue
    .line 468141
    sget-object v0, LX/2qZ;->a:[I

    iget-object v1, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->q:LX/2pR;

    invoke-virtual {v1}, LX/2pR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 468142
    :cond_0
    :goto_0
    return-void

    .line 468143
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->b:Landroid/widget/FrameLayout;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 468144
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->f:Z

    if-eqz v0, :cond_0

    .line 468145
    if-eqz p1, :cond_2

    .line 468146
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->e:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;

    .line 468147
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->b:Z

    .line 468148
    iget-object v2, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v2, v2, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->t:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->d:J

    .line 468149
    iget-object v2, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v2, v2, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->s:LX/0Sh;

    invoke-virtual {v2, v0}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 468150
    goto :goto_0

    .line 468151
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->getInvisibleConst()I

    move-result v0

    goto :goto_1

    .line 468152
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->e:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;

    invoke-static {v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->b$redex0(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;)V

    goto :goto_0

    .line 468153
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->b:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->getInvisibleConst()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 468154
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->f:Z

    if-eqz v0, :cond_0

    .line 468155
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->e:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;

    invoke-static {v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->b$redex0(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static g(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V
    .locals 2

    .prologue
    .line 468180
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a:LX/2pS;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2pS;->removeMessages(I)V

    .line 468181
    return-void
.end method

.method public static i(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V
    .locals 2

    .prologue
    .line 468138
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->c:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->getInvisibleConst()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->setVisibility(I)V

    .line 468139
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->d:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 468140
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 468131
    if-eqz p2, :cond_0

    .line 468132
    sget-object v0, LX/2pR;->DEFAULT:LX/2pR;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->q:LX/2pR;

    .line 468133
    :cond_0
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 468134
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 468135
    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a$redex0(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;Z)V

    .line 468136
    return-void

    .line 468137
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 468128
    invoke-static {p0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->g(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V

    .line 468129
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a$redex0(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;Z)V

    .line 468130
    return-void
.end method

.method public getInvisibleConst()I
    .locals 1

    .prologue
    .line 468127
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public getLoadingSpinnerView()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 468126
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->b:Landroid/widget/FrameLayout;

    return-object v0
.end method
