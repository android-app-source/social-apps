.class public Lcom/facebook/video/player/plugins/VideoPlugin;
.super LX/2oy;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Z

.field public c:D

.field public d:Z

.field public e:LX/19d;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2oz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0sV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:D

.field public q:LX/2p8;

.field public r:Landroid/view/ViewGroup;

.field private s:Z

.field public t:Landroid/graphics/RectF;

.field public u:Z

.field private v:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 467430
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 467431
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 467432
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 467433
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 467397
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 467398
    const-wide v0, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    iput-wide v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->p:D

    .line 467399
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->c:D

    .line 467400
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->u:Z

    .line 467401
    const-class v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-static {v0, p0}, Lcom/facebook/video/player/plugins/VideoPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 467402
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->o:LX/0Uh;

    const/16 v1, 0x2c5

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->b:Z

    .line 467403
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/2p0;

    invoke-direct {v1, p0}, LX/2p0;-><init>(Lcom/facebook/video/player/plugins/VideoPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467404
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/2p1;

    invoke-direct {v1, p0}, LX/2p1;-><init>(Lcom/facebook/video/player/plugins/VideoPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467405
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/2p2;

    invoke-direct {v1, p0}, LX/2p2;-><init>(Lcom/facebook/video/player/plugins/VideoPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467406
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/2p3;

    invoke-direct {v1, p0}, LX/2p3;-><init>(Lcom/facebook/video/player/plugins/VideoPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467407
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->n:LX/0sV;

    iget-boolean v0, v0, LX/0sV;->n:Z

    if-eqz v0, :cond_0

    .line 467408
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7OD;

    invoke-direct {v1, p0}, LX/7OD;-><init>(Lcom/facebook/video/player/plugins/VideoPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467409
    :cond_0
    const v0, 0x7f0315b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 467410
    const v0, 0x7f0d0950

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->r:Landroid/view/ViewGroup;

    .line 467411
    const v0, 0x7f0d30ef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->v:Landroid/widget/ImageView;

    .line 467412
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/VideoPlugin;->i()LX/2p8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    .line 467413
    return-void
.end method

.method private static a(Lcom/facebook/video/player/plugins/VideoPlugin;LX/19d;LX/2oz;LX/0sV;LX/0Uh;LX/0ad;)V
    .locals 0

    .prologue
    .line 467434
    iput-object p1, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->e:LX/19d;

    iput-object p2, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->f:LX/2oz;

    iput-object p3, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->n:LX/0sV;

    iput-object p4, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->o:LX/0Uh;

    iput-object p5, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->a:LX/0ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/player/plugins/VideoPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-static {v5}, LX/19d;->b(LX/0QB;)LX/19d;

    move-result-object v1

    check-cast v1, LX/19d;

    invoke-static {v5}, LX/2oz;->a(LX/0QB;)LX/2oz;

    move-result-object v2

    check-cast v2, LX/2oz;

    invoke-static {v5}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v3

    check-cast v3, LX/0sV;

    invoke-static {v5}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v5}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static/range {v0 .. v5}, Lcom/facebook/video/player/plugins/VideoPlugin;->a(Lcom/facebook/video/player/plugins/VideoPlugin;LX/19d;LX/2oz;LX/0sV;LX/0Uh;LX/0ad;)V

    return-void
.end method

.method public static g(Lcom/facebook/video/player/plugins/VideoPlugin;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 467435
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 467436
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->s:Z

    if-eqz v3, :cond_0

    if-eqz v0, :cond_2

    .line 467437
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->v:Landroid/widget/ImageView;

    if-eqz v1, :cond_3

    const/16 v0, 0x8

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 467438
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->r:Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    :goto_3
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 467439
    return-void

    :cond_1
    move v0, v2

    .line 467440
    goto :goto_0

    :cond_2
    move v1, v2

    .line 467441
    goto :goto_1

    :cond_3
    move v0, v2

    .line 467442
    goto :goto_2

    .line 467443
    :cond_4
    const/4 v2, 0x4

    goto :goto_3
.end method

.method public static setPauseFrame(Lcom/facebook/video/player/plugins/VideoPlugin;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 467444
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->v:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 467445
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->s:Z

    .line 467446
    invoke-static {p0}, Lcom/facebook/video/player/plugins/VideoPlugin;->g(Lcom/facebook/video/player/plugins/VideoPlugin;)V

    .line 467447
    return-void

    .line 467448
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(II)V
    .locals 4

    .prologue
    .line 467449
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 467450
    int-to-double v0, p1

    int-to-double v2, p2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->p:D

    .line 467451
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/VideoPlugin;->v()V

    .line 467452
    :cond_0
    return-void
.end method

.method public a(LX/2pa;)V
    .locals 3

    .prologue
    .line 467453
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->a:LX/0ad;

    sget-short v1, LX/0ws;->eG:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467454
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/facebook/video/player/plugins/VideoPlugin;->a(LX/2pa;Z)V

    .line 467455
    :goto_0
    return-void

    .line 467456
    :cond_0
    invoke-super {p0, p1}, LX/2oy;->a(LX/2pa;)V

    goto :goto_0
.end method

.method public a(LX/2pa;Z)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const-wide/16 v8, 0x0

    .line 467414
    iget-wide v2, p1, LX/2pa;->d:D

    .line 467415
    cmpl-double v1, v2, v8

    if-eqz v1, :cond_7

    iget-wide v4, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->p:D

    sub-double v4, v2, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v1, v4, v6

    if-lez v1, :cond_7

    const/4 v1, 0x1

    .line 467416
    :goto_0
    if-eqz p2, :cond_0

    .line 467417
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    iput-wide v4, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->c:D

    .line 467418
    :cond_0
    if-nez p2, :cond_1

    if-eqz v1, :cond_3

    .line 467419
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/VideoPlugin;->j()V

    .line 467420
    cmpl-double v1, v2, v8

    if-eqz v1, :cond_2

    .line 467421
    iput-wide v2, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->p:D

    .line 467422
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/VideoPlugin;->v()V

    .line 467423
    :cond_3
    if-nez p2, :cond_4

    iget-boolean v1, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->s:Z

    if-nez v1, :cond_6

    .line 467424
    :cond_4
    iget-object v1, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_5

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    .line 467425
    :cond_5
    if-lez v0, :cond_6

    .line 467426
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->f:LX/2oz;

    iget-object v1, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2oz;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 467427
    invoke-static {p0, v0}, Lcom/facebook/video/player/plugins/VideoPlugin;->setPauseFrame(Lcom/facebook/video/player/plugins/VideoPlugin;Landroid/graphics/Bitmap;)V

    .line 467428
    :cond_6
    return-void

    :cond_7
    move v1, v0

    .line 467429
    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 467457
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467458
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->a()V

    .line 467459
    :cond_0
    return-void
.end method

.method public getAdjustedVideoSize()Landroid/graphics/RectF;
    .locals 14

    .prologue
    .line 467351
    iget-object v0, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->t:Landroid/graphics/RectF;

    iget-wide v2, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->p:D

    const/4 v12, 0x0

    .line 467352
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 467353
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 467354
    if-lez v5, :cond_0

    if-lez v4, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v6

    cmpg-float v6, v6, v12

    if-lez v6, :cond_0

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v6

    cmpg-float v6, v6, v12

    if-lez v6, :cond_0

    const-wide/16 v6, 0x0

    cmpg-double v6, v2, v6

    if-gtz v6, :cond_1

    .line 467355
    :cond_0
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v12, v12, v12, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 467356
    :goto_0
    move-object v0, v4

    .line 467357
    return-object v0

    .line 467358
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v7

    div-float/2addr v6, v7

    float-to-double v6, v6

    .line 467359
    mul-double/2addr v6, v2

    .line 467360
    int-to-double v8, v5

    int-to-double v10, v4

    div-double/2addr v8, v10

    .line 467361
    cmpl-double v6, v6, v8

    if-lez v6, :cond_2

    .line 467362
    int-to-double v6, v5

    div-double/2addr v6, v2

    double-to-int v4, v6

    .line 467363
    :goto_1
    new-instance v6, Landroid/graphics/RectF;

    int-to-float v5, v5

    int-to-float v4, v4

    invoke-direct {v6, v12, v12, v5, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object v4, v6

    goto :goto_0

    .line 467364
    :cond_2
    int-to-double v6, v4

    mul-double/2addr v6, v2

    double-to-int v5, v6

    goto :goto_1
.end method

.method public getCropRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 467365
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->t:Landroid/graphics/RectF;

    return-object v0
.end method

.method public i()LX/2p8;
    .locals 2

    .prologue
    .line 467366
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->e:LX/19d;

    sget-object v1, LX/2p7;->NORMAL:LX/2p7;

    invoke-virtual {v0, v1}, LX/19d;->a(LX/2p7;)LX/2p8;

    move-result-object v0

    return-object v0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 467367
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467368
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 467369
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, LX/2p8;->a(Landroid/view/ViewGroup;)V

    .line 467370
    :cond_0
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0, v1}, LX/2pb;->a(LX/2p8;)V

    .line 467371
    return-void
.end method

.method public setCropRect(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 467372
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->t:Landroid/graphics/RectF;

    .line 467373
    return-void
.end method

.method public setNeedCentering(Z)V
    .locals 0

    .prologue
    .line 467374
    iput-boolean p1, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->u:Z

    .line 467375
    return-void
.end method

.method public setShouldCropToFit(Z)V
    .locals 0

    .prologue
    .line 467376
    iput-boolean p1, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->d:Z

    .line 467377
    return-void
.end method

.method public setVideoPluginAlignment(LX/7OE;)V
    .locals 5

    .prologue
    const/16 v4, 0xf

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 467378
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/2p4;

    .line 467379
    invoke-virtual {v0, v4, v1}, LX/2p4;->addRule(II)V

    .line 467380
    invoke-virtual {v0, v3, v1}, LX/2p4;->addRule(II)V

    .line 467381
    sget-object v1, LX/7OC;->a:[I

    invoke-virtual {p1}, LX/7OE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 467382
    :goto_0
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->r:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 467383
    return-void

    .line 467384
    :pswitch_0
    invoke-virtual {v0, v3}, LX/2p4;->addRule(I)V

    goto :goto_0

    .line 467385
    :pswitch_1
    invoke-virtual {v0, v4}, LX/2p4;->addRule(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setVideoRotation(F)V
    .locals 2

    .prologue
    .line 467386
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->r:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    invoke-static {v0, v1, p1}, LX/2pC;->a(Landroid/view/View;Landroid/view/View;F)V

    .line 467387
    return-void
.end method

.method public setVideoVisibilty(I)V
    .locals 1

    .prologue
    .line 467388
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 467389
    return-void
.end method

.method public final v()V
    .locals 9

    .prologue
    .line 467390
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    .line 467391
    iget-object v1, v0, LX/2p8;->i:Landroid/view/TextureView;

    move-object v1, v1

    .line 467392
    if-eqz v1, :cond_0

    .line 467393
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->t:Landroid/graphics/RectF;

    if-eqz v0, :cond_1

    .line 467394
    iget-object v0, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->t:Landroid/graphics/RectF;

    iget-wide v4, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->p:D

    invoke-static {v0, v1, v2, v4, v5}, LX/2pC;->a(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;D)V

    .line 467395
    :cond_0
    :goto_0
    return-void

    .line 467396
    :cond_1
    iget-object v0, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    iget-wide v2, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->p:D

    iget-wide v4, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->c:D

    iget-boolean v6, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->d:Z

    iget-boolean v7, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->u:Z

    iget-boolean v8, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->b:Z

    invoke-static/range {v0 .. v8}, LX/2pC;->a(Landroid/view/View;Landroid/view/View;DDZZZ)V

    goto :goto_0
.end method
