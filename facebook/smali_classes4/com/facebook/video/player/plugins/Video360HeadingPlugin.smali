.class public Lcom/facebook/video/player/plugins/Video360HeadingPlugin;
.super LX/2oy;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public a:LX/3IZ;

.field public b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 546138
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 546139
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 546140
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546141
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 546142
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546143
    const p1, 0x7f03157b

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 546144
    const p1, 0x7f0d24df

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iput-object p1, p0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    .line 546145
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/3GT;

    invoke-direct {p2, p0}, LX/3GT;-><init>(Lcom/facebook/video/player/plugins/Video360HeadingPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546146
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/3IU;

    invoke-direct {p2, p0}, LX/3IU;-><init>(Lcom/facebook/video/player/plugins/Video360HeadingPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546147
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/3IV;

    invoke-direct {p2, p0}, LX/3IV;-><init>(Lcom/facebook/video/player/plugins/Video360HeadingPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546148
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/3IW;

    invoke-direct {p2, p0}, LX/3IW;-><init>(Lcom/facebook/video/player/plugins/Video360HeadingPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546149
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/3IX;

    invoke-direct {p2, p0}, LX/3IX;-><init>(Lcom/facebook/video/player/plugins/Video360HeadingPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546150
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/3IY;

    invoke-direct {p2, p0}, LX/3IY;-><init>(Lcom/facebook/video/player/plugins/Video360HeadingPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546151
    new-instance p1, LX/3IZ;

    invoke-direct {p1, p0}, LX/3IZ;-><init>(Lcom/facebook/video/player/plugins/Video360HeadingPlugin;)V

    iput-object p1, p0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->a:LX/3IZ;

    .line 546152
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 546153
    invoke-super {p0, p1, p2}, LX/2oy;->a(LX/2pa;Z)V

    .line 546154
    const/4 v0, 0x0

    .line 546155
    if-eqz p1, :cond_0

    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v1, :cond_0

    .line 546156
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 546157
    :cond_0
    iget-object v3, p0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-static {v0}, LX/7E5;->a(Lcom/facebook/spherical/model/SphericalVideoParams;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 546158
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/spherical/model/SphericalVideoParams;->a()LX/03z;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/spherical/model/SphericalVideoParams;->a()LX/03z;

    move-result-object v1

    iget-boolean v1, v1, LX/03z;->isSpatial:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 546159
    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    iget-object v4, p0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->a:LX/3IZ;

    invoke-virtual {v3, v0, p2, v1, v4}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(LX/7DE;ZZLX/3Ia;)V

    .line 546160
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0, v2}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->setClickable(Z)V

    .line 546161
    return-void

    .line 546162
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 546163
    invoke-super {p0}, LX/2oy;->d()V

    .line 546164
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b()V

    .line 546165
    return-void
.end method
