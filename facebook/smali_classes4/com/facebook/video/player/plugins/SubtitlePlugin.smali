.class public Lcom/facebook/video/player/plugins/SubtitlePlugin;
.super LX/3Ga;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final b:LX/2pw;

.field private c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

.field private d:LX/7QP;

.field public e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 541696
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/SubtitlePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 541697
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 541694
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/SubtitlePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 541695
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 541688
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 541689
    new-instance v0, LX/3Gi;

    invoke-direct {v0, p0}, LX/3Gi;-><init>(Lcom/facebook/video/player/plugins/SubtitlePlugin;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->b:LX/2pw;

    .line 541690
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Gj;

    invoke-direct {v1, p0}, LX/3Gj;-><init>(Lcom/facebook/video/player/plugins/SubtitlePlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541691
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Gk;

    invoke-direct {v1, p0}, LX/3Gk;-><init>(Lcom/facebook/video/player/plugins/SubtitlePlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541692
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Gl;

    invoke-direct {v1, p0}, LX/3Gl;-><init>(Lcom/facebook/video/player/plugins/SubtitlePlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541693
    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/player/plugins/SubtitlePlugin;LX/2qV;)V
    .locals 2

    .prologue
    .line 541682
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    if-nez v0, :cond_0

    .line 541683
    :goto_0
    return-void

    .line 541684
    :cond_0
    sget-object v0, LX/7Na;->a:[I

    invoke-virtual {p1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 541685
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->d()V

    goto :goto_0

    .line 541686
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->c()V

    goto :goto_0

    .line 541687
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/video/player/plugins/SubtitlePlugin;LX/7QP;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 541673
    iput-object p1, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->d:LX/7QP;

    .line 541674
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 541675
    iput-boolean v3, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->e:Z

    .line 541676
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->b:LX/2pw;

    iget-object v2, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->d:LX/7QP;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a(LX/2pw;LX/7QP;)V

    .line 541677
    invoke-virtual {p0, v3}, Lcom/facebook/video/player/plugins/SubtitlePlugin;->a(Z)V

    .line 541678
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 541679
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 541680
    invoke-static {p0, v0}, Lcom/facebook/video/player/plugins/SubtitlePlugin;->a$redex0(Lcom/facebook/video/player/plugins/SubtitlePlugin;LX/2qV;)V

    .line 541681
    :cond_0
    return-void
.end method

.method public static i(Lcom/facebook/video/player/plugins/SubtitlePlugin;)V
    .locals 1

    .prologue
    .line 541668
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->d:LX/7QP;

    .line 541669
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    if-eqz v0, :cond_0

    .line 541670
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->f()V

    .line 541671
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->e:Z

    .line 541672
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 541698
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541699
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 541700
    iget-object p1, v0, LX/2pb;->J:LX/7QP;

    move-object v0, p1

    .line 541701
    if-eqz v0, :cond_0

    .line 541702
    invoke-static {p0, v0}, Lcom/facebook/video/player/plugins/SubtitlePlugin;->a$redex0(Lcom/facebook/video/player/plugins/SubtitlePlugin;LX/7QP;)V

    .line 541703
    :goto_0
    return-void

    .line 541704
    :cond_0
    invoke-static {p0}, Lcom/facebook/video/player/plugins/SubtitlePlugin;->i(Lcom/facebook/video/player/plugins/SubtitlePlugin;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 541663
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    if-nez v0, :cond_0

    .line 541664
    :goto_0
    return-void

    .line 541665
    :cond_0
    if-eqz p1, :cond_1

    .line 541666
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a()V

    goto :goto_0

    .line 541667
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->b()V

    goto :goto_0
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 541662
    iget-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->d:LX/7QP;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 541660
    invoke-static {p0}, Lcom/facebook/video/player/plugins/SubtitlePlugin;->i(Lcom/facebook/video/player/plugins/SubtitlePlugin;)V

    .line 541661
    return-void
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 541659
    const v0, 0x7f031414

    return v0
.end method

.method public getStubLayout()I
    .locals 1

    .prologue
    .line 541658
    const v0, 0x7f031415

    return v0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 0

    .prologue
    .line 541657
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 541655
    const v0, 0x7f0d175e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/subtitles/views/FbSubtitleView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/SubtitlePlugin;->c:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    .line 541656
    return-void
.end method
