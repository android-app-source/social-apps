.class public final Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

.field public b:Z

.field private c:J

.field public d:J


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 468182
    iput-object p1, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 468183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->b:Z

    .line 468184
    iput-wide v2, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->c:J

    .line 468185
    iput-wide v2, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->d:J

    return-void
.end method

.method public static b$redex0(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 468186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->b:Z

    .line 468187
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->c:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(J)V

    .line 468188
    iput-wide v2, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->c:J

    .line 468189
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 468190
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->b:Z

    if-nez v0, :cond_0

    .line 468191
    :goto_0
    return-void

    .line 468192
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    .line 468193
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 468194
    iget-object v6, v0, LX/2pb;->E:LX/2q7;

    invoke-interface {v6}, LX/2q7;->v()J

    move-result-wide v6

    move-wide v0, v6

    .line 468195
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 468196
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-static {v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->i(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468197
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->s:LX/0Sh;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-wide v2, v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->n:J

    invoke-virtual {v0, p0, v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 468198
    :cond_2
    :try_start_1
    iget-wide v2, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->c:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    .line 468199
    iget-object v2, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    .line 468200
    iget-object v3, v2, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->c:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->setVisibility(I)V

    .line 468201
    iget-object v3, v2, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->getInvisibleConst()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 468202
    iput-wide v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->c:J

    .line 468203
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->t:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->d:J

    .line 468204
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->c:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    iget-wide v2, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 468205
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->s:LX/0Sh;

    iget-object v2, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-wide v2, v2, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->n:J

    invoke-virtual {v1, p0, v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    throw v0

    .line 468206
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->t:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->d:J

    iget-object v4, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget v4, v4, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->o:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 468207
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin$ProgressPoller;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-static {v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->i(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
