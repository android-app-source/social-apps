.class public Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;
.super LX/2oy;
.source ""

# interfaces
.implements LX/3Gr;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field public static final q:Ljava/lang/String;

.field public static final r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/2oy;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:LX/0lF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:LX/2pa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:LX/D6v;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Z

.field private F:Z

.field public G:I

.field public H:J

.field public I:J

.field public a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3HR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/3HS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/3HT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/14v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final s:LX/3Gv;

.field private final t:LX/3Gw;

.field private final u:LX/3Gx;

.field private final v:LX/3Gz;

.field private final w:I

.field public final x:LX/157;

.field public y:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:LX/2pa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 542124
    const-class v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->q:Ljava/lang/String;

    .line 542125
    const-class v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->r:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 542022
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 542023
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 542122
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 542123
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    .line 542110
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 542111
    new-instance v0, LX/3Gv;

    invoke-direct {v0, p0}, LX/3Gv;-><init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->s:LX/3Gv;

    .line 542112
    new-instance v0, LX/3Gw;

    invoke-direct {v0, p0}, LX/3Gw;-><init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->t:LX/3Gw;

    .line 542113
    new-instance v0, LX/3Gx;

    invoke-direct {v0, p0}, LX/3Gx;-><init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->u:LX/3Gx;

    .line 542114
    new-instance v0, LX/3Gz;

    invoke-direct {v0, p0}, LX/3Gz;-><init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->v:LX/3Gz;

    .line 542115
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->w:I

    .line 542116
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static {v0}, LX/3HR;->b(LX/0QB;)LX/3HR;

    move-result-object v4

    check-cast v4, LX/3HR;

    invoke-static {v0}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object v5

    check-cast v5, LX/2ml;

    invoke-static {v0}, LX/3HS;->a(LX/0QB;)LX/3HS;

    move-result-object v6

    check-cast v6, LX/3HS;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p1

    check-cast p1, LX/0So;

    invoke-static {v0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object p2

    check-cast p2, LX/3HT;

    const/16 p3, 0x122d

    invoke-static {v0, p3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p3

    invoke-static {v0}, LX/14v;->a(LX/0QB;)LX/14v;

    move-result-object v0

    check-cast v0, LX/14v;

    iput-object v3, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iput-object v4, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->b:LX/3HR;

    iput-object v5, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->c:LX/2ml;

    iput-object v6, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->d:LX/3HS;

    iput-object v7, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->e:LX/03V;

    iput-object p1, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->f:LX/0So;

    iput-object p2, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->n:LX/3HT;

    iput-object p3, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->o:LX/0Or;

    iput-object v0, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->p:LX/14v;

    .line 542117
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3HU;

    invoke-direct {v1, p0}, LX/3HU;-><init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542118
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3HV;

    invoke-direct {v1, p0}, LX/3HV;-><init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542119
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3HW;

    invoke-direct {v1, p0}, LX/3HW;-><init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542120
    new-instance v0, LX/3HX;

    invoke-direct {v0, p0}, LX/3HX;-><init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->x:LX/157;

    .line 542121
    return-void
.end method

.method public static getLiveStreamingFormat(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)LX/BSW;
    .locals 2

    .prologue
    .line 542097
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->w()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 542098
    :cond_0
    sget-object v0, LX/BSW;->UNKNOWN:LX/BSW;

    .line 542099
    :goto_0
    return-object v0

    .line 542100
    :cond_1
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->w()Ljava/lang/String;

    move-result-object v0

    .line 542101
    const-string v1, "dash"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 542102
    sget-object v0, LX/BSW;->DASH:LX/BSW;

    goto :goto_0

    .line 542103
    :cond_2
    const-string v1, "dash_live"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 542104
    sget-object v0, LX/BSW;->DASH_LIVE:LX/BSW;

    goto :goto_0

    .line 542105
    :cond_3
    const-string v1, "progressive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 542106
    sget-object v0, LX/BSW;->PROGRESSIVE_DOWNLOAD:LX/BSW;

    goto :goto_0

    .line 542107
    :cond_4
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 542108
    sget-object v0, LX/BSW;->HLS:LX/BSW;

    goto :goto_0

    .line 542109
    :cond_5
    sget-object v0, LX/BSW;->UNKNOWN:LX/BSW;

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 542094
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget-object v0, v0, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-eq v0, v1, :cond_0

    .line 542095
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->B:LX/0lF;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/0lF;)V

    .line 542096
    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 542091
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget-object v0, v0, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-eq v0, v1, :cond_0

    .line 542092
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->B:LX/0lF;

    invoke-virtual {v0, v1}, LX/D6v;->b(LX/0lF;)V

    .line 542093
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 6

    .prologue
    .line 542051
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 542052
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-nez v0, :cond_1

    .line 542053
    :cond_0
    :goto_0
    return-void

    .line 542054
    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542055
    invoke-static {p1}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 542056
    if-eqz v1, :cond_0

    .line 542057
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v2, "GraphQLStoryProps"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 542058
    instance-of v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_2

    .line 542059
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 542060
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 542061
    if-eqz v0, :cond_0

    .line 542062
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 542063
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 542064
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542065
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 542066
    invoke-static {v0}, LX/3In;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542067
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->c:LX/2ml;

    iget-boolean v0, v0, LX/2ml;->a:Z

    if-eqz v0, :cond_0

    .line 542068
    iput-object p1, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->z:LX/2pa;

    .line 542069
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    .line 542070
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->B:LX/0lF;

    .line 542071
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    iput-boolean v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->F:Z

    .line 542072
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_3

    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    .line 542073
    :goto_1
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v3, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->s:LX/3Gv;

    iget-boolean v5, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->F:Z

    .line 542074
    iget-object p1, v2, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e:LX/3H1;

    iget-object p2, v2, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a:LX/3Fh;

    invoke-virtual {p1, v3, p2, v5}, LX/3H1;->a(Ljava/lang/String;LX/3Fh;Z)V

    .line 542075
    iget-object p1, v2, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->l:Ljava/util/Map;

    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {p1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542076
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v3, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->v:LX/3Gz;

    .line 542077
    iget-object v5, v2, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->n:Ljava/util/Map;

    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v5, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542078
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v3, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    .line 542079
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget-object v3, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->t:LX/3Gw;

    invoke-virtual {v2, v3}, LX/D6v;->a(LX/3GF;)V

    .line 542080
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    sget-object v3, LX/3H0;->LIVE:LX/3H0;

    .line 542081
    iput-object v3, v2, LX/D6v;->w:LX/3H0;

    .line 542082
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget-boolean v3, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->F:Z

    .line 542083
    iput-boolean v3, v2, LX/D6v;->C:Z

    .line 542084
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    .line 542085
    iput-object v0, v2, LX/D6v;->G:LX/04D;

    .line 542086
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->z:LX/2pa;

    iget-wide v4, v3, LX/2pa;->d:D

    double-to-float v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Ljava/lang/String;F)V

    .line 542087
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->d:LX/3HS;

    invoke-virtual {v0, p0}, LX/3HS;->a(LX/3Gr;)V

    .line 542088
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->n:LX/3HT;

    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->u:LX/3Gx;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 542089
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->p:LX/14v;

    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->x:LX/157;

    invoke-virtual {v0, v1, v2}, LX/14v;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/157;)V

    goto/16 :goto_0

    .line 542090
    :cond_3
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->s()LX/04D;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(LX/D6q;)V
    .locals 4

    .prologue
    .line 542047
    sget-object v0, LX/D7A;->b:[I

    invoke-virtual {p1}, LX/D6q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 542048
    :cond_0
    :goto_0
    return-void

    .line 542049
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    .line 542050
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    const-wide/16 v2, 0x0

    sget-object v1, LX/BST;->HIDE_AD:LX/BST;

    invoke-virtual {v0, v2, v3, v1}, LX/D6v;->a(JLX/BST;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 542039
    iput-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 542040
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->d:LX/3HS;

    invoke-virtual {v0, p0}, LX/3HS;->b(LX/3Gr;)V

    .line 542041
    iput-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    .line 542042
    iput-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->C:LX/2pa;

    .line 542043
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->E:Z

    .line 542044
    iput-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    .line 542045
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->n:LX/3HT;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->u:LX/3Gx;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 542046
    return-void
.end method

.method public final dH_()V
    .locals 0

    .prologue
    .line 542037
    invoke-direct {p0}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->i()V

    .line 542038
    return-void
.end method

.method public final dI_()V
    .locals 4

    .prologue
    .line 542029
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->H:J

    .line 542030
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 542031
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 542032
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-eq v0, v1, :cond_1

    .line 542033
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    .line 542034
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, LX/D6v;->f:J

    .line 542035
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->i()V

    .line 542036
    :cond_1
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 542026
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    .line 542027
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    invoke-virtual {v0}, LX/D6v;->f()V

    .line 542028
    :cond_0
    return-void
.end method

.method public final s()V
    .locals 0

    .prologue
    .line 542024
    invoke-direct {p0}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->j()V

    .line 542025
    return-void
.end method

.method public final t()V
    .locals 10

    .prologue
    .line 542003
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 542004
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 542005
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-eq v0, v1, :cond_1

    .line 542006
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    .line 542007
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v0, LX/D6v;->f:J

    .line 542008
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->j()V

    .line 542009
    :cond_1
    iget-wide v4, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->H:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->f:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->H:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x2710

    cmp-long v4, v4, v6

    if-gez v4, :cond_3

    .line 542010
    :cond_2
    :goto_0
    return-void

    .line 542011
    :cond_3
    iget-object v4, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    if-nez v4, :cond_4

    const-string v4, "-1"

    move-object v5, v4

    .line 542012
    :goto_1
    iget-object v4, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget v4, v4, LX/D6v;->d:I

    const/4 v6, -0x1

    if-eq v4, v6, :cond_5

    .line 542013
    iget-object v4, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->o:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0gt;

    const-string v6, "218383881911011"

    .line 542014
    iput-object v6, v4, LX/0gt;->a:Ljava/lang/String;

    .line 542015
    move-object v4, v4

    .line 542016
    const-string v6, "video_id"

    invoke-virtual {v4, v6, v5}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v4

    const-string v5, "ad_id"

    iget-object v6, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    invoke-virtual {v6}, LX/D6v;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v4

    const-string v5, "elapsed_time_since_ad"

    iget-object v6, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->f:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->I:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v4

    const-string v5, "ad_position_in_video"

    iget-object v6, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget v6, v6, LX/D6v;->d:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v4

    const-string v5, "integration_point_name"

    const-string v6, "ISLIVE_COMMERCIAL_BREAK_IMPRESSION_STOP_WATCHING"

    invoke-virtual {v4, v5, v6}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 542017
    :cond_4
    iget-object v4, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    move-object v5, v4

    goto :goto_1

    .line 542018
    :cond_5
    iget-object v4, p0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->o:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0gt;

    const-string v6, "1785253761723390"

    .line 542019
    iput-object v6, v4, LX/0gt;->a:Ljava/lang/String;

    .line 542020
    move-object v4, v4

    .line 542021
    const-string v6, "video_id"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v4

    const-string v5, "integration_point_name"

    const-string v6, "ISLIVE_NO_COMMERCIAL_BREAK_IMPRESSION_STOP_WATCHING"

    invoke-virtual {v4, v5, v6}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0gt;->a(Landroid/content/Context;)V

    goto/16 :goto_0
.end method
