.class public Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;
.super LX/3Ga;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public b:LX/2ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLMedia;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Landroid/widget/TextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:LX/D73;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/D74;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:D

.field public y:Lcom/facebook/common/callercontext/CallerContext;

.field public z:LX/3HY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 543656
    const-class v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 543670
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v2}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 543671
    sget-object v0, LX/3HY;->REGULAR:LX/3HY;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->z:LX/3HY;

    .line 543672
    const-class v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    invoke-static {v0, p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 543673
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3HZ;

    invoke-direct {v1, p0}, LX/3HZ;-><init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 543674
    iput-object p2, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->y:Lcom/facebook/common/callercontext/CallerContext;

    .line 543675
    return-void
.end method

.method public static B(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V
    .locals 2

    .prologue
    .line 543667
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 543668
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 543669
    :cond_0
    return-void
.end method

.method public static D(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V
    .locals 1

    .prologue
    .line 543663
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543664
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 543665
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->d(Landroid/view/View;)V

    .line 543666
    :cond_0
    return-void
.end method

.method public static G(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V
    .locals 1

    .prologue
    .line 543659
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->a(Landroid/view/View;)V

    .line 543660
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->q:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->a(Landroid/view/View;)V

    .line 543661
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->a(Landroid/view/View;)V

    .line 543662
    return-void
.end method

.method private static a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 543657
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, -0x3db80000    # -50.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 543658
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    invoke-static {p0}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object p0

    check-cast p0, LX/2ml;

    iput-object p0, p1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->b:LX/2ml;

    return-void
.end method

.method public static b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 543591
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 543592
    return-void
.end method

.method public static c(Landroid/view/View;)V
    .locals 6

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 543649
    const-string v0, "scaleX"

    invoke-static {p0, v0, v2, v4}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;FF)LX/4mQ;

    move-result-object v0

    invoke-static {v0}, LX/BSf;->a(LX/4mQ;)LX/4mQ;

    move-result-object v0

    .line 543650
    const-string v1, "scaleY"

    invoke-static {p0, v1, v2, v4}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;FF)LX/4mQ;

    move-result-object v1

    invoke-static {v1}, LX/BSf;->a(LX/4mQ;)LX/4mQ;

    move-result-object v1

    .line 543651
    const-string v2, "alpha"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3, v4}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;FF)LX/4mQ;

    move-result-object v2

    invoke-static {v2}, LX/BSf;->a(LX/4mQ;)LX/4mQ;

    move-result-object v2

    .line 543652
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 543653
    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v2, v4, v0

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 543654
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 543655
    return-void
.end method

.method public static d(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 543676
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, LX/BSf;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 543677
    return-void
.end method

.method public static y(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V
    .locals 2

    .prologue
    .line 543646
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 543647
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 543648
    :cond_0
    return-void
.end method

.method public static z(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V
    .locals 2

    .prologue
    .line 543643
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 543644
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 543645
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    .line 543610
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->b:LX/2ml;

    iget-boolean v0, v0, LX/2ml;->a:Z

    if-nez v0, :cond_1

    .line 543611
    :cond_0
    :goto_0
    return-void

    .line 543612
    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 543613
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 543614
    if-eqz v0, :cond_0

    .line 543615
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 543616
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    .line 543617
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 543618
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->e:Lcom/facebook/graphql/model/GraphQLActor;

    .line 543619
    :cond_2
    sget-object v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->c:Ljava/lang/String;

    invoke-static {p1, v0}, LX/3JF;->a(LX/2pa;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543620
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 543621
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 543622
    sget-object v1, LX/04G;->INLINE_PLAYER:LX/04G;

    if-ne v0, v1, :cond_3

    .line 543623
    invoke-virtual {p1}, LX/2pa;->f()LX/3HY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->z:LX/3HY;

    .line 543624
    :cond_3
    iget-wide v0, p1, LX/2pa;->d:D

    iput-wide v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->x:D

    .line 543625
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->f:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 543626
    invoke-virtual {p0}, LX/3Ga;->g()Z

    .line 543627
    const/16 p2, 0x8

    const/4 p1, 0x0

    .line 543628
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->q:Landroid/widget/TextView;

    if-nez v0, :cond_5

    .line 543629
    :cond_4
    :goto_1
    invoke-static {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->z(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543630
    invoke-static {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->D(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543631
    goto :goto_0

    .line 543632
    :cond_5
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->f:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->f:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->f:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    .line 543633
    :cond_6
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 543634
    :goto_2
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->e:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->e:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->e:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    .line 543635
    :cond_7
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1

    .line 543636
    :cond_8
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->f:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 543637
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->y:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 543638
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_2

    .line 543639
    :cond_9
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->e:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 543640
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->y:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 543641
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 543642
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->e:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 543609
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 543605
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 543606
    if-eqz v0, :cond_0

    .line 543607
    invoke-static {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->z(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;)V

    .line 543608
    :cond_0
    return-void
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 543604
    const v0, 0x7f030985

    return v0
.end method

.method public getStubLayout()I
    .locals 1

    .prologue
    .line 543603
    const v0, 0x7f030986

    return v0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 0

    .prologue
    .line 543602
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 543593
    const v0, 0x7f0d0553

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->d:Landroid/view/View;

    .line 543594
    const v0, 0x7f0d1853

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->w:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionContainerLinearLayout;

    .line 543595
    const v0, 0x7f0d05d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 543596
    const v0, 0x7f0d0575

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 543597
    const v0, 0x7f0d02c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->q:Landroid/widget/TextView;

    .line 543598
    const v0, 0x7f0d1854

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 543599
    const v0, 0x7f0d1855

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 543600
    const v0, 0x7f0d1856

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 543601
    return-void
.end method
