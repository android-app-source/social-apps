.class public Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;
.super LX/3Ga;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final r:Ljava/lang/String;


# instance fields
.field public A:LX/D7C;

.field public B:LX/D6v;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Z

.field public D:LX/04g;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:D

.field public b:LX/2mZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/3H4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0iX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0iY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:LX/D71;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Landroid/view/View;

.field public w:Lcom/facebook/video/commercialbreak/plugins/CommercialBreakVideoContainerFrameLayout;

.field public x:Landroid/widget/TextView;

.field public y:Lcom/facebook/video/player/RichVideoPlayer;

.field private z:LX/D70;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 544790
    const-class v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->r:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 544914
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 544915
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 544912
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 544913
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    .line 544908
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 544909
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    const-class v3, LX/2mZ;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2mZ;

    invoke-static {v0}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object v4

    check-cast v4, LX/2ml;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static {v0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v6

    check-cast v6, LX/1C2;

    invoke-static {v0}, LX/3H4;->a(LX/0QB;)LX/3H4;

    move-result-object p1

    check-cast p1, LX/3H4;

    invoke-static {v0}, LX/0iX;->a(LX/0QB;)LX/0iX;

    move-result-object p2

    check-cast p2, LX/0iX;

    invoke-static {v0}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object p3

    check-cast p3, LX/0iY;

    invoke-static {v0}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v3, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->b:LX/2mZ;

    iput-object v4, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->c:LX/2ml;

    iput-object v5, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->d:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iput-object v6, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->e:LX/1C2;

    iput-object p1, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->f:LX/3H4;

    iput-object p2, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->o:LX/0iX;

    iput-object p3, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->p:LX/0iY;

    iput-object v0, v2, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->q:Ljava/lang/Boolean;

    .line 544910
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Ho;

    invoke-direct {v1, p0}, LX/3Ho;-><init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 544911
    return-void
.end method

.method public static b(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 544869
    invoke-static {p1}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 544870
    if-nez v1, :cond_0

    .line 544871
    :goto_0
    return-void

    .line 544872
    :cond_0
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 544873
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    .line 544874
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->b:LX/2mZ;

    invoke-virtual {v0, v1, v2}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v0

    .line 544875
    invoke-virtual {v0, v4, v4}, LX/3Im;->a(ZZ)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v7

    .line 544876
    const-wide v0, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    .line 544877
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v3

    .line 544878
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v2

    .line 544879
    if-lez v2, :cond_1

    if-lez v3, :cond_1

    .line 544880
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    int-to-double v12, v3

    mul-double/2addr v0, v12

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 544881
    :cond_1
    new-instance v2, LX/2pZ;

    invoke-direct {v2}, LX/2pZ;-><init>()V

    const-string v3, "GraphQLStoryProps"

    iget-object v5, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->t:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3, v5}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v2

    .line 544882
    iput-object v7, v2, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 544883
    move-object v2, v2

    .line 544884
    invoke-virtual {v2, v0, v1}, LX/2pZ;->a(D)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 544885
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 544886
    iget-object v1, p0, LX/3Gb;->n:LX/7Lf;

    if-eqz v1, :cond_2

    .line 544887
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, LX/3Gb;->n:LX/7Lf;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPluginEnvironment(LX/7Lf;)V

    .line 544888
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 544889
    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    .line 544890
    iget-object v2, v1, LX/2pb;->D:LX/04G;

    move-object v2, v2

    .line 544891
    :goto_1
    iget-object v1, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 544892
    iget-object v3, v1, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v6, v3

    .line 544893
    :goto_2
    sget-object v1, LX/04G;->INLINE_PLAYER:LX/04G;

    if-ne v2, v1, :cond_7

    .line 544894
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->o:LX/0iX;

    invoke-virtual {v2, v6, v0}, LX/0iX;->a(LX/04D;LX/2pa;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v8

    :goto_3
    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 544895
    :goto_4
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/7OE;->CENTER:LX/7OE;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    .line 544896
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v8}, Lcom/facebook/video/player/RichVideoPlayer;->setIsInstreamVideoAdPlayer(Z)V

    .line 544897
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->B:LX/D6v;

    if-eqz v0, :cond_a

    .line 544898
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->B:LX/D6v;

    iget-wide v0, v0, LX/D6v;->c:J

    .line 544899
    :goto_5
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    cmp-long v3, v0, v10

    if-lez v3, :cond_3

    long-to-int v4, v0

    :cond_3
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v2, v4, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 544900
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto/16 :goto_0

    .line 544901
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 544902
    :cond_5
    sget-object v6, LX/04D;->UNKNOWN:LX/04D;

    goto :goto_2

    :cond_6
    move v0, v4

    .line 544903
    goto :goto_3

    .line 544904
    :cond_7
    sget-object v0, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-eq v2, v0, :cond_8

    sget-object v0, LX/04G;->CHANNEL_PLAYER:LX/04G;

    if-ne v2, v0, :cond_9

    .line 544905
    :cond_8
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v4, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 544906
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->e:LX/1C2;

    iget-object v1, v7, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v3, LX/04g;->BY_AUTOPLAY:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    iget-object v5, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    goto :goto_4

    .line 544907
    :cond_9
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v8, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    goto :goto_4

    :cond_a
    move-wide v0, v10

    goto :goto_5
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 544859
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->c:LX/2ml;

    iget-boolean v0, v0, LX/2ml;->a:Z

    if-nez v0, :cond_1

    .line 544860
    :cond_0
    :goto_0
    return-void

    .line 544861
    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 544862
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->t:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 544863
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->t:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 544864
    sget-object v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->r:Ljava/lang/String;

    invoke-static {p1, v0}, LX/3JF;->a(LX/2pa;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 544865
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->s:Ljava/lang/String;

    .line 544866
    iget-wide v0, p1, LX/2pa;->d:D

    iput-wide v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->E:D

    .line 544867
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    iput-boolean v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->C:Z

    .line 544868
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->d:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->B:LX/D6v;

    goto :goto_0
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 544858
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 544916
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->v:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 544917
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->v:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 544918
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    .line 544919
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 544920
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 544921
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->s:Ljava/lang/String;

    .line 544922
    return-void
.end method

.method public final dI_()V
    .locals 2

    .prologue
    .line 544852
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 544853
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 544854
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_1

    .line 544855
    :cond_0
    :goto_0
    return-void

    .line 544856
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->D:LX/04g;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    if-ne v0, v1, :cond_0

    .line 544857
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_0
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 544851
    const v0, 0x7f030983

    return v0
.end method

.method public getStubLayout()I
    .locals 1

    .prologue
    .line 544850
    const v0, 0x7f030984

    return v0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 0

    .prologue
    .line 544849
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 544799
    const v0, 0x7f0d0553

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->v:Landroid/view/View;

    .line 544800
    const v0, 0x7f0d1851

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/commercialbreak/plugins/CommercialBreakVideoContainerFrameLayout;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->w:Lcom/facebook/video/commercialbreak/plugins/CommercialBreakVideoContainerFrameLayout;

    .line 544801
    const v0, 0x7f0d1852

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->x:Landroid/widget/TextView;

    .line 544802
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->x:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->C:Z

    if-eqz v0, :cond_5

    const v0, 0x7f080dde

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 544803
    const v0, 0x7f0d1850

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    .line 544804
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 544805
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v0, v1

    .line 544806
    :goto_1
    new-instance v1, LX/7LN;

    invoke-direct {v1}, LX/7LN;-><init>()V

    .line 544807
    iput-object v0, v1, LX/7LN;->a:LX/04D;

    .line 544808
    move-object v0, v1

    .line 544809
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 544810
    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 544811
    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 544812
    sget-object v1, LX/3H0;->NONE:LX/3H0;

    .line 544813
    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->B:LX/D6v;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->B:LX/D6v;

    .line 544814
    iget-object p1, v2, LX/D6v;->w:LX/3H0;

    move-object v2, p1

    .line 544815
    if-eqz v2, :cond_0

    .line 544816
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->B:LX/D6v;

    .line 544817
    iget-object v2, v1, LX/D6v;->w:LX/3H0;

    move-object v1, v2

    .line 544818
    :cond_0
    iget-object v2, p0, LX/2oy;->j:LX/2pb;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/2oy;->j:LX/2pb;

    .line 544819
    iget-object p1, v2, LX/2pb;->D:LX/04G;

    move-object v2, p1

    .line 544820
    :goto_2
    sget-object p1, LX/D6z;->b:[I

    invoke-virtual {v1}, LX/3H0;->ordinal()I

    move-result v1

    aget v1, p1, v1

    packed-switch v1, :pswitch_data_0

    .line 544821
    :cond_1
    :goto_3
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->p:LX/0iY;

    invoke-virtual {v1}, LX/0iY;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/04G;->INLINE_PLAYER:LX/04G;

    if-ne v2, v1, :cond_2

    .line 544822
    new-instance v1, LX/Bx2;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Bx2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 544823
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->q:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 544824
    new-instance v1, LX/7MZ;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/7MZ;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 544825
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 544826
    invoke-virtual {v0, v1}, LX/7LN;->a(Ljava/util/List;)LX/7LN;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v1}, LX/7LN;->a(Lcom/facebook/video/player/RichVideoPlayer;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    .line 544827
    new-instance v0, LX/D70;

    invoke-direct {v0, p0}, LX/D70;-><init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->z:LX/D70;

    .line 544828
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->z:LX/D70;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    .line 544829
    iget-boolean v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->C:Z

    if-nez v0, :cond_4

    .line 544830
    new-instance v0, LX/D71;

    invoke-direct {v0, p0}, LX/D71;-><init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->u:LX/D71;

    .line 544831
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->u:LX/D71;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    .line 544832
    :cond_4
    return-void

    .line 544833
    :cond_5
    const v0, 0x7f080ddf

    goto/16 :goto_0

    .line 544834
    :cond_6
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    goto/16 :goto_1

    .line 544835
    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    .line 544836
    :pswitch_0
    new-instance v1, LX/D7C;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, LX/D7C;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->A:LX/D7C;

    .line 544837
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->A:LX/D7C;

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 544838
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-ne v2, v1, :cond_1

    .line 544839
    new-instance v1, LX/D79;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, LX/D79;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_3

    .line 544840
    :pswitch_1
    new-instance v1, LX/D78;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, LX/D78;-><init>(Landroid/content/Context;)V

    .line 544841
    sget-object p1, LX/3H0;->NONLIVE_POST_ROLL:LX/3H0;

    .line 544842
    iput-object p1, v1, LX/D78;->b:LX/3H0;

    .line 544843
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 544844
    sget-object v1, LX/04G;->CHANNEL_PLAYER:LX/04G;

    if-ne v2, v1, :cond_1

    .line 544845
    new-instance v1, LX/D7F;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, LX/D7F;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_3

    .line 544846
    :pswitch_2
    new-instance v1, LX/D78;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, LX/D78;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 544847
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-ne v2, v1, :cond_1

    .line 544848
    new-instance v1, LX/D7G;

    invoke-virtual {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, LX/D7G;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 544791
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 544792
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 544793
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_1

    .line 544794
    :cond_0
    :goto_0
    return-void

    .line 544795
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v0

    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    .line 544796
    :goto_1
    if-eqz v0, :cond_0

    .line 544797
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto :goto_0

    .line 544798
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
