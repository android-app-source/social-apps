.class public Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Ljava/lang/String;

.field public static final c:Lcom/facebook/common/callercontext/CallerContext;

.field public static final d:LX/3H0;

.field private static volatile u:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;


# instance fields
.field public final a:LX/3Fh;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final e:LX/3H1;

.field private final f:LX/3H3;

.field public final g:LX/2ml;

.field private final h:Ljava/util/concurrent/Executor;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0kL;

.field public final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/D6o;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker$InfoChangedListener;",
            ">;>;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker$ViewerCountListener;",
            ">;>;"
        }
    .end annotation
.end field

.field public final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker$SupportMessageListener;",
            ">;>;"
        }
    .end annotation
.end field

.field public final o:LX/3H4;

.field public final p:LX/1JD;

.field public final q:LX/3H5;

.field private final r:LX/3H6;

.field private final s:LX/0So;

.field public final t:LX/04J;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 542606
    const-class v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->b:Ljava/lang/String;

    .line 542607
    const-class v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 542608
    sget-object v0, LX/3H0;->LIVE:LX/3H0;

    sput-object v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d:LX/3H0;

    return-void
.end method

.method public constructor <init>(LX/3H1;LX/3H3;LX/2ml;Ljava/util/concurrent/Executor;LX/0Or;LX/0kL;LX/3H4;LX/1JD;LX/3H5;LX/3H6;LX/0So;LX/04J;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/video/commercialbreak/annotations/IsCommercialBreakDebugToastsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3H1;",
            "LX/3H3;",
            "LX/2ml;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0kL;",
            "LX/3H4;",
            "LX/1JD;",
            "LX/3H5;",
            "LX/3H6;",
            "LX/0So;",
            "LX/04J;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 542587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 542588
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    .line 542589
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->l:Ljava/util/Map;

    .line 542590
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->m:Ljava/util/Map;

    .line 542591
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->n:Ljava/util/Map;

    .line 542592
    new-instance v0, LX/3Fh;

    invoke-direct {v0, p0}, LX/3Fh;-><init>(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;)V

    iput-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a:LX/3Fh;

    .line 542593
    iput-object p1, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e:LX/3H1;

    .line 542594
    iput-object p2, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->f:LX/3H3;

    .line 542595
    iput-object p3, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g:LX/2ml;

    .line 542596
    iput-object p4, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->h:Ljava/util/concurrent/Executor;

    .line 542597
    iput-object p5, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->i:LX/0Or;

    .line 542598
    iput-object p6, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->j:LX/0kL;

    .line 542599
    iput-object p7, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->o:LX/3H4;

    .line 542600
    iput-object p8, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->p:LX/1JD;

    .line 542601
    iput-object p9, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->q:LX/3H5;

    .line 542602
    iput-object p10, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->r:LX/3H6;

    .line 542603
    iput-object p11, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->s:LX/0So;

    .line 542604
    iput-object p12, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->t:LX/04J;

    .line 542605
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .locals 3

    .prologue
    .line 542577
    sget-object v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->u:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    if-nez v0, :cond_1

    .line 542578
    const-class v1, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    monitor-enter v1

    .line 542579
    :try_start_0
    sget-object v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->u:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 542580
    if-eqz v2, :cond_0

    .line 542581
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->b(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->u:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 542582
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 542583
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 542584
    :cond_1
    sget-object v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->u:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    return-object v0

    .line 542585
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 542586
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;LX/D6o;IILjava/lang/String;I)V
    .locals 9

    .prologue
    .line 542554
    iget-object v0, p2, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542555
    iget-object v0, p2, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6p;

    move-object v8, v0

    .line 542556
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->r:LX/3H6;

    .line 542557
    iget-object v1, v0, LX/3H6;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 542558
    if-eqz v0, :cond_1

    .line 542559
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->r:LX/3H6;

    .line 542560
    iget-object v1, v0, LX/3H6;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 542561
    iput-object v0, v8, LX/D6p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 542562
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->r:LX/3H6;

    .line 542563
    iget-object v1, v0, LX/3H6;->e:Ljava/lang/String;

    const-string v2, "-1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, LX/D6r;->FAILED:LX/D6r;

    :goto_2
    move-object v0, v1

    .line 542564
    iput-object v0, v8, LX/D6p;->b:LX/D6r;

    .line 542565
    :goto_3
    return-void

    .line 542566
    :cond_0
    new-instance v0, LX/D6p;

    invoke-direct {v0, p0}, LX/D6p;-><init>(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;)V

    .line 542567
    iget-object v1, p2, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v8, v0

    goto :goto_0

    .line 542568
    :cond_1
    sget-object v0, LX/D6r;->FETCHING:LX/D6r;

    iput-object v0, v8, LX/D6p;->b:LX/D6r;

    .line 542569
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, v8, LX/D6p;->c:J

    .line 542570
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->f:LX/3H3;

    iget v4, p2, LX/D6o;->e:F

    iget-object v1, p2, LX/D6o;->a:LX/D6v;

    invoke-virtual {v1}, LX/D6v;->d()LX/04D;

    move-result-object v7

    move-object v1, p1

    move v2, p3

    move v3, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, LX/3H3;->a(Ljava/lang/String;IIFLjava/lang/String;ILX/04D;)LX/1Zp;

    move-result-object v2

    .line 542571
    :try_start_0
    invoke-static {p5}, LX/3H0;->valueOf(Ljava/lang/String;)LX/3H0;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 542572
    :goto_4
    move-object v6, v0

    .line 542573
    const-string v0, "Kicking off video ad fetch"

    invoke-static {p0, v0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->j(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)V

    .line 542574
    new-instance v0, LX/D6m;

    move-object v1, p0

    move-object v3, v8

    move-object v4, p1

    move v5, p6

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/D6m;-><init>(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;LX/1Zp;LX/D6p;Ljava/lang/String;ILX/3H0;LX/D6o;)V

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->h:Ljava/util/concurrent/Executor;

    invoke-static {v2, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_3

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    sget-object v1, LX/D6r;->SUCCESS:LX/D6r;

    goto :goto_2

    .line 542575
    :catch_0
    sget-object v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->b:Ljava/lang/String;

    const-string v1, "Do not recognize instream %s ad string. Defaulting to %s "

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d:LX/3H0;

    invoke-virtual {v5}, LX/3H0;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 542576
    sget-object v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d:LX/3H0;

    goto :goto_4
.end method

.method private static b(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .locals 13

    .prologue
    .line 542546
    new-instance v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    .line 542547
    new-instance v3, LX/3H1;

    const-class v1, LX/3H2;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/3H2;

    invoke-static {p0}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object v2

    check-cast v2, LX/2ml;

    invoke-direct {v3, v1, v2}, LX/3H1;-><init>(LX/3H2;LX/2ml;)V

    .line 542548
    move-object v1, v3

    .line 542549
    check-cast v1, LX/3H1;

    .line 542550
    new-instance v4, LX/3H3;

    invoke-static {p0}, LX/0w9;->a(LX/0QB;)LX/0w9;

    move-result-object v2

    check-cast v2, LX/0w9;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {v4, v2, v3}, LX/3H3;-><init>(LX/0w9;LX/0tX;)V

    .line 542551
    move-object v2, v4

    .line 542552
    check-cast v2, LX/3H3;

    invoke-static {p0}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object v3

    check-cast v3, LX/2ml;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    const/16 v5, 0x1591

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {p0}, LX/3H4;->a(LX/0QB;)LX/3H4;

    move-result-object v7

    check-cast v7, LX/3H4;

    const-class v8, LX/1JD;

    invoke-interface {p0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1JD;

    const-class v9, LX/3H5;

    invoke-interface {p0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/3H5;

    invoke-static {p0}, LX/3H6;->a(LX/0QB;)LX/3H6;

    move-result-object v10

    check-cast v10, LX/3H6;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v11

    check-cast v11, LX/0So;

    invoke-static {p0}, LX/04J;->a(LX/0QB;)LX/04J;

    move-result-object v12

    check-cast v12, LX/04J;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;-><init>(LX/3H1;LX/3H3;LX/2ml;Ljava/util/concurrent/Executor;LX/0Or;LX/0kL;LX/3H4;LX/1JD;LX/3H5;LX/3H6;LX/0So;LX/04J;)V

    .line 542553
    return-object v0
.end method

.method public static i(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)LX/D6o;
    .locals 2

    .prologue
    .line 542541
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542542
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6o;

    .line 542543
    :goto_0
    return-object v0

    .line 542544
    :cond_0
    new-instance v0, LX/D6o;

    invoke-direct {v0, p0, p1}, LX/D6o;-><init>(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)V

    .line 542545
    iget-object v1, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static j(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 542538
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542539
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->j:LX/0kL;

    new-instance v1, LX/27k;

    invoke-direct {v1, p1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 542540
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 542482
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6o;

    .line 542483
    if-nez v0, :cond_0

    move-object v0, v1

    .line 542484
    :goto_0
    return-object v0

    .line 542485
    :cond_0
    iget-object v2, v0, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 542486
    iget-object v0, v0, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6p;

    iget-object v0, v0, LX/D6p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 542487
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;F)V
    .locals 1

    .prologue
    .line 542535
    invoke-static {p0, p1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->i(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)LX/D6o;

    move-result-object v0

    .line 542536
    iput p2, v0, LX/D6o;->e:F

    .line 542537
    return-void
.end method

.method public final a(Ljava/lang/String;IILjava/lang/String;I)V
    .locals 7

    .prologue
    .line 542531
    invoke-static {p0, p1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->i(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)LX/D6o;

    move-result-object v2

    .line 542532
    add-int/lit16 v0, p2, 0xfa0

    int-to-long v0, v0

    iput-wide v0, v2, LX/D6o;->c:J

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    .line 542533
    invoke-static/range {v0 .. v6}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;LX/D6o;IILjava/lang/String;I)V

    .line 542534
    return-void
.end method

.method public final b(Ljava/lang/String;I)LX/D6r;
    .locals 3

    .prologue
    .line 542524
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6o;

    .line 542525
    if-nez v0, :cond_0

    .line 542526
    sget-object v0, LX/D6r;->IDLE:LX/D6r;

    .line 542527
    :goto_0
    return-object v0

    .line 542528
    :cond_0
    iget-object v1, v0, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 542529
    iget-object v0, v0, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6p;

    iget-object v0, v0, LX/D6p;->b:LX/D6r;

    goto :goto_0

    .line 542530
    :cond_1
    sget-object v0, LX/D6r;->IDLE:LX/D6r;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 542522
    invoke-virtual {p0, p1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v0

    iget v0, v0, LX/D6v;->g:I

    .line 542523
    invoke-virtual {p0, p1, v0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Ljava/lang/String;I)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 542518
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6o;

    .line 542519
    if-nez v0, :cond_0

    .line 542520
    const-wide/16 v0, -0x1

    .line 542521
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, v0, LX/D6o;->c:J

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 542511
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6o;

    .line 542512
    const-wide/16 v1, -0x1

    .line 542513
    iput-wide v1, v0, LX/D6o;->c:J

    .line 542514
    iput-wide v1, v0, LX/D6o;->d:J

    .line 542515
    iget-object v1, v0, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 542516
    iget-object v1, v0, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, LX/D6p;

    iget-object v4, v0, LX/D6o;->f:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-direct {v3, v4}, LX/D6p;-><init>(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542517
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 542507
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6o;

    .line 542508
    if-nez v0, :cond_0

    .line 542509
    const-wide/16 v0, -0x1

    .line 542510
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, v0, LX/D6o;->d:J

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;I)Z
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 542494
    invoke-virtual {p0, p1, p2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->b(Ljava/lang/String;I)LX/D6r;

    move-result-object v2

    .line 542495
    sget-object v3, LX/D6r;->FETCHING:LX/D6r;

    if-ne v2, v3, :cond_1

    .line 542496
    :cond_0
    :goto_0
    return v0

    .line 542497
    :cond_1
    sget-object v3, LX/D6r;->FAILED:LX/D6r;

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 542498
    goto :goto_0

    .line 542499
    :cond_2
    const-wide/16 v8, -0x1

    .line 542500
    iget-object v6, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/D6o;

    .line 542501
    if-nez v6, :cond_4

    move-wide v6, v8

    .line 542502
    :goto_1
    move-wide v2, v6

    .line 542503
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->s:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    sub-long v2, v4, v2

    const-wide/16 v4, 0x4e20

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 542504
    :cond_4
    iget-object v7, v6, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 542505
    iget-object v6, v6, LX/D6o;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/D6p;

    iget-wide v6, v6, LX/D6p;->c:J

    goto :goto_1

    :cond_5
    move-wide v6, v8

    .line 542506
    goto :goto_1
.end method

.method public final e(Ljava/lang/String;)LX/D6v;
    .locals 1

    .prologue
    .line 542492
    invoke-static {p0, p1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->i(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)LX/D6o;

    move-result-object v0

    .line 542493
    iget-object v0, v0, LX/D6o;->a:LX/D6v;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)LX/2oN;
    .locals 1

    .prologue
    .line 542488
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6o;

    .line 542489
    if-nez v0, :cond_0

    .line 542490
    sget-object v0, LX/2oN;->NONE:LX/2oN;

    .line 542491
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, LX/D6o;->a:LX/D6v;

    iget-object v0, v0, LX/D6v;->a:LX/2oN;

    goto :goto_0
.end method
