.class public Lcom/facebook/facecastdisplay/LiveVideoStatusView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3HT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

.field private final d:Landroid/widget/TextView;

.field public final e:Landroid/animation/ObjectAnimator;

.field private final f:Lcom/facebook/widget/text/BetterTextView;

.field private final g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field private final h:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field private final i:Ljava/lang/Runnable;

.field private final j:Landroid/widget/TextView;

.field private k:Z

.field public l:I

.field private m:Z

.field public n:I

.field private o:J

.field private p:LX/3HY;

.field private q:LX/3Hg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 544527
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 544528
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 544529
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 544530
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 544531
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 544532
    sget-object v0, LX/3HY;->REGULAR:LX/3HY;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->p:LX/3HY;

    .line 544533
    sget-object v0, LX/3Hg;->LIVE:LX/3Hg;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->q:LX/3Hg;

    .line 544534
    const-class v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 544535
    const v0, 0x7f030a2b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 544536
    const v0, 0x7f0d19bb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    .line 544537
    const v0, 0x7f0d19c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->d:Landroid/widget/TextView;

    .line 544538
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 544539
    const/16 v2, 0xff

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 544540
    new-array v2, v4, [Landroid/animation/PropertyValuesHolder;

    const-string v3, "alpha"

    new-array v4, v4, [I

    const/16 v5, 0xb3

    aput v5, v4, v6

    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 544541
    const-wide/16 v4, 0x2bc

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 544542
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 544543
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 544544
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 544545
    move-object v0, v2

    .line 544546
    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e:Landroid/animation/ObjectAnimator;

    .line 544547
    const v0, 0x7f0d19bd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 544548
    const v0, 0x7f0d19be

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 544549
    const v0, 0x7f0d19c0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->h:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 544550
    const v0, 0x7f0d19bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->j:Landroid/widget/TextView;

    .line 544551
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    new-instance v1, LX/3Hi;

    invoke-direct {v1, p0}, LX/3Hi;-><init>(Lcom/facebook/facecastdisplay/LiveVideoStatusView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 544552
    new-instance v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView$2;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView$2;-><init>(Lcom/facebook/facecastdisplay/LiveVideoStatusView;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->i:Ljava/lang/Runnable;

    .line 544553
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    new-instance v1, LX/3Hj;

    invoke-direct {v1, p0}, LX/3Hj;-><init>(Lcom/facebook/facecastdisplay/LiveVideoStatusView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 544554
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->m:Z

    .line 544555
    return-void
.end method

.method private a(IIIIII)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 544556
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 544557
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 544558
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setTextSize(IF)V

    .line 544559
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 544560
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    invoke-static {v1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(Landroid/view/View;I)V

    .line 544561
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-static {v1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(Landroid/view/View;I)V

    .line 544562
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 544563
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-static {v1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->b(Landroid/view/View;I)V

    .line 544564
    div-int/lit8 v0, v0, 0x2

    .line 544565
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->j:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->b(Landroid/view/View;I)V

    .line 544566
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    invoke-static {v1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->b(Landroid/view/View;I)V

    .line 544567
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->b(Landroid/view/View;I)V

    .line 544568
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 544569
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setPadding(IIII)V

    .line 544570
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 544571
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 544572
    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 544573
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p6}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 544574
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 544575
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 544576
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 544577
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 544578
    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/LiveVideoStatusView;LX/154;LX/3HT;)V
    .locals 0

    .prologue
    .line 544579
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a:LX/154;

    iput-object p2, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->b:LX/3HT;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-static {v1}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v0

    check-cast v0, LX/154;

    invoke-static {v1}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object v1

    check-cast v1, LX/3HT;

    invoke-static {p0, v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(Lcom/facebook/facecastdisplay/LiveVideoStatusView;LX/154;LX/3HT;)V

    return-void
.end method

.method private static b(Landroid/view/View;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 544587
    invoke-virtual {p0, p1, v0, p1, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 544588
    return-void
.end method

.method private b(Z)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x12c

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 544589
    sget-object v0, LX/Ace;->a:[I

    iget-object v3, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->q:LX/3Hg;

    invoke-virtual {v3}, LX/3Hg;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 544590
    :cond_0
    :goto_0
    return-void

    .line 544591
    :pswitch_0
    iget-object v3, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->k:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->setVisibility(I)V

    .line 544592
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 544593
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->h:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 544594
    iget v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->l:I

    if-lez v0, :cond_0

    .line 544595
    invoke-direct {p0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCountVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 544596
    goto :goto_1

    .line 544597
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->setVisibility(I)V

    .line 544598
    invoke-direct {p0, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCountVisibility(I)V

    .line 544599
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080c2f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 544600
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 544601
    iget v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->l:I

    if-lez v0, :cond_0

    .line 544602
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->h:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 544603
    if-eqz p1, :cond_0

    .line 544604
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->h:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setAlpha(F)V

    .line 544605
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->h:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 544606
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 544607
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 544608
    :pswitch_2
    invoke-direct {p0, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCountVisibility(I)V

    goto :goto_0

    .line 544609
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->setVisibility(I)V

    .line 544610
    invoke-direct {p0, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCountVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private e()V
    .locals 2

    .prologue
    .line 544583
    iget-wide v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->o:J

    invoke-static {v0, v1}, LX/3Hk;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 544584
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 544585
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 544586
    :cond_0
    return-void
.end method

.method private setViewerCountVisibility(I)V
    .locals 1

    .prologue
    .line 544580
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->m:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 544581
    :goto_0
    return-void

    .line 544582
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 544518
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->l:I

    .line 544519
    iget v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->l:I

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCount(I)V

    .line 544520
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 544521
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e()V

    .line 544522
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a(Z)V

    .line 544523
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 544524
    if-eqz p1, :cond_0

    .line 544525
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/widget/text/BetterTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 544526
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 544471
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a(Z)V

    .line 544472
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 544473
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 544474
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 544475
    return-void
.end method

.method public getCurrentViewCount()I
    .locals 1

    .prologue
    .line 544476
    iget v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->l:I

    return v0
.end method

.method public getLiveAnimator()Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 544477
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method public getMaxViewersDisplayed()I
    .locals 1

    .prologue
    .line 544478
    iget v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->n:I

    return v0
.end method

.method public setIndicatorType(LX/3Hg;)V
    .locals 1

    .prologue
    .line 544479
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->q:LX/3Hg;

    .line 544480
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->b(Z)V

    .line 544481
    return-void
.end method

.method public setIsAudioLive(Z)V
    .locals 3

    .prologue
    .line 544482
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_0

    const v0, 0x7f02076c

    :goto_0
    invoke-static {v2, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 544483
    return-void

    .line 544484
    :cond_0
    const v0, 0x7f020863

    goto :goto_0
.end method

.method public setIsLiveNow(Z)V
    .locals 1

    .prologue
    .line 544485
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->k:Z

    .line 544486
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->b(Z)V

    .line 544487
    return-void
.end method

.method public setLiveIndicatorClickable(Z)V
    .locals 1

    .prologue
    .line 544488
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c:Lcom/facebook/facecastdisplay/AnimatableLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->setClickable(Z)V

    .line 544489
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setClickable(Z)V

    .line 544490
    return-void
.end method

.method public setShowViewerCount(Z)V
    .locals 1

    .prologue
    .line 544491
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->m:Z

    if-ne v0, p1, :cond_0

    .line 544492
    :goto_0
    return-void

    .line 544493
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->m:Z

    .line 544494
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->m:Z

    if-eqz v0, :cond_1

    .line 544495
    iget v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->l:I

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCount(I)V

    goto :goto_0

    .line 544496
    :cond_1
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCountVisibility(I)V

    goto :goto_0
.end method

.method public setTimeElapsed(J)V
    .locals 1

    .prologue
    .line 544497
    iput-wide p1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->o:J

    .line 544498
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 544499
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e()V

    .line 544500
    :cond_0
    return-void
.end method

.method public setVideoPlayerViewSize(LX/3HY;)V
    .locals 7

    .prologue
    .line 544501
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->p:LX/3HY;

    if-ne v0, p1, :cond_0

    .line 544502
    :goto_0
    return-void

    .line 544503
    :cond_0
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->p:LX/3HY;

    .line 544504
    sget-object v0, LX/Ace;->b:[I

    invoke-virtual {p1}, LX/3HY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 544505
    :pswitch_0
    const v1, 0x7f0b04e4

    const v2, 0x7f0b04e7

    const v3, 0x7f0b04ea

    const v4, 0x7f0b04f1

    const v5, 0x7f0b04ee

    const v6, 0x7f020864

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(IIIIII)V

    goto :goto_0

    .line 544506
    :pswitch_1
    const v1, 0x7f0b04e3

    const v2, 0x7f0b04e6

    const v3, 0x7f0b04e9

    const v4, 0x7f0b04f0

    const v5, 0x7f0b04ed

    const v6, 0x7f020863

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(IIIIII)V

    goto :goto_0

    .line 544507
    :pswitch_2
    const v1, 0x7f0b04e5

    const v2, 0x7f0b04e8

    const v3, 0x7f0b04eb

    const v4, 0x7f0b04f2

    const v5, 0x7f0b04ef

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(IIIIII)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setViewerCount(I)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 544508
    iput p1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->l:I

    .line 544509
    iget v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->l:I

    iget v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->n:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->n:I

    .line 544510
    if-lez p1, :cond_0

    .line 544511
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a:LX/154;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/154;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 544512
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 544513
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->h:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 544514
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->b(Z)V

    .line 544515
    :goto_0
    return-void

    .line 544516
    :cond_0
    invoke-direct {p0, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCountVisibility(I)V

    .line 544517
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->h:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    goto :goto_0
.end method
