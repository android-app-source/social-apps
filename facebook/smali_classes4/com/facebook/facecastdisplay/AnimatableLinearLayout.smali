.class public Lcom/facebook/facecastdisplay/AnimatableLinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Landroid/animation/ValueAnimator;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 544656
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 544657
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 544654
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 544655
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 544648
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 544649
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    .line 544650
    iget-object v0, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 544651
    iget-object v0, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 544652
    iget-object v0, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    new-instance v1, LX/3Hh;

    invoke-direct {v1, p0}, LX/3Hh;-><init>(Lcom/facebook/facecastdisplay/AnimatableLinearLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 544653
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 544631
    invoke-virtual {p0, v4}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 544632
    iget-object v0, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->b:Z

    if-ne p1, v0, :cond_1

    .line 544633
    :cond_0
    :goto_0
    return-void

    .line 544634
    :cond_1
    if-eqz p1, :cond_2

    move v0, v1

    .line 544635
    :goto_1
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eq v3, v0, :cond_0

    .line 544636
    iget-object v3, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 544637
    iget-object v3, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 544638
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 544639
    if-eqz p1, :cond_3

    .line 544640
    iget-object v1, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    new-array v3, v5, [F

    fill-array-data v3, :array_0

    invoke-virtual {v1, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 544641
    iput-boolean v4, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->b:Z

    .line 544642
    :goto_2
    iget-object v1, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    new-instance v3, LX/Abo;

    invoke-direct {v3, p0, v2, v0}, LX/Abo;-><init>(Lcom/facebook/facecastdisplay/AnimatableLinearLayout;Landroid/view/View;I)V

    invoke-virtual {v1, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 544643
    iget-object v0, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 544644
    :cond_2
    const/16 v0, 0x8

    goto :goto_1

    .line 544645
    :cond_3
    iget-object v3, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    new-array v4, v5, [F

    fill-array-data v4, :array_1

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 544646
    iput-boolean v1, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->b:Z

    goto :goto_2

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 544647
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public getIsAnimatingToVisible()Z
    .locals 1

    .prologue
    .line 544630
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->b:Z

    return v0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 544619
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 544620
    iget-object v0, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 544621
    :goto_0
    return-void

    .line 544622
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 544623
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 544624
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 544625
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 544626
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v2

    .line 544627
    add-int/2addr v0, v1

    .line 544628
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, v0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v2

    sub-float v0, v1, v0

    float-to-int v0, v0

    .line 544629
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/facecastdisplay/AnimatableLinearLayout;->setMeasuredDimension(II)V

    goto :goto_0
.end method
