.class public Lcom/facebook/facecastdisplay/LiveEventsPlugin;
.super LX/2oy;
.source ""

# interfaces
.implements LX/3Gs;
.implements LX/3Gt;
.implements LX/3Gu;


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field private A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:I

.field public C:Z

.field public D:Z

.field public E:Z

.field private F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:LX/AcK;

.field public a:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Aby;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Ae5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Agt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/BSE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/37Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final o:LX/AcP;

.field private final p:Landroid/app/Activity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:I

.field private final r:LX/7M2;

.field public s:LX/Aca;

.field private t:LX/Aca;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:LX/Aca;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:LX/Aca;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Z

.field public x:Z

.field private y:Z

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 542429
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 542430
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 542126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 542127
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 542411
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 542412
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-static {v0}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v4

    check-cast v4, LX/Ac6;

    invoke-static {v0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v5

    check-cast v5, LX/1b4;

    invoke-static {v0}, LX/Aby;->b(LX/0QB;)LX/Aby;

    move-result-object v6

    check-cast v6, LX/Aby;

    new-instance v9, LX/Ae5;

    invoke-static {v0}, LX/Ae2;->b(LX/0QB;)LX/Ae2;

    move-result-object v7

    check-cast v7, LX/Ae2;

    invoke-static {v0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object v8

    check-cast v8, LX/3HT;

    invoke-direct {v9, v7, v8}, LX/Ae5;-><init>(LX/Ae2;LX/3HT;)V

    move-object v7, v9

    check-cast v7, LX/Ae5;

    new-instance v8, LX/Agt;

    const/16 v9, 0x1c20

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct {v8, v9}, LX/Agt;-><init>(LX/0Ot;)V

    move-object v8, v8

    check-cast v8, LX/Agt;

    invoke-static {v0}, LX/BSE;->a(LX/0QB;)LX/BSE;

    move-result-object v9

    check-cast v9, LX/BSE;

    invoke-static {v0}, LX/37Y;->a(LX/0QB;)LX/37Y;

    move-result-object v0

    check-cast v0, LX/37Y;

    iput-object v4, v2, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a:LX/Ac6;

    iput-object v5, v2, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    iput-object v6, v2, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    iput-object v7, v2, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    iput-object v8, v2, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->e:LX/Agt;

    iput-object v9, v2, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->f:LX/BSE;

    iput-object v0, v2, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->n:LX/37Y;

    .line 542413
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 542414
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 542415
    invoke-virtual {p0, v0, v3, v3}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->addView(Landroid/view/View;II)V

    .line 542416
    new-instance v0, LX/AcP;

    invoke-direct {v0, p0}, LX/AcP;-><init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->o:LX/AcP;

    .line 542417
    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->p:Landroid/app/Activity;

    .line 542418
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->LiveEventsTickerView:[I

    invoke-virtual {v0, p2, v1, p3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 542419
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0399

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->q:I

    .line 542420
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 542421
    new-instance v0, LX/7M2;

    sget-object v1, LX/7M1;->TOGGLE:LX/7M1;

    invoke-direct {v0, v1}, LX/7M2;-><init>(LX/7M1;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->r:LX/7M2;

    .line 542422
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/AcO;

    invoke-direct {v1, p0}, LX/AcO;-><init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542423
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/AcM;

    invoke-direct {v1, p0}, LX/AcM;-><init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542424
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/AcN;

    invoke-direct {v1, p0}, LX/AcN;-><init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542425
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/AcL;

    invoke-direct {v1, p0}, LX/AcL;-><init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542426
    new-instance v0, LX/AcK;

    invoke-direct {v0, p0}, LX/AcK;-><init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->G:LX/AcK;

    .line 542427
    return-void

    .line 542428
    :cond_0
    const/16 v0, 0x9

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    goto :goto_0
.end method

.method private a(ZI)LX/Aca;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 542387
    if-eqz p1, :cond_2

    .line 542388
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->u:LX/Aca;

    if-nez v0, :cond_1

    .line 542389
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 542390
    const v2, 0x7f0e038e

    move v2, v2

    .line 542391
    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 542392
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->H()LX/6Ri;

    move-result-object v1

    .line 542393
    iget-boolean v2, v1, LX/6Ri;->e:Z

    move v1, v2

    .line 542394
    if-eqz v1, :cond_0

    .line 542395
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0e0391

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 542396
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0e0392

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 542397
    :cond_0
    new-instance v1, LX/Aca;

    invoke-direct {v1, v0}, LX/Aca;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->u:LX/Aca;

    .line 542398
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->u:LX/Aca;

    .line 542399
    :goto_0
    return-object v0

    .line 542400
    :cond_2
    invoke-direct {p0, p2}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 542401
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->v:LX/Aca;

    if-nez v0, :cond_3

    .line 542402
    new-instance v0, LX/Aca;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 542403
    const v3, 0x7f0e038f

    move v3, v3

    .line 542404
    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, LX/Aca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->v:LX/Aca;

    .line 542405
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->v:LX/Aca;

    goto :goto_0

    .line 542406
    :cond_4
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->t:LX/Aca;

    if-nez v0, :cond_5

    .line 542407
    new-instance v0, LX/Aca;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 542408
    const v3, 0x7f0e038d

    move v3, v3

    .line 542409
    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, LX/Aca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->t:LX/Aca;

    .line 542410
    :cond_5
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->t:LX/Aca;

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLMedia;)V
    .locals 4

    .prologue
    .line 542358
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->bQ()Z

    move-result v0

    if-nez v0, :cond_1

    .line 542359
    :cond_0
    :goto_0
    return-void

    .line 542360
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    .line 542361
    iget v1, v0, LX/Aby;->z:I

    move v0, v1

    .line 542362
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 542363
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->f:LX/BSE;

    invoke-virtual {v0}, LX/BSE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542364
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    .line 542365
    iget v2, v1, LX/Aby;->z:I

    move v1, v2

    .line 542366
    invoke-virtual {v0, v1}, LX/Aby;->c(I)V

    .line 542367
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    .line 542368
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 542369
    check-cast v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->getVisibility()I

    move-result v1

    move v0, v1

    .line 542370
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, LX/Ae5;->a(I)V

    .line 542371
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->f:LX/BSE;

    new-instance v2, Lcom/facebook/facecastdisplay/LiveEventsPlugin$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin$1;-><init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;I)V

    .line 542372
    invoke-virtual {v1}, LX/BSE;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 542373
    :cond_2
    :goto_1
    goto :goto_0

    .line 542374
    :cond_3
    iget-object v0, v1, LX/BSE;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 542375
    iget-object v3, v1, LX/BSE;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BSF;

    invoke-virtual {v3}, LX/BSF;->a()Z

    move-result v3

    if-nez v3, :cond_5

    .line 542376
    new-instance v3, LX/BSG;

    invoke-direct {v3, v0}, LX/BSG;-><init>(Landroid/content/Context;)V

    iput-object v3, v1, LX/BSE;->d:Landroid/app/Dialog;

    .line 542377
    iget-object v0, v1, LX/BSE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BSF;

    .line 542378
    iget-object v3, v0, LX/BSF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object p0, LX/BSF;->a:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v3, p0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 542379
    :cond_4
    :goto_2
    iget-object v0, v1, LX/BSE;->d:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    .line 542380
    iget-object v0, v1, LX/BSE;->d:Landroid/app/Dialog;

    new-instance v3, LX/BSD;

    invoke-direct {v3, v1, v2}, LX/BSD;-><init>(LX/BSE;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 542381
    iget-object v0, v1, LX/BSE;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_1

    .line 542382
    :cond_5
    iget-object v3, v1, LX/BSE;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BSF;

    invoke-virtual {v3}, LX/BSF;->c()Z

    move-result v3

    if-nez v3, :cond_4

    .line 542383
    new-instance v3, LX/BSK;

    invoke-direct {v3, v0}, LX/BSK;-><init>(Landroid/content/Context;)V

    iput-object v3, v1, LX/BSE;->d:Landroid/app/Dialog;

    .line 542384
    iget-object v0, v1, LX/BSE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BSF;

    .line 542385
    iget-object v3, v0, LX/BSF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object p0, LX/BSF;->b:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v3, p0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 542386
    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/facecastdisplay/LiveEventsPlugin;Z)V
    .locals 3

    .prologue
    const/16 v0, 0x8

    .line 542351
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 542352
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v1, v1, LX/Aca;->e:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 542353
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542354
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    invoke-virtual {v0, p1}, LX/Ae5;->a(Z)V

    .line 542355
    :cond_0
    :goto_0
    return-void

    .line 542356
    :cond_1
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    invoke-virtual {v1, p1}, LX/Ae5;->a(Z)V

    .line 542357
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v1, v1, LX/Aca;->e:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static c(Lcom/facebook/facecastdisplay/LiveEventsPlugin;I)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 542328
    iget-object v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a:LX/Ac6;

    invoke-virtual {v2}, LX/Ac6;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 542329
    iget-boolean v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->C:Z

    invoke-direct {p0, v2, p1}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a(ZI)LX/Aca;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    .line 542330
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->j()V

    .line 542331
    :cond_0
    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    .line 542332
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a:LX/Ac6;

    invoke-virtual {v1}, LX/Ac6;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 542333
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v3}, LX/Aby;->b(Z)V

    .line 542334
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v3}, LX/Aby;->c(Z)V

    .line 542335
    :goto_0
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_1

    .line 542336
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2pF;

    invoke-direct {v1, v4, v5}, LX/2pF;-><init>(D)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 542337
    :cond_1
    :goto_1
    return-void

    .line 542338
    :cond_2
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v1, v1, LX/Aca;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 542339
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v1, v0}, LX/Aby;->b(I)V

    .line 542340
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    invoke-virtual {v1, v0}, LX/Ae5;->a(I)V

    goto :goto_0

    .line 542341
    :cond_3
    iget-object v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v2, v2, LX/Aca;->b:Landroid/view/View;

    iget-boolean v3, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->C:Z

    if-eqz v3, :cond_4

    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 542342
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    iget-boolean v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->C:Z

    invoke-virtual {v0, v2}, LX/Aby;->b(Z)V

    .line 542343
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v1}, LX/Aby;->c(Z)V

    .line 542344
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v1}, LX/Aby;->b(I)V

    .line 542345
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    invoke-virtual {v0, v1}, LX/Ae5;->a(I)V

    .line 542346
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_1

    .line 542347
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->C:Z

    if-eqz v0, :cond_5

    .line 542348
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2pF;

    invoke-direct {v1, v4, v5}, LX/2pF;-><init>(D)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_1

    :cond_4
    move v0, v1

    .line 542349
    goto :goto_2

    .line 542350
    :cond_5
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2pF;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {v1, v2, v3}, LX/2pF;-><init>(D)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_1
.end method

.method private d(I)Z
    .locals 1

    .prologue
    .line 542327
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 542318
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    .line 542319
    iget-boolean v1, p0, LX/2oy;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2oy;->a:Landroid/view/ViewGroup;

    if-ne v0, v1, :cond_1

    .line 542320
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v1, v1, LX/Aca;->c:Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 542321
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v1, v1, LX/Aca;->d:Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 542322
    return-void

    .line 542323
    :cond_1
    iget-object v1, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 542324
    iget-object v2, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/2oy;->m:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 542325
    invoke-static {p0}, LX/2oy;->g(LX/2oy;)V

    .line 542326
    invoke-static {p0, v0, v1}, LX/2oy;->a(LX/2oy;Landroid/view/ViewGroup;I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 542431
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 542432
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->r:LX/7M2;

    sget-object v1, LX/7M1;->TOGGLE:LX/7M1;

    iput-object v1, v0, LX/7M2;->a:LX/7M1;

    .line 542433
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->r:LX/7M2;

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 542434
    :cond_0
    return-void
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    .line 542313
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    .line 542314
    invoke-static {v0}, LX/Aby;->l(LX/Aby;)V

    .line 542315
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 542316
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7M7;

    invoke-direct {v1, p1, p2}, LX/7M7;-><init>(IZ)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 542317
    :cond_0
    return-void
.end method

.method public final a(LX/2pa;Z)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 542222
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    .line 542223
    :cond_0
    :goto_0
    return-void

    .line 542224
    :cond_1
    invoke-virtual {p1}, LX/2pa;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->E:Z

    .line 542225
    iget-wide v0, p1, LX/2pa;->d:D

    const-wide/16 v4, 0x0

    cmpl-double v0, v0, v4

    if-eqz v0, :cond_e

    iget-wide v0, p1, LX/2pa;->d:D

    move-wide v4, v0

    .line 542226
    :goto_1
    if-nez p2, :cond_2

    invoke-static {p1}, LX/393;->p(LX/2pa;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 542227
    :cond_2
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    .line 542228
    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->E:Z

    if-nez v0, :cond_3

    invoke-virtual {p1}, LX/2pa;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->H()LX/6Ri;

    move-result-object v0

    invoke-virtual {v0}, LX/6Ri;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_3
    move v0, v3

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->C:Z

    .line 542229
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->C:Z

    if-nez v0, :cond_10

    invoke-virtual {p1}, LX/2pa;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->H()LX/6Ri;

    move-result-object v0

    .line 542230
    iget-object v1, v0, LX/6Ri;->c:Ljava/lang/String;

    const-string v6, "crop"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 542231
    if-eqz v0, :cond_10

    move v0, v3

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->D:Z

    .line 542232
    const-wide v0, 0x3ff199999999999aL    # 1.1

    cmpl-double v0, v4, v0

    if-ltz v0, :cond_11

    move v0, v3

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->y:Z

    .line 542233
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->y:Z

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 542234
    :goto_5
    iget-boolean v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->C:Z

    invoke-direct {p0, v1, v0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a(ZI)LX/Aca;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    .line 542235
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->j()V

    .line 542236
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    iget-boolean v6, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->C:Z

    if-nez v6, :cond_4

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d(I)Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_4
    move v0, v3

    :goto_6
    invoke-virtual {v1, v0}, LX/Aby;->b(Z)V

    .line 542237
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->w:Z

    .line 542238
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->z:Ljava/lang/String;

    .line 542239
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v2}, LX/Aby;->b(I)V

    .line 542240
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v4, v5}, LX/Aby;->a(D)V

    .line 542241
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->w:Z

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    .line 542242
    iget-object v4, v0, LX/1b4;->a:LX/0ad;

    sget-short v5, LX/1v6;->Y:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    move v0, v4

    .line 542243
    if-eqz v0, :cond_14

    move v0, v3

    .line 542244
    :goto_7
    iput-boolean v0, v1, LX/Aby;->y:Z

    .line 542245
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->C:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->D:Z

    if-eqz v0, :cond_18

    :cond_5
    const/4 v0, 0x0

    move v1, v0

    .line 542246
    :goto_8
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v0, v0, LX/Aca;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 542247
    const/4 v4, 0x5

    invoke-virtual {v0, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 542248
    const/4 v4, 0x6

    invoke-virtual {v0, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 542249
    const/4 v4, 0x7

    invoke-virtual {v0, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 542250
    const/16 v4, 0x8

    invoke-virtual {v0, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 542251
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v1, v1, LX/Aca;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 542252
    :cond_6
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 542253
    const/4 v4, 0x0

    .line 542254
    if-eqz v0, :cond_17

    .line 542255
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 542256
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_17

    .line 542257
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 542258
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_17

    .line 542259
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 542260
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 542261
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 542262
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    .line 542263
    if-eqz v5, :cond_7

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->F:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 542264
    :cond_7
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    iget-object v6, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->z:Ljava/lang/String;

    iget-boolean v7, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->w:Z

    iget-object v8, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    invoke-virtual {v8, v4}, LX/1b4;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v8

    invoke-virtual {v1, v0, v6, v7, v8}, LX/Aby;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;ZZ)V

    .line 542265
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 542266
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_15

    .line 542267
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    invoke-virtual {v1, v0}, LX/Ae5;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 542268
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    .line 542269
    iget-object v1, v0, LX/Ae5;->a:LX/Ae2;

    .line 542270
    iput-object p0, v1, LX/Ae2;->Q:LX/3Gt;

    .line 542271
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    invoke-virtual {v0, v2}, LX/Ae5;->a(I)V

    .line 542272
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    invoke-virtual {v1, v4}, LX/1b4;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v1

    .line 542273
    iget-object v6, v0, LX/Ae5;->a:LX/Ae2;

    .line 542274
    iput-boolean v1, v6, LX/Ae2;->N:Z

    .line 542275
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v0, v0, LX/Aca;->e:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v9}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 542276
    :goto_9
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0}, LX/Aby;->e()V

    .line 542277
    iput-object v5, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->F:Ljava/lang/String;

    .line 542278
    :cond_8
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 542279
    if-eqz v0, :cond_9

    .line 542280
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->A:Ljava/lang/String;

    .line 542281
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v1, v1, LX/Aca;->e:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080c2a

    new-array v7, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 542282
    :cond_9
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    .line 542283
    iput-object p0, v0, LX/Aby;->o:LX/3Gs;

    .line 542284
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->g()V

    move-object v1, v4

    .line 542285
    :goto_a
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->e:LX/Agt;

    iget-object v2, p0, LX/2oy;->i:LX/2oj;

    .line 542286
    iput-object v2, v0, LX/Agt;->b:LX/2oj;

    .line 542287
    if-nez p2, :cond_a

    invoke-static {p1}, LX/393;->p(LX/2pa;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 542288
    :cond_a
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v2, "ShowLiveCommentDialogFragment"

    invoke-virtual {v0, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v2, "ShowLiveCommentDialogFragment"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 542289
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v2, "ShowLiveCommentDialogFragment"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 542290
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    .line 542291
    iget-object v2, v0, LX/Ae5;->a:LX/Ae2;

    .line 542292
    iget-object v0, v2, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 542293
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_b

    .line 542294
    iget-object v0, v2, LX/Ae2;->f:LX/Adk;

    invoke-virtual {v0}, LX/Adk;->d()V

    .line 542295
    :cond_b
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->p:Landroid/app/Activity;

    if-eqz v0, :cond_c

    .line 542296
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->p:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->B:I

    .line 542297
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->y:Z

    if-eqz v0, :cond_16

    .line 542298
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->p:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 542299
    :cond_c
    :goto_b
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c(Lcom/facebook/facecastdisplay/LiveEventsPlugin;I)V

    .line 542300
    :cond_d
    invoke-direct {p0, v1}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a(Lcom/facebook/graphql/model/GraphQLMedia;)V

    .line 542301
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->n:LX/37Y;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->G:LX/AcK;

    invoke-virtual {v0, v1}, LX/37Y;->a(LX/7Iw;)V

    goto/16 :goto_0

    .line 542302
    :cond_e
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    move-wide v4, v0

    goto/16 :goto_1

    :cond_f
    move v0, v2

    .line 542303
    goto/16 :goto_2

    :cond_10
    move v0, v2

    .line 542304
    goto/16 :goto_3

    :cond_11
    move v0, v2

    .line 542305
    goto/16 :goto_4

    :cond_12
    move v0, v3

    .line 542306
    goto/16 :goto_5

    :cond_13
    move v0, v2

    .line 542307
    goto/16 :goto_6

    :cond_14
    move v0, v2

    .line 542308
    goto/16 :goto_7

    .line 542309
    :cond_15
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    invoke-virtual {v0, v9}, LX/Ae5;->a(I)V

    .line 542310
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v0, v0, LX/Aca;->e:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v9}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    goto/16 :goto_9

    .line 542311
    :cond_16
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->p:Landroid/app/Activity;

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_b

    :cond_17
    move-object v1, v4

    goto/16 :goto_a

    .line 542312
    :cond_18
    const v0, 0x7f0d0950

    move v1, v0

    goto/16 :goto_8
.end method

.method public final a(Ljava/lang/String;F)V
    .locals 6

    .prologue
    .line 542207
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    .line 542208
    invoke-static {v0}, LX/Aby;->l(LX/Aby;)V

    .line 542209
    iget-object v1, v0, LX/Aby;->a:LX/AcX;

    .line 542210
    float-to-int v2, p2

    .line 542211
    iget-object v3, v1, LX/AcX;->v:LX/AcC;

    if-nez v3, :cond_0

    .line 542212
    iget-object v3, v1, LX/AcX;->u:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v3, :cond_1

    .line 542213
    iget-object v3, v1, LX/AcX;->u:Lcom/facebook/graphql/model/GraphQLPage;

    .line 542214
    new-instance v4, LX/AcC;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->I()Z

    move-result v0

    const/4 p2, 0x0

    invoke-direct {v4, v5, p0, v0, p2}, LX/AcC;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    move-object v3, v4

    .line 542215
    iput-object v3, v1, LX/AcX;->v:LX/AcC;

    .line 542216
    :cond_0
    :goto_0
    iget-object v3, v1, LX/AcX;->v:LX/AcC;

    move-object v3, v3

    .line 542217
    invoke-static {v1, p1, v2, v3}, LX/AcX;->b(LX/AcX;Ljava/lang/String;ILX/AcC;)V

    .line 542218
    return-void

    .line 542219
    :cond_1
    iget-object v3, v1, LX/AcX;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/20i;

    invoke-virtual {v3}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 542220
    if-eqz v3, :cond_0

    .line 542221
    invoke-static {v3}, LX/AcC;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/AcC;

    move-result-object v3

    iput-object v3, v1, LX/AcX;->v:LX/AcC;

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 542201
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->E:Z

    if-eqz v0, :cond_1

    .line 542202
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 542203
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Ly;

    sget-object v2, LX/7Lx;->HIDE:LX/7Lx;

    invoke-direct {v1, v2}, LX/7Ly;-><init>(LX/7Lx;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 542204
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    if-eqz v0, :cond_1

    .line 542205
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Aby;->h(Z)V

    .line 542206
    :cond_1
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 542188
    iget-boolean v0, p0, LX/2oy;->b:Z

    move v0, v0

    .line 542189
    if-nez v0, :cond_0

    .line 542190
    :goto_0
    return-void

    .line 542191
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->o:LX/AcP;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/AcP;->removeMessages(I)V

    .line 542192
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0}, LX/Aby;->f()V

    .line 542193
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a$redex0(Lcom/facebook/facecastdisplay/LiveEventsPlugin;Z)V

    .line 542194
    iput-object v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->z:Ljava/lang/String;

    .line 542195
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->p:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 542196
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->p:Landroid/app/Activity;

    iget v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->B:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 542197
    :cond_1
    iput-object v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->F:Ljava/lang/String;

    .line 542198
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0}, LX/AVi;->b()V

    .line 542199
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    invoke-virtual {v0}, LX/AVi;->b()V

    .line 542200
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->n:LX/37Y;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->G:LX/AcK;

    invoke-virtual {v0, v1}, LX/37Y;->b(LX/7Iw;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 542164
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    .line 542165
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->w:Z

    if-eqz v0, :cond_3

    .line 542166
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->i()I

    move-result v0

    int-to-long v0, v0

    .line 542167
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 542168
    :goto_0
    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 542169
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v1, v0}, LX/Aby;->a(F)V

    .line 542170
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->d:LX/Ae5;

    .line 542171
    iget-object v2, v1, LX/Ae5;->a:LX/Ae2;

    .line 542172
    iput v0, v2, LX/Ae2;->K:F

    .line 542173
    iget-object v3, v2, LX/AVi;->a:Landroid/view/View;

    move-object v3, v3

    .line 542174
    check-cast v3, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v3, v3, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    if-eqz v3, :cond_0

    .line 542175
    iget-object v3, v2, LX/Ae2;->g:LX/AgU;

    .line 542176
    iput v0, v3, LX/AgU;->g:F

    .line 542177
    :cond_0
    iget-object v3, v2, LX/Ae2;->f:LX/Adk;

    iget v1, v2, LX/Ae2;->K:F

    .line 542178
    iput v1, v3, LX/Adk;->j:F

    .line 542179
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->e:LX/Agt;

    .line 542180
    iget-object v2, v1, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    if-eqz v2, :cond_1

    iget-object v2, v1, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 542181
    iget-object v2, v1, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    .line 542182
    iput v0, v2, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->C:F

    .line 542183
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->o:LX/AcP;

    invoke-virtual {v0, v4}, LX/AcP;->removeMessages(I)V

    .line 542184
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->o:LX/AcP;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v4, v2, v3}, LX/AcP;->sendEmptyMessageDelayed(IJ)Z

    .line 542185
    return-void

    .line 542186
    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 542187
    :cond_3
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final h()V
    .locals 10

    .prologue
    .line 542140
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->e:LX/Agt;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->z:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->A:Ljava/lang/String;

    iget-object v5, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    move-object v4, p0

    const/4 v8, 0x0

    .line 542141
    iput-object v4, v0, LX/Agt;->c:LX/3Gu;

    .line 542142
    const-class v6, LX/0ew;

    invoke-static {v1, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0ew;

    .line 542143
    if-nez v6, :cond_1

    move-object v7, v8

    .line 542144
    :goto_0
    if-eqz v7, :cond_0

    const-string v6, "tip_jar_fragment"

    invoke-virtual {v7, v6}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 542145
    :cond_0
    :goto_1
    return-void

    .line 542146
    :cond_1
    invoke-interface {v6}, LX/0ew;->iC_()LX/0gc;

    move-result-object v6

    move-object v7, v6

    goto :goto_0

    .line 542147
    :cond_2
    iget-object v6, v0, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    if-nez v6, :cond_3

    .line 542148
    new-instance v6, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-direct {v6}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;-><init>()V

    .line 542149
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 542150
    const-string p0, "video_id"

    invoke-virtual {v9, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 542151
    const-string p0, "recipient_id"

    invoke-virtual {v9, p0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 542152
    invoke-virtual {v6, v9}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 542153
    move-object v6, v6

    .line 542154
    iput-object v6, v0, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    .line 542155
    iget-object v6, v0, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v9, v0, LX/Agt;->b:LX/2oj;

    .line 542156
    iput-object v9, v6, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->D:LX/2oj;

    .line 542157
    iget-object v6, v0, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v9, v0, LX/Agt;->c:LX/3Gu;

    .line 542158
    iput-object v9, v6, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->E:LX/3Gu;

    .line 542159
    :cond_3
    const v6, 0x7f0d1976

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 542160
    invoke-virtual {v6}, Landroid/view/View;->bringToFront()V

    .line 542161
    iget-object v6, v0, LX/Agt;->d:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Agw;

    .line 542162
    iget-object v9, v6, LX/Agw;->a:LX/3RX;

    const p0, 0x7f07008a

    iget-object v1, v6, LX/Agw;->b:LX/3RZ;

    invoke-virtual {v1}, LX/3RZ;->a()I

    move-result v1

    const v2, 0x3e19999a    # 0.15f

    invoke-virtual {v9, p0, v1, v2}, LX/3RX;->a(IIF)LX/7Cb;

    .line 542163
    invoke-virtual {v7}, LX/0gc;->a()LX/0hH;

    move-result-object v6

    const v7, 0x7f0d1976

    iget-object v9, v0, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    const-string p0, "tip_jar_fragment"

    invoke-virtual {v6, v7, v9, p0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v6

    invoke-virtual {v6}, LX/0hH;->b()I

    goto :goto_1
.end method

.method public final il_()V
    .locals 3

    .prologue
    .line 542134
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->E:Z

    if-eqz v0, :cond_1

    .line 542135
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 542136
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Ly;

    sget-object v2, LX/7Lx;->SHOW:LX/7Lx;

    invoke-direct {v1, v2}, LX/7Ly;-><init>(LX/7Lx;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 542137
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    if-eqz v0, :cond_1

    .line 542138
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Aby;->h(Z)V

    .line 542139
    :cond_1
    return-void
.end method

.method public final s()V
    .locals 2

    .prologue
    .line 542128
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->f:LX/BSE;

    const/4 p0, 0x0

    .line 542129
    iget-object v1, v0, LX/BSE;->d:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 542130
    iget-object v1, v0, LX/BSE;->d:Landroid/app/Dialog;

    invoke-virtual {v1, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 542131
    iget-object v1, v0, LX/BSE;->d:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 542132
    iput-object p0, v0, LX/BSE;->d:Landroid/app/Dialog;

    .line 542133
    :cond_0
    return-void
.end method
