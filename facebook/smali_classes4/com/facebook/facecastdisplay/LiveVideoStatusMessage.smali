.class public Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field public final a:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 544688
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 544689
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 544686
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 544687
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 544690
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 544691
    const v0, 0x7f0e0366

    invoke-virtual {p0, p1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setTextAppearance(Landroid/content/Context;I)V

    .line 544692
    invoke-static {p0}, LX/3Hk;->a(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->a:Landroid/animation/ObjectAnimator;

    .line 544693
    return-void
.end method


# virtual methods
.method public getLiveAnimator()Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 544685
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->a:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method public setLiveText(LX/Acd;)V
    .locals 2

    .prologue
    .line 544678
    sget-object v0, LX/Acc;->a:[I

    invoke-virtual {p1}, LX/Acd;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 544679
    :goto_0
    return-void

    .line 544680
    :pswitch_0
    const v0, 0x7f080c12

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setText(I)V

    goto :goto_0

    .line 544681
    :pswitch_1
    const v0, 0x7f080bec

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setText(I)V

    goto :goto_0

    .line 544682
    :pswitch_2
    const v0, 0x7f080bef

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setText(I)V

    goto :goto_0

    .line 544683
    :pswitch_3
    const v0, 0x7f080bed

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setText(I)V

    goto :goto_0

    .line 544684
    :pswitch_4
    const v0, 0x7f080bee

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setVideoPlayerViewSize(LX/3HY;)V
    .locals 2

    .prologue
    .line 544674
    sget-object v0, LX/3HY;->REGULAR:LX/3HY;

    if-eq p1, v0, :cond_0

    .line 544675
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 544676
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setTextSize(IF)V

    .line 544677
    :cond_0
    return-void
.end method
