.class public abstract Lcom/facebook/chrome/FbChromeDelegatingActivity;
.super Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;
.source ""

# interfaces
.implements LX/0f1;
.implements LX/0f5;
.implements LX/0f8;
.implements LX/1ZF;


# instance fields
.field public p:LX/GVq;


# direct methods
.method public constructor <init>(LX/GVq;)V
    .locals 0

    .prologue
    .line 375224
    invoke-direct {p0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;-><init>(LX/42j;)V

    .line 375225
    iput-object p1, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    .line 375226
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 375227
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0hE;)V
    .locals 1

    .prologue
    .line 375228
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0, p1}, LX/GVq;->a(LX/0hE;)V

    .line 375229
    return-void
.end method

.method public final a(LX/63W;)V
    .locals 1

    .prologue
    .line 375230
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0, p1}, LX/GVq;->a(LX/63W;)V

    .line 375231
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 1

    .prologue
    .line 375232
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0, p1}, LX/GVq;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 375233
    return-void
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 375234
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0, p1}, LX/GVq;->a_(Ljava/lang/String;)V

    .line 375235
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375236
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 1

    .prologue
    .line 375237
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0, p1}, LX/GVq;->b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 375238
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 1

    .prologue
    .line 375222
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->f()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 375223
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->g()Z

    move-result v0

    return v0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375221
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->getDebugInfo()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/0hE;
    .locals 1

    .prologue
    .line 375220
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->i()LX/0hE;

    move-result-object v0

    return-object v0
.end method

.method public final k_(Z)V
    .locals 1

    .prologue
    .line 375218
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0, p1}, LX/GVq;->k_(Z)V

    .line 375219
    return-void
.end method

.method public final l()LX/0hE;
    .locals 1

    .prologue
    .line 375217
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->l()LX/0hE;

    move-result-object v0

    return-object v0
.end method

.method public final lH_()V
    .locals 1

    .prologue
    .line 375215
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->lH_()V

    .line 375216
    return-void
.end method

.method public final m()LX/0hE;
    .locals 1

    .prologue
    .line 375214
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->m()LX/0hE;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/0hE;
    .locals 1

    .prologue
    .line 375213
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->o()LX/0hE;

    move-result-object v0

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 375212
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0}, LX/GVq;->p()Z

    move-result v0

    return v0
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 375210
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0, p1}, LX/GVq;->setCustomTitle(Landroid/view/View;)V

    .line 375211
    return-void
.end method

.method public final x_(I)V
    .locals 1

    .prologue
    .line 375208
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    invoke-virtual {v0, p1}, LX/GVq;->x_(I)V

    .line 375209
    return-void
.end method
