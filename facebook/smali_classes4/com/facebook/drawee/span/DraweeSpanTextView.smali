.class public Lcom/facebook/drawee/span/DraweeSpanTextView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field private a:LX/2nQ;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 464544
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 464545
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->b:Z

    .line 464546
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 464541
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 464542
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->b:Z

    .line 464543
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 464538
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 464539
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->b:Z

    .line 464540
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 464534
    iget-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    if-eqz v0, :cond_0

    .line 464535
    iget-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    invoke-virtual {v0, p0}, LX/2nQ;->b(Landroid/view/View;)V

    .line 464536
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    .line 464537
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6ca42d79

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 464511
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onAttachedToWindow()V

    .line 464512
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->b:Z

    .line 464513
    iget-object v1, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    if-eqz v1, :cond_0

    .line 464514
    iget-object v1, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    invoke-virtual {v1, p0}, LX/2nQ;->a(Landroid/view/View;)V

    .line 464515
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x56a14b6c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3c8fe8ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 464547
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->b:Z

    .line 464548
    iget-object v1, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    if-eqz v1, :cond_0

    .line 464549
    iget-object v1, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    invoke-virtual {v1, p0}, LX/2nQ;->b(Landroid/view/View;)V

    .line 464550
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onDetachedFromWindow()V

    .line 464551
    const/16 v1, 0x2d

    const v2, 0x3f64377d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 464529
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onFinishTemporaryDetach()V

    .line 464530
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->b:Z

    .line 464531
    iget-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    if-eqz v0, :cond_0

    .line 464532
    iget-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    invoke-virtual {v0, p0}, LX/2nQ;->a(Landroid/view/View;)V

    .line 464533
    :cond_0
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 464524
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->b:Z

    .line 464525
    iget-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    if-eqz v0, :cond_0

    .line 464526
    iget-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    invoke-virtual {v0, p0}, LX/2nQ;->b(Landroid/view/View;)V

    .line 464527
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onStartTemporaryDetach()V

    .line 464528
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 464521
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/text/BetterTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 464522
    invoke-virtual {p0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->a()V

    .line 464523
    return-void
.end method

.method public setDraweeSpanStringBuilder(LX/2nQ;)V
    .locals 1

    .prologue
    .line 464516
    invoke-virtual {p0, p1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 464517
    iput-object p1, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    .line 464518
    iget-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->b:Z

    if-eqz v0, :cond_0

    .line 464519
    iget-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanTextView;->a:LX/2nQ;

    invoke-virtual {v0, p0}, LX/2nQ;->a(Landroid/view/View;)V

    .line 464520
    :cond_0
    return-void
.end method
