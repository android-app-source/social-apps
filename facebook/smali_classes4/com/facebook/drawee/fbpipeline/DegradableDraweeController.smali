.class public Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;
.super Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;
.source ""

# interfaces
.implements LX/0yL;


# instance fields
.field public a:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public final b:LX/0yc;

.field private c:Z

.field public d:Landroid/net/Uri;

.field public e:Z

.field public f:LX/396;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1br;Ljava/util/concurrent/Executor;LX/1Fh;LX/1c3;LX/0yc;LX/0Zb;Ljava/util/Set;LX/1Gd;LX/1Gd;Landroid/net/Uri;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)V
    .locals 14
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .param p4    # LX/1Fh;
        .annotation runtime Lcom/facebook/imagepipeline/module/BitmapMemoryCache;
        .end annotation
    .end param
    .param p8    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/1Gd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/1Gd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # LX/1bh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/1br;",
            "Ljava/util/concurrent/Executor;",
            "LX/1Fh;",
            "Lcom/facebook/imagepipeline/animated/factory/AnimatedDrawableFactory;",
            "LX/0yc;",
            "LX/0Zb;",
            "Ljava/util/Set",
            "<",
            "LX/1c8;",
            ">;",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "LX/1bh;",
            "Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 471332
    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p9

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    invoke-direct/range {v2 .. v13}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;-><init>(Landroid/content/res/Resources;LX/1br;LX/1c3;LX/0Zb;Ljava/util/Set;Ljava/util/concurrent/Executor;LX/1Fh;LX/1Gd;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)V

    .line 471333
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    .line 471334
    move-object/from16 v0, p11

    move-object/from16 v1, p10

    invoke-direct {p0, v0, v1}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->a(Landroid/net/Uri;LX/1Gd;)V

    .line 471335
    return-void
.end method

.method private a(Landroid/net/Uri;LX/1Gd;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 471325
    iput-object p1, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->d:Landroid/net/Uri;

    .line 471326
    iput-boolean v1, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->c:Z

    .line 471327
    iput-boolean v1, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->e:Z

    .line 471328
    iput-object p2, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->a:LX/1Gd;

    .line 471329
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->g:Z

    .line 471330
    sget-object v0, LX/397;->PHOTO:LX/397;

    invoke-static {p0, v1, v0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;ILX/397;)V

    .line 471331
    return-void
.end method

.method public static b(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;ILX/397;)V
    .locals 3

    .prologue
    .line 471315
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/safe_image.php"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 471316
    if-eqz v0, :cond_0

    .line 471317
    sget-object p2, LX/397;->LINK:LX/397;

    .line 471318
    :cond_0
    new-instance v0, LX/396;

    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->t(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, LX/396;-><init>(Ljava/lang/String;ILX/397;)V

    iput-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->f:LX/396;

    .line 471319
    invoke-direct {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->v()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->t(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Ljava/lang/String;

    move-result-object v0

    .line 471320
    const-string v1, "cover_photo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "entity_cards"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 471321
    if-eqz v0, :cond_1

    .line 471322
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->e:Z

    .line 471323
    new-instance v0, LX/5N8;

    iget-object v1, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->f:LX/396;

    invoke-direct {v0, v1, v2}, LX/5N8;-><init>(LX/0yc;LX/396;)V

    invoke-virtual {p0, v0}, LX/1bp;->a(Landroid/graphics/drawable/Drawable;)V

    .line 471324
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static t(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 471311
    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->s()Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v0

    .line 471312
    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 471313
    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->s()Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v0

    .line 471314
    :cond_0
    return-object v0
.end method

.method public static u(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Z
    .locals 1

    .prologue
    .line 471310
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->v()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 471305
    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    invoke-virtual {v2}, LX/0yc;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 471306
    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->y(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 471307
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 471308
    goto :goto_0

    .line 471309
    :cond_2
    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    iget-object v3, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->d:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->s()Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0yc;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->t(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->s()Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0yc;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    iget-object v3, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->d:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->s()Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0yc;->b(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static y(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Z
    .locals 2

    .prologue
    .line 471271
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->f:LX/396;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->f:LX/396;

    iget-object v0, v0, LX/396;->c:LX/397;

    sget-object v1, LX/397;->VIDEO:LX/397;

    invoke-virtual {v0, v1}, LX/397;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1Gd;LX/1Gd;Landroid/net/Uri;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "LX/1bh;",
            "Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 471302
    invoke-super {p0, p1, p4, p5, p6}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->a(LX/1Gd;Ljava/lang/String;LX/1bh;Ljava/lang/Object;)V

    .line 471303
    invoke-direct {p0, p3, p2}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->a(Landroid/net/Uri;LX/1Gd;)V

    .line 471304
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 471294
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->c:Z

    .line 471295
    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->u(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 471296
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->a(LX/0yL;)V

    .line 471297
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->d()V

    .line 471298
    return-void

    .line 471299
    :cond_1
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->e:Z

    if-eqz v0, :cond_0

    .line 471300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->e:Z

    .line 471301
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1bp;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 471267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->c:Z

    .line 471268
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->b(LX/0yL;)V

    .line 471269
    invoke-super {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->e()V

    .line 471270
    return-void
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 471272
    iput-boolean p1, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->g:Z

    .line 471273
    return-void
.end method

.method public final e_(Z)V
    .locals 1

    .prologue
    .line 471274
    if-nez p1, :cond_0

    .line 471275
    invoke-virtual {p0}, LX/1bp;->b()V

    .line 471276
    iget-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->c:Z

    if-eqz v0, :cond_0

    .line 471277
    invoke-virtual {p0}, LX/1bp;->n()V

    .line 471278
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->e:Z

    .line 471279
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1bp;->a(Landroid/graphics/drawable/Drawable;)V

    .line 471280
    :cond_0
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 471281
    return-void
.end method

.method public final m()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 471282
    iget-boolean v1, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->g:Z

    if-nez v1, :cond_1

    .line 471283
    :cond_0
    :goto_0
    return v0

    .line 471284
    :cond_1
    iget-object v1, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->f:LX/396;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->f:LX/396;

    iget-object v1, v1, LX/396;->c:LX/397;

    sget-object v2, LX/397;->LINK:LX/397;

    if-eq v1, v2, :cond_0

    .line 471285
    :cond_2
    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->u(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-super {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final o()LX/1ca;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 471286
    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->u(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->y(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 471287
    if-eqz v0, :cond_1

    invoke-super {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->o()LX/1ca;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->a:LX/1Gd;

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ca;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick()Z
    .locals 4

    .prologue
    .line 471288
    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->u(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471289
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    iget-object v1, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->d:Landroid/net/Uri;

    .line 471290
    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b:LX/0yc;

    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->t(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0yc;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->f:LX/396;

    iget-object v2, v2, LX/396;->c:LX/397;

    sget-object v3, LX/397;->PHOTO:LX/397;

    invoke-virtual {v2, v3}, LX/397;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 471291
    invoke-virtual {v0, p0, v1, v2}, LX/0yc;->a(LX/0yL;Landroid/net/Uri;Z)Z

    move-result v0

    .line 471292
    :goto_1
    return v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->onClick()Z

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 471293
    iget-boolean v0, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->e:Z

    return v0
.end method
