.class public final Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/33u;


# direct methods
.method public constructor <init>(LX/33u;)V
    .locals 0

    .prologue
    .line 473917
    iput-object p1, p0, Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;->a:LX/33u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 473896
    iget-object v0, p0, Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;->a:LX/33u;

    iget-object v0, v0, LX/33u;->n:LX/33y;

    if-eqz v0, :cond_2

    .line 473897
    iget-object v0, p0, Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;->a:LX/33u;

    iget-object v0, v0, LX/33u;->n:LX/33y;

    invoke-virtual {v0}, LX/33y;->j()V

    .line 473898
    iget-object v0, p0, Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;->a:LX/33u;

    iget-object v0, v0, LX/33u;->n:LX/33y;

    invoke-virtual {v0}, LX/33y;->b()LX/349;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;->a:LX/33u;

    iget-object v1, v1, LX/33u;->l:LX/33w;

    invoke-virtual {v0, v1}, LX/349;->b(LX/33w;)V

    .line 473899
    iget-object v0, p0, Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;->a:LX/33u;

    const/4 v1, 0x0

    .line 473900
    iput-object v1, v0, LX/33u;->n:LX/33y;

    .line 473901
    iget-object v0, p0, Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;->a:LX/33u;

    .line 473902
    iget-boolean v1, v0, LX/33u;->p:Z

    if-eqz v1, :cond_0

    .line 473903
    iget-object v1, v0, LX/33u;->j:LX/1LT;

    invoke-virtual {v1}, LX/1LT;->b()V

    .line 473904
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/33u;->p:Z

    .line 473905
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;->a:LX/33u;

    iget-object v0, v0, LX/33u;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33z;

    .line 473906
    iget-object v2, v0, LX/33z;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    iget-object v2, v2, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/33z;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    iget-object v2, v2, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    if-eqz v2, :cond_1

    .line 473907
    iget-object v2, v0, LX/33z;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    iget-object v2, v2, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    iget-object p0, v0, LX/33z;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    iget-object p0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    invoke-virtual {v2, p0}, Lcom/facebook/widget/CustomFrameLayout;->removeView(Landroid/view/View;)V

    .line 473908
    iget-object v2, v0, LX/33z;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    iget-object v2, v2, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    invoke-virtual {v2}, Lcom/facebook/react/ReactRootView;->a()V

    .line 473909
    iget-object v2, v0, LX/33z;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    const/4 p0, 0x0

    .line 473910
    iput-object p0, v2, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    .line 473911
    iget-object v2, v0, LX/33z;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    const/4 p0, 0x1

    .line 473912
    iput-boolean p0, v2, Lcom/facebook/fbreact/fragment/FbReactFragment;->n:Z

    .line 473913
    :cond_1
    iget-object v2, v0, LX/33z;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    const/4 p0, 0x0

    .line 473914
    iput-boolean p0, v2, Lcom/facebook/fbreact/fragment/FbReactFragment;->o:Z

    .line 473915
    goto :goto_0

    .line 473916
    :cond_2
    return-void
.end method
