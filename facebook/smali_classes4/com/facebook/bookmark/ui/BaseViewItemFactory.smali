.class public Lcom/facebook/bookmark/ui/BaseViewItemFactory;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Ljava/lang/Class;

.field private static final g:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:Landroid/view/LayoutInflater;

.field public c:LX/0xB;

.field private final e:LX/0ad;

.field public final f:LX/0Uh;

.field public final h:LX/17S;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 457508
    const-class v0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    sput-object v0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->d:Ljava/lang/Class;

    .line 457509
    const-class v0, LX/2lH;

    const-string v1, "bookmarks"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->g:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/LayoutInflater;LX/0xB;)V
    .locals 2

    .prologue
    .line 457499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457500
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    .line 457501
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b:Landroid/view/LayoutInflater;

    .line 457502
    iput-object p3, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->c:LX/0xB;

    .line 457503
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 457504
    invoke-static {v1}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v0

    check-cast v0, LX/17S;

    iput-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->h:LX/17S;

    .line 457505
    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->e:LX/0ad;

    .line 457506
    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->f:LX/0Uh;

    .line 457507
    return-void
.end method

.method private a(JI)Ljava/lang/String;
    .locals 3

    .prologue
    .line 457481
    const-wide v0, 0x70ccb8b19e36L

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 457482
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->h:LX/17S;

    .line 457483
    sget v1, LX/1TU;->r:I

    invoke-static {v0, v1}, LX/17S;->a(LX/17S;I)Z

    move-result v1

    move v0, v1

    .line 457484
    if-eqz v0, :cond_5

    .line 457485
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f012e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, p3, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 457486
    :goto_0
    move-object v0, v0

    .line 457487
    :goto_1
    return-object v0

    .line 457488
    :cond_0
    const-wide v0, 0xfa57ba2cd207L

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 457489
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->f:LX/0Uh;

    sget v1, LX/FCD;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 457490
    invoke-static {p0, p3}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->e(Lcom/facebook/bookmark/ui/BaseViewItemFactory;I)Ljava/lang/String;

    move-result-object v0

    .line 457491
    :goto_2
    move-object v0, v0

    .line 457492
    goto :goto_1

    .line 457493
    :cond_1
    const-wide v0, 0xe8d530ffbaefL

    cmp-long v0, p1, v0

    if-eqz v0, :cond_2

    const-wide v0, 0x2d3022abd8f60L

    cmp-long v0, p1, v0

    if-nez v0, :cond_3

    .line 457494
    :cond_2
    invoke-static {p0, p3}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->e(Lcom/facebook/bookmark/ui/BaseViewItemFactory;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 457495
    :cond_3
    const-wide v0, 0xafdb5a7f796fL

    cmp-long v0, p1, v0

    if-nez v0, :cond_4

    .line 457496
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f012f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, p3, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 457497
    goto :goto_1

    .line 457498
    :cond_4
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    invoke-static {p0, p3}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->e(Lcom/facebook/bookmark/ui/BaseViewItemFactory;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/bookmark/ui/BaseViewItemFactory;Lcom/facebook/bookmark/model/Bookmark;Landroid/widget/TextView;)V
    .locals 12

    .prologue
    const-wide v10, 0xc63f291b142bL

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 457454
    const-string v0, "page"

    iget-object v3, p1, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 457455
    invoke-virtual {p1}, Lcom/facebook/bookmark/model/Bookmark;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "app"

    iget-object v3, p1, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 457456
    :goto_0
    iget-wide v6, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v3, v6, v10

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->c:LX/0xB;

    sget-object v4, LX/12j;->INBOX:LX/12j;

    invoke-virtual {v3, v4}, LX/0xB;->a(LX/12j;)I

    move-result v3

    .line 457457
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/bookmark/model/Bookmark;->c()Ljava/lang/String;

    move-result-object v4

    .line 457458
    if-nez v5, :cond_0

    if-eqz v0, :cond_3

    :cond_0
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 457459
    :goto_2
    if-eqz v1, :cond_4

    move-object v0, v4

    .line 457460
    :goto_3
    if-lez v3, :cond_8

    .line 457461
    iget-wide v6, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v8, 0x70ccb8b19e36L

    cmp-long v1, v6, v8

    if-nez v1, :cond_5

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->h:LX/17S;

    .line 457462
    sget v3, LX/1TU;->s:I

    invoke-static {v1, v3}, LX/17S;->a(LX/17S;I)Z

    move-result v3

    move v1, v3

    .line 457463
    if-eqz v1, :cond_5

    .line 457464
    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 457465
    :goto_4
    iget-wide v0, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_6

    .line 457466
    const v0, 0x7f02014c

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 457467
    sget-object v0, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v1, LX/0xr;->MEDIUM:LX/0xr;

    invoke-virtual {p2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {p2, v0, v1, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 457468
    :goto_5
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 457469
    :goto_6
    return-void

    :cond_1
    move v0, v2

    .line 457470
    goto :goto_0

    .line 457471
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/bookmark/model/Bookmark;->b()I

    move-result v3

    goto :goto_1

    :cond_3
    move v1, v2

    .line 457472
    goto :goto_2

    .line 457473
    :cond_4
    iget-wide v0, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-direct {p0, v0, v1, v3}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 457474
    :cond_5
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 457475
    :cond_6
    invoke-static {p0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a$redex0(Lcom/facebook/bookmark/ui/BaseViewItemFactory;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 457476
    invoke-virtual {p2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e05ad

    invoke-virtual {p2, v0, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 457477
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 457478
    :goto_7
    sget-object v0, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v1, LX/0xr;->REGULAR:LX/0xr;

    invoke-virtual {p2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {p2, v0, v1, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    goto :goto_5

    .line 457479
    :cond_7
    const v0, 0x7f02014b

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_7

    .line 457480
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6
.end method

.method public static a$redex0(Lcom/facebook/bookmark/ui/BaseViewItemFactory;)Z
    .locals 3

    .prologue
    .line 457453
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->e:LX/0ad;

    sget-short v1, LX/EgI;->c:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static b(Lcom/facebook/fbui/widget/contentview/ContentView;)V
    .locals 4

    .prologue
    .line 457451
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b088d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setPadding(IIII)V

    .line 457452
    return-void
.end method

.method public static b$redex0(Lcom/facebook/bookmark/ui/BaseViewItemFactory;Lcom/facebook/bookmark/model/Bookmark;Landroid/widget/TextView;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 457510
    const/4 v0, 0x0

    .line 457511
    invoke-virtual {p2}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 457512
    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 457513
    iget-wide v2, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v4, 0xc63f291b142bL

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->c:LX/0xB;

    sget-object v2, LX/12j;->INBOX:LX/12j;

    invoke-virtual {v0, v2}, LX/0xB;->a(LX/12j;)I

    move-result v0

    .line 457514
    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f012c

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 457515
    :cond_0
    :goto_1
    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 457516
    if-eqz v0, :cond_3

    .line 457517
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082a75

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    aput-object v4, v3, v6

    iget-object v4, p1, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    aput-object v4, v3, v7

    aput-object v0, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 457518
    :goto_2
    return-object v0

    .line 457519
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/bookmark/model/Bookmark;->b()I

    move-result v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 457520
    goto :goto_1

    .line 457521
    :cond_3
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082a76

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    aput-object v3, v2, v6

    iget-object v3, p1, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 457522
    :cond_4
    if-eqz v0, :cond_5

    .line 457523
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082a74

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    aput-object v4, v3, v6

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 457524
    :cond_5
    iget-object v0, p1, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    goto :goto_2
.end method

.method public static e(Lcom/facebook/bookmark/ui/BaseViewItemFactory;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 457450
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f012d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/facebook/bookmark/model/Bookmark;)I
    .locals 1

    .prologue
    .line 457447
    instance-of v0, p1, LX/EhJ;

    if-eqz v0, :cond_0

    .line 457448
    check-cast p1, LX/EhJ;

    invoke-interface {p1}, LX/EhJ;->a()I

    move-result v0

    .line 457449
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/2lb;Lcom/facebook/bookmark/model/BookmarksGroup;Z)LX/2lI;
    .locals 1
    .param p2    # Lcom/facebook/bookmark/model/BookmarksGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 457446
    new-instance v0, LX/EhP;

    invoke-direct {v0, p0, p1, p2, p3}, LX/EhP;-><init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Lcom/facebook/bookmark/model/BookmarksGroup;Z)V

    return-object v0
.end method

.method public final a(LX/2lb;Ljava/lang/Object;II)LX/2lI;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2lb;",
            "TD;II)",
            "LX/2lI;"
        }
    .end annotation

    .prologue
    .line 457444
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 457445
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Ljava/lang/Object;ILjava/lang/CharSequence;)LX/2lI;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2lb;Ljava/lang/Object;ILjava/lang/CharSequence;)LX/2lI;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2lb;",
            "TD;I",
            "Ljava/lang/CharSequence;",
            ")",
            "LX/2lI;"
        }
    .end annotation

    .prologue
    .line 457443
    new-instance v0, LX/EhQ;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/EhQ;-><init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Ljava/lang/Object;ILjava/lang/CharSequence;)V

    return-object v0
.end method

.method public final b(LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)LX/2lI;
    .locals 1

    .prologue
    .line 457442
    new-instance v0, Lcom/facebook/bookmark/ui/BaseViewItemFactory$FamilyBridgesProfileViewItem;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/bookmark/ui/BaseViewItemFactory$FamilyBridgesProfileViewItem;-><init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)V

    return-object v0
.end method

.method public final c(LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)LX/2lI;
    .locals 1

    .prologue
    .line 457441
    new-instance v0, LX/EhO;

    invoke-direct {v0, p0, p1, p2}, LX/EhO;-><init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)V

    return-object v0
.end method
