.class public Lcom/facebook/bookmark/FetchBookmarksResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/bookmark/FetchBookmarksResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 458173
    new-instance v0, LX/2le;

    invoke-direct {v0}, LX/2le;-><init>()V

    sput-object v0, Lcom/facebook/bookmark/FetchBookmarksResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;JLX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ta;",
            "J",
            "LX/0Px",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 458170
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 458171
    iput-object p4, p0, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    .line 458172
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 458165
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 458166
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 458167
    sget-object v1, Lcom/facebook/bookmark/model/BookmarksGroup;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 458168
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    .line 458169
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 458156
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 458160
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mBookmarksGroups"

    iget-object v2, p0, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "status"

    .line 458161
    iget-object v2, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 458162
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "syncTime"

    .line 458163
    iget-wide v4, p0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v4

    .line 458164
    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 458157
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 458158
    iget-object v0, p0, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 458159
    return-void
.end method
