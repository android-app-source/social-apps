.class public Lcom/facebook/bookmark/client/BookmarkClient;
.super Landroid/content/BroadcastReceiver;
.source ""

# interfaces
.implements LX/2l5;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/bookmark/client/BookmarkClient;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile q:Lcom/facebook/bookmark/client/BookmarkClient;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0aG;

.field private final d:LX/0TD;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/2l7;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public j:J

.field public k:I

.field public l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "BookmarkClient.this"
    .end annotation
.end field

.field public m:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "BookmarkClient.this"
    .end annotation
.end field

.field public final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "BookmarkClient.this"
    .end annotation
.end field

.field public final o:Landroid/os/Handler;

.field private final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0nz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 456856
    const-class v0, Lcom/facebook/bookmark/client/BookmarkClient;

    sput-object v0, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0aG;LX/0TD;Ljava/util/concurrent/ExecutorService;LX/2l7;)V
    .locals 2
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 456857
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 456858
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    .line 456859
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->h:Ljava/util/Map;

    .line 456860
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->i:Ljava/util/Set;

    .line 456861
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->j:J

    .line 456862
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->k:I

    .line 456863
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->n:Ljava/util/Map;

    .line 456864
    iput-object p1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->b:Landroid/content/Context;

    .line 456865
    iput-object p2, p0, Lcom/facebook/bookmark/client/BookmarkClient;->c:LX/0aG;

    .line 456866
    iput-object p3, p0, Lcom/facebook/bookmark/client/BookmarkClient;->d:LX/0TD;

    .line 456867
    iput-object p4, p0, Lcom/facebook/bookmark/client/BookmarkClient;->e:Ljava/util/concurrent/ExecutorService;

    .line 456868
    iput-object p5, p0, Lcom/facebook/bookmark/client/BookmarkClient;->f:LX/2l7;

    .line 456869
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->p:Ljava/util/Set;

    .line 456870
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->o:Landroid/os/Handler;

    .line 456871
    new-instance v0, Landroid/content/IntentFilter;

    sget-object v1, LX/0Ow;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 456872
    :try_start_0
    const-string v1, "vnd.android.cursor.item/vnd.facebook.katana.bookmark"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    .line 456873
    const-string v1, "vnd.android.cursor.item/vnd.facebook.katana.favorites"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    .line 456874
    const-string v1, "vnd.android.cursor.item/vnd.facebook.katana.bookmark_unread_count"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 456875
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->b:Landroid/content/Context;

    invoke-static {v1}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 456876
    return-void

    .line 456877
    :catch_0
    move-exception v0

    .line 456878
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/bookmark/client/BookmarkClient;
    .locals 9

    .prologue
    .line 456879
    sget-object v0, Lcom/facebook/bookmark/client/BookmarkClient;->q:Lcom/facebook/bookmark/client/BookmarkClient;

    if-nez v0, :cond_1

    .line 456880
    const-class v1, Lcom/facebook/bookmark/client/BookmarkClient;

    monitor-enter v1

    .line 456881
    :try_start_0
    sget-object v0, Lcom/facebook/bookmark/client/BookmarkClient;->q:Lcom/facebook/bookmark/client/BookmarkClient;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 456882
    if-eqz v2, :cond_0

    .line 456883
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 456884
    new-instance v3, Lcom/facebook/bookmark/client/BookmarkClient;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/2l7;->a(LX/0QB;)LX/2l7;

    move-result-object v8

    check-cast v8, LX/2l7;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/bookmark/client/BookmarkClient;-><init>(Landroid/content/Context;LX/0aG;LX/0TD;Ljava/util/concurrent/ExecutorService;LX/2l7;)V

    .line 456885
    move-object v0, v3

    .line 456886
    sput-object v0, Lcom/facebook/bookmark/client/BookmarkClient;->q:Lcom/facebook/bookmark/client/BookmarkClient;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 456887
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 456888
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 456889
    :cond_1
    sget-object v0, Lcom/facebook/bookmark/client/BookmarkClient;->q:Lcom/facebook/bookmark/client/BookmarkClient;

    return-object v0

    .line 456890
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 456891
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/bookmark/client/BookmarkClient;Z)Lcom/facebook/bookmark/model/BookmarksGroup;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 456892
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 456893
    const-string v2, "pinned"

    iget-object v3, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 456894
    if-eqz p1, :cond_1

    .line 456895
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->f()Lcom/facebook/bookmark/model/BookmarksGroup;

    move-result-object v0

    .line 456896
    :cond_1
    :goto_0
    return-object v0

    .line 456897
    :cond_2
    sget-object v0, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v1, "The favorite bookmark group was not found in BookmarkManager."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 456898
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/facebook/bookmark/model/BookmarksGroup;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 456899
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 456900
    const-string v0, "newFavoriteBookmarksGroup"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 456901
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->c:LX/0aG;

    const-string v1, "setFavoriteBookmarks"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/bookmark/client/BookmarkClient;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x570ac737

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 456902
    if-eqz p2, :cond_0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 456903
    :goto_0
    return-object v0

    .line 456904
    :cond_0
    invoke-interface {v0}, LX/1MF;->startTryNotToUseMainThread()LX/1ML;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 456905
    const-string v0, "bookmark_new_favorites"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 456906
    if-nez v0, :cond_0

    .line 456907
    sget-object v0, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v1, "Invalid favorites bookmarks broadcast!"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 456908
    :goto_0
    return-void

    .line 456909
    :cond_0
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 456910
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v1, v1, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-static {v1, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 456911
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 456912
    :cond_1
    invoke-static {p0}, Lcom/facebook/bookmark/client/BookmarkClient;->j(Lcom/facebook/bookmark/client/BookmarkClient;)LX/0Px;

    move-result-object v3

    .line 456913
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result p1

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, p1, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0nz;

    .line 456914
    invoke-interface {v1, v0}, LX/0nz;->a(Lcom/facebook/bookmark/model/BookmarksGroup;)V

    .line 456915
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 456916
    :cond_2
    goto :goto_0

    .line 456917
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method private a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V
    .locals 4

    .prologue
    .line 456956
    invoke-static {p0}, Lcom/facebook/bookmark/client/BookmarkClient;->j(Lcom/facebook/bookmark/client/BookmarkClient;)LX/0Px;

    move-result-object v2

    .line 456957
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0nz;

    .line 456958
    invoke-interface {v0, p1, p2}, LX/0nz;->a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V

    .line 456959
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 456960
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/facebook/bookmark/FetchBookmarksResult;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 456918
    iget-object v0, p1, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    move-object v0, v0

    .line 456919
    if-nez v0, :cond_0

    .line 456920
    new-instance v0, Lcom/facebook/bookmark/FetchBookmarksResult;

    .line 456921
    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 456922
    iget-wide v8, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v8

    .line 456923
    iget-object v4, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/bookmark/FetchBookmarksResult;-><init>(LX/0ta;JLX/0Px;)V

    invoke-direct {p0, v0, v5}, Lcom/facebook/bookmark/client/BookmarkClient;->a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V

    .line 456924
    :goto_0
    return-void

    .line 456925
    :cond_0
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    .line 456926
    iget-object v1, p1, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    move-object v1, v1

    .line 456927
    invoke-static {v0, v1}, Lcom/facebook/bookmark/model/BookmarksGroup;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 456928
    iget-object v0, p1, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    move-object v0, v0

    .line 456929
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    .line 456930
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 456931
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 456932
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/model/Bookmark;

    .line 456933
    iget-object v4, p0, Lcom/facebook/bookmark/client/BookmarkClient;->h:Ljava/util/Map;

    iget-wide v6, v1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 456934
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    .line 456935
    iget-object v3, p0, Lcom/facebook/bookmark/client/BookmarkClient;->h:Ljava/util/Map;

    iget-wide v4, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 456936
    :cond_3
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/facebook/bookmark/client/BookmarkClient;->a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V

    goto :goto_0

    .line 456937
    :cond_4
    invoke-direct {p0, p1, v5}, Lcom/facebook/bookmark/client/BookmarkClient;->a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/bookmark/client/BookmarkClient;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 456938
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 456939
    check-cast v0, Lcom/facebook/fbservice/service/ServiceException;

    .line 456940
    iget-object v1, v0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 456941
    iget-object v2, v1, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v1, v2

    .line 456942
    if-eqz v1, :cond_0

    .line 456943
    sget-object v2, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Failed , "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "originalExceptionMessage"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "originalExceptionStack"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 456944
    :goto_0
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->o:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/bookmark/client/BookmarkClient$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/bookmark/client/BookmarkClient$1;-><init>(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/facebook/fbservice/service/ServiceException;)V

    const v3, -0x63b2e0f1

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 456945
    :goto_1
    return-void

    .line 456946
    :cond_0
    sget-object v1, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v2, "%s %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    const-string v4, "Failed"

    aput-object v4, v3, v5

    invoke-static {v1, p1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 456947
    :cond_1
    sget-object v0, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v1, "%s %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p2, v2, v4

    const-string v3, " Failed for other reasons."

    aput-object v3, v2, v5

    invoke-static {v0, p1, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static declared-synchronized j(Lcom/facebook/bookmark/client/BookmarkClient;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/0nz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 456948
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->p:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 456949
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456950
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456951
    :goto_0
    monitor-exit p0

    return-object v0

    .line 456952
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->d:LX/0TD;

    new-instance v1, LX/EgK;

    invoke-direct {v1, p0, p1}, LX/EgK;-><init>(Lcom/facebook/bookmark/client/BookmarkClient;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 456953
    new-instance v1, LX/EgL;

    invoke-direct {v1, p0, p1}, LX/EgL;-><init>(Lcom/facebook/bookmark/client/BookmarkClient;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/bookmark/client/BookmarkClient;->d:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 456954
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->n:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 456955
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 456849
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->m:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 456850
    :goto_0
    monitor-exit p0

    return-void

    .line 456851
    :cond_0
    :try_start_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 456852
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->c:LX/0aG;

    const-string v2, "syncWithServer"

    const v3, 0x46eb9ebd

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 456853
    iput-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 456854
    new-instance v1, LX/EgM;

    invoke-direct {v1, p0}, LX/EgM;-><init>(Lcom/facebook/bookmark/client/BookmarkClient;)V

    iget-object v2, p0, Lcom/facebook/bookmark/client/BookmarkClient;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 456855
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(JI)V
    .locals 5

    .prologue
    .line 456770
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 456771
    sget-object v1, LX/2md;->b:LX/0U1;

    .line 456772
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 456773
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 456774
    sget-object v1, LX/2md;->g:LX/0U1;

    .line 456775
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 456776
    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 456777
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->c:LX/0aG;

    const-string v2, "updateUnreadCount"

    const v3, 0x6ac3c3b2

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 456778
    new-instance v1, LX/EgN;

    invoke-direct {v1, p0}, LX/EgN;-><init>(Lcom/facebook/bookmark/client/BookmarkClient;)V

    iget-object v2, p0, Lcom/facebook/bookmark/client/BookmarkClient;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 456779
    return-void
.end method

.method public final declared-synchronized a(LX/0nz;)V
    .locals 2

    .prologue
    .line 456846
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->p:Ljava/util/Set;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456847
    monitor-exit p0

    return-void

    .line 456848
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 456845
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/0nz;)V
    .locals 1

    .prologue
    .line 456842
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456843
    monitor-exit p0

    return-void

    .line 456844
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 456834
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 456835
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->l:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456836
    :goto_0
    monitor-exit p0

    return-object v0

    .line 456837
    :cond_0
    :try_start_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 456838
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->c:LX/0aG;

    const-string v2, "syncWithDB"

    const v3, -0x6867e7be

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 456839
    iput-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 456840
    new-instance v1, LX/2li;

    invoke-direct {v1, p0}, LX/2li;-><init>(Lcom/facebook/bookmark/client/BookmarkClient;)V

    iget-object v2, p0, Lcom/facebook/bookmark/client/BookmarkClient;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 456841
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 2

    .prologue
    .line 456827
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 456828
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 456829
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 456830
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->k:I

    .line 456831
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->j:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456832
    monitor-exit p0

    return-void

    .line 456833
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()LX/0ta;
    .locals 4

    .prologue
    .line 456820
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456821
    sget-object v0, LX/0ta;->NO_DATA:LX/0ta;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456822
    :goto_0
    monitor-exit p0

    return-object v0

    .line 456823
    :cond_0
    :try_start_1
    iget-wide v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->j:J

    iget v2, p0, Lcom/facebook/bookmark/client/BookmarkClient;->k:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 456824
    :cond_1
    sget-object v0, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    goto :goto_0

    .line 456825
    :cond_2
    sget-object v0, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 456826
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Lcom/facebook/bookmark/FetchBookmarksResult;
    .locals 5

    .prologue
    .line 456819
    new-instance v0, Lcom/facebook/bookmark/FetchBookmarksResult;

    invoke-virtual {p0}, Lcom/facebook/bookmark/client/BookmarkClient;->d()LX/0ta;

    move-result-object v1

    iget-wide v2, p0, Lcom/facebook/bookmark/client/BookmarkClient;->j:J

    iget-object v4, p0, Lcom/facebook/bookmark/client/BookmarkClient;->g:Ljava/util/List;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/bookmark/FetchBookmarksResult;-><init>(LX/0ta;JLX/0Px;)V

    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 456817
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/bookmark/client/BookmarkClient;->j:J

    .line 456818
    return-void
.end method

.method public final g()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 456810
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->i:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 456811
    :cond_0
    :goto_0
    return v0

    .line 456812
    :cond_1
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/facebook/bookmark/client/BookmarkClient;->a(Lcom/facebook/bookmark/client/BookmarkClient;Z)Lcom/facebook/bookmark/model/BookmarksGroup;

    move-result-object v2

    .line 456813
    if-nez v2, :cond_3

    .line 456814
    :cond_2
    iget-object v1, p0, Lcom/facebook/bookmark/client/BookmarkClient;->i:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 456815
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/bookmark/model/BookmarksGroup;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/bookmark/model/Bookmark;

    .line 456816
    iget-object v4, p0, Lcom/facebook/bookmark/client/BookmarkClient;->i:Ljava/util/Set;

    iget-wide v6, v2, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13

    .prologue
    monitor-enter p0

    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x2415c210

    :try_start_0
    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 456780
    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    .line 456781
    const-string v2, "vnd.android.cursor.item/vnd.facebook.katana.bookmark"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 456782
    const-string v5, "bookmark_groups"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/facebook/bookmark/FetchBookmarksResult;

    .line 456783
    if-nez v5, :cond_5

    .line 456784
    sget-object v5, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v6, "Invalid bookmark content broadcast!"

    invoke-static {v5, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 456785
    :goto_0
    const v1, -0x175080ad

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    .line 456786
    :cond_0
    :try_start_1
    const-string v2, "vnd.android.cursor.item/vnd.facebook.katana.bookmark_unread_count"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 456787
    const-wide/16 v11, 0x0

    const/4 v9, -0x1

    .line 456788
    const-string v5, "bookmark_fbid"

    invoke-virtual {p2, v5, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    .line 456789
    const-string v5, "bookmark_unread_count"

    invoke-virtual {p2, v5, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 456790
    cmp-long v5, v7, v11

    if-eqz v5, :cond_1

    if-ne v6, v9, :cond_6

    .line 456791
    :cond_1
    sget-object v5, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v6, "Bookmark unread count update broadcast has invalid data."

    invoke-static {v5, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 456792
    :cond_2
    :goto_1
    goto :goto_0

    .line 456793
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 456794
    :cond_3
    :try_start_2
    const-string v2, "vnd.android.cursor.item/vnd.facebook.katana.favorites"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 456795
    invoke-direct {p0, p2}, Lcom/facebook/bookmark/client/BookmarkClient;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 456796
    :cond_4
    sget-object v2, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unexpected broadcast type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 456797
    :cond_5
    :try_start_3
    iget-wide v9, v5, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v7, v9

    .line 456798
    iput-wide v7, p0, Lcom/facebook/bookmark/client/BookmarkClient;->j:J

    .line 456799
    invoke-static {p0, v5}, Lcom/facebook/bookmark/client/BookmarkClient;->a$redex0(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/facebook/bookmark/FetchBookmarksResult;)V

    goto :goto_0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 456800
    :cond_6
    iget-object v5, p0, Lcom/facebook/bookmark/client/BookmarkClient;->h:Ljava/util/Map;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/bookmark/model/Bookmark;

    .line 456801
    if-nez v5, :cond_7

    .line 456802
    sget-object v5, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "Bookmark unread count update broadcast has invalid bookmark fbid: "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1

    .line 456803
    :cond_7
    invoke-virtual {v5}, Lcom/facebook/bookmark/model/Bookmark;->b()I

    move-result v7

    if-eq v7, v6, :cond_2

    .line 456804
    invoke-virtual {v5, v6}, Lcom/facebook/bookmark/model/Bookmark;->a(I)V

    .line 456805
    invoke-static {p0}, Lcom/facebook/bookmark/client/BookmarkClient;->j(Lcom/facebook/bookmark/client/BookmarkClient;)LX/0Px;

    move-result-object v8

    .line 456806
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v7, v6

    :goto_2
    if-ge v7, v9, :cond_8

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0nz;

    .line 456807
    invoke-interface {v6, v5}, LX/0nz;->a(Lcom/facebook/bookmark/model/Bookmark;)V

    .line 456808
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_2

    .line 456809
    :cond_8
    goto :goto_1
.end method
