.class public Lcom/facebook/bookmark/model/Bookmark;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/model/BookmarkDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/bookmark/model/BookmarksGroup;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public final clientToken:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "client_token"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final id:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final layout:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "layout"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "count"
    .end annotation
.end field

.field private mCountString:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "count_string"
    .end annotation
.end field

.field public final miniAppIcon:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mini_app_icon"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final pic:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pic"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final subName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sub_name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public url:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "url"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 457700
    const-class v0, Lcom/facebook/bookmark/model/BookmarkDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 457699
    new-instance v0, LX/2lL;

    invoke-direct {v0}, LX/2lL;-><init>()V

    sput-object v0, Lcom/facebook/bookmark/model/Bookmark;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 457684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457685
    iput-boolean v3, p0, Lcom/facebook/bookmark/model/Bookmark;->b:Z

    .line 457686
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    .line 457687
    iput-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    .line 457688
    iput-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    .line 457689
    iput v3, p0, Lcom/facebook/bookmark/model/Bookmark;->mCount:I

    .line 457690
    iput-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->mCountString:Ljava/lang/String;

    .line 457691
    iput-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    .line 457692
    iput-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    .line 457693
    iput-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->clientToken:Ljava/lang/String;

    .line 457694
    iput-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->layout:Ljava/lang/String;

    .line 457695
    iput-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    .line 457696
    iput-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->miniAppIcon:Ljava/lang/String;

    .line 457697
    iput-object v2, p0, Lcom/facebook/bookmark/model/Bookmark;->a:Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 457698
    return-void
.end method

.method public constructor <init>(LX/2lK;)V
    .locals 2

    .prologue
    .line 457669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457670
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/bookmark/model/Bookmark;->b:Z

    .line 457671
    iget-wide v0, p1, LX/2lK;->a:J

    iput-wide v0, p0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    .line 457672
    iget-object v0, p1, LX/2lK;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    .line 457673
    iget-object v0, p1, LX/2lK;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    .line 457674
    iget v0, p1, LX/2lK;->d:I

    iput v0, p0, Lcom/facebook/bookmark/model/Bookmark;->mCount:I

    .line 457675
    iget-object v0, p1, LX/2lK;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->mCountString:Ljava/lang/String;

    .line 457676
    iget-object v0, p1, LX/2lK;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    .line 457677
    iget-object v0, p1, LX/2lK;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    .line 457678
    iget-object v0, p1, LX/2lK;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->clientToken:Ljava/lang/String;

    .line 457679
    iget-object v0, p1, LX/2lK;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->layout:Ljava/lang/String;

    .line 457680
    iget-object v0, p1, LX/2lK;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    .line 457681
    iget-object v0, p1, LX/2lK;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->miniAppIcon:Ljava/lang/String;

    .line 457682
    iget-object v0, p1, LX/2lK;->l:Lcom/facebook/bookmark/model/BookmarksGroup;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->a:Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 457683
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 457645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457646
    iput-boolean v1, p0, Lcom/facebook/bookmark/model/Bookmark;->b:Z

    .line 457647
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    .line 457648
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    .line 457649
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    .line 457650
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bookmark/model/Bookmark;->mCount:I

    .line 457651
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->mCountString:Ljava/lang/String;

    .line 457652
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    .line 457653
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    .line 457654
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->clientToken:Ljava/lang/String;

    .line 457655
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->layout:Ljava/lang/String;

    .line 457656
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    .line 457657
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->miniAppIcon:Ljava/lang/String;

    .line 457658
    const-class v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    iput-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->a:Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 457659
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/bookmark/model/Bookmark;->b:Z

    .line 457660
    return-void

    :cond_0
    move v0, v1

    .line 457661
    goto :goto_0
.end method

.method private static a(Lcom/facebook/bookmark/model/Bookmark;Lcom/facebook/bookmark/model/Bookmark;)Z
    .locals 4

    .prologue
    .line 457668
    iget-wide v0, p0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    iget-wide v2, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->clientToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->clientToken:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/bookmark/model/Bookmark;->mCount:I

    iget v1, p1, Lcom/facebook/bookmark/model/Bookmark;->mCount:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->mCountString:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->mCountString:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->a:Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->a:Lcom/facebook/bookmark/model/BookmarksGroup;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/bookmark/model/Bookmark;->b:Z

    iget-boolean v1, p1, Lcom/facebook/bookmark/model/Bookmark;->b:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->layout:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->layout:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->miniAppIcon:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->miniAppIcon:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 457662
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    move v2, v3

    .line 457663
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 457664
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/model/Bookmark;

    invoke-static {v0, v1}, Lcom/facebook/bookmark/model/Bookmark;->a(Lcom/facebook/bookmark/model/Bookmark;Lcom/facebook/bookmark/model/Bookmark;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 457665
    :cond_0
    :goto_1
    return v3

    .line 457666
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 457667
    :cond_2
    const/4 v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 457701
    iput p1, p0, Lcom/facebook/bookmark/model/Bookmark;->mCount:I

    .line 457702
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 457625
    iput-boolean p1, p0, Lcom/facebook/bookmark/model/Bookmark;->b:Z

    .line 457626
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 457623
    iget-boolean v0, p0, Lcom/facebook/bookmark/model/Bookmark;->b:Z

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 457624
    iget v0, p0, Lcom/facebook/bookmark/model/Bookmark;->mCount:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 457627
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->mCountString:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 457628
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 457629
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 457630
    iget-wide v0, p0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 457631
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 457632
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 457633
    iget v0, p0, Lcom/facebook/bookmark/model/Bookmark;->mCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 457634
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->mCountString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 457635
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 457636
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 457637
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->clientToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 457638
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->layout:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 457639
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 457640
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->miniAppIcon:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 457641
    iget-object v0, p0, Lcom/facebook/bookmark/model/Bookmark;->a:Lcom/facebook/bookmark/model/BookmarksGroup;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 457642
    iget-boolean v0, p0, Lcom/facebook/bookmark/model/Bookmark;->b:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 457643
    return-void

    .line 457644
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
