.class public Lcom/facebook/checkin/rows/RatingBarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Environment::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlace;",
        "LX/3Bb;",
        "TEnvironment;",
        "Landroid/widget/RatingBar;",
        ">;",
        "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlace;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527717
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 527718
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/checkin/rows/RatingBarPartDefinition;
    .locals 3

    .prologue
    .line 527706
    const-class v1, Lcom/facebook/checkin/rows/RatingBarPartDefinition;

    monitor-enter v1

    .line 527707
    :try_start_0
    sget-object v0, Lcom/facebook/checkin/rows/RatingBarPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 527708
    sput-object v2, Lcom/facebook/checkin/rows/RatingBarPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 527709
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527710
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 527711
    new-instance v0, Lcom/facebook/checkin/rows/RatingBarPartDefinition;

    invoke-direct {v0}, Lcom/facebook/checkin/rows/RatingBarPartDefinition;-><init>()V

    .line 527712
    move-object v0, v0

    .line 527713
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 527714
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/checkin/rows/RatingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527715
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 527716
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPlace;)Z
    .locals 1

    .prologue
    .line 527705
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->B()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 527702
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 527703
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPlace;->B()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    .line 527704
    new-instance v1, LX/3Bb;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLRating;->k()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLRating;->j()I

    move-result v0

    invoke-direct {v1, v2, v0}, LX/3Bb;-><init>(FI)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7d4ae9a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 527697
    check-cast p2, LX/3Bb;

    check-cast p4, Landroid/widget/RatingBar;

    .line 527698
    iget v1, p2, LX/3Bb;->a:F

    invoke-virtual {p4, v1}, Landroid/widget/RatingBar;->setRating(F)V

    .line 527699
    iget v1, p2, LX/3Bb;->b:I

    invoke-virtual {p4, v1}, Landroid/widget/RatingBar;->setNumStars(I)V

    .line 527700
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 527701
    const/16 v1, 0x1f

    const v2, -0x3d33fc73

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 527693
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-static {p1}, Lcom/facebook/checkin/rows/RatingBarPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 527694
    check-cast p4, Landroid/widget/RatingBar;

    .line 527695
    const/16 v0, 0x8

    invoke-virtual {p4, v0}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 527696
    return-void
.end method
