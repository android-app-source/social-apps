.class public Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/31T;",
        "Ljava/lang/Boolean;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final e:Lcom/facebook/checkin/rows/RatingBarPartDefinition;

.field private final f:LX/0W9;

.field private final g:Landroid/content/Context;

.field private final h:LX/3BG;

.field private final i:LX/3BH;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 527669
    const-class v0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/checkin/rows/RatingBarPartDefinition;LX/0W9;Landroid/content/Context;LX/3BG;LX/3BH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527659
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 527660
    iput-object p1, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 527661
    iput-object p2, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 527662
    iput-object p3, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 527663
    iput-object p4, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->e:Lcom/facebook/checkin/rows/RatingBarPartDefinition;

    .line 527664
    iput-object p5, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->f:LX/0W9;

    .line 527665
    iput-object p6, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->g:Landroid/content/Context;

    .line 527666
    iput-object p7, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->h:LX/3BG;

    .line 527667
    iput-object p8, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->i:LX/3BH;

    .line 527668
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;
    .locals 12

    .prologue
    .line 527648
    const-class v1, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    monitor-enter v1

    .line 527649
    :try_start_0
    sget-object v0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 527650
    sput-object v2, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 527651
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527652
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 527653
    new-instance v3, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/checkin/rows/RatingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/rows/RatingBarPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/checkin/rows/RatingBarPartDefinition;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v8

    check-cast v8, LX/0W9;

    const-class v9, Landroid/content/Context;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {v0}, LX/3BG;->a(LX/0QB;)LX/3BG;

    move-result-object v10

    check-cast v10, LX/3BG;

    invoke-static {v0}, LX/3BH;->b(LX/0QB;)LX/3BH;

    move-result-object v11

    check-cast v11, LX/3BH;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/checkin/rows/RatingBarPartDefinition;LX/0W9;Landroid/content/Context;LX/3BG;LX/3BH;)V

    .line 527654
    move-object v0, v3

    .line 527655
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 527656
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527657
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 527658
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1aD;LX/31T;)Ljava/lang/Boolean;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/31T;",
            ")",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 527670
    iget-object v4, p2, LX/31T;->c:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 527671
    const v0, 0x7f0d116e

    iget-object v5, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v0, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527672
    invoke-static {v4}, Lcom/facebook/checkin/rows/RatingBarPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527673
    const v0, 0x7f0d1170

    iget-object v5, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->e:Lcom/facebook/checkin/rows/RatingBarPartDefinition;

    invoke-interface {p1, v0, v5, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527674
    const v0, 0x7f0d116f

    iget-object v5, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v6, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->h:LX/3BG;

    const v7, 0x7f0f00a4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlace;->B()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLRating;->k()D

    move-result-wide v8

    double-to-float v8, v8

    float-to-double v8, v8

    invoke-virtual {v6, v7, v8, v9}, LX/3BG;->a(ID)Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v0, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527675
    :cond_0
    invoke-static {v4}, LX/3BH;->a(Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 527676
    iget-object v0, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->f:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 527677
    iget-object v5, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->g:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f008f

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;->a()I

    move-result v7

    new-array v8, v2, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;->a()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v0, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v3

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 527678
    :goto_0
    const v5, 0x7f0d1172

    iget-object v6, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-interface {p1, v5, v6, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527679
    invoke-static {v4}, LX/3BH;->b(Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlace;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 527680
    :goto_1
    const v1, 0x7f0d1171

    iget-object v5, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-interface {p1, v1, v5, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527681
    invoke-static {v4}, LX/3BH;->c(Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 527682
    const v0, 0x7f0d116d

    iget-object v1, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v3

    sget-object v4, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 527683
    iput-object v4, v3, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 527684
    move-object v3, v3

    .line 527685
    invoke-virtual {v3}, LX/2f8;->a()LX/2f9;

    move-result-object v3

    invoke-interface {p1, v0, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    move v0, v2

    .line 527686
    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    .line 527687
    goto :goto_1

    .line 527688
    :cond_2
    iget-boolean v0, p2, LX/31T;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->i:LX/3BH;

    iget-object v1, p2, LX/31T;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v4, v1}, LX/3BH;->a(Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 527689
    const v0, 0x7f0d116d

    iget-object v1, p0, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "res:///"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v5, 0x7f0201d2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v3

    sget-object v4, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 527690
    iput-object v4, v3, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 527691
    move-object v3, v3

    .line 527692
    invoke-virtual {v3}, LX/2f8;->a()LX/2f9;

    move-result-object v3

    invoke-interface {p1, v0, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 527647
    check-cast p2, LX/31T;

    invoke-direct {p0, p1, p2}, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->a(LX/1aD;LX/31T;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x25c491ae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 527642
    check-cast p2, Ljava/lang/Boolean;

    check-cast p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 527643
    iget-object v1, p4, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    move-object v2, v1

    .line 527644
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 527645
    const/16 v1, 0x1f

    const v2, 0x78256887

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 527646
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 527638
    check-cast p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 527639
    iget-object v0, p4, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    move-object v0, v0

    .line 527640
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 527641
    return-void
.end method
