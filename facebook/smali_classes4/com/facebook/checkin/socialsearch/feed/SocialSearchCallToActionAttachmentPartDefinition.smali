.class public Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/BbR;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 528064
    new-instance v0, LX/3BM;

    invoke-direct {v0}, LX/3BM;-><init>()V

    sput-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 528065
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 528066
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 528067
    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 528068
    iput-object p3, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 528069
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 528070
    const-class v1, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    monitor-enter v1

    .line 528071
    :try_start_0
    sget-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 528072
    sput-object v2, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 528073
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528074
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 528075
    new-instance p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 528076
    move-object v0, p0

    .line 528077
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 528078
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528079
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 528080
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 528081
    sget-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 528082
    check-cast p2, LX/BbR;

    .line 528083
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    iget-object v2, p2, LX/BbR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 528084
    const v0, 0x7f0d2d10

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v2, p2, LX/BbR;->b:Landroid/view/View$OnClickListener;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 528085
    const v0, 0x7f0d2d10

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/BbR;->c:Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 528086
    const v0, 0x7f0d2d0f

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/BbR;->d:Ljava/lang/CharSequence;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 528087
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 528088
    const/4 v0, 0x1

    return v0
.end method
