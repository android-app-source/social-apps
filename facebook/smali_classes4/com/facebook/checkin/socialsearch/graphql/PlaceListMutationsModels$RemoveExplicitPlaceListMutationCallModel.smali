.class public final Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x613cf1d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 561702
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 561701
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 561699
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 561700
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 561693
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 561694
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 561695
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 561696
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 561697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 561698
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 561685
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 561686
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 561687
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    .line 561688
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 561689
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;

    .line 561690
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->e:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    .line 561691
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 561692
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 561683
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->e:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->e:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    .line 561684
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->e:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 561680
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;-><init>()V

    .line 561681
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 561682
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 561678
    const v0, 0x6cd53900

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 561679
    const v0, -0x4a969f53

    return v0
.end method
