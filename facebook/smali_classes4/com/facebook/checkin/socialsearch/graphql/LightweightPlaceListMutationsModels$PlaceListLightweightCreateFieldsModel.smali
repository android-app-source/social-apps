.class public final Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7d4e537b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 561441
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 561475
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 561473
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 561474
    return-void
.end method

.method private k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 561471
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    .line 561472
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 561463
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 561464
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 561465
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 561466
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 561467
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 561468
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 561469
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 561470
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 561450
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 561451
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 561452
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    .line 561453
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 561454
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;

    .line 561455
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    .line 561456
    :cond_0
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 561457
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    .line 561458
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 561459
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;

    .line 561460
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    .line 561461
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 561462
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 561449
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 561446
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;-><init>()V

    .line 561447
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 561448
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 561445
    const v0, -0x7d208a2c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 561444
    const v0, -0x3cbfd076

    return v0
.end method

.method public final j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 561442
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    .line 561443
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->e:Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    return-object v0
.end method
