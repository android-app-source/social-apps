.class public final Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x393bb538
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 561750
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 561751
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 561760
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 561761
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 561752
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 561753
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 561754
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 561755
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 561756
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 561757
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 561758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 561759
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 561737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 561738
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 561739
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    .line 561740
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 561741
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;

    .line 561742
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->e:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    .line 561743
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 561744
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    .line 561745
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 561746
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;

    .line 561747
    iput-object v0, v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->f:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    .line 561748
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 561749
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 561728
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->e:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->e:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    .line 561729
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->e:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 561734
    new-instance v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;-><init>()V

    .line 561735
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 561736
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 561730
    const v0, 0x24928691

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 561731
    const v0, -0x2ec6d79e

    return v0
.end method

.method public final j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 561732
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->f:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->f:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    .line 561733
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->f:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    return-object v0
.end method
