.class public Lcom/facebook/multirow/parts/FacepilePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/8Cj;",
        "LX/8Ck;",
        "LX/1PW;",
        "Lcom/facebook/fbui/facepile/FacepileView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final c:LX/1Uo;

.field public final d:LX/1Ad;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 364154
    const-class v0, Lcom/facebook/multirow/parts/FacepilePartDefinition;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 364155
    new-instance v0, LX/8Ci;

    invoke-direct {v0}, LX/8Ci;-><init>()V

    sput-object v0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/res/Resources;LX/1Ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 364149
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 364150
    iput-object p1, p0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 364151
    new-instance v0, LX/1Uo;

    invoke-direct {v0, p2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->c:LX/1Uo;

    .line 364152
    iput-object p3, p0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->d:LX/1Ad;

    .line 364153
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/FacepilePartDefinition;
    .locals 6

    .prologue
    .line 364138
    const-class v1, Lcom/facebook/multirow/parts/FacepilePartDefinition;

    monitor-enter v1

    .line 364139
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 364140
    sput-object v2, Lcom/facebook/multirow/parts/FacepilePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 364141
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364142
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 364143
    new-instance p0, Lcom/facebook/multirow/parts/FacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/multirow/parts/FacepilePartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/res/Resources;LX/1Ad;)V

    .line 364144
    move-object v0, p0

    .line 364145
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 364146
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/FacepilePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364147
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 364148
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/facepile/FacepileView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 364156
    sget-object v0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 364104
    check-cast p2, LX/8Cj;

    .line 364105
    iget-object v0, p2, LX/8Cj;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 364106
    iget-object v0, p0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p2, LX/8Cj;->b:Landroid/view/View$OnClickListener;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 364107
    :cond_0
    iget v1, p2, LX/8Cj;->c:I

    .line 364108
    iget-object v0, p2, LX/8Cj;->a:LX/0Px;

    invoke-static {v0}, Lcom/facebook/fbui/facepile/FacepileView;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 364109
    if-nez v2, :cond_1

    .line 364110
    new-instance v0, LX/8Ck;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, LX/8Ck;-><init>(Ljava/util/List;LX/4Ac;I)V

    .line 364111
    :goto_0
    move-object v0, v0

    .line 364112
    return-object v0

    .line 364113
    :cond_1
    new-instance v3, LX/4Ac;

    invoke-direct {v3}, LX/4Ac;-><init>()V

    .line 364114
    iget-object v0, p0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->d:LX/1Ad;

    sget-object v4, Lcom/facebook/multirow/parts/FacepilePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 364115
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6UY;

    .line 364116
    if-eqz v0, :cond_2

    iget-object v5, v0, LX/6UY;->a:Landroid/net/Uri;

    if-eqz v5, :cond_2

    .line 364117
    iget-object v5, p0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->d:LX/1Ad;

    iget-object v6, v0, LX/6UY;->a:Landroid/net/Uri;

    invoke-virtual {v5, v6}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v5

    invoke-virtual {v5}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v5

    .line 364118
    new-instance v6, LX/1aX;

    iget-object p1, p0, Lcom/facebook/multirow/parts/FacepilePartDefinition;->c:LX/1Uo;

    iget-object p3, p2, LX/8Cj;->d:LX/4Ab;

    .line 364119
    iput-object p3, p1, LX/1Uo;->u:LX/4Ab;

    .line 364120
    move-object p1, p1

    .line 364121
    iget-object p3, p2, LX/8Cj;->g:Landroid/graphics/drawable/Drawable;

    .line 364122
    iput-object p3, p1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 364123
    move-object p1, p1

    .line 364124
    invoke-virtual {p1}, LX/1Uo;->u()LX/1af;

    move-result-object p1

    invoke-direct {v6, p1}, LX/1aX;-><init>(LX/1aY;)V

    .line 364125
    invoke-virtual {v6, v5}, LX/1aX;->a(LX/1aZ;)V

    .line 364126
    invoke-virtual {v6}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, v0, LX/6UY;->b:Landroid/graphics/drawable/Drawable;

    .line 364127
    invoke-virtual {v3, v6}, LX/4Ac;->a(LX/1aX;)V

    goto :goto_1

    .line 364128
    :cond_3
    new-instance v0, LX/8Ck;

    invoke-direct {v0, v2, v3, v1}, LX/8Ck;-><init>(Ljava/util/List;LX/4Ac;I)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x59440482

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 364129
    check-cast p1, LX/8Cj;

    check-cast p2, LX/8Ck;

    check-cast p4, Lcom/facebook/fbui/facepile/FacepileView;

    .line 364130
    iget-object v1, p2, LX/8Ck;->b:Ljava/util/List;

    iget-object v2, p2, LX/8Ck;->a:LX/4Ac;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->a(Ljava/util/List;LX/4Ac;)V

    .line 364131
    iget v1, p2, LX/8Ck;->c:I

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceCountForOverflow(I)V

    .line 364132
    iget-object v1, p1, LX/8Cj;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 364133
    iget-object v1, p1, LX/8Cj;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setHorizontalPadding(I)V

    .line 364134
    :cond_0
    iget-object v1, p1, LX/8Cj;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 364135
    iget-object v1, p1, LX/8Cj;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceSize(I)V

    .line 364136
    :cond_1
    const/16 v1, 0x1f

    const v2, -0x6394a5c7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 364137
    const/4 v0, 0x1

    return v0
.end method
