.class public Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365133
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 365134
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;
    .locals 3

    .prologue
    .line 365135
    const-class v1, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    monitor-enter v1

    .line 365136
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 365137
    sput-object v2, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 365138
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365139
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 365140
    new-instance v0, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;-><init>()V

    .line 365141
    move-object v0, v0

    .line 365142
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 365143
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365144
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 365145
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4cc74dd5    # 1.04492712E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 365146
    check-cast p1, Ljava/lang/CharSequence;

    check-cast p4, Landroid/widget/TextView;

    .line 365147
    if-nez p1, :cond_0

    .line 365148
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 365149
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x36aeb6e9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 365150
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 365151
    invoke-virtual {p4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 365152
    check-cast p4, Landroid/widget/TextView;

    .line 365153
    const/16 v0, 0x8

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 365154
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365155
    return-void
.end method
