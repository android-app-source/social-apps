.class public Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2eV;",
        "Landroid/graphics/drawable/Drawable;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/text/ImageWithTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0wM;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444356
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 444357
    iput-object p1, p0, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->a:Landroid/content/res/Resources;

    .line 444358
    iput-object p2, p0, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->b:LX/0wM;

    .line 444359
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;
    .locals 5

    .prologue
    .line 444360
    const-class v1, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    monitor-enter v1

    .line 444361
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444362
    sput-object v2, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444363
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444364
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444365
    new-instance p0, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-direct {p0, v3, v4}, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;-><init>(Landroid/content/res/Resources;LX/0wM;)V

    .line 444366
    move-object v0, p0

    .line 444367
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444368
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444369
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444370
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 444371
    check-cast p2, LX/2eV;

    .line 444372
    iget v0, p2, LX/2eV;->a:I

    if-nez v0, :cond_0

    .line 444373
    const/4 v0, 0x0

    .line 444374
    :goto_0
    return-object v0

    .line 444375
    :cond_0
    iget-object v0, p2, LX/2eV;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 444376
    iget-object v0, p0, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->b:LX/0wM;

    iget v1, p2, LX/2eV;->a:I

    iget-object v2, p2, LX/2eV;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 444377
    :cond_1
    iget-object v0, p0, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->a:Landroid/content/res/Resources;

    iget v1, p2, LX/2eV;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x42da3cde

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 444378
    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p4, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 444379
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 444380
    const/16 v1, 0x1f

    const v2, -0x24d5873

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
