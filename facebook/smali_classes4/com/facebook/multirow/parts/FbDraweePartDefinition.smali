.class public Lcom/facebook/multirow/parts/FbDraweePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2f9;",
        "LX/1aZ;",
        "TE;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1Ad;

.field private final b:LX/0Uh;

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/1Ad;LX/0Uh;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365204
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 365205
    iput-object p1, p0, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a:LX/1Ad;

    .line 365206
    iput-object p2, p0, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->b:LX/0Uh;

    .line 365207
    iput-object p3, p0, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->c:LX/0ad;

    .line 365208
    return-void
.end method

.method public static a()LX/2f8;
    .locals 1

    .prologue
    .line 365192
    new-instance v0, LX/2f8;

    invoke-direct {v0}, LX/2f8;-><init>()V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .locals 6

    .prologue
    .line 365193
    const-class v1, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    monitor-enter v1

    .line 365194
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 365195
    sput-object v2, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 365196
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365197
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 365198
    new-instance p0, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;-><init>(LX/1Ad;LX/0Uh;LX/0ad;)V

    .line 365199
    move-object v0, p0

    .line 365200
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 365201
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365202
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 365203
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 365182
    check-cast p2, LX/2f9;

    check-cast p3, LX/1Pp;

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 365183
    iget-object v0, p0, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->c:LX/0ad;

    sget-short v2, LX/0fe;->E:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 365184
    :goto_0
    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, LX/1Pu;

    invoke-interface {v0}, LX/1Pu;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/2f9;->a:LX/1bf;

    if-eqz v0, :cond_0

    move-object v0, p3

    .line 365185
    check-cast v0, LX/1Pt;

    iget-object v2, p2, LX/2f9;->a:LX/1bf;

    iget-object v3, p2, LX/2f9;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v2, v3}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 365186
    :cond_0
    iget-object v0, p2, LX/2f9;->a:LX/1bf;

    if-nez v0, :cond_3

    move-object v0, v1

    .line 365187
    :cond_1
    :goto_1
    return-object v0

    .line 365188
    :cond_2
    iget-boolean v0, p2, LX/2f9;->g:Z

    goto :goto_0

    .line 365189
    :cond_3
    iget-object v0, p0, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a:LX/1Ad;

    iget-object v2, p2, LX/2f9;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v2, p2, LX/2f9;->a:LX/1bf;

    invoke-virtual {v0, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 365190
    iget-object v2, p0, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->b:LX/0Uh;

    sget v3, LX/2fA;->a:I

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 365191
    iget-object v2, p2, LX/2f9;->a:LX/1bf;

    iget-object v3, p2, LX/2f9;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v0, v1, v2, v3}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x44373e18

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 365159
    check-cast p1, LX/2f9;

    check-cast p2, LX/1aZ;

    check-cast p4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 365160
    iget v1, p1, LX/2f9;->b:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 365161
    iget v1, p1, LX/2f9;->b:F

    invoke-virtual {p4, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 365162
    :cond_0
    iget-object v1, p1, LX/2f9;->d:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    .line 365163
    iget-object v1, p1, LX/2f9;->d:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 365164
    :cond_1
    iget-object v1, p1, LX/2f9;->h:Landroid/graphics/PointF;

    if-eqz v1, :cond_6

    .line 365165
    invoke-virtual {p4}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    sget-object v2, LX/1Up;->h:LX/1Up;

    invoke-virtual {v1, v2}, LX/1af;->a(LX/1Up;)V

    .line 365166
    invoke-virtual {p4}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    iget-object v2, p1, LX/2f9;->h:Landroid/graphics/PointF;

    invoke-virtual {v1, v2}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 365167
    :cond_2
    :goto_0
    invoke-virtual {p4}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    iget-object v2, p1, LX/2f9;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, LX/1af;->f(Landroid/graphics/drawable/Drawable;)V

    .line 365168
    iget v1, p1, LX/2f9;->k:I

    if-eqz v1, :cond_3

    .line 365169
    invoke-virtual {p4}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    invoke-virtual {p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget p0, p1, LX/2f9;->k:I

    invoke-static {v2, p0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 365170
    :cond_3
    iget v1, p1, LX/2f9;->e:I

    if-eqz v1, :cond_4

    iget v1, p1, LX/2f9;->f:I

    if-eqz v1, :cond_4

    .line 365171
    invoke-virtual {p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 365172
    if-nez v1, :cond_7

    .line 365173
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget v2, p1, LX/2f9;->f:I

    iget p0, p1, LX/2f9;->e:I

    invoke-direct {v1, v2, p0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 365174
    invoke-virtual {p4, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 365175
    :cond_4
    :goto_1
    if-eqz p2, :cond_5

    .line 365176
    invoke-virtual {p4, p2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 365177
    :cond_5
    const/16 v1, 0x1f

    const v2, -0x6bf6b4b5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 365178
    :cond_6
    iget-object v1, p1, LX/2f9;->i:LX/1Up;

    if-eqz v1, :cond_2

    .line 365179
    invoke-virtual {p4}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    iget-object v2, p1, LX/2f9;->i:LX/1Up;

    invoke-virtual {v1, v2}, LX/1af;->a(LX/1Up;)V

    goto :goto_0

    .line 365180
    :cond_7
    iget v2, p1, LX/2f9;->e:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 365181
    iget v2, p1, LX/2f9;->f:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 365156
    check-cast p4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 365157
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 365158
    return-void
.end method
