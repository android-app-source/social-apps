.class public Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445884
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 445885
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;
    .locals 3

    .prologue
    .line 445886
    const-class v1, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    monitor-enter v1

    .line 445887
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 445888
    sput-object v2, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 445889
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445890
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 445891
    new-instance v0, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;-><init>()V

    .line 445892
    move-object v0, v0

    .line 445893
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 445894
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445895
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 445896
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x21254f7d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 445897
    check-cast p1, Ljava/lang/CharSequence;

    .line 445898
    invoke-virtual {p4, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 445899
    const/16 v1, 0x1f

    const v2, 0x27f4bbc0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 445900
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 445901
    return-void
.end method
