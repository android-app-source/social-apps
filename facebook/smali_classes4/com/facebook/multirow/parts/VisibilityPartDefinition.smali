.class public Lcom/facebook/multirow/parts/VisibilityPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 364202
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 364203
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;
    .locals 3

    .prologue
    .line 364191
    const-class v1, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    monitor-enter v1

    .line 364192
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 364193
    sput-object v2, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 364194
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364195
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 364196
    new-instance v0, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;-><init>()V

    .line 364197
    move-object v0, v0

    .line 364198
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 364199
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/VisibilityPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364200
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 364201
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5c5f2c67

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 364188
    check-cast p1, Ljava/lang/Integer;

    .line 364189
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 364190
    const/16 v1, 0x1f

    const v2, 0x6e15656d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 364186
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 364187
    return-void
.end method
