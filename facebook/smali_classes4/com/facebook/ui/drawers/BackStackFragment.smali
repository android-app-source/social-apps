.class public Lcom/facebook/ui/drawers/BackStackFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0fL;

.field public c:Landroid/view/View;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 548543
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 548544
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 548545
    iput-object v0, p0, Lcom/facebook/ui/drawers/BackStackFragment;->a:LX/0Ot;

    .line 548546
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ui/drawers/BackStackFragment;->c:Landroid/view/View;

    .line 548547
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ui/drawers/BackStackFragment;->d:Z

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/ui/drawers/BackStackFragment;

    const/16 p0, 0x271

    invoke-static {v1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p1, Lcom/facebook/ui/drawers/BackStackFragment;->a:LX/0Ot;

    return-void
.end method

.method public static b(Lcom/facebook/ui/drawers/BackStackFragment;)I
    .locals 3

    .prologue
    .line 548538
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 548539
    const-string v1, "argument_stack_container_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 548540
    const-class v0, Lcom/facebook/ui/drawers/BackStackFragment;

    invoke-static {v0, p0}, Lcom/facebook/ui/drawers/BackStackFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 548541
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 548542
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4919d865

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 548526
    invoke-static {p0}, Lcom/facebook/ui/drawers/BackStackFragment;->b(Lcom/facebook/ui/drawers/BackStackFragment;)I

    move-result v1

    .line 548527
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 548528
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Can\'t create a BackStackFragment without a viewId"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2b

    const v3, -0x29214fa1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1

    .line 548529
    :cond_0
    new-instance v2, Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 548530
    invoke-virtual {v2, v1}, Lcom/facebook/widget/CustomFrameLayout;->setId(I)V

    .line 548531
    iget-object v1, p0, Lcom/facebook/ui/drawers/BackStackFragment;->b:LX/0fL;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/ui/drawers/BackStackFragment;->b(Lcom/facebook/ui/drawers/BackStackFragment;)I

    move-result v3

    invoke-virtual {v1, v3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_1

    .line 548532
    iget-object v1, p0, Lcom/facebook/ui/drawers/BackStackFragment;->b:LX/0fL;

    iget-object v3, p0, Lcom/facebook/ui/drawers/BackStackFragment;->b:LX/0fL;

    .line 548533
    iget-object v4, v3, LX/0fM;->c:Landroid/content/Context;

    move-object v3, v4

    .line 548534
    invoke-virtual {v1, v3}, LX/0fL;->b(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/ui/drawers/BackStackFragment;->c:Landroid/view/View;

    .line 548535
    iget-object v1, p0, Lcom/facebook/ui/drawers/BackStackFragment;->c:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 548536
    iget-object v1, p0, Lcom/facebook/ui/drawers/BackStackFragment;->c:Landroid/view/View;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;)V

    .line 548537
    :cond_1
    const v1, 0x302b8b4

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-object v2
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3e665f79

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 548520
    iget-object v1, p0, Lcom/facebook/ui/drawers/BackStackFragment;->b:LX/0fL;

    if-eqz v1, :cond_0

    .line 548521
    iget-object v1, p0, Lcom/facebook/ui/drawers/BackStackFragment;->b:LX/0fL;

    .line 548522
    invoke-virtual {v1}, LX/0fM;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/0fL;->h:LX/10f;

    invoke-virtual {v2}, LX/10f;->shouldLoadWhenIdle()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 548523
    invoke-static {v1}, LX/0fL;->u(LX/0fL;)V

    .line 548524
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 548525
    const/16 v1, 0x2b

    const v2, 0x74eb91d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7fb271dc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 548494
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 548495
    iget-object v1, p0, Lcom/facebook/ui/drawers/BackStackFragment;->b:LX/0fL;

    if-eqz v1, :cond_0

    .line 548496
    iget-object v1, p0, Lcom/facebook/ui/drawers/BackStackFragment;->b:LX/0fL;

    .line 548497
    iget-boolean v2, v1, LX/0fL;->l:Z

    move v2, v2

    .line 548498
    if-eqz v2, :cond_1

    .line 548499
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x487f90e3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 548500
    :cond_1
    iget-object v2, v1, LX/0fL;->h:LX/10f;

    invoke-virtual {v2}, LX/10f;->shouldLoadImmediately()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, LX/0fM;->e()Z

    move-result v2

    if-nez v2, :cond_3

    .line 548501
    :cond_2
    invoke-static {v1}, LX/0fL;->s(LX/0fL;)V

    goto :goto_0

    .line 548502
    :cond_3
    iget-object v2, v1, LX/0fL;->h:LX/10f;

    invoke-virtual {v2}, LX/10f;->shouldLoadWhenIdle()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LX/0fM;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 548503
    invoke-static {v1}, LX/0fL;->t(LX/0fL;)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 548504
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 548505
    iget-object v0, p0, Lcom/facebook/ui/drawers/BackStackFragment;->b:LX/0fL;

    if-eqz v0, :cond_2

    .line 548506
    iget-object v0, p0, Lcom/facebook/ui/drawers/BackStackFragment;->b:LX/0fL;

    iget-boolean v1, p0, Lcom/facebook/ui/drawers/BackStackFragment;->d:Z

    .line 548507
    iget-object p1, v0, LX/0fL;->k:Lcom/facebook/ui/drawers/BackStackFragment;

    if-ne p1, p0, :cond_3

    const/4 p1, 0x1

    :goto_0
    const-string p2, "Unexpected BackStackFragment"

    invoke-static {p1, p2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 548508
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object p1

    iput-object p1, v0, LX/0fL;->j:LX/0gc;

    .line 548509
    iget-object p1, v0, LX/0fL;->j:LX/0gc;

    invoke-virtual {p1, v0}, LX/0gc;->a(LX/0fN;)V

    .line 548510
    if-nez v1, :cond_1

    invoke-virtual {v0}, LX/0fM;->e()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, v0, LX/0fL;->h:LX/10f;

    invoke-virtual {p1}, LX/10f;->shouldLoadImmediately()Z

    move-result p1

    if-nez p1, :cond_1

    .line 548511
    iget-boolean p1, v0, LX/0fM;->b:Z

    move p1, p1

    .line 548512
    if-eqz p1, :cond_0

    iget-object p1, v0, LX/0fL;->h:LX/10f;

    invoke-virtual {p1}, LX/10f;->shouldLoadWhenFocused()Z

    move-result p1

    if-nez p1, :cond_1

    .line 548513
    :cond_0
    iget-boolean p1, v0, LX/0fM;->a:Z

    move p1, p1

    .line 548514
    if-eqz p1, :cond_4

    iget-object p1, v0, LX/0fL;->h:LX/10f;

    invoke-virtual {p1}, LX/10f;->shouldLoadWhenVisible()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 548515
    :cond_1
    invoke-static {v0}, LX/0fL;->s(LX/0fL;)V

    .line 548516
    :cond_2
    :goto_1
    return-void

    .line 548517
    :cond_3
    const/4 p1, 0x0

    goto :goto_0

    .line 548518
    :cond_4
    iget-object p1, v0, LX/0fL;->h:LX/10f;

    invoke-virtual {p1}, LX/10f;->shouldLoadWhenIdle()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {v0}, LX/0fL;->p()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 548519
    invoke-static {v0}, LX/0fL;->t(LX/0fL;)V

    goto :goto_1
.end method
