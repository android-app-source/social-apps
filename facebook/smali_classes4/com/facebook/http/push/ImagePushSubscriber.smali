.class public Lcom/facebook/http/push/ImagePushSubscriber;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static volatile f:Lcom/facebook/http/push/ImagePushSubscriber;


# instance fields
.field public final b:LX/1HI;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/FA8;

.field public final e:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 495778
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "photo-"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/http/push/ImagePushSubscriber;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1HI;Ljava/util/concurrent/ExecutorService;LX/FA8;LX/0Zb;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 495779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495780
    iput-object p1, p0, Lcom/facebook/http/push/ImagePushSubscriber;->b:LX/1HI;

    .line 495781
    iput-object p2, p0, Lcom/facebook/http/push/ImagePushSubscriber;->c:Ljava/util/concurrent/ExecutorService;

    .line 495782
    iput-object p3, p0, Lcom/facebook/http/push/ImagePushSubscriber;->d:LX/FA8;

    .line 495783
    iput-object p4, p0, Lcom/facebook/http/push/ImagePushSubscriber;->e:LX/0Zb;

    .line 495784
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/http/push/ImagePushSubscriber;
    .locals 7

    .prologue
    .line 495785
    sget-object v0, Lcom/facebook/http/push/ImagePushSubscriber;->f:Lcom/facebook/http/push/ImagePushSubscriber;

    if-nez v0, :cond_1

    .line 495786
    const-class v1, Lcom/facebook/http/push/ImagePushSubscriber;

    monitor-enter v1

    .line 495787
    :try_start_0
    sget-object v0, Lcom/facebook/http/push/ImagePushSubscriber;->f:Lcom/facebook/http/push/ImagePushSubscriber;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 495788
    if-eqz v2, :cond_0

    .line 495789
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 495790
    new-instance p0, Lcom/facebook/http/push/ImagePushSubscriber;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v3

    check-cast v3, LX/1HI;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/FA8;->a(LX/0QB;)LX/FA8;

    move-result-object v5

    check-cast v5, LX/FA8;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/http/push/ImagePushSubscriber;-><init>(LX/1HI;Ljava/util/concurrent/ExecutorService;LX/FA8;LX/0Zb;)V

    .line 495791
    move-object v0, p0

    .line 495792
    sput-object v0, Lcom/facebook/http/push/ImagePushSubscriber;->f:Lcom/facebook/http/push/ImagePushSubscriber;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 495793
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 495794
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 495795
    :cond_1
    sget-object v0, Lcom/facebook/http/push/ImagePushSubscriber;->f:Lcom/facebook/http/push/ImagePushSubscriber;

    return-object v0

    .line 495796
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 495797
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 495798
    monitor-enter p0

    .line 495799
    :try_start_0
    iget-object v0, p0, Lcom/facebook/http/push/ImagePushSubscriber;->e:LX/0Zb;

    const-string v1, "android_image_push_subscriber"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 495800
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 495801
    :goto_0
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 495802
    :goto_1
    monitor-exit p0

    return-void

    .line 495803
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/http/push/ImagePushSubscriber;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/http/push/ImagePushSubscriber$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/http/push/ImagePushSubscriber$1;-><init>(Lcom/facebook/http/push/ImagePushSubscriber;Ljava/lang/String;Ljava/lang/String;)V

    const v2, -0x51d87374

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 495804
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 495805
    :cond_1
    const-string v1, "tag"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 495806
    const-string v1, "url"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 495807
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
