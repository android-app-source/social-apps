.class public final Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService$BroadcastReceiver;
.super LX/37V;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadSuperClassBroadcastReceiver.SecureBroadcastReceiver"
    }
.end annotation


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 501141
    invoke-direct {p0}, LX/37V;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService$BroadcastReceiver;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService$BroadcastReceiver;->a:LX/0ad;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, -0x768775ba    # -2.991693E-33f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 501142
    iget-object v1, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService$BroadcastReceiver;->a:LX/0ad;

    if-nez v1, :cond_0

    .line 501143
    invoke-static {p0, p1}, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService$BroadcastReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 501144
    :cond_0
    iget-object v1, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService$BroadcastReceiver;->a:LX/0ad;

    sget-short v2, LX/37W;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 501145
    const/16 v1, 0x27

    const v2, -0x80d9b2c

    invoke-static {p2, v4, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 501146
    :goto_0
    return-void

    .line 501147
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {p1, v1}, LX/37V;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 501148
    const v1, 0x26d4baed

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
