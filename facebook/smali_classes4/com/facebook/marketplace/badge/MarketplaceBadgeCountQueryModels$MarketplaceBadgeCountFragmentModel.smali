.class public final Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4ac06df8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 474007
    const-class v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 474006
    const-class v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 474004
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 474005
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 473996
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 473997
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 473998
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->j()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 473999
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 474000
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 474001
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 474002
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 474003
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 473983
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 473984
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 473985
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    .line 473986
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 473987
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    .line 473988
    iput-object v0, v1, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->e:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    .line 473989
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->j()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 473990
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->j()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    .line 473991
    invoke-virtual {p0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->j()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 473992
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    .line 473993
    iput-object v0, v1, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->f:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    .line 473994
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 473995
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 474008
    iget-object v0, p0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->e:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    iput-object v0, p0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->e:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    .line 474009
    iget-object v0, p0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->e:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 473980
    new-instance v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    invoke-direct {v0}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;-><init>()V

    .line 473981
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 473982
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 473979
    const v0, 0x34abb100

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 473978
    const v0, -0x6747e1ce

    return v0
.end method

.method public final j()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 473976
    iget-object v0, p0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->f:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    iput-object v0, p0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->f:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    .line 473977
    iget-object v0, p0, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->f:Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    return-object v0
.end method
