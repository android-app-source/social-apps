.class public Lcom/facebook/placetips/bootstrap/PresenceSource;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/bootstrap/PresenceSourceDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/bootstrap/PresenceSourceSerializer;
.end annotation


# instance fields
.field public final mAccuracy:Ljava/lang/Float;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "accuracy"
    .end annotation
.end field

.field public final mLatitude:Ljava/lang/Double;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "latitude"
    .end annotation
.end field

.field public final mLongitude:Ljava/lang/Double;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "longitude"
    .end annotation
.end field

.field public final mPresenceSourceType:LX/2cx;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field

.field public final mPulsarRssi:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pulsar_rssi"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 538717
    const-class v0, Lcom/facebook/placetips/bootstrap/PresenceSourceDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 538716
    const-class v0, Lcom/facebook/placetips/bootstrap/PresenceSourceSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 538709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538710
    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mPresenceSourceType:LX/2cx;

    .line 538711
    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mPulsarRssi:Ljava/lang/Integer;

    .line 538712
    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mLatitude:Ljava/lang/Double;

    .line 538713
    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mLongitude:Ljava/lang/Double;

    .line 538714
    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mAccuracy:Ljava/lang/Float;

    .line 538715
    return-void
.end method

.method private constructor <init>(LX/2cx;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;)V
    .locals 0

    .prologue
    .line 538702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538703
    iput-object p1, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mPresenceSourceType:LX/2cx;

    .line 538704
    iput-object p2, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mPulsarRssi:Ljava/lang/Integer;

    .line 538705
    iput-object p3, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mLatitude:Ljava/lang/Double;

    .line 538706
    iput-object p4, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mLongitude:Ljava/lang/Double;

    .line 538707
    iput-object p5, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mAccuracy:Ljava/lang/Float;

    .line 538708
    return-void
.end method

.method public static a(DDLjava/lang/Float;)Lcom/facebook/placetips/bootstrap/PresenceSource;
    .locals 6

    .prologue
    .line 538694
    new-instance v0, Lcom/facebook/placetips/bootstrap/PresenceSource;

    sget-object v1, LX/2cx;->GPS:LX/2cx;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/placetips/bootstrap/PresenceSource;-><init>(LX/2cx;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;)V

    return-object v0
.end method

.method public static a(I)Lcom/facebook/placetips/bootstrap/PresenceSource;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 538701
    new-instance v0, Lcom/facebook/placetips/bootstrap/PresenceSource;

    sget-object v1, LX/2cx;->BLE:LX/2cx;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/placetips/bootstrap/PresenceSource;-><init>(LX/2cx;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;)V

    return-object v0
.end method

.method public static a(LX/2cx;)Lcom/facebook/placetips/bootstrap/PresenceSource;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 538700
    new-instance v0, Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/placetips/bootstrap/PresenceSource;-><init>(LX/2cx;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/2cx;
    .locals 1

    .prologue
    .line 538699
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mPresenceSourceType:LX/2cx;

    return-object v0
.end method

.method public final b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 538698
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mPulsarRssi:Ljava/lang/Integer;

    return-object v0
.end method

.method public final c()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 538697
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mLatitude:Ljava/lang/Double;

    return-object v0
.end method

.method public final d()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 538696
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mLongitude:Ljava/lang/Double;

    return-object v0
.end method

.method public final e()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 538695
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mAccuracy:Ljava/lang/Float;

    return-object v0
.end method
