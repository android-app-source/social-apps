.class public Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;
.super LX/25h;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3FA;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 369879
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "FOREGROUND_LOCATION_AVAILABLE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "FOREGROUND_LOCATION_CHECK_SKIPPED"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "FOREGROUND_LOCATION_CHECK_FAILED"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, LX/25h;-><init>([Ljava/lang/String;)V

    .line 369880
    return-void
.end method

.method private static a(Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;",
            "LX/0Ot",
            "<",
            "LX/3FA;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 369881
    iput-object p1, p0, Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;->b:LX/0ad;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;

    const/16 v1, 0xf55

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0, v1, v0}, Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;->a(Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;LX/0Ot;LX/0ad;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 369882
    const-string v0, "FOREGROUND_LOCATION_AVAILABLE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 369883
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3FA;

    .line 369884
    iget-boolean p0, v0, LX/3FA;->i:Z

    if-nez p0, :cond_3

    .line 369885
    :cond_0
    :goto_0
    return-void

    .line 369886
    :cond_1
    const-string v0, "FOREGROUND_LOCATION_CHECK_SKIPPED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 369887
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3FA;

    .line 369888
    iget-boolean p0, v0, LX/3FA;->i:Z

    if-nez p0, :cond_4

    .line 369889
    :goto_1
    goto :goto_0

    .line 369890
    :cond_2
    const-string v0, "FOREGROUND_LOCATION_CHECK_FAILED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369891
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3FA;

    .line 369892
    iget-boolean p0, v0, LX/3FA;->i:Z

    if-nez p0, :cond_5

    .line 369893
    :goto_2
    goto :goto_0

    .line 369894
    :cond_3
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/3FA;->i:Z

    .line 369895
    iget-object p0, v0, LX/3FA;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/3FB;

    sget-object p1, LX/3FC;->GPS_LOCATION_REPORTED:LX/3FC;

    invoke-virtual {p0, p1}, LX/3FB;->a(LX/3FC;)V

    .line 369896
    invoke-static {v0}, LX/3FA;->e(LX/3FA;)V

    goto :goto_0

    .line 369897
    :cond_4
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/3FA;->i:Z

    .line 369898
    iget-object p0, v0, LX/3FA;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/3FB;

    sget-object p1, LX/3FC;->GPS_LOCATION_CHECK_SKIPPED:LX/3FC;

    invoke-virtual {p0, p1}, LX/3FB;->a(LX/3FC;)V

    .line 369899
    invoke-static {v0}, LX/3FA;->e(LX/3FA;)V

    goto :goto_1

    .line 369900
    :cond_5
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/3FA;->i:Z

    .line 369901
    iget-object p0, v0, LX/3FA;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/3FB;

    sget-object p1, LX/3FC;->GPS_LOCATION_CHECK_FAILED:LX/3FC;

    invoke-virtual {p0, p1}, LX/3FB;->a(LX/3FC;)V

    .line 369902
    const/4 p0, 0x0

    invoke-static {v0, p0}, LX/3FA;->a(LX/3FA;Lcom/facebook/location/ImmutableLocation;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 369903
    invoke-static {p0, p1}, Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 369904
    iget-object v0, p0, Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;->b:LX/0ad;

    sget-short v1, LX/1ST;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369905
    invoke-direct {p0, p3}, Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationReceiver;->b(Ljava/lang/String;)V

    .line 369906
    :cond_0
    return-void
.end method
