.class public Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/35p;


# instance fields
.field public final a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Landroid/graphics/Paint;

.field public g:Z

.field private h:I

.field private i:I

.field private final j:I

.field private final k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 492174
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 492175
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 492176
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 492177
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 492122
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 492123
    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->b:I

    .line 492124
    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->c:I

    .line 492125
    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->d:I

    .line 492126
    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->e:I

    .line 492127
    iput-boolean v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 492128
    const v0, 0x7f030796

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 492129
    const v0, 0x7f0d1445

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 492130
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 492131
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->f:Landroid/graphics/Paint;

    .line 492132
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->f:Landroid/graphics/Paint;

    const v2, 0x7f0a0160

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 492133
    const v1, 0x7f0b00d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->i:I

    .line 492134
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->f:Landroid/graphics/Paint;

    iget v2, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->i:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 492135
    const v1, 0x7f0b00cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->h:I

    .line 492136
    const v1, 0x7f0b0065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->j:I

    .line 492137
    const v1, 0x7f0a09f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->k:I

    .line 492138
    invoke-virtual {p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->b:I

    .line 492139
    invoke-virtual {p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->c:I

    .line 492140
    invoke-virtual {p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->d:I

    .line 492141
    invoke-virtual {p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->getPaddingBottom()I

    move-result v0

    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->e:I

    .line 492142
    invoke-virtual {p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    .line 492143
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 492180
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->b:I

    .line 492181
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->c:I

    .line 492182
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->d:I

    .line 492183
    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    iput v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->e:I

    .line 492184
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 492178
    iget v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->b:I

    iget v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->c:I

    iget v2, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->d:I

    iget v3, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->e:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 492179
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 492152
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 492153
    iput-boolean v5, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 492154
    iget v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->b:I

    iget v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->c:I

    iget v2, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->d:I

    iget v3, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->e:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setPadding(IIII)V

    .line 492155
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 492156
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 492157
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 492158
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 492159
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 492160
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setSelected(Z)V

    .line 492161
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setBackgroundResource(I)V

    .line 492162
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->j:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 492163
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->k:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setTextColor(I)V

    .line 492164
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v4, v4, v4, v4}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 492165
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 492166
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 492167
    iget-boolean v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    if-eqz v0, :cond_0

    .line 492168
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->i:I

    sub-int/2addr v0, v1

    .line 492169
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 492170
    int-to-float v1, v0

    iget v2, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->h:I

    int-to-float v2, v2

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->getHeight()I

    move-result v0

    iget v4, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->h:I

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 492171
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 492172
    :cond_0
    return-void

    .line 492173
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
    .locals 0

    .prologue
    .line 492151
    return-object p0
.end method

.method public getTextView()Lcom/facebook/fbui/widget/text/GlyphWithTextView;
    .locals 1

    .prologue
    .line 492150
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 492147
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->onLayout(ZIIII)V

    .line 492148
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const/4 v1, -0x1

    invoke-static {v0, v1}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 492149
    return-void
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 492144
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v0, v0

    .line 492145
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 492146
    return-void
.end method

.method public setButtonBackgroundResource(I)V
    .locals 1

    .prologue
    .line 492118
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-direct {p0, v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a(Landroid/view/View;)V

    .line 492119
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setBackgroundResource(I)V

    .line 492120
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-direct {p0, v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->b(Landroid/view/View;)V

    .line 492121
    return-void
.end method

.method public setDividerEnabled(Z)V
    .locals 0

    .prologue
    .line 492116
    iput-boolean p1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 492117
    return-void
.end method
