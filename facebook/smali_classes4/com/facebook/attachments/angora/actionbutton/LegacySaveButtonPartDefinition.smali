.class public Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2fn;",
        "LX/AEa;",
        "LX/1PW;",
        "Lcom/facebook/attachments/angora/actionbutton/SaveButton;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field private final a:LX/03V;

.field private final b:LX/0bH;

.field private final c:LX/2fk;

.field private final d:LX/1Sa;

.field private final e:LX/1Sj;

.field private final f:LX/2fh;

.field private final g:LX/14w;

.field private final h:Landroid/content/Context;

.field private final i:LX/1Sl;

.field private final j:LX/0tX;

.field private final k:Ljava/util/concurrent/ExecutorService;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/2fm;

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/2fk;LX/14w;Landroid/content/Context;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;LX/0Ot;)V
    .locals 0
    .param p11    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Sa;",
            "LX/1Sj;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2fh;",
            "LX/0bH;",
            "LX/2fk;",
            "LX/14w;",
            "Landroid/content/Context;",
            "LX/1Sl;",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;",
            "LX/2fm;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 447243
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 447244
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->d:LX/1Sa;

    .line 447245
    iput-object p2, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->e:LX/1Sj;

    .line 447246
    iput-object p3, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->a:LX/03V;

    .line 447247
    iput-object p4, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->f:LX/2fh;

    .line 447248
    iput-object p5, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->b:LX/0bH;

    .line 447249
    iput-object p6, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->c:LX/2fk;

    .line 447250
    iput-object p7, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->g:LX/14w;

    .line 447251
    iput-object p8, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->h:Landroid/content/Context;

    .line 447252
    iput-object p9, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->i:LX/1Sl;

    .line 447253
    iput-object p10, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->j:LX/0tX;

    .line 447254
    iput-object p11, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->k:Ljava/util/concurrent/ExecutorService;

    .line 447255
    iput-object p12, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->l:LX/0Ot;

    .line 447256
    iput-object p13, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->m:LX/2fm;

    .line 447257
    iput-object p14, p0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->n:LX/0Ot;

    .line 447258
    return-void
.end method

.method private a(LX/2fn;)LX/AEa;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2fn;",
            ")",
            "LX/AEa;"
        }
    .end annotation

    .prologue
    .line 447206
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 447207
    const v2, 0x7f0810f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 447208
    const v3, 0x7f0810f8

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 447209
    new-instance v22, LX/AEa;

    move-object/from16 v0, v22

    invoke-direct {v0, v2, v1}, LX/AEa;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 447210
    new-instance v1, LX/AEZ;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->d:LX/1Sa;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->e:LX/1Sj;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->a:LX/03V;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->f:LX/2fh;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->b:LX/0bH;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->c:LX/2fk;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->g:LX/14w;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->h:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->i:LX/1Sl;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->j:LX/0tX;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->k:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->l:LX/0Ot;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->m:LX/2fm;

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2fn;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2fn;->b:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2fn;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2fn;->d:Landroid/view/View$OnClickListener;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->n:LX/0Ot;

    move-object/from16 v20, v0

    move-object/from16 v2, p0

    move-object/from16 v21, p1

    invoke-direct/range {v1 .. v22}, LX/AEZ;-><init>(Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/2fk;LX/14w;Landroid/content/Context;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;LX/0Ot;LX/2fn;LX/AEa;)V

    move-object/from16 v0, v22

    iput-object v1, v0, LX/AEa;->c:LX/2fo;

    .line 447211
    return-object v22
.end method

.method public static a(LX/0QB;)Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;
    .locals 3

    .prologue
    .line 447235
    const-class v1, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;

    monitor-enter v1

    .line 447236
    :try_start_0
    sget-object v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 447237
    sput-object v2, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 447238
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447239
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->b(LX/0QB;)Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 447240
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447241
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 447242
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/AEa;Z)V
    .locals 2

    .prologue
    .line 447221
    iget-object v0, p0, LX/AEa;->c:LX/2fo;

    invoke-virtual {v0}, LX/2fo;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 447222
    iget-object v0, p0, LX/AEa;->d:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->setVisibility(I)V

    .line 447223
    :goto_0
    return-void

    .line 447224
    :cond_0
    iget-object v0, p0, LX/AEa;->d:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    .line 447225
    iget-object v1, v0, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->a:Landroid/widget/ImageView;

    move-object v1, v1

    .line 447226
    if-eqz p1, :cond_1

    const v0, 0x7f02178c

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 447227
    if-eqz p1, :cond_2

    iget-object v0, p0, LX/AEa;->b:Ljava/lang/String;

    .line 447228
    :goto_2
    iget-object v1, p0, LX/AEa;->d:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    .line 447229
    iget-object p1, v1, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->a:Landroid/widget/ImageView;

    move-object v1, p1

    .line 447230
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 447231
    iget-object v0, p0, LX/AEa;->d:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    iget-object v1, p0, LX/AEa;->c:LX/2fo;

    iget-object v1, v1, LX/2fo;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 447232
    iget-object v0, p0, LX/AEa;->d:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->setVisibility(I)V

    goto :goto_0

    .line 447233
    :cond_1
    const v0, 0x7f02178b

    goto :goto_1

    .line 447234
    :cond_2
    iget-object v0, p0, LX/AEa;->a:Ljava/lang/String;

    goto :goto_2
.end method

.method private static b(LX/0QB;)Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;
    .locals 15

    .prologue
    .line 447219
    new-instance v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;

    invoke-static {p0}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v1

    check-cast v1, LX/1Sa;

    invoke-static {p0}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v2

    check-cast v2, LX/1Sj;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/2fh;->a(LX/0QB;)LX/2fh;

    move-result-object v4

    check-cast v4, LX/2fh;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {p0}, LX/2fk;->b(LX/0QB;)LX/2fk;

    move-result-object v6

    check-cast v6, LX/2fk;

    invoke-static {p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v7

    check-cast v7, LX/14w;

    const-class v8, Landroid/content/Context;

    invoke-interface {p0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {p0}, LX/1Sl;->b(LX/0QB;)LX/1Sl;

    move-result-object v9

    check-cast v9, LX/1Sl;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v10

    check-cast v10, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ExecutorService;

    const/16 v12, 0x3279

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const-class v13, LX/2fm;

    invoke-interface {p0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/2fm;

    const/16 v14, 0x327a

    invoke-static {p0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v0 .. v14}, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;-><init>(LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/2fk;LX/14w;Landroid/content/Context;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;LX/0Ot;)V

    .line 447220
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 447218
    check-cast p2, LX/2fn;

    invoke-direct {p0, p2}, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->a(LX/2fn;)LX/AEa;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x522be257

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 447212
    check-cast p1, LX/2fn;

    check-cast p2, LX/AEa;

    check-cast p4, Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    .line 447213
    sget-object v1, LX/1vY;->SAVE_BUTTON:LX/1vY;

    invoke-static {p4, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 447214
    iput-object p4, p2, LX/AEa;->d:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    .line 447215
    iget-object v1, p1, LX/2fn;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {p2, v1}, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->a(LX/AEa;Z)V

    .line 447216
    const/16 v1, 0x1f

    const v2, -0x6a1f4a3f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 447217
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
