.class public Lcom/facebook/attachments/angora/actionbutton/SaveButton;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public a:Landroid/widget/ImageView;

.field public b:Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 447191
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 447192
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 447193
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 447194
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 447195
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 447196
    const v0, 0x7f0302b1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 447197
    const-class v0, Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    invoke-static {v0, p0}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 447198
    const v0, 0x7f0d0996

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->a:Landroid/widget/ImageView;

    .line 447199
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    invoke-static {v0}, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;

    iput-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->b:Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;)V
    .locals 6
    .param p1    # Lcom/facebook/graphql/model/GraphQLNode;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 447200
    new-instance v0, LX/2fn;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/2fn;-><init>(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Z)V

    .line 447201
    new-instance v1, LX/5eI;

    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->b:Lcom/facebook/attachments/angora/actionbutton/LegacySaveButtonPartDefinition;

    sget-object v3, LX/1Qm;->a:LX/1PW;

    invoke-direct {v1, v2, v3}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    .line 447202
    invoke-virtual {v1, v0}, LX/5eI;->a(Ljava/lang/Object;)V

    .line 447203
    invoke-virtual {v1, p0}, LX/5eI;->a(Landroid/view/View;)V

    .line 447204
    return-void
.end method

.method public getButton()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 447205
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->a:Landroid/widget/ImageView;

    return-object v0
.end method
