.class public final Lcom/facebook/attachments/angora/actionbutton/ShareActionButton$ShareActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2yR;


# direct methods
.method public constructor <init>(LX/2yR;)V
    .locals 0

    .prologue
    .line 482002
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/ShareActionButton$ShareActionButtonPartDefinition;->a:LX/2yR;

    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 481982
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481983
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 481984
    if-nez v1, :cond_0

    .line 481985
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/ShareActionButton$ShareActionButtonPartDefinition;->a:LX/2yR;

    iget-object v0, v0, LX/2yR;->a:LX/03V;

    const-class v1, LX/2yR;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "attachment.getParentStory() is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 481986
    const/4 v0, 0x0

    .line 481987
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/AEp;

    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/ShareActionButton$ShareActionButtonPartDefinition;->a:LX/2yR;

    invoke-static {v1}, LX/2yR;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, LX/AEp;-><init>(LX/2yR;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4e071b94

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 481991
    check-cast p2, Landroid/view/View$OnClickListener;

    const/4 p0, 0x0

    .line 481992
    move-object v1, p4

    check-cast v1, LX/35p;

    invoke-interface {v1}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 481993
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v2

    .line 481994
    iget-object p4, v2, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v2, p4

    .line 481995
    invoke-virtual {v1, p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 481996
    iput-boolean p0, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 481997
    invoke-virtual {v2, p0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 481998
    const v1, 0x7f080fd9

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(I)V

    .line 481999
    const v1, 0x7f02079d

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setBackgroundResource(I)V

    .line 482000
    invoke-virtual {v2, p2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 482001
    const/16 v1, 0x1f

    const v2, 0xcfcc46c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 481988
    move-object v0, p4

    check-cast v0, LX/35p;

    invoke-interface {v0}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 481989
    :goto_0
    return-void

    .line 481990
    :cond_0
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    goto :goto_0
.end method
