.class public Lcom/facebook/attachments/angora/InstantArticleIconView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# instance fields
.field public c:LX/0yH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Landroid/graphics/Rect;

.field private final e:Landroid/graphics/drawable/Drawable;

.field private final f:Landroid/graphics/Rect;

.field private g:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 447108
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/attachments/angora/InstantArticleIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 447109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 447077
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/attachments/angora/InstantArticleIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 447078
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 447100
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 447101
    const-class v0, Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-static {v0, p0}, Lcom/facebook/attachments/angora/InstantArticleIconView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 447102
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->d:Landroid/graphics/Rect;

    .line 447103
    iget-object v0, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->c:LX/0yH;

    sget-object v1, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447104
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setVisibility(I)V

    .line 447105
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/attachments/angora/InstantArticleIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020cde

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->e:Landroid/graphics/drawable/Drawable;

    .line 447106
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->f:Landroid/graphics/Rect;

    .line 447107
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/attachments/angora/InstantArticleIconView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-static {v0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v0

    check-cast v0, LX/0yH;

    iput-object v0, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->c:LX/0yH;

    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 447096
    const/16 v0, 0x35

    iget-object v1, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->d:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->f:Landroid/graphics/Rect;

    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v7

    move v5, v4

    invoke-static/range {v0 .. v7}, LX/1uf;->a(IIILandroid/graphics/Rect;IILandroid/graphics/Rect;I)V

    .line 447097
    iget-object v0, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->e:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 447098
    invoke-direct {p0}, Lcom/facebook/attachments/angora/InstantArticleIconView;->d()V

    .line 447099
    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    const v5, 0x3f666666    # 0.9f

    .line 447086
    iget-object v0, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 447087
    iget-object v1, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->d:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 447088
    iget-object v2, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 447089
    iget-object v3, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 447090
    int-to-float v4, v0

    int-to-float v2, v2

    div-float v2, v4, v2

    mul-float/2addr v2, v5

    .line 447091
    int-to-float v1, v1

    int-to-float v3, v3

    div-float/2addr v1, v3

    mul-float/2addr v1, v5

    .line 447092
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    iput-object v3, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->g:Landroid/graphics/Matrix;

    .line 447093
    iget-object v3, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->g:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 447094
    iget-object v1, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->g:Landroid/graphics/Matrix;

    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 447095
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 447079
    invoke-super {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 447080
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v0

    .line 447081
    iget-object v1, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->g:Landroid/graphics/Matrix;

    if-eqz v1, :cond_0

    .line 447082
    iget-object v1, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->g:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 447083
    :cond_0
    iget-object v1, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 447084
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 447085
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 447073
    invoke-super/range {p0 .. p5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onLayout(ZIIII)V

    .line 447074
    iget-object v0, p0, Lcom/facebook/attachments/angora/InstantArticleIconView;->d:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/attachments/angora/InstantArticleIconView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/attachments/angora/InstantArticleIconView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 447075
    invoke-direct {p0}, Lcom/facebook/attachments/angora/InstantArticleIconView;->c()V

    .line 447076
    return-void
.end method
