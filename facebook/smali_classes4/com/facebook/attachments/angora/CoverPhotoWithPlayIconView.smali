.class public Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# instance fields
.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Z

.field private final e:Landroid/graphics/drawable/Drawable;

.field private final f:Landroid/graphics/Rect;

.field private final g:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 492114
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 492115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 492078
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 492079
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 492097
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 492098
    const-class v0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-static {v0, p0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 492099
    invoke-virtual {p0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0214b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->e:Landroid/graphics/drawable/Drawable;

    .line 492100
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->f:Landroid/graphics/Rect;

    .line 492101
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->g:Landroid/graphics/Rect;

    .line 492102
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 492103
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a045d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 492104
    new-instance v2, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    const v3, 0x7f021af6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/16 v4, 0x3e8

    invoke-direct {v2, v3, v4}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 492105
    new-instance v3, LX/1Uo;

    invoke-direct {v3, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 492106
    iput-object v1, v3, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 492107
    move-object v0, v3

    .line 492108
    iput-object v2, v0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 492109
    move-object v0, v0

    .line 492110
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 492111
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 492112
    const v0, 0x3ff745d1

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 492113
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    const/16 v1, 0x1488

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->c:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 492096
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 492091
    invoke-super {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 492092
    iget-boolean v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->d:Z

    move v0, v0

    .line 492093
    if-eqz v0, :cond_0

    .line 492094
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 492095
    :cond_0
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 492086
    invoke-super/range {p0 .. p5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onLayout(ZIIII)V

    .line 492087
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->f:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 492088
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->f:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->g:Landroid/graphics/Rect;

    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v5

    invoke-static/range {v0 .. v5}, LX/1uf;->a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 492089
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->e:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 492090
    return-void
.end method

.method public setAspectRatio(F)V
    .locals 1

    .prologue
    .line 492082
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 492083
    const v0, 0x3ff745d1

    invoke-super {p0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAspectRatio(F)V

    .line 492084
    :goto_0
    return-void

    .line 492085
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAspectRatio(F)V

    goto :goto_0
.end method

.method public setIsPlayIconVisible(Z)V
    .locals 0

    .prologue
    .line 492080
    iput-boolean p1, p0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->d:Z

    .line 492081
    return-void
.end method
