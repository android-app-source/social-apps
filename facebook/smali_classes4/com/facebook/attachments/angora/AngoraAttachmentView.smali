.class public Lcom/facebook/attachments/angora/AngoraAttachmentView;
.super LX/35n;
.source ""

# interfaces
.implements LX/35q;
.implements LX/35r;


# static fields
.field public static final a:LX/1Cz;

.field public static final b:LX/1Cz;


# instance fields
.field private final c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

.field private final e:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private final f:Landroid/graphics/drawable/Drawable;

.field private g:Z

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 492076
    new-instance v0, LX/35s;

    invoke-direct {v0}, LX/35s;-><init>()V

    sput-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    .line 492077
    new-instance v0, LX/35t;

    invoke-direct {v0}, LX/35t;-><init>()V

    sput-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 492074
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 492075
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 492072
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 492073
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 492070
    const v0, 0x7f0300db

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 492071
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 492065
    invoke-direct {p0, p1, p2, p3, p4}, LX/35n;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 492066
    const v0, 0x7f0d052f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    iput-object v0, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    .line 492067
    const v0, 0x7f0d0530

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->e:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 492068
    invoke-virtual {p0}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02106b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->f:Landroid/graphics/drawable/Drawable;

    .line 492069
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 492057
    invoke-super {p0}, LX/35n;->a()V

    .line 492058
    const/4 v0, 0x0

    .line 492059
    iput-boolean v0, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->h:Z

    .line 492060
    invoke-virtual {p0, v2}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 492061
    invoke-virtual {p0, v2}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->setLargeImageController(LX/1aZ;)V

    .line 492062
    invoke-virtual {p0, v1}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->setCoverPhotoPlayIconVisibility(I)V

    .line 492063
    invoke-virtual {p0, v1}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->setSidePhotoPlayIconVisibility(I)V

    .line 492064
    return-void
.end method

.method public setCoverPhotoPlayIconVisibility(I)V
    .locals 2

    .prologue
    .line 492035
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 492036
    :goto_0
    iget-object v1, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    .line 492037
    iput-boolean v0, v1, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->d:Z

    .line 492038
    return-void

    .line 492039
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIsLinkAttachment(Z)V
    .locals 0

    .prologue
    .line 492055
    iput-boolean p1, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->h:Z

    .line 492056
    return-void
.end method

.method public setLargeImageAspectRatio(F)V
    .locals 1

    .prologue
    .line 492053
    iget-object v0, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 492054
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 3
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 492046
    iget-object v2, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;->setVisibility(I)V

    .line 492047
    instance-of v0, p1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->h:Z

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 492048
    check-cast v0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    sget-object v2, LX/397;->LINK:LX/397;

    .line 492049
    invoke-static {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;ILX/397;)V

    .line 492050
    :cond_0
    iget-object v0, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 492051
    return-void

    .line 492052
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setSidePhotoPlayIconVisibility(I)V
    .locals 2

    .prologue
    .line 492040
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 492041
    :goto_0
    iput-boolean v0, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->g:Z

    .line 492042
    iget-object v1, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->e:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->f:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 492043
    return-void

    .line 492044
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 492045
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
