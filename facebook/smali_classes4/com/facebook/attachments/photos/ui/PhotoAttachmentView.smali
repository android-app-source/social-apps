.class public Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;
.super Lcom/facebook/drawee/view/GenericDraweeView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 367649
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 367650
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 367651
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 367652
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 367653
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 367654
    invoke-direct {p0, p1}, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;->a(Landroid/content/Context;)V

    .line 367655
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 367656
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 367657
    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080ff9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 367658
    invoke-virtual {p0, v1}, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 367659
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a045d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 367660
    new-instance v2, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    const v3, 0x7f021af6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/16 v4, 0x3e8

    invoke-direct {v2, v3, v4}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 367661
    new-instance v3, LX/1Uo;

    invoke-direct {v3, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 367662
    iput-object v1, v3, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 367663
    move-object v1, v3

    .line 367664
    const v3, 0x7f020aa6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 367665
    iput-object v0, v1, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 367666
    move-object v0, v1

    .line 367667
    iput-object v2, v0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 367668
    move-object v0, v0

    .line 367669
    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 367670
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 367671
    return-void
.end method
