.class public Lcom/facebook/attachments/photos/ui/PostPostBadge;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Rect;

.field private g:Landroid/graphics/Rect;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Landroid/graphics/Paint;

.field private l:Ljava/lang/String;

.field private final m:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 367746
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 367747
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->l:Ljava/lang/String;

    .line 367748
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->m:Landroid/graphics/Rect;

    .line 367749
    invoke-direct {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->a()V

    .line 367750
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 367741
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 367742
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->l:Ljava/lang/String;

    .line 367743
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->m:Landroid/graphics/Rect;

    .line 367744
    invoke-direct {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->a()V

    .line 367745
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 367736
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 367737
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->l:Ljava/lang/String;

    .line 367738
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->m:Landroid/graphics/Rect;

    .line 367739
    invoke-direct {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->a()V

    .line 367740
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 367709
    const-class v0, Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-static {v0, p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 367710
    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 367711
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 367712
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->e:Landroid/graphics/Paint;

    .line 367713
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->e:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 367714
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->e:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 367715
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->e:Landroid/graphics/Paint;

    const v3, 0x7f0a0048

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 367716
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->e:Landroid/graphics/Paint;

    const v3, 0x7f0b0050

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 367717
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->k:Landroid/graphics/Paint;

    .line 367718
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->k:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 367719
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->k:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 367720
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->k:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 367721
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->k:Landroid/graphics/Paint;

    const v3, 0x7f0a0048

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 367722
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->k:Landroid/graphics/Paint;

    const v3, 0x7f0b004b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 367723
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->f:Landroid/graphics/Rect;

    .line 367724
    const/high16 v2, 0x40800000    # 4.0f

    mul-float/2addr v2, v1

    float-to-int v2, v2

    .line 367725
    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v1

    float-to-int v3, v3

    iput v3, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->i:I

    .line 367726
    const/high16 v3, 0x41800000    # 16.0f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->j:I

    .line 367727
    const v1, 0x7f020ad8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->c:Landroid/graphics/drawable/Drawable;

    .line 367728
    const v1, 0x7f020ad6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->d:Landroid/graphics/drawable/Drawable;

    .line 367729
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    .line 367730
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->d:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 367731
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 367732
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 367733
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 367734
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 367735
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->a:Ljava/lang/Boolean;

    return-void
.end method

.method private setBadgeNumber(I)V
    .locals 5

    .prologue
    .line 367672
    if-eqz p1, :cond_0

    .line 367673
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->l:Ljava/lang/String;

    .line 367674
    :goto_0
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->k:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->l:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->l:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->m:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 367675
    return-void

    .line 367676
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->l:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 6

    .prologue
    .line 367699
    invoke-static {p1}, LX/8x4;->a(I)I

    .line 367700
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->a:Ljava/lang/Boolean;

    invoke-static {v0, p1}, LX/8x8;->a(Ljava/lang/Boolean;I)I

    move-result v0

    .line 367701
    invoke-static {p1}, LX/8x4;->a(I)I

    move-result v1

    .line 367702
    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->h:Ljava/lang/String;

    .line 367703
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->e:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->h:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->h:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 367704
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 367705
    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    .line 367706
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 367707
    invoke-direct {p0, p2}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->setBadgeNumber(I)V

    .line 367708
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 367688
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 367689
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 367690
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    .line 367691
    iget-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->e:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->descent()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    .line 367692
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 367693
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 367694
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 367695
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    .line 367696
    iget-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->k:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    .line 367697
    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 367698
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 367684
    iget-object v0, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->f:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 367685
    iget-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 367686
    invoke-virtual {p0, v0, v1}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->setMeasuredDimension(II)V

    .line 367687
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x2c

    const v1, -0x5039038a

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 367677
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 367678
    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingTop()I

    move-result v1

    sub-int v1, p2, v1

    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->f:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    .line 367679
    div-int/lit8 v2, v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    .line 367680
    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingTop()I

    move-result v3

    add-int/2addr v1, v3

    .line 367681
    iget-object v3, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->d:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->getPaddingRight()I

    move-result v5

    sub-int v5, p1, v5

    sub-int v1, p2, v1

    invoke-virtual {v3, v4, v2, v5, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 367682
    iget-object v1, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->c:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->i:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->j:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->i:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget v5, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->i:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget v6, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->i:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/facebook/attachments/photos/ui/PostPostBadge;->j:I

    add-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 367683
    const/16 v1, 0x2d

    const v2, 0x2e45a6e9

    invoke-static {v7, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
