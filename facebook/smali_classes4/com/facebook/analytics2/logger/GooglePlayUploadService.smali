.class public Lcom/facebook/analytics2/logger/GooglePlayUploadService;
.super LX/2fD;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "AlarmManagerUse",
        "SharedPreferencesUse"
    }
.end annotation


# static fields
.field private static final a:J

.field private static final b:J

.field private static final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static e:Z


# instance fields
.field private d:LX/2Xc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 446293
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->a:J

    .line 446294
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->b:J

    .line 446295
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 446296
    sput-boolean v4, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->e:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 446288
    invoke-direct {p0}, LX/2fD;-><init>()V

    .line 446289
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 446290
    :try_start_0
    const-string v0, "-"

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 446291
    :catch_0
    move-exception v0

    .line 446292
    new-instance v1, LX/403;

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/403;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private a()LX/2Xc;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 446270
    iget-object v0, p0, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->d:LX/2Xc;

    .line 446271
    move-object v0, v0

    .line 446272
    check-cast v0, LX/2Xc;

    return-object v0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 446300
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "com.facebook.analytics2.logger.gms.TRY_SCHEDULE-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 6

    .prologue
    .line 446297
    invoke-static {p0}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/analytics2/logger/GooglePlayUploadService;

    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v0, LX/2E6;->b:Landroid/content/Context;

    invoke-direct {v3, v4, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v1}, LX/2E6;->a(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, LX/2E6;->b(LX/2E6;Ljava/lang/String;)V

    invoke-static {v0}, LX/2E6;->a(LX/2E6;)Landroid/content/Intent;

    move-result-object v4

    if-nez v4, :cond_0

    .line 446298
    :goto_0
    invoke-static {p0, p1}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->b(Landroid/content/Context;I)V

    .line 446299
    return-void

    :cond_0
    const-string v5, "scheduler_action"

    const-string v2, "CANCEL_TASK"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "tag"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "component"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v5, v0, LX/2E6;->b:Landroid/content/Context;

    invoke-virtual {v5, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;ILX/2DI;JJ)V
    .locals 9

    .prologue
    .line 446301
    const-class v2, Lcom/facebook/analytics2/logger/GooglePlayUploadService;

    monitor-enter v2

    const-wide/16 v0, 0x3e8

    :try_start_0
    div-long v4, p3, v0

    .line 446302
    const-wide/16 v0, 0x3e8

    div-long v0, p5, v0

    .line 446303
    cmp-long v3, v4, v0

    if-nez v3, :cond_0

    .line 446304
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    .line 446305
    :cond_0
    new-instance v3, LX/2E9;

    invoke-direct {v3}, LX/2E9;-><init>()V

    const-class v6, Lcom/facebook/analytics2/logger/GooglePlayUploadService;

    invoke-virtual {v3, v6}, LX/2E9;->a(Ljava/lang/Class;)LX/2E9;

    move-result-object v3

    invoke-static {p1}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->b(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, LX/2E9;->c:Ljava/lang/String;

    move-object v3, v3

    .line 446306
    const/4 v6, 0x0

    iput v6, v3, LX/2E9;->a:I

    move-object v3, v3

    .line 446307
    invoke-virtual {v3, v4, v5, v0, v1}, LX/2E9;->a(JJ)LX/2E9;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/2E9;->e:Z

    move-object v1, v0

    .line 446308
    new-instance v0, LX/400;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-direct {v0, v3}, LX/400;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {p2, v0}, LX/2DI;->a(LX/2Ev;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, v1, LX/2E9;->h:Landroid/os/Bundle;

    move-object v0, v1

    .line 446309
    sget-boolean v1, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->e:Z

    iput-boolean v1, v0, LX/2E9;->d:Z

    move-object v0, v0

    .line 446310
    invoke-virtual {v0}, LX/2E9;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v0

    .line 446311
    invoke-static {p0, p1, v0}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->a(Landroid/content/Context;ILcom/google/android/gms/gcm/OneoffTask;)V

    .line 446312
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446313
    monitor-exit v2

    return-void

    .line 446314
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static a(Landroid/content/Context;ILcom/google/android/gms/gcm/OneoffTask;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 446273
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 446274
    invoke-virtual {v0, p0}, LX/1od;->a(Landroid/content/Context;)I

    move-result v1

    .line 446275
    packed-switch v1, :pswitch_data_0

    .line 446276
    sget-object v2, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    .line 446277
    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 446278
    const-string v2, "GooglePlayUploadService"

    const-string v3, "Google Play Services became consistently unavailable after initial check: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/1od;->c(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446279
    :goto_0
    return-void

    .line 446280
    :pswitch_0
    :try_start_0
    invoke-static {p0}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2E6;->a(Lcom/google/android/gms/gcm/Task;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 446281
    :goto_1
    sget-object v0, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 446282
    :catch_0
    move-exception v0

    .line 446283
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p2, Lcom/google/android/gms/gcm/Task;->a:Ljava/lang/String;

    move-object v2, v2

    .line 446284
    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p0, v1, v0}, LX/2EB;->a(Landroid/content/Context;Landroid/content/ComponentName;Ljava/lang/IllegalArgumentException;)V

    goto :goto_1

    .line 446285
    :cond_0
    invoke-virtual {v0, v1}, LX/1od;->c(I)Ljava/lang/String;

    .line 446286
    invoke-static {p0, p1, p2}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->b(Landroid/content/Context;ILcom/google/android/gms/gcm/OneoffTask;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private static b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 446287
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "analytics2-gcm-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 446216
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->c(Landroid/content/Context;ILcom/google/android/gms/gcm/OneoffTask;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x20000000

    invoke-static {p0, v0, v1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 446217
    if-eqz v1, :cond_0

    .line 446218
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 446219
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 446220
    :cond_0
    return-void
.end method

.method private static b(Landroid/content/Context;ILcom/google/android/gms/gcm/OneoffTask;)V
    .locals 8

    .prologue
    .line 446221
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 446222
    const/4 v1, 0x0

    invoke-static {p0, p1, p2}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->c(Landroid/content/Context;ILcom/google/android/gms/gcm/OneoffTask;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {p0, v1, v2, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 446223
    const/4 v2, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sget-wide v6, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->b:J

    add-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 446224
    return-void
.end method

.method private static c(Landroid/content/Context;I)LX/2DI;
    .locals 3

    .prologue
    .line 446225
    invoke-static {p0, p1}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->d(Landroid/content/Context;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 446226
    new-instance v1, LX/2DI;

    new-instance v2, LX/401;

    invoke-direct {v2, v0}, LX/401;-><init>(Landroid/content/SharedPreferences;)V

    invoke-direct {v1, v2}, LX/2DI;-><init>(LX/2Eu;)V

    .line 446227
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 446228
    return-object v1
.end method

.method private static c(Landroid/content/Context;ILcom/google/android/gms/gcm/OneoffTask;)Landroid/content/Intent;
    .locals 2
    .param p2    # Lcom/google/android/gms/gcm/OneoffTask;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 446229
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/analytics2/logger/GooglePlayUploadService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 446230
    if-eqz p2, :cond_0

    .line 446231
    new-instance v1, LX/402;

    invoke-direct {v1, p1, p2}, LX/402;-><init>(ILcom/google/android/gms/gcm/OneoffTask;)V

    invoke-virtual {v1}, LX/402;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 446232
    :cond_0
    return-object v0
.end method

.method private static d(Landroid/content/Context;I)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 446233
    invoke-static {p1}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->b(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/2fH;)I
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 446234
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 446235
    iget-object v2, p1, LX/2fH;->a:Ljava/lang/String;

    move-object v2, v2

    .line 446236
    invoke-static {v2}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->a(Ljava/lang/String;)I

    move-result v3

    .line 446237
    iget-object v2, p1, LX/2fH;->b:Landroid/os/Bundle;

    move-object v2, v2

    .line 446238
    if-eqz v2, :cond_0

    .line 446239
    new-instance v2, LX/2DI;

    iget-object v6, p1, LX/2fH;->b:Landroid/os/Bundle;

    move-object v6, v6

    .line 446240
    invoke-direct {v2, v6}, LX/2DI;-><init>(Landroid/os/Bundle;)V

    .line 446241
    :goto_0
    new-instance v6, LX/2fI;

    invoke-direct {v6}, LX/2fI;-><init>()V

    .line 446242
    invoke-direct {p0}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->a()LX/2Xc;

    move-result-object v7

    invoke-virtual {v7, v3, v2, v6}, LX/2Xc;->a(ILX/2DI;LX/2fJ;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/403; {:try_start_0 .. :try_end_0} :catch_2

    .line 446243
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v4, v8, v4

    .line 446244
    sget-wide v8, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->a:J

    sub-long v4, v8, v4

    invoke-virtual {v6, v4, v5}, LX/2fI;->a(J)Z
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/403; {:try_start_1 .. :try_end_1} :catch_2

    move-result v2

    .line 446245
    if-eqz v2, :cond_1

    .line 446246
    :goto_1
    return v0

    .line 446247
    :cond_0
    :try_start_2
    invoke-static {p0, v3}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->c(Landroid/content/Context;I)LX/2DI;

    move-result-object v2

    goto :goto_0

    :cond_1
    move v0, v1

    .line 446248
    goto :goto_1

    .line 446249
    :catch_0
    invoke-direct {p0}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->a()LX/2Xc;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/2Xc;->a(I)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch LX/403; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 446250
    :catch_1
    move-exception v0

    .line 446251
    :goto_2
    const-string v1, "GooglePlayUploadService"

    const-string v2, "Misunderstood job extras: %s"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 446252
    const/4 v0, 0x2

    goto :goto_1

    .line 446253
    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x3d6fb3c1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 446254
    new-instance v1, LX/2Xc;

    invoke-direct {v1, p0}, LX/2Xc;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->d:LX/2Xc;

    .line 446255
    const/16 v1, 0x25

    const v2, 0x3586b32

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x360aaa07

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 446256
    if-nez p1, :cond_0

    .line 446257
    :try_start_0
    new-instance v1, LX/403;

    const-string v3, "Received a null intent, did you ever return START_STICKY?"

    invoke-direct {v1, v3}, LX/403;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x2

    const/16 v4, 0x25

    const v5, -0x6172af78

    invoke-static {v3, v4, v5, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1
    :try_end_0
    .catch LX/403; {:try_start_0 .. :try_end_0} :catch_0

    .line 446258
    :catch_0
    move-exception v1

    .line 446259
    const-string v3, "GooglePlayUploadService"

    const-string v4, "Unexpected service start parameters: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, LX/403;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-static {v3, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446260
    invoke-virtual {p0, p3}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->stopSelf(I)V

    .line 446261
    const v1, -0x2d31fc46

    invoke-static {v1, v2}, LX/02F;->d(II)V

    :goto_0
    return v0

    .line 446262
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 446263
    const-string v3, "com.facebook.analytics2.logger.gms.TRY_SCHEDULE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 446264
    new-instance v1, LX/402;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {v1, v3}, LX/402;-><init>(Landroid/os/Bundle;)V

    .line 446265
    iget v3, v1, LX/402;->a:I

    iget-object v1, v1, LX/402;->b:Lcom/google/android/gms/gcm/OneoffTask;

    invoke-static {p0, v3, v1}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->a(Landroid/content/Context;ILcom/google/android/gms/gcm/OneoffTask;)V
    :try_end_1
    .catch LX/403; {:try_start_1 .. :try_end_1} :catch_0

    .line 446266
    const v1, -0x1c7be960

    invoke-static {v1, v2}, LX/02F;->d(II)V

    goto :goto_0

    .line 446267
    :cond_1
    :try_start_2
    const-string v3, "com.facebook"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 446268
    invoke-direct {p0}, Lcom/facebook/analytics2/logger/GooglePlayUploadService;->a()LX/2Xc;

    move-result-object v1

    new-instance v3, LX/2WW;

    invoke-direct {v3, p0, p3}, LX/2WW;-><init>(Landroid/app/Service;I)V

    invoke-virtual {v1, p1, v3}, LX/2Xc;->a(Landroid/content/Intent;LX/2WW;)I
    :try_end_2
    .catch LX/403; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    const v1, 0x1b33a427

    invoke-static {v1, v2}, LX/02F;->d(II)V

    goto :goto_0

    .line 446269
    :cond_2
    :try_start_3
    invoke-super {p0, p1, p2, p3}, LX/2fD;->onStartCommand(Landroid/content/Intent;II)I
    :try_end_3
    .catch LX/403; {:try_start_3 .. :try_end_3} :catch_0

    move-result v0

    const v1, -0xdb38393

    invoke-static {v1, v2}, LX/02F;->d(II)V

    goto :goto_0
.end method
