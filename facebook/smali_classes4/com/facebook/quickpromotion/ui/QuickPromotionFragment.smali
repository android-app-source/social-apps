.class public abstract Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

.field private c:LX/0gr;

.field public d:LX/13A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/78A;

.field private f:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private h:Ljava/lang/String;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 450420
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 450421
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    const-class p0, LX/13A;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/13A;

    iput-object v1, p1, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->d:LX/13A;

    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 450422
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 450423
    if-nez v0, :cond_1

    .line 450424
    :cond_0
    :goto_0
    return-void

    .line 450425
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    if-lez v1, :cond_2

    .line 450426
    invoke-static {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->s(Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;)V

    goto :goto_0

    .line 450427
    :cond_2
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-nez v1, :cond_0

    .line 450428
    new-instance v1, LX/77x;

    invoke-direct {v1, p0}, LX/77x;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;)V

    iput-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 450429
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public static s(Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;)V
    .locals 2

    .prologue
    .line 450430
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    invoke-virtual {v0}, LX/78A;->a()V

    .line 450431
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->b()LX/77n;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/78A;->a(LX/77n;)V

    .line 450432
    return-void
.end method


# virtual methods
.method public a(LX/77y;Z)V
    .locals 1

    .prologue
    .line 450433
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->c:LX/0gr;

    if-eqz v0, :cond_0

    .line 450434
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->c:LX/0gr;

    invoke-interface {v0, p2}, LX/0gr;->c(Z)V

    .line 450435
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 450436
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 450437
    const-class v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-static {v0, p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 450438
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 450439
    const-string v0, "qp_definition"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 450440
    const-string v0, "qp_creative"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    .line 450441
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    if-nez v0, :cond_0

    .line 450442
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    .line 450443
    :cond_0
    const-string v0, "qp_trigger"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->f:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 450444
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    const-string v1, "A QuickPromotionDefinition object must be passed via arguments using the key \'qp_definition\'"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450445
    return-void
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 450446
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 450447
    const-string v1, "qp_definition"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 450448
    const-string v1, "qp_definition"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 450449
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()LX/77n;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 450450
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 450416
    const/4 v0, 0x1

    return v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 450417
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    invoke-virtual {v0}, LX/78A;->b()V

    .line 450418
    sget-object v0, LX/77y;->PRIMARY:LX/77y;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    invoke-virtual {v1}, LX/78A;->e()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(LX/77y;Z)V

    .line 450419
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 450413
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    invoke-virtual {v0}, LX/78A;->f()V

    .line 450414
    sget-object v0, LX/77y;->SECONDARY:LX/77y;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    invoke-virtual {v1}, LX/78A;->h()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(LX/77y;Z)V

    .line 450415
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0x628ced4

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 450407
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 450408
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 450409
    const-string v2, "qp_controller_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->h:Ljava/lang/String;

    .line 450410
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->h:Ljava/lang/String;

    const-string v2, "The controller id must be passed in for logging"

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450411
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->d:LX/13A;

    iget-object v2, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v3, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v5, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->f:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/13A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/78A;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    .line 450412
    const/16 v1, 0x2b

    const v2, -0x76817fbd

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 450404
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 450405
    const-class v0, LX/0gr;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gr;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->c:LX/0gr;

    .line 450406
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7aec027a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 450398
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v1, :cond_0

    .line 450399
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 450400
    iget-object v2, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v1, v2}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 450401
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 450402
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 450403
    const/16 v1, 0x2b

    const v2, -0x5ffe8b48

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetach()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3355bb12    # -8.9270128E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 450395
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDetach()V

    .line 450396
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->c:LX/0gr;

    .line 450397
    const/16 v1, 0x2b

    const v2, 0x5fc92892    # 2.8989992E19f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7dff981a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 450390
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 450391
    iget-boolean v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->i:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 450392
    invoke-direct {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->k()V

    .line 450393
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->i:Z

    .line 450394
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x49bafb1e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 450387
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    invoke-virtual {v0}, LX/78A;->i()V

    .line 450388
    sget-object v0, LX/77y;->XOUT:LX/77y;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->e:LX/78A;

    invoke-virtual {v1}, LX/78A;->j()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(LX/77y;Z)V

    .line 450389
    return-void
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 450384
    invoke-static {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->s(Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;)V

    .line 450385
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->i:Z

    .line 450386
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 450377
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    .line 450378
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 450379
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v1, v1

    .line 450380
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    if-eq v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->i:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450381
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->i:Z

    .line 450382
    invoke-direct {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->k()V

    .line 450383
    :cond_0
    return-void
.end method
