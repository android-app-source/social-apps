.class public Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;
.super Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 450612
    const-class v0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;

    const-string v1, "quick_promotion_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 450613
    invoke-direct {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;

    invoke-static {p0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    move-result-object v1

    check-cast v1, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object v1, p1, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iput-object p0, p1, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->b:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 450614
    invoke-super {p0, p1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/os/Bundle;)V

    .line 450615
    const-class v0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;

    invoke-static {v0, p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 450616
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-object v0, v0

    .line 450617
    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    .line 450618
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5368cce9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 450619
    const v1, 0x7f0310d2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x784b9c9b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 450620
    invoke-super {p0, p1, p2}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 450621
    const v0, 0x7f0d27f0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 450622
    const v1, 0x7f0d27f1

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 450623
    const v2, 0x7f0d27f2

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 450624
    const v3, 0x7f0d27f3

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 450625
    iget-object v4, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v4, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450626
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450627
    new-instance v1, LX/788;

    invoke-direct {v1, p0}, LX/788;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450628
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v1, :cond_0

    .line 450629
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->b:LX/0wM;

    const v2, 0x7f021572

    const/4 v4, -0x1

    invoke-virtual {v1, v2, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 450630
    new-instance v1, LX/789;

    invoke-direct {v1, p0}, LX/789;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;)V

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450631
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 450632
    :cond_0
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iget-object v2, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    sget-object v3, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v4}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a()LX/1Ai;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/common/callercontext/CallerContext;LX/1Ai;)Z

    move-result v1

    .line 450633
    if-eqz v1, :cond_1

    .line 450634
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    invoke-static {v1, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 450635
    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 450636
    :cond_1
    return-void
.end method
