.class public Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/13A;

.field private final c:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 560241
    const-class v0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;

    const-string v1, "quick_promotion_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/13A;Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 560237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560238
    iput-object p1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->b:LX/13A;

    .line 560239
    iput-object p2, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->c:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    .line 560240
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 11

    .prologue
    .line 560188
    invoke-virtual {p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v9

    .line 560189
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->b:LX/13A;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, v9, v1}, LX/13A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/78A;

    move-result-object v3

    .line 560190
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 560191
    const v1, 0x7f0310c7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 560192
    const v0, 0x7f0d00bd

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 560193
    const v0, 0x7f0d1286

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 560194
    const v0, 0x7f0d27eb

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 560195
    const v1, 0x7f0d02c4

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 560196
    const v1, 0x7f0d0cbc

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 560197
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 560198
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 560199
    const/16 v1, 0x8

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 560200
    :goto_0
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {v6, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 560201
    new-instance v1, LX/77o;

    invoke-direct {v1, p0, v3, p4}, LX/77o;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;LX/78A;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v6, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 560202
    iget-object v10, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 560203
    if-eqz v10, :cond_3

    iget-object v1, v10, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    move v8, v1

    .line 560204
    :goto_1
    if-eqz v8, :cond_4

    .line 560205
    iget-object v1, v10, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {v7, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 560206
    new-instance v1, LX/77p;

    invoke-direct {v1, p0, v3, p4}, LX/77p;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;LX/78A;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v7, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 560207
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 560208
    const v1, 0x7f0d27ec

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 560209
    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->setMeasureWithLargestChildEnabled(Z)V

    .line 560210
    :goto_2
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 560211
    if-nez v1, :cond_0

    if-nez v8, :cond_5

    .line 560212
    :cond_0
    new-instance v1, LX/77q;

    invoke-direct {v1, p0, v3, p4}, LX/77q;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;LX/78A;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 560213
    new-instance v1, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory$4;

    invoke-direct {v1, p0, v0, v2}, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory$4;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;Landroid/widget/Button;Landroid/view/View;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 560214
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 560215
    :goto_3
    const v0, 0x7f0d0340

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 560216
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->c:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v8, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v10, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->c:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v10}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a()LX/1Ai;

    move-result-object v10

    invoke-virtual {v0, v1, v9, v8, v10}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/common/callercontext/CallerContext;LX/1Ai;)Z

    move-result v0

    .line 560217
    if-eqz v0, :cond_6

    .line 560218
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v9, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 560219
    sget-object v0, LX/76S;->ANY:LX/76S;

    invoke-static {v9, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    move-result-object v0

    .line 560220
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->c:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v1, v0, v9}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v1

    .line 560221
    iget-object v8, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->c:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v8, v0, v9}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v0

    .line 560222
    iget-object v8, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .line 560223
    iput v1, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 560224
    iput v0, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 560225
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 560226
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 560227
    :goto_4
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v8

    new-instance v0, LX/77r;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/77r;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;Landroid/view/View;LX/78A;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/Button;Landroid/widget/Button;)V

    invoke-virtual {v8, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 560228
    return-object v2

    .line 560229
    :cond_1
    invoke-virtual {v4}, Landroid/widget/TextView;->getLineCount()I

    move-result v1

    const/4 v8, 0x2

    if-lt v1, v8, :cond_2

    .line 560230
    const/4 v1, 0x2

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 560231
    :cond_2
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 560232
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 560233
    :cond_3
    const/4 v1, 0x0

    move v8, v1

    goto/16 :goto_1

    .line 560234
    :cond_4
    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_2

    .line 560235
    :cond_5
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3

    .line 560236
    :cond_6
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_4
.end method
