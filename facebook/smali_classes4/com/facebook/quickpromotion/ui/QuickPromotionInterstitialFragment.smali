.class public Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;
.super Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/76b;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/1Ai;

.field private final g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/ImageButton;

.field private k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/fbui/facepile/FacepileView;

.field private q:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public r:Landroid/widget/LinearLayout;

.field private s:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public t:I

.field public u:I

.field public v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

.field private w:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 450787
    const-class v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    const-string v1, "quick_promotion_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 450781
    invoke-direct {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;-><init>()V

    .line 450782
    new-instance v0, LX/77z;

    invoke-direct {v0, p0}, LX/77z;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 450783
    iput v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->t:I

    .line 450784
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->u:I

    .line 450785
    iput-boolean v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->w:Z

    .line 450786
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-static {p0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    move-result-object v1

    check-cast v1, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-static {p0}, LX/76b;->b(LX/0QB;)LX/76b;

    move-result-object v2

    check-cast v2, LX/76b;

    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object p0

    check-cast p0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object v1, p1, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iput-object v2, p1, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->b:LX/76b;

    iput-object p0, p1, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->c:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    return-void
.end method

.method public static v(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 450780
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->h:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v1

    if-gt v1, v0, :cond_0

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->i:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v1

    if-le v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static w(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 450769
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->r:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 450770
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->r:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->h:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 450771
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->r:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->h:Landroid/widget/Button;

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 450772
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->h:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 450773
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 450774
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->h:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 450775
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->i:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 450776
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 450777
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 450778
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->i:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 450779
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 450788
    invoke-super {p0, p1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/os/Bundle;)V

    .line 450789
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 450790
    const-class v0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;

    invoke-static {v0, p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 450791
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-object v0, v0

    .line 450792
    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->s:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 450793
    const-string v0, "qp_creative"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    .line 450794
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    if-nez v0, :cond_0

    .line 450795
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->s:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    .line 450796
    :goto_0
    return-void

    .line 450797
    :cond_0
    const-string v0, "page_position"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->t:I

    .line 450798
    const-string v0, "num_pages"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->u:I

    .line 450799
    const-string v0, "is_multi"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->w:Z

    goto :goto_0
.end method

.method public final b()LX/77n;
    .locals 2

    .prologue
    .line 450753
    new-instance v0, LX/77n;

    invoke-direct {v0}, LX/77n;-><init>()V

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->l:Landroid/widget/TextView;

    invoke-static {v1}, LX/191;->b(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    .line 450754
    iput-object v1, v0, LX/77n;->a:Ljava/lang/String;

    .line 450755
    move-object v0, v0

    .line 450756
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->m:Landroid/widget/TextView;

    invoke-static {v1}, LX/191;->b(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    .line 450757
    iput-object v1, v0, LX/77n;->b:Ljava/lang/String;

    .line 450758
    move-object v0, v0

    .line 450759
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->h:Landroid/widget/Button;

    invoke-static {v1}, LX/191;->b(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    .line 450760
    iput-object v1, v0, LX/77n;->c:Ljava/lang/String;

    .line 450761
    move-object v0, v0

    .line 450762
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->i:Landroid/widget/Button;

    invoke-static {v1}, LX/191;->b(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    .line 450763
    iput-object v1, v0, LX/77n;->d:Ljava/lang/String;

    .line 450764
    move-object v0, v0

    .line 450765
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->n:Landroid/widget/TextView;

    invoke-static {v1}, LX/191;->b(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    .line 450766
    iput-object v1, v0, LX/77n;->e:Ljava/lang/String;

    .line 450767
    move-object v0, v0

    .line 450768
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 450752
    iget-boolean v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->w:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x8

    const/4 v2, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v3, -0x409f0e67

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 450637
    invoke-super {p0, p1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 450638
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450639
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 450640
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 450641
    :goto_0
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    sget-object v5, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->e:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v6, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->f:LX/1Ai;

    invoke-virtual {v0, v1, v4, v5, v6}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/common/callercontext/CallerContext;LX/1Ai;)Z

    move-result v0

    .line 450642
    if-eqz v0, :cond_6

    .line 450643
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0, v1}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 450644
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    .line 450645
    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->MESSENGER_CARD:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {v1, v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 450646
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    sget-object v1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v1}, LX/1af;->a(LX/1Up;)V

    .line 450647
    :goto_1
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    sget-object v1, LX/76S;->ANY:LX/76S;

    invoke-static {v0, v1}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    move-result-object v0

    .line 450648
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iget-object v4, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    invoke-virtual {v1, v0, v4}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v0

    .line 450649
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 450650
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 450651
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 450652
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 450653
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->m:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0c0033

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 450654
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 450655
    :goto_2
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 450656
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->h:Landroid/widget/Button;

    new-instance v1, LX/781;

    invoke-direct {v1, p0}, LX/781;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450657
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 450658
    if-eqz v0, :cond_8

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    move v1, v0

    .line 450659
    :goto_3
    if-eqz v1, :cond_9

    .line 450660
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->i:Landroid/widget/Button;

    iget-object v4, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v4, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v4, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 450661
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->i:Landroid/widget/Button;

    new-instance v4, LX/782;

    invoke-direct {v4, p0}, LX/782;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450662
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 450663
    :cond_0
    :goto_4
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->f()LX/0Rf;

    move-result-object v0

    sget-object v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Attribute;->IS_UNCANCELABLE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Attribute;

    invoke-virtual {v0, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 450664
    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-nez v0, :cond_1

    if-nez v1, :cond_a

    .line 450665
    :cond_1
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->j:Landroid/widget/ImageButton;

    new-instance v1, LX/783;

    invoke-direct {v1, p0}, LX/783;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450666
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 450667
    :goto_6
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    if-eqz v0, :cond_2

    .line 450668
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 450669
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 450670
    :goto_7
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->friendIds:LX/0Px;

    .line 450671
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 450672
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v1, v8}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 450673
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 450674
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->b:LX/76b;

    new-instance v4, LX/784;

    invoke-direct {v4, p0}, LX/784;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V

    .line 450675
    iput-object v4, v1, LX/76b;->g:LX/3Mb;

    .line 450676
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->b:LX/76b;

    invoke-virtual {v1, v0}, LX/76b;->a(Ljava/util/List;)V

    .line 450677
    :cond_2
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 450678
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->footer:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 450679
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 450680
    :cond_3
    :goto_8
    const v0, 0x4c1e1914    # 4.1444432E7f

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-void

    .line 450681
    :cond_4
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 450682
    :cond_5
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    sget-object v1, LX/1Up;->f:LX/1Up;

    invoke-virtual {v0, v1}, LX/1af;->a(LX/1Up;)V

    goto/16 :goto_1

    .line 450683
    :cond_6
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    if-nez v0, :cond_7

    .line 450684
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0c0032

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 450685
    :goto_9
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->m:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 450686
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_2

    .line 450687
    :cond_7
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0c0031

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    goto :goto_9

    :cond_8
    move v1, v2

    .line 450688
    goto/16 :goto_3

    .line 450689
    :cond_9
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->i:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 450690
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->q:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450691
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->q:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    .line 450692
    :cond_a
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_6

    .line 450693
    :cond_b
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450694
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 450695
    :cond_c
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->footer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450696
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020839

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 450697
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b0e7e

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v1, v2, v2, v0, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 450698
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v8, v1, v8, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 450699
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_5
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x6992672f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 450713
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v2, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    .line 450714
    sget-object v0, LX/785;->a:[I

    invoke-virtual {v2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 450715
    const v0, 0x7f0310ca

    .line 450716
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 450717
    const v0, 0x7f0d27e5

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->h:Landroid/widget/Button;

    .line 450718
    const v0, 0x7f0d27e4

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->i:Landroid/widget/Button;

    .line 450719
    const v0, 0x7f0d27e6

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->j:Landroid/widget/ImageButton;

    .line 450720
    const v0, 0x7f0d02c4

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->l:Landroid/widget/TextView;

    .line 450721
    const v0, 0x7f0d0cbc

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->m:Landroid/widget/TextView;

    .line 450722
    const v0, 0x7f0d056b

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->n:Landroid/widget/TextView;

    .line 450723
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 450724
    const v0, 0x7f0d27e2

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    .line 450725
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 450726
    const v0, 0x7f0d1c4b

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->r:Landroid/widget/LinearLayout;

    .line 450727
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 450728
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->MESSENGER_CARD:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {v0, v2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 450729
    const v0, 0x7f0d27e9

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 450730
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/facepile/FacepileView;->setShowRoundFaces(Z)V

    .line 450731
    :goto_1
    new-instance v0, LX/780;

    invoke-direct {v0, p0}, LX/780;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;)V

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->f:LX/1Ai;

    .line 450732
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->CARD_WITH_HEADER:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {v0, v2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 450733
    const/4 v2, 0x0

    .line 450734
    const v0, 0x7f0d27e0

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 450735
    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    const-string v4, "color_scheme"

    invoke-virtual {v0, v4}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 450736
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    const-string v4, "color_scheme"

    invoke-virtual {v0, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 450737
    const/4 v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result p2

    sparse-switch p2, :sswitch_data_0

    :cond_0
    move v0, v4

    :goto_2
    packed-switch v0, :pswitch_data_1

    .line 450738
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 450739
    :cond_1
    :goto_3
    const v0, 0x7f0d27ed

    invoke-static {v3, v0}, LX/0jc;->a(Landroid/view/View;I)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->q:LX/0am;

    .line 450740
    const v0, 0x7f0d27e3

    invoke-static {v3, v0}, LX/0jc;->a(Landroid/view/View;I)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->o:LX/0am;

    .line 450741
    const-string v2, "quick_promotion_interstitial"

    invoke-static {v3, v2, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 450742
    const v0, -0x10aea7fb

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v3

    .line 450743
    :pswitch_0
    const v0, 0x7f0310c6

    goto/16 :goto_0

    .line 450744
    :pswitch_1
    const v0, 0x7f0310c3

    goto/16 :goto_0

    .line 450745
    :pswitch_2
    const v0, 0x7f0310c8    # 1.74216E38f

    goto/16 :goto_0

    .line 450746
    :pswitch_3
    const v0, 0x7f0310ce

    goto/16 :goto_0

    .line 450747
    :cond_2
    const v0, 0x7f0d27e1

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    goto/16 :goto_1

    .line 450748
    :sswitch_0
    const-string p2, "yellow"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_2

    :sswitch_1
    const-string p2, "clear"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_2

    .line 450749
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 450750
    invoke-virtual {v0}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p2, 0x7f0a0573

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 450751
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        -0x2bc39b8c -> :sswitch_0
        0x5a5b64d -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x614820a4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 450706
    invoke-super {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->onDestroy()V

    .line 450707
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->f:LX/1Ai;

    .line 450708
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->b:LX/76b;

    .line 450709
    iget-object v2, v1, LX/76b;->a:LX/1Mv;

    if-eqz v2, :cond_0

    .line 450710
    iget-object v2, v1, LX/76b;->a:LX/1Mv;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, LX/1Mv;->a(Z)V

    .line 450711
    const/4 v2, 0x0

    iput-object v2, v1, LX/76b;->a:LX/1Mv;

    .line 450712
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x6b9e640f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 450700
    invoke-super {p0, p1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->setUserVisibleHint(Z)V

    .line 450701
    iget-boolean v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->w:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    if-eqz v0, :cond_0

    .line 450702
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0}, Lcom/facebook/fbui/facepile/FacepileView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 450703
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setWillNotDraw(Z)V

    .line 450704
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialFragment;->p:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0}, Lcom/facebook/fbui/facepile/FacepileView;->postInvalidate()V

    .line 450705
    :cond_0
    return-void
.end method
