.class public Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;
.super Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private k:LX/1Ai;

.field private l:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 450606
    const-class v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    const-string v1, "quick_promotion_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 450604
    invoke-direct {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;-><init>()V

    .line 450605
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-static {p0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    move-result-object v1

    check-cast v1, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v2

    check-cast v2, LX/0wM;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v1, p1, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iput-object v2, p1, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->b:LX/0wM;

    iput-object p0, p1, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->c:LX/0ad;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 450607
    invoke-super {p0, p1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/os/Bundle;)V

    .line 450608
    const-class v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-static {v0, p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 450609
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-object v0, v0

    .line 450610
    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->l:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    .line 450611
    return-void
.end method

.method public final b()LX/77n;
    .locals 2

    .prologue
    .line 450594
    new-instance v0, LX/77n;

    invoke-direct {v0}, LX/77n;-><init>()V

    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->h:Landroid/widget/TextView;

    invoke-static {v1}, LX/191;->b(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    .line 450595
    iput-object v1, v0, LX/77n;->a:Ljava/lang/String;

    .line 450596
    move-object v0, v0

    .line 450597
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->i:Landroid/widget/TextView;

    invoke-static {v1}, LX/191;->b(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    .line 450598
    iput-object v1, v0, LX/77n;->b:Ljava/lang/String;

    .line 450599
    move-object v0, v0

    .line 450600
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->f:Landroid/widget/TextView;

    invoke-static {v1}, LX/191;->b(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    .line 450601
    iput-object v1, v0, LX/77n;->c:Ljava/lang/String;

    .line 450602
    move-object v0, v0

    .line 450603
    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v3, 0x2

    const/4 v6, 0x0

    const/16 v0, 0x2a

    const v1, -0x33d3e830    # -4.5113152E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 450572
    invoke-super {p0, p1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 450573
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 450574
    if-eqz v1, :cond_0

    .line 450575
    new-instance v2, LX/77t;

    invoke-direct {v2, p0}, LX/77t;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 450576
    :cond_0
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->h:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->l:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450577
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->l:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 450578
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 450579
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 450580
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 450581
    :goto_0
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->l:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450582
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->f:Landroid/widget/TextView;

    new-instance v2, LX/77u;

    invoke-direct {v2, p0}, LX/77u;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450583
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->l:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v1, :cond_1

    .line 450584
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->g:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->b:LX/0wM;

    const v3, 0x7f021572

    const v4, -0x6e685d

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 450585
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 450586
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->g:Landroid/widget/ImageView;

    new-instance v2, LX/77v;

    invoke-direct {v2, p0}, LX/77v;-><init>(Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450587
    :cond_1
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iget-object v2, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->l:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    sget-object v4, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->e:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v5, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->k:LX/1Ai;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/common/callercontext/CallerContext;LX/1Ai;)Z

    move-result v1

    .line 450588
    if-eqz v1, :cond_3

    .line 450589
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->l:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v2, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1, v2}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 450590
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 450591
    :goto_1
    const v1, -0x64b183f8    # -1.7079997E-22f

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 450592
    :cond_2
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->i:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->l:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 450593
    :cond_3
    iget-object v1, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x77d4e668    # 8.636251E33f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 450554
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->c:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/2tg;->a:S

    invoke-interface {v0, v2, v3, v4}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0310d1

    .line 450555
    :goto_0
    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 450556
    const v0, 0x7f0d02c4

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->h:Landroid/widget/TextView;

    .line 450557
    const v0, 0x7f0d0cbc

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->i:Landroid/widget/TextView;

    .line 450558
    const v0, 0x7f0d00bd

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->f:Landroid/widget/TextView;

    .line 450559
    const v0, 0x7f0d0632

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->g:Landroid/widget/ImageView;

    .line 450560
    const v0, 0x7f0d0340

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 450561
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->a:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a()LX/1Ai;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->k:LX/1Ai;

    .line 450562
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 450563
    if-nez v0, :cond_2

    .line 450564
    sget-object v0, LX/77w;->PRIMARY:LX/77w;

    .line 450565
    :cond_0
    :goto_1
    move-object v0, v0

    .line 450566
    iget-object v3, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->f:Landroid/widget/TextView;

    iget v4, v0, LX/77w;->backgroundResId:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 450567
    iget-object v3, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v0, v0, LX/77w;->textColorResId:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 450568
    const v0, 0x143e0d66

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 450569
    :cond_1
    const v0, 0x7f0310c9

    goto :goto_0

    .line 450570
    :cond_2
    const-string v3, "ACTION_BUTTON_THEME_ARG"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/77w;

    .line 450571
    if-nez v0, :cond_0

    sget-object v0, LX/77w;->PRIMARY:LX/77w;

    goto :goto_1
.end method
