.class public Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile e:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public final c:LX/1HI;

.field private final d:LX/1Ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 560242
    const-class v0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    const-string v1, "quick_promotion_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1HI;LX/1Ad;Landroid/content/res/Resources;)V
    .locals 0
    .param p3    # Landroid/content/res/Resources;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 560243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560244
    iput-object p1, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->c:LX/1HI;

    .line 560245
    iput-object p2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->d:LX/1Ad;

    .line 560246
    iput-object p3, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    .line 560247
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
    .locals 6

    .prologue
    .line 560248
    sget-object v0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->e:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    if-nez v0, :cond_1

    .line 560249
    const-class v1, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    monitor-enter v1

    .line 560250
    :try_start_0
    sget-object v0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->e:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 560251
    if-eqz v2, :cond_0

    .line 560252
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 560253
    new-instance p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v3

    check-cast v3, LX/1HI;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v5

    invoke-static {v5}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;-><init>(LX/1HI;LX/1Ad;Landroid/content/res/Resources;)V

    .line 560254
    move-object v0, p0

    .line 560255
    sput-object v0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->e:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 560256
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 560257
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 560258
    :cond_1
    sget-object v0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->e:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    return-object v0

    .line 560259
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 560260
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 2

    .prologue
    .line 560261
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    .line 560262
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->name:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 560263
    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 560264
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;
    .locals 2

    .prologue
    .line 560265
    const/4 v0, 0x0

    .line 560266
    sget-object v1, LX/76S;->STATIC:LX/76S;

    if-eq p1, v1, :cond_1

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 560267
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    .line 560268
    :cond_0
    :goto_0
    return-object v0

    .line 560269
    :cond_1
    sget-object v1, LX/76S;->ANIMATED:LX/76S;

    if-eq p1, v1, :cond_0

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 560270
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I
    .locals 4

    .prologue
    .line 560271
    iget v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->width:I

    .line 560272
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 560273
    iget v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->scale:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    iget v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->scale:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 560274
    iget v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->width:I

    int-to-float v0, v0

    iget v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->scale:F

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 560275
    :cond_0
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    .line 560276
    if-eqz v1, :cond_1

    sget-object v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->UNKNOWN:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-eq v1, v2, :cond_1

    .line 560277
    sget-object v2, LX/76R;->a:[I

    invoke-virtual {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 560278
    const/4 v2, -0x1

    :goto_0
    move v1, v2

    .line 560279
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 560280
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 560281
    :cond_1
    return v0

    .line 560282
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b0e7b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_0

    .line 560283
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b0e79

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_0

    .line 560284
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b0e7f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_0

    .line 560285
    :pswitch_3
    iget-object v2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b0e80

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_0

    .line 560286
    :pswitch_4
    iget-object v2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b0e82

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final a()LX/1Ai;
    .locals 1

    .prologue
    .line 560287
    new-instance v0, LX/76Q;

    invoke-direct {v0, p0}, LX/76Q;-><init>(Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)LX/1bf;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 560288
    invoke-static {p1, p2}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    move-result-object v0

    .line 560289
    invoke-static {}, LX/4eB;->newBuilder()LX/4eC;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    .line 560290
    sget-object v3, LX/76R;->a:[I

    invoke-virtual {v2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 560291
    const/4 v3, -0x1

    :goto_0
    move v2, v3

    .line 560292
    iput v2, v1, LX/4eC;->b:I

    .line 560293
    move-object v1, v1

    .line 560294
    invoke-virtual {v1}, LX/4eC;->l()LX/4eB;

    move-result-object v1

    .line 560295
    sget-object v2, LX/76S;->STATIC:LX/76S;

    if-eq p2, v2, :cond_0

    iget-object v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 560296
    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    .line 560297
    iput-object v1, v0, LX/1bX;->e:LX/1bZ;

    .line 560298
    move-object v0, v0

    .line 560299
    :goto_1
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    :goto_2
    return-object v0

    .line 560300
    :cond_0
    sget-object v1, LX/76S;->ANIMATED:LX/76S;

    if-eq p2, v1, :cond_1

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 560301
    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    goto :goto_1

    .line 560302
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 560303
    :pswitch_0
    iget-object v3, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v4, 0x7f0a00e9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    goto :goto_0

    .line 560304
    :pswitch_1
    iget-object v3, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v4, 0x7f0a01db

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/1bf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 560305
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 560306
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->b()LX/0Px;

    move-result-object v3

    .line 560307
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 560308
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    sget-object v4, LX/76S;->ANY:LX/76S;

    invoke-virtual {p0, v0, v4}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)LX/1bf;

    move-result-object v0

    .line 560309
    if-eqz v0, :cond_0

    .line 560310
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 560311
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 560312
    :cond_1
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/common/callercontext/CallerContext;LX/1Ai;)Z
    .locals 3

    .prologue
    .line 560313
    sget-object v0, LX/76S;->ANY:LX/76S;

    invoke-virtual {p0, p2, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)LX/1bf;

    move-result-object v1

    .line 560314
    if-eqz v1, :cond_0

    .line 560315
    iget-object v0, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->d:LX/1Ad;

    invoke-virtual {v0, p3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p4}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 560316
    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 560317
    const/4 v0, 0x1

    .line 560318
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I
    .locals 4

    .prologue
    .line 560319
    iget v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->height:I

    .line 560320
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 560321
    iget v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->scale:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    iget v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->scale:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 560322
    iget v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->height:I

    int-to-float v0, v0

    iget v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->scale:F

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 560323
    :cond_0
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    .line 560324
    if-eqz v1, :cond_1

    sget-object v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->UNKNOWN:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-eq v1, v2, :cond_1

    .line 560325
    sget-object v2, LX/76R;->a:[I

    invoke-virtual {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 560326
    :pswitch_0
    const/4 v2, -0x1

    :goto_0
    move v1, v2

    .line 560327
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 560328
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 560329
    :cond_1
    return v0

    .line 560330
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b0e7c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_0

    .line 560331
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b0e7a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_0

    .line 560332
    :pswitch_3
    iget-object v2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b0e7d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_0

    .line 560333
    :pswitch_4
    iget-object v2, p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b0e81

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
