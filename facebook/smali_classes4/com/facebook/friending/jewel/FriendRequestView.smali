.class public Lcom/facebook/friending/jewel/FriendRequestView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2nY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:Z

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/view/View;

.field private q:Lcom/facebook/friends/ui/SmartButtonLite;

.field private r:Landroid/view/View;

.field public s:Lcom/facebook/friends/model/FriendRequest;

.field public t:LX/2na;

.field private u:LX/2lu;

.field public v:LX/2iW;

.field public w:LX/2iY;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 464813
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/friending/jewel/FriendRequestView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 464814
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 464809
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 464810
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->m:Z

    .line 464811
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->d()V

    .line 464812
    return-void
.end method

.method private a(LX/2na;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 464808
    new-instance v0, LX/2nb;

    invoke-direct {v0, p0, p1}, LX/2nb;-><init>(Lcom/facebook/friending/jewel/FriendRequestView;LX/2na;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 464807
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->j:LX/23P;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/friending/jewel/FriendRequestView;LX/23P;LX/2nY;LX/0ad;)V
    .locals 0

    .prologue
    .line 464806
    iput-object p1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->j:LX/23P;

    iput-object p2, p0, Lcom/facebook/friending/jewel/FriendRequestView;->k:LX/2nY;

    iput-object p3, p0, Lcom/facebook/friending/jewel/FriendRequestView;->l:LX/0ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/friending/jewel/FriendRequestView;

    invoke-static {v2}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    invoke-static {v2}, LX/2nY;->a(LX/0QB;)LX/2nY;

    move-result-object v1

    check-cast v1, LX/2nY;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Lcom/facebook/friending/jewel/FriendRequestView;LX/23P;LX/2nY;LX/0ad;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/friending/jewel/FriendRequestView;LX/2lu;)V
    .locals 0

    .prologue
    .line 464803
    iput-object p1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->u:LX/2lu;

    .line 464804
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->e()V

    .line 464805
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 464792
    const-class v0, Lcom/facebook/friending/jewel/FriendRequestView;

    invoke-static {v0, p0}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 464793
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->l:LX/0ad;

    sget-short v1, LX/2ez;->m:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->m:Z

    .line 464794
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->l:LX/0ad;

    sget-short v1, LX/2nX;->b:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464795
    const v0, 0x7f0306e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 464796
    :goto_0
    const v0, 0x7f0d128d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->n:Landroid/widget/TextView;

    .line 464797
    const v0, 0x7f0d128e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->o:Landroid/widget/TextView;

    .line 464798
    const v0, 0x7f0d127f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->p:Landroid/view/View;

    .line 464799
    const v0, 0x7f0d1280

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->q:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 464800
    const v0, 0x7f0d1281

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->r:Landroid/view/View;

    .line 464801
    return-void

    .line 464802
    :cond_0
    const v0, 0x7f0306e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    goto :goto_0
.end method

.method private e()V
    .locals 0

    .prologue
    .line 464787
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->f()V

    .line 464788
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->g()V

    .line 464789
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->h()V

    .line 464790
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->k()V

    .line 464791
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 464785
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v1}, Lcom/facebook/friends/model/FriendRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 464786
    return-void
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 464768
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 464769
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 464770
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v1

    .line 464771
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->k:LX/2nY;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestView;->u:LX/2lu;

    invoke-virtual {v0, v2, v1}, LX/2nY;->a(LX/2lu;Z)Ljava/lang/String;

    move-result-object v0

    .line 464772
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 464773
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    .line 464774
    iget-object v1, v0, Lcom/facebook/friends/model/FriendRequest;->g:LX/0Px;

    move-object v0, v1

    .line 464775
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 464776
    invoke-virtual {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081fe6

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    .line 464777
    iget-object v5, v3, Lcom/facebook/friends/model/FriendRequest;->g:LX/0Px;

    move-object v3, v5

    .line 464778
    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 464779
    :cond_0
    :goto_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 464780
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->o:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 464781
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->o:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 464782
    :goto_1
    return-void

    .line 464783
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 464784
    :cond_2
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private h()V
    .locals 3

    .prologue
    .line 464698
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v0

    .line 464699
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->u:LX/2lu;

    sget-object v2, LX/2lu;->NEEDS_RESPONSE:LX/2lu;

    if-eq v1, v2, :cond_0

    .line 464700
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 464701
    :goto_0
    return-void

    .line 464702
    :cond_0
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->p:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 464703
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->i()V

    .line 464704
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->j()V

    .line 464705
    if-eqz v0, :cond_1

    .line 464706
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->q:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->l()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464707
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->r:Landroid/view/View;

    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->m()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 464708
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->q:Lcom/facebook/friends/ui/SmartButtonLite;

    sget-object v1, LX/2na;->CONFIRM:LX/2na;

    invoke-direct {p0, v1}, Lcom/facebook/friending/jewel/FriendRequestView;->a(LX/2na;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464709
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->r:Landroid/view/View;

    sget-object v1, LX/2na;->REJECT:LX/2na;

    invoke-direct {p0, v1}, Lcom/facebook/friending/jewel/FriendRequestView;->a(LX/2na;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private i()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 464749
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v1}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v1

    .line 464750
    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestView;->k:LX/2nY;

    invoke-virtual {v2, v1}, LX/2nY;->d(Z)Ljava/lang/String;

    move-result-object v2

    .line 464751
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestView;->l:LX/0ad;

    sget-short v4, LX/2nX;->a:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 464752
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestView;->k:LX/2nY;

    .line 464753
    if-eqz v1, :cond_5

    iget-object v4, v3, LX/2nY;->e:Ljava/lang/String;

    :goto_0
    move-object v1, v4

    .line 464754
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestView;->q:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-direct {p0, v1}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    if-nez v2, :cond_0

    :goto_1
    invoke-virtual {v3, v4, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 464755
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->q:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 464756
    :goto_2
    return-void

    .line 464757
    :cond_0
    invoke-direct {p0, v2}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 464758
    :cond_1
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestView;->l:LX/0ad;

    sget-short v4, LX/2nX;->c:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 464759
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestView;->k:LX/2nY;

    .line 464760
    if-eqz v1, :cond_6

    iget-object v4, v3, LX/2nY;->e:Ljava/lang/String;

    :goto_3
    move-object v1, v4

    .line 464761
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestView;->q:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-direct {p0, v1}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    if-nez v2, :cond_2

    :goto_4
    invoke-virtual {v3, v4, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 464762
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->q:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 464763
    :cond_2
    invoke-direct {p0, v2}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_4

    .line 464764
    :cond_3
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestView;->k:LX/2nY;

    invoke-virtual {v3, v1}, LX/2nY;->a(Z)Ljava/lang/String;

    move-result-object v1

    .line 464765
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestView;->q:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-direct {p0, v1}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    if-nez v2, :cond_4

    :goto_5
    invoke-virtual {v3, v4, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 464766
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->q:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 464767
    :cond_4
    invoke-direct {p0, v2}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_5

    :cond_5
    iget-object v4, v3, LX/2nY;->l:Ljava/lang/String;

    goto :goto_0

    :cond_6
    iget-object v4, v3, LX/2nY;->j:Ljava/lang/String;

    goto :goto_3
.end method

.method private j()V
    .locals 5

    .prologue
    .line 464738
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v1

    .line 464739
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->r:Landroid/view/View;

    check-cast v0, Landroid/widget/Button;

    .line 464740
    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestView;->l:LX/0ad;

    sget-short v3, LX/2nX;->c:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 464741
    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestView;->k:LX/2nY;

    .line 464742
    if-eqz v1, :cond_1

    iget-object v3, v2, LX/2nY;->h:Ljava/lang/String;

    :goto_0
    move-object v1, v3

    .line 464743
    invoke-direct {p0, v1}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 464744
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 464745
    :goto_1
    return-void

    .line 464746
    :cond_0
    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestView;->k:LX/2nY;

    invoke-virtual {v2, v1}, LX/2nY;->e(Z)Ljava/lang/String;

    move-result-object v1

    .line 464747
    invoke-direct {p0, v1}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 464748
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    iget-object v3, v2, LX/2nY;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 464729
    sget-object v0, LX/2nc;->a:[I

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->u:LX/2lu;

    invoke-virtual {v1}, LX/2lu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 464730
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    .line 464731
    iget-boolean v1, v0, Lcom/facebook/friends/model/FriendRequest;->h:Z

    move v0, v1

    .line 464732
    if-eqz v0, :cond_0

    const v0, 0x7f0a004f

    .line 464733
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/friending/jewel/FriendRequestView;->setBackgroundResource(I)V

    .line 464734
    return-void

    .line 464735
    :pswitch_0
    const v0, 0x7f0a08a2

    goto :goto_0

    .line 464736
    :pswitch_1
    const v0, 0x7f0a08a3

    goto :goto_0

    .line 464737
    :cond_0
    const v0, 0x7f020b67

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private l()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 464728
    new-instance v0, LX/EyW;

    invoke-direct {v0, p0}, LX/EyW;-><init>(Lcom/facebook/friending/jewel/FriendRequestView;)V

    return-object v0
.end method

.method private m()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 464727
    new-instance v0, LX/EyX;

    invoke-direct {v0, p0}, LX/EyX;-><init>(Lcom/facebook/friending/jewel/FriendRequestView;)V

    return-object v0
.end method

.method public static n(Lcom/facebook/friending/jewel/FriendRequestView;)V
    .locals 3

    .prologue
    .line 464722
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->v:LX/2iW;

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestView;->t:LX/2na;

    invoke-interface {v0, v1, v2}, LX/2iW;->a(Lcom/facebook/friends/model/FriendRequest;LX/2na;)V

    .line 464723
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->t:LX/2na;

    sget-object v1, LX/2na;->CONFIRM:LX/2na;

    if-ne v0, v1, :cond_0

    sget-object v0, LX/2lu;->ACCEPTED:LX/2lu;

    .line 464724
    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/friending/jewel/FriendRequestView;->a$redex0(Lcom/facebook/friending/jewel/FriendRequestView;LX/2lu;)V

    .line 464725
    return-void

    .line 464726
    :cond_0
    sget-object v0, LX/2lu;->REJECTED:LX/2lu;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/friends/model/FriendRequest;)V
    .locals 2

    .prologue
    .line 464714
    iput-object p1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->s:Lcom/facebook/friends/model/FriendRequest;

    .line 464715
    iget-object v0, p1, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v0, v0

    .line 464716
    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestView;->u:LX/2lu;

    .line 464717
    invoke-virtual {p1}, Lcom/facebook/friends/model/FriendRequest;->d()Ljava/lang/String;

    move-result-object v0

    .line 464718
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 464719
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestView;->e()V

    .line 464720
    return-void

    .line 464721
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public setOnResponseListener(LX/2iW;)V
    .locals 0

    .prologue
    .line 464712
    iput-object p1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->v:LX/2iW;

    .line 464713
    return-void
.end method

.method public setOnSuggestionResponseListener(LX/2iY;)V
    .locals 0

    .prologue
    .line 464710
    iput-object p1, p0, Lcom/facebook/friending/jewel/FriendRequestView;->w:LX/2iY;

    .line 464711
    return-void
.end method
