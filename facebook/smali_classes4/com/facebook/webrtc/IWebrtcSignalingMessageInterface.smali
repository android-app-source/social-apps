.class public interface abstract Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract sendMultiwaySignalingMessage(Ljava/lang/String;Ljava/lang/String;[B)Z
.end method

.method public abstract sendOfferToPeer(JJJ[B)Z
.end method

.method public abstract sendThriftToPeer(JJJ[B)Z
.end method

.method public abstract sendThriftToSelf(JJ[B)Z
.end method

.method public abstract sendToPeer(JJJLjava/lang/String;)Z
.end method

.method public abstract sendToSelf(JJLjava/lang/String;)Z
.end method

.method public abstract setWebrtcManager(LX/2Tm;)V
.end method
