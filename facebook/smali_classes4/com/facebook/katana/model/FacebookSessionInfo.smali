.class public Lcom/facebook/katana/model/FacebookSessionInfo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/katana/model/FacebookSessionInfoDeserializer;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/credentials/SessionCookie;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/03V;

.field public final errorData:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "error_data"
    .end annotation
.end field

.field public mFilterKey:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "filter"
    .end annotation
.end field

.field public mMyself:Lcom/facebook/ipc/model/FacebookUser;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "profile"
    .end annotation
.end field

.field public final machineID:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "machine_id"
    .end annotation
.end field

.field public final oAuthToken:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "access_token"
    .end annotation
.end field

.field public final sessionKey:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_key"
    .end annotation
.end field

.field public final sessionSecret:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "secret"
    .end annotation
.end field

.field public final userId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation
.end field

.field public username:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "username"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 566589
    const-class v0, Lcom/facebook/katana/model/FacebookSessionInfoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 566588
    const-class v0, Lcom/facebook/katana/model/FacebookSessionInfoSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 566551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566552
    iput-object v2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->username:Ljava/lang/String;

    .line 566553
    iput-object v2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->sessionKey:Ljava/lang/String;

    .line 566554
    iput-object v2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->sessionSecret:Ljava/lang/String;

    .line 566555
    iput-object v2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->oAuthToken:Ljava/lang/String;

    .line 566556
    iput-object v2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->machineID:Ljava/lang/String;

    .line 566557
    iput-object v2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->errorData:Ljava/lang/String;

    .line 566558
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->userId:J

    .line 566559
    iput-object v2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->a:Ljava/util/List;

    .line 566560
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/facebook/ipc/model/FacebookUser;Ljava/util/List;LX/03V;)V
    .locals 1
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/model/FacebookUser;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/credentials/SessionCookie;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 566575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566576
    iput-object p1, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->username:Ljava/lang/String;

    .line 566577
    iput-object p2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->sessionKey:Ljava/lang/String;

    .line 566578
    iput-object p3, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->sessionSecret:Ljava/lang/String;

    .line 566579
    iput-object p4, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->oAuthToken:Ljava/lang/String;

    .line 566580
    iput-wide p5, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->userId:J

    .line 566581
    iput-object p7, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->machineID:Ljava/lang/String;

    .line 566582
    iput-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->errorData:Ljava/lang/String;

    .line 566583
    iput-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->mFilterKey:Ljava/lang/String;

    .line 566584
    iput-object p8, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->mMyself:Lcom/facebook/ipc/model/FacebookUser;

    .line 566585
    iput-object p9, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->a:Ljava/util/List;

    .line 566586
    iput-object p10, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->b:LX/03V;

    .line 566587
    return-void
.end method

.method public static a(Lcom/facebook/katana/model/FacebookSessionInfo;)Z
    .locals 4

    .prologue
    .line 566574
    if-eqz p0, :cond_0

    iget-wide v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->userId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->sessionKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->sessionSecret:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->oAuthToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->mMyself:Lcom/facebook/ipc/model/FacebookUser;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/model/FacebookUser;
    .locals 1

    .prologue
    .line 566573
    iget-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->mMyself:Lcom/facebook/ipc/model/FacebookUser;

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 566571
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->a:Ljava/util/List;

    .line 566572
    return-void
.end method

.method public getSessionCookies()Ljava/util/List;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_cookies"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/credentials/SessionCookie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566568
    iget-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 566569
    iget-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 566570
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSessionCookies(LX/0nW;)V
    .locals 3
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_cookies"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 566561
    if-nez p1, :cond_1

    .line 566562
    iput-object v2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->a:Ljava/util/List;

    .line 566563
    :cond_0
    :goto_0
    return-void

    .line 566564
    :cond_1
    :try_start_0
    invoke-virtual {p1}, LX/0nW;->i()LX/15w;

    move-result-object v0

    .line 566565
    if-eqz v0, :cond_0

    .line 566566
    new-instance v1, LX/FBD;

    invoke-direct {v1, p0}, LX/FBD;-><init>(Lcom/facebook/katana/model/FacebookSessionInfo;)V

    invoke-virtual {v0, v1}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->a:Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 566567
    :catch_0
    iput-object v2, p0, Lcom/facebook/katana/model/FacebookSessionInfo;->a:Ljava/util/List;

    goto :goto_0
.end method
