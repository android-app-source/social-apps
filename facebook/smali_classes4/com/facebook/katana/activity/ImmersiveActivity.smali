.class public Lcom/facebook/katana/activity/ImmersiveActivity;
.super Lcom/facebook/chrome/FbChromeDelegatingActivity;
.source ""

# interfaces
.implements LX/13v;
.implements LX/0f4;
.implements LX/0fE;
.implements LX/0fI;


# annotations
.annotation build Lcom/facebook/common/componentmap/ComponentMapConfig;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 375206
    new-instance v0, LX/Gto;

    invoke-direct {v0}, LX/Gto;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/chrome/FbChromeDelegatingActivity;-><init>(LX/GVq;)V

    .line 375207
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/base/fragment/FbFragment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 375205
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    check-cast v0, LX/Gto;

    invoke-virtual {v0}, LX/Gto;->c()Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 375203
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    move-object v0, v0

    .line 375204
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 375199
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    check-cast v0, LX/Gto;

    invoke-virtual {v0}, LX/Gto;->v()V

    .line 375200
    return-void
.end method

.method public final w()LX/0cQ;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 375202
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    check-cast v0, LX/Gto;

    invoke-virtual {v0}, LX/Gto;->w()LX/0cQ;

    move-result-object v0

    return-object v0
.end method

.method public final x()LX/0gz;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 375201
    iget-object v0, p0, Lcom/facebook/chrome/FbChromeDelegatingActivity;->p:LX/GVq;

    check-cast v0, LX/Gto;

    invoke-virtual {v0}, LX/Gto;->x()LX/0gz;

    move-result-object v0

    return-object v0
.end method
