.class public Lcom/facebook/katana/activity/react/ImmersiveReactActivity;
.super Lcom/facebook/katana/activity/ImmersiveActivity;
.source ""

# interfaces
.implements LX/2lD;


# annotations
.annotation build Lcom/facebook/common/componentmap/ComponentMapConfig;
.end annotation


# instance fields
.field private q:LX/9n2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 457391
    invoke-direct {p0}, Lcom/facebook/katana/activity/ImmersiveActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/String;ILX/9n2;)V
    .locals 0

    .prologue
    .line 457392
    iput-object p3, p0, Lcom/facebook/katana/activity/react/ImmersiveReactActivity;->q:LX/9n2;

    .line 457393
    invoke-virtual {p0, p1, p2}, Lcom/facebook/katana/activity/react/ImmersiveReactActivity;->requestPermissions([Ljava/lang/String;I)V

    .line 457394
    return-void
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 457395
    invoke-virtual {p0}, Lcom/facebook/katana/activity/ImmersiveActivity;->c()Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    .line 457396
    const/16 v1, 0x52

    if-ne p1, v1, :cond_0

    instance-of v1, v0, LX/0ic;

    if-eqz v1, :cond_0

    check-cast v0, LX/0ic;

    invoke-interface {v0}, LX/0ic;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457397
    const/4 v0, 0x1

    .line 457398
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/katana/activity/ImmersiveActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 457399
    iget-object v0, p0, Lcom/facebook/katana/activity/react/ImmersiveReactActivity;->q:LX/9n2;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/activity/react/ImmersiveReactActivity;->q:LX/9n2;

    invoke-interface {v0, p1, p3}, LX/9n2;->a(I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457400
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/activity/react/ImmersiveReactActivity;->q:LX/9n2;

    .line 457401
    :cond_0
    return-void
.end method
