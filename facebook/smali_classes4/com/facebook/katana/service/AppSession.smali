.class public Lcom/facebook/katana/service/AppSession;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final b:LX/29x;

.field private static c:I


# instance fields
.field public a:LX/0wb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0wb",
            "<",
            "LX/278;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field public e:LX/2A1;

.field public f:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field public h:Z

.field public i:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final k:LX/0en;

.field public final l:LX/03V;

.field public final m:LX/0WJ;

.field private final n:LX/28Q;

.field public final o:LX/0aG;

.field public final p:Ljava/util/concurrent/ExecutorService;

.field public final q:Landroid/content/Context;

.field public final r:LX/2A0;

.field public final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2c4;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0fW;

.field private final u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 540796
    new-instance v0, LX/29x;

    invoke-direct {v0}, LX/29x;-><init>()V

    sput-object v0, Lcom/facebook/katana/service/AppSession;->b:LX/29x;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 540797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540798
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 540799
    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->l:LX/03V;

    .line 540800
    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 540801
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 540802
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->l:LX/03V;

    const-string v2, "AppSession created before initialization was completed, t2045339"

    const-string v3, "t2045339"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 540803
    :cond_0
    invoke-static {p1}, LX/1mU;->a(Landroid/content/Context;)V

    .line 540804
    invoke-static {v1}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v0

    check-cast v0, LX/0VT;

    .line 540805
    const-string v2, ""

    const/4 v3, 0x0

    .line 540806
    invoke-virtual {v0}, LX/0VT;->a()LX/00G;

    move-result-object v4

    .line 540807
    if-nez v4, :cond_2

    .line 540808
    sget-object v4, LX/0VT;->a:Ljava/lang/Class;

    const-string v5, "Couldn\'t find own process name"

    invoke-static {v4, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 540809
    :goto_0
    move v2, v3

    .line 540810
    if-nez v2, :cond_1

    .line 540811
    iget-object v2, p0, Lcom/facebook/katana/service/AppSession;->l:LX/03V;

    const-string v3, "non_main_process_accesses_appsession"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AppSession should not be accessed from process "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/0VT;->a()LX/00G;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 540812
    :cond_1
    invoke-static {v1}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v0

    check-cast v0, LX/0en;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->k:LX/0en;

    .line 540813
    invoke-static {v1}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v0

    check-cast v0, LX/0WJ;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->m:LX/0WJ;

    .line 540814
    invoke-static {v1}, LX/28Q;->b(LX/0QB;)LX/28Q;

    move-result-object v0

    check-cast v0, LX/28Q;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->n:LX/28Q;

    .line 540815
    invoke-static {v1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->o:LX/0aG;

    .line 540816
    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->p:Ljava/util/concurrent/ExecutorService;

    .line 540817
    invoke-virtual {v1}, LX/0QA;->getApplicationInjector()LX/0QA;

    move-result-object v0

    const-class v2, Landroid/content/Context;

    invoke-virtual {v0, v2}, LX/0QA;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    .line 540818
    invoke-static {v1}, LX/29y;->a(LX/0QB;)LX/2A0;

    move-result-object v0

    check-cast v0, LX/2A0;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->r:LX/2A0;

    .line 540819
    const/16 v0, 0xe7c

    invoke-static {v1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->s:LX/0Ot;

    .line 540820
    invoke-static {v1}, LX/0fW;->a(LX/0QB;)LX/0fW;

    move-result-object v0

    check-cast v0, LX/0fW;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->t:LX/0fW;

    .line 540821
    const/16 v0, 0x15e7

    invoke-static {v1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->u:LX/0Or;

    .line 540822
    invoke-virtual {p0}, Lcom/facebook/katana/service/AppSession;->a()V

    .line 540823
    return-void

    .line 540824
    :cond_2
    iget-object v5, v4, LX/00G;->b:Ljava/lang/String;

    move-object v4, v5

    .line 540825
    iget-object v5, v0, LX/0VT;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 540826
    sget-object v4, LX/0VT;->a:Ljava/lang/Class;

    const-string v5, "Process name doesn\'t start with package name"

    invoke-static {v4, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 540827
    :cond_3
    iget-object v3, v0, LX/0VT;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 540828
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;
    .locals 1

    .prologue
    .line 540829
    sget-object v0, Lcom/facebook/katana/service/AppSession;->b:LX/29x;

    invoke-virtual {v0, p0}, LX/29x;->b(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 540833
    sget-object v0, Lcom/facebook/katana/service/AppSession;->b:LX/29x;

    invoke-virtual {v0, p0, p1}, LX/29x;->a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/katana/service/AppSession;LX/2A1;)V
    .locals 1

    .prologue
    .line 540752
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    invoke-virtual {v0, p1}, LX/2A1;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 540753
    :cond_0
    iput-object p1, p0, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    .line 540754
    :cond_1
    return-void
.end method

.method public static b(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 540830
    invoke-static {p0, p1}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 540831
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/katana/service/AppSession;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 540832
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 540834
    sget-object v0, Lcom/facebook/katana/service/AppSession;->b:LX/29x;

    invoke-virtual {v0, p0}, LX/29x;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static e(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 540791
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 540792
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 540793
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 540794
    return-void
.end method

.method private static declared-synchronized j()Ljava/lang/String;
    .locals 4

    .prologue
    .line 540795
    const-class v1, Lcom/facebook/katana/service/AppSession;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lcom/facebook/katana/service/AppSession;->c:I

    add-int/lit8 v3, v2, 0x1

    sput v3, Lcom/facebook/katana/service/AppSession;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 540781
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/katana/service/AppSession;->h:Z

    .line 540782
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/service/AppSession;->g:Z

    .line 540783
    invoke-static {}, Lcom/facebook/katana/service/AppSession;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->d:Ljava/lang/String;

    .line 540784
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->f:LX/0Ve;

    if-eqz v0, :cond_0

    .line 540785
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->f:LX/0Ve;

    invoke-interface {v0}, LX/0Ve;->dispose()V

    .line 540786
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->f:LX/0Ve;

    .line 540787
    :cond_0
    new-instance v0, LX/0wb;

    invoke-direct {v0}, LX/0wb;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->a:LX/0wb;

    .line 540788
    sget-object v0, LX/2A1;->STATUS_LOGGED_OUT:LX/2A1;

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540789
    monitor-exit p0

    return-void

    .line 540790
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/278;)V
    .locals 1

    .prologue
    .line 540779
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->a:LX/0wb;

    invoke-virtual {v0, p1}, LX/0wb;->a(Ljava/lang/Object;)V

    .line 540780
    return-void
.end method

.method public final a(Landroid/content/Context;LX/279;)V
    .locals 9

    .prologue
    .line 540759
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v0

    check-cast v0, LX/0hx;

    .line 540760
    iget-object v1, v0, LX/0hx;->c:LX/0gh;

    invoke-virtual {v1}, LX/0gh;->f()V

    .line 540761
    sget-object v0, LX/2A1;->STATUS_LOGGING_OUT:LX/2A1;

    invoke-static {p0, v0}, Lcom/facebook/katana/service/AppSession;->a$redex0(Lcom/facebook/katana/service/AppSession;LX/2A1;)V

    .line 540762
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 540763
    const-string v0, "logout_reason_param"

    invoke-virtual {p2}, LX/279;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 540764
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 540765
    iget-object v2, p0, Lcom/facebook/katana/service/AppSession;->t:LX/0fW;

    .line 540766
    iget-object v5, v2, LX/0fW;->a:LX/0WP;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "dbl_local_auth_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v5

    .line 540767
    const-string v6, "persisted_ts"

    const-wide v7, 0x7fffffffffffffffL

    invoke-virtual {v5, v6, v7, v8}, LX/0WS;->a(Ljava/lang/String;J)J

    move-result-wide v5

    .line 540768
    iget-object v7, v2, LX/0fW;->f:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    sub-long v5, v7, v5

    const-wide v7, 0x1cf7c5800L

    cmp-long v5, v5, v7

    if-lez v5, :cond_1

    const/4 v5, 0x1

    :goto_0
    move v4, v5

    .line 540769
    if-eqz v4, :cond_0

    .line 540770
    invoke-virtual {v2, v0}, LX/0fW;->c(Ljava/lang/String;)V

    .line 540771
    :cond_0
    iget-object v2, p0, Lcom/facebook/katana/service/AppSession;->t:LX/0fW;

    invoke-virtual {v2, v0}, LX/0fW;->b(Ljava/lang/String;)Z

    move-result v0

    .line 540772
    const-string v2, "retain_session_for_dbl"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 540773
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->o:LX/0aG;

    const-string v2, "logout"

    const v3, 0x2eef6190

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 540774
    new-instance v1, LX/FBd;

    .line 540775
    iget-object v2, p0, Lcom/facebook/katana/service/AppSession;->a:LX/0wb;

    invoke-virtual {v2}, LX/0wb;->a()Ljava/util/Set;

    move-result-object v2

    move-object v2, v2

    .line 540776
    invoke-direct {v1, p0, v2}, LX/FBd;-><init>(Lcom/facebook/katana/service/AppSession;Ljava/util/Set;)V

    .line 540777
    iget-object v2, p0, Lcom/facebook/katana/service/AppSession;->p:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 540778
    return-void

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/katana/model/FacebookSessionInfo;
    .locals 4

    .prologue
    .line 540755
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->n:LX/28Q;

    invoke-virtual {v0}, LX/28Q;->a()Lcom/facebook/katana/model/FacebookSessionInfo;

    move-result-object v0

    .line 540756
    invoke-static {v0}, Lcom/facebook/katana/model/FacebookSessionInfo;->a(Lcom/facebook/katana/model/FacebookSessionInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 540757
    iget-object v1, p0, Lcom/facebook/katana/service/AppSession;->l:LX/03V;

    const-string v2, "SessionInfo"

    const-string v3, "Invalid Session Info encountered"

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 540758
    :cond_0
    return-object v0
.end method

.method public final b(LX/278;)V
    .locals 1

    .prologue
    .line 540750
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->a:LX/0wb;

    invoke-virtual {v0, p1}, LX/0wb;->b(Ljava/lang/Object;)V

    .line 540751
    return-void
.end method

.method public final c(Landroid/content/Context;Z)V
    .locals 7

    .prologue
    .line 540736
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    .line 540737
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/service/AppSession;->g:Z

    .line 540738
    sget-object v0, LX/2A1;->STATUS_LOGGED_IN:LX/2A1;

    invoke-static {p0, v0}, Lcom/facebook/katana/service/AppSession;->a$redex0(Lcom/facebook/katana/service/AppSession;LX/2A1;)V

    .line 540739
    if-eqz p2, :cond_1

    .line 540740
    invoke-static {v6}, LX/2Wp;->b(LX/0QB;)LX/2Wp;

    move-result-object v0

    check-cast v0, LX/2Wp;

    .line 540741
    const-string v1, "SSO_LOGIN"

    invoke-static {v0, v1}, LX/2Wp;->a(LX/2Wp;Ljava/lang/String;)V

    .line 540742
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->o:LX/0aG;

    const-string v1, "login_data_fetch"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/katana/service/AppSession;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x7d459bbd

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 540743
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 540744
    iget-object v1, p0, Lcom/facebook/katana/service/AppSession;->i:LX/0TF;

    if-eqz v1, :cond_0

    .line 540745
    iget-object v1, p0, Lcom/facebook/katana/service/AppSession;->i:LX/0TF;

    iget-object v2, p0, Lcom/facebook/katana/service/AppSession;->p:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 540746
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/service/AppSession;->i:LX/0TF;

    .line 540747
    :cond_0
    :goto_0
    invoke-static {v6}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v0

    check-cast v0, LX/0Wd;

    new-instance v1, Lcom/facebook/katana/service/AppSession$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/katana/service/AppSession$1;-><init>(Lcom/facebook/katana/service/AppSession;Landroid/content/Context;)V

    const v2, 0x76d74f28

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 540748
    return-void

    .line 540749
    :cond_1
    invoke-static {v6}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v0

    check-cast v0, LX/0hx;

    invoke-virtual {v0}, LX/0hx;->a()V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 540735
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->n:LX/28Q;

    invoke-virtual {v0}, LX/28Q;->a()Lcom/facebook/katana/model/FacebookSessionInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/katana/model/FacebookSessionInfo;->a(Lcom/facebook/katana/model/FacebookSessionInfo;)Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 540725
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->r:LX/2A0;

    .line 540726
    iget-object v1, v0, LX/2A0;->a:LX/0Xp;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.orca.login.AuthStateMachineMonitor.AUTH_COMPLETE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 540727
    iget-object v1, v0, LX/2A0;->b:LX/0bD;

    new-instance v2, LX/0bE;

    invoke-direct {v2}, LX/0bE;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 540728
    const-string v0, "AppSession.doLogin"

    const v1, -0x4752ecf9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 540729
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/katana/service/AppSession;->c(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540730
    const v0, 0x1f541094

    invoke-static {v0}, LX/02m;->a(I)V

    .line 540731
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession;->a:LX/0wb;

    invoke-virtual {v0}, LX/0wb;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/278;

    .line 540732
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/278;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 540733
    :catchall_0
    move-exception v0

    const v1, -0x16da53d2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 540734
    :cond_0
    return-void
.end method
