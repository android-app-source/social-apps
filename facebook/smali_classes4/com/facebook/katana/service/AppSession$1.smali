.class public final Lcom/facebook/katana/service/AppSession$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/katana/service/AppSession;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/service/AppSession;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 540835
    iput-object p1, p0, Lcom/facebook/katana/service/AppSession$1;->b:Lcom/facebook/katana/service/AppSession;

    iput-object p2, p0, Lcom/facebook/katana/service/AppSession$1;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 540836
    const-string v0, "AppSession.runOnLoginNonCritical"

    const v1, 0x232dcbc8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 540837
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/service/AppSession$1;->b:Lcom/facebook/katana/service/AppSession;

    iget-object v1, p0, Lcom/facebook/katana/service/AppSession$1;->a:Landroid/content/Context;

    .line 540838
    iget-object v2, v0, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    sget-object v3, LX/2A1;->STATUS_LOGGED_IN:LX/2A1;

    if-ne v2, v3, :cond_0

    .line 540839
    iget-object v2, v0, Lcom/facebook/katana/service/AppSession;->s:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2c4;

    sget-object v3, Lcom/facebook/notifications/constants/NotificationType;->AUTHENTICATION_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v2, v3}, LX/2c4;->a(Lcom/facebook/notifications/constants/NotificationType;)V

    .line 540840
    iget-object v2, v0, Lcom/facebook/katana/service/AppSession;->s:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2c4;

    const/16 v3, 0x2714

    invoke-virtual {v2, v3}, LX/2c4;->a(I)V

    .line 540841
    :cond_0
    iget-object v2, v0, Lcom/facebook/katana/service/AppSession;->m:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 540842
    if-nez v2, :cond_1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540843
    :goto_0
    const v0, -0x6b77c19e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 540844
    return-void

    .line 540845
    :catchall_0
    move-exception v0

    const v1, 0x1871bf71

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 540846
    :cond_1
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 540847
    iget-object v4, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v4

    .line 540848
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/facebook/attribution/AttributionIdService;->a(Landroid/content/Context;J)V

    goto :goto_0
.end method
