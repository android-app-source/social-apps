.class public Lcom/facebook/katana/orca/DiodeQpFragment;
.super Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0fv;


# static fields
.field private static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/76b;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/FkC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Fk3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Fk4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Fk5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Z

.field private l:Landroid/widget/ScrollView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Lcom/facebook/user/tiles/UserTileView;

.field private p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public q:Landroid/widget/Button;

.field private r:Landroid/view/ViewStub;

.field public s:Landroid/widget/TextView;

.field private t:LX/1Ai;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 450539
    const-class v0, Lcom/facebook/katana/orca/DiodeQpFragment;

    const-string v1, "diode_qp_module"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/orca/DiodeQpFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 450536
    invoke-direct {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;-><init>()V

    .line 450537
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->k:Z

    .line 450538
    return-void
.end method

.method public static a(Landroid/widget/TextView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 450465
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 450466
    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450467
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450468
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 450469
    :goto_0
    return-void

    .line 450470
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 450531
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 450532
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450533
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 450534
    :goto_0
    return-void

    .line 450535
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 450528
    invoke-super {p0, p1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/os/Bundle;)V

    .line 450529
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/katana/orca/DiodeQpFragment;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    move-result-object v4

    check-cast v4, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-static {v0}, LX/76b;->b(LX/0QB;)LX/76b;

    move-result-object v5

    check-cast v5, LX/76b;

    invoke-static {v0}, LX/FkC;->a(LX/0QB;)LX/FkC;

    move-result-object v6

    check-cast v6, LX/FkC;

    invoke-static {v0}, LX/Fk3;->b(LX/0QB;)LX/Fk3;

    move-result-object v7

    check-cast v7, LX/Fk3;

    invoke-static {v0}, LX/Fk4;->b(LX/0QB;)LX/Fk4;

    move-result-object v8

    check-cast v8, LX/Fk4;

    invoke-static {v0}, LX/Fk5;->b(LX/0QB;)LX/Fk5;

    move-result-object p1

    check-cast p1, LX/Fk5;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/katana/orca/DiodeQpFragment;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, v2, Lcom/facebook/katana/orca/DiodeQpFragment;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iput-object v5, v2, Lcom/facebook/katana/orca/DiodeQpFragment;->c:LX/76b;

    iput-object v6, v2, Lcom/facebook/katana/orca/DiodeQpFragment;->e:LX/FkC;

    iput-object v7, v2, Lcom/facebook/katana/orca/DiodeQpFragment;->f:LX/Fk3;

    iput-object v8, v2, Lcom/facebook/katana/orca/DiodeQpFragment;->g:LX/Fk4;

    iput-object p1, v2, Lcom/facebook/katana/orca/DiodeQpFragment;->h:LX/Fk5;

    iput-object v0, v2, Lcom/facebook/katana/orca/DiodeQpFragment;->i:LX/0ad;

    .line 450530
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 450526
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->l:Landroid/widget/ScrollView;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->fullScroll(I)Z

    .line 450527
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 450525
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->l:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 450524
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v3, 0x2a

    const v4, -0x8bc9c6e

    invoke-static {v0, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 450487
    invoke-super {p0, p1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 450488
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-object v4, v0

    .line 450489
    invoke-virtual {v4}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v5

    .line 450490
    invoke-virtual {v4}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 450491
    iget-object v0, v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    sget-object v7, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_TAB_BADGEABLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-virtual {v0, v7}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450492
    const/4 v0, 0x1

    .line 450493
    :goto_0
    const v6, 0x7f0d0ca3

    invoke-virtual {p0, v6}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v6

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 450494
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->m:Landroid/widget/TextView;

    iget-object v6, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-static {v0, v6}, Lcom/facebook/katana/orca/DiodeQpFragment;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 450495
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->n:Landroid/widget/TextView;

    iget-object v6, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-static {v0, v6}, Lcom/facebook/katana/orca/DiodeQpFragment;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 450496
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v6, "CIRCLE_CROP"

    sget-object v7, LX/8ue;->NONE:LX/8ue;

    invoke-virtual {v0, v6, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v6, "MESSENGER_BADGE"

    sget-object v7, LX/8ue;->MESSENGER:LX/8ue;

    invoke-virtual {v0, v6, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 450497
    iget-object v6, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    if-eqz v6, :cond_3

    iget-object v6, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v6, v6, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v6, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    if-eqz v6, :cond_3

    iget-object v6, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    const-string v7, "image_overlay"

    invoke-virtual {v6, v7}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    const-string v7, "image_overlay"

    invoke-virtual {v6, v7}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 450498
    iget-object v4, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    const-string v6, "image_overlay"

    invoke-virtual {v4, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8ue;

    .line 450499
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0b2106

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 450500
    new-instance v6, Lcom/facebook/user/model/PicSquare;

    new-instance v7, Lcom/facebook/user/model/PicSquareUrlWithSize;

    iget-object v8, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v8, v8, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-direct {v7, v4, v8}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-direct {v6, v7, v9, v9}, Lcom/facebook/user/model/PicSquare;-><init>(Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;)V

    .line 450501
    iget-object v4, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->o:Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v6, v0}, LX/8t9;->a(Lcom/facebook/user/model/PicSquare;LX/8ue;)LX/8t9;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 450502
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->o:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 450503
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 450504
    :goto_2
    iget-object v0, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 450505
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->q:Landroid/widget/Button;

    new-instance v2, LX/FBK;

    invoke-direct {v2, p0, v0}, LX/FBK;-><init>(Lcom/facebook/katana/orca/DiodeQpFragment;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)V

    invoke-static {v1, v0, v2}, Lcom/facebook/katana/orca/DiodeQpFragment;->a(Landroid/widget/TextView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/view/View$OnClickListener;)V

    .line 450506
    iget-object v0, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 450507
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->s:Landroid/widget/TextView;

    new-instance v2, LX/FBL;

    invoke-direct {v2, p0, v0}, LX/FBL;-><init>(Lcom/facebook/katana/orca/DiodeQpFragment;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)V

    invoke-static {v1, v0, v2}, Lcom/facebook/katana/orca/DiodeQpFragment;->a(Landroid/widget/TextView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/view/View$OnClickListener;)V

    .line 450508
    iget-object v0, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    if-eqz v0, :cond_1

    .line 450509
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->r:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 450510
    const v0, 0x7f0d0cac

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 450511
    const v2, 0x7f0d0cab

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/facepile/FacepileView;

    .line 450512
    iget-object v2, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/facebook/katana/orca/DiodeQpFragment;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 450513
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->c:LX/76b;

    new-instance v2, LX/FBJ;

    invoke-direct {v2, p0, v1}, LX/FBJ;-><init>(Lcom/facebook/katana/orca/DiodeQpFragment;Lcom/facebook/fbui/facepile/FacepileView;)V

    .line 450514
    iput-object v2, v0, LX/76b;->g:LX/3Mb;

    .line 450515
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->c:LX/76b;

    iget-object v1, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->friendIds:LX/0Px;

    invoke-virtual {v0, v1}, LX/76b;->a(Ljava/util/List;)V

    .line 450516
    :cond_1
    const v0, 0xe033646

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-void

    :cond_2
    move v0, v2

    .line 450517
    goto/16 :goto_1

    .line 450518
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iget-object v6, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v7

    sget-object v8, Lcom/facebook/katana/orca/DiodeQpFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v9, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->t:LX/1Ai;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/common/callercontext/CallerContext;LX/1Ai;)Z

    move-result v0

    .line 450519
    if-eqz v0, :cond_4

    .line 450520
    invoke-virtual {v4}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0, v4}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 450521
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 450522
    :goto_3
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->o:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v0, v2}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    goto :goto_2

    .line 450523
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_3

    :cond_5
    move v0, v1

    goto/16 :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4e6d31b2    # 9.9486426E8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 450485
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 450486
    const v1, 0x7f03041f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x43b0df90

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x25baac5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 450482
    invoke-super {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->onDestroyView()V

    .line 450483
    iget-object v1, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->e:LX/FkC;

    invoke-virtual {v1}, LX/FkC;->a()V

    .line 450484
    const/16 v1, 0x2b

    const v2, 0x1e60e1eb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 450471
    invoke-super {p0, p1, p2}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 450472
    const v0, 0x7f0d0ca2

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->l:Landroid/widget/ScrollView;

    .line 450473
    const v0, 0x7f0d0ca4

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->m:Landroid/widget/TextView;

    .line 450474
    const v0, 0x7f0d0ca5

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->n:Landroid/widget/TextView;

    .line 450475
    const v0, 0x7f0d0ca7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 450476
    const v0, 0x7f0d0ca6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->o:Lcom/facebook/user/tiles/UserTileView;

    .line 450477
    const v0, 0x7f0d0ca8

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->r:Landroid/view/ViewStub;

    .line 450478
    const v0, 0x7f0d0ca9

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->q:Landroid/widget/Button;

    .line 450479
    const v0, 0x7f0d0caa

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->s:Landroid/widget/TextView;

    .line 450480
    iget-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a()LX/1Ai;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/orca/DiodeQpFragment;->t:LX/1Ai;

    .line 450481
    return-void
.end method
