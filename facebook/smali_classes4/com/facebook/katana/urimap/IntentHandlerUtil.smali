.class public Lcom/facebook/katana/urimap/IntentHandlerUtil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:Lcom/facebook/katana/urimap/IntentHandlerUtil;


# instance fields
.field private final a:LX/2lN;

.field private final b:LX/17X;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/0aG;

.field private final f:LX/03V;

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/47S;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Zb;

.field private final i:LX/0gh;

.field private final j:LX/0id;

.field public final k:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>(LX/2lN;LX/17X;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/ExecutorService;LX/0aG;LX/03V;Ljava/util/Set;LX/0Zb;LX/0gh;LX/0id;Landroid/content/ComponentName;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p11    # Landroid/content/ComponentName;
        .annotation runtime Lcom/facebook/katana/login/LoginActivityComponent;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2lN;",
            "LX/17X;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0aG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/util/Set",
            "<",
            "LX/47S;",
            ">;",
            "LX/0Zb;",
            "LX/0gh;",
            "LX/0id;",
            "Landroid/content/ComponentName;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 457718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457719
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lN;

    iput-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a:LX/2lN;

    .line 457720
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17X;

    iput-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->b:LX/17X;

    .line 457721
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->c:Lcom/facebook/content/SecureContextHelper;

    .line 457722
    iput-object p4, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->d:Ljava/util/concurrent/ExecutorService;

    .line 457723
    iput-object p5, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->e:LX/0aG;

    .line 457724
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->f:LX/03V;

    .line 457725
    iput-object p7, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->g:Ljava/util/Set;

    .line 457726
    iput-object p8, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->h:LX/0Zb;

    .line 457727
    iput-object p9, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->i:LX/0gh;

    .line 457728
    iput-object p10, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->j:LX/0id;

    .line 457729
    iput-object p11, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->k:Landroid/content/ComponentName;

    .line 457730
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/katana/urimap/IntentHandlerUtil;
    .locals 15

    .prologue
    .line 457731
    sget-object v0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->l:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    if-nez v0, :cond_1

    .line 457732
    const-class v1, Lcom/facebook/katana/urimap/IntentHandlerUtil;

    monitor-enter v1

    .line 457733
    :try_start_0
    sget-object v0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->l:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 457734
    if-eqz v2, :cond_0

    .line 457735
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 457736
    new-instance v3, Lcom/facebook/katana/urimap/IntentHandlerUtil;

    invoke-static {v0}, LX/2lN;->a(LX/0QB;)LX/2lN;

    move-result-object v4

    check-cast v4, LX/2lN;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17X;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v8

    check-cast v8, LX/0aG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    .line 457737
    new-instance v10, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v11

    new-instance v12, LX/17t;

    invoke-direct {v12, v0}, LX/17t;-><init>(LX/0QB;)V

    invoke-direct {v10, v11, v12}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v10, v10

    .line 457738
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v11

    check-cast v11, LX/0Zb;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v12

    check-cast v12, LX/0gh;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v13

    check-cast v13, LX/0id;

    invoke-static {v0}, LX/27F;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v14

    check-cast v14, Landroid/content/ComponentName;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/katana/urimap/IntentHandlerUtil;-><init>(LX/2lN;LX/17X;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/ExecutorService;LX/0aG;LX/03V;Ljava/util/Set;LX/0Zb;LX/0gh;LX/0id;Landroid/content/ComponentName;)V

    .line 457739
    move-object v0, v3

    .line 457740
    sput-object v0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->l:Lcom/facebook/katana/urimap/IntentHandlerUtil;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457741
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 457742
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 457743
    :cond_1
    sget-object v0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->l:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    return-object v0

    .line 457744
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 457745
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Ljava/util/Map;)Z
    .locals 5
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 457746
    if-nez p3, :cond_0

    move v0, v2

    .line 457747
    :goto_0
    return v0

    .line 457748
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/47S;

    .line 457749
    invoke-interface {v0, p1, p2, p3, p4}, LX/47S;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Ljava/util/Map;)V

    goto :goto_1

    .line 457750
    :cond_1
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 457751
    if-eqz v0, :cond_2

    move-object v1, v0

    :goto_2
    invoke-direct {p0, v1, p3}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->b(Landroid/content/Context;Landroid/content/Intent;)V

    .line 457752
    :try_start_0
    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_3

    .line 457753
    invoke-direct {p0, p1, p3}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->c(Landroid/content/Context;Landroid/content/Intent;)V

    .line 457754
    :goto_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move-object v1, p1

    .line 457755
    goto :goto_2

    .line 457756
    :cond_3
    invoke-virtual {p3}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v1, 0x10000000

    or-int/2addr v0, v1

    invoke-virtual {p3, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 457757
    invoke-direct {p0, p1, p3}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->c(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 457758
    :catch_0
    move-exception v0

    .line 457759
    iget-object v1, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->f:LX/03V;

    const-string v3, "IntentHandlerUtil"

    const-string v4, "Exception caught while starting Activity"

    invoke-virtual {v1, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v2

    .line 457760
    goto :goto_0
.end method

.method public static a(Lcom/facebook/katana/urimap/IntentHandlerUtil;Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;Landroid/content/Intent;)Z
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/content/Intent;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 457761
    iget-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->j:LX/0id;

    const-string v1, "IntentHandlerUtil"

    invoke-virtual {v0, p1, v1}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 457762
    if-nez p5, :cond_0

    .line 457763
    iget-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->b:LX/17X;

    invoke-virtual {v0, p1, p2}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p5

    .line 457764
    :cond_0
    if-nez p5, :cond_1

    .line 457765
    iget-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->j:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    .line 457766
    const/4 v0, 0x0

    .line 457767
    :goto_0
    return v0

    .line 457768
    :cond_1
    if-eqz p3, :cond_2

    .line 457769
    invoke-virtual {p5, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 457770
    :cond_2
    const-string v0, "is_diode"

    invoke-virtual {p5, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 457771
    invoke-virtual {p5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 457772
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 457773
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 457774
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    instance-of p3, p3, Landroid/os/Parcelable;

    if-eqz p3, :cond_3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    instance-of p3, p3, LX/5Qp;

    if-nez p3, :cond_3

    .line 457775
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 457776
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 457777
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_2

    .line 457778
    :cond_5
    move-object v0, v0

    .line 457779
    invoke-virtual {p5, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 457780
    const-string v0, "trigger"

    invoke-virtual {p5, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 457781
    const-string v0, "trigger"

    invoke-virtual {p5, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 457782
    const-string v1, "diode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 457783
    const-string v1, "trigger"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "diode_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457784
    :cond_6
    :goto_3
    invoke-direct {p0, p1, p2, p5, p4}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Ljava/util/Map;)Z

    move-result v0

    goto/16 :goto_0

    .line 457785
    :cond_7
    const-string v0, "trigger"

    const-string v1, "diode"

    invoke-virtual {p5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3
.end method

.method private b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 457786
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 457787
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457788
    iget-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->c:Lcom/facebook/content/SecureContextHelper;

    const/4 v1, 0x1

    .line 457789
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object p2, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->k:Landroid/content/ComponentName;

    invoke-virtual {v2, p2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    const/high16 p2, 0x10200000

    invoke-virtual {v2, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    const-string p2, "android.intent.action.MAIN"

    invoke-virtual {v2, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string p2, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, p2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string p2, "finish_immediately"

    invoke-virtual {v2, p2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    move-object v1, v2

    .line 457790
    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 457791
    :cond_0
    return-void
.end method

.method private c(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 457792
    const-string v0, "request_code"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    .line 457793
    const-string v0, "request_code"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 457794
    if-eqz v1, :cond_0

    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 457795
    :goto_0
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 457796
    invoke-static {p1, p2}, LX/2A3;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 457797
    iget-object v1, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, p2, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 457798
    :goto_1
    return-void

    .line 457799
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 457800
    :cond_1
    invoke-static {p1, p2}, LX/2A3;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 457801
    iget-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 457802
    :goto_2
    goto :goto_1

    .line 457803
    :cond_2
    iget-object v1, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, p2, v2, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_1

    .line 457804
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p2, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    .prologue
    .line 457805
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 457806
    const/4 v0, 0x0

    .line 457807
    if-nez v1, :cond_8

    .line 457808
    :cond_0
    :goto_0
    move v0, v0

    .line 457809
    if-eqz v0, :cond_2

    .line 457810
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 457811
    const-string v5, "url"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 457812
    iget-object v5, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->e:LX/0aG;

    const-string v6, "fetchNotificationURI"

    sget-object v8, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v9, Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-static {v9}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v9

    const v10, 0x2d1b59b8

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    .line 457813
    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    move-result-object v5

    .line 457814
    new-instance v6, LX/2tE;

    invoke-direct {v6, p0, p1}, LX/2tE;-><init>(Lcom/facebook/katana/urimap/IntentHandlerUtil;Landroid/content/Context;)V

    iget-object v7, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 457815
    :cond_1
    :goto_1
    return-void

    .line 457816
    :cond_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 457817
    const-string v2, "trigger"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 457818
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 457819
    sget-object v4, LX/0ax;->ah:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v2, :cond_3

    const-string v4, "divebar"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 457820
    const-string v2, "is_from_fb4a"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 457821
    const-string v2, "trigger"

    const-string v4, "tap_dive_bar"

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 457822
    :cond_3
    const/4 v9, 0x0

    .line 457823
    move-object v5, p0

    move-object v6, p1

    move-object v7, v0

    move-object v8, v3

    move-object v10, v9

    invoke-static/range {v5 .. v10}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Lcom/facebook/katana/urimap/IntentHandlerUtil;Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;Landroid/content/Intent;)Z

    move-result v5

    move v0, v5

    .line 457824
    if-nez v0, :cond_6

    .line 457825
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_5

    const-string v1, "al_applink_data"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 457826
    const-string v1, "al_applink_data"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 457827
    if-eqz v1, :cond_5

    const-string v2, "target_url"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 457828
    const-string v2, "target_url"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 457829
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, LX/2lN;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 457830
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 457831
    iget-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->b:LX/17X;

    invoke-static {v0, p1, v3}, LX/2lN;->a(LX/17X;Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 457832
    const/4 v3, 0x0

    invoke-direct {p0, p1, v2, v0, v3}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Ljava/util/Map;)Z

    move-result v0

    .line 457833
    if-eqz v0, :cond_5

    .line 457834
    const-string v3, "extras"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 457835
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "applink_navigation_event"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "target_url"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 457836
    const-string v3, ""

    .line 457837
    if-eqz v1, :cond_4

    const-string v5, "ref"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 457838
    :cond_4
    const-string v5, "ref"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 457839
    :goto_2
    move-object v1, v4

    .line 457840
    iget-object v2, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->h:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 457841
    :cond_5
    if-nez v0, :cond_1

    .line 457842
    sget-object v0, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_1

    .line 457843
    :cond_6
    const-string v0, "shortcut_open"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 457844
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "open_shortcut"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "uri"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 457845
    iget-object v1, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->h:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 457846
    iget-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->i:LX/0gh;

    const-string v1, "tap_shortcut"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto/16 :goto_1

    .line 457847
    :cond_7
    iget-object v0, p0, Lcom/facebook/katana/urimap/IntentHandlerUtil;->i:LX/0gh;

    const-string v1, "from_other_app"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto/16 :goto_1

    .line 457848
    :cond_8
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 457849
    if-eqz v2, :cond_0

    const-string v3, "http"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string v3, "https"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 457850
    :cond_9
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 457851
    if-eqz v2, :cond_0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ".fb.com"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 457852
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 457853
    if-eqz v2, :cond_0

    const-string v3, "/l/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 457854
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 457855
    :cond_a
    const-string v3, "ref"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 457856
    if-nez v3, :cond_b

    const-string v3, ""

    .line 457857
    :cond_b
    const-string v5, "ref"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_2
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 457858
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Lcom/facebook/katana/urimap/IntentHandlerUtil;Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
