.class public Lcom/facebook/spherical/ui/SphericalNuxAnimationController;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final a:Landroid/view/animation/Interpolator;

.field public b:LX/7Nl;

.field public c:Landroid/animation/AnimatorSet;

.field public d:Landroid/animation/AnimatorSet;

.field public e:Landroid/animation/ObjectAnimator;

.field public f:Landroid/animation/ObjectAnimator;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 546891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 546892
    const v0, 0x3e6b851f    # 0.23f

    const v1, 0x3ea3d70a    # 0.32f

    invoke-static {v0, v2, v1, v2}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a:Landroid/view/animation/Interpolator;

    .line 546893
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->g:Z

    .line 546894
    return-void

    .line 546895
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 546888
    invoke-virtual {p0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c()V

    .line 546889
    invoke-virtual {p0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->b()V

    .line 546890
    return-void
.end method

.method public final a(Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;JJJI)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 546872
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    .line 546873
    invoke-virtual {p1, v3}, Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;->setAlpha(F)V

    .line 546874
    const-string v0, "alpha"

    new-array v1, v5, [F

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v1, v4

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 546875
    iget-object v1, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546876
    invoke-virtual {v0, p2, p3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 546877
    const-string v1, "alpha"

    new-array v2, v5, [F

    aput v3, v2, v4

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 546878
    iget-object v2, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546879
    invoke-virtual {v1, p4, p5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 546880
    new-instance v2, LX/3K5;

    invoke-direct {v2, p0}, LX/3K5;-><init>(Lcom/facebook/spherical/ui/SphericalNuxAnimationController;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 546881
    invoke-virtual {p1, p6, p7, p8}, Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;->a(JI)V

    .line 546882
    iget-object v2, p1, Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;->a:Landroid/animation/ObjectAnimator;

    move-object v2, v2

    .line 546883
    iput-object v2, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->e:Landroid/animation/ObjectAnimator;

    .line 546884
    iget-object v2, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 546885
    iget-object v2, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    iget-object v3, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 546886
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 546887
    return-void
.end method

.method public final a(Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;JJJJ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 546856
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    .line 546857
    invoke-virtual {p1, v4}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->setAlpha(F)V

    .line 546858
    const-string v0, "alpha"

    new-array v1, v3, [F

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v1, v5

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 546859
    iget-object v1, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546860
    invoke-virtual {v0, p2, p3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 546861
    const-string v1, "alpha"

    new-array v2, v3, [F

    aput v4, v2, v5

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 546862
    iget-object v2, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546863
    invoke-virtual {v1, p4, p5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 546864
    invoke-virtual {p1, p6, p7, v3}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(JI)V

    .line 546865
    iget-object v2, p1, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->s:Landroid/animation/ObjectAnimator;

    move-object v2, v2

    .line 546866
    iput-object v2, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->f:Landroid/animation/ObjectAnimator;

    .line 546867
    iget-object v2, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 546868
    iget-object v2, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    iget-object v3, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 546869
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 546870
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p8, p9}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 546871
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 546832
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 546833
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 546834
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 546853
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 546854
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 546855
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 546850
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 546851
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 546852
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 546847
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 546848
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 546849
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 546841
    iget-boolean v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->g:Z

    if-eqz v0, :cond_1

    .line 546842
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546843
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->pause()V

    .line 546844
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546845
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->pause()V

    .line 546846
    :cond_1
    return-void
.end method

.method public final j()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 546835
    iget-boolean v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->g:Z

    if-eqz v0, :cond_1

    .line 546836
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546837
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->resume()V

    .line 546838
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546839
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->resume()V

    .line 546840
    :cond_1
    return-void
.end method
