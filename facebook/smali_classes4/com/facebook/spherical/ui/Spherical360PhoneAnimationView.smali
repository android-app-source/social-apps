.class public Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;
.super Landroid/view/View;
.source ""


# instance fields
.field private A:Landroid/graphics/RectF;

.field private B:Ljava/lang/String;

.field private C:Landroid/graphics/Typeface;

.field private D:Landroid/graphics/Path;

.field private E:Landroid/graphics/Path;

.field private F:Landroid/graphics/Path;

.field private G:Landroid/graphics/Path;

.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field public j:I

.field private k:I

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field public s:Landroid/animation/ObjectAnimator;

.field private t:Landroid/graphics/Paint;

.field private u:Landroid/graphics/Paint;

.field private v:Landroid/graphics/Paint;

.field private w:Landroid/graphics/CornerPathEffect;

.field private x:Landroid/graphics/CornerPathEffect;

.field private y:Landroid/graphics/CornerPathEffect;

.field private z:Landroid/graphics/CornerPathEffect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 546677
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 546678
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 546829
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546830
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/16 v2, 0x10

    const/16 v1, 0xc

    .line 546816
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546817
    const/16 v0, 0x44

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a:I

    .line 546818
    iput v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->b:I

    .line 546819
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->c:I

    .line 546820
    const/4 v0, 0x4

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->d:I

    .line 546821
    const/16 v0, 0x24

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->e:I

    .line 546822
    const/16 v0, 0x18

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->f:I

    .line 546823
    const/16 v0, 0x19

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->g:I

    .line 546824
    iput v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->h:I

    .line 546825
    iput v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->i:I

    .line 546826
    iput v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->j:I

    .line 546827
    invoke-direct {p0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a()V

    .line 546828
    return-void
.end method

.method private a(F)I
    .locals 1

    .prologue
    .line 546815
    invoke-virtual {p0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    return v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 546794
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-direct {p0, v0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->o:F

    .line 546795
    const/high16 v0, 0x40800000    # 4.0f

    invoke-direct {p0, v0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->p:F

    .line 546796
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->q:F

    .line 546797
    const/high16 v0, 0x40000000    # 2.0f

    invoke-direct {p0, v0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->r:F

    .line 546798
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->t:Landroid/graphics/Paint;

    .line 546799
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->u:Landroid/graphics/Paint;

    .line 546800
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    .line 546801
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->A:Landroid/graphics/RectF;

    .line 546802
    const/high16 v0, 0x41c80000    # 25.0f

    invoke-direct {p0, v0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->k:I

    .line 546803
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    .line 546804
    invoke-virtual {p0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080d50

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->B:Ljava/lang/String;

    .line 546805
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->D:Landroid/graphics/Path;

    .line 546806
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->E:Landroid/graphics/Path;

    .line 546807
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->F:Landroid/graphics/Path;

    .line 546808
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->G:Landroid/graphics/Path;

    .line 546809
    new-instance v0, Landroid/graphics/CornerPathEffect;

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->o:F

    invoke-direct {v0, v1}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->w:Landroid/graphics/CornerPathEffect;

    .line 546810
    new-instance v0, Landroid/graphics/CornerPathEffect;

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->p:F

    invoke-direct {v0, v1}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->x:Landroid/graphics/CornerPathEffect;

    .line 546811
    new-instance v0, Landroid/graphics/CornerPathEffect;

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->q:F

    invoke-direct {v0, v1}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->y:Landroid/graphics/CornerPathEffect;

    .line 546812
    new-instance v0, Landroid/graphics/CornerPathEffect;

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->r:F

    invoke-direct {v0, v1}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->z:Landroid/graphics/CornerPathEffect;

    .line 546813
    const-string v0, "roboto-medium"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->C:Landroid/graphics/Typeface;

    .line 546814
    return-void
.end method


# virtual methods
.method public final a(JI)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 546787
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    .line 546788
    const-string v0, "offset"

    const/4 v1, 0x2

    new-array v1, v1, [F

    iget v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    aput v2, v1, v4

    const/high16 v2, 0x42c80000    # 100.0f

    aput v2, v1, v3

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 546789
    new-array v1, v3, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v1, v4

    invoke-static {p0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->s:Landroid/animation/ObjectAnimator;

    .line 546790
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->s:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 546791
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->s:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 546792
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->s:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p3}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 546793
    return-void
.end method

.method public getPhoneAnimator()Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 546831
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->s:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method public getVerticalOffset()I
    .locals 1

    .prologue
    .line 546786
    iget v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->j:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    .line 546697
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 546698
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->C:Landroid/graphics/Typeface;

    if-eqz v0, :cond_0

    .line 546699
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->u:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->C:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 546700
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->u:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-direct {p0, v1}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 546701
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->u:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 546702
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->u:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 546703
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->u:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 546704
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->u:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 546705
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->u:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-direct {p0, v1}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {p0, v2}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {p0, v3}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, -0x1000000

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 546706
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->B:Ljava/lang/String;

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->l:F

    iget v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->m:F

    iget v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->j:I

    int-to-float v3, v3

    invoke-direct {p0, v3}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->u:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 546707
    iget v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->j:I

    add-int/lit8 v0, v0, 0x42

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v0

    .line 546708
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 546709
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 546710
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->w:Landroid/graphics/CornerPathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 546711
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 546712
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    const/16 v2, 0x44

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 546713
    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v1, v2

    .line 546714
    iget v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->l:F

    iget v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v4, 0x41a00000    # 20.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    .line 546715
    iget v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->l:F

    const/high16 v4, 0x41c00000    # 24.0f

    invoke-direct {p0, v4}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v5, 0x41a00000    # 20.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v5, 0x41200000    # 10.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sub-float/2addr v3, v4

    .line 546716
    iget v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->m:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    .line 546717
    iget v5, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->m:F

    const/high16 v6, 0x42100000    # 36.0f

    invoke-direct {p0, v6}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    int-to-float v6, v0

    sub-float/2addr v5, v6

    .line 546718
    sub-float v6, v3, v2

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    .line 546719
    sub-float/2addr v2, v6

    .line 546720
    sub-float/2addr v3, v6

    .line 546721
    const/high16 v6, 0x40000000    # 2.0f

    invoke-direct {p0, v6}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v6

    int-to-float v6, v6

    .line 546722
    iget-object v7, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->D:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->rewind()V

    .line 546723
    iget-object v7, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->D:Landroid/graphics/Path;

    sub-float v8, v2, v6

    add-float v9, v4, v1

    sub-float/2addr v9, v6

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 546724
    iget-object v7, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->D:Landroid/graphics/Path;

    add-float v8, v3, v6

    sub-float/2addr v4, v1

    sub-float/2addr v4, v6

    invoke-virtual {v7, v8, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546725
    iget-object v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->D:Landroid/graphics/Path;

    add-float/2addr v3, v6

    add-float v7, v5, v1

    add-float/2addr v7, v6

    invoke-virtual {v4, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546726
    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->D:Landroid/graphics/Path;

    sub-float/2addr v2, v6

    sub-float v1, v5, v1

    add-float/2addr v1, v6

    invoke-virtual {v3, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546727
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->D:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 546728
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->D:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 546729
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 546730
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 546731
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->x:Landroid/graphics/CornerPathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 546732
    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 546733
    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v2, 0x41200000    # 10.0f

    div-float/2addr v1, v2

    .line 546734
    iget v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->l:F

    iget v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v4, 0x41a00000    # 20.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    .line 546735
    iget v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->l:F

    const/high16 v4, 0x41c00000    # 24.0f

    invoke-direct {p0, v4}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v5, 0x41a00000    # 20.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v5, 0x41200000    # 10.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sub-float/2addr v3, v4

    .line 546736
    iget v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->m:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    .line 546737
    iget v5, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->m:F

    const/high16 v6, 0x42100000    # 36.0f

    invoke-direct {p0, v6}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    int-to-float v6, v0

    sub-float/2addr v5, v6

    .line 546738
    sub-float v6, v3, v2

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    .line 546739
    sub-float/2addr v2, v6

    .line 546740
    sub-float/2addr v3, v6

    .line 546741
    iget-object v7, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->E:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->rewind()V

    .line 546742
    iget-object v7, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->E:Landroid/graphics/Path;

    add-float v8, v4, v1

    invoke-virtual {v7, v2, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 546743
    iget-object v7, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->E:Landroid/graphics/Path;

    sub-float/2addr v4, v1

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546744
    iget-object v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->E:Landroid/graphics/Path;

    add-float v7, v5, v1

    invoke-virtual {v4, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546745
    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->E:Landroid/graphics/Path;

    sub-float v4, v5, v1

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546746
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->E:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 546747
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->E:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 546748
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->t:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 546749
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->t:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a03c8

    invoke-static {v3, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 546750
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->t:Landroid/graphics/Paint;

    const/16 v3, 0xd8

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 546751
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->t:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->y:Landroid/graphics/CornerPathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 546752
    const v2, 0x3f451eb8    # 0.77f

    mul-float/2addr v2, v1

    const/high16 v3, 0x3f400000    # 0.75f

    mul-float/2addr v2, v3

    .line 546753
    const v3, 0x3f1c28f6    # 0.61f

    mul-float/2addr v3, v1

    const/high16 v4, 0x3f400000    # 0.75f

    mul-float/2addr v3, v4

    .line 546754
    iget v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->l:F

    iget v5, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v7, 0x41a00000    # 20.0f

    div-float/2addr v5, v7

    add-float/2addr v4, v5

    const/high16 v5, 0x40800000    # 4.0f

    invoke-direct {p0, v5}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v7, 0x41200000    # 10.0f

    div-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    add-float/2addr v4, v5

    .line 546755
    iget v5, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->l:F

    const/high16 v7, 0x41800000    # 16.0f

    invoke-direct {p0, v7}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v5, v7

    iget v7, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    add-float/2addr v5, v7

    iget v7, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v8, 0x41a00000    # 20.0f

    div-float/2addr v7, v8

    add-float/2addr v5, v7

    const/high16 v7, 0x40800000    # 4.0f

    invoke-direct {p0, v7}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v5, v7

    iget v7, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v8, 0x41200000    # 10.0f

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    sub-float/2addr v5, v7

    .line 546756
    iget v7, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->m:F

    int-to-float v8, v0

    sub-float/2addr v7, v8

    const/high16 v8, 0x40800000    # 4.0f

    invoke-direct {p0, v8}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    .line 546757
    iget v8, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->m:F

    const/high16 v9, 0x41c80000    # 25.0f

    invoke-direct {p0, v9}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v9

    int-to-float v9, v9

    add-float/2addr v8, v9

    int-to-float v0, v0

    sub-float v0, v8, v0

    const/high16 v8, 0x40800000    # 4.0f

    invoke-direct {p0, v8}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v0, v8

    .line 546758
    sub-float/2addr v4, v6

    .line 546759
    sub-float/2addr v5, v6

    .line 546760
    iget-object v6, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->F:Landroid/graphics/Path;

    invoke-virtual {v6}, Landroid/graphics/Path;->rewind()V

    .line 546761
    iget-object v6, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->F:Landroid/graphics/Path;

    add-float v8, v7, v2

    invoke-virtual {v6, v4, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 546762
    iget-object v6, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->F:Landroid/graphics/Path;

    sub-float v2, v7, v2

    invoke-virtual {v6, v5, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546763
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->F:Landroid/graphics/Path;

    add-float v6, v0, v3

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546764
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->F:Landroid/graphics/Path;

    sub-float v3, v0, v3

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546765
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->F:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 546766
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->F:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 546767
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->t:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->z:Landroid/graphics/CornerPathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 546768
    const v2, 0x3de147ae    # 0.11f

    mul-float/2addr v1, v2

    .line 546769
    add-float v2, v4, v5

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    .line 546770
    const/high16 v3, 0x40e00000    # 7.0f

    invoke-direct {p0, v3}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->a(F)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v0, v3

    .line 546771
    iget v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->r:F

    .line 546772
    iget v4, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->r:F

    iget v5, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    const/high16 v6, 0x41f00000    # 30.0f

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    sub-float/2addr v4, v5

    .line 546773
    sub-float v5, v2, v4

    .line 546774
    add-float/2addr v2, v4

    .line 546775
    sub-float v4, v0, v3

    .line 546776
    add-float/2addr v0, v3

    .line 546777
    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->G:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->rewind()V

    .line 546778
    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->G:Landroid/graphics/Path;

    add-float v6, v4, v1

    invoke-virtual {v3, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 546779
    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->G:Landroid/graphics/Path;

    sub-float/2addr v4, v1

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546780
    iget-object v3, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->G:Landroid/graphics/Path;

    add-float v4, v0, v1

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546781
    iget-object v2, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->G:Landroid/graphics/Path;

    sub-float/2addr v0, v1

    invoke-virtual {v2, v5, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546782
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->G:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 546783
    iget-object v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->G:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 546784
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 546785
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 546690
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 546691
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 546692
    invoke-virtual {p0, v0, v1}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->setMeasuredDimension(II)V

    .line 546693
    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->l:F

    .line 546694
    int-to-float v0, v1

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->m:F

    .line 546695
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 546696
    return-void
.end method

.method public setOffset(F)V
    .locals 4

    .prologue
    const/high16 v3, 0x42960000    # 75.0f

    const/high16 v2, 0x42480000    # 50.0f

    const/high16 v1, 0x41c80000    # 25.0f

    .line 546681
    cmpg-float v0, p1, v1

    if-gtz v0, :cond_0

    .line 546682
    div-float v0, p1, v1

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->k:I

    neg-int v1, v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    .line 546683
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->invalidate()V

    .line 546684
    return-void

    .line 546685
    :cond_0
    cmpg-float v0, p1, v2

    if-gtz v0, :cond_1

    .line 546686
    sub-float v0, p1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->k:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->k:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    goto :goto_0

    .line 546687
    :cond_1
    cmpg-float v0, p1, v3

    if-gtz v0, :cond_2

    .line 546688
    sub-float v0, p1, v2

    div-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->k:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    goto :goto_0

    .line 546689
    :cond_2
    sub-float v0, p1, v3

    div-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->k:I

    neg-int v1, v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->k:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->n:F

    goto :goto_0
.end method

.method public setVerticalOffset(I)V
    .locals 0

    .prologue
    .line 546679
    iput p1, p0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->j:I

    .line 546680
    return-void
.end method
