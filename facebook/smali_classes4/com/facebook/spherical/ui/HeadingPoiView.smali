.class public Lcom/facebook/spherical/ui/HeadingPoiView;
.super LX/3IQ;
.source ""


# instance fields
.field private b:Landroid/graphics/RectF;

.field private c:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 546423
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/spherical/ui/HeadingPoiView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 546424
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 546425
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/spherical/ui/HeadingPoiView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546426
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 546427
    invoke-direct {p0, p1, p2, p3}, LX/3IQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546428
    invoke-direct {p0}, Lcom/facebook/spherical/ui/HeadingPoiView;->c()V

    .line 546429
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->b:Landroid/graphics/RectF;

    .line 546430
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 546431
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->c:Landroid/graphics/Paint;

    .line 546432
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 546433
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->c:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 546434
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 546435
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->c:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p0, v1}, LX/3IQ;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 546436
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v3, 0x41840000    # 16.5f

    .line 546437
    invoke-super {p0, p1}, LX/3IQ;->onDraw(Landroid/graphics/Canvas;)V

    .line 546438
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->b:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v3}, LX/3IQ;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 546439
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->b:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v3}, LX/3IQ;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 546440
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->b:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v3}, LX/3IQ;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 546441
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->b:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v3}, LX/3IQ;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 546442
    iget-object v1, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->b:Landroid/graphics/RectF;

    const/high16 v2, -0x3d2e0000    # -105.0f

    const/high16 v3, 0x41f00000    # 30.0f

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/spherical/ui/HeadingPoiView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 546443
    return-void
.end method
