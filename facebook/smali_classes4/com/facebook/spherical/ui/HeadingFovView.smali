.class public Lcom/facebook/spherical/ui/HeadingFovView;
.super LX/3IQ;
.source ""


# instance fields
.field private b:F

.field private c:F

.field private d:F

.field private e:Landroid/graphics/RectF;

.field private f:Landroid/graphics/Path;

.field private g:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 546413
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/spherical/ui/HeadingFovView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 546414
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 546415
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/spherical/ui/HeadingFovView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546416
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 546417
    invoke-direct {p0, p1, p2, p3}, LX/3IQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546418
    const/high16 v0, 0x428c0000    # 70.0f

    iput v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->b:F

    .line 546419
    invoke-direct {p0}, Lcom/facebook/spherical/ui/HeadingFovView;->c()V

    .line 546420
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    .line 546421
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->f:Landroid/graphics/Path;

    .line 546422
    return-void
.end method

.method private static a(FFF)F
    .locals 6

    .prologue
    .line 546409
    float-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    .line 546410
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    float-to-double v4, p0

    mul-double/2addr v2, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    float-to-double v4, p1

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private static b(FFF)F
    .locals 6

    .prologue
    .line 546411
    float-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    .line 546412
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    neg-double v2, v2

    float-to-double v4, p0

    mul-double/2addr v2, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    float-to-double v4, p1

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 546405
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    .line 546406
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 546407
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 546408
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/high16 v12, 0x41400000    # 12.0f

    const/high16 v11, 0x40c80000    # 6.25f

    const v10, 0x3fd9999a    # 1.7f

    const/4 v8, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 546350
    invoke-super {p0, p1}, LX/3IQ;->onDraw(Landroid/graphics/Canvas;)V

    .line 546351
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 546352
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->f:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 546353
    const/high16 v0, 0x41680000    # 14.5f

    invoke-virtual {p0, v0}, LX/3IQ;->a(F)F

    move-result v0

    .line 546354
    const/high16 v1, 0x40200000    # 2.5f

    invoke-virtual {p0, v1}, LX/3IQ;->a(F)F

    move-result v1

    add-float/2addr v1, v8

    .line 546355
    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {p0, v2}, LX/3IQ;->a(F)F

    move-result v2

    sub-float v2, v0, v2

    .line 546356
    const/high16 v3, 0x40200000    # 2.5f

    invoke-virtual {p0, v3}, LX/3IQ;->a(F)F

    move-result v3

    sub-float v3, v8, v3

    .line 546357
    const/high16 v4, 0x40400000    # 3.0f

    invoke-virtual {p0, v4}, LX/3IQ;->a(F)F

    move-result v4

    sub-float v4, v0, v4

    .line 546358
    iget v5, p0, Lcom/facebook/spherical/ui/HeadingFovView;->d:F

    invoke-static {v8, v0, v5}, Lcom/facebook/spherical/ui/HeadingFovView;->a(FFF)F

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    add-float/2addr v5, v6

    .line 546359
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    iget v7, p0, Lcom/facebook/spherical/ui/HeadingFovView;->d:F

    invoke-static {v8, v0, v7}, Lcom/facebook/spherical/ui/HeadingFovView;->b(FFF)F

    move-result v0

    sub-float v0, v6, v0

    .line 546360
    iget v6, p0, Lcom/facebook/spherical/ui/HeadingFovView;->d:F

    invoke-static {v1, v2, v6}, Lcom/facebook/spherical/ui/HeadingFovView;->a(FFF)F

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    add-float/2addr v6, v7

    .line 546361
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    iget v8, p0, Lcom/facebook/spherical/ui/HeadingFovView;->d:F

    invoke-static {v1, v2, v8}, Lcom/facebook/spherical/ui/HeadingFovView;->b(FFF)F

    move-result v1

    sub-float v1, v7, v1

    .line 546362
    iget v2, p0, Lcom/facebook/spherical/ui/HeadingFovView;->d:F

    invoke-static {v3, v4, v2}, Lcom/facebook/spherical/ui/HeadingFovView;->a(FFF)F

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    add-float/2addr v2, v7

    .line 546363
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    iget v8, p0, Lcom/facebook/spherical/ui/HeadingFovView;->d:F

    invoke-static {v3, v4, v8}, Lcom/facebook/spherical/ui/HeadingFovView;->b(FFF)F

    move-result v3

    sub-float v3, v7, v3

    .line 546364
    iget-object v4, p0, Lcom/facebook/spherical/ui/HeadingFovView;->f:Landroid/graphics/Path;

    invoke-virtual {v4, v5, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 546365
    iget-object v4, p0, Lcom/facebook/spherical/ui/HeadingFovView;->f:Landroid/graphics/Path;

    invoke-virtual {v4, v6, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546366
    iget-object v1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->f:Landroid/graphics/Path;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546367
    iget-object v1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->f:Landroid/graphics/Path;

    invoke-virtual {v1, v5, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546368
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->f:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 546369
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->f:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 546370
    iget-boolean v0, p0, LX/3IQ;->a:Z

    if-eqz v0, :cond_0

    .line 546371
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 546372
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/high16 v2, 0x41480000    # 12.5f

    invoke-virtual {p0, v2}, LX/3IQ;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 546373
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/high16 v2, 0x41480000    # 12.5f

    invoke-virtual {p0, v2}, LX/3IQ;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 546374
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/high16 v2, 0x41480000    # 12.5f

    invoke-virtual {p0, v2}, LX/3IQ;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 546375
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/high16 v2, 0x41480000    # 12.5f

    invoke-virtual {p0, v2}, LX/3IQ;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 546376
    :goto_0
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 546377
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 546378
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    const v1, 0x40b66666    # 5.7f

    invoke-virtual {p0, v1}, LX/3IQ;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 546379
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v11}, LX/3IQ;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 546380
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v11}, LX/3IQ;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 546381
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v11}, LX/3IQ;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 546382
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v11}, LX/3IQ;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 546383
    iget-boolean v0, p0, LX/3IQ;->a:Z

    if-eqz v0, :cond_1

    .line 546384
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 546385
    iget-object v1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    iget v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->c:F

    const/high16 v2, 0x42b40000    # 90.0f

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/facebook/spherical/ui/HeadingFovView;->b:F

    mul-float/2addr v2, v10

    div-float/2addr v2, v9

    sub-float v2, v0, v2

    iget v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->b:F

    mul-float v3, v0, v10

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 546386
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 546387
    :goto_1
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 546388
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v9}, LX/3IQ;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 546389
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v9}, LX/3IQ;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 546390
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v9}, LX/3IQ;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 546391
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v9}, LX/3IQ;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 546392
    iget-boolean v0, p0, LX/3IQ;->a:Z

    if-eqz v0, :cond_2

    .line 546393
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 546394
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 546395
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 546396
    :goto_2
    return-void

    .line 546397
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 546398
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1}, LX/3IQ;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 546399
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v12}, LX/3IQ;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 546400
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v12}, LX/3IQ;->a(F)F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 546401
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v12}, LX/3IQ;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 546402
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v12}, LX/3IQ;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 546403
    :cond_1
    iget-object v1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    iget v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->c:F

    const/high16 v2, 0x42b40000    # 90.0f

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/facebook/spherical/ui/HeadingFovView;->b:F

    mul-float/2addr v2, v10

    div-float/2addr v2, v9

    sub-float v2, v0, v2

    iget v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->b:F

    mul-float v3, v0, v10

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 546404
    :cond_2
    iget-object v0, p0, Lcom/facebook/spherical/ui/HeadingFovView;->e:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_2
.end method

.method public setCompassYaw(F)V
    .locals 0

    .prologue
    .line 546347
    iput p1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->d:F

    .line 546348
    invoke-virtual {p0}, Lcom/facebook/spherical/ui/HeadingFovView;->invalidate()V

    .line 546349
    return-void
.end method

.method public setFov(F)V
    .locals 0

    .prologue
    .line 546341
    iput p1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->b:F

    .line 546342
    invoke-virtual {p0}, Lcom/facebook/spherical/ui/HeadingFovView;->invalidate()V

    .line 546343
    return-void
.end method

.method public setFovYaw(F)V
    .locals 0

    .prologue
    .line 546344
    iput p1, p0, Lcom/facebook/spherical/ui/HeadingFovView;->c:F

    .line 546345
    invoke-virtual {p0}, Lcom/facebook/spherical/ui/HeadingFovView;->invalidate()V

    .line 546346
    return-void
.end method
