.class public Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:LX/15W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7DE;

.field public c:Lcom/facebook/spherical/ui/HeadingBackgroundView;

.field public d:Lcom/facebook/spherical/ui/HeadingFovView;

.field public e:Lcom/facebook/spherical/ui/HeadingPoiView;

.field private f:Landroid/animation/ValueAnimator;

.field private g:Landroid/animation/ValueAnimator;

.field private h:Landroid/animation/ValueAnimator;

.field private i:Landroid/animation/ValueAnimator;

.field public j:LX/3Ia;

.field private k:Landroid/view/View$OnClickListener;

.field private l:LX/7E6;

.field private m:LX/3IR;

.field private n:LX/7E7;

.field private o:LX/3IS;

.field public p:LX/3E2;

.field public q:Z

.field private r:Z

.field private s:F

.field private t:F

.field private u:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 546309
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 546310
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 546307
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546308
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 546302
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546303
    iput-boolean v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->q:Z

    .line 546304
    iput-boolean v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->r:Z

    .line 546305
    invoke-direct {p0, p1}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(Landroid/content/Context;)V

    .line 546306
    return-void
.end method

.method private a(F)F
    .locals 1

    .prologue
    .line 546297
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b:LX/7DE;

    move-object v0, v0

    .line 546298
    if-nez v0, :cond_0

    .line 546299
    :goto_0
    return p1

    .line 546300
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b:LX/7DE;

    move-object v0, v0

    .line 546301
    invoke-interface {v0}, LX/7DE;->c()F

    move-result v0

    sub-float/2addr p1, v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 7

    .prologue
    const-wide/16 v2, 0x578

    .line 546286
    const-class v0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-static {v0, p0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 546287
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031395

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 546288
    const v0, 0x7f0d2d3e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/ui/HeadingBackgroundView;

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c:Lcom/facebook/spherical/ui/HeadingBackgroundView;

    .line 546289
    const v0, 0x7f0d2d3f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/ui/HeadingFovView;

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    .line 546290
    const v0, 0x7f0d2d40

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/ui/HeadingPoiView;

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    .line 546291
    new-instance v0, LX/3E2;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, LX/3E2;-><init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;JJ)V

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->p:LX/3E2;

    .line 546292
    new-instance v0, LX/3IR;

    invoke-direct {v0, p0}, LX/3IR;-><init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;)V

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->m:LX/3IR;

    .line 546293
    new-instance v0, LX/3IS;

    invoke-direct {v0, p0}, LX/3IS;-><init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;)V

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->o:LX/3IS;

    .line 546294
    invoke-direct {p0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->g()V

    .line 546295
    invoke-direct {p0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f()V

    .line 546296
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 546280
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 546281
    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 546282
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 546283
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 546284
    :cond_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 546285
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-static {v0}, LX/15W;->b(LX/0QB;)LX/15W;

    move-result-object v0

    check-cast v0, LX/15W;

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a:LX/15W;

    return-void
.end method

.method private b(FF)V
    .locals 4

    .prologue
    .line 546272
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->g:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 546273
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546274
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->g:Landroid/animation/ValueAnimator;

    .line 546275
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->g:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546276
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->g:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 546277
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->g:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->m:LX/3IR;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 546278
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 546279
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 546270
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c:Lcom/facebook/spherical/ui/HeadingBackgroundView;

    invoke-virtual {p0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b064a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 546271
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 546268
    new-instance v0, LX/3IT;

    invoke-direct {v0, p0}, LX/3IT;-><init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;)V

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->k:Landroid/view/View$OnClickListener;

    .line 546269
    return-void
.end method

.method private getDefaultFov()F
    .locals 2

    .prologue
    .line 546259
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b:LX/7DE;

    move-object v0, v0

    .line 546260
    if-nez v0, :cond_1

    .line 546261
    const/high16 v0, 0x428c0000    # 70.0f

    .line 546262
    :cond_0
    :goto_0
    return v0

    .line 546263
    :cond_1
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b:LX/7DE;

    move-object v0, v0

    .line 546264
    invoke-interface {v0}, LX/7DE;->b()F

    move-result v0

    .line 546265
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 546266
    const/high16 v1, 0x42f00000    # 120.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 546267
    const/high16 v1, 0x42200000    # 40.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0
.end method

.method private getSphericalParams()LX/7DE;
    .locals 1

    .prologue
    .line 546311
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b:LX/7DE;

    return-object v0
.end method

.method public static h(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;)V
    .locals 2

    .prologue
    .line 546166
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b(FF)V

    .line 546167
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 546257
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, v1}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b(FF)V

    .line 546258
    return-void
.end method

.method private j()V
    .locals 5

    .prologue
    .line 546168
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a:LX/15W;

    invoke-virtual {p0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->HEADING_INDICATOR_SHOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/3lF;

    iget-object v4, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c:Lcom/facebook/spherical/ui/HeadingBackgroundView;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 546169
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 546170
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->j:LX/3Ia;

    if-nez v0, :cond_0

    .line 546171
    :goto_0
    return-void

    .line 546172
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->p:LX/3E2;

    invoke-virtual {v0}, LX/3E2;->cancel()V

    .line 546173
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546174
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 546175
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->q:Z

    if-nez v0, :cond_2

    .line 546176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->q:Z

    .line 546177
    invoke-direct {p0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i()V

    .line 546178
    :cond_2
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->p:LX/3E2;

    invoke-virtual {v0}, LX/3E2;->start()Landroid/os/CountDownTimer;

    goto :goto_0
.end method

.method public final a(FF)V
    .locals 6

    .prologue
    .line 546179
    iget-boolean v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->r:Z

    if-eqz v0, :cond_0

    .line 546180
    iput p1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->s:F

    .line 546181
    iput p2, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->t:F

    .line 546182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->r:Z

    .line 546183
    :goto_0
    return-void

    .line 546184
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546185
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 546186
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f:Landroid/animation/ValueAnimator;

    .line 546187
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546188
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 546189
    iget v1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->s:F

    .line 546190
    iget v4, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->t:F

    .line 546191
    iput p1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->s:F

    .line 546192
    iput p2, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->t:F

    .line 546193
    new-instance v0, LX/7E6;

    invoke-direct {p0, v1}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(F)F

    move-result v2

    iget v1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->s:F

    invoke-direct {p0, v1}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(F)F

    move-result v3

    iget v5, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->t:F

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/7E6;-><init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;FFFF)V

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->l:LX/7E6;

    .line 546194
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->l:LX/7E6;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 546195
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 546196
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->h:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 546197
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546198
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->h:Landroid/animation/ValueAnimator;

    .line 546199
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->h:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546200
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->h:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 546201
    new-instance v0, LX/7E7;

    iget v1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->u:F

    invoke-direct {p0, v1}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(F)F

    move-result v1

    int-to-float v2, p1

    invoke-direct {p0, v2}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(F)F

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/7E7;-><init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;FF)V

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->n:LX/7E7;

    .line 546202
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->h:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->n:LX/7E7;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 546203
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 546204
    int-to-float v0, p1

    iput v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->u:F

    .line 546205
    return-void

    .line 546206
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a(LX/7DE;ZZLX/3Ia;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 546207
    iput-object p1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b:LX/7DE;

    .line 546208
    iput-object p4, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->j:LX/3Ia;

    .line 546209
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->r:Z

    .line 546210
    if-nez p2, :cond_1

    .line 546211
    :cond_0
    :goto_0
    return-void

    .line 546212
    :cond_1
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-virtual {v0}, LX/3IQ;->b()V

    .line 546213
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c:Lcom/facebook/spherical/ui/HeadingBackgroundView;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/HeadingBackgroundView;->setVisibility(I)V

    .line 546214
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/HeadingFovView;->setVisibility(I)V

    .line 546215
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    invoke-virtual {v0, v2}, Lcom/facebook/spherical/ui/HeadingPoiView;->setRotation(F)V

    .line 546216
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/HeadingPoiView;->setVisibility(I)V

    .line 546217
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c:Lcom/facebook/spherical/ui/HeadingBackgroundView;

    invoke-virtual {v0, v2}, Lcom/facebook/spherical/ui/HeadingBackgroundView;->setAlpha(F)V

    .line 546218
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-virtual {v0, v2}, Lcom/facebook/spherical/ui/HeadingFovView;->setCompassYaw(F)V

    .line 546219
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-virtual {v0, v2}, Lcom/facebook/spherical/ui/HeadingFovView;->setAlpha(F)V

    .line 546220
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-direct {p0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->getDefaultFov()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/HeadingFovView;->setFov(F)V

    .line 546221
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 546222
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546223
    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    .line 546224
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 546225
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 546226
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->o:LX/3IS;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 546227
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 546228
    if-eqz p3, :cond_0

    .line 546229
    invoke-direct {p0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->j()V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 546230
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->p:LX/3E2;

    invoke-virtual {v0}, LX/3E2;->cancel()V

    .line 546231
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 546232
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546233
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->g:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 546234
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546235
    :cond_1
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 546236
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546237
    :cond_2
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->h:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 546238
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 546239
    :cond_3
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c:Lcom/facebook/spherical/ui/HeadingBackgroundView;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/HeadingBackgroundView;->setVisibility(I)V

    .line 546240
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/HeadingFovView;->setVisibility(I)V

    .line 546241
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/HeadingPoiView;->setVisibility(I)V

    .line 546242
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 546243
    invoke-virtual {p0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0649

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 546244
    invoke-static {p0, v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(Landroid/view/View;I)V

    .line 546245
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 546246
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    .line 546247
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/3IQ;->a:Z

    .line 546248
    invoke-virtual {v0}, LX/3IQ;->invalidate()V

    .line 546249
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 546250
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-virtual {v0}, LX/3IQ;->b()V

    .line 546251
    return-void
.end method

.method public setClickable(Z)V
    .locals 2

    .prologue
    .line 546252
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 546253
    if-eqz p1, :cond_0

    .line 546254
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c:Lcom/facebook/spherical/ui/HeadingBackgroundView;

    iget-object v1, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/HeadingBackgroundView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 546255
    :goto_0
    return-void

    .line 546256
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c:Lcom/facebook/spherical/ui/HeadingBackgroundView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/HeadingBackgroundView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
