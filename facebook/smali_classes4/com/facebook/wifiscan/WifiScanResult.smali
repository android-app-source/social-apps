.class public Lcom/facebook/wifiscan/WifiScanResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/wifiscan/WifiScanResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 478152
    new-instance v0, LX/2TD;

    invoke-direct {v0}, LX/2TD;-><init>()V

    sput-object v0, Lcom/facebook/wifiscan/WifiScanResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 478153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478154
    iput-wide p1, p0, Lcom/facebook/wifiscan/WifiScanResult;->a:J

    .line 478155
    iput-object p3, p0, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    .line 478156
    iput p4, p0, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    .line 478157
    iput-object p5, p0, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    .line 478158
    iput-object p6, p0, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    .line 478159
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 478160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478161
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->a:J

    .line 478162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    .line 478163
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    .line 478164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    .line 478165
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    .line 478166
    return-void
.end method

.method public static a(Ljava/util/List;LX/0SG;LX/0So;)LX/0Px;
    .locals 10
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;",
            "LX/0SG;",
            "LX/0So;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/wifiscan/WifiScanResult;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 478167
    if-nez p0, :cond_1

    .line 478168
    :cond_0
    :goto_0
    return-object v0

    .line 478169
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 478170
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 478171
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 478172
    new-instance v1, Lcom/facebook/wifiscan/WifiScanResult;

    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {p2}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, v0, Landroid/net/wifi/ScanResult;->timestamp:J

    invoke-static {v6, v7}, LX/1lQ;->r(J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    iget-object v4, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    iget v5, v0, Landroid/net/wifi/ScanResult;->level:I

    iget-object v6, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget v0, v0, Landroid/net/wifi/ScanResult;->frequency:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/facebook/wifiscan/WifiScanResult;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 478173
    :cond_2
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 478174
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 478175
    if-ne p0, p1, :cond_1

    .line 478176
    :cond_0
    :goto_0
    return v0

    .line 478177
    :cond_1
    instance-of v2, p1, Lcom/facebook/wifiscan/WifiScanResult;

    if-nez v2, :cond_2

    move v0, v1

    .line 478178
    goto :goto_0

    .line 478179
    :cond_2
    check-cast p1, Lcom/facebook/wifiscan/WifiScanResult;

    .line 478180
    iget-wide v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->a:J

    iget-wide v4, p1, Lcom/facebook/wifiscan/WifiScanResult;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    iget v3, p1, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    if-nez v2, :cond_6

    iget-object v2, p1, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 478181
    iget-wide v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->a:J

    iget-wide v4, p0, Lcom/facebook/wifiscan/WifiScanResult;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 478182
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 478183
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    add-int/2addr v0, v2

    .line 478184
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 478185
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 478186
    return v0

    .line 478187
    :cond_0
    iget-object v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 478188
    :cond_1
    iget-object v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 478189
    :cond_2
    iget-object v1, p0, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_2
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 478190
    iget-wide v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 478191
    iget-object v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478192
    iget v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 478193
    iget-object v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478194
    iget-object v0, p0, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 478195
    return-void
.end method
