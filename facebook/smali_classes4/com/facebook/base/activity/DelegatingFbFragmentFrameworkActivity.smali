.class public Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private final p:LX/42g;


# direct methods
.method public constructor <init>(LX/42j;)V
    .locals 2

    .prologue
    .line 375360
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 375361
    new-instance v0, LX/42h;

    invoke-direct {v0, p0}, LX/42h;-><init>(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 375362
    iput-object p0, p1, LX/42j;->a:Landroid/app/Activity;

    .line 375363
    iput-object v0, p1, LX/42j;->b:LX/42g;

    .line 375364
    new-instance v1, LX/42i;

    invoke-direct {v1, p1}, LX/42i;-><init>(LX/42j;)V

    move-object v0, v1

    .line 375365
    iput-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    .line 375366
    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 375359
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 375358
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->getProperty(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375357
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)V
    .locals 0

    .prologue
    .line 375356
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onTrimMemory(I)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 375294
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;ILandroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 375355
    invoke-super {p0, p1, p2}, Lcom/facebook/base/activity/FbFragmentActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;LX/0T2;)V
    .locals 0

    .prologue
    .line 375354
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;LX/0je;)V
    .locals 0

    .prologue
    .line 375353
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0je;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 375352
    invoke-super {p0, p1}, Landroid/app/Activity;->finishFromChild(Landroid/app/Activity;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 375351
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 375350
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 375349
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 375348
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 375332
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 375346
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    .prologue
    .line 375345
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 375344
    invoke-super {p0, p1, p2}, Lcom/facebook/base/activity/FbFragmentActivity;->setProperty(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Z)V
    .locals 0

    .prologue
    .line 375343
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onWindowFocusChanged(Z)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 375342
    invoke-super {p0, p1, p2}, Lcom/facebook/base/activity/FbFragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 375341
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 375340
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 375339
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 375338
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 375337
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 375336
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375335
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    return-void
.end method

.method public static synthetic b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;LX/0T2;)V
    .locals 0

    .prologue
    .line 375334
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(LX/0T2;)V

    return-void
.end method

.method public static synthetic b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 375333
    invoke-super {p0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 375369
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public static synthetic b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 375384
    invoke-super {p0, p1, p2}, Lcom/facebook/base/activity/FbFragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 375385
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 375383
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public static synthetic c(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 375387
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic c(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375388
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onLowMemory()V

    return-void
.end method

.method public static synthetic c(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 375389
    invoke-super {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic c(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 375390
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public static synthetic d(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 375391
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic d(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375392
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    return-void
.end method

.method public static synthetic d(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 375393
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public static synthetic e(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375394
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    return-void
.end method

.method public static synthetic e(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)V
    .locals 0

    .prologue
    .line 375395
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->setContentView(I)V

    return-void
.end method

.method public static synthetic f(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375396
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPostResume()V

    return-void
.end method

.method public static synthetic f(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)V
    .locals 0

    .prologue
    .line 375386
    invoke-super {p0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    return-void
.end method

.method public static synthetic g(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375382
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->lj_()V

    return-void
.end method

.method public static synthetic h(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375381
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    return-void
.end method

.method public static synthetic i(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375380
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onUserInteraction()V

    return-void
.end method

.method public static synthetic j(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Z
    .locals 1

    .prologue
    .line 375379
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method public static synthetic k(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375368
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    return-void
.end method

.method public static synthetic l(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375378
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onContentChanged()V

    return-void
.end method

.method public static synthetic m(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375377
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public static synthetic n(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)LX/0QA;
    .locals 1

    .prologue
    .line 375376
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic o(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 375375
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->mp_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic p(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375374
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iA_()V

    return-void
.end method

.method public static synthetic q(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 375373
    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    return-void
.end method

.method public static synthetic r(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)LX/0gc;
    .locals 1

    .prologue
    .line 375372
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic s(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Landroid/view/Window;
    .locals 1

    .prologue
    .line 375371
    invoke-super {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic t(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 375370
    invoke-super {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic u(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 375347
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic v(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 375367
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic w(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Z
    .locals 1

    .prologue
    .line 375259
    invoke-super {p0}, Landroid/app/Activity;->hasWindowFocus()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 375281
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0je;)V
    .locals 1

    .prologue
    .line 375279
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(LX/0je;)V

    .line 375280
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 375277
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/content/Intent;)V

    .line 375278
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 375275
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/os/Bundle;)V

    .line 375276
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 375273
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/support/v4/app/Fragment;)V

    .line 375274
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 375271
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1, p2, p3}, LX/42g;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 375272
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 375270
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 375269
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 375267
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->b(Landroid/os/Bundle;)V

    .line 375268
    return-void
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 375266
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 375265
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 375263
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->k()V

    .line 375264
    return-void
.end method

.method public final finishFromChild(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 375261
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/app/Activity;)V

    .line 375262
    return-void
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 375260
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->t()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 375239
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->v()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public final getProperty(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 375242
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 375243
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->u()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final getWindow()Landroid/view/Window;
    .locals 1

    .prologue
    .line 375244
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->s()Landroid/view/Window;

    move-result-object v0

    return-object v0
.end method

.method public final hasWindowFocus()Z
    .locals 1

    .prologue
    .line 375245
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->w()Z

    move-result v0

    return v0
.end method

.method public final iC_()LX/0gc;
    .locals 1

    .prologue
    .line 375246
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->r()LX/0gc;

    move-result-object v0

    return-object v0
.end method

.method public final iz_()LX/0QA;
    .locals 1

    .prologue
    .line 375247
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->n()LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public final lj_()V
    .locals 1

    .prologue
    .line 375248
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->g()V

    .line 375249
    return-void
.end method

.method public final mp_()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0T2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375250
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->o()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 375251
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1, p2, p3}, LX/42g;->a(IILandroid/content/Intent;)V

    .line 375252
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 375253
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->q()V

    .line 375254
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 375255
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->m()V

    .line 375256
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 375257
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/content/res/Configuration;)V

    .line 375258
    return-void
.end method

.method public final onContentChanged()V
    .locals 1

    .prologue
    .line 375240
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->l()V

    .line 375241
    return-void
.end method

.method public final onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 375310
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->b(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 375330
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1, p2, p3}, LX/42g;->a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 375331
    return-void
.end method

.method public final onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 375329
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->b(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 375328
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onCreatePanelView(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 375327
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->d(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x1bd91fc6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 375325
    iget-object v1, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v1}, LX/42g;->h()V

    .line 375326
    const/16 v1, 0x23

    const v2, -0x13f74c2c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 375324
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1, p2}, LX/42g;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 375323
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1, p2}, LX/42g;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 375321
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->c()V

    .line 375322
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 375320
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x26f80e8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 375318
    iget-object v1, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v1}, LX/42g;->d()V

    .line 375319
    const/16 v1, 0x23

    const v2, -0x2e7a01f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 375316
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->c(Landroid/os/Bundle;)V

    .line 375317
    return-void
.end method

.method public final onPostResume()V
    .locals 1

    .prologue
    .line 375314
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->f()V

    .line 375315
    return-void
.end method

.method public final onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 375312
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1, p2}, LX/42g;->a(ILandroid/app/Dialog;)V

    .line 375313
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 375311
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->b(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x10aecd8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 375282
    iget-object v1, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v1}, LX/42g;->e()V

    .line 375283
    const/16 v1, 0x23

    const v2, 0x20200a34

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 375308
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->d(Landroid/os/Bundle;)V

    .line 375309
    return-void
.end method

.method public final onSearchRequested()Z
    .locals 1

    .prologue
    .line 375307
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->j()Z

    move-result v0

    return v0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x1fb3ca8b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 375305
    iget-object v1, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v1}, LX/42g;->a()V

    .line 375306
    const/16 v1, 0x23

    const v2, -0x43cfb81b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x21b8c20c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 375303
    iget-object v1, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v1}, LX/42g;->b()V

    .line 375304
    const/16 v1, 0x23

    const v2, -0x2896c0a8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTrimMemory(I)V
    .locals 1

    .prologue
    .line 375301
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(I)V

    .line 375302
    return-void
.end method

.method public final onUserInteraction()V
    .locals 1

    .prologue
    .line 375299
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0}, LX/42g;->i()V

    .line 375300
    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 375297
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->a(Z)V

    .line 375298
    return-void
.end method

.method public final setContentView(I)V
    .locals 1

    .prologue
    .line 375295
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->e(I)V

    .line 375296
    return-void
.end method

.method public final setIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 375292
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->b(Landroid/content/Intent;)V

    .line 375293
    return-void
.end method

.method public final setProperty(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 375290
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1, p2}, LX/42g;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 375291
    return-void
.end method

.method public final setRequestedOrientation(I)V
    .locals 1

    .prologue
    .line 375288
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->f(I)V

    .line 375289
    return-void
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 375286
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1}, LX/42g;->c(Landroid/content/Intent;)V

    .line 375287
    return-void
.end method

.method public final startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 375284
    iget-object v0, p0, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p:LX/42g;

    invoke-interface {v0, p1, p2}, LX/42g;->a(Landroid/content/Intent;I)V

    .line 375285
    return-void
.end method
