.class public Lcom/facebook/messaging/cache/ReadThreadManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final j:Ljava/lang/Object;


# instance fields
.field public final b:LX/2Ow;

.field private final c:LX/0aG;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private e:LX/0Xl;

.field private f:LX/0Yb;

.field public g:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/service/model/MarkThreadFields;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Sg;

.field public final i:LX/2Og;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 564639
    const-class v0, Lcom/facebook/messaging/cache/ReadThreadManager;

    sput-object v0, Lcom/facebook/messaging/cache/ReadThreadManager;->a:Ljava/lang/Class;

    .line 564640
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/cache/ReadThreadManager;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Ow;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0Xl;LX/0Sg;LX/2Og;)V
    .locals 3
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 564628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564629
    iput-object p1, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->b:LX/2Ow;

    .line 564630
    iput-object p2, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->c:LX/0aG;

    .line 564631
    iput-object p3, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->d:Ljava/util/concurrent/ExecutorService;

    .line 564632
    iput-object p4, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->e:LX/0Xl;

    .line 564633
    iput-object p5, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->h:LX/0Sg;

    .line 564634
    iput-object p6, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->i:LX/2Og;

    .line 564635
    iget-object v0, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->e:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v2, LX/3QT;

    invoke-direct {v2, p0}, LX/3QT;-><init>(Lcom/facebook/messaging/cache/ReadThreadManager;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->f:LX/0Yb;

    .line 564636
    iget-object v0, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->f:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 564637
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->g:LX/01J;

    .line 564638
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/cache/ReadThreadManager;
    .locals 14

    .prologue
    .line 564522
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 564523
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 564524
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 564525
    if-nez v1, :cond_0

    .line 564526
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 564527
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 564528
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 564529
    sget-object v1, Lcom/facebook/messaging/cache/ReadThreadManager;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 564530
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 564531
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 564532
    :cond_1
    if-nez v1, :cond_4

    .line 564533
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 564534
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 564535
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 564536
    new-instance v7, Lcom/facebook/messaging/cache/ReadThreadManager;

    invoke-static {v0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v8

    check-cast v8, LX/2Ow;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v9

    check-cast v9, LX/0aG;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v11

    check-cast v11, LX/0Xl;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v12

    check-cast v12, LX/0Sg;

    invoke-static {v0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v13

    check-cast v13, LX/2Og;

    invoke-direct/range {v7 .. v13}, Lcom/facebook/messaging/cache/ReadThreadManager;-><init>(LX/2Ow;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0Xl;LX/0Sg;LX/2Og;)V

    .line 564537
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 564538
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 564539
    if-nez v1, :cond_2

    .line 564540
    sget-object v0, Lcom/facebook/messaging/cache/ReadThreadManager;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/cache/ReadThreadManager;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 564541
    :goto_1
    if-eqz v0, :cond_3

    .line 564542
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 564543
    :goto_3
    check-cast v0, Lcom/facebook/messaging/cache/ReadThreadManager;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 564544
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 564545
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 564546
    :catchall_1
    move-exception v0

    .line 564547
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 564548
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 564549
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 564550
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/cache/ReadThreadManager;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/cache/ReadThreadManager;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a$redex0(Lcom/facebook/messaging/cache/ReadThreadManager;)V
    .locals 10

    .prologue
    .line 564614
    new-instance v0, LX/6ie;

    invoke-direct {v0}, LX/6ie;-><init>()V

    sget-object v1, LX/6iW;->READ:LX/6iW;

    .line 564615
    iput-object v1, v0, LX/6ie;->a:LX/6iW;

    .line 564616
    move-object v2, v0

    .line 564617
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->g:LX/01J;

    invoke-virtual {v1}, LX/01J;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 564618
    iget-object v0, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->g:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 564619
    iget-object v4, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->i:LX/2Og;

    iget-object v5, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 564620
    invoke-static {v4, v5}, LX/2Og;->d(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/2OQ;

    move-result-object v8

    invoke-virtual {v8, v5}, LX/2OQ;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)J

    move-result-wide v8

    move-wide v4, v8

    .line 564621
    iget-wide v6, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    cmp-long v4, v6, v4

    if-ltz v4, :cond_0

    .line 564622
    invoke-virtual {v2, v0}, LX/6ie;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)LX/6ie;

    .line 564623
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 564624
    :cond_1
    invoke-virtual {v2}, LX/6ie;->a()Lcom/facebook/messaging/service/model/MarkThreadsParams;

    move-result-object v0

    .line 564625
    iget-object v1, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/messaging/cache/ReadThreadManager$4;

    invoke-direct {v2, p0, v0}, Lcom/facebook/messaging/cache/ReadThreadManager$4;-><init>(Lcom/facebook/messaging/cache/ReadThreadManager;Lcom/facebook/messaging/service/model/MarkThreadsParams;)V

    const v0, -0x4c92c017

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 564626
    iget-object v0, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->g:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 564627
    return-void
.end method

.method public static b(Lcom/facebook/messaging/cache/ReadThreadManager;Lcom/facebook/messaging/service/model/MarkThreadsParams;)V
    .locals 6

    .prologue
    .line 564609
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 564610
    const-string v0, "markThreadsParams"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 564611
    iget-object v0, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->c:LX/0aG;

    const-string v1, "mark_threads"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x5ebeddf2

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 564612
    new-instance v1, LX/DdK;

    invoke-direct {v1, p0, p1}, LX/DdK;-><init>(Lcom/facebook/messaging/cache/ReadThreadManager;Lcom/facebook/messaging/service/model/MarkThreadsParams;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 564613
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 10

    .prologue
    .line 564574
    iget-object v0, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->i:LX/2Og;

    invoke-virtual {v0, p1}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 564575
    if-eqz v0, :cond_2

    .line 564576
    const/4 v1, 0x1

    .line 564577
    iget-wide v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 564578
    iget-wide v4, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    .line 564579
    iget-object v6, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v6}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-nez v1, :cond_0

    .line 564580
    const-wide/16 v4, 0x0

    .line 564581
    :cond_0
    new-instance v6, LX/6ia;

    invoke-direct {v6}, LX/6ia;-><init>()V

    iget-object v7, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 564582
    iput-object v7, v6, LX/6ia;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 564583
    move-object v6, v6

    .line 564584
    iput-boolean v1, v6, LX/6ia;->b:Z

    .line 564585
    move-object v6, v6

    .line 564586
    iput-wide v4, v6, LX/6ia;->c:J

    .line 564587
    move-object v4, v6

    .line 564588
    iget-wide v6, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    .line 564589
    iput-wide v6, v4, LX/6ia;->d:J

    .line 564590
    move-object v4, v4

    .line 564591
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    .line 564592
    iput-object v5, v4, LX/6ia;->f:LX/6ek;

    .line 564593
    move-object v4, v4

    .line 564594
    iput-wide v2, v4, LX/6ia;->e:J

    .line 564595
    move-object v4, v4

    .line 564596
    invoke-virtual {v4}, LX/6ia;->a()Lcom/facebook/messaging/service/model/MarkThreadFields;

    move-result-object v4

    .line 564597
    new-instance v5, LX/6ie;

    invoke-direct {v5}, LX/6ie;-><init>()V

    sget-object v6, LX/6iW;->READ:LX/6iW;

    .line 564598
    iput-object v6, v5, LX/6ie;->a:LX/6iW;

    .line 564599
    move-object v5, v5

    .line 564600
    invoke-virtual {v5, v4}, LX/6ie;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)LX/6ie;

    move-result-object v5

    invoke-virtual {v5}, LX/6ie;->a()Lcom/facebook/messaging/service/model/MarkThreadsParams;

    move-result-object v5

    .line 564601
    iget-object v6, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->i:LX/2Og;

    invoke-virtual {v6, v4}, LX/2Og;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)V

    .line 564602
    iget-object v4, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->i:LX/2Og;

    .line 564603
    iget-object v8, v5, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    if-ge v7, v9, :cond_1

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 564604
    invoke-virtual {v4, v6}, LX/2Og;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)V

    .line 564605
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    .line 564606
    :cond_1
    iget-object v4, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->h:LX/0Sg;

    const-string v6, "markThreadsRead"

    new-instance v7, Lcom/facebook/messaging/cache/ReadThreadManager$2;

    invoke-direct {v7, p0, v5}, Lcom/facebook/messaging/cache/ReadThreadManager$2;-><init>(Lcom/facebook/messaging/cache/ReadThreadManager;Lcom/facebook/messaging/service/model/MarkThreadsParams;)V

    sget-object v8, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    sget-object v9, LX/0Vm;->UI:LX/0Vm;

    invoke-virtual {v4, v6, v7, v8, v9}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 564607
    iget-object v4, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->b:LX/2Ow;

    iget-object v6, v5, Lcom/facebook/messaging/service/model/MarkThreadsParams;->d:LX/0Px;

    invoke-virtual {v4, v6}, LX/2Ow;->b(LX/0Px;)V

    .line 564608
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJ)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 564551
    new-instance v0, LX/6ia;

    invoke-direct {v0}, LX/6ia;-><init>()V

    .line 564552
    iput-object p1, v0, LX/6ia;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 564553
    move-object v0, v0

    .line 564554
    iput-boolean v6, v0, LX/6ia;->b:Z

    .line 564555
    move-object v0, v0

    .line 564556
    iput-wide p2, v0, LX/6ia;->c:J

    .line 564557
    move-object v0, v0

    .line 564558
    iput-wide p4, v0, LX/6ia;->e:J

    .line 564559
    move-object v0, v0

    .line 564560
    invoke-virtual {v0}, LX/6ia;->a()Lcom/facebook/messaging/service/model/MarkThreadFields;

    move-result-object v0

    .line 564561
    iget-object v1, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->i:LX/2Og;

    invoke-virtual {v1, v0}, LX/2Og;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)V

    .line 564562
    new-instance v1, LX/6ie;

    invoke-direct {v1}, LX/6ie;-><init>()V

    sget-object v2, LX/6iW;->READ:LX/6iW;

    .line 564563
    iput-object v2, v1, LX/6ie;->a:LX/6iW;

    .line 564564
    move-object v1, v1

    .line 564565
    invoke-virtual {v1, v0}, LX/6ie;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)LX/6ie;

    move-result-object v0

    const/4 v1, 0x0

    .line 564566
    iput-boolean v1, v0, LX/6ie;->b:Z

    .line 564567
    move-object v0, v0

    .line 564568
    invoke-virtual {v0}, LX/6ie;->a()Lcom/facebook/messaging/service/model/MarkThreadsParams;

    move-result-object v0

    .line 564569
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 564570
    const-string v1, "markThreadsParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 564571
    iget-object v0, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->c:LX/0aG;

    const-string v1, "mark_threads"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x6de63b0a

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0, v6}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 564572
    iget-object v0, p0, Lcom/facebook/messaging/cache/ReadThreadManager;->b:LX/2Ow;

    invoke-virtual {v0, p1}, LX/2Ow;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564573
    return-void
.end method
