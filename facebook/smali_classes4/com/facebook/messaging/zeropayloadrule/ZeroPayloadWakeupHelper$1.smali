.class public final Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeupHelper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2BG;


# direct methods
.method public constructor <init>(LX/2BG;)V
    .locals 0

    .prologue
    .line 565201
    iput-object p1, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeupHelper$1;->a:LX/2BG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 565202
    iget-object v0, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeupHelper$1;->a:LX/2BG;

    iget-object v1, v0, LX/2BG;->e:LX/1qk;

    monitor-enter v1

    .line 565203
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeupHelper$1;->a:LX/2BG;

    iget-object v0, v0, LX/2BG;->e:LX/1qk;

    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 565204
    iget-object v0, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeupHelper$1;->a:LX/2BG;

    const/4 v2, 0x0

    .line 565205
    iput-object v2, v0, LX/2BG;->g:Ljava/util/concurrent/Future;

    .line 565206
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565207
    iget-object v0, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeupHelper$1;->a:LX/2BG;

    iget-object v0, v0, LX/2BG;->c:LX/290;

    .line 565208
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "zp_undeliver"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 565209
    invoke-static {v0, v1}, LX/290;->a(LX/290;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 565210
    return-void

    .line 565211
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
