.class public final Lcom/facebook/messaging/sms/SmsThreadManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sms/SmsThreadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/SmsThreadManager;)V
    .locals 0

    .prologue
    .line 556285
    iput-object p1, p0, Lcom/facebook/messaging/sms/SmsThreadManager$1;->a:Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 556286
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager$1;->a:Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v0, v0, Lcom/facebook/messaging/sms/SmsThreadManager;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/messaging/sms/SmsThreadManager$1;->a:Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v1, v1, Lcom/facebook/messaging/sms/SmsThreadManager;->j:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 556287
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager$1;->a:Lcom/facebook/messaging/sms/SmsThreadManager;

    .line 556288
    iget-object v1, v0, Lcom/facebook/messaging/sms/SmsThreadManager;->r:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FMc;

    invoke-virtual {v1}, LX/FMc;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 556289
    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    const-class v3, LX/FMm;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 556290
    const-string v2, "com.facebook.messaging.sms.MARK_PENDING_MMS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 556291
    iget-object v2, v0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 556292
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager$1;->a:Lcom/facebook/messaging/sms/SmsThreadManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Z)V

    .line 556293
    return-void
.end method
