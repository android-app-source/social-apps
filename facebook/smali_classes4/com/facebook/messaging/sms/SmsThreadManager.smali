.class public Lcom/facebook/messaging/sms/SmsThreadManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile I:Lcom/facebook/messaging/sms/SmsThreadManager;

.field private static final a:Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:Landroid/content/ContentValues;

.field private static final h:Landroid/content/ContentValues;


# instance fields
.field private A:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FM9;",
            ">;"
        }
    .end annotation
.end field

.field private B:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMI;",
            ">;"
        }
    .end annotation
.end field

.field private C:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNX;",
            ">;"
        }
    .end annotation
.end field

.field private D:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FML;",
            ">;"
        }
    .end annotation
.end field

.field private E:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2uq;",
            ">;"
        }
    .end annotation
.end field

.field private F:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Uq;",
            ">;"
        }
    .end annotation
.end field

.field private G:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Rc;",
            ">;"
        }
    .end annotation
.end field

.field private H:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3MH;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/lang/Runnable;

.field public k:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/2Oq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNh;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3MF;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMc;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Mo;",
            ">;"
        }
    .end annotation
.end field

.field private t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6cV;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FME;",
            ">;"
        }
    .end annotation
.end field

.field private v:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMl;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMo;",
            ">;"
        }
    .end annotation
.end field

.field private y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;"
        }
    .end annotation
.end field

.field private z:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 556086
    sget-object v0, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "simple"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->a:Landroid/net/Uri;

    .line 556087
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v3

    const-string v1, "recipient_ids"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "snippet"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "snippet_cs"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "read"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "error"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "message_count"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->b:[Ljava/lang/String;

    .line 556088
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "recipient_ids"

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->c:[Ljava/lang/String;

    .line 556089
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "read"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->d:[Ljava/lang/String;

    .line 556090
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "message_count"

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->e:[Ljava/lang/String;

    .line 556091
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "thread_id"

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->f:[Ljava/lang/String;

    .line 556092
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, v5}, Landroid/content/ContentValues;-><init>(I)V

    sput-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->g:Landroid/content/ContentValues;

    .line 556093
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    sput-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->h:Landroid/content/ContentValues;

    .line 556094
    sget-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->g:Landroid/content/ContentValues;

    const-string v1, "read"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 556095
    sget-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->g:Landroid/content/ContentValues;

    const-string v1, "seen"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 556096
    sget-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->h:Landroid/content/ContentValues;

    const-string v1, "read"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 556097
    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 556113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 556114
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->i:Ljava/util/Set;

    .line 556115
    new-instance v0, Lcom/facebook/messaging/sms/SmsThreadManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/sms/SmsThreadManager$1;-><init>(Lcom/facebook/messaging/sms/SmsThreadManager;)V

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->j:Ljava/lang/Runnable;

    .line 556116
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556117
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->m:LX/0Ot;

    .line 556118
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556119
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->n:LX/0Ot;

    .line 556120
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556121
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->o:LX/0Ot;

    .line 556122
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556123
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->p:LX/0Ot;

    .line 556124
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556125
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->q:LX/0Ot;

    .line 556126
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556127
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->r:LX/0Ot;

    .line 556128
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556129
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->s:LX/0Ot;

    .line 556130
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556131
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->t:LX/0Ot;

    .line 556132
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556133
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->u:LX/0Ot;

    .line 556134
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556135
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->v:LX/0Ot;

    .line 556136
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556137
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->w:LX/0Ot;

    .line 556138
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556139
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->x:LX/0Ot;

    .line 556140
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556141
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->y:LX/0Ot;

    .line 556142
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556143
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->z:LX/0Ot;

    .line 556144
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556145
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->A:LX/0Ot;

    .line 556146
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556147
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->B:LX/0Ot;

    .line 556148
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556149
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->C:LX/0Ot;

    .line 556150
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556151
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->D:LX/0Ot;

    .line 556152
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556153
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->E:LX/0Ot;

    .line 556154
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556155
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->F:LX/0Ot;

    .line 556156
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556157
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->G:LX/0Ot;

    .line 556158
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 556159
    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->H:LX/0Ot;

    .line 556160
    return-void
.end method

.method private a(JLjava/util/Map;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 3
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 556161
    const-string v0, "SmsThreadManager.getThreadSummaryHack"

    const v1, -0x24b75302

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 556162
    const/4 v0, 0x0

    .line 556163
    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/sms/SmsThreadManager;->e(Lcom/facebook/messaging/sms/SmsThreadManager;J)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 556164
    if-eqz v1, :cond_1

    .line 556165
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 556166
    invoke-static {p0, v1, p3}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 556167
    :cond_0
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 556168
    :cond_1
    const v1, 0x79d2bd76

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 556169
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 556170
    :catchall_1
    move-exception v0

    const v1, 0x3f22cca4

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(Lcom/facebook/messaging/sms/SmsThreadManager;JJLjava/lang/String;IZILjava/util/List;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Float;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 15
    .param p3    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "IZI",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Float;",
            ")",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;"
        }
    .end annotation

    .prologue
    .line 556171
    move-object/from16 v0, p5

    move/from16 v1, p6

    move/from16 v2, p12

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v8

    .line 556172
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 556173
    const/4 v7, 0x1

    .line 556174
    const-string v4, "SmsThreadManager.loadThreadParticipants"

    const v5, 0x26b239c1

    invoke-static {v4, v5}, LX/02m;->a(Ljava/lang/String;I)V

    .line 556175
    :try_start_0
    invoke-interface/range {p9 .. p9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 556176
    invoke-static {v4}, LX/FNh;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 556177
    const/4 v5, 0x0

    move v6, v5

    .line 556178
    :goto_1
    if-eqz p10, :cond_1

    move-object/from16 v0, p10

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 556179
    move-object/from16 v0, p10

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/user/model/User;

    .line 556180
    :goto_2
    if-eqz p10, :cond_0

    .line 556181
    move-object/from16 v0, p10

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556182
    :cond_0
    new-instance v4, LX/6fz;

    invoke-direct {v4}, LX/6fz;-><init>()V

    invoke-static {v5}, LX/FNh;->a(Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/6fz;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6fz;

    move-result-object v4

    invoke-virtual {v4}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v4

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v7, v6

    .line 556183
    goto :goto_0

    .line 556184
    :cond_1
    iget-object v5, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->m:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FNh;

    invoke-virtual {v5, v4}, LX/FNh;->a(Ljava/lang/String;)Lcom/facebook/user/model/User;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    goto :goto_2

    .line 556185
    :cond_2
    const v4, 0xcada277

    invoke-static {v4}, LX/02m;->a(I)V

    .line 556186
    new-instance v5, LX/6fz;

    invoke-direct {v5}, LX/6fz;-><init>()V

    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FNh;

    invoke-virtual {v4}, LX/FNh;->a()Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/6fz;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6fz;

    move-result-object v4

    invoke-virtual {v4}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v4

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 556187
    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->t:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6cV;

    move-wide/from16 v0, p1

    move-wide/from16 v2, p3

    invoke-virtual {v4, v0, v1, v2, v3}, LX/6cV;->a(JJ)J

    move-result-wide v10

    .line 556188
    const/4 v6, 0x0

    .line 556189
    const/4 v5, 0x0

    .line 556190
    invoke-interface/range {p9 .. p9}, Ljava/util/List;->size()I

    move-result v4

    const/4 v9, 0x1

    if-ne v4, v9, :cond_e

    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->F:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2Uq;

    invoke-virtual {v4}, LX/2Uq;->m()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 556191
    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->C:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FNX;

    const/4 v5, 0x0

    move-object/from16 v0, p9

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/FNX;->a(Ljava/lang/String;)Z

    move-result v6

    .line 556192
    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->D:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FML;

    const/4 v5, 0x0

    move-object/from16 v0, p9

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/FML;->a(Ljava/lang/String;)Z

    move-result v4

    move v5, v6

    .line 556193
    :goto_3
    sget-object v6, LX/6ek;->INBOX:LX/6ek;

    .line 556194
    if-eqz v5, :cond_a

    .line 556195
    sget-object v4, LX/6ek;->SMS_SPAM:LX/6ek;

    move-object v5, v4

    .line 556196
    :goto_4
    const/4 v6, 0x0

    .line 556197
    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->F:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2Uq;

    invoke-virtual {v4}, LX/2Uq;->k()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 556198
    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->s:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3Mo;

    move-wide/from16 v0, p1

    move-object/from16 v2, p9

    invoke-virtual {v4, v0, v1, v2}, LX/3Mo;->b(JLjava/util/List;)LX/6fr;

    move-result-object v4

    move-object v6, v4

    .line 556199
    :cond_3
    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->G:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3Rc;

    invoke-virtual {v4}, LX/3Rc;->a()I

    move-result v4

    .line 556200
    if-nez v6, :cond_b

    .line 556201
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadCustomization;->newBuilder()LX/6fr;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/6fr;->b(I)LX/6fr;

    move-result-object v6

    .line 556202
    :cond_4
    :goto_5
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/6g6;->a(LX/6ek;)LX/6g6;

    move-result-object v4

    invoke-static/range {p1 .. p2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/6g6;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6g6;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/6g6;->a(Z)LX/6g6;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, LX/6g6;->e(J)LX/6g6;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, LX/6g6;->j(J)LX/6g6;

    move-result-object v4

    invoke-virtual {v4, v12}, LX/6g6;->a(Ljava/util/List;)LX/6g6;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/6g6;->c(Ljava/lang/String;)LX/6g6;

    move-result-object v5

    if-lez p8, :cond_c

    const/4 v4, 0x1

    :goto_6
    invoke-virtual {v5, v4}, LX/6g6;->c(Z)LX/6g6;

    move-result-object v4

    move/from16 v0, p12

    int-to-long v8, v0

    invoke-virtual {v4, v8, v9}, LX/6g6;->g(J)LX/6g6;

    move-result-object v4

    invoke-virtual {v6}, LX/6fr;->g()Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadCustomization;)LX/6g6;

    move-result-object v13

    .line 556203
    if-nez p7, :cond_5

    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->l:LX/2Oq;

    invoke-virtual {v4}, LX/2Oq;->b()Z

    move-result v4

    if-nez v4, :cond_5

    .line 556204
    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->s:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/3Mo;

    const/4 v10, 0x0

    move-wide/from16 v6, p1

    move-wide/from16 v8, p3

    move-object/from16 v11, p9

    invoke-virtual/range {v5 .. v11}, LX/3Mo;->a(JJILjava/util/List;)Z

    move-result p7

    .line 556205
    :cond_5
    if-eqz p7, :cond_6

    .line 556206
    move-wide/from16 v0, p3

    invoke-virtual {v13, v0, v1}, LX/6g6;->f(J)LX/6g6;

    .line 556207
    :cond_6
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x2

    if-le v4, v5, :cond_7

    invoke-static/range {p11 .. p11}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 556208
    iget-object v4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->s:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3Mo;

    move-wide/from16 v0, p1

    move-object/from16 v2, p9

    invoke-virtual {v4, v0, v1, v2}, LX/3Mo;->a(JLjava/util/List;)Ljava/lang/String;

    move-result-object p11

    .line 556209
    :cond_7
    invoke-static/range {p11 .. p11}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 556210
    move-object/from16 v0, p11

    invoke-virtual {v13, v0}, LX/6g6;->b(Ljava/lang/String;)LX/6g6;

    .line 556211
    :cond_8
    if-eqz p13, :cond_9

    .line 556212
    invoke-virtual/range {p13 .. p13}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v13, v4}, LX/6g6;->a(F)LX/6g6;

    .line 556213
    :cond_9
    invoke-virtual {v13}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v4

    return-object v4

    .line 556214
    :catchall_0
    move-exception v4

    const v5, -0x7437bfc9

    invoke-static {v5}, LX/02m;->a(I)V

    throw v4

    .line 556215
    :cond_a
    if-eqz v4, :cond_d

    .line 556216
    sget-object v4, LX/6ek;->SMS_BUSINESS:LX/6ek;

    move-object v5, v4

    goto/16 :goto_4

    .line 556217
    :cond_b
    invoke-virtual {v6}, LX/6fr;->b()I

    move-result v9

    if-nez v9, :cond_4

    .line 556218
    invoke-virtual {v6, v4}, LX/6fr;->b(I)LX/6fr;

    goto/16 :goto_5

    .line 556219
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_6

    :cond_d
    move-object v5, v6

    goto/16 :goto_4

    :cond_e
    move v4, v5

    move v5, v6

    goto/16 :goto_3

    :cond_f
    move v6, v7

    goto/16 :goto_1
.end method

.method private static a(Lcom/facebook/messaging/sms/SmsThreadManager;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 17
    .param p1    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;"
        }
    .end annotation

    .prologue
    .line 556220
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 556221
    const-string v2, "date"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 556222
    const-string v2, "snippet"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 556223
    const-string v2, "snippet_cs"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v9

    .line 556224
    const-string v2, "read"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v10, 0x1

    .line 556225
    :goto_0
    const-string v2, "error"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v11

    .line 556226
    const-string v2, "message_count"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v15

    .line 556227
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(J)Ljava/util/List;

    move-result-object v12

    .line 556228
    const/4 v14, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    move-object/from16 v13, p2

    invoke-static/range {v3 .. v16}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;JJLjava/lang/String;IZILjava/util/List;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Float;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    return-object v2

    .line 556229
    :cond_0
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/sms/SmsThreadManager;
    .locals 3

    .prologue
    .line 556230
    sget-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->I:Lcom/facebook/messaging/sms/SmsThreadManager;

    if-nez v0, :cond_1

    .line 556231
    const-class v1, Lcom/facebook/messaging/sms/SmsThreadManager;

    monitor-enter v1

    .line 556232
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->I:Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 556233
    if-eqz v2, :cond_0

    .line 556234
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/messaging/sms/SmsThreadManager;->b(LX/0QB;)Lcom/facebook/messaging/sms/SmsThreadManager;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->I:Lcom/facebook/messaging/sms/SmsThreadManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 556235
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 556236
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 556237
    :cond_1
    sget-object v0, Lcom/facebook/messaging/sms/SmsThreadManager;->I:Lcom/facebook/messaging/sms/SmsThreadManager;

    return-object v0

    .line 556238
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 556239
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 556240
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 556241
    if-nez p3, :cond_1

    .line 556242
    const-string p1, ""

    .line 556243
    :cond_0
    :goto_0
    return-object p1

    .line 556244
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0808f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 556245
    :cond_2
    if-eqz p2, :cond_0

    .line 556246
    new-instance v0, LX/EdS;

    invoke-static {p1}, LX/Edh;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, p2, v1}, LX/EdS;-><init>(I[B)V

    invoke-virtual {v0}, LX/EdS;->c()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/sms/SmsThreadManager;LX/6ek;ILjava/util/Map;J)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ek;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;J)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 556247
    sget-object v0, LX/FMJ;->a:[I

    invoke-virtual {p1}, LX/6ek;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 556248
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown folderName"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/6ek;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 556249
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNX;

    invoke-virtual {v0}, LX/FNX;->a()LX/0Rf;

    move-result-object v0

    invoke-direct {p0, v0, p2, p4, p5}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Ljava/util/Collection;IJ)Ljava/util/List;

    move-result-object v0

    .line 556250
    :goto_0
    return-object v0

    .line 556251
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FML;

    .line 556252
    invoke-virtual {v0}, LX/FML;->b()LX/0Rf;

    move-result-object v1

    .line 556253
    invoke-virtual {v1}, LX/0Rf;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 556254
    sget-object v1, LX/0Re;->a:LX/0Re;

    move-object v1, v1

    .line 556255
    :goto_1
    move-object v0, v1

    .line 556256
    invoke-direct {p0, v0, p2, p4, p5}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Ljava/util/Collection;IJ)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 556257
    :pswitch_2
    invoke-virtual {p0, p2, p4, p5, p3}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(IJLjava/util/Map;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 556258
    :cond_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 556259
    iget-object v1, v0, LX/FML;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FM9;

    invoke-virtual {v1}, LX/FM9;->a()LX/0Rf;

    move-result-object v3

    .line 556260
    iget-object v1, v0, LX/FML;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FNX;

    invoke-virtual {v1}, LX/FNX;->a()LX/0Rf;

    move-result-object v1

    .line 556261
    invoke-interface {v2, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 556262
    invoke-interface {v2, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 556263
    invoke-static {v2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/util/Collection;IJ)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;IJ)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 556264
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 556265
    new-array v0, v7, [LX/0ux;

    const-string v1, "message_count"

    const-string v2, "0"

    invoke-static {v1, v2}, LX/0uu;->e(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "_id"

    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    .line 556266
    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-lez v1, :cond_2

    .line 556267
    const-string v1, "date"

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->c(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 556268
    new-array v2, v7, [LX/0ux;

    aput-object v0, v2, v4

    aput-object v1, v2, v5

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    move-object v4, v0

    .line 556269
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/sms/SmsThreadManager;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "date DESC LIMIT "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 556270
    if-eqz v1, :cond_1

    .line 556271
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 556272
    const/4 v0, 0x0

    :try_start_1
    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 556273
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 556274
    :catch_0
    move-exception v0

    .line 556275
    :try_start_2
    const-string v2, "SmsThreadManager"

    const-string v3, "Failed to load thread"

    invoke-static {v2, v3, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 556276
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 556277
    :cond_1
    return-object v6

    :cond_2
    move-object v4, v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/sms/SmsThreadManager;Landroid/content/Context;LX/2Oq;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/sms/SmsThreadManager;",
            "Landroid/content/Context;",
            "LX/2Oq;",
            "LX/0Ot",
            "<",
            "LX/FNh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3MF;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3Mo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6cV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FME;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMl;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FNO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FM9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FNX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FML;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2uq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Uq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3Rc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3MH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 556278
    iput-object p1, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    iput-object p2, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->l:LX/2Oq;

    iput-object p3, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->m:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->n:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->o:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->p:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->q:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->r:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->s:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->t:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->u:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->v:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->w:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->x:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->y:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->z:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->A:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->B:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->C:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->D:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->E:LX/0Ot;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->F:LX/0Ot;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->G:LX/0Ot;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->H:LX/0Ot;

    return-void
.end method

.method private b(JLjava/util/Map;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 15
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;"
        }
    .end annotation

    .prologue
    .line 556279
    invoke-virtual/range {p0 .. p2}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(J)Ljava/util/List;

    move-result-object v10

    .line 556280
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v1, p0

    move-wide/from16 v2, p1

    move-object/from16 v11, p3

    invoke-static/range {v1 .. v14}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;JJLjava/lang/String;IZILjava/util/List;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Float;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/sms/SmsThreadManager;
    .locals 27

    .prologue
    .line 556281
    new-instance v2, Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-direct {v2}, Lcom/facebook/messaging/sms/SmsThreadManager;-><init>()V

    .line 556282
    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v4

    check-cast v4, LX/2Oq;

    const/16 v5, 0x29b7

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xdac

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2e3

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x542

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x298a

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xda7

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x26f9

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x297e

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x298e

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x29

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x298f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0xd9d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x29b2

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x297b

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x2982

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x29b5

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x2984

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0xda8

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0xd9f

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0xdaa

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0xda2

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-static/range {v2 .. v26}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;Landroid/content/Context;LX/2Oq;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 556283
    return-object v2
.end method

.method private static e(Lcom/facebook/messaging/sms/SmsThreadManager;J)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 556284
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "subject"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->b:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private f(J)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 556098
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/sms/SmsThreadManager;->e(Lcom/facebook/messaging/sms/SmsThreadManager;J)Landroid/database/Cursor;

    move-result-object v2

    .line 556099
    if-eqz v2, :cond_2

    .line 556100
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556101
    const-string v0, "recipient_ids"

    invoke-static {v2, v0}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 556102
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MF;

    invoke-virtual {v0, v1}, LX/3MF;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 556103
    if-eqz v1, :cond_0

    .line 556104
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMI;

    invoke-virtual {v0, p1, p2, v1}, LX/FMI;->a(JLjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 556105
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-object v0, v1

    .line 556106
    :goto_0
    return-object v0

    .line 556107
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 556108
    :cond_2
    invoke-static {p1, p2}, LX/5e6;->a(J)Ljava/util/Set;

    move-result-object v1

    .line 556109
    if-eqz v1, :cond_3

    .line 556110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    .line 556111
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 556112
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method private g(J)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 555845
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 555846
    :try_start_0
    sget-object v1, LX/2Uo;->a:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 555847
    new-instance v2, Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "thread_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 555848
    const-string v3, " and ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555849
    const-string v3, "read=1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555850
    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555851
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 555852
    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->d:[Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 555853
    if-eqz v2, :cond_0

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 555854
    const-string v3, "_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 555855
    const-string v3, "_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    .line 555856
    sget-object v4, Lcom/facebook/messaging/sms/SmsThreadManager;->h:Landroid/content/ContentValues;

    invoke-virtual {v3}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v4, v5, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 555857
    :cond_0
    if-eqz v2, :cond_1

    .line 555858
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 555859
    :cond_1
    :goto_0
    return-void

    .line 555860
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 555861
    :goto_1
    :try_start_2
    const-string v2, "SmsThreadManager"

    const-string v3, "Failed to query read SMS messages in thread %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 555862
    if-eqz v1, :cond_1

    .line 555863
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 555864
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v6, :cond_2

    .line 555865
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 555866
    :catchall_1
    move-exception v0

    move-object v6, v2

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v6, v1

    goto :goto_2

    .line 555867
    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/net/Uri;J)J
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 555880
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->f:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 555881
    if-eqz v0, :cond_4

    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 555882
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v6

    .line 555883
    :goto_0
    if-eqz v0, :cond_0

    .line 555884
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 555885
    :cond_0
    :goto_1
    cmp-long v0, v6, p2

    if-eqz v0, :cond_1

    .line 555886
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 555887
    new-instance v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;

    invoke-static {p2, p3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/service/model/DeleteThreadsParams;-><init>(Ljava/util/List;)V

    .line 555888
    const-string v1, "deleteThreadsParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 555889
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "delete_threads"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x741a1ebb

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0, v8}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 555890
    sget-object v0, LX/3RH;->H:Ljava/lang/String;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 555891
    new-instance v1, Landroid/content/Intent;

    sget-object v2, LX/3GK;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 555892
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 555893
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 555894
    const-string v0, "focus_compose"

    invoke-virtual {v1, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 555895
    const-string v0, "show_composer"

    invoke-virtual {v1, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 555896
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    move-wide p2, v6

    .line 555897
    :cond_1
    return-wide p2

    .line 555898
    :catch_0
    move-object v0, v6

    :goto_2
    if-eqz v0, :cond_3

    .line 555899
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-wide v6, p2

    goto :goto_1

    .line 555900
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_2

    .line 555901
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 555902
    :catchall_1
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    goto :goto_3

    :catch_1
    goto :goto_2

    :cond_3
    move-wide v6, p2

    goto/16 :goto_1

    :cond_4
    move-wide v6, p2

    goto/16 :goto_0
.end method

.method public final a()LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 555903
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 555904
    const-string v0, "recipient_ids"

    const-string v1, "% %"

    invoke-static {v0, v1}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 555905
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/sms/SmsThreadManager;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const-string v5, "date DESC LIMIT 60"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 555906
    if-eqz v1, :cond_2

    .line 555907
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 555908
    const/4 v0, 0x0

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 555909
    if-eqz v0, :cond_0

    .line 555910
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 555911
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 555912
    :cond_2
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 17
    .param p4    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 555913
    invoke-static/range {p3 .. p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 555914
    const/4 v2, 0x0

    .line 555915
    :goto_0
    return-object v2

    .line 555916
    :cond_0
    const-string v2, " "

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 555917
    array-length v2, v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_1

    if-nez p4, :cond_2

    .line 555918
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 555919
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/sms/SmsThreadManager;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3MF;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, LX/3MF;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    .line 555920
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/sms/SmsThreadManager;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v12}, LX/3MF;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 555921
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/sms/SmsThreadManager;->y:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Or;

    invoke-virtual {v2}, LX/2Or;->o()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/sms/SmsThreadManager;->H:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3MH;

    invoke-virtual {v2, v3}, LX/3MH;->c(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v2, v2

    .line 555922
    :goto_1
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-object/from16 v14, p5

    invoke-static/range {v3 .. v16}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;JJLjava/lang/String;IZILjava/util/List;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Float;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    goto :goto_0

    .line 555923
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a(JLjava/util/Map;LX/6ek;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 7
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/6ek;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/6ek;",
            ")",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 555924
    const-string v0, "SmsThreadManager.getThreadSummary"

    const v1, 0x61498833

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 555925
    :try_start_0
    const-string v0, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 555926
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/sms/SmsThreadManager;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 555927
    if-eqz v1, :cond_3

    .line 555928
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 555929
    invoke-static {p0, v1, p3}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 555930
    :goto_0
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 555931
    :goto_1
    if-nez v0, :cond_0

    .line 555932
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(JLjava/util/Map;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 555933
    :cond_0
    if-nez v0, :cond_1

    .line 555934
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/messaging/sms/SmsThreadManager;->b(JLjava/util/Map;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 555935
    :cond_1
    const v1, -0x1dbdc7de

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 555936
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 555937
    :catchall_1
    move-exception v0

    const v1, -0x7731de1a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_2
    move-object v0, v6

    goto :goto_0

    :cond_3
    move-object v0, v6

    goto :goto_1
.end method

.method public final a(IJLjava/util/Map;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 555938
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 555939
    const/4 v2, 0x0

    .line 555940
    const/4 v1, 0x0

    .line 555941
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Uq;

    invoke-virtual {v0}, LX/2Uq;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555942
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNX;

    invoke-virtual {v0}, LX/FNX;->b()LX/0Rf;

    move-result-object v1

    .line 555943
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FML;

    invoke-virtual {v0}, LX/FML;->b()LX/0Rf;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    .line 555944
    :cond_0
    const-string v0, "SmsThreadManager.getThreadSummaryList"

    const v3, -0x33469ae1    # -9.7200376E7f

    invoke-static {v0, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 555945
    :try_start_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 555946
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FM9;

    invoke-virtual {v0}, LX/FM9;->a()LX/0Rf;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 555947
    if-eqz v2, :cond_1

    .line 555948
    invoke-interface {v3, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 555949
    :cond_1
    if-eqz v1, :cond_2

    .line 555950
    invoke-interface {v3, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 555951
    :cond_2
    const/4 v0, 0x0

    .line 555952
    const-wide/16 v4, 0x0

    cmp-long v1, p2, v4

    if-lez v1, :cond_3

    .line 555953
    const-string v0, "date"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->c(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 555954
    :cond_3
    const-string v1, "message_count"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v2, v4

    invoke-static {v1, v2}, LX/0uu;->b(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 555955
    const-string v2, "_id"

    invoke-static {v2, v3}, LX/0uu;->b(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v2

    .line 555956
    if-eqz v0, :cond_6

    .line 555957
    const/4 v3, 0x3

    new-array v3, v3, [LX/0ux;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    aput-object v2, v3, v0

    invoke-static {v3}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    move-object v4, v0

    .line 555958
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/sms/SmsThreadManager;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "date DESC LIMIT "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 555959
    const-wide v2, 0x7fffffffffffffffL

    .line 555960
    const/4 v1, 0x0

    .line 555961
    if-eqz v4, :cond_8

    .line 555962
    :cond_4
    :goto_1
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Landroid/database/Cursor;->getPosition()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-ge v0, p1, :cond_7

    .line 555963
    :try_start_2
    invoke-static {p0, v4, p4}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 555964
    if-eqz v0, :cond_4

    .line 555965
    iget-wide v8, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v5, v8, v2

    if-lez v5, :cond_5

    .line 555966
    const/4 v1, 0x1

    .line 555967
    :cond_5
    iget-wide v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 555968
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 555969
    :catch_0
    move-exception v0

    .line 555970
    :try_start_3
    const-string v5, "SmsThreadManager"

    const-string v7, "Failed to load thread"

    invoke-static {v5, v7, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 555971
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 555972
    :catchall_1
    move-exception v0

    const v1, 0x7c4f46c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 555973
    :cond_6
    const/4 v0, 0x2

    :try_start_5
    new-array v0, v0, [LX/0ux;

    const/4 v3, 0x0

    aput-object v1, v0, v3

    const/4 v1, 0x1

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    .line 555974
    :cond_7
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 555975
    :cond_8
    if-eqz v1, :cond_9

    .line 555976
    new-instance v0, LX/6g7;

    invoke-direct {v0}, LX/6g7;-><init>()V

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 555977
    :cond_9
    const v0, -0x28f26a1d

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v6
.end method

.method public final a(J)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 555978
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMI;

    invoke-virtual {v0, p1, p2}, LX/FMI;->a(J)Ljava/util/List;

    move-result-object v0

    .line 555979
    if-eqz v0, :cond_0

    .line 555980
    :goto_0
    return-object v0

    .line 555981
    :cond_0
    const-string v0, "_id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 555982
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/sms/SmsThreadManager;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->c:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 555983
    if-eqz v2, :cond_3

    .line 555984
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 555985
    const-string v0, "recipient_ids"

    invoke-static {v2, v0}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 555986
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MF;

    invoke-virtual {v0, v1}, LX/3MF;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 555987
    if-eqz v1, :cond_1

    .line 555988
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMI;

    invoke-virtual {v0, p1, p2, v1}, LX/FMI;->a(JLjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555989
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 555990
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/sms/SmsThreadManager;->f(J)Ljava/util/List;

    move-result-object v1

    .line 555991
    const-string v2, "SmsThreadManager"

    const-string v3, "Reverted to get thread hack %s"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz v1, :cond_4

    const-string v0, "successfully"

    :goto_1
    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 555992
    goto :goto_0

    .line 555993
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 555994
    :cond_4
    const-string v0, "unsuccessfully"

    goto :goto_1
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    const/4 v3, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 555995
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v0, v8, :cond_0

    .line 555996
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1, v0}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/messaging/sms/SmsThreadManager;->c(J)V

    .line 555997
    :goto_0
    return-void

    .line 555998
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->l:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 555999
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Mo;

    invoke-virtual {v0, p1}, LX/3Mo;->a(Ljava/util/Collection;)V

    goto :goto_0

    .line 556000
    :cond_1
    new-array v0, v3, [LX/0ux;

    const-string v1, "read"

    const-string v2, "0"

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v9

    const-string v1, "seen"

    const-string v2, "0"

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static {v0}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v0

    .line 556001
    const-string v1, "thread_id"

    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v1

    .line 556002
    new-array v2, v3, [LX/0ux;

    aput-object v0, v2, v9

    aput-object v1, v2, v8

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 556003
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 556004
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 556005
    if-eqz v0, :cond_2

    .line 556006
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v6, v10

    if-gez v1, :cond_3

    .line 556007
    iget-object v1, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->u:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FME;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, LX/FME;->a(J)V

    goto :goto_1

    .line 556008
    :cond_3
    sget-object v1, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v1, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 556009
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 556010
    const-string v1, "read"

    const-string v5, "1"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 556011
    const-string v1, "seen"

    const-string v5, "1"

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 556012
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 556013
    :cond_4
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mms-sms"

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 556014
    :catch_0
    move-exception v0

    .line 556015
    :goto_2
    const-string v1, "SmsThreadManager"

    const-string v2, "sms/mms thread mark read failed. # threads = %d"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 556016
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public final a(Ljava/util/Set;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 556017
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 556018
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 556019
    if-eqz v0, :cond_0

    .line 556020
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-gez v1, :cond_1

    .line 556021
    iget-object v1, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->u:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FME;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, LX/FME;->c(J)V

    goto :goto_0

    .line 556022
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(J)Ljava/util/List;

    move-result-object v2

    .line 556023
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v10, :cond_2

    .line 556024
    iget-object v1, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->z:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FNO;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 556025
    iget-object v6, v1, LX/FNO;->a:LX/3MI;

    invoke-virtual {v6, v2}, LX/3MI;->c(Ljava/lang/String;)V

    .line 556026
    :cond_2
    sget-object v1, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v1, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 556027
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    .line 556028
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 556029
    iget-object v1, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6cV;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 556030
    sget-object v2, LX/6cS;->a:LX/0U1;

    .line 556031
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 556032
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 556033
    iget-object v8, v1, LX/6cV;->a:LX/6cW;

    invoke-virtual {v8}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, "time_skew"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v9, v11, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 556034
    iget-object v1, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->B:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FMI;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 556035
    invoke-static {v1}, LX/FMI;->a(LX/FMI;)V

    .line 556036
    iget-object v0, v1, LX/FMI;->c:LX/0tf;

    invoke-virtual {v0, v6, v7}, LX/0tf;->b(J)V

    .line 556037
    goto/16 :goto_0

    .line 556038
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mms-sms"

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 556039
    return-void

    .line 556040
    :catch_0
    move-exception v0

    .line 556041
    :goto_1
    const-string v1, "SmsThreadManager"

    const-string v2, "sms/mms thread deletion failed. # threads = %d"

    new-array v3, v10, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 556042
    new-instance v0, LX/FMS;

    const-string v1, "Failed to delete sms thread."

    invoke-direct {v0, v1}, LX/FMS;-><init>(Ljava/lang/String;)V

    throw v0

    .line 556043
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 555868
    const/4 v6, 0x0

    .line 555869
    if-nez p1, :cond_0

    .line 555870
    const-string v3, "message_count=0"

    .line 555871
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/sms/SmsThreadManager;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->b:[Ljava/lang/String;

    const-string v5, "date DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 555872
    if-eqz v1, :cond_0

    .line 555873
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_3

    .line 555874
    const/4 v0, 0x1

    .line 555875
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v6, v0

    .line 555876
    :cond_0
    if-nez p1, :cond_1

    if-eqz v6, :cond_2

    .line 555877
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/2Uo;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 555878
    :cond_2
    return-void

    .line 555879
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    move v0, v6

    goto :goto_0
.end method

.method public final b(J)I
    .locals 1

    .prologue
    .line 556044
    invoke-virtual {p0, p1, p2}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(J)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(Ljava/util/Collection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 556045
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 556046
    if-eqz v0, :cond_0

    .line 556047
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-gez v1, :cond_1

    .line 556048
    iget-object v1, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->u:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FME;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, LX/FME;->b(J)V

    goto :goto_0

    .line 556049
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/messaging/sms/SmsThreadManager;->g(J)V

    goto :goto_0

    .line 556050
    :cond_2
    return-void
.end method

.method public final c(J)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 556051
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->l:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 556052
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Mo;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Mo;->a(Ljava/util/Collection;)V

    .line 556053
    :cond_0
    :goto_0
    return-void

    .line 556054
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    .line 556055
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FME;

    invoke-virtual {v0, p1, p2}, LX/FME;->a(J)V

    goto :goto_0

    .line 556056
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 556057
    sget-object v1, LX/2Uo;->a:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 556058
    new-instance v2, Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "thread_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 556059
    const-string v3, " and ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556060
    const-string v3, "read=0 or seen=0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556061
    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556062
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 556063
    :try_start_0
    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->d:[Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 556064
    if-eqz v2, :cond_3

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 556065
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    .line 556066
    sget-object v4, Lcom/facebook/messaging/sms/SmsThreadManager;->g:Landroid/content/ContentValues;

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v4, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 556067
    :cond_3
    if-eqz v2, :cond_0

    .line 556068
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 556069
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_4

    .line 556070
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 556071
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public final d(J)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 556072
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->i:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556073
    :cond_0
    :goto_0
    return v7

    .line 556074
    :cond_1
    const-string v0, "_id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 556075
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/sms/SmsThreadManager;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/sms/SmsThreadManager;->e:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 556076
    if-eqz v1, :cond_5

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 556077
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_3

    move v0, v6

    :goto_1
    move v7, v0

    .line 556078
    :goto_2
    if-eqz v1, :cond_2

    .line 556079
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 556080
    :cond_2
    if-nez v7, :cond_0

    .line 556081
    iget-object v0, p0, Lcom/facebook/messaging/sms/SmsThreadManager;->i:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move v0, v7

    .line 556082
    goto :goto_1

    .line 556083
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_3
    if-eqz v1, :cond_4

    .line 556084
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 556085
    :catchall_1
    move-exception v0

    goto :goto_3

    :cond_5
    move v7, v6

    goto :goto_2
.end method
