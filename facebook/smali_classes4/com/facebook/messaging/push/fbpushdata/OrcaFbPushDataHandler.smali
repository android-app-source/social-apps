.class public Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/3Pw;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final O:Ljava/lang/Object;

.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Landroid/content/Context;

.field public final B:LX/2Mq;

.field private final C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Mk;",
            ">;"
        }
    .end annotation
.end field

.field private final D:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final E:LX/3A0;

.field public final F:Ljava/util/Random;

.field private final G:LX/3QU;

.field private final H:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final I:LX/3QV;

.field public final J:LX/2BG;

.field private final K:LX/2XO;

.field private final L:LX/0Uh;

.field private final M:LX/3GI;

.field private final N:LX/3QY;

.field private final c:LX/0WJ;

.field private final d:Landroid/content/res/Resources;

.field public final e:LX/3QI;

.field private final f:LX/3QN;

.field private final g:LX/2a9;

.field private final h:LX/3QL;

.field private final i:LX/3Q2;

.field private final j:LX/150;

.field private final k:LX/23j;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/01T;

.field private final n:LX/0lC;

.field public final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/4Al;",
            ">;"
        }
    .end annotation
.end field

.field public final p:Lcom/facebook/messaging/cache/ReadThreadManager;

.field public final q:LX/2gZ;

.field public final r:LX/0aG;

.field private final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0kb;

.field private final u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field public final v:LX/2N4;

.field private final w:LX/03V;

.field private final x:LX/0Xl;

.field private final y:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final z:LX/3GK;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 564001
    const-class v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;

    sput-object v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a:Ljava/lang/Class;

    .line 564002
    const/16 v0, 0x17

    new-array v0, v0, [Lcom/facebook/notifications/constants/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MSG:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ORCA_MESSAGE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->WEBRTC_VOIP_CALL:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ORCA_FRIEND_MSG:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->VOIP:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->VOIP_PRESENCE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ORCA_THREAD_READ:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_STATUS_CHANGE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->WAKEUP_MQTT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ZP:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->P2P_PAYMENT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_STALE_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSAGE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->INTERNAL:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PRE_REG_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_EVENT_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_GROUP_JOIN_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_FIRST_POST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_MESSAGE_EXPIRING:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_DAILY_DIGEST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_REACTIONS:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->b:LX/2QP;

    .line 564003
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->O:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0WJ;Landroid/content/res/Resources;LX/3QI;LX/3QN;LX/2a9;LX/3QL;LX/3Q2;LX/150;LX/23j;LX/0Ot;LX/0lC;LX/01T;LX/0Or;Lcom/facebook/messaging/cache/ReadThreadManager;LX/2gZ;LX/0aG;LX/0Or;LX/0kb;LX/0Or;LX/2N4;LX/03V;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3GK;Landroid/content/Context;LX/2Mq;LX/0Ot;LX/0Or;LX/3A0;LX/3QU;LX/0Or;LX/3QV;LX/2BG;LX/2XO;LX/3GI;LX/0Uh;LX/3QY;)V
    .locals 2
    .param p17    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/push/fbpushdata/IgnoreReadPushGateKeeper;
        .end annotation
    .end param
    .param p22    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p28    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/customthreads/annotations/CanViewThreadCustomization;
        .end annotation
    .end param
    .param p31    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "Landroid/content/res/Resources;",
            "LX/3QI;",
            "LX/3QN;",
            "LX/2a9;",
            "LX/3QL;",
            "LX/3Q2;",
            "LX/150;",
            "LX/23j;",
            "LX/0Ot",
            "<",
            "LX/4Ad;",
            ">;",
            "LX/0lC;",
            "LX/01T;",
            "LX/0Or",
            "<",
            "LX/4Al;",
            ">;",
            "Lcom/facebook/messaging/cache/ReadThreadManager;",
            "LX/2gZ;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0kb;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;",
            "LX/2N4;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Xl;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/3GK;",
            "Landroid/content/Context;",
            "LX/2Mq;",
            "LX/0Ot",
            "<",
            "LX/2Mk;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/3A0;",
            "LX/3QU;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3QV;",
            "LX/2BG;",
            "LX/2XO;",
            "LX/3GI;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/3QY;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 563961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563962
    iput-object p1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->c:LX/0WJ;

    .line 563963
    iput-object p2, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->d:Landroid/content/res/Resources;

    .line 563964
    iput-object p3, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563965
    iput-object p4, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->f:LX/3QN;

    .line 563966
    iput-object p5, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->g:LX/2a9;

    .line 563967
    iput-object p6, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->h:LX/3QL;

    .line 563968
    iput-object p7, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->i:LX/3Q2;

    .line 563969
    iput-object p8, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->j:LX/150;

    .line 563970
    iput-object p9, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->k:LX/23j;

    .line 563971
    iput-object p10, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->l:LX/0Ot;

    .line 563972
    iput-object p11, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->n:LX/0lC;

    .line 563973
    iput-object p12, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->m:LX/01T;

    .line 563974
    iput-object p13, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->o:LX/0Or;

    .line 563975
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->p:Lcom/facebook/messaging/cache/ReadThreadManager;

    .line 563976
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->q:LX/2gZ;

    .line 563977
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->r:LX/0aG;

    .line 563978
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->s:LX/0Or;

    .line 563979
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->t:LX/0kb;

    .line 563980
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->u:LX/0Or;

    .line 563981
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->v:LX/2N4;

    .line 563982
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->w:LX/03V;

    .line 563983
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->x:LX/0Xl;

    .line 563984
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 563985
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->z:LX/3GK;

    .line 563986
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->A:Landroid/content/Context;

    .line 563987
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->B:LX/2Mq;

    .line 563988
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->C:LX/0Ot;

    .line 563989
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->D:LX/0Or;

    .line 563990
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    iput-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->F:Ljava/util/Random;

    .line 563991
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->E:LX/3A0;

    .line 563992
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->G:LX/3QU;

    .line 563993
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->H:LX/0Or;

    .line 563994
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->I:LX/3QV;

    .line 563995
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->J:LX/2BG;

    .line 563996
    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->K:LX/2XO;

    .line 563997
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->L:LX/0Uh;

    .line 563998
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->M:LX/3GI;

    .line 563999
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->N:LX/3QY;

    .line 564000
    return-void
.end method

.method private static a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;LX/0lF;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 563952
    const-string v0, "gti"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563953
    const-string v0, "gti"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 563954
    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 563955
    :goto_0
    return-object v0

    .line 563956
    :cond_0
    const-string v0, "oui"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563957
    const-string v0, "oui"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 563958
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->H:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 563959
    invoke-static {v2, v3, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0

    .line 563960
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;
    .locals 7

    .prologue
    .line 563925
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 563926
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 563927
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 563928
    if-nez v1, :cond_0

    .line 563929
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 563930
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 563931
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 563932
    sget-object v1, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->O:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 563933
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 563934
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 563935
    :cond_1
    if-nez v1, :cond_4

    .line 563936
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 563937
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 563938
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->b(LX/0QB;)Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 563939
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 563940
    if-nez v1, :cond_2

    .line 563941
    sget-object v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->O:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 563942
    :goto_1
    if-eqz v0, :cond_3

    .line 563943
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 563944
    :goto_3
    check-cast v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 563945
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 563946
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 563947
    :catchall_1
    move-exception v0

    .line 563948
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 563949
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 563950
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 563951
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->O:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 563920
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->D:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563921
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 563922
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    move-object v0, v1

    .line 563923
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 563924
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;LX/3RF;)V
    .locals 3

    .prologue
    .line 563889
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->M:LX/3GI;

    .line 563890
    iget-object v1, v0, LX/3GI;->b:LX/3QX;

    sget-object v2, LX/6hP;->MONTAGE_NOTIFICATIONS_ENABLED:LX/6hP;

    invoke-virtual {v1, v2}, LX/3QX;->a(LX/6hP;)LX/0am;

    move-result-object v1

    .line 563891
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 563892
    if-nez v0, :cond_0

    .line 563893
    :goto_0
    return-void

    .line 563894
    :cond_0
    new-instance v0, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    invoke-direct {v0, p1, p2, p3}, Lcom/facebook/messaging/notify/SimpleMessageNotification;-><init>(Ljava/lang/String;Lcom/facebook/push/PushProperty;LX/3RF;)V

    .line 563895
    sget-object v1, LX/Jp3;->a:[I

    invoke-virtual {p3}, LX/3RF;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 563896
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563897
    iget-object v2, v1, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563898
    new-instance p1, Landroid/content/Intent;

    sget-object p0, LX/2b2;->G:Ljava/lang/String;

    invoke-direct {p1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563899
    const-string p0, "notification"

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563900
    iget-object p0, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Bae;

    iget-object v1, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {p0, p1, v1}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563901
    goto :goto_0

    .line 563902
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563903
    iget-object v2, v1, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563904
    new-instance p1, Landroid/content/Intent;

    sget-object p0, LX/2b2;->H:Ljava/lang/String;

    invoke-direct {p1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563905
    const-string p0, "notification"

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563906
    iget-object p0, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Bae;

    iget-object v1, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {p0, p1, v1}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563907
    goto :goto_0

    .line 563908
    :pswitch_2
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563909
    iget-object v2, v1, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563910
    new-instance p1, Landroid/content/Intent;

    sget-object p0, LX/2b2;->I:Ljava/lang/String;

    invoke-direct {p1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563911
    const-string p0, "notification"

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563912
    iget-object p0, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Bae;

    iget-object v1, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {p0, p1, v1}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563913
    goto :goto_0

    .line 563914
    :pswitch_3
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563915
    iget-object v2, v1, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563916
    new-instance p1, Landroid/content/Intent;

    sget-object p0, LX/2b2;->J:Ljava/lang/String;

    invoke-direct {p1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563917
    const-string p0, "notification"

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563918
    iget-object p0, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Bae;

    iget-object v1, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {p0, p1, v1}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563919
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V
    .locals 7
    .param p2    # Lcom/facebook/push/PushProperty;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/push/PushProperty;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 563887
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->h:LX/3QL;

    const-string v1, ""

    const/4 v2, 0x0

    iget-object v3, p2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v3}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p2, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    move-object v5, p1

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, LX/3QL;->a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 563888
    return-void
.end method

.method private a(Ljava/lang/String;LX/0lF;)V
    .locals 4

    .prologue
    .line 563501
    invoke-static {p0, p2}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;LX/0lF;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 563502
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->N:LX/3QY;

    invoke-virtual {v1, v0}, LX/3QY;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0db;->e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 563503
    :cond_0
    :goto_0
    return-void

    .line 563504
    :cond_1
    new-instance v1, Lcom/facebook/messaging/notify/MessageReactionNotification;

    invoke-direct {v1, v0, p1}, Lcom/facebook/messaging/notify/MessageReactionNotification;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    .line 563505
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563506
    iget-object v2, v0, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563507
    new-instance p0, Landroid/content/Intent;

    sget-object v3, LX/2b2;->K:Ljava/lang/String;

    invoke-direct {p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563508
    const-string v3, "notification"

    invoke-virtual {p0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563509
    iget-object v3, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bae;

    iget-object v0, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v3, p0, v0}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563510
    goto :goto_0
.end method

.method private declared-synchronized a(Ljava/lang/String;LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 11

    .prologue
    .line 563775
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->f:LX/3QN;

    invoke-virtual {v0, p1, p2}, LX/3QN;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 563776
    const/4 v4, 0x0

    .line 563777
    const-string v0, "disable_sound"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0, v4}, LX/16N;->a(LX/0lF;Z)Z

    move-result v0

    .line 563778
    const-string v2, "disable_vibrate"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2, v4}, LX/16N;->a(LX/0lF;Z)Z

    move-result v2

    .line 563779
    const-string v3, "disable_light"

    invoke-virtual {p2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3, v4}, LX/16N;->a(LX/0lF;Z)Z

    move-result v3

    .line 563780
    new-instance v4, LX/Dnh;

    invoke-direct {v4}, LX/Dnh;-><init>()V

    .line 563781
    iput-boolean v0, v4, LX/Dnh;->a:Z

    .line 563782
    move-object v0, v4

    .line 563783
    iput-boolean v2, v0, LX/Dnh;->b:Z

    .line 563784
    move-object v0, v0

    .line 563785
    iput-boolean v3, v0, LX/Dnh;->c:Z

    .line 563786
    move-object v0, v0

    .line 563787
    invoke-virtual {v0}, LX/Dnh;->a()Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    move-result-object v0

    move-object v8, v0

    .line 563788
    const-string v0, "unified_tid"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/DoA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 563789
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->f:LX/3QN;

    const/4 v2, 0x0

    .line 563790
    const-string v3, "gpc"

    invoke-virtual {p2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 563791
    :goto_0
    move-object v3, v2

    .line 563792
    if-nez v1, :cond_0

    .line 563793
    const-string v0, "invalid_payload"

    const/4 v1, 0x0

    invoke-static {p0, v0, p3, v1}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 563794
    :goto_1
    monitor-exit p0

    return-void

    .line 563795
    :cond_0
    :try_start_1
    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 563796
    if-nez v2, :cond_6

    .line 563797
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;

    invoke-virtual {v0, v4}, LX/2Og;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 563798
    if-nez v0, :cond_1

    .line 563799
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->v:LX/2N4;

    .line 563800
    new-instance v5, Lcom/facebook/messaging/model/threads/ThreadCriteria;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v5, v2, v6}, Lcom/facebook/messaging/model/threads/ThreadCriteria;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v5

    .line 563801
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4}, LX/2N4;->a(Lcom/facebook/messaging/model/threads/ThreadCriteria;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 563802
    :cond_1
    if-eqz v0, :cond_5

    .line 563803
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 563804
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 563805
    iput-object v5, v4, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 563806
    move-object v4, v4

    .line 563807
    iget-object v5, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->C:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v1}, LX/2Mk;->u(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 563808
    invoke-static {p0, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;

    move-result-object v0

    .line 563809
    if-eqz v0, :cond_2

    .line 563810
    iput-object v0, v4, LX/6f7;->f:Ljava/lang/String;

    .line 563811
    :cond_2
    invoke-virtual {v4}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 563812
    :cond_3
    :goto_2
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 563813
    iget-object v4, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->v:LX/2N4;

    invoke-virtual {v4, v0}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    .line 563814
    if-eqz v4, :cond_d

    iget-boolean v4, v4, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v4, :cond_d

    const/4 v4, 0x1

    :goto_3
    move v0, v4

    .line 563815
    if-nez v0, :cond_4

    .line 563816
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->x:LX/0Xl;

    sget-object v4, LX/2Sy;->a:Ljava/lang/String;

    invoke-interface {v0, v4}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 563817
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->m:LX/01T;

    sget-object v4, LX/01T;->MESSENGER:LX/01T;

    if-eq v0, v4, :cond_8

    .line 563818
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->j:LX/150;

    iget-object v4, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->i:LX/3Q2;

    invoke-virtual {v4}, LX/3Q2;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/151;->a(Ljava/lang/String;)LX/152;

    move-result-object v0

    .line 563819
    iget-boolean v0, v0, LX/152;->a:Z

    if-eqz v0, :cond_7

    .line 563820
    const-string v0, "eaten_messenger"

    const/4 v1, 0x0

    invoke-static {p0, v0, p3, v1}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 563821
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 563822
    :cond_5
    :try_start_2
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->w:LX/03V;

    const-string v1, "OrcaC2DMPush"

    const-string v2, "Received C2DM push for unrecognized threadId."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 563823
    :cond_6
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->D:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v1}, LX/2Mk;->u(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 563824
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;

    invoke-virtual {v0, v2}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 563825
    invoke-static {p0, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;

    move-result-object v0

    .line 563826
    if-eqz v0, :cond_3

    .line 563827
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v1

    .line 563828
    iput-object v0, v1, LX/6f7;->f:Ljava/lang/String;

    .line 563829
    move-object v0, v1

    .line 563830
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    goto :goto_2

    .line 563831
    :cond_7
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->k:LX/23j;

    iget-object v4, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->i:LX/3Q2;

    invoke-virtual {v4}, LX/3Q2;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/151;->a(Ljava/lang/String;)LX/152;

    move-result-object v0

    .line 563832
    iget-boolean v0, v0, LX/152;->a:Z

    if-eqz v0, :cond_8

    .line 563833
    const-string v0, "eaten_messenger_lite"

    const/4 v1, 0x0

    invoke-static {p0, v0, p3, v1}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    goto/16 :goto_1

    .line 563834
    :cond_8
    iget-wide v4, p3, Lcom/facebook/push/PushProperty;->c:J

    iget-wide v6, v1, Lcom/facebook/messaging/model/messages/Message;->c:J

    sub-long/2addr v4, v6

    .line 563835
    iget-object v0, p3, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-static {v0}, LX/3B4;->isPushNotification(LX/3B4;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-wide/32 v6, 0x1b7740

    cmp-long v0, v4, v6

    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->t:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->e()LX/0am;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x493e0

    cmp-long v0, v4, v6

    if-gez v0, :cond_9

    .line 563836
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 563837
    iget-object v4, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->F:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v5

    .line 563838
    iget-object v4, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->B:LX/2Mq;

    const-string v6, "OrcaFbPushDataHandler"

    invoke-virtual {v4, v5, v6}, LX/2Mq;->a(ILjava/lang/String;)V

    .line 563839
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 563840
    new-instance v6, LX/6iM;

    invoke-direct {v6}, LX/6iM;-><init>()V

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v7

    .line 563841
    iput-object v7, v6, LX/6iM;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 563842
    move-object v6, v6

    .line 563843
    sget-object v7, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 563844
    iput-object v7, v6, LX/6iM;->b:LX/0rS;

    .line 563845
    move-object v6, v6

    .line 563846
    const/4 v7, 0x0

    .line 563847
    iput v7, v6, LX/6iM;->g:I

    .line 563848
    move-object v6, v6

    .line 563849
    invoke-virtual {v6}, LX/6iM;->j()Lcom/facebook/messaging/service/model/FetchThreadParams;

    move-result-object v6

    .line 563850
    const-string v7, "fetchThreadParams"

    invoke-virtual {v4, v7, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 563851
    const-string v6, "logger_instance_key"

    invoke-virtual {v4, v6, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 563852
    iget-object v6, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->r:LX/0aG;

    const-string v7, "fetch_thread"

    const-class v9, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;

    invoke-static {v9}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v9

    const v10, -0x77ef3ac9

    invoke-static {v6, v7, v4, v9, v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v6

    .line 563853
    invoke-static {v6}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/fbservice/service/OperationResult;

    .line 563854
    new-instance v7, LX/Jp2;

    invoke-direct {v7, p0, v5}, LX/Jp2;-><init>(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;I)V

    invoke-static {v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 563855
    if-nez v4, :cond_e

    .line 563856
    const/4 v4, 0x1

    .line 563857
    :goto_4
    move v0, v4

    .line 563858
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 563859
    if-nez v0, :cond_9

    .line 563860
    const-string v0, "dropped_by_readness"

    const/4 v1, 0x0

    invoke-static {p0, v0, p3, v1}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    goto/16 :goto_1

    .line 563861
    :cond_9
    const-string v0, "mu"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v4, "-1"

    invoke-static {v0, v4}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 563862
    const/4 v4, 0x1

    if-ne v0, v4, :cond_a

    sget-object v9, LX/DiB;->IS_MESSENGER_USER:LX/DiB;

    .line 563863
    :goto_5
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->g:LX/2a9;

    invoke-virtual {v0, v1}, LX/2a9;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 563864
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-static {v1}, LX/2Mk;->m(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v7

    move-object v0, p1

    move-object v5, p3

    invoke-static/range {v0 .. v9}, LX/3QU;->a(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/GroupMessageInfo;LX/DiC;Lcom/facebook/push/PushProperty;LX/Dhq;ZLcom/facebook/messaging/push/flags/ServerMessageAlertFlags;LX/DiB;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v0

    .line 563865
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    const-wide/16 v2, -0x1

    .line 563866
    iget-object v4, v1, LX/3QI;->e:LX/0Uq;

    invoke-virtual {v4}, LX/0Uq;->b()V

    .line 563867
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 563868
    const-string v5, "prevLastVisibleActionId"

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 563869
    const-string v5, "message"

    iget-object v6, v0, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 563870
    const-string v5, "pushProperty"

    iget-object v6, v0, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 563871
    iget-object v5, v1, LX/3QI;->d:LX/0aG;

    const-string v6, "pushed_message"

    const v7, -0x50514c64

    invoke-static {v5, v6, v4, v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    .line 563872
    invoke-virtual {v1, v0}, LX/3QI;->a(Lcom/facebook/messaging/notify/NewMessageNotification;)V

    .line 563873
    goto/16 :goto_1

    .line 563874
    :cond_a
    if-nez v0, :cond_b

    sget-object v9, LX/DiB;->IS_NOT_MESSENGER_USER:LX/DiB;

    goto :goto_5

    :cond_b
    sget-object v9, LX/DiB;->UNKNOWN:LX/DiB;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 563875
    :cond_c
    :try_start_3
    const-string v3, "gpc"

    invoke-virtual {p2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->d(LX/0lF;)I

    move-result v3

    .line 563876
    const-string v5, "gpi"

    invoke-virtual {p2, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 563877
    const-string v6, "gpn"

    invoke-virtual {p2, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 563878
    const-string v7, "gn"

    invoke-virtual {p2, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    .line 563879
    const-string v9, "gp"

    invoke-virtual {p2, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    invoke-static {v9}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 563880
    :try_start_4
    iget-object v10, v0, LX/3QN;->e:LX/0lC;

    invoke-virtual {v10, v5}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 563881
    iget-object v10, v0, LX/3QN;->e:LX/0lC;

    invoke-virtual {v10, v6}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    move-result-object v6

    .line 563882
    new-instance v2, Lcom/facebook/messaging/model/threads/GroupMessageInfo;

    invoke-static {v5, v6}, LX/3QN;->a(LX/0lF;LX/0lF;)LX/0Px;

    move-result-object v5

    invoke-direct {v2, v3, v5, v7, v9}, Lcom/facebook/messaging/model/threads/GroupMessageInfo;-><init>(ILjava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 563883
    :catch_0
    sget-object v3, LX/3QN;->a:Ljava/lang/Class;

    const-string v5, "Error deserializing ids and names. Return no group info."

    invoke-static {v3, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_d
    :try_start_6
    const/4 v4, 0x0

    goto/16 :goto_3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 563884
    :cond_e
    invoke-virtual {v4}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 563885
    iget-object v4, v4, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 563886
    invoke-virtual {v4}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v4

    goto/16 :goto_4
.end method

.method private a(Ljava/lang/String;Lcom/facebook/push/PushProperty;)V
    .locals 2

    .prologue
    .line 563768
    new-instance v0, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    sget-object v1, LX/3RF;->INTERNAL:LX/3RF;

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/notify/SimpleMessageNotification;-><init>(Ljava/lang/String;Lcom/facebook/push/PushProperty;LX/3RF;)V

    .line 563769
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563770
    iget-object p0, v1, LX/3QI;->b:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Di5;

    .line 563771
    new-instance p2, Landroid/content/Intent;

    sget-object p1, LX/2b2;->y:Ljava/lang/String;

    invoke-direct {p2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563772
    const-string p1, "notification"

    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563773
    iget-object p1, p0, LX/Di5;->c:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/Bae;

    iget-object v1, p0, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {p1, p2, v1}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563774
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;
    .locals 40

    .prologue
    .line 563766
    new-instance v2, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/3QI;->a(LX/0QB;)LX/3QI;

    move-result-object v5

    check-cast v5, LX/3QI;

    invoke-static/range {p0 .. p0}, LX/3QN;->a(LX/0QB;)LX/3QN;

    move-result-object v6

    check-cast v6, LX/3QN;

    invoke-static/range {p0 .. p0}, LX/2a9;->a(LX/0QB;)LX/2a9;

    move-result-object v7

    check-cast v7, LX/2a9;

    invoke-static/range {p0 .. p0}, LX/3QL;->a(LX/0QB;)LX/3QL;

    move-result-object v8

    check-cast v8, LX/3QL;

    invoke-static/range {p0 .. p0}, LX/3Q2;->a(LX/0QB;)LX/3Q2;

    move-result-object v9

    check-cast v9, LX/3Q2;

    invoke-static/range {p0 .. p0}, LX/150;->a(LX/0QB;)LX/150;

    move-result-object v10

    check-cast v10, LX/150;

    invoke-static/range {p0 .. p0}, LX/23j;->a(LX/0QB;)LX/23j;

    move-result-object v11

    check-cast v11, LX/23j;

    const/16 v12, 0x1c34

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v13

    check-cast v13, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v14

    check-cast v14, LX/01T;

    const/16 v15, 0x1c58

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/cache/ReadThreadManager;->a(LX/0QB;)Lcom/facebook/messaging/cache/ReadThreadManager;

    move-result-object v16

    check-cast v16, Lcom/facebook/messaging/cache/ReadThreadManager;

    invoke-static/range {p0 .. p0}, LX/2gZ;->a(LX/0QB;)LX/2gZ;

    move-result-object v17

    check-cast v17, LX/2gZ;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v18

    check-cast v18, LX/0aG;

    const/16 v19, 0x337

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v20

    check-cast v20, LX/0kb;

    const/16 v21, 0xce8

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v22

    check-cast v22, LX/2N4;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v23

    check-cast v23, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v24

    check-cast v24, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v25

    check-cast v25, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/35f;->a(LX/0QB;)LX/35f;

    move-result-object v26

    check-cast v26, LX/3GK;

    const-class v27, Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/2Mq;->a(LX/0QB;)LX/2Mq;

    move-result-object v28

    check-cast v28, LX/2Mq;

    const/16 v29, 0xd66

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v29

    const/16 v30, 0x1510

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v30

    invoke-static/range {p0 .. p0}, LX/3A0;->a(LX/0QB;)LX/3A0;

    move-result-object v31

    check-cast v31, LX/3A0;

    invoke-static/range {p0 .. p0}, LX/3QU;->a(LX/0QB;)LX/3QU;

    move-result-object v32

    check-cast v32, LX/3QU;

    const/16 v33, 0x15e8

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v33

    invoke-static/range {p0 .. p0}, LX/3QV;->a(LX/0QB;)LX/3QV;

    move-result-object v34

    check-cast v34, LX/3QV;

    invoke-static/range {p0 .. p0}, LX/2BG;->a(LX/0QB;)LX/2BG;

    move-result-object v35

    check-cast v35, LX/2BG;

    invoke-static/range {p0 .. p0}, LX/2XO;->a(LX/0QB;)LX/2XO;

    move-result-object v36

    check-cast v36, LX/2XO;

    invoke-static/range {p0 .. p0}, LX/3GI;->a(LX/0QB;)LX/3GI;

    move-result-object v37

    check-cast v37, LX/3GI;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v38

    check-cast v38, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/3QY;->a(LX/0QB;)LX/3QY;

    move-result-object v39

    check-cast v39, LX/3QY;

    invoke-direct/range {v2 .. v39}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;-><init>(LX/0WJ;Landroid/content/res/Resources;LX/3QI;LX/3QN;LX/2a9;LX/3QL;LX/3Q2;LX/150;LX/23j;LX/0Ot;LX/0lC;LX/01T;LX/0Or;Lcom/facebook/messaging/cache/ReadThreadManager;LX/2gZ;LX/0aG;LX/0Or;LX/0kb;LX/0Or;LX/2N4;LX/03V;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3GK;Landroid/content/Context;LX/2Mq;LX/0Ot;LX/0Or;LX/3A0;LX/3QU;LX/0Or;LX/3QV;LX/2BG;LX/2XO;LX/3GI;LX/0Uh;LX/3QY;)V

    .line 563767
    return-object v2
.end method

.method private b(LX/0lF;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 563765
    if-eqz p1, :cond_0

    const-string v0, "path"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "path"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->z:LX/3GK;

    invoke-interface {v0}, LX/3GK;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/facebook/push/PushProperty;)V
    .locals 3

    .prologue
    .line 563759
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->L:LX/0Uh;

    const/16 v1, 0x25a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 563760
    :cond_0
    :goto_0
    return-void

    .line 563761
    :cond_1
    sget-object v0, LX/3B4;->C2DM:LX/3B4;

    iget-object v1, p1, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v0, v1}, LX/3B4;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, LX/3B4;->FBNS_LITE:LX/3B4;

    iget-object v1, p1, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v0, v1}, LX/3B4;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563762
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->K:LX/2XO;

    .line 563763
    iget-object v1, v0, LX/2XO;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushMqttClientActiveCallback$1;

    invoke-direct {v2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushMqttClientActiveCallback$1;-><init>(LX/2XO;)V

    const p0, 0x1562a5c4

    invoke-static {v1, v2, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 563764
    goto :goto_0
.end method

.method private c(LX/0lF;)V
    .locals 14

    .prologue
    .line 563733
    const-string v0, "thread_timestamps"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 563734
    if-nez v0, :cond_0

    .line 563735
    :goto_0
    return-void

    .line 563736
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->n:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 563737
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 563738
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v3

    .line 563739
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 563740
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 563741
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, LX/DoA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 563742
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->u:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Og;

    invoke-virtual {v1, v4}, LX/2Og;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 563743
    if-eqz v1, :cond_1

    .line 563744
    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 563745
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    const-wide/16 v4, -0x1

    invoke-static {v0, v4, v5}, LX/16N;->a(LX/0lF;J)J

    move-result-wide v4

    .line 563746
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 563747
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 563748
    iget-object v8, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->p:Lcom/facebook/messaging/cache/ReadThreadManager;

    invoke-static {v4, v5}, LX/6fa;->a(J)J

    move-result-wide v10

    move-object v9, v1

    move-wide v12, v4

    invoke-virtual/range {v8 .. v13}, Lcom/facebook/messaging/cache/ReadThreadManager;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 563749
    goto :goto_1

    .line 563750
    :catch_0
    sget-object v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a:Ljava/lang/Class;

    const-string v1, "Failed to parse thread_counts"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 563751
    :cond_2
    :try_start_1
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 563752
    new-instance v1, Lcom/facebook/messaging/notify/ReadThreadNotification;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/notify/ReadThreadNotification;-><init>(LX/0P1;)V

    .line 563753
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563754
    iget-object v2, v0, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563755
    new-instance v4, Landroid/content/Intent;

    sget-object v3, LX/2b2;->h:Ljava/lang/String;

    invoke-direct {v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563756
    const-string v3, "notification"

    invoke-virtual {v4, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563757
    iget-object v3, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bae;

    iget-object v0, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v0}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 563758
    goto/16 :goto_0
.end method

.method private g(Ljava/lang/String;Lcom/facebook/push/PushProperty;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 563711
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563712
    const-string v0, "invalid_payload"

    invoke-static {p0, v0, p2, v2}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    .line 563713
    :goto_0
    return-void

    .line 563714
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->m:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-eq v0, v1, :cond_1

    .line 563715
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->j:LX/150;

    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->i:LX/3Q2;

    invoke-virtual {v1}, LX/3Q2;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/151;->a(Ljava/lang/String;)LX/152;

    move-result-object v0

    .line 563716
    iget-boolean v0, v0, LX/152;->b:Z

    if-eqz v0, :cond_3

    .line 563717
    const-string v0, "eaten_messenger"

    invoke-static {p0, v0, p2, v2}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    goto :goto_0

    .line 563718
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->i:LX/3Q2;

    invoke-virtual {v0}, LX/3Q2;->a()Ljava/lang/String;

    move-result-object v0

    .line 563719
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4Ad;

    iget-object v3, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->A:Landroid/content/Context;

    invoke-virtual {v1, v3}, LX/4Ad;->a(Landroid/content/Context;)LX/3dh;

    move-result-object v1

    .line 563720
    if-eqz v1, :cond_2

    iget-object v3, v1, LX/3dh;->a:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 563721
    :cond_2
    const/4 v1, 0x0

    .line 563722
    :goto_1
    move v0, v1

    .line 563723
    if-eqz v0, :cond_3

    .line 563724
    const-string v0, "eaten_fb4a"

    invoke-static {p0, v0, p2, v2}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    goto :goto_0

    .line 563725
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->d:Landroid/content/res/Resources;

    const v1, 0x7f080011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 563726
    new-instance v1, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;

    invoke-direct {v1, v0, p1, p2}, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/push/PushProperty;)V

    .line 563727
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563728
    iget-object v2, v0, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563729
    new-instance p0, Landroid/content/Intent;

    sget-object v3, LX/2b2;->j:Ljava/lang/String;

    invoke-direct {p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563730
    const-string v3, "notification"

    invoke-virtual {p0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563731
    iget-object v3, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bae;

    iget-object v0, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v3, p0, v0}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563732
    goto :goto_0

    :cond_4
    iget-object v1, v1, LX/3dh;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 563710
    sget-object v0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->b:LX/2QP;

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 563511
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->m:LX/01T;

    sget-object v2, LX/01T;->PAA:LX/01T;

    if-ne v1, v2, :cond_1

    .line 563512
    :cond_0
    :goto_0
    return-void

    .line 563513
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->m:LX/01T;

    sget-object v2, LX/01T;->FB4A:LX/01T;

    if-ne v1, v2, :cond_2

    .line 563514
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0hM;->r:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    .line 563515
    if-eqz v1, :cond_0

    .line 563516
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->c:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->b()Z

    move-result v1

    .line 563517
    const-string v2, "is_logged_out_push"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->g(LX/0lF;)Z

    move-result v3

    .line 563518
    const-string v2, "message"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 563519
    const-string v4, "params"

    invoke-virtual {p1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 563520
    const-string v4, "type"

    invoke-virtual {p1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 563521
    sget-object v5, LX/01T;->MESSENGER:LX/01T;

    iget-object v7, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->m:LX/01T;

    if-ne v5, v7, :cond_3

    sget-object v5, Lcom/facebook/notifications/constants/NotificationType;->PRE_REG_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v5, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-nez v1, :cond_3

    .line 563522
    new-instance v0, Lcom/facebook/messaging/notify/SimpleMessageNotification;

    sget-object v1, LX/3RF;->PRE_REG_PUSH:LX/3RF;

    invoke-direct {v0, v2, p2, v1}, Lcom/facebook/messaging/notify/SimpleMessageNotification;-><init>(Ljava/lang/String;Lcom/facebook/push/PushProperty;LX/3RF;)V

    .line 563523
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563524
    iget-object p0, v1, LX/3QI;->b:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Di5;

    .line 563525
    new-instance p2, Landroid/content/Intent;

    sget-object v2, LX/2b2;->z:Ljava/lang/String;

    invoke-direct {p2, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563526
    const-string v2, "notification"

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563527
    iget-object v2, p0, LX/Di5;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Bae;

    iget-object v1, p0, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v2, p2, v1}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563528
    goto :goto_0

    .line 563529
    :cond_3
    iget-object v5, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->i:LX/3Q2;

    invoke-virtual {v5}, LX/3Q2;->a()Ljava/lang/String;

    move-result-object v5

    .line 563530
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 563531
    const-string v1, "no_user"

    invoke-static {p0, v1, p2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 563532
    :cond_4
    const-string v7, "target_uid"

    invoke-virtual {p1, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    .line 563533
    invoke-static {v7, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 563534
    const-string v1, "eaten_wrong_user"

    invoke-static {p0, v1, p2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 563535
    :cond_5
    if-nez v1, :cond_6

    if-nez v3, :cond_6

    .line 563536
    const-string v1, "logged_out_user"

    invoke-static {p0, v1, p2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 563537
    :cond_6
    if-eqz v1, :cond_7

    if-eqz v3, :cond_7

    .line 563538
    const-string v1, "logged_in_user"

    invoke-static {p0, v1, p2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 563539
    :cond_7
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->MSG:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->ORCA_MESSAGE:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->WEBRTC_VOIP_CALL:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 563540
    :cond_8
    if-eqz v3, :cond_b

    .line 563541
    invoke-direct {p0, v2, p2}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->g(Ljava/lang/String;Lcom/facebook/push/PushProperty;)V

    .line 563542
    :cond_9
    :goto_1
    invoke-direct {p0, p2}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->b(Lcom/facebook/push/PushProperty;)V

    .line 563543
    const-string v0, "params"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "params"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, "trace_info"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 563544
    const-string v0, "params"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 563545
    const-string v1, "trace_info"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 563546
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 563547
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 563548
    const-string v2, "traceInfo"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 563549
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4Al;

    .line 563550
    const/4 v2, 0x1

    .line 563551
    iput-boolean v2, v0, LX/4Al;->fireAndForget:Z

    .line 563552
    const-string v2, "push_trace_confirmation"

    .line 563553
    const/4 p0, 0x0

    invoke-virtual {v0, v2, v1, p0}, LX/4Al;->start(Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 563554
    :cond_a
    goto/16 :goto_0

    .line 563555
    :cond_b
    invoke-direct {p0, v2, v6, p2}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Ljava/lang/String;LX/0lF;Lcom/facebook/push/PushProperty;)V

    goto :goto_1

    .line 563556
    :cond_c
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->ORCA_FRIEND_MSG:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 563557
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->d:Landroid/content/res/Resources;

    const v3, 0x7f080011

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 563558
    invoke-static {v2, v1, v6, p2}, Lcom/facebook/messaging/notify/FriendInstallNotification;->a(Ljava/lang/String;Ljava/lang/String;LX/0lF;Lcom/facebook/push/PushProperty;)Lcom/facebook/messaging/notify/FriendInstallNotification;

    move-result-object v1

    .line 563559
    if-eqz v1, :cond_d

    .line 563560
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563561
    iget-object v2, v0, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563562
    new-instance v4, Landroid/content/Intent;

    sget-object v3, LX/2b2;->c:Ljava/lang/String;

    invoke-direct {v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563563
    const-string v3, "notification"

    invoke-virtual {v4, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563564
    iget-object v3, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bae;

    iget-object v0, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v0}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563565
    goto/16 :goto_1

    .line 563566
    :cond_d
    const-string v1, "invalid_payload"

    invoke-static {p0, v1, p2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    goto/16 :goto_1

    .line 563567
    :cond_e
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->VOIP:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 563568
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->E:LX/3A0;

    invoke-virtual {v0, v6}, LX/3A0;->b(LX/0lF;)V

    goto/16 :goto_1

    .line 563569
    :cond_f
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->VOIP_PRESENCE:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 563570
    if-nez v6, :cond_10

    :goto_2
    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    .line 563571
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->E:LX/3A0;

    invoke-virtual {v0, v6}, LX/3A0;->a(LX/0lF;)V

    goto/16 :goto_1

    .line 563572
    :cond_10
    const-string v0, "uid"

    invoke-virtual {v6, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    goto :goto_2

    .line 563573
    :cond_11
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->ORCA_THREAD_READ:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 563574
    invoke-direct {p0, v6}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->c(LX/0lF;)V

    goto/16 :goto_1

    .line 563575
    :cond_12
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_STATUS_CHANGE:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 563576
    const-string v0, "uid"

    invoke-virtual {v6, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 563577
    const-string v1, "is_messenger_user"

    invoke-virtual {v6, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 563578
    const-string v1, "is_messenger_user"

    invoke-virtual {v6, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    .line 563579
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 563580
    iget-object v2, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->q:LX/2gZ;

    invoke-virtual {v2, v0, v1}, LX/2gZ;->a(Ljava/lang/String;Z)V

    .line 563581
    :cond_13
    goto/16 :goto_1

    .line 563582
    :cond_14
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->WAKEUP_MQTT:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 563583
    const-string v0, "mqtt_wakeup_via_gcm"

    const/4 v1, 0x0

    invoke-static {p0, v0, p2, v1}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    .line 563584
    goto/16 :goto_1

    .line 563585
    :cond_15
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->ZP:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 563586
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->J:LX/2BG;

    invoke-virtual {v0, v6}, LX/2BG;->a(LX/0lF;)V

    .line 563587
    goto/16 :goto_1

    .line 563588
    :cond_16
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->P2P_PAYMENT:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 563589
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->d:Landroid/content/res/Resources;

    const v3, 0x7f080011

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 563590
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 563591
    const-string v5, "t"

    invoke-static {v6, v5}, Lcom/facebook/messaging/notify/PaymentNotification;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 563592
    if-nez v5, :cond_24

    move-object v3, v4

    .line 563593
    :goto_3
    move-object v1, v3

    .line 563594
    if-eqz v1, :cond_17

    .line 563595
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563596
    iget-object v2, v0, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563597
    new-instance v4, Landroid/content/Intent;

    sget-object v3, LX/2b2;->d:Ljava/lang/String;

    invoke-direct {v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563598
    const-string v3, "notification"

    invoke-virtual {v4, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563599
    iget-object v3, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bae;

    iget-object v0, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v0}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563600
    goto/16 :goto_1

    .line 563601
    :cond_17
    const-string v1, "invalid_payload"

    invoke-static {p0, v1, p2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;Ljava/util/Map;)V

    goto/16 :goto_1

    .line 563602
    :cond_18
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 563603
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->d:Landroid/content/res/Resources;

    const v1, 0x7f080011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 563604
    invoke-direct {p0, v6}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 563605
    new-instance v3, Lcom/facebook/messaging/notify/PromotionNotification;

    invoke-direct {v3, v0, v2, v2, v1}, Lcom/facebook/messaging/notify/PromotionNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 563606
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563607
    iget-object v1, v0, LX/3QI;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Di5;

    .line 563608
    new-instance v4, Landroid/content/Intent;

    sget-object v2, LX/2b2;->t:Ljava/lang/String;

    invoke-direct {v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563609
    const-string v2, "notification"

    invoke-virtual {v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563610
    iget-object v2, v1, LX/Di5;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Bae;

    iget-object v0, v1, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v2, v4, v0}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563611
    goto/16 :goto_1

    .line 563612
    :cond_19
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_STALE_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 563613
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->d:Landroid/content/res/Resources;

    const v1, 0x7f080011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 563614
    new-instance v1, Lcom/facebook/messaging/notify/StaleNotification;

    invoke-direct {v1, v0, v2, v2}, Lcom/facebook/messaging/notify/StaleNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 563615
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563616
    iget-object v2, v0, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563617
    new-instance v4, Landroid/content/Intent;

    sget-object v3, LX/2b2;->u:Ljava/lang/String;

    invoke-direct {v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563618
    const-string v3, "notification"

    invoke-virtual {v4, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563619
    iget-object v3, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bae;

    iget-object v0, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v0}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563620
    goto/16 :goto_1

    .line 563621
    :cond_1a
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSAGE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 563622
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->d:Landroid/content/res/Resources;

    const v1, 0x7f080011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 563623
    new-instance v1, Lcom/facebook/messaging/notify/MessageRequestNotification;

    invoke-direct {v1, v0, v2}, Lcom/facebook/messaging/notify/MessageRequestNotification;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 563624
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563625
    iget-object v2, v0, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563626
    new-instance v4, Landroid/content/Intent;

    sget-object v3, LX/2b2;->v:Ljava/lang/String;

    invoke-direct {v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563627
    const-string v3, "notification"

    invoke-virtual {v4, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563628
    iget-object v3, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bae;

    iget-object v0, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v0}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563629
    goto/16 :goto_1

    .line 563630
    :cond_1b
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->m:LX/01T;

    if-ne v0, v1, :cond_1c

    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->INTERNAL:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 563631
    invoke-direct {p0, v2, p2}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Ljava/lang/String;Lcom/facebook/push/PushProperty;)V

    goto/16 :goto_1

    .line 563632
    :cond_1c
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_EVENT_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 563633
    iget-object v0, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->d:Landroid/content/res/Resources;

    const v1, 0x7f080011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 563634
    invoke-direct {p0, v6}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 563635
    const-string v0, "lrt"

    invoke-virtual {v6, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 563636
    const-string v0, "let"

    invoke-virtual {v6, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v5

    .line 563637
    invoke-static {p0, v6}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;LX/0lF;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v6

    .line 563638
    if-eqz v6, :cond_9

    .line 563639
    new-instance v0, Lcom/facebook/messaging/notify/EventReminderNotification;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/notify/EventReminderNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLLightweightEventType;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 563640
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->e:LX/3QI;

    .line 563641
    iget-object v2, v1, LX/3QI;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    .line 563642
    new-instance v4, Landroid/content/Intent;

    sget-object v3, LX/2b2;->E:Ljava/lang/String;

    invoke-direct {v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563643
    const-string v3, "notification"

    invoke-virtual {v4, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563644
    iget-object v3, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bae;

    iget-object v1, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v1}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563645
    goto/16 :goto_1

    .line 563646
    :cond_1d
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_GROUP_JOIN_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 563647
    invoke-static {p0, v6}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;LX/0lF;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 563648
    iget-object v1, p0, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->I:LX/3QV;

    .line 563649
    iget-object v2, v1, LX/3QV;->a:LX/3QW;

    invoke-virtual {v2}, LX/3QW;->a()Z

    move-result v2

    if-nez v2, :cond_29

    .line 563650
    :cond_1e
    :goto_4
    goto/16 :goto_1

    .line 563651
    :cond_1f
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_FIRST_POST:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 563652
    sget-object v0, LX/3RF;->MESSENGER_MONTAGE_FIRST_POST:LX/3RF;

    invoke-static {p0, v2, p2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;LX/3RF;)V

    .line 563653
    goto/16 :goto_1

    .line 563654
    :cond_20
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_MESSAGE_EXPIRING:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 563655
    sget-object v0, LX/3RF;->MESSENGER_MONTAGE_MESSAGE_EXPIRING:LX/3RF;

    invoke-static {p0, v2, p2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;LX/3RF;)V

    .line 563656
    goto/16 :goto_1

    .line 563657
    :cond_21
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 563658
    sget-object v0, LX/3RF;->MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS:LX/3RF;

    invoke-static {p0, v2, p2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;LX/3RF;)V

    .line 563659
    goto/16 :goto_1

    .line 563660
    :cond_22
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_DAILY_DIGEST:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 563661
    sget-object v0, LX/3RF;->MESSENGER_MONTAGE_DAILY_DIGEST:LX/3RF;

    invoke-static {p0, v2, p2, v0}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;Ljava/lang/String;Lcom/facebook/push/PushProperty;LX/3RF;)V

    .line 563662
    goto/16 :goto_1

    .line 563663
    :cond_23
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_REACTIONS:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 563664
    invoke-direct {p0, v2, v6}, Lcom/facebook/messaging/push/fbpushdata/OrcaFbPushDataHandler;->a(Ljava/lang/String;LX/0lF;)V

    goto/16 :goto_1

    .line 563665
    :cond_24
    const-string v7, "o"

    invoke-static {v6, v7}, Lcom/facebook/messaging/notify/PaymentNotification;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 563666
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/CharSequence;

    aput-object v7, v8, v3

    invoke-static {v8}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_25

    move-object v3, v4

    .line 563667
    goto/16 :goto_3

    .line 563668
    :cond_25
    new-instance v8, LX/DiE;

    invoke-direct {v8}, LX/DiE;-><init>()V

    .line 563669
    iput-object v7, v8, LX/DiE;->a:Ljava/lang/String;

    .line 563670
    move-object v7, v8

    .line 563671
    const-string v8, "oui"

    invoke-static {v6, v8}, Lcom/facebook/messaging/notify/PaymentNotification;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 563672
    iput-object v8, v7, LX/DiE;->c:Ljava/lang/String;

    .line 563673
    move-object v7, v7

    .line 563674
    const-string v8, "gti"

    invoke-static {v6, v8}, Lcom/facebook/messaging/notify/PaymentNotification;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 563675
    iput-object v8, v7, LX/DiE;->d:Ljava/lang/String;

    .line 563676
    move-object v7, v7

    .line 563677
    iput-object v1, v7, LX/DiE;->e:Ljava/lang/String;

    .line 563678
    move-object v7, v7

    .line 563679
    iput-object v2, v7, LX/DiE;->f:Ljava/lang/String;

    .line 563680
    move-object v7, v7

    .line 563681
    iput-object v2, v7, LX/DiE;->g:Ljava/lang/String;

    .line 563682
    move-object v7, v7

    .line 563683
    iput-object p2, v7, LX/DiE;->h:Lcom/facebook/push/PushProperty;

    .line 563684
    move-object v7, v7

    .line 563685
    const-string v8, "p2pr"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_27

    .line 563686
    const-string v4, "st"

    invoke-static {v6, v4}, Lcom/facebook/messaging/notify/PaymentNotification;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 563687
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_26

    const-string v5, "^\\d+$"

    invoke-virtual {v4, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_26

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 563688
    :cond_26
    sget-object v4, LX/DiF;->REQUEST:LX/DiF;

    .line 563689
    iput-object v4, v7, LX/DiE;->b:LX/DiF;

    .line 563690
    move-object v4, v7

    .line 563691
    iput v3, v4, LX/DiE;->i:I

    .line 563692
    move-object v3, v4

    .line 563693
    invoke-static {v3}, LX/DiE;->a$redex0(LX/DiE;)Lcom/facebook/messaging/notify/PaymentNotification;

    move-result-object v3

    goto/16 :goto_3

    .line 563694
    :cond_27
    const-string v3, "p2pt"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 563695
    sget-object v3, LX/DiF;->TRANSFER:LX/DiF;

    .line 563696
    iput-object v3, v7, LX/DiE;->b:LX/DiF;

    .line 563697
    move-object v3, v7

    .line 563698
    invoke-static {v3}, LX/DiE;->a$redex0(LX/DiE;)Lcom/facebook/messaging/notify/PaymentNotification;

    move-result-object v3

    goto/16 :goto_3

    :cond_28
    move-object v3, v4

    .line 563699
    goto/16 :goto_3

    .line 563700
    :cond_29
    if-eqz p1, :cond_1e

    const-string v2, "message"

    invoke-virtual {p1, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 563701
    const-string v2, "message"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 563702
    const-string v2, "params"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 563703
    if-eqz v2, :cond_1e

    const-string v4, "o"

    invoke-virtual {v2, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 563704
    const-string v4, "o"

    invoke-virtual {v2, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v4

    .line 563705
    iget-object v2, v1, LX/3QV;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Di5;

    new-instance v5, Lcom/facebook/messaging/notify/JoinRequestNotification;

    iget-object v6, v1, LX/3QV;->c:Landroid/content/res/Resources;

    invoke-static {v6}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v3, v0, v4}, Lcom/facebook/messaging/notify/JoinRequestNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;)V

    .line 563706
    new-instance v4, Landroid/content/Intent;

    sget-object v3, LX/2b2;->B:Ljava/lang/String;

    invoke-direct {v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 563707
    const-string v3, "notification"

    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563708
    iget-object v3, v2, LX/Di5;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bae;

    iget-object v6, v2, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v6}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 563709
    goto/16 :goto_4
.end method
