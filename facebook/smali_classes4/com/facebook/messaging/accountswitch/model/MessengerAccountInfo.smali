.class public Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfoDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final lastLogout:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_logout_timestamp"
    .end annotation
.end field

.field public final lastUnseenTimestamp:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_unseen_timestamp"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final unseenCountsAccessToken:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "unseen_count_access_token"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final userId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 549344
    const-class v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfoDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 549364
    new-instance v0, LX/3dg;

    invoke-direct {v0}, LX/3dg;-><init>()V

    sput-object v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 549357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549358
    iput-object v2, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    .line 549359
    iput-object v2, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    .line 549360
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastLogout:J

    .line 549361
    iput-object v2, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->unseenCountsAccessToken:Ljava/lang/String;

    .line 549362
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastUnseenTimestamp:J

    .line 549363
    return-void
.end method

.method public constructor <init>(LX/2ud;)V
    .locals 4

    .prologue
    .line 549345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549346
    iget-object v0, p1, LX/2ud;->a:Ljava/lang/String;

    move-object v0, v0

    .line 549347
    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    .line 549348
    iget-object v0, p1, LX/2ud;->b:Ljava/lang/String;

    move-object v0, v0

    .line 549349
    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    .line 549350
    iget-wide v2, p1, LX/2ud;->c:J

    move-wide v0, v2

    .line 549351
    iput-wide v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastLogout:J

    .line 549352
    iget-object v0, p1, LX/2ud;->d:Ljava/lang/String;

    move-object v0, v0

    .line 549353
    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->unseenCountsAccessToken:Ljava/lang/String;

    .line 549354
    iget-wide v2, p1, LX/2ud;->e:J

    move-wide v0, v2

    .line 549355
    iput-wide v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastUnseenTimestamp:J

    .line 549356
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 549365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549366
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    .line 549367
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    .line 549368
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastLogout:J

    .line 549369
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->unseenCountsAccessToken:Ljava/lang/String;

    .line 549370
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastUnseenTimestamp:J

    .line 549371
    return-void
.end method

.method public static a(LX/3dh;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;
    .locals 3

    .prologue
    .line 549320
    new-instance v0, LX/2ud;

    invoke-direct {v0}, LX/2ud;-><init>()V

    .line 549321
    iget-object v1, p0, LX/3dh;->a:Ljava/lang/String;

    iput-object v1, v0, LX/2ud;->a:Ljava/lang/String;

    .line 549322
    iget-object v1, p0, LX/3dh;->b:Ljava/lang/String;

    iput-object v1, v0, LX/2ud;->b:Ljava/lang/String;

    .line 549323
    const-wide/16 v1, -0x1

    iput-wide v1, v0, LX/2ud;->c:J

    .line 549324
    const/4 v1, 0x0

    iput-object v1, v0, LX/2ud;->d:Ljava/lang/String;

    .line 549325
    const-wide/16 v1, 0x0

    iput-wide v1, v0, LX/2ud;->e:J

    .line 549326
    move-object v0, v0

    .line 549327
    invoke-virtual {v0}, LX/2ud;->f()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/user/model/User;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;
    .locals 3

    .prologue
    .line 549328
    new-instance v0, LX/2ud;

    invoke-direct {v0}, LX/2ud;-><init>()V

    .line 549329
    iget-object v1, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 549330
    iput-object v1, v0, LX/2ud;->a:Ljava/lang/String;

    .line 549331
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/2ud;->b:Ljava/lang/String;

    .line 549332
    const-wide/16 v1, -0x1

    iput-wide v1, v0, LX/2ud;->c:J

    .line 549333
    const/4 v1, 0x0

    iput-object v1, v0, LX/2ud;->d:Ljava/lang/String;

    .line 549334
    const-wide/16 v1, 0x0

    iput-wide v1, v0, LX/2ud;->e:J

    .line 549335
    move-object v0, v0

    .line 549336
    invoke-virtual {v0}, LX/2ud;->f()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 549337
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 549338
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 549339
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 549340
    iget-wide v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastLogout:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 549341
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->unseenCountsAccessToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 549342
    iget-wide v0, p0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastUnseenTimestamp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 549343
    return-void
.end method
