.class public Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;
.super LX/3Ml;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:LX/3Mt;

.field private static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final f:Landroid/content/res/Resources;

.field private final g:LX/3Mu;

.field private final h:Z

.field private i:Z

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 556492
    new-instance v0, LX/3Mt;

    invoke-direct {v0}, LX/3Mt;-><init>()V

    sput-object v0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->c:LX/3Mt;

    .line 556493
    const-class v0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;

    const-string v1, "contact_picker_non_friend_filter"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 556494
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->e:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0Zr;Landroid/content/res/Resources;LX/3Mu;Ljava/lang/Boolean;)V
    .locals 1
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 556458
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 556459
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->j:Z

    .line 556460
    iput-object p2, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->f:Landroid/content/res/Resources;

    .line 556461
    iput-object p3, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->g:LX/3Mu;

    .line 556462
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->h:Z

    .line 556463
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;
    .locals 1

    .prologue
    .line 556491
    invoke-static {p0}, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->b(LX/0QB;)Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/0Px;Ljava/util/Set;LX/0Pz;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0Pz",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 556495
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 556496
    iget-object v3, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 556497
    invoke-interface {p2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 556498
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->m()Lcom/facebook/user/model/UserFbidIdentifier;

    move-result-object v3

    .line 556499
    if-eqz v3, :cond_1

    .line 556500
    iget-boolean v4, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->i:Z

    if-eqz v4, :cond_0

    .line 556501
    iget-boolean v4, v0, Lcom/facebook/user/model/User;->p:Z

    move v4, v4

    .line 556502
    if-nez v4, :cond_1

    .line 556503
    :cond_0
    invoke-virtual {p0, v3}, LX/3Ml;->a(Lcom/facebook/user/model/UserIdentifier;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 556504
    iget-object v3, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v3, v0}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v3

    invoke-virtual {p3, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 556505
    iget-object v3, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v3

    .line 556506
    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 556507
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 556508
    :cond_2
    return-void
.end method

.method private a(Ljava/lang/String;LX/0Pz;LX/0Pz;LX/0Pz;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Pz",
            "<",
            "LX/3OQ;",
            ">;",
            "LX/0Pz",
            "<",
            "LX/3OQ;",
            ">;",
            "LX/0Pz",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 556471
    :try_start_0
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 556472
    sget-object v0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->e:LX/0Rf;

    .line 556473
    new-instance v2, Lcom/facebook/messaging/service/model/SearchUserParams;

    invoke-direct {v2, p1, v0}, Lcom/facebook/messaging/service/model/SearchUserParams;-><init>(Ljava/lang/String;LX/0Rf;)V

    .line 556474
    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->g:LX/3Mu;

    .line 556475
    iget-object v3, v2, Lcom/facebook/messaging/service/model/SearchUserParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 556476
    new-instance v4, LX/IsU;

    invoke-direct {v4}, LX/IsU;-><init>()V

    .line 556477
    const-string v5, "search_query"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "entity_types"

    const-string v7, "user"

    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v5

    const-string v6, "results_limit"

    const/16 v7, 0x14

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 556478
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    move-object v3, v4

    .line 556479
    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 556480
    const-wide/16 v5, 0xe10

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    .line 556481
    iget-object v4, v0, LX/3Mu;->b:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 556482
    new-instance v4, LX/IsS;

    invoke-direct {v4, v0, v2}, LX/IsS;-><init>(LX/3Mu;Lcom/facebook/messaging/service/model/SearchUserParams;)V

    iget-object v5, v0, LX/3Mu;->a:LX/0TD;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 556483
    const v2, -0x34962f2

    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SearchUserResult;

    .line 556484
    iget-object v2, v0, Lcom/facebook/messaging/service/model/SearchUserResult;->b:LX/0Px;

    move-object v2, v2

    .line 556485
    invoke-static {p0, v2, v1, p3}, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->a(Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/0Px;Ljava/util/Set;LX/0Pz;)V

    .line 556486
    iget-object v2, v0, Lcom/facebook/messaging/service/model/SearchUserResult;->c:LX/0Px;

    move-object v2, v2

    .line 556487
    invoke-static {p0, v2, v1, p2}, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->a(Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/0Px;Ljava/util/Set;LX/0Pz;)V

    .line 556488
    iget-object v2, v0, Lcom/facebook/messaging/service/model/SearchUserResult;->d:LX/0Px;

    move-object v0, v2

    .line 556489
    invoke-static {p0, v0, v1, p4}, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->a(Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/0Px;Ljava/util/Set;LX/0Pz;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 556490
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;
    .locals 7

    .prologue
    .line 556464
    new-instance v4, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    .line 556465
    new-instance v6, LX/3Mu;

    invoke-direct {v6}, LX/3Mu;-><init>()V

    .line 556466
    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/3Mv;->a(LX/0QB;)LX/3Mv;

    move-result-object v5

    check-cast v5, LX/3Mv;

    .line 556467
    iput-object v2, v6, LX/3Mu;->a:LX/0TD;

    iput-object v3, v6, LX/3Mu;->b:LX/0tX;

    iput-object v5, v6, LX/3Mu;->c:LX/3Mv;

    .line 556468
    move-object v2, v6

    .line 556469
    check-cast v2, LX/3Mu;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;-><init>(LX/0Zr;Landroid/content/res/Resources;LX/3Mu;Ljava/lang/Boolean;)V

    .line 556470
    return-object v4
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 9
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 556423
    new-instance v2, LX/39y;

    invoke-direct {v2}, LX/39y;-><init>()V

    .line 556424
    const-string v1, "ContactPickerNonFriendUsersFilter.Filtering"

    const v3, -0x5bd1fa6e

    invoke-static {v1, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 556425
    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 556426
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    .line 556427
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 556428
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 556429
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 556430
    invoke-direct {p0, v1, v3, v4, v5}, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->a(Ljava/lang/String;LX/0Pz;LX/0Pz;LX/0Pz;)V

    .line 556431
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 556432
    iget-boolean v1, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->h:Z

    if-eqz v1, :cond_2

    .line 556433
    iget-object v1, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->f:Landroid/content/res/Resources;

    const v7, 0x7f0802d4

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 556434
    :goto_1
    new-instance v7, LX/DAQ;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    iget-boolean v8, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->j:Z

    if-eqz v8, :cond_3

    :goto_2
    invoke-direct {v7, v4, v1}, LX/DAQ;-><init>(LX/0Px;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 556435
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 556436
    new-instance v1, LX/DAQ;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    iget-boolean v4, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->j:Z

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->f:Landroid/content/res/Resources;

    const v4, 0x7f0802d3

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-direct {v1, v3, v0}, LX/DAQ;-><init>(LX/0Px;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 556437
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 556438
    new-instance v1, LX/3Og;

    sget-object v3, LX/3Oh;->OK:LX/3Oh;

    invoke-direct {v1, v3, p1, v0}, LX/3Og;-><init>(LX/3Oh;Ljava/lang/CharSequence;LX/0Px;)V

    move-object v0, v1

    .line 556439
    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    .line 556440
    iget v1, v0, LX/3Og;->d:I

    move v0, v1

    .line 556441
    iput v0, v2, LX/39y;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 556442
    :goto_3
    const v0, -0xccb310

    invoke-static {v0}, LX/02m;->a(I)V

    .line 556443
    const-string v0, "ContactPickerNonFriendUsersFilter"

    invoke-static {v0}, LX/0PR;->c(Ljava/lang/String;)V

    .line 556444
    :goto_4
    return-object v2

    .line 556445
    :cond_1
    :try_start_1
    const-string v1, ""

    goto :goto_0

    .line 556446
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->f:Landroid/content/res/Resources;

    const v7, 0x7f0802de

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    .line 556447
    goto :goto_2

    .line 556448
    :cond_4
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    .line 556449
    const/4 v0, -0x1

    iput v0, v2, LX/39y;->b:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 556450
    :catch_0
    move-exception v0

    .line 556451
    :try_start_2
    const-string v1, "ContactPickerNonFriendUsersFilter"

    const-string v3, "Exception during filtering"

    invoke-static {v1, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 556452
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v2, LX/39y;->a:Ljava/lang/Object;

    .line 556453
    const/4 v0, 0x0

    iput v0, v2, LX/39y;->b:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 556454
    const v0, -0x5d753ceb

    invoke-static {v0}, LX/02m;->a(I)V

    .line 556455
    const-string v0, "ContactPickerNonFriendUsersFilter"

    invoke-static {v0}, LX/0PR;->c(Ljava/lang/String;)V

    goto :goto_4

    .line 556456
    :catchall_0
    move-exception v0

    const v1, -0x25f6d3f2

    invoke-static {v1}, LX/02m;->a(I)V

    .line 556457
    const-string v1, "ContactPickerNonFriendUsersFilter"

    invoke-static {v1}, LX/0PR;->c(Ljava/lang/String;)V

    throw v0
.end method
