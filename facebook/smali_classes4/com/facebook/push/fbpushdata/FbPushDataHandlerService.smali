.class public Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;
.super LX/1ZN;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0Tn;

.field private static final d:LX/0Tn;


# instance fields
.field private e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/3Pw;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0lC;

.field private g:LX/2bC;

.field private h:LX/3Pt;

.field private i:LX/0SG;

.field private j:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private k:LX/3Pu;

.field private l:LX/2Gc;

.field private m:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 527369
    const-class v0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;

    sput-object v0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->a:Ljava/lang/Class;

    .line 527370
    invoke-static {}, LX/2Ch;->a()Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->b:Ljava/util/Set;

    .line 527371
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "mqtt/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 527372
    sput-object v0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->c:LX/0Tn;

    const-string v1, "push_channel"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 527366
    const-string v0, "PushDataHandlerService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 527367
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->setIntentRedelivery(Z)V

    .line 527368
    return-void
.end method

.method private a()LX/3B4;
    .locals 2

    .prologue
    .line 527355
    iget-object v0, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->l:LX/2Gc;

    invoke-virtual {v0}, LX/2Gc;->a()Ljava/util/Set;

    move-result-object v0

    .line 527356
    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 527357
    sget-object v0, LX/3B4;->FBNS_LITE:LX/3B4;

    .line 527358
    :goto_0
    return-object v0

    .line 527359
    :cond_0
    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 527360
    sget-object v0, LX/3B4;->MQTT_PUSH:LX/3B4;

    goto :goto_0

    .line 527361
    :cond_1
    sget-object v1, LX/2Ge;->ADM:LX/2Ge;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 527362
    sget-object v0, LX/3B4;->ADM:LX/3B4;

    goto :goto_0

    .line 527363
    :cond_2
    sget-object v1, LX/2Ge;->NNA:LX/2Ge;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 527364
    sget-object v0, LX/3B4;->NNA:LX/3B4;

    goto :goto_0

    .line 527365
    :cond_3
    sget-object v0, LX/3B4;->C2DM:LX/3B4;

    goto :goto_0
.end method

.method private static a(LX/3B4;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 527344
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 527345
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 527346
    sget-object v0, LX/3B4;->FBNS_LITE:LX/3B4;

    if-ne v0, p0, :cond_1

    .line 527347
    const/16 v0, 0x26a1

    .line 527348
    :goto_0
    const-string v3, "orca_message"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "msg"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    array-length v3, v2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 527349
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 527350
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\"$"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 527351
    :cond_1
    sget-object v0, LX/3B4;->MQTT_PUSH:LX/3B4;

    if-ne v0, p0, :cond_2

    .line 527352
    const/16 v0, 0x2744

    goto :goto_0

    .line 527353
    :cond_2
    const/16 v0, 0x2600

    goto :goto_0

    .line 527354
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;

    new-instance v1, LX/0U8;

    invoke-interface {v9}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/3Ps;

    invoke-direct {v3, v9}, LX/3Ps;-><init>(LX/0QB;)V

    invoke-direct {v1, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v1

    invoke-static {v9}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lC;

    invoke-static {v9}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v3

    check-cast v3, LX/2bC;

    invoke-static {v9}, LX/3Pt;->a(LX/0QB;)LX/3Pt;

    move-result-object v4

    check-cast v4, LX/3Pt;

    invoke-static {v9}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v9}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v9}, LX/3Pu;->a(LX/0QB;)LX/3Pu;

    move-result-object v7

    check-cast v7, LX/3Pu;

    invoke-static {v9}, LX/2Gc;->a(LX/0QB;)LX/2Gc;

    move-result-object v8

    check-cast v8, LX/2Gc;

    invoke-static {v9}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->a(Ljava/util/Set;LX/0lC;LX/2bC;LX/3Pt;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3Pu;LX/2Gc;LX/03V;)V

    return-void
.end method

.method private a(Ljava/util/Set;LX/0lC;LX/2bC;LX/3Pt;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3Pu;LX/2Gc;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/3Pw;",
            ">;",
            "LX/0lC;",
            "LX/2bC;",
            "LX/3Pt;",
            "LX/0SG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/3Pu;",
            "LX/2Gc;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527334
    iput-object p1, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->e:Ljava/util/Set;

    .line 527335
    iput-object p2, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->f:LX/0lC;

    .line 527336
    iput-object p3, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->g:LX/2bC;

    .line 527337
    iput-object p4, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->h:LX/3Pt;

    .line 527338
    iput-object p5, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->i:LX/0SG;

    .line 527339
    iput-object p6, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 527340
    iput-object p7, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->k:LX/3Pu;

    .line 527341
    iput-object p8, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->l:LX/2Gc;

    .line 527342
    iput-object p9, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->m:LX/03V;

    .line 527343
    return-void
.end method

.method public static a(Lcom/facebook/notifications/constants/NotificationType;)Z
    .locals 1

    .prologue
    .line 527333
    sget-object v0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;LX/3Pw;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 527325
    invoke-interface {p1}, LX/3Pw;->a()LX/2QP;

    move-result-object v0

    .line 527326
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 527327
    invoke-virtual {v0}, LX/2QP;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 527328
    :goto_0
    return v0

    .line 527329
    :cond_0
    invoke-virtual {v0}, LX/2QP;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/constants/NotificationType;

    .line 527330
    invoke-virtual {v0, p0}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 527331
    goto :goto_0

    .line 527332
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 527281
    if-nez p1, :cond_1

    .line 527282
    iget-object v0, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->g:LX/2bC;

    invoke-virtual {v0}, LX/2bC;->a()V

    .line 527283
    :cond_0
    :goto_0
    return-void

    .line 527284
    :cond_1
    const-string v0, "push_content"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 527285
    const-string v1, "push_source"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/3B4;->valueOf(Ljava/lang/String;)LX/3B4;

    move-result-object v8

    .line 527286
    iget-object v1, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->k:LX/3Pu;

    invoke-virtual {v8}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v2

    .line 527287
    iget-object v3, v1, LX/3Pu;->a:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move v3, v3

    .line 527288
    if-nez v3, :cond_a

    .line 527289
    :goto_1
    :try_start_0
    iget-object v1, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->f:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 527290
    :goto_2
    if-eqz v1, :cond_0

    sget-object v0, LX/2FN;->a:LX/2FN;

    if-eq v1, v0, :cond_0

    .line 527291
    const-string v0, "params"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 527292
    if-eqz v0, :cond_2

    .line 527293
    const-string v2, "PushNotifID"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 527294
    if-eqz v0, :cond_2

    .line 527295
    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v4

    .line 527296
    :cond_2
    const-string v0, "type"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 527297
    const-string v0, ""

    .line 527298
    if-eqz v2, :cond_9

    .line 527299
    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 527300
    :goto_3
    iget-object v0, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->d:LX/0Tn;

    invoke-interface {v0, v3, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 527301
    if-eqz v0, :cond_3

    const-string v0, "message"

    invoke-virtual {v1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 527302
    const-string v0, "message"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    .line 527303
    invoke-static {v8, v0, v2}, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->a(LX/3B4;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, v1

    .line 527304
    check-cast v0, LX/0m9;

    const-string v5, "message"

    invoke-virtual {v0, v5, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 527305
    :cond_3
    const-string v0, "time"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 527306
    if-eqz v0, :cond_8

    .line 527307
    invoke-virtual {v0}, LX/0lF;->w()I

    move-result v0

    .line 527308
    :goto_4
    if-eqz v4, :cond_5

    .line 527309
    iget-object v3, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->h:LX/3Pt;

    invoke-virtual {v3, v4}, LX/3Pt;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 527310
    iget-object v1, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->g:LX/2bC;

    invoke-virtual {v8}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4, v0}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 527311
    :catch_0
    iget-object v0, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->g:LX/2bC;

    invoke-virtual {v8}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    const-string v3, "invalid_json"

    const-string v5, ""

    const-string v6, ""

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v4

    goto/16 :goto_2

    .line 527312
    :cond_4
    iget-object v3, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->h:LX/3Pt;

    invoke-virtual {v3, v4}, LX/3Pt;->a(Ljava/lang/String;)V

    .line 527313
    :cond_5
    iget-object v3, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->g:LX/2bC;

    invoke-virtual {v8}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5, v4, v2, v0}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 527314
    invoke-direct {p0}, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->a()LX/3B4;

    move-result-object v3

    .line 527315
    if-eq v8, v3, :cond_6

    .line 527316
    iget-object v5, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->g:LX/2bC;

    invoke-virtual {v8}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v6, v3, v4, v0}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 527317
    :cond_6
    new-instance v3, Lcom/facebook/push/PushProperty;

    iget-object v0, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    invoke-direct {v3, v8, v4, v6, v7}, Lcom/facebook/push/PushProperty;-><init>(LX/3B4;Ljava/lang/String;J)V

    .line 527318
    iget-object v0, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Pw;

    .line 527319
    invoke-static {v2, v0}, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->a(Ljava/lang/String;LX/3Pw;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 527320
    :try_start_1
    invoke-interface {v0, v1, v3}, LX/3Pw;->a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    .line 527321
    :catch_1
    move-exception v6

    .line 527322
    iget-object v7, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->m:LX/03V;

    const-string v9, "FbPushDataHandler.onNotification exception"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v9, v10, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 527323
    iget-object v7, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->g:LX/2bC;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v0, v9, v4, v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_5

    :cond_8
    move v0, v7

    goto/16 :goto_4

    :cond_9
    move-object v2, v0

    goto/16 :goto_3

    .line 527324
    :cond_a
    iget-object v3, v1, LX/3Pu;->c:LX/0ZL;

    new-instance v5, LX/Ceu;

    invoke-direct {v5, v1, v2, v0}, LX/Ceu;-><init>(LX/3Pu;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, LX/0ZL;->a(Ljava/lang/Object;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x72520a48

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 527273
    :try_start_0
    invoke-direct {p0, p1}, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->b(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527274
    if-eqz p1, :cond_0

    .line 527275
    const-string v1, "extra_notification_sender"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "extra_notification_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2}, LX/05I;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 527276
    invoke-static {p1}, LX/37V;->a(Landroid/content/Intent;)Z

    .line 527277
    :cond_0
    const v1, -0x3897d120    # -59438.875f

    invoke-static {v1, v0}, LX/02F;->d(II)V

    return-void

    .line 527278
    :catchall_0
    move-exception v1

    if-eqz p1, :cond_1

    .line 527279
    const-string v2, "extra_notification_sender"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "extra_notification_id"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v2, v3}, LX/05I;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 527280
    invoke-static {p1}, LX/37V;->a(Landroid/content/Intent;)Z

    :cond_1
    const v2, -0x7445ec74

    invoke-static {v2, v0}, LX/02F;->d(II)V

    throw v1
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, -0x7d9daa9b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 527263
    const-string v0, "%s.onCreate"

    const-class v2, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const v3, -0x684bbeb4

    invoke-static {v0, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 527264
    :try_start_0
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 527265
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 527266
    invoke-static {p0, p0}, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 527267
    sget-object v0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 527268
    iget-object v0, p0, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Pw;

    .line 527269
    invoke-interface {v0}, LX/3Pw;->a()LX/2QP;

    move-result-object v3

    invoke-static {v3}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 527270
    sget-object v3, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;->b:Ljava/util/Set;

    invoke-interface {v0}, LX/3Pw;->a()LX/2QP;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 527271
    :catchall_0
    move-exception v0

    const v2, 0x7000c32f

    invoke-static {v2}, LX/02m;->a(I)V

    const v2, 0xb827492

    invoke-static {v2, v1}, LX/02F;->d(II)V

    throw v0

    :cond_1
    const v0, -0x6316d3ea

    invoke-static {v0}, LX/02m;->a(I)V

    .line 527272
    const v0, -0x3665a241

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void
.end method
