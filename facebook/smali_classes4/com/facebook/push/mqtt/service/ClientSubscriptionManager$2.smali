.class public final Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:LX/1fU;


# direct methods
.method public constructor <init>(LX/1fU;LX/0Px;LX/0Px;)V
    .locals 0

    .prologue
    .line 465573
    iput-object p1, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->c:LX/1fU;

    iput-object p2, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->a:LX/0Px;

    iput-object p3, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->b:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 465574
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->c:LX/1fU;

    iget-object v0, v0, LX/1fU;->b:LX/1fY;

    iget-object v1, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->a:LX/0Px;

    iget-object v2, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->b:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/1fZ;->a(Ljava/util/List;Ljava/util/List;)V

    .line 465575
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->c:LX/1fU;

    iget-object v0, v0, LX/1fU;->b:LX/1fY;

    iget-object v1, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->c:LX/1fU;

    iget-object v1, v1, LX/1fU;->d:LX/0Uh;

    const/16 v2, 0x203

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    iget-object v2, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->c:LX/1fU;

    iget-boolean v2, v2, LX/1fU;->h:Z

    iget-object v3, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->c:LX/1fU;

    iget-object v3, v3, LX/1fU;->g:LX/1tH;

    iget-object v4, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->a:LX/0Px;

    iget-object v5, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$2;->b:LX/0Px;

    .line 465576
    if-eqz v3, :cond_4

    .line 465577
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v4}, LX/0Ph;->c(Ljava/lang/Iterable;)Ljava/lang/String;

    invoke-static {v5}, LX/0Ph;->c(Ljava/lang/Iterable;)Ljava/lang/String;

    .line 465578
    if-eqz v1, :cond_2

    .line 465579
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_1

    .line 465580
    :cond_0
    invoke-virtual {v0, v2, v3, v4, v5}, LX/1fZ;->a(ZLX/1tH;Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 465581
    :cond_1
    :goto_0
    return-void

    .line 465582
    :cond_2
    :try_start_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_3

    .line 465583
    invoke-virtual {v0, v3, v4}, LX/1fZ;->a(LX/1tH;Ljava/util/List;)V

    .line 465584
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_1

    .line 465585
    invoke-virtual {v0, v3, v5}, LX/1fZ;->b(LX/1tH;Ljava/util/List;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 465586
    :catch_0
    goto :goto_0

    .line 465587
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_1

    .line 465588
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v4}, LX/0Ph;->c(Ljava/lang/Iterable;)Ljava/lang/String;

    goto :goto_0
.end method
