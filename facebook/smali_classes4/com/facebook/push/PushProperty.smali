.class public Lcom/facebook/push/PushProperty;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/push/PushProperty;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/3B4;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 566021
    new-instance v0, LX/3CF;

    invoke-direct {v0}, LX/3CF;-><init>()V

    sput-object v0, Lcom/facebook/push/PushProperty;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/3B4;)V
    .locals 4

    .prologue
    .line 566022
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/facebook/push/PushProperty;-><init>(LX/3B4;Ljava/lang/String;J)V

    .line 566023
    return-void
.end method

.method public constructor <init>(LX/3B4;Ljava/lang/String;J)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 566024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566025
    iput-object p1, p0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    .line 566026
    iput-object p2, p0, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    .line 566027
    iput-wide p3, p0, Lcom/facebook/push/PushProperty;->c:J

    .line 566028
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 566029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566030
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/3B4;

    iput-object v0, p0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    .line 566031
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    .line 566032
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/push/PushProperty;->c:J

    .line 566033
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 566034
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 566035
    iget-object v0, p0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 566036
    iget-object v0, p0, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566037
    iget-wide v0, p0, Lcom/facebook/push/PushProperty;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 566038
    return-void
.end method
