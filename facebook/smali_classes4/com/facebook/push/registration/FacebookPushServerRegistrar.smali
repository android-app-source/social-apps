.class public Lcom/facebook/push/registration/FacebookPushServerRegistrar;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile x:Lcom/facebook/push/registration/FacebookPushServerRegistrar;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0aG;

.field private final d:LX/11H;

.field private final e:LX/2Gt;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0dC;

.field private final h:LX/0kb;

.field private final i:LX/2Gq;

.field private final j:LX/2Gs;

.field private final k:LX/12x;

.field private final l:LX/2Gu;

.field private final m:LX/0SG;

.field public final n:LX/01T;

.field public final o:LX/00H;

.field private final p:LX/0TD;

.field private final q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final r:I

.field private final s:I

.field private final t:LX/0Uh;

.field public final u:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final v:LX/2Gr;

.field private final w:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 527259
    const-class v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sput-object v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0aG;LX/11H;LX/2Gt;LX/0Or;LX/0dC;LX/0kb;LX/2Gq;LX/2Gs;LX/12x;LX/2Gu;LX/0SG;LX/01T;LX/00H;LX/0TD;LX/0Or;LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gr;LX/0ad;)V
    .locals 4
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p15    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p16    # LX/0Or;
        .annotation runtime Lcom/facebook/push/externalcloud/annotations/IsPreRegPushTokenRegistrationEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0aG;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/2Gt;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0kb;",
            "LX/2Gq;",
            "LX/2Gs;",
            "Lcom/facebook/common/alarm/FbAlarmManager;",
            "LX/2Gu;",
            "LX/0SG;",
            "LX/01T;",
            "LX/00H;",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2Gr;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527233
    iput-object p1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b:Landroid/content/Context;

    .line 527234
    iput-object p2, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->c:LX/0aG;

    .line 527235
    iput-object p3, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->d:LX/11H;

    .line 527236
    iput-object p4, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->e:LX/2Gt;

    .line 527237
    iput-object p5, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->f:LX/0Or;

    .line 527238
    iput-object p6, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->g:LX/0dC;

    .line 527239
    iput-object p7, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->h:LX/0kb;

    .line 527240
    iput-object p8, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    .line 527241
    iput-object p9, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->j:LX/2Gs;

    .line 527242
    iput-object p10, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->k:LX/12x;

    .line 527243
    iput-object p11, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->l:LX/2Gu;

    .line 527244
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->m:LX/0SG;

    .line 527245
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->n:LX/01T;

    .line 527246
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->o:LX/00H;

    .line 527247
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->p:LX/0TD;

    .line 527248
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->q:LX/0Or;

    .line 527249
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->t:LX/0Uh;

    .line 527250
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 527251
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->v:LX/2Gr;

    .line 527252
    iget-object v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->t:LX/0Uh;

    const/16 v2, 0x258

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 527253
    const v1, 0x7f0d01f2

    iput v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->r:I

    .line 527254
    const v1, 0x7f0d01f3

    iput v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->s:I

    .line 527255
    :goto_0
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->w:LX/0ad;

    .line 527256
    return-void

    .line 527257
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->r:I

    .line 527258
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->s:I

    goto :goto_0
.end method

.method private a(LX/2Ge;)LX/CfA;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 527214
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    invoke-virtual {v0, p1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v7

    .line 527215
    invoke-virtual {v7}, LX/2H0;->k()Z

    move-result v0

    .line 527216
    if-nez v0, :cond_0

    .line 527217
    sget-object v0, LX/CfA;->NONE:LX/CfA;

    .line 527218
    :goto_0
    return-object v0

    .line 527219
    :cond_0
    invoke-virtual {v7}, LX/2H0;->i()Ljava/lang/String;

    move-result-object v2

    .line 527220
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 527221
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 527222
    :goto_1
    if-nez v1, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 527223
    sget-object v0, LX/CfA;->WRONG_USER:LX/CfA;

    goto :goto_0

    :cond_1
    move v1, v6

    .line 527224
    goto :goto_1

    .line 527225
    :cond_2
    invoke-virtual {v7}, LX/2H0;->o()J

    move-result-wide v8

    .line 527226
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->w:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-wide v2, LX/Cei;->a:J

    const-wide/16 v4, 0x18

    invoke-interface/range {v0 .. v5}, LX/0ad;->a(LX/0c0;JJ)J

    move-result-wide v0

    .line 527227
    iget-object v2, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->m:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, v8

    const-wide/32 v4, 0x36ee80

    mul-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_3

    .line 527228
    sget-object v0, LX/CfA;->EXPIRED:LX/CfA;

    goto :goto_0

    .line 527229
    :cond_3
    invoke-virtual {v7}, LX/2H0;->b()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->t:LX/0Uh;

    const/16 v1, 0x256

    invoke-virtual {v0, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v7}, LX/2H0;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 527230
    :cond_4
    sget-object v0, LX/CfA;->EXPIRED:LX/CfA;

    goto :goto_0

    .line 527231
    :cond_5
    sget-object v0, LX/CfA;->CURRENT:LX/CfA;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/push/registration/FacebookPushServerRegistrar;
    .locals 3

    .prologue
    .line 527204
    sget-object v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->x:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    if-nez v0, :cond_1

    .line 527205
    const-class v1, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    monitor-enter v1

    .line 527206
    :try_start_0
    sget-object v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->x:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 527207
    if-eqz v2, :cond_0

    .line 527208
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b(LX/0QB;)Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    move-result-object v0

    sput-object v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->x:Lcom/facebook/push/registration/FacebookPushServerRegistrar;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527209
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 527210
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 527211
    :cond_1
    sget-object v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->x:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    return-object v0

    .line 527212
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 527213
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x5f

    .line 527192
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v0, LX/2gP;->FAILED:LX/2gP;

    invoke-virtual {v0}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 527193
    instance-of v0, p0, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_1

    .line 527194
    check-cast p0, Lcom/facebook/fbservice/service/ServiceException;

    .line 527195
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 527196
    iget-object v2, p0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v2, v2

    .line 527197
    invoke-virtual {v2}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 527198
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 527199
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 527200
    if-eqz v0, :cond_0

    .line 527201
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 527202
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 527203
    :cond_1
    const-string v0, "_"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private a(LX/2Ge;LX/2H0;LX/2H7;)V
    .locals 11

    .prologue
    .line 527167
    invoke-virtual {p2}, LX/2H0;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2}, LX/2H0;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_0
    const/4 v5, 0x1

    .line 527168
    :goto_0
    new-instance v1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;

    .line 527169
    iget-object v0, p2, LX/2H0;->e:LX/2Ge;

    move-object v2, v0

    .line 527170
    invoke-virtual {p2}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->g:LX/0dC;

    invoke-virtual {v0}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    .line 527171
    iget-object v0, p2, LX/2H0;->f:LX/0WV;

    invoke-virtual {v0}, LX/0WV;->b()I

    move-result v0

    move v6, v0

    .line 527172
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->l:LX/2Gu;

    .line 527173
    const/4 v7, 0x0

    .line 527174
    const-string v8, "true"

    iget-object v9, v0, LX/2Gu;->a:LX/04P;

    const-string v10, "com.facebook.vi"

    invoke-virtual {v9, v10}, LX/04P;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 527175
    const/4 v7, 0x2

    .line 527176
    :cond_1
    iget-object v8, v0, LX/2Gu;->b:LX/2zj;

    if-eqz v8, :cond_5

    .line 527177
    iget-object v8, v0, LX/2Gu;->b:LX/2zj;

    invoke-interface {v8}, LX/2zj;->e()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 527178
    or-int/lit8 v7, v7, 0x10

    .line 527179
    :cond_2
    iget-object v8, v0, LX/2Gu;->b:LX/2zj;

    invoke-interface {v8}, LX/2zj;->d()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 527180
    or-int/lit8 v7, v7, 0x20

    .line 527181
    :cond_3
    iget-object v8, v0, LX/2Gu;->b:LX/2zj;

    invoke-interface {v8}, LX/2zj;->f()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 527182
    or-int/lit8 v7, v7, 0x40

    .line 527183
    :cond_4
    iget-object v8, v0, LX/2Gu;->b:LX/2zj;

    invoke-interface {v8}, LX/2zj;->a()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 527184
    or-int/lit16 v7, v7, 0x80

    .line 527185
    :cond_5
    move v7, v7

    .line 527186
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->l:LX/2Gu;

    invoke-virtual {v0}, LX/2Gu;->b()J

    move-result-wide v8

    invoke-virtual {p2}, LX/2H0;->g()Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v1 .. v10}, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;-><init>(LX/2Ge;Ljava/lang/String;Ljava/lang/String;ZIIJLjava/lang/String;)V

    .line 527187
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 527188
    const-string v2, "registerPushTokenParams"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 527189
    const-string v1, "register_push"

    invoke-static {p0, v1, v0, p1, p3}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(Lcom/facebook/push/registration/FacebookPushServerRegistrar;Ljava/lang/String;Landroid/os/Bundle;LX/2Ge;LX/2H7;)V

    .line 527190
    return-void

    .line 527191
    :cond_6
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private a(LX/2Ge;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 527159
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->j:LX/2Gs;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    invoke-virtual {v2, p1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v2

    invoke-virtual {v2}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b()Z

    move-result v4

    .line 527160
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 527161
    const-string p0, "service_type"

    invoke-interface {v5, p0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527162
    if-eqz v4, :cond_0

    .line 527163
    const-string p0, "pre_log"

    const-string p1, "true"

    invoke-interface {v5, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527164
    :cond_0
    const-string p0, "push_reg_server"

    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "registration_id"

    invoke-static {p0, p1, v5, p2, v2}, LX/2gN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 527165
    invoke-static {v0, v5}, LX/2Gs;->a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 527166
    return-void
.end method

.method public static a(Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Ge;J)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 527131
    invoke-direct {p0, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b(LX/2Ge;)I

    move-result v0

    .line 527132
    if-eqz v0, :cond_0

    .line 527133
    iget-object v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/45m;->a(Landroid/content/Context;)LX/45m;

    move-result-object v1

    .line 527134
    if-eqz v1, :cond_0

    .line 527135
    invoke-virtual {v1, v0}, LX/45m;->a(I)V

    .line 527136
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 527137
    new-instance v2, LX/46T;

    invoke-direct {v2}, LX/46T;-><init>()V

    .line 527138
    :goto_0
    move-object v2, v2

    .line 527139
    const-string v3, "serviceType"

    invoke-virtual {p1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/46S;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 527140
    new-instance v3, LX/45t;

    invoke-direct {v3, v0}, LX/45t;-><init>(I)V

    .line 527141
    iput-wide p2, v3, LX/45t;->d:J

    .line 527142
    move-object v0, v3

    .line 527143
    const-wide/16 v4, 0x2

    div-long v4, p2, v4

    add-long/2addr v4, p2

    .line 527144
    iput-wide v4, v0, LX/45t;->e:J

    .line 527145
    move-object v0, v0

    .line 527146
    iput v6, v0, LX/45t;->b:I

    .line 527147
    move-object v0, v0

    .line 527148
    iput-object v2, v0, LX/45t;->g:LX/46S;

    .line 527149
    move-object v0, v0

    .line 527150
    invoke-virtual {v0}, LX/45t;->a()LX/45u;

    move-result-object v0

    .line 527151
    invoke-virtual {v1, v0}, LX/45m;->a(LX/45u;)V

    .line 527152
    :goto_1
    return-void

    .line 527153
    :cond_0
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->m:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    add-long/2addr v0, p2

    .line 527154
    :try_start_0
    iget-object v2, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->k:LX/12x;

    const/4 v3, 0x1

    invoke-direct {p0, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->f(LX/2Ge;)Landroid/app/PendingIntent;

    move-result-object v4

    .line 527155
    invoke-virtual {v2, v3, v0, v1, v4}, LX/12x;->c(IJLandroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 527156
    goto :goto_1

    .line 527157
    :catch_0
    move-exception v0

    .line 527158
    sget-object v1, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a:Ljava/lang/Class;

    const-string v2, "Exception while setting an alarm"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    new-instance v2, LX/46V;

    invoke-direct {v2}, LX/46V;-><init>()V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/push/registration/FacebookPushServerRegistrar;Ljava/lang/String;Landroid/os/Bundle;LX/2Ge;LX/2H7;)V
    .locals 5

    .prologue
    .line 527125
    sget-object v0, LX/2gP;->ATTEMPT:LX/2gP;

    invoke-direct {p0, p3, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/Object;)V

    .line 527126
    const-wide/32 v3, 0xa4cb80

    invoke-static {p0, p3, v3, v4}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Ge;J)V

    .line 527127
    const-wide/16 v3, 0x7530

    invoke-static {p0, p3, v3, v4}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b(Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Ge;J)V

    .line 527128
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->c:LX/0aG;

    const v1, 0x4b863457    # 1.7590446E7f

    invoke-static {v0, p1, p2, v1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 527129
    new-instance v1, LX/2XK;

    invoke-direct {v1, p0, p1, p3, p4}, LX/2XK;-><init>(Lcom/facebook/push/registration/FacebookPushServerRegistrar;Ljava/lang/String;LX/2Ge;LX/2H7;)V

    iget-object v2, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->p:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 527130
    return-void
.end method

.method private b(LX/2Ge;)I
    .locals 1

    .prologue
    .line 527260
    sget-object v0, LX/2Ge;->FBNS:LX/2Ge;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/2Ge;->FBNS_LITE:LX/2Ge;

    if-ne p1, v0, :cond_1

    .line 527261
    :cond_0
    iget v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->s:I

    .line 527262
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->r:I

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/push/registration/FacebookPushServerRegistrar;
    .locals 23

    .prologue
    .line 527123
    new-instance v2, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v5

    check-cast v5, LX/11H;

    invoke-static/range {p0 .. p0}, LX/2Gt;->a(LX/0QB;)LX/2Gt;

    move-result-object v6

    check-cast v6, LX/2Gt;

    const/16 v7, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/0dB;->a(LX/0QB;)LX/0dC;

    move-result-object v8

    check-cast v8, LX/0dC;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v9

    check-cast v9, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/2Gq;->a(LX/0QB;)LX/2Gq;

    move-result-object v10

    check-cast v10, LX/2Gq;

    invoke-static/range {p0 .. p0}, LX/2Gs;->a(LX/0QB;)LX/2Gs;

    move-result-object v11

    check-cast v11, LX/2Gs;

    invoke-static/range {p0 .. p0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v12

    check-cast v12, LX/12x;

    invoke-static/range {p0 .. p0}, LX/2Gu;->a(LX/0QB;)LX/2Gu;

    move-result-object v13

    check-cast v13, LX/2Gu;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v14

    check-cast v14, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v15

    check-cast v15, LX/01T;

    const-class v16, LX/00H;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, LX/00H;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v17

    check-cast v17, LX/0TD;

    const/16 v18, 0x354

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v19

    check-cast v19, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v20

    check-cast v20, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/2Gr;->a(LX/0QB;)LX/2Gr;

    move-result-object v21

    check-cast v21, LX/2Gr;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v22

    check-cast v22, LX/0ad;

    invoke-direct/range {v2 .. v22}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;-><init>(Landroid/content/Context;LX/0aG;LX/11H;LX/2Gt;LX/0Or;LX/0dC;LX/0kb;LX/2Gq;LX/2Gs;LX/12x;LX/2Gu;LX/0SG;LX/01T;LX/00H;LX/0TD;LX/0Or;LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gr;LX/0ad;)V

    .line 527124
    return-object v2
.end method

.method public static b(Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Ge;J)V
    .locals 4

    .prologue
    const-wide/32 v0, 0x1b7740

    .line 527116
    cmp-long v2, p2, v0

    if-lez v2, :cond_0

    move-wide p2, v0

    .line 527117
    :cond_0
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->v:LX/2Gr;

    invoke-virtual {v0, p1}, LX/2Gr;->a(LX/2Ge;)LX/2H3;

    move-result-object v0

    .line 527118
    iget-object v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 527119
    iget-object v2, v0, LX/2H3;->f:LX/0Tn;

    move-object v0, v2

    .line 527120
    invoke-interface {v1, v0, p2, p3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 527121
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 527122
    return-void
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 527115
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private d(LX/2Ge;)V
    .locals 2

    .prologue
    .line 527108
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->k:LX/12x;

    invoke-direct {p0, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->f(LX/2Ge;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 527109
    invoke-direct {p0, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b(LX/2Ge;)I

    move-result v0

    .line 527110
    if-eqz v0, :cond_0

    .line 527111
    iget-object v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/45m;->a(Landroid/content/Context;)LX/45m;

    move-result-object v1

    .line 527112
    if-eqz v1, :cond_0

    .line 527113
    invoke-virtual {v1, v0}, LX/45m;->a(I)V

    .line 527114
    :cond_0
    return-void
.end method

.method private f(LX/2Ge;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 527104
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/push/registration/FacebookPushServerRegistrar$LocalBroadcastReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 527105
    const-string v1, "com.facebook.push.registration.ACTION_ALARM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 527106
    const-string v1, "serviceType"

    invoke-virtual {p1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 527107
    iget-object v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b:Landroid/content/Context;

    const/4 v2, -0x1

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/2Ge;LX/2H7;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 527085
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 527086
    invoke-virtual {v0, v4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    .line 527087
    invoke-direct {p0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b()Z

    move-result v1

    .line 527088
    if-eqz v1, :cond_2

    .line 527089
    sget-object v2, LX/01T;->FB4A:LX/01T;

    iget-object v3, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->n:LX/01T;

    if-eq v2, v3, :cond_0

    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    iget-object v3, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->n:LX/01T;

    if-ne v2, v3, :cond_5

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 527090
    if-eqz v2, :cond_1

    if-nez v0, :cond_2

    .line 527091
    :cond_1
    sget-object v0, LX/2gP;->AUTH_NO_USER:LX/2gP;

    invoke-direct {p0, p1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/Object;)V

    .line 527092
    :goto_1
    return-void

    .line 527093
    :cond_2
    iget-object v2, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    invoke-virtual {v2, p1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v2

    .line 527094
    invoke-virtual {v2}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 527095
    sget-object v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a:Ljava/lang/Class;

    const-string v1, "Registration id is empty for %s, should not register with facebook"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 527096
    sget-object v0, LX/2gP;->NO_TOKEN:LX/2gP;

    invoke-direct {p0, p1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/Object;)V

    goto :goto_1

    .line 527097
    :cond_3
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 527098
    new-instance v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;

    invoke-virtual {v2}, LX/2H0;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p2, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->o:LX/00H;

    invoke-virtual {p2}, LX/00H;->c()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p2, "|"

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object p2, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->o:LX/00H;

    invoke-virtual {p2}, LX/00H;->e()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object p2, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->g:LX/0dC;

    invoke-virtual {p2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, v1, v3, v4, p2}, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 527099
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 527100
    const-string v3, "registerPushTokenNoUserParams"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 527101
    const-string v0, "register_push_no_user"

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, p1, v3}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(Lcom/facebook/push/registration/FacebookPushServerRegistrar;Ljava/lang/String;Landroid/os/Bundle;LX/2Ge;LX/2H7;)V

    .line 527102
    goto :goto_1

    .line 527103
    :cond_4
    invoke-direct {p0, p1, v2, p2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H0;LX/2H7;)V

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/2Ge;Lcom/facebook/fbservice/service/OperationResult;LX/2H7;)V
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 527055
    invoke-direct {p0, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->d(LX/2Ge;)V

    .line 527056
    invoke-virtual {p2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;

    .line 527057
    iget-boolean v1, v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;->a:Z

    move v1, v1

    .line 527058
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 527059
    iget-boolean v1, v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;->a:Z

    move v1, v1

    .line 527060
    if-nez v1, :cond_1

    .line 527061
    sget-object v0, LX/2gP;->SERVER_FAILED:LX/2gP;

    invoke-direct {p0, p1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/Object;)V

    .line 527062
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    invoke-virtual {v0, p1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    invoke-virtual {v0}, LX/2H0;->n()V

    .line 527063
    :cond_0
    :goto_0
    return-void

    .line 527064
    :cond_1
    if-eqz p3, :cond_2

    .line 527065
    iget-boolean v1, v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;->b:Z

    move v0, v1

    .line 527066
    if-eqz v0, :cond_2

    .line 527067
    sget-object v0, LX/2gP;->INVALID_TOKEN:LX/2gP;

    invoke-direct {p0, p1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/Object;)V

    .line 527068
    invoke-interface {p3}, LX/2H7;->a()V

    goto :goto_0

    .line 527069
    :cond_2
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    invoke-virtual {v0, p1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    .line 527070
    iget-object v2, v0, LX/2H0;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 527071
    if-nez v2, :cond_3

    .line 527072
    const-string v2, ""

    .line 527073
    :cond_3
    invoke-virtual {v0}, LX/2H0;->a()Ljava/lang/String;

    .line 527074
    iget-object v3, v0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    iget-object v4, v0, LX/2H0;->g:LX/2H3;

    .line 527075
    iget-object v5, v4, LX/2H3;->h:LX/0Tn;

    move-object v4, v5

    .line 527076
    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    iget-object v4, v0, LX/2H0;->g:LX/2H3;

    .line 527077
    iget-object v5, v4, LX/2H3;->i:LX/0Tn;

    move-object v4, v5

    .line 527078
    iget-object v5, v0, LX/2H0;->h:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v3, v4, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    iget-object v4, v0, LX/2H0;->g:LX/2H3;

    .line 527079
    iget-object v5, v4, LX/2H3;->b:LX/0Tn;

    move-object v4, v5

    .line 527080
    invoke-interface {v3, v4, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 527081
    sget-object v0, LX/2gP;->SUCCESS:LX/2gP;

    invoke-direct {p0, p1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/Object;)V

    .line 527082
    sget-object v0, LX/2Ge;->GCM:LX/2Ge;

    invoke-virtual {v0, p1}, LX/2Ge;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527083
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    invoke-virtual {v0}, LX/2H0;->n()V

    .line 527084
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    invoke-virtual {v0}, LX/2H0;->n()V

    goto :goto_0
.end method

.method public final a(LX/2Ge;Ljava/lang/Throwable;)V
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 527036
    invoke-direct {p0, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->d(LX/2Ge;)V

    .line 527037
    invoke-static {p2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/Object;)V

    .line 527038
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    invoke-virtual {v0, p1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    invoke-virtual {v0}, LX/2H0;->n()V

    .line 527039
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->t:LX/0Uh;

    const/16 v1, 0x259

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 527040
    instance-of v0, p2, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_3

    .line 527041
    check-cast p2, Lcom/facebook/fbservice/service/ServiceException;

    .line 527042
    iget-object v0, p2, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 527043
    :goto_0
    move-object v0, v0

    .line 527044
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/1nY;->HTTP_500_CLASS:LX/1nY;

    if-ne v0, v1, :cond_2

    .line 527045
    :cond_0
    const/4 v0, 0x1

    .line 527046
    :goto_1
    move v0, v0

    .line 527047
    if-eqz v0, :cond_1

    .line 527048
    iget-object v4, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->v:LX/2Gr;

    invoke-virtual {v4, p1}, LX/2Gr;->a(LX/2Ge;)LX/2H3;

    move-result-object v4

    .line 527049
    iget-object v5, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 527050
    iget-object v6, v4, LX/2H3;->f:LX/0Tn;

    move-object v4, v6

    .line 527051
    const-wide/16 v6, 0x7530

    invoke-interface {v5, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    move-wide v0, v4

    .line 527052
    invoke-static {p0, p1, v0, v1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Ge;J)V

    .line 527053
    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    invoke-static {p0, p1, v0, v1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b(Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Ge;J)V

    .line 527054
    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    invoke-static {p2}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/2Ge;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 527022
    iget-object v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    invoke-virtual {v1, p1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v1

    .line 527023
    invoke-virtual {v1}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v2

    .line 527024
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 527025
    invoke-virtual {p1}, LX/2Ge;->name()Ljava/lang/String;

    .line 527026
    :goto_0
    return v0

    .line 527027
    :cond_0
    iget-object v3, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->j:LX/2Gs;

    sget-object v4, LX/Cem;->ATTEMPT:LX/Cem;

    invoke-virtual {v4}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v2, v5}, LX/2Gs;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 527028
    :try_start_0
    new-instance v3, Lcom/facebook/push/fbpushtoken/UnregisterPushTokenParams;

    invoke-direct {v3, v2}, Lcom/facebook/push/fbpushtoken/UnregisterPushTokenParams;-><init>(Ljava/lang/String;)V

    .line 527029
    iget-object v4, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->d:LX/11H;

    iget-object v5, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->e:LX/2Gt;

    const-class v6, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    invoke-static {v6, p2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-virtual {v4, v5, v3, v6}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 527030
    iget-object v3, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->j:LX/2Gs;

    sget-object v4, LX/Cem;->SUCCESS:LX/Cem;

    invoke-virtual {v4}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v2, v5}, LX/2Gs;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527031
    invoke-virtual {v1}, LX/2H0;->n()V

    goto :goto_0

    .line 527032
    :catch_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->j:LX/2Gs;

    sget-object v3, LX/Cem;->FAILED:LX/Cem;

    invoke-virtual {v3}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v2, v4}, LX/2Gs;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527033
    invoke-virtual {v1}, LX/2H0;->n()V

    .line 527034
    const/4 v0, 0x0

    goto :goto_0

    .line 527035
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2H0;->n()V

    throw v0
.end method

.method public final b(LX/2Ge;LX/2H7;)V
    .locals 10

    .prologue
    .line 527005
    invoke-direct {p0, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;)LX/CfA;

    move-result-object v0

    .line 527006
    invoke-virtual {v0}, LX/CfA;->name()Ljava/lang/String;

    .line 527007
    sget-object v1, LX/CfA;->CURRENT:LX/CfA;

    if-eq v0, v1, :cond_1

    .line 527008
    iget-object v1, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->j:LX/2Gs;

    invoke-virtual {v0}, LX/CfA;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->i:LX/2Gq;

    invoke-virtual {v3, p1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v3

    invoke-virtual {v3}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b()Z

    move-result v5

    .line 527009
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 527010
    const-string v7, "service_type"

    invoke-interface {v6, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527011
    if-eqz v5, :cond_0

    .line 527012
    const-string v7, "pre_log"

    const-string v8, "true"

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527013
    :cond_0
    const-string v7, "push_reg_server_initial_status"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "registration_id"

    invoke-static {v7, v8, v6, v9, v3}, LX/2gN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 527014
    invoke-static {v1, v6}, LX/2Gs;->a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 527015
    :cond_1
    sget-object v1, LX/Cf8;->a:[I

    invoke-virtual {v0}, LX/CfA;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 527016
    sget-object v1, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a:Ljava/lang/Class;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected facebook registration status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 527017
    :cond_2
    :goto_0
    :pswitch_0
    return-void

    .line 527018
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->t:LX/0Uh;

    const/16 v1, 0x255

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->h:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 527019
    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 527020
    :pswitch_2
    invoke-virtual {p0, p1, p2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 527021
    :pswitch_3
    invoke-virtual {p0, p1, p2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
