.class public Lcom/facebook/push/c2dm/C2DMService;
.super LX/1ZN;
.source ""


# static fields
.field private static final l:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/2HA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Gs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2bC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2HE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Uo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2bD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 527004
    const-class v0, Lcom/facebook/push/c2dm/C2DMService;

    sput-object v0, Lcom/facebook/push/c2dm/C2DMService;->l:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 527002
    const-string v0, "C2DMReceiver"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 527003
    return-void
.end method

.method private a(J)J
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 526997
    cmp-long v0, p1, v2

    if-lez v0, :cond_1

    .line 526998
    iget-object v0, p0, Lcom/facebook/push/c2dm/C2DMService;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long p1, v0, p1

    .line 526999
    :cond_0
    :goto_0
    return-wide p1

    .line 527000
    :cond_1
    cmp-long v0, p1, v2

    if-gez v0, :cond_0

    .line 527001
    iget-object v0, p0, Lcom/facebook/push/c2dm/C2DMService;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    add-long/2addr v0, p1

    neg-long p1, v0

    goto :goto_0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 526991
    iget-object v0, p0, Lcom/facebook/push/c2dm/C2DMService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 526992
    iget-object v1, p0, Lcom/facebook/push/c2dm/C2DMService;->e:LX/2HE;

    .line 526993
    iget-object v2, v1, LX/2H3;->g:LX/0Tn;

    move-object v1, v2

    .line 526994
    iget-object v2, p0, Lcom/facebook/push/c2dm/C2DMService;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 526995
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 526996
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 526987
    const-class v0, Lcom/facebook/push/c2dm/C2DMService;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 526988
    invoke-static {p0, p1}, LX/37V;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    .line 526989
    sget-object v0, Lcom/facebook/push/c2dm/C2DMService;->l:Ljava/lang/Class;

    const-string v1, "Failed to start service"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 526990
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/push/c2dm/C2DMService;LX/2HA;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gs;LX/2bC;LX/2HE;LX/0SG;LX/0So;LX/0kb;LX/0Xl;LX/0Uo;LX/2bD;)V
    .locals 0

    .prologue
    .line 526986
    iput-object p1, p0, Lcom/facebook/push/c2dm/C2DMService;->a:LX/2HA;

    iput-object p2, p0, Lcom/facebook/push/c2dm/C2DMService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/push/c2dm/C2DMService;->c:LX/2Gs;

    iput-object p4, p0, Lcom/facebook/push/c2dm/C2DMService;->d:LX/2bC;

    iput-object p5, p0, Lcom/facebook/push/c2dm/C2DMService;->e:LX/2HE;

    iput-object p6, p0, Lcom/facebook/push/c2dm/C2DMService;->f:LX/0SG;

    iput-object p7, p0, Lcom/facebook/push/c2dm/C2DMService;->g:LX/0So;

    iput-object p8, p0, Lcom/facebook/push/c2dm/C2DMService;->h:LX/0kb;

    iput-object p9, p0, Lcom/facebook/push/c2dm/C2DMService;->i:LX/0Xl;

    iput-object p10, p0, Lcom/facebook/push/c2dm/C2DMService;->j:LX/0Uo;

    iput-object p11, p0, Lcom/facebook/push/c2dm/C2DMService;->k:LX/2bD;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v0, p0

    check-cast v0, Lcom/facebook/push/c2dm/C2DMService;

    invoke-static {v11}, LX/2HA;->a(LX/0QB;)LX/2HA;

    move-result-object v1

    check-cast v1, LX/2HA;

    invoke-static {v11}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v11}, LX/2Gs;->a(LX/0QB;)LX/2Gs;

    move-result-object v3

    check-cast v3, LX/2Gs;

    invoke-static {v11}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v4

    check-cast v4, LX/2bC;

    invoke-static {v11}, LX/2HE;->a(LX/0QB;)LX/2HE;

    move-result-object v5

    check-cast v5, LX/2HE;

    invoke-static {v11}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v11}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v11}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-static {v11}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-static {v11}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v10

    check-cast v10, LX/0Uo;

    invoke-static {v11}, LX/2bD;->a(LX/0QB;)LX/2bD;

    move-result-object v11

    check-cast v11, LX/2bD;

    invoke-static/range {v0 .. v11}, Lcom/facebook/push/c2dm/C2DMService;->a(Lcom/facebook/push/c2dm/C2DMService;LX/2HA;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gs;LX/2bC;LX/2HE;LX/0SG;LX/0So;LX/0kb;LX/0Xl;LX/0Uo;LX/2bD;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 526982
    iget-object v0, p0, Lcom/facebook/push/c2dm/C2DMService;->d:LX/2bC;

    sget-object v1, LX/3B4;->C2DM:LX/3B4;

    invoke-virtual {v1}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "gcm_deleted_messages"

    const/4 v7, 0x0

    .line 526983
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "messaging_push_notif_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 526984
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "total_deleted"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    aput-object p1, v3, v5

    invoke-static {v3}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v6

    move-object v3, v0

    move-object v5, v2

    move-object v8, v7

    move-object v9, v7

    invoke-virtual/range {v3 .. v9}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 526985
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 526959
    const-string v0, "registration_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 526960
    const-string v0, "error"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 526961
    const-string v0, "unregistered"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 526962
    iget-object v3, p0, Lcom/facebook/push/c2dm/C2DMService;->a:LX/2HA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 p1, 0x0

    const/4 p0, 0x0

    .line 526963
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v4, v3, LX/2HA;->h:LX/2H0;

    invoke-virtual {v4}, LX/2H0;->a()Ljava/lang/String;

    .line 526964
    if-eqz v0, :cond_1

    .line 526965
    iget-object v4, v3, LX/2HA;->h:LX/2H0;

    invoke-virtual {v4}, LX/2H0;->h()V

    .line 526966
    iget-object v4, v3, LX/2HA;->e:LX/2Gs;

    sget-object v5, LX/Cem;->SUCCESS:LX/Cem;

    invoke-virtual {v5}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v3, LX/2HA;->h:LX/2H0;

    invoke-virtual {v6}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/2Gs;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 526967
    :goto_1
    return-void

    .line 526968
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 526969
    :cond_1
    iget-object v4, v3, LX/2HA;->k:LX/2H4;

    invoke-virtual {v4}, LX/2H4;->c()V

    .line 526970
    if-eqz v2, :cond_3

    .line 526971
    sget-object v4, LX/2HA;->b:Ljava/lang/Class;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Registration error "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 526972
    iget-object v4, v3, LX/2HA;->k:LX/2H4;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 526973
    const-string v4, "SERVICE_NOT_AVAILABLE"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 526974
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.c2dm.intent.RETRY"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 526975
    iget-object v5, v3, LX/2HA;->c:Landroid/content/Context;

    invoke-static {v5, p0, v4, p0}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 526976
    iget-object v5, v3, LX/2HA;->k:LX/2H4;

    invoke-virtual {v5, v4}, LX/2H4;->a(Landroid/app/PendingIntent;)V

    goto :goto_1

    .line 526977
    :cond_2
    iget-object v4, v3, LX/2HA;->h:LX/2H0;

    invoke-virtual {v4}, LX/2H0;->h()V

    goto :goto_1

    .line 526978
    :cond_3
    iget-object v4, v3, LX/2HA;->h:LX/2H0;

    invoke-virtual {v4, v1}, LX/2H0;->a(Ljava/lang/String;)V

    .line 526979
    iget-object v4, v3, LX/2HA;->k:LX/2H4;

    sget-object v5, LX/2gP;->SUCCESS:LX/2gP;

    invoke-virtual {v5}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 526980
    iget-object v4, v3, LX/2HA;->k:LX/2H4;

    invoke-virtual {v4}, LX/2H4;->d()V

    .line 526981
    iget-object v4, v3, LX/2HA;->g:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v5, LX/2Ge;->GCM:LX/2Ge;

    iget-object v6, v3, LX/2HA;->a:LX/2H7;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_1
.end method

.method private c(Landroid/content/Intent;)V
    .locals 13

    .prologue
    .line 526907
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 526908
    if-eqz v0, :cond_0

    .line 526909
    const-string v1, "message_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 526910
    const-string v2, "deleted_messages"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 526911
    const-string v1, "total_deleted"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 526912
    invoke-direct {p0, v0}, Lcom/facebook/push/c2dm/C2DMService;->a(Ljava/lang/String;)V

    .line 526913
    :cond_0
    :goto_0
    return-void

    .line 526914
    :cond_1
    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 526915
    invoke-direct {p0}, Lcom/facebook/push/c2dm/C2DMService;->a()V

    .line 526916
    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 526917
    iget-object v1, p0, Lcom/facebook/push/c2dm/C2DMService;->k:LX/2bD;

    sget-object v2, LX/3B4;->C2DM:LX/3B4;

    invoke-virtual {v1, p0, v0, v2}, LX/2bD;->a(Landroid/content/Context;Ljava/lang/String;LX/3B4;)V

    goto :goto_0

    .line 526918
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 526919
    const-string v1, "rti.mqtt.mqtt_config"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/facebook/push/c2dm/C2DMService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 526920
    const-string v2, "mqtt/connect_state"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 526921
    const-string v2, "mqtt/connect_state"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 526922
    const-string v1, "ml_state"

    invoke-direct {p0, v2, v3}, Lcom/facebook/push/c2dm/C2DMService;->a(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526923
    :cond_3
    const-string v1, "net_state"

    iget-object v2, p0, Lcom/facebook/push/c2dm/C2DMService;->h:LX/0kb;

    .line 526924
    iget-wide v6, v2, LX/0kb;->x:J

    move-wide v2, v6

    .line 526925
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526926
    const-string v1, "app_launch"

    iget-object v2, p0, Lcom/facebook/push/c2dm/C2DMService;->j:LX/0Uo;

    invoke-virtual {v2}, LX/0Uo;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526927
    const-string v1, "is_update"

    iget-object v2, p0, Lcom/facebook/push/c2dm/C2DMService;->j:LX/0Uo;

    .line 526928
    iget-boolean v3, v2, LX/0Uo;->V:Z

    move v2, v3

    .line 526929
    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526930
    const-string v1, "is_istl"

    iget-object v2, p0, Lcom/facebook/push/c2dm/C2DMService;->j:LX/0Uo;

    .line 526931
    iget-boolean v3, v2, LX/0Uo;->U:Z

    move v2, v3

    .line 526932
    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526933
    iget-object v1, p0, Lcom/facebook/push/c2dm/C2DMService;->d:LX/2bC;

    const/4 v8, 0x0

    .line 526934
    const-string v7, "gcm_empty_push_notification"

    move-object v6, v1

    move-object v9, v0

    move-object v10, v8

    move-object v11, v8

    move-object v12, v8

    .line 526935
    invoke-virtual/range {v6 .. v12}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 526936
    iget-object v0, p0, Lcom/facebook/push/c2dm/C2DMService;->i:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.facebook.rti.mqtt.intent.ACTION_WAKEUP"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x3e22ba3e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 526940
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 526941
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_2

    .line 526942
    :cond_0
    if-eqz p1, :cond_1

    .line 526943
    invoke-static {p1}, LX/37V;->a(Landroid/content/Intent;)Z

    :cond_1
    const/16 v0, 0x25

    const v2, 0x3db805a7

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 526944
    :goto_0
    return-void

    .line 526945
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 526946
    const-string v2, "com.google.android.c2dm.intent.REGISTRATION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 526947
    iget-object v2, p0, Lcom/facebook/push/c2dm/C2DMService;->c:LX/2Gs;

    sget-object v3, LX/2Ge;->GCM:LX/2Ge;

    invoke-virtual {v3}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v3

    const-string v4, "gcm_response"

    invoke-virtual {v2, v3, v4, v0}, LX/2Gs;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 526948
    invoke-direct {p0, p1}, Lcom/facebook/push/c2dm/C2DMService;->b(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 526949
    :cond_3
    :goto_1
    if-eqz p1, :cond_4

    .line 526950
    invoke-static {p1}, LX/37V;->a(Landroid/content/Intent;)Z

    .line 526951
    :cond_4
    const v0, 0x6c1093ea

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    .line 526952
    :cond_5
    :try_start_2
    const-string v2, "com.google.android.c2dm.intent.RECEIVE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 526953
    invoke-direct {p0, p1}, Lcom/facebook/push/c2dm/C2DMService;->c(Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 526954
    :catchall_0
    move-exception v0

    if-eqz p1, :cond_6

    .line 526955
    invoke-static {p1}, LX/37V;->a(Landroid/content/Intent;)Z

    :cond_6
    const v2, -0x3b66a438

    invoke-static {v2, v1}, LX/02F;->d(II)V

    throw v0

    .line 526956
    :cond_7
    :try_start_3
    const-string v2, "com.google.android.c2dm.intent.RETRY"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 526957
    iget-object v2, p0, Lcom/facebook/push/c2dm/C2DMService;->c:LX/2Gs;

    sget-object v3, LX/2Ge;->GCM:LX/2Ge;

    invoke-virtual {v3}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v3

    const-string v4, "gcm_response"

    invoke-virtual {v2, v3, v4, v0}, LX/2Gs;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 526958
    iget-object v0, p0, Lcom/facebook/push/c2dm/C2DMService;->a:LX/2HA;

    invoke-virtual {v0}, LX/2HA;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x675c618

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 526937
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 526938
    invoke-static {p0, p0}, Lcom/facebook/push/c2dm/C2DMService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 526939
    const/16 v1, 0x25

    const v2, 0x2b72cdda

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
