.class public Lcom/facebook/widget/listview/EmptyListViewItem;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hx;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/view/ViewStub;

.field private d:Landroid/widget/TextView;

.field private e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 560014
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 560015
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 560016
    iput-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->a:LX/0Ot;

    .line 560017
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 560018
    iput-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->b:LX/0Ot;

    .line 560019
    invoke-direct {p0}, Lcom/facebook/widget/listview/EmptyListViewItem;->a()V

    .line 560020
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 560012
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/listview/EmptyListViewItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 560013
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 560001
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 560002
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 560003
    iput-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->a:LX/0Ot;

    .line 560004
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 560005
    iput-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->b:LX/0Ot;

    .line 560006
    invoke-direct {p0}, Lcom/facebook/widget/listview/EmptyListViewItem;->a()V

    .line 560007
    invoke-virtual {p0}, Lcom/facebook/widget/listview/EmptyListViewItem;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->EmptyListViewItem:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 560008
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 560009
    iget-object v1, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->d:Landroid/widget/TextView;

    const/16 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 560010
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 560011
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 559994
    const-class v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-static {v0, p0}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 559995
    const v0, 0x7f030d0a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 559996
    const v0, 0x7f0d2098

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->c:Landroid/view/ViewStub;

    .line 559997
    const v0, 0x7f0d2099

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->d:Landroid/widget/TextView;

    .line 559998
    invoke-virtual {p0}, Lcom/facebook/widget/listview/EmptyListViewItem;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 559999
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 560000
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/widget/listview/EmptyListViewItem;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/listview/EmptyListViewItem;",
            "LX/0Ot",
            "<",
            "LX/0hx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559993
    iput-object p1, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->b:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v1, 0x91

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2db

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Lcom/facebook/widget/listview/EmptyListViewItem;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 559972
    iget-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    .line 559973
    iget-object v1, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 559974
    return-void

    .line 559975
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 559986
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->c:Landroid/view/ViewStub;

    invoke-static {v0}, LX/0hx;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559987
    iget-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->e:J

    .line 559988
    :cond_0
    if-nez p1, :cond_1

    iget-wide v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->e:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hx;

    iget-object v1, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->e:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->c:Landroid/view/ViewStub;

    invoke-virtual {v0, v2, v3, v1}, LX/0hx;->a(JLandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 559989
    iput-wide v6, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->e:J

    .line 559990
    :cond_1
    iget-object v1, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->c:Landroid/view/ViewStub;

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 559991
    return-void

    .line 559992
    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setMessage(I)V
    .locals 1

    .prologue
    .line 559983
    iget-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 559984
    invoke-direct {p0}, Lcom/facebook/widget/listview/EmptyListViewItem;->b()V

    .line 559985
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 559980
    iget-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 559981
    invoke-direct {p0}, Lcom/facebook/widget/listview/EmptyListViewItem;->b()V

    .line 559982
    return-void
.end method

.method public setMovementMethod(Landroid/text/method/MovementMethod;)V
    .locals 1

    .prologue
    .line 559978
    iget-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 559979
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 559976
    iget-object v0, p0, Lcom/facebook/widget/listview/EmptyListViewItem;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 559977
    return-void
.end method
