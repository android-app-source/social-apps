.class public Lcom/facebook/widget/FacebookProgressCircleViewAnimated;
.super Lcom/facebook/widget/FacebookProgressCircleView;
.source ""


# instance fields
.field public b:J

.field public c:Landroid/os/Handler;

.field public d:Z

.field private e:LX/0Sh;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 468214
    invoke-direct {p0, p1}, Lcom/facebook/widget/FacebookProgressCircleView;-><init>(Landroid/content/Context;)V

    .line 468215
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->c:Landroid/os/Handler;

    .line 468216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->d:Z

    .line 468217
    new-instance v0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/FacebookProgressCircleViewAnimated$1;-><init>(Lcom/facebook/widget/FacebookProgressCircleViewAnimated;)V

    iput-object v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->f:Ljava/lang/Runnable;

    .line 468218
    invoke-direct {p0, p1}, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->a(Landroid/content/Context;)V

    .line 468219
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 468220
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/FacebookProgressCircleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 468221
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->c:Landroid/os/Handler;

    .line 468222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->d:Z

    .line 468223
    new-instance v0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/FacebookProgressCircleViewAnimated$1;-><init>(Lcom/facebook/widget/FacebookProgressCircleViewAnimated;)V

    iput-object v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->f:Ljava/lang/Runnable;

    .line 468224
    invoke-direct {p0, p1}, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->a(Landroid/content/Context;)V

    .line 468225
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 468226
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/FacebookProgressCircleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 468227
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->c:Landroid/os/Handler;

    .line 468228
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->d:Z

    .line 468229
    new-instance v0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/FacebookProgressCircleViewAnimated$1;-><init>(Lcom/facebook/widget/FacebookProgressCircleViewAnimated;)V

    iput-object v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->f:Ljava/lang/Runnable;

    .line 468230
    invoke-direct {p0, p1}, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->a(Landroid/content/Context;)V

    .line 468231
    return-void
.end method

.method private final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 468232
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->e:LX/0Sh;

    .line 468233
    return-void
.end method


# virtual methods
.method public setProgress(D)V
    .locals 3

    .prologue
    .line 468234
    double-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(J)V

    .line 468235
    return-void
.end method

.method public setProgress(J)V
    .locals 5

    .prologue
    .line 468236
    iget-object v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 468237
    iput-wide p1, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->b:J

    .line 468238
    iget-wide v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->b:J

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 468239
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->a:D

    .line 468240
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->b:J

    .line 468241
    :cond_0
    :goto_0
    return-void

    .line 468242
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->d:Z

    if-nez v0, :cond_0

    .line 468243
    iget-object v0, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x14

    const v4, -0x1e8d810d

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
