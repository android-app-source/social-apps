.class public Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;
.super Lcom/facebook/drawee/span/DraweeSpanTextView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/5Ph;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:I

.field private final d:I

.field private final e:I

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vv;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 464466
    new-instance v0, LX/8uG;

    invoke-direct {v0}, LX/8uG;-><init>()V

    sput-object v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a:Ljava/util/Comparator;

    .line 464467
    const-class v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 464468
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 464469
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 464470
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 464471
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 464456
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/span/DraweeSpanTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 464457
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 464458
    iput-object v0, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->g:LX/0Ot;

    .line 464459
    const-class v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v0, p0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 464460
    sget-object v0, LX/03r;->TextWithEntitiesView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 464461
    const/16 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getCurrentTextColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->e:I

    .line 464462
    const/16 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a010c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->c:I

    .line 464463
    const/16 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->d:I

    .line 464464
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 464465
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLInlineStyle;)I
    .locals 1

    .prologue
    .line 464472
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->BOLD:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    if-ne p0, v0, :cond_0

    .line 464473
    const/4 v0, 0x1

    .line 464474
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/2nQ;Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V
    .locals 10
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 464475
    new-instance v2, Ljava/util/TreeSet;

    sget-object v0, LX/1vv;->a:Ljava/util/Comparator;

    invoke-direct {v2, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 464476
    float-to-int v3, p3

    .line 464477
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageAtRange;

    .line 464478
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLEntityWithImage;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLEntityWithImage;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 464479
    :try_start_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->k()I

    move-result v7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->j()I

    move-result v8

    invoke-static {v6, v7, v8}, LX/1yM;->a(Ljava/lang/String;II)LX/1yN;

    move-result-object v6

    .line 464480
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityWithImage;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 464481
    new-instance v7, LX/34R;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    invoke-static {v9, v0, v3}, LX/1vv;->a(III)I

    move-result v0

    invoke-direct {v7, v8, v6, v0, v3}, LX/34R;-><init>(Landroid/net/Uri;LX/1yN;II)V

    invoke-virtual {v2, v7}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 464482
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464483
    :catch_0
    move-exception v0

    .line 464484
    const-string v6, "TextWithEntitiesView"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 464485
    :cond_1
    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LX/34R;

    .line 464486
    iget-object v0, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vv;

    invoke-static {p4}, LX/34T;->a(I)I

    move-result v3

    sget-object v4, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1, v3, v4}, LX/1vv;->a(LX/2nQ;LX/34R;ILcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_2

    .line 464487
    :cond_2
    return-void
.end method

.method private a(Landroid/text/Spannable;II)V
    .locals 1

    .prologue
    .line 464488
    iget v0, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->c:I

    invoke-static {p1, p2, p3, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;III)V

    .line 464489
    iget v0, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->d:I

    invoke-static {p1, p2, p3, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Landroid/text/Spannable;III)V

    .line 464490
    return-void
.end method

.method private static a(Landroid/text/Spannable;III)V
    .locals 2

    .prologue
    .line 464491
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v1, 0x12

    invoke-interface {p0, v0, p1, p2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 464492
    return-void
.end method

.method private a(Landroid/text/Spannable;IILandroid/text/style/ClickableSpan;)V
    .locals 1

    .prologue
    .line 464493
    const/16 v0, 0x12

    invoke-interface {p1, p4, p2, p3, v0}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 464494
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;II)V

    .line 464495
    return-void
.end method

.method private a(Landroid/text/Spannable;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 8

    .prologue
    .line 464496
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 464497
    :try_start_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/1yL;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v7

    invoke-direct {v5, v6, v7}, LX/1yL;-><init>(II)V

    invoke-static {v4, v5}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v4

    .line 464498
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 464499
    :cond_0
    iget v0, v4, LX/1yN;->a:I

    move v0, v0

    .line 464500
    invoke-virtual {v4}, LX/1yN;->c()I

    move-result v4

    invoke-direct {p0, p1, v0, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;II)V

    .line 464501
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464502
    :cond_1
    iget v5, v4, LX/1yN;->a:I

    move v5, v5

    .line 464503
    invoke-virtual {v4}, LX/1yN;->c()I

    move-result v6

    new-instance v7, LX/8uL;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, p0, v0}, LX/8uL;-><init>(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Ljava/lang/String;)V

    invoke-direct {p0, p1, v5, v6, v7}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;IILandroid/text/style/ClickableSpan;)V

    .line 464504
    iget v0, v4, LX/1yN;->a:I

    move v0, v0

    .line 464505
    invoke-virtual {v4}, LX/1yN;->c()I

    move-result v4

    invoke-direct {p0, p1, v0, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Landroid/text/Spannable;II)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 464506
    :catch_0
    move-exception v0

    .line 464507
    const-string v4, "TextWithEntitiesView"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 464508
    :cond_2
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 464509
    return-void
.end method

.method private static a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/content/SecureContextHelper;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/1vv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 464510
    iput-object p1, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->g:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x1241

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/content/SecureContextHelper;LX/0Ot;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/text/Spannable;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/text/Spannable;",
            "LX/0Px",
            "<+",
            "LX/5Ph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 464324
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ph;

    .line 464325
    :try_start_0
    invoke-interface {v0}, LX/5Ph;->c()I

    move-result v3

    invoke-interface {v0}, LX/5Ph;->b()I

    move-result v0

    invoke-static {p1, v3, v0}, LX/1yM;->a(Ljava/lang/String;II)LX/1yN;

    move-result-object v0

    .line 464326
    iget v3, v0, LX/1yN;->a:I

    move v3, v3

    .line 464327
    invoke-virtual {v0}, LX/1yN;->c()I

    move-result v0

    iget v4, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->d:I

    invoke-static {p2, v3, v0, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Landroid/text/Spannable;III)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 464328
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464329
    :catch_0
    move-exception v0

    .line 464330
    const-string v3, "TextWithEntitiesView"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 464331
    :cond_0
    return-void
.end method

.method private b(Landroid/text/Spannable;II)V
    .locals 1

    .prologue
    .line 464342
    invoke-virtual {p0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getLinkTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    invoke-static {p1, p2, p3, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;III)V

    .line 464343
    return-void
.end method

.method private static b(Landroid/text/Spannable;III)V
    .locals 2

    .prologue
    .line 464344
    if-ltz p3, :cond_0

    .line 464345
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, p3}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 464346
    const/16 v1, 0x12

    invoke-interface {p0, v0, p1, p2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 464347
    :cond_0
    return-void
.end method

.method private static b(Landroid/text/Spannable;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 8

    .prologue
    .line 464348
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;

    .line 464349
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/1yL;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->k()I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->j()I

    move-result v7

    invoke-direct {v5, v6, v7}, LX/1yL;-><init>(II)V

    invoke-static {v4, v5}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v4

    .line 464350
    iget v5, v4, LX/1yN;->a:I

    move v5, v5

    .line 464351
    invoke-virtual {v4}, LX/1yN;->c()I

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->a()Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Lcom/facebook/graphql/enums/GraphQLInlineStyle;)I

    move-result v0

    invoke-static {p0, v5, v4, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Landroid/text/Spannable;III)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 464352
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464353
    :catch_0
    move-exception v0

    .line 464354
    const-string v4, "TextWithEntitiesView"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 464355
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/175;LX/8uH;)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 464356
    invoke-interface {p1}, LX/175;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464357
    invoke-interface {p1}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464358
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 464359
    :goto_0
    return-void

    .line 464360
    :cond_0
    invoke-interface {p1}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    .line 464361
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, LX/175;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 464362
    sget-object v2, LX/16z;->j:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 464363
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 464364
    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v3

    invoke-interface {v3}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 464365
    :try_start_0
    invoke-interface {p1}, LX/175;->a()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/1yL;

    invoke-interface {v0}, LX/1W5;->c()I

    move-result v5

    invoke-interface {v0}, LX/1W5;->b()I

    move-result v6

    invoke-direct {v4, v5, v6}, LX/1yL;-><init>(II)V

    invoke-static {v3, v4}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v3

    .line 464366
    iget v4, v3, LX/1yN;->a:I

    move v4, v4

    .line 464367
    invoke-virtual {v3}, LX/1yN;->c()I

    move-result v3

    new-instance v5, LX/8uJ;

    invoke-direct {v5, p2, v0}, LX/8uJ;-><init>(LX/8uH;LX/1W5;)V

    invoke-direct {p0, v1, v4, v3, v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;IILandroid/text/style/ClickableSpan;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 464368
    :catch_0
    move-exception v0

    .line 464369
    const-string v3, "TextWithEntitiesView"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 464370
    :cond_2
    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464371
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_0
.end method

.method public final a(LX/3Ab;LX/7wC;)V
    .locals 7

    .prologue
    .line 464372
    invoke-interface {p1}, LX/3Ab;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464373
    invoke-interface {p1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464374
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 464375
    :goto_0
    return-void

    .line 464376
    :cond_0
    invoke-interface {p1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    .line 464377
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 464378
    sget-object v2, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 464379
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ph;

    .line 464380
    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v3

    invoke-interface {v3}, LX/1yA;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 464381
    :try_start_0
    invoke-interface {p1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/1yL;

    invoke-interface {v0}, LX/5Ph;->c()I

    move-result v5

    invoke-interface {v0}, LX/5Ph;->b()I

    move-result v6

    invoke-direct {v4, v5, v6}, LX/1yL;-><init>(II)V

    invoke-static {v3, v4}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v3

    .line 464382
    iget v4, v3, LX/1yN;->a:I

    move v4, v4

    .line 464383
    invoke-virtual {v3}, LX/1yN;->c()I

    move-result v3

    new-instance v5, LX/8uK;

    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v0

    invoke-direct {v5, p2, v0}, LX/8uK;-><init>(LX/7wC;LX/1yA;)V

    invoke-direct {p0, v1, v4, v3, v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;IILandroid/text/style/ClickableSpan;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 464384
    :catch_0
    move-exception v0

    .line 464385
    const-string v3, "TextWithEntitiesView"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 464386
    :cond_2
    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464387
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V
    .locals 2

    .prologue
    .line 464388
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 464389
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 464390
    invoke-virtual {p0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->a()V

    .line 464391
    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464392
    :goto_0
    return-void

    .line 464393
    :cond_0
    new-instance v1, LX/2nQ;

    invoke-direct {v1, v0}, LX/2nQ;-><init>(Ljava/lang/CharSequence;)V

    .line 464394
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/2nQ;Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V

    .line 464395
    invoke-virtual {p0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setDraweeSpanStringBuilder(LX/2nQ;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<+",
            "LX/1W5;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 464396
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 464397
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 464398
    :try_start_0
    invoke-interface {v0}, LX/1W5;->c()I

    move-result v4

    invoke-interface {v0}, LX/1W5;->b()I

    move-result v0

    invoke-static {p1, v4, v0}, LX/1yM;->a(Ljava/lang/String;II)LX/1yN;

    move-result-object v0

    .line 464399
    iget v4, v0, LX/1yN;->a:I

    move v4, v4

    .line 464400
    invoke-virtual {v0}, LX/1yN;->c()I

    move-result v0

    iget v5, p0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->d:I

    invoke-static {v2, v4, v0, v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Landroid/text/Spannable;III)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 464401
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464402
    :catch_0
    move-exception v0

    .line 464403
    const-string v4, "TextWithEntitiesView"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 464404
    :cond_0
    invoke-virtual {p0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464405
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/479",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 464406
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 464407
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 464408
    if-eqz p2, :cond_1

    .line 464409
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/479;

    .line 464410
    :try_start_0
    iget-object v3, v0, LX/479;->a:LX/1yL;

    move-object v3, v3

    .line 464411
    invoke-static {p1, v3}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v3

    .line 464412
    iget-object v4, v0, LX/479;->b:Ljava/lang/Object;

    move-object v4, v4

    .line 464413
    if-eqz v4, :cond_0

    .line 464414
    iget v4, v3, LX/1yN;->a:I

    move v4, v4

    .line 464415
    invoke-virtual {v3}, LX/1yN;->c()I

    move-result v3

    new-instance v5, LX/8uL;

    .line 464416
    iget-object p2, v0, LX/479;->b:Ljava/lang/Object;

    move-object v0, p2

    .line 464417
    check-cast v0, Ljava/lang/String;

    invoke-direct {v5, p0, v0}, LX/8uL;-><init>(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Ljava/lang/String;)V

    invoke-direct {p0, v1, v4, v3, v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;IILandroid/text/style/ClickableSpan;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 464418
    :catch_0
    move-exception v0

    .line 464419
    const-string v3, "TextWithEntitiesView"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 464420
    :cond_0
    :try_start_1
    iget v0, v3, LX/1yN;->a:I

    move v0, v0

    .line 464421
    invoke-virtual {v3}, LX/1yN;->c()I

    move-result v3

    invoke-direct {p0, v1, v0, v3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;II)V
    :try_end_1
    .catch LX/47A; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 464422
    :cond_1
    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464423
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V
    .locals 2

    .prologue
    .line 464332
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 464333
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 464334
    invoke-virtual {p0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->a()V

    .line 464335
    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464336
    :goto_0
    return-void

    .line 464337
    :cond_0
    new-instance v1, LX/2nQ;

    invoke-direct {v1, v0}, LX/2nQ;-><init>(Ljava/lang/CharSequence;)V

    .line 464338
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/2nQ;Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V

    .line 464339
    invoke-direct {p0, v1, p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    .line 464340
    invoke-static {v1, p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Landroid/text/Spannable;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    .line 464341
    invoke-virtual {p0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setDraweeSpanStringBuilder(LX/2nQ;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<+",
            "LX/5Ph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 464424
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 464425
    invoke-direct {p0, p1, v0, p2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Ljava/lang/String;Landroid/text/Spannable;LX/0Px;)V

    .line 464426
    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464427
    return-void
.end method

.method public setLinkableTextWithEntities(LX/3Ab;)V
    .locals 9

    .prologue
    .line 464428
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 464429
    invoke-interface {p1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 464430
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-interface {p1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 464431
    invoke-interface {p1}, LX/3Ab;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 464432
    invoke-interface {p1}, LX/3Ab;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Ph;

    .line 464433
    invoke-interface {p1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/1yL;

    invoke-interface {v0}, LX/5Ph;->c()I

    move-result v7

    invoke-interface {v0}, LX/5Ph;->b()I

    move-result v8

    invoke-direct {v6, v7, v8}, LX/1yL;-><init>(II)V

    invoke-static {v5, v6}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v5

    .line 464434
    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v6

    invoke-interface {v6}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 464435
    :cond_0
    iget v0, v5, LX/1yN;->a:I

    move v0, v0

    .line 464436
    invoke-virtual {v5}, LX/1yN;->c()I

    move-result v5

    invoke-direct {p0, v2, v0, v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;II)V

    .line 464437
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464438
    :cond_1
    iget v6, v5, LX/1yN;->a:I

    move v6, v6

    .line 464439
    invoke-virtual {v5}, LX/1yN;->c()I

    move-result v5

    new-instance v7, LX/8uL;

    invoke-interface {v0}, LX/5Ph;->a()LX/1yA;

    move-result-object v0

    invoke-interface {v0}, LX/1yA;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, p0, v0}, LX/8uL;-><init>(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Ljava/lang/String;)V

    invoke-direct {p0, v2, v6, v5, v7}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;IILandroid/text/style/ClickableSpan;)V

    goto :goto_1

    .line 464440
    :cond_2
    invoke-virtual {p0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464441
    return-void
.end method

.method public setTextWithEntities(LX/175;)V
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 464442
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 464443
    invoke-interface {p1}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 464444
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-interface {p1}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 464445
    invoke-interface {p1}, LX/175;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 464446
    invoke-interface {p1}, LX/175;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 464447
    invoke-interface {p1}, LX/175;->a()Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/1yL;

    invoke-interface {v0}, LX/1W5;->c()I

    move-result v7

    invoke-interface {v0}, LX/1W5;->b()I

    move-result v8

    invoke-direct {v6, v7, v8}, LX/1yL;-><init>(II)V

    invoke-static {v5, v6}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v5

    .line 464448
    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v6

    invoke-interface {v6}, LX/171;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 464449
    :cond_0
    iget v0, v5, LX/1yN;->a:I

    move v0, v0

    .line 464450
    invoke-virtual {v5}, LX/1yN;->c()I

    move-result v5

    invoke-direct {p0, v2, v0, v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;II)V

    .line 464451
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464452
    :cond_1
    iget v6, v5, LX/1yN;->a:I

    move v6, v6

    .line 464453
    invoke-virtual {v5}, LX/1yN;->c()I

    move-result v5

    new-instance v7, LX/8uL;

    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v0

    invoke-interface {v0}, LX/171;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, p0, v0}, LX/8uL;-><init>(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Ljava/lang/String;)V

    invoke-direct {p0, v2, v6, v5, v7}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Landroid/text/Spannable;IILandroid/text/style/ClickableSpan;)V

    goto :goto_1

    .line 464454
    :cond_2
    invoke-virtual {p0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 464455
    return-void
.end method
