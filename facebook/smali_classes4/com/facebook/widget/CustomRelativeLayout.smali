.class public Lcom/facebook/widget/CustomRelativeLayout;
.super Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
.source ""

# interfaces
.implements LX/0h0;
.implements LX/0h1;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Z

.field private e:I

.field private f:LX/0QA;

.field private g:LX/1B1;

.field public h:LX/JiT;

.field private i:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "LX/10U;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 443262
    invoke-direct {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 443263
    iput-object v1, p0, Lcom/facebook/widget/CustomRelativeLayout;->a:Ljava/lang/String;

    .line 443264
    iput-object v1, p0, Lcom/facebook/widget/CustomRelativeLayout;->b:Ljava/lang/String;

    .line 443265
    iput-object v1, p0, Lcom/facebook/widget/CustomRelativeLayout;->c:Ljava/lang/String;

    .line 443266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->d:Z

    .line 443267
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 443268
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 443231
    invoke-direct {p0, p1, p2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 443232
    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->a:Ljava/lang/String;

    .line 443233
    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->b:Ljava/lang/String;

    .line 443234
    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->c:Ljava/lang/String;

    .line 443235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->d:Z

    .line 443236
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 443237
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 443238
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 443239
    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->a:Ljava/lang/String;

    .line 443240
    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->b:Ljava/lang/String;

    .line 443241
    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->c:Ljava/lang/String;

    .line 443242
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->d:Z

    .line 443243
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 443244
    return-void
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 443245
    if-eqz p2, :cond_0

    .line 443246
    sget-object v0, LX/03r;->CustomRelativeLayout:[I

    invoke-virtual {p1, p2, v0, p3, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 443247
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/CustomRelativeLayout;->a:Ljava/lang/String;

    .line 443248
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 443249
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 443250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/widget/CustomRelativeLayout;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onMeasure"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->b:Ljava/lang/String;

    .line 443251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/widget/CustomRelativeLayout;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onLayout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->c:Ljava/lang/String;

    .line 443252
    :cond_0
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->f:LX/0QA;

    .line 443253
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 443254
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->g:LX/1B1;

    if-eqz v0, :cond_0

    .line 443255
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->g:LX/1B1;

    invoke-virtual {p0}, Lcom/facebook/widget/CustomRelativeLayout;->getEventBus()LX/0b4;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 443256
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 443257
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->g:LX/1B1;

    if-eqz v0, :cond_0

    .line 443258
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->g:LX/1B1;

    invoke-virtual {p0}, Lcom/facebook/widget/CustomRelativeLayout;->getEventBus()LX/0b4;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 443259
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 443260
    invoke-static {p0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0b2;)Z
    .locals 1

    .prologue
    .line 443278
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->g:LX/1B1;

    if-nez v0, :cond_0

    .line 443279
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->g:LX/1B1;

    .line 443280
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->g:LX/1B1;

    invoke-virtual {v0, p1}, LX/1B1;->a(LX/0b2;)Z

    move-result v0

    return v0
.end method

.method public final asViewGroup()Landroid/view/ViewGroup;
    .locals 0

    .prologue
    .line 443261
    return-object p0
.end method

.method public final attachRecyclableViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 443269
    instance-of v0, p1, LX/2ea;

    if-eqz v0, :cond_0

    .line 443270
    invoke-static {p0, p1, p2}, LX/4oH;->a(LX/0h1;Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443271
    :goto_0
    return-void

    .line 443272
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 443273
    invoke-virtual {p0}, Lcom/facebook/widget/CustomRelativeLayout;->requestLayout()V

    goto :goto_0
.end method

.method public final b(I)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)",
            "LX/0am",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 443274
    invoke-static {p0, p1}, LX/0jc;->a(Landroid/view/View;I)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final detachRecyclableViewFromParent(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 443275
    invoke-super {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->detachViewFromParent(Landroid/view/View;)V

    .line 443276
    invoke-virtual {p0}, Lcom/facebook/widget/CustomRelativeLayout;->requestLayout()V

    .line 443277
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 443214
    :try_start_0
    invoke-super {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 443215
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    if-nez v0, :cond_1

    .line 443216
    :cond_0
    :goto_0
    return-void

    .line 443217
    :cond_1
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 443218
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10U;

    .line 443219
    invoke-interface {v0}, LX/10U;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 443220
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 443221
    :catch_0
    move-exception v0

    .line 443222
    iget v1, p0, Lcom/facebook/widget/CustomRelativeLayout;->e:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V

    goto :goto_0

    .line 443223
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->removeAll(Ljava/util/Collection;)Z

    .line 443224
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->i:Ljava/util/concurrent/CopyOnWriteArraySet;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 443226
    :catch_1
    move-exception v0

    .line 443227
    iget v1, p0, Lcom/facebook/widget/CustomRelativeLayout;->e:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 443228
    iget-boolean v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->d:Z

    if-eqz v0, :cond_0

    .line 443229
    invoke-super {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 443230
    :cond_0
    return-void
.end method

.method public final dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 443211
    iget-boolean v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->d:Z

    if-eqz v0, :cond_0

    .line 443212
    invoke-super {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    .line 443213
    :cond_0
    return-void
.end method

.method public getEventBus()LX/0b4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0b4;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 443210
    const/4 v0, 0x0

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x25167123

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 443207
    invoke-super {p0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->onAttachedToWindow()V

    .line 443208
    invoke-direct {p0}, Lcom/facebook/widget/CustomRelativeLayout;->b()V

    .line 443209
    const/16 v1, 0x2d

    const v2, 0x6bc2629c    # 4.6999456E26f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5c5da5be

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 443204
    invoke-super {p0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->onDetachedFromWindow()V

    .line 443205
    invoke-direct {p0}, Lcom/facebook/widget/CustomRelativeLayout;->c()V

    .line 443206
    const/16 v1, 0x2d

    const v2, -0x7fcd7e94

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 0

    .prologue
    .line 443201
    invoke-super {p0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->onFinishTemporaryDetach()V

    .line 443202
    invoke-direct {p0}, Lcom/facebook/widget/CustomRelativeLayout;->b()V

    .line 443203
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 443132
    iget-object v2, p0, Lcom/facebook/widget/CustomRelativeLayout;->c:Ljava/lang/String;

    .line 443133
    if-eqz v2, :cond_3

    const/4 v0, 0x1

    move v1, v0

    .line 443134
    :goto_0
    if-eqz v1, :cond_0

    .line 443135
    const v0, -0x47494e06

    invoke-static {v2, v0}, LX/02m;->a(Ljava/lang/String;I)V

    .line 443136
    :cond_0
    :try_start_0
    invoke-super/range {p0 .. p5}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->onLayout(ZIIII)V

    .line 443137
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->h:LX/JiT;

    if-eqz v0, :cond_1

    .line 443138
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->h:LX/JiT;

    invoke-virtual {v0}, LX/JiT;->a()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 443139
    :cond_1
    if-eqz v1, :cond_2

    .line 443140
    const v0, 0x4b3a5855    # 1.2212309E7f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 443141
    :cond_2
    :goto_1
    return-void

    .line 443142
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 443143
    :catch_0
    move-exception v0

    .line 443144
    :try_start_1
    iget v2, p0, Lcom/facebook/widget/CustomRelativeLayout;->e:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 443145
    if-eqz v1, :cond_2

    .line 443146
    const v0, -0x66293b79

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 443147
    :catch_1
    move-exception v0

    .line 443148
    :try_start_2
    iget v2, p0, Lcom/facebook/widget/CustomRelativeLayout;->e:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 443149
    if-eqz v1, :cond_2

    .line 443150
    const v0, 0x30d32913

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 443151
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 443152
    const v1, -0x275732ca

    invoke-static {v1}, LX/02m;->a(I)V

    :cond_4
    throw v0
.end method

.method public onMeasure(II)V
    .locals 3

    .prologue
    .line 443182
    iget-object v2, p0, Lcom/facebook/widget/CustomRelativeLayout;->b:Ljava/lang/String;

    .line 443183
    if-eqz v2, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 443184
    :goto_0
    if-eqz v1, :cond_0

    .line 443185
    const v0, 0x41d3d445

    invoke-static {v2, v0}, LX/02m;->a(Ljava/lang/String;I)V

    .line 443186
    :cond_0
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->onMeasure(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 443187
    if-eqz v1, :cond_1

    .line 443188
    const v0, -0x579aef75

    invoke-static {v0}, LX/02m;->a(I)V

    .line 443189
    :cond_1
    :goto_1
    return-void

    .line 443190
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 443191
    :catch_0
    move-exception v0

    .line 443192
    :try_start_1
    iget v2, p0, Lcom/facebook/widget/CustomRelativeLayout;->e:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 443193
    if-eqz v1, :cond_1

    .line 443194
    const v0, 0x68edf901

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 443195
    :catch_1
    move-exception v0

    .line 443196
    :try_start_2
    iget v2, p0, Lcom/facebook/widget/CustomRelativeLayout;->e:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 443197
    if-eqz v1, :cond_1

    .line 443198
    const v0, -0x3eceeb0a

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 443199
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 443200
    const v1, 0x471a16b4

    invoke-static {v1}, LX/02m;->a(I)V

    :cond_3
    throw v0
.end method

.method public onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 443179
    invoke-super {p0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->onStartTemporaryDetach()V

    .line 443180
    invoke-direct {p0}, Lcom/facebook/widget/CustomRelativeLayout;->c()V

    .line 443181
    return-void
.end method

.method public final removeRecyclableViewFromParent(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 443177
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->removeDetachedView(Landroid/view/View;Z)V

    .line 443178
    return-void
.end method

.method public final restoreHierarchyState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 443175
    invoke-super {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 443176
    return-void
.end method

.method public final saveHierarchyState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 443173
    invoke-super {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    .line 443174
    return-void
.end method

.method public setContentView(I)V
    .locals 4

    .prologue
    .line 443157
    iput p1, p0, Lcom/facebook/widget/CustomRelativeLayout;->e:I

    .line 443158
    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/CustomRelativeLayout;->a:Ljava/lang/String;

    .line 443159
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/CustomRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/widget/CustomRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 443160
    const-string v1, "%s.setContentView(%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/widget/CustomRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const v0, 0x152dcaa8

    invoke-static {v1, v2, v0}, LX/02m;->a(Ljava/lang/String;[Ljava/lang/Object;I)V

    .line 443161
    :goto_1
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/widget/CustomRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 443162
    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 443163
    const v0, -0x2f4e26ad

    invoke-static {v0}, LX/02m;->a(I)V

    .line 443164
    :goto_2
    return-void

    .line 443165
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 443166
    :cond_1
    const-string v1, "%s.setContentView"

    const v2, -0x31db19ba

    invoke-static {v1, v0, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    goto :goto_1

    .line 443167
    :catch_0
    move-exception v0

    .line 443168
    :try_start_1
    iget v1, p0, Lcom/facebook/widget/CustomRelativeLayout;->e:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 443169
    const v0, -0x7cb745e1

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_2

    .line 443170
    :catch_1
    move-exception v0

    .line 443171
    :try_start_2
    iget v1, p0, Lcom/facebook/widget/CustomRelativeLayout;->e:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 443172
    const v0, 0x6fa818f3

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_2

    :catchall_0
    move-exception v0

    const v1, 0x220bbb20

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setOnSupportLayoutChangeListener(LX/JiT;)V
    .locals 0

    .prologue
    .line 443155
    iput-object p1, p0, Lcom/facebook/widget/CustomRelativeLayout;->h:LX/JiT;

    .line 443156
    return-void
.end method

.method public setSaveFromParentEnabledCompat(Z)V
    .locals 0

    .prologue
    .line 443153
    iput-boolean p1, p0, Lcom/facebook/widget/CustomRelativeLayout;->d:Z

    .line 443154
    return-void
.end method
