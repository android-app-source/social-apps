.class public Lcom/facebook/widget/ShimmerFrameLayout;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field private static final c:Landroid/graphics/PorterDuffXfermode;


# instance fields
.field public a:Landroid/animation/ValueAnimator;

.field public b:Landroid/graphics/Bitmap;

.field private d:LX/03V;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Paint;

.field private g:LX/24V;

.field public h:LX/24Y;

.field private i:Landroid/graphics/Bitmap;

.field private j:Landroid/graphics/Bitmap;

.field public k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field public r:Z

.field private s:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 367312
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    sput-object v0, Lcom/facebook/widget/ShimmerFrameLayout;->c:Landroid/graphics/PorterDuffXfermode;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 367309
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/ShimmerFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 367310
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 367313
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/ShimmerFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 367314
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 367315
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 367316
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 367317
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->d:LX/03V;

    .line 367318
    invoke-virtual {p0, v2}, Lcom/facebook/widget/ShimmerFrameLayout;->setWillNotDraw(Z)V

    .line 367319
    new-instance v0, LX/24V;

    invoke-direct {v0}, LX/24V;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    .line 367320
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->e:Landroid/graphics/Paint;

    .line 367321
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->f:Landroid/graphics/Paint;

    .line 367322
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 367323
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 367324
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 367325
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->f:Landroid/graphics/Paint;

    sget-object v1, Lcom/facebook/widget/ShimmerFrameLayout;->c:Landroid/graphics/PorterDuffXfermode;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 367326
    invoke-direct {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->a()V

    .line 367327
    if-eqz p2, :cond_f

    .line 367328
    sget-object v0, LX/03r;->ShimmerFrameLayout:[I

    invoke-virtual {p1, p2, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 367329
    :try_start_0
    const/16 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367330
    const/16 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->setAutoStart(Z)V

    .line 367331
    :cond_0
    const/16 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367332
    const/16 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->setBaseAlpha(F)V

    .line 367333
    :cond_1
    const/16 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 367334
    const/16 v0, 0x2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->setDuration(I)V

    .line 367335
    :cond_2
    const/16 v0, 0x3

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 367336
    const/16 v0, 0x3

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->setRepeatCount(I)V

    .line 367337
    :cond_3
    const/16 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 367338
    const/16 v0, 0x4

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->setRepeatDelay(I)V

    .line 367339
    :cond_4
    const/16 v0, 0x5

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 367340
    const/16 v0, 0x5

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->setRepeatMode(I)V

    .line 367341
    :cond_5
    const/16 v0, 0x6

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 367342
    const/16 v0, 0x6

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 367343
    sparse-switch v0, :sswitch_data_0

    .line 367344
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    sget-object v2, LX/24W;->CW_0:LX/24W;

    iput-object v2, v0, LX/24V;->a:LX/24W;

    .line 367345
    :cond_6
    :goto_0
    const/16 v0, 0xd

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 367346
    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 367347
    packed-switch v0, :pswitch_data_0

    .line 367348
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    sget-object v2, LX/24X;->LINEAR:LX/24X;

    iput-object v2, v0, LX/24V;->i:LX/24X;

    .line 367349
    :cond_7
    :goto_1
    const/16 v0, 0x7

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 367350
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    const/16 v2, 0x7

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/24V;->c:F

    .line 367351
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 367352
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, v0, LX/24V;->d:I

    .line 367353
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 367354
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, v0, LX/24V;->e:I

    .line 367355
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 367356
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/24V;->f:F

    .line 367357
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 367358
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/24V;->g:F

    .line 367359
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 367360
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/24V;->h:F

    .line 367361
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 367362
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/24V;->b:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367363
    :cond_e
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 367364
    :cond_f
    return-void

    .line 367365
    :sswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    sget-object v2, LX/24W;->CW_90:LX/24W;

    iput-object v2, v0, LX/24V;->a:LX/24W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 367366
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 367367
    :sswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    sget-object v2, LX/24W;->CW_180:LX/24W;

    iput-object v2, v0, LX/24V;->a:LX/24W;

    goto/16 :goto_0

    .line 367368
    :sswitch_2
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    sget-object v2, LX/24W;->CW_270:LX/24W;

    iput-object v2, v0, LX/24V;->a:LX/24W;

    goto/16 :goto_0

    .line 367369
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    sget-object v2, LX/24X;->RADIAL:LX/24X;

    iput-object v2, v0, LX/24V;->i:LX/24X;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(FFF)F
    .locals 1

    .prologue
    .line 367370
    invoke-static {p0, p2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method private static a(II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 367371
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p0, p1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 367372
    :goto_0
    return-object v0

    .line 367373
    :catch_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 367374
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p0, p1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 367375
    invoke-virtual {p0, v2}, Lcom/facebook/widget/ShimmerFrameLayout;->setAutoStart(Z)V

    .line 367376
    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->setDuration(I)V

    .line 367377
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->setRepeatCount(I)V

    .line 367378
    invoke-virtual {p0, v2}, Lcom/facebook/widget/ShimmerFrameLayout;->setRepeatDelay(I)V

    .line 367379
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->setRepeatMode(I)V

    .line 367380
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    sget-object v1, LX/24W;->CW_0:LX/24W;

    iput-object v1, v0, LX/24V;->a:LX/24W;

    .line 367381
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    sget-object v1, LX/24X;->LINEAR:LX/24X;

    iput-object v1, v0, LX/24V;->i:LX/24X;

    .line 367382
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, v0, LX/24V;->c:F

    .line 367383
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput v2, v0, LX/24V;->d:I

    .line 367384
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput v2, v0, LX/24V;->e:I

    .line 367385
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    const/4 v1, 0x0

    iput v1, v0, LX/24V;->f:F

    .line 367386
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput v3, v0, LX/24V;->g:F

    .line 367387
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput v3, v0, LX/24V;->h:F

    .line 367388
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    const/high16 v1, 0x41a00000    # 20.0f

    iput v1, v0, LX/24V;->b:F

    .line 367389
    new-instance v0, LX/24Y;

    invoke-direct {v0}, LX/24Y;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->h:LX/24Y;

    .line 367390
    const v0, 0x3e99999a    # 0.3f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->setBaseAlpha(F)V

    .line 367391
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367392
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 367393
    invoke-direct {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 367394
    invoke-direct {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->e()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 367395
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 367396
    :cond_0
    const/4 v0, 0x0

    .line 367397
    :goto_0
    return v0

    .line 367398
    :cond_1
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {p0, v2}, Lcom/facebook/widget/ShimmerFrameLayout;->b(Landroid/graphics/Canvas;)V

    .line 367399
    iget-object v2, p0, Lcom/facebook/widget/ShimmerFrameLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 367400
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {p0, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->c(Landroid/graphics/Canvas;)V

    .line 367401
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v3, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 367402
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 367403
    const/4 v0, 0x0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 367404
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 367405
    return-void
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 367406
    invoke-direct {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getMaskBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 367407
    if-nez v0, :cond_0

    .line 367408
    :goto_0
    return-void

    .line 367409
    :cond_0
    iget v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->p:I

    iget v2, p0, Lcom/facebook/widget/ShimmerFrameLayout;->q:I

    iget v3, p0, Lcom/facebook/widget/ShimmerFrameLayout;->p:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/facebook/widget/ShimmerFrameLayout;->q:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 367410
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 367411
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 367412
    iget v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->p:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/widget/ShimmerFrameLayout;->q:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/widget/ShimmerFrameLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private d()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 367413
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->j:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 367414
    invoke-direct {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->j:Landroid/graphics/Bitmap;

    .line 367415
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->j:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private e()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 367416
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 367417
    invoke-direct {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    .line 367418
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private f()Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 367419
    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getWidth()I

    move-result v0

    .line 367420
    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getHeight()I

    move-result v1

    .line 367421
    :try_start_0
    invoke-static {v0, v1}, Lcom/facebook/widget/ShimmerFrameLayout;->a(II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 367422
    :goto_0
    return-object v0

    .line 367423
    :catch_0
    const-string v2, "ShimmerFrameLayout failed to create working bitmap"

    .line 367424
    iget-object v3, p0, Lcom/facebook/widget/ShimmerFrameLayout;->d:LX/03V;

    const-string v4, "ShimmerFrameLayout_frame_layout_oom"

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 367425
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 367426
    const-string v2, " (width = "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367427
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 367428
    const-string v0, ", height = "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367429
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 367430
    const-string v0, ")\n\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367431
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v4, v1, v0

    .line 367432
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367433
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367434
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 367435
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/widget/ShimmerFrameLayout;)V
    .locals 0

    .prologue
    .line 367436
    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->c()V

    .line 367437
    invoke-direct {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->h()V

    .line 367438
    invoke-direct {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->i()V

    .line 367439
    return-void
.end method

.method private getLayoutListener()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1

    .prologue
    .line 367440
    new-instance v0, LX/24Z;

    invoke-direct {v0, p0}, LX/24Z;-><init>(Lcom/facebook/widget/ShimmerFrameLayout;)V

    return-object v0
.end method

.method private getMaskBitmap()Landroid/graphics/Bitmap;
    .locals 14

    .prologue
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    const/4 v0, 0x0

    .line 367441
    iget-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 367442
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    .line 367443
    :goto_0
    return-object v0

    .line 367444
    :cond_0
    iget-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, LX/24V;->a(I)I

    move-result v9

    .line 367445
    iget-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, LX/24V;->b(I)I

    move-result v8

    .line 367446
    invoke-static {v9, v8}, Lcom/facebook/widget/ShimmerFrameLayout;->a(II)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    .line 367447
    new-instance v10, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    invoke-direct {v10, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 367448
    sget-object v1, LX/4oX;->a:[I

    iget-object v2, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget-object v2, v2, LX/24V;->i:LX/24X;

    invoke-virtual {v2}, LX/24X;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 367449
    sget-object v1, LX/4oX;->b:[I

    iget-object v2, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget-object v2, v2, LX/24V;->a:LX/24W;

    invoke-virtual {v2}, LX/24W;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    move v4, v0

    move v3, v9

    move v2, v0

    move v1, v0

    .line 367450
    :goto_1
    new-instance v0, Landroid/graphics/LinearGradient;

    int-to-float v1, v1

    int-to-float v2, v2

    int-to-float v3, v3

    int-to-float v4, v4

    iget-object v5, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    invoke-virtual {v5}, LX/24V;->a()[I

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    invoke-virtual {v6}, LX/24V;->b()[F

    move-result-object v6

    sget-object v7, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 367451
    :goto_2
    iget-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget v1, v1, LX/24V;->b:F

    div-int/lit8 v2, v9, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v8, 0x2

    int-to-float v3, v3

    invoke-virtual {v10, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 367452
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 367453
    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 367454
    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-static {v9, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    .line 367455
    neg-int v1, v0

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    add-int v3, v9, v0

    int-to-float v3, v3

    add-int/2addr v0, v8

    int-to-float v4, v0

    move-object v0, v10

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 367456
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    :pswitch_0
    move v4, v8

    move v3, v0

    move v2, v0

    move v1, v0

    .line 367457
    goto :goto_1

    :pswitch_1
    move v4, v0

    move v3, v0

    move v2, v0

    move v1, v9

    .line 367458
    goto :goto_1

    :pswitch_2
    move v4, v0

    move v3, v0

    move v2, v8

    move v1, v0

    .line 367459
    goto :goto_1

    .line 367460
    :pswitch_3
    div-int/lit8 v1, v9, 0x2

    .line 367461
    div-int/lit8 v2, v8, 0x2

    .line 367462
    new-instance v0, Landroid/graphics/RadialGradient;

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-static {v9, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-double v4, v3

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    double-to-float v3, v4

    iget-object v4, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    invoke-virtual {v4}, LX/24V;->a()[I

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    invoke-virtual {v5}, LX/24V;->b()[F

    move-result-object v5

    sget-object v6, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getShimmerAnimation()Landroid/animation/Animator;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 367463
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 367464
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    .line 367465
    :goto_0
    return-object v0

    .line 367466
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getWidth()I

    move-result v0

    .line 367467
    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getHeight()I

    move-result v1

    .line 367468
    iget-object v2, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget-object v2, v2, LX/24V;->i:LX/24X;

    invoke-virtual {v2}, LX/24X;->ordinal()I

    .line 367469
    sget-object v2, LX/4oX;->b:[I

    iget-object v3, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget-object v3, v3, LX/24V;->a:LX/24W;

    invoke-virtual {v3}, LX/24W;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 367470
    iget-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->h:LX/24Y;

    neg-int v2, v0

    invoke-virtual {v1, v2, v4, v0, v4}, LX/24Y;->a(IIII)V

    .line 367471
    :goto_1
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v1, v0, v4

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    iget v3, p0, Lcom/facebook/widget/ShimmerFrameLayout;->n:I

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/widget/ShimmerFrameLayout;->l:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    .line 367472
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->l:I

    iget v2, p0, Lcom/facebook/widget/ShimmerFrameLayout;->n:I

    add-int/2addr v1, v2

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 367473
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->m:I

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 367474
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->o:I

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 367475
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    new-instance v1, LX/4oW;

    invoke-direct {v1, p0}, LX/4oW;-><init>(Lcom/facebook/widget/ShimmerFrameLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 367476
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    goto :goto_0

    .line 367477
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->h:LX/24Y;

    neg-int v2, v1

    invoke-virtual {v0, v4, v2, v4, v1}, LX/24Y;->a(IIII)V

    goto :goto_1

    .line 367478
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->h:LX/24Y;

    neg-int v2, v0

    invoke-virtual {v1, v0, v4, v2, v4}, LX/24Y;->a(IIII)V

    goto :goto_1

    .line 367479
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->h:LX/24Y;

    neg-int v2, v1

    invoke-virtual {v0, v4, v1, v4, v2}, LX/24Y;->a(IIII)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private h()V
    .locals 1

    .prologue
    .line 367480
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 367481
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 367482
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    .line 367483
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 367484
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->j:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 367485
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 367486
    iput-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->j:Landroid/graphics/Bitmap;

    .line 367487
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 367488
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 367489
    iput-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    .line 367490
    :cond_1
    return-void
.end method

.method public static setMaskOffsetX(Lcom/facebook/widget/ShimmerFrameLayout;I)V
    .locals 1

    .prologue
    .line 367491
    iget v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->p:I

    if-ne v0, p1, :cond_0

    .line 367492
    :goto_0
    return-void

    .line 367493
    :cond_0
    iput p1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->p:I

    .line 367494
    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->invalidate()V

    goto :goto_0
.end method

.method public static setMaskOffsetY(Lcom/facebook/widget/ShimmerFrameLayout;I)V
    .locals 1

    .prologue
    .line 367495
    iget v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->q:I

    if-ne v0, p1, :cond_0

    .line 367496
    :goto_0
    return-void

    .line 367497
    :cond_0
    iput p1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->q:I

    .line 367498
    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->invalidate()V

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 367499
    iget-boolean v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->r:Z

    if-eqz v0, :cond_0

    .line 367500
    :goto_0
    return-void

    .line 367501
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getShimmerAnimation()Landroid/animation/Animator;

    move-result-object v0

    .line 367502
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 367503
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->r:Z

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 367504
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 367505
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 367506
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 367507
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 367508
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    .line 367509
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->r:Z

    .line 367510
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 367511
    iget-boolean v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->r:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getHeight()I

    move-result v0

    if-gtz v0, :cond_1

    .line 367512
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 367513
    :goto_0
    return-void

    .line 367514
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/widget/ShimmerFrameLayout;->a(Landroid/graphics/Canvas;)Z

    goto :goto_0
.end method

.method public getAngle()LX/24W;
    .locals 1

    .prologue
    .line 367515
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget-object v0, v0, LX/24V;->a:LX/24W;

    return-object v0
.end method

.method public getBaseAlpha()F
    .locals 2

    .prologue
    .line 367516
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public getDropoff()F
    .locals 1

    .prologue
    .line 367311
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget v0, v0, LX/24V;->c:F

    return v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 367517
    iget v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->l:I

    return v0
.end method

.method public getFixedHeight()I
    .locals 1

    .prologue
    .line 367269
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget v0, v0, LX/24V;->e:I

    return v0
.end method

.method public getFixedWidth()I
    .locals 1

    .prologue
    .line 367268
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget v0, v0, LX/24V;->d:I

    return v0
.end method

.method public getIntensity()F
    .locals 1

    .prologue
    .line 367267
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget v0, v0, LX/24V;->f:F

    return v0
.end method

.method public getMaskShape()LX/24X;
    .locals 1

    .prologue
    .line 367266
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget-object v0, v0, LX/24V;->i:LX/24X;

    return-object v0
.end method

.method public getRelativeHeight()F
    .locals 1

    .prologue
    .line 367265
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget v0, v0, LX/24V;->h:F

    return v0
.end method

.method public getRelativeWidth()F
    .locals 1

    .prologue
    .line 367264
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget v0, v0, LX/24V;->g:F

    return v0
.end method

.method public getRepeatCount()I
    .locals 1

    .prologue
    .line 367263
    iget v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->m:I

    return v0
.end method

.method public getRepeatDelay()I
    .locals 1

    .prologue
    .line 367262
    iget v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->n:I

    return v0
.end method

.method public getRepeatMode()I
    .locals 1

    .prologue
    .line 367261
    iget v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->o:I

    return v0
.end method

.method public getTilt()F
    .locals 1

    .prologue
    .line 367260
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iget v0, v0, LX/24V;->b:F

    return v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7d70a095    # 1.9990532E37f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 367255
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 367256
    iget-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->s:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-nez v1, :cond_0

    .line 367257
    invoke-direct {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getLayoutListener()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->s:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 367258
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/ShimmerFrameLayout;->s:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 367259
    const/16 v1, 0x2d

    const v2, -0x70eb53da

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x40523ba0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 367249
    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->c()V

    .line 367250
    iget-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->s:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v1, :cond_0

    .line 367251
    invoke-virtual {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/ShimmerFrameLayout;->s:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 367252
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->s:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 367253
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 367254
    const/16 v1, 0x2d

    const v2, 0x111d3c3d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAngle(LX/24W;)V
    .locals 1

    .prologue
    .line 367246
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput-object p1, v0, LX/24V;->a:LX/24W;

    .line 367247
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367248
    return-void
.end method

.method public setAutoStart(Z)V
    .locals 0

    .prologue
    .line 367243
    iput-boolean p1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->k:Z

    .line 367244
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367245
    return-void
.end method

.method public setBaseAlpha(F)V
    .locals 3

    .prologue
    .line 367270
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->e:Landroid/graphics/Paint;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2, p1}, Lcom/facebook/widget/ShimmerFrameLayout;->a(FFF)F

    move-result v1

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 367271
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367272
    return-void
.end method

.method public setDropoff(F)V
    .locals 1

    .prologue
    .line 367273
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput p1, v0, LX/24V;->c:F

    .line 367274
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367275
    return-void
.end method

.method public setDuration(I)V
    .locals 0

    .prologue
    .line 367276
    iput p1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->l:I

    .line 367277
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367278
    return-void
.end method

.method public setFixedHeight(I)V
    .locals 1

    .prologue
    .line 367279
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput p1, v0, LX/24V;->e:I

    .line 367280
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367281
    return-void
.end method

.method public setFixedWidth(I)V
    .locals 1

    .prologue
    .line 367282
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput p1, v0, LX/24V;->d:I

    .line 367283
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367284
    return-void
.end method

.method public setIntensity(F)V
    .locals 1

    .prologue
    .line 367285
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput p1, v0, LX/24V;->f:F

    .line 367286
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367287
    return-void
.end method

.method public setMaskShape(LX/24X;)V
    .locals 1

    .prologue
    .line 367288
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput-object p1, v0, LX/24V;->i:LX/24X;

    .line 367289
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367290
    return-void
.end method

.method public setRelativeHeight(I)V
    .locals 2

    .prologue
    .line 367291
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    int-to-float v1, p1

    iput v1, v0, LX/24V;->h:F

    .line 367292
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367293
    return-void
.end method

.method public setRelativeWidth(I)V
    .locals 2

    .prologue
    .line 367294
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    int-to-float v1, p1

    iput v1, v0, LX/24V;->g:F

    .line 367295
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367296
    return-void
.end method

.method public setRepeatCount(I)V
    .locals 0

    .prologue
    .line 367297
    iput p1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->m:I

    .line 367298
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367299
    return-void
.end method

.method public setRepeatDelay(I)V
    .locals 0

    .prologue
    .line 367300
    iput p1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->n:I

    .line 367301
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367302
    return-void
.end method

.method public setRepeatMode(I)V
    .locals 0

    .prologue
    .line 367303
    iput p1, p0, Lcom/facebook/widget/ShimmerFrameLayout;->o:I

    .line 367304
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367305
    return-void
.end method

.method public setTilt(F)V
    .locals 1

    .prologue
    .line 367306
    iget-object v0, p0, Lcom/facebook/widget/ShimmerFrameLayout;->g:LX/24V;

    iput p1, v0, LX/24V;->b:F

    .line 367307
    invoke-static {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->g(Lcom/facebook/widget/ShimmerFrameLayout;)V

    .line 367308
    return-void
.end method
