.class public Lcom/facebook/widget/animatablelistview/AnimatingItemView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements LX/2i1;


# instance fields
.field private a:LX/JEV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JEV",
            "<*>;"
        }
    .end annotation
.end field

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:I

.field private final g:LX/3Nk;

.field private h:LX/3Nl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 559821
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 559822
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->b:F

    .line 559823
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->c:F

    .line 559824
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->d:F

    .line 559825
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->e:F

    .line 559826
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->f:I

    .line 559827
    new-instance v0, LX/3Nk;

    invoke-direct {v0, p0}, LX/3Nk;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->g:LX/3Nk;

    .line 559828
    invoke-direct {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a()V

    .line 559829
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 559920
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 559921
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->b:F

    .line 559922
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->c:F

    .line 559923
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->d:F

    .line 559924
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->e:F

    .line 559925
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->f:I

    .line 559926
    new-instance v0, LX/3Nk;

    invoke-direct {v0, p0}, LX/3Nk;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->g:LX/3Nk;

    .line 559927
    invoke-direct {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a()V

    .line 559928
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 559911
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 559912
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->b:F

    .line 559913
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->c:F

    .line 559914
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->d:F

    .line 559915
    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->e:F

    .line 559916
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->f:I

    .line 559917
    new-instance v0, LX/3Nk;

    invoke-direct {v0, p0}, LX/3Nk;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->g:LX/3Nk;

    .line 559918
    invoke-direct {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a()V

    .line 559919
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 559909
    new-instance v0, LX/3Nl;

    invoke-direct {v0, p0}, LX/3Nl;-><init>(Lcom/facebook/widget/animatablelistview/AnimatingItemView;)V

    iput-object v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->h:LX/3Nl;

    .line 559910
    return-void
.end method

.method public static b(Lcom/facebook/widget/animatablelistview/AnimatingItemView;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 559868
    iget-object v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    if-eqz v0, :cond_4

    .line 559869
    iget v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->b:F

    iget-object v3, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    .line 559870
    iget v4, v3, LX/JEV;->b:F

    move v3, v4

    .line 559871
    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    move v0, v1

    .line 559872
    :goto_0
    iget v3, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->f:I

    iget-object v4, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    .line 559873
    iget v5, v4, LX/JEV;->f:I

    move v4, v5

    .line 559874
    if-eq v3, v4, :cond_3

    .line 559875
    :goto_1
    iget-object v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    .line 559876
    iget v3, v2, LX/JEV;->b:F

    move v2, v3

    .line 559877
    iput v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->b:F

    .line 559878
    iget-object v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    .line 559879
    iget v3, v2, LX/JEV;->f:I

    move v2, v3

    .line 559880
    iput v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->f:I

    .line 559881
    iget-object v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    .line 559882
    iget v3, v2, LX/JEV;->c:F

    move v2, v3

    .line 559883
    iput v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->c:F

    .line 559884
    iget-object v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    .line 559885
    iget v3, v2, LX/JEV;->d:F

    move v2, v3

    .line 559886
    iput v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->d:F

    .line 559887
    iget-object v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    .line 559888
    iget v3, v2, LX/JEV;->e:F

    move v2, v3

    .line 559889
    iput v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->e:F

    .line 559890
    :goto_2
    iget-object v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->g:LX/3Nk;

    iget v3, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->c:F

    invoke-virtual {v2, v3}, LX/3Nk;->setScaleX(F)V

    .line 559891
    iget-object v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->g:LX/3Nk;

    iget v3, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->d:F

    invoke-virtual {v2, v3}, LX/3Nk;->setScaleY(F)V

    .line 559892
    iget-object v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->g:LX/3Nk;

    iget v3, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->e:F

    invoke-virtual {v2, v3}, LX/3Nk;->setAlpha(F)V

    .line 559893
    if-eqz v1, :cond_0

    .line 559894
    iget-object v1, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->g:LX/3Nk;

    iget v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->f:I

    invoke-virtual {v1, v2}, LX/3Nk;->setVisibility(I)V

    .line 559895
    :cond_0
    if-eqz v0, :cond_1

    .line 559896
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->requestLayout()V

    .line 559897
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 559898
    goto :goto_0

    :cond_3
    move v1, v2

    .line 559899
    goto :goto_1

    .line 559900
    :cond_4
    iget v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->b:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_5

    move v0, v1

    .line 559901
    :goto_3
    iget v3, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->f:I

    if-eqz v3, :cond_6

    .line 559902
    :goto_4
    iput v4, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->b:F

    .line 559903
    iput v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->f:I

    .line 559904
    iput v4, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->c:F

    .line 559905
    iput v4, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->d:F

    .line 559906
    iput v4, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->e:F

    goto :goto_2

    :cond_5
    move v0, v2

    .line 559907
    goto :goto_3

    :cond_6
    move v1, v2

    .line 559908
    goto :goto_4
.end method


# virtual methods
.method public getItemInfo()LX/JEV;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/JEV",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 559929
    iget-object v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    return-object v0
.end method

.method public getWrappedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 559867
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 559852
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getChildCount()I

    move-result v7

    .line 559853
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getPaddingLeft()I

    move-result v1

    .line 559854
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getPaddingRight()I

    move-result v2

    sub-int v2, v0, v2

    .line 559855
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getPaddingTop()I

    move-result v3

    .line 559856
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getTop()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getPaddingBottom()I

    move-result v4

    sub-int v4, v0, v4

    .line 559857
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_2

    .line 559858
    invoke-virtual {p0, v6}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 559859
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v8, 0x8

    if-eq v0, v8, :cond_1

    .line 559860
    iget v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->b:F

    const/4 v8, 0x0

    cmpg-float v0, v0, v8

    if-gez v0, :cond_0

    .line 559861
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v8, v4, v3

    sub-int/2addr v0, v8

    .line 559862
    sub-int/2addr v3, v0

    .line 559863
    sub-int/2addr v4, v0

    :cond_0
    move-object v0, p0

    .line 559864
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/widget/CustomViewGroup;->layoutChild(IIIILandroid/view/View;)V

    .line 559865
    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 559866
    :cond_2
    return-void
.end method

.method public final onMeasure(II)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 559837
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getChildCount()I

    move-result v9

    move v8, v3

    move v6, v3

    move v7, v3

    .line 559838
    :goto_0
    if-ge v8, v9, :cond_0

    .line 559839
    invoke-virtual {p0, v8}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 559840
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_1

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    .line 559841
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 559842
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 559843
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v7, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 559844
    :goto_1
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v6, v0

    move v7, v1

    goto :goto_0

    .line 559845
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, v6

    .line 559846
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v7

    .line 559847
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 559848
    invoke-virtual {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 559849
    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->b:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 559850
    invoke-static {v0, p1}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->resolveSize(II)I

    move-result v0

    invoke-static {v1, p2}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->setMeasuredDimension(II)V

    .line 559851
    return-void

    :cond_1
    move v0, v6

    move v1, v7

    goto :goto_1
.end method

.method public setItemInfo(LX/JEV;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JEV",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 559830
    iget-object v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    if-eqz v0, :cond_0

    .line 559831
    iget-object v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    iget-object v1, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->h:LX/3Nl;

    invoke-virtual {v0, v1}, LX/JEV;->b(LX/3Nm;)V

    .line 559832
    :cond_0
    iput-object p1, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    .line 559833
    iget-object v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    if-eqz v0, :cond_1

    .line 559834
    iget-object v0, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->a:LX/JEV;

    iget-object v1, p0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->h:LX/3Nl;

    invoke-virtual {v0, v1}, LX/JEV;->a(LX/3Nm;)V

    .line 559835
    :cond_1
    invoke-static {p0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->b(Lcom/facebook/widget/animatablelistview/AnimatingItemView;)V

    .line 559836
    return-void
.end method
