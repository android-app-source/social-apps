.class public Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;
.super LX/25R;
.source ""

# interfaces
.implements LX/25S;


# instance fields
.field public l:LX/25T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:LX/1OU;

.field public n:LX/2ec;

.field public o:LX/7Tw;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:I

.field private q:I

.field private r:I

.field private s:F

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field public x:Z

.field private y:LX/1P5;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 369276
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 369277
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 369278
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 369279
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 369280
    invoke-direct {p0, p1, p2, p3}, LX/25R;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 369281
    iput v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->p:I

    .line 369282
    iput v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->q:I

    .line 369283
    iput v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->r:I

    .line 369284
    iput-boolean v2, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->v:Z

    .line 369285
    iput-boolean v2, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->w:Z

    .line 369286
    iput-boolean v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->x:Z

    .line 369287
    invoke-direct {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->m()V

    .line 369288
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-static {v0}, LX/25T;->a(LX/0QB;)LX/25T;

    move-result-object v0

    check-cast v0, LX/25T;

    iput-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    return-void
.end method

.method public static f(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;I)V
    .locals 2

    .prologue
    .line 369289
    iget-boolean v0, p0, LX/25R;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->x:Z

    if-eqz v0, :cond_1

    .line 369290
    if-nez p1, :cond_1

    .line 369291
    invoke-direct {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getNewPositionForSnap()I

    move-result v0

    .line 369292
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 369293
    iget v1, p0, LX/25R;->i:I

    move v1, v1

    .line 369294
    if-eq v0, v1, :cond_1

    .line 369295
    iget-object v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->o:LX/7Tw;

    if-eqz v1, :cond_0

    .line 369296
    iget-object v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->o:LX/7Tw;

    invoke-interface {v1}, LX/7Tw;->a()V

    .line 369297
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->b(IZ)V

    .line 369298
    :cond_1
    return-void
.end method

.method public static g(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;I)V
    .locals 2

    .prologue
    .line 369299
    iget-boolean v0, p0, LX/25R;->h:Z

    if-nez v0, :cond_1

    .line 369300
    invoke-virtual {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getOffset()I

    move-result v0

    .line 369301
    iget-object v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->j(II)V

    .line 369302
    :cond_0
    :goto_0
    return-void

    .line 369303
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->x:Z

    if-eqz v0, :cond_0

    .line 369304
    invoke-virtual {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getOffset()I

    move-result v0

    .line 369305
    if-eqz p1, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    .line 369306
    iget-object v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->j(II)V

    goto :goto_0
.end method

.method private getNewPositionForSnap()I
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 369307
    invoke-virtual {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getChildCount()I

    move-result v6

    .line 369308
    iget-object v2, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    invoke-virtual {v2}, LX/1P1;->l()I

    move-result v3

    .line 369309
    iget-object v2, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    invoke-virtual {v2}, LX/1P1;->m()I

    move-result v2

    if-nez v2, :cond_1

    .line 369310
    :cond_0
    :goto_0
    return v0

    .line 369311
    :cond_1
    iget-object v2, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    invoke-virtual {v2}, LX/1P1;->o()I

    move-result v2

    iget-object v4, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    invoke-virtual {v4}, LX/1OR;->D()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v2, v4, :cond_5

    move v5, v1

    .line 369312
    :goto_1
    if-gt v6, v1, :cond_2

    move v0, v3

    .line 369313
    goto :goto_0

    .line 369314
    :cond_2
    const v2, 0x7fffffff

    .line 369315
    invoke-virtual {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getRight()I

    move-result v4

    add-int/2addr v1, v4

    div-int/lit8 v7, v1, 0x2

    move v4, v0

    move v0, v3

    .line 369316
    :goto_2
    if-ge v4, v6, :cond_3

    .line 369317
    invoke-virtual {p0, v4}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 369318
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v8

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, v8

    div-int/lit8 v1, v1, 0x2

    .line 369319
    sub-int/2addr v1, v7

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 369320
    if-ge v1, v2, :cond_4

    .line 369321
    add-int v0, v3, v4

    .line 369322
    :goto_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_2

    .line 369323
    :cond_3
    if-eqz v5, :cond_0

    add-int/lit8 v1, v0, 0x1

    if-ge v1, v6, :cond_0

    .line 369324
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    move v5, v0

    goto :goto_1
.end method

.method private j(II)V
    .locals 3

    .prologue
    .line 369325
    iget v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->p:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->q:I

    if-ne p2, v0, :cond_1

    .line 369326
    :cond_0
    :goto_0
    return-void

    .line 369327
    :cond_1
    iput p1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->p:I

    .line 369328
    iput p2, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->q:I

    .line 369329
    iget-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->n:LX/2ec;

    if-eqz v0, :cond_0

    .line 369330
    iget-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->n:LX/2ec;

    iget v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->p:I

    iget v2, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->q:I

    invoke-interface {v0, v1, v2}, LX/2ec;->a(II)V

    goto :goto_0
.end method

.method private m()V
    .locals 3

    .prologue
    .line 369331
    const-class v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-static {v0, p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 369332
    invoke-virtual {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getLayoutManagerForInit()LX/25T;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    .line 369333
    iget-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 369334
    iget-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 369335
    iget-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    iget-object v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    .line 369336
    iget v2, v1, LX/1P1;->j:I

    move v1, v2

    .line 369337
    invoke-static {v0, v1}, LX/1P5;->a(LX/1OR;I)LX/1P5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->y:LX/1P5;

    .line 369338
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setOverScrollMode(I)V

    .line 369339
    new-instance v0, LX/25b;

    invoke-direct {v0, p0}, LX/25b;-><init>(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 369340
    iput-object p0, p0, LX/25R;->p:LX/25S;

    .line 369341
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 369342
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 369343
    iget v1, p0, LX/25R;->j:I

    if-gt v0, v1, :cond_0

    .line 369344
    const/4 v0, 0x0

    .line 369345
    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->r:I

    if-nez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->r:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b(IZ)V
    .locals 1

    .prologue
    .line 369346
    invoke-super {p0, p1, p2}, LX/25R;->b(IZ)V

    .line 369347
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->j(II)V

    .line 369348
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 369262
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 369263
    :cond_0
    :goto_0
    :pswitch_0
    iget-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->w:Z

    if-eqz v0, :cond_2

    invoke-super {p0, p1}, LX/25R;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 369264
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->s:F

    .line 369265
    invoke-virtual {p0, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->canScrollHorizontally(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->u:Z

    .line 369266
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->canScrollHorizontally(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->t:Z

    .line 369267
    iput-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->v:Z

    move-object v1, p0

    .line 369268
    :goto_2
    iput-boolean v0, v1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->w:Z

    goto :goto_0

    .line 369269
    :pswitch_2
    iget-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->v:Z

    if-eqz v0, :cond_0

    .line 369270
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->s:F

    sub-float/2addr v0, v1

    .line 369271
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v3, 0x40a00000    # 5.0f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    .line 369272
    iput-boolean v2, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->v:Z

    .line 369273
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->t:Z

    move-object v1, p0

    goto :goto_2

    :cond_1
    iget-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->u:Z

    move-object v1, p0

    goto :goto_2

    :cond_2
    move v0, v2

    .line 369274
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getLayoutManagerForInit()LX/25T;
    .locals 1

    .prologue
    .line 369275
    iget-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    return-object v0
.end method

.method public getOffset()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 369222
    iget-object v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->y:LX/1P5;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 369223
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->y:LX/1P5;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1P5;->a(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->y:LX/1P5;

    invoke-virtual {v1}, LX/1P5;->c()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public getRecyclerListener()LX/1OU;
    .locals 1

    .prologue
    .line 369224
    iget-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->m:LX/1OU;

    return-object v0
.end method

.method public final h(II)V
    .locals 6

    .prologue
    .line 369225
    iput p1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->r:I

    .line 369226
    invoke-virtual {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getPaddingLeft()I

    move-result v0

    sub-int v0, p2, v0

    invoke-virtual {p0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 369227
    iget-object v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    sub-int/2addr v0, p1

    div-int/lit8 v0, v0, 0x2

    .line 369228
    iput v0, v1, LX/25T;->f:I

    .line 369229
    iget-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    iget v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->r:I

    int-to-double v2, v1

    int-to-double v4, p2

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/25T;->a(D)V

    .line 369230
    return-void
.end method

.method public final i(II)V
    .locals 1

    .prologue
    .line 369231
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 369232
    if-nez v0, :cond_0

    .line 369233
    :goto_0
    return-void

    .line 369234
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->c()V

    .line 369235
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 369236
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/25R;->o:LX/1P1;

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    .line 369237
    :cond_1
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->j(II)V

    goto :goto_0

    .line 369238
    :cond_2
    iput p1, p0, LX/25R;->i:I

    .line 369239
    iget-object v0, p0, LX/25R;->o:LX/1P1;

    invoke-virtual {v0, p1, p2}, LX/1P1;->d(II)V

    goto :goto_1
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 369240
    const-string v0, "HScrollRecyclerView.onLayout"

    const v1, -0x30f1fba9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 369241
    :try_start_0
    invoke-super/range {p0 .. p5}, LX/25R;->onLayout(ZIIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369242
    const v0, -0x1ad2292b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 369243
    return-void

    .line 369244
    :catchall_0
    move-exception v0

    const v1, -0x14375862

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 369245
    iget-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->x:Z

    if-eqz v0, :cond_0

    .line 369246
    const/4 v0, 0x0

    .line 369247
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/25R;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setAdapter(LX/1OM;)V
    .locals 2

    .prologue
    .line 369248
    iget-object v1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->l:LX/25T;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 369249
    :goto_0
    iput-object v0, v1, LX/25T;->a:Ljava/lang/String;

    .line 369250
    invoke-super {p0, p1}, LX/25R;->setAdapter(LX/1OM;)V

    .line 369251
    return-void

    .line 369252
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setCurrentPosition(I)V
    .locals 1

    .prologue
    .line 369253
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->b(IZ)V

    .line 369254
    return-void
.end method

.method public setOnPageChangedListener(LX/2ec;)V
    .locals 0

    .prologue
    .line 369220
    iput-object p1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->n:LX/2ec;

    .line 369221
    return-void
.end method

.method public setOnSwipeListener(LX/7Tw;)V
    .locals 0

    .prologue
    .line 369255
    iput-object p1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->o:LX/7Tw;

    .line 369256
    return-void
.end method

.method public setRecyclerListener(LX/1OU;)V
    .locals 0

    .prologue
    .line 369257
    iput-object p1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->m:LX/1OU;

    .line 369258
    invoke-super {p0, p1}, LX/25R;->setRecyclerListener(LX/1OU;)V

    .line 369259
    return-void
.end method

.method public setScrollVelocityEnabled(Z)V
    .locals 0

    .prologue
    .line 369260
    iput-boolean p1, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->x:Z

    .line 369261
    return-void
.end method
