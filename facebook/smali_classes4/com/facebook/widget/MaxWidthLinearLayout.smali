.class public Lcom/facebook/widget/MaxWidthLinearLayout;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 464857
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 464858
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/widget/MaxWidthLinearLayout;->a:I

    .line 464859
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/MaxWidthLinearLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 464860
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 464871
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 464872
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/widget/MaxWidthLinearLayout;->a:I

    .line 464873
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/MaxWidthLinearLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 464874
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 464867
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 464868
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/widget/MaxWidthLinearLayout;->a:I

    .line 464869
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/MaxWidthLinearLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 464870
    return-void
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 464875
    if-eqz p2, :cond_0

    .line 464876
    sget-object v0, LX/03r;->MaxWidthLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 464877
    const/16 v1, 0x0

    const v2, 0x7fffffff

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/MaxWidthLinearLayout;->a:I

    .line 464878
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 464879
    :cond_0
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 464863
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 464864
    invoke-virtual {p0}, Lcom/facebook/widget/MaxWidthLinearLayout;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/MaxWidthLinearLayout;->a:I

    if-le v0, v1, :cond_0

    .line 464865
    iget v0, p0, Lcom/facebook/widget/MaxWidthLinearLayout;->a:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/facebook/widget/MaxWidthLinearLayout;->measure(II)V

    .line 464866
    :cond_0
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0

    .prologue
    .line 464861
    iput p1, p0, Lcom/facebook/widget/MaxWidthLinearLayout;->a:I

    .line 464862
    return-void
.end method
