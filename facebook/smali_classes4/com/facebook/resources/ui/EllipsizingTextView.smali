.class public Lcom/facebook/resources/ui/EllipsizingTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:Ljava/lang/CharSequence;

.field private b:I

.field private c:Z

.field private d:I

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/CharSequence;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 447156
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 447157
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 447158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 447114
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 447115
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/EllipsizingTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 447116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 447159
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 447160
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/EllipsizingTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 447161
    return-void
.end method

.method private a()Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 447179
    iget-object v0, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->a:Ljava/lang/CharSequence;

    .line 447180
    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->d(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v1

    .line 447181
    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    iget v3, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->b:I

    if-le v2, v3, :cond_1

    .line 447182
    iget-object v0, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->a:Ljava/lang/CharSequence;

    iget v2, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->b:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v1

    invoke-interface {v0, v4, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 447183
    iget-boolean v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->f:Z

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_1

    .line 447184
    :cond_0
    invoke-static {v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 447185
    iget-boolean v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->h:Z

    if-eqz v1, :cond_2

    .line 447186
    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 447187
    :cond_1
    :goto_0
    return-object v0

    .line 447188
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->g:Z

    if-eqz v1, :cond_3

    .line 447189
    new-array v1, v6, [Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v1, v4

    iget-object v0, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->i:Ljava/lang/CharSequence;

    aput-object v0, v1, v5

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 447190
    :cond_3
    new-array v1, v6, [Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v1, v4

    iget-object v0, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->i:Ljava/lang/CharSequence;

    aput-object v0, v1, v5

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 447162
    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    aput-object p1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->i:Ljava/lang/CharSequence;

    aput-object v2, v0, v1

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->d(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->b:I

    if-le v0, v1, :cond_0

    .line 447163
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 447164
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 447165
    invoke-interface {p1, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 447166
    :cond_0
    return-object p1
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 447167
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 447168
    sget-object v0, LX/03r;->EllipsizingTextView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 447169
    const/16 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->f:Z

    .line 447170
    iget v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->b:I

    if-nez v1, :cond_0

    .line 447171
    const v1, 0x7fffffff

    iput v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->b:I

    .line 447172
    :cond_0
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->g:Z

    .line 447173
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->h:Z

    .line 447174
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 447175
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->i:Ljava/lang/CharSequence;

    .line 447176
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 447177
    return-void

    .line 447178
    :cond_1
    const-string v1, "\u2026"

    iput-object v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->i:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method private b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 447138
    new-instance v1, LX/45I;

    invoke-direct {v1, p1}, LX/45I;-><init>(Ljava/lang/CharSequence;)V

    .line 447139
    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->i:Ljava/lang/CharSequence;

    aput-object v3, v0, v2

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->d(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v0

    iget v2, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->b:I

    if-le v0, v2, :cond_1

    .line 447140
    const/4 v3, -0x1

    .line 447141
    iget-object v0, v1, LX/45I;->a:Ljava/text/BreakIterator;

    invoke-virtual {v0}, Ljava/text/BreakIterator;->previous()I

    move-result v0

    :goto_1
    if-eq v0, v3, :cond_3

    .line 447142
    invoke-static {v1, v0}, LX/45I;->a(LX/45I;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 447143
    add-int/lit8 v2, v0, -0x1

    if-eq v2, v3, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-static {v1, v2}, LX/45I;->a(LX/45I;I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 447144
    :cond_0
    iget-object v2, v1, LX/45I;->b:Ljava/lang/CharSequence;

    const/4 v3, 0x0

    invoke-interface {v2, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 447145
    :goto_2
    move-object v0, v0

    .line 447146
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object p1, v0

    .line 447147
    goto :goto_0

    .line 447148
    :cond_1
    return-object p1

    .line 447149
    :cond_2
    iget-object v0, v1, LX/45I;->a:Ljava/text/BreakIterator;

    invoke-virtual {v0}, Ljava/text/BreakIterator;->previous()I

    move-result v0

    goto :goto_1

    .line 447150
    :cond_3
    const-string v0, ""

    goto :goto_2
.end method

.method private c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 447151
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 447152
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->i:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/resources/ui/EllipsizingTextView;->d(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v1

    iget v2, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->b:I

    if-le v1, v2, :cond_1

    .line 447153
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 447154
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    .line 447155
    :cond_1
    return-object v0
.end method

.method private d(Ljava/lang/CharSequence;)Landroid/text/Layout;
    .locals 8

    .prologue
    .line 447137
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/EllipsizingTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/resources/ui/EllipsizingTextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/resources/ui/EllipsizingTextView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/facebook/resources/ui/EllipsizingTextView;->getPaddingRight()I

    move-result v3

    sub-int v3, v1, v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object v0
.end method

.method private static e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 447134
    :goto_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447135
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0

    .line 447136
    :cond_0
    return-object p0
.end method


# virtual methods
.method public getMaxLines()I
    .locals 1

    .prologue
    .line 447133
    iget v0, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->b:I

    return v0
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2c

    const v1, 0x4b072506    # 8856838.0f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 447124
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 447125
    iget-boolean v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->c:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->d:I

    invoke-virtual {p0}, Lcom/facebook/resources/ui/EllipsizingTextView;->getMeasuredWidth()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 447126
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->e:Z

    .line 447127
    invoke-direct {p0}, Lcom/facebook/resources/ui/EllipsizingTextView;->a()Ljava/lang/CharSequence;

    move-result-object v1

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 447128
    iput-boolean v3, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->e:Z

    .line 447129
    iput-boolean v3, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->c:Z

    .line 447130
    invoke-virtual {p0}, Lcom/facebook/resources/ui/EllipsizingTextView;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->d:I

    .line 447131
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 447132
    :cond_1
    const/16 v1, 0x2d

    const v2, -0x75da080d

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 447118
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 447119
    iget-boolean v0, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->e:Z

    if-nez v0, :cond_0

    .line 447120
    iput-object p1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->a:Ljava/lang/CharSequence;

    .line 447121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->c:Z

    .line 447122
    :cond_0
    invoke-static {p0, p1, p4}, LX/0zn;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->j:Z

    .line 447123
    return-void
.end method

.method public setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
    .locals 0

    .prologue
    .line 447117
    return-void
.end method

.method public setMaxLines(I)V
    .locals 1

    .prologue
    .line 447110
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 447111
    iput p1, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->b:I

    .line 447112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/resources/ui/EllipsizingTextView;->c:Z

    .line 447113
    return-void
.end method
