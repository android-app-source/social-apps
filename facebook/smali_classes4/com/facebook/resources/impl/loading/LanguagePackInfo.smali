.class public Lcom/facebook/resources/impl/loading/LanguagePackInfo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final contentChecksum:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "content_checksum"
    .end annotation
.end field

.field public final delta:Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "delta"
    .end annotation
.end field

.field public final downloadChecksum:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "download_checksum"
    .end annotation
.end field

.field public final downloadUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "download_url"
    .end annotation
.end field

.field public final locale:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "locale"
    .end annotation
.end field

.field public final releaseNumber:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "release_number"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 495207
    const-class v0, Lcom/facebook/resources/impl/loading/LanguagePackInfoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 495208
    const-class v0, Lcom/facebook/resources/impl/loading/LanguagePackInfoSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 495209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495210
    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadChecksum:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    .line 495211
    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->delta:Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    .line 495212
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    .line 495213
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;)V
    .locals 0

    .prologue
    .line 495214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495215
    iput-object p1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    .line 495216
    iput p2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    .line 495217
    iput-object p3, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadChecksum:Ljava/lang/String;

    .line 495218
    iput-object p4, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    .line 495219
    iput-object p5, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    .line 495220
    iput-object p6, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->delta:Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    .line 495221
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 495222
    if-ne p0, p1, :cond_1

    .line 495223
    :cond_0
    :goto_0
    return v0

    .line 495224
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 495225
    goto :goto_0

    .line 495226
    :cond_3
    check-cast p1, Lcom/facebook/resources/impl/loading/LanguagePackInfo;

    .line 495227
    iget v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    iget v3, p1, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadChecksum:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadChecksum:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->delta:Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    iget-object v3, p1, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->delta:Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 495228
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadChecksum:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->delta:Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 495229
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{ downloadUrl:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " downloadChecksum:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadChecksum:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " contentChecksum:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " releaseNumber:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " locale:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delta:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->delta:Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
