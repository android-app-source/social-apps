.class public Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final downloadUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "download_url"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 495236
    const-class v0, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 495237
    const-class v0, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfoSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 495238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;->downloadUrl:Ljava/lang/String;

    .line 495240
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 495241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495242
    iput-object p1, p0, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;->downloadUrl:Ljava/lang/String;

    .line 495243
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 495244
    if-ne p0, p1, :cond_0

    .line 495245
    const/4 v0, 0x1

    .line 495246
    :goto_0
    return v0

    .line 495247
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 495248
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 495249
    :cond_2
    check-cast p1, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    .line 495250
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;->downloadUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;->downloadUrl:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 495251
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;->downloadUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 495252
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{ downloadUrl:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;->downloadUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
