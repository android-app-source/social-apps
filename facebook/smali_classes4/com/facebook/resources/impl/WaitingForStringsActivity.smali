.class public Lcom/facebook/resources/impl/WaitingForStringsActivity;
.super Landroid/app/Activity;
.source ""

# interfaces
.implements LX/0Xn;
.implements LX/2JL;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0Vv;

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;

.field private e:Ljava/util/concurrent/ScheduledExecutorService;

.field public f:LX/0Wn;

.field private g:Lcom/facebook/content/SecureContextHelper;

.field private h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/16I;

.field private k:LX/0Yb;

.field public l:Landroid/view/View;

.field public m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Landroid/content/Intent;

.field public p:Z

.field public q:Z

.field public r:LX/0Uh;

.field private final s:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 525055
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "i18n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/facebook/resources/impl/WaitingForStringsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 525065
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 525066
    iput-boolean v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->p:Z

    .line 525067
    iput-boolean v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->q:Z

    .line 525068
    new-instance v0, LX/2z3;

    invoke-direct {v0, p0}, LX/2z3;-><init>(Lcom/facebook/resources/impl/WaitingForStringsActivity;)V

    iput-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->s:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a(LX/0Vv;Ljava/util/concurrent/ScheduledExecutorService;LX/0Wn;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0Or;LX/16I;LX/0Uh;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/common/build/IsInternalBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Vv;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Wn;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/16I;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 525056
    iput-object p1, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->b:LX/0Vv;

    .line 525057
    iput-object p2, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 525058
    iput-object p3, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->f:LX/0Wn;

    .line 525059
    iput-object p4, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->g:Lcom/facebook/content/SecureContextHelper;

    .line 525060
    iput-object p5, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->h:LX/0Or;

    .line 525061
    iput-object p6, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->i:LX/0Or;

    .line 525062
    iput-object p7, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->j:LX/16I;

    .line 525063
    iput-object p8, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->r:LX/0Uh;

    .line 525064
    return-void
.end method

.method public static a(Lcom/facebook/resources/impl/WaitingForStringsActivity;Z)V
    .locals 2

    .prologue
    .line 524986
    iget-boolean v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->p:Z

    if-nez v0, :cond_0

    .line 524987
    :goto_0
    return-void

    .line 524988
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->p:Z

    .line 524989
    if-eqz p1, :cond_1

    .line 524990
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->f:LX/0Wn;

    .line 524991
    iget-object v1, v0, LX/0Wn;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    const-string p1, "fbresources_loading_retry"

    invoke-interface {v1, p1}, LX/0Zb;->a(Ljava/lang/String;)V

    .line 524992
    :goto_1
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->f:LX/0Wn;

    invoke-virtual {v0}, LX/0Wn;->g()V

    .line 524993
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->b:LX/0Vv;

    invoke-virtual {v0}, LX/0Vv;->a()V

    .line 524994
    invoke-direct {p0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->b()V

    goto :goto_0

    .line 524995
    :cond_1
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->f:LX/0Wn;

    .line 524996
    iget-object v1, v0, LX/0Wn;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    const-string p1, "fbresources_auto_retry_loading"

    invoke-interface {v1, p1}, LX/0Zb;->a(Ljava/lang/String;)V

    .line 524997
    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/resources/impl/WaitingForStringsActivity;

    invoke-static {v8}, LX/0Vv;->a(LX/0QB;)LX/0Vv;

    move-result-object v1

    check-cast v1, LX/0Vv;

    invoke-static {v8}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v8}, LX/0Wn;->a(LX/0QB;)LX/0Wn;

    move-result-object v3

    check-cast v3, LX/0Wn;

    invoke-static {v8}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x1466

    invoke-static {v8, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x12c4

    invoke-static {v8, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v8}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v7

    check-cast v7, LX/16I;

    invoke-static {v8}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->a(LX/0Vv;Ljava/util/concurrent/ScheduledExecutorService;LX/0Wn;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0Or;LX/16I;LX/0Uh;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 525048
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 525049
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 525050
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->b:LX/0Vv;

    invoke-virtual {v0}, LX/0Vv;->d()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 525051
    if-nez v0, :cond_0

    .line 525052
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 525053
    :cond_0
    new-instance v1, LX/2yz;

    invoke-direct {v1, p0}, LX/2yz;-><init>(Lcom/facebook/resources/impl/WaitingForStringsActivity;)V

    iget-object v2, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->e:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 525054
    return-void
.end method

.method public static c$redex0(Lcom/facebook/resources/impl/WaitingForStringsActivity;)V
    .locals 3

    .prologue
    .line 525040
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->o:Landroid/content/Intent;

    if-eqz v0, :cond_2

    .line 525041
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->o:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 525042
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v2, -0x10000001

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 525043
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_1

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 525044
    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    and-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 525045
    :cond_1
    iget-object v1, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->g:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 525046
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->finish()V

    .line 525047
    return-void
.end method


# virtual methods
.method public final getProperty(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 525069
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 525035
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 525036
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 525037
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 525038
    iget-object v1, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->g:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 525039
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v0, 0x22

    const v1, 0x1901d3e9

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 525011
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 525012
    invoke-static {p0, p0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 525013
    invoke-virtual {p0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 525014
    if-eqz v0, :cond_0

    .line 525015
    const-string v2, "return_intent"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->o:Landroid/content/Intent;

    .line 525016
    :cond_0
    const v0, 0x7f031606

    invoke-virtual {p0, v0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->setContentView(I)V

    .line 525017
    const v0, 0x7f0d3180

    invoke-virtual {p0, v0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->c:Landroid/view/View;

    .line 525018
    const v0, 0x7f0d317e

    invoke-virtual {p0, v0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->d:Landroid/view/View;

    .line 525019
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->b:LX/0Vv;

    invoke-virtual {v0}, LX/0Vv;->b()Ljava/util/Locale;

    move-result-object v0

    .line 525020
    invoke-static {v0}, LX/0e9;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 525021
    invoke-virtual {p0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 525022
    const v0, 0x7f0d317f

    invoke-virtual {p0, v0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 525023
    const v4, 0x7f083c27

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 525024
    const v0, 0x7f0d3181

    invoke-virtual {p0, v0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 525025
    const v4, 0x7f083c28

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 525026
    const v0, 0x7f0d3182

    invoke-virtual {p0, v0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->l:Landroid/view/View;

    .line 525027
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->l:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 525028
    const v0, 0x7f0d3183

    invoke-virtual {p0, v0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->m:Landroid/view/View;

    .line 525029
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->m:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 525030
    const v0, 0x7f0d06e5

    invoke-virtual {p0, v0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->n:Landroid/view/View;

    .line 525031
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->n:Landroid/view/View;

    new-instance v2, LX/2z2;

    invoke-direct {v2, p0}, LX/2z2;-><init>(Lcom/facebook/resources/impl/WaitingForStringsActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 525032
    invoke-direct {p0}, Lcom/facebook/resources/impl/WaitingForStringsActivity;->b()V

    .line 525033
    iget-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->j:LX/16I;

    sget-object v2, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v3, Lcom/facebook/resources/impl/WaitingForStringsActivity$2;

    invoke-direct {v3, p0}, Lcom/facebook/resources/impl/WaitingForStringsActivity$2;-><init>(Lcom/facebook/resources/impl/WaitingForStringsActivity;)V

    invoke-virtual {v0, v2, v3}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->k:LX/0Yb;

    .line 525034
    const/16 v0, 0x23

    const v2, 0x28ccdba7

    invoke-static {v8, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x6a82bcf5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 525006
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 525007
    iget-object v1, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->k:LX/0Yb;

    if-eqz v1, :cond_0

    .line 525008
    iget-object v1, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->k:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 525009
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->k:LX/0Yb;

    .line 525010
    :cond_0
    const/16 v1, 0x23

    const v2, -0x5a10defc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x3e7f8618

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 525002
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 525003
    iget-object v1, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->f:LX/0Wn;

    .line 525004
    const v2, 0x440004

    const-string p0, "FbResourcesWaitingActivity"

    invoke-static {v1, v2, p0}, LX/0Wn;->b(LX/0Wn;ILjava/lang/String;)V

    .line 525005
    const/16 v1, 0x23

    const v2, 0x48d502af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x55c0ce7b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 524999
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 525000
    iget-object v1, p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;->f:LX/0Wn;

    invoke-virtual {v1}, LX/0Wn;->g()V

    .line 525001
    const/16 v1, 0x23

    const v2, 0x3d45b12f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setProperty(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 524998
    return-void
.end method
