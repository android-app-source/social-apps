.class public final Lcom/google/android/gms/gcm/GcmTaskService$zza;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field public final synthetic a:LX/2fD;

.field private final b:Ljava/lang/String;

.field private final c:LX/2fG;

.field private final d:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(LX/2fD;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->a:LX/2fD;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " GCM Task"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->b:Ljava/lang/String;

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->c:LX/2fG;

    iput-object p4, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->d:Landroid/os/Bundle;

    return-void

    :cond_0
    const-string v0, "com.google.android.gms.gcm.INetworkTaskCallback"

    invoke-interface {p3, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, LX/2fG;

    if-eqz v1, :cond_1

    check-cast v0, LX/2fG;

    goto :goto_0

    :cond_1
    new-instance v0, LX/2fF;

    invoke-direct {v0, p3}, LX/2fF;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public final run()V
    .locals 4

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->a:LX/2fD;

    new-instance v1, LX/2fH;

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->d:Landroid/os/Bundle;

    invoke-direct {v1, v2, v3}, LX/2fH;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, LX/2fD;->a(LX/2fH;)I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->c:LX/2fG;

    invoke-interface {v1, v0}, LX/2fG;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->a:LX/2fD;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2fD;->a$redex0(LX/2fD;Ljava/lang/String;)V

    :goto_0
    return-void

    :catch_0
    :try_start_1
    const-string v1, "GcmTaskService"

    const-string v2, "Error reporting result of operation to scheduler for "

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->a:LX/2fD;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2fD;->a$redex0(LX/2fD;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    :try_start_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->a:LX/2fD;

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmTaskService$zza;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/2fD;->a$redex0(LX/2fD;Ljava/lang/String;)V

    throw v0
.end method
