.class public final Lcom/google/android/gms/internal/zzpw$zzc;
.super Lcom/google/android/gms/internal/zzpw$zzf;
.source ""


# instance fields
.field public final synthetic a:LX/2wr;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/2wJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2wr;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/2wJ;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/internal/zzpw$zzc;->a:LX/2wr;

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzpw$zzf;-><init>(LX/2wr;)V

    iput-object p2, p0, Lcom/google/android/gms/internal/zzpw$zzc;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzpw$zzc;->a:LX/2wr;

    iget-object v0, v0, LX/2wr;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->g:LX/2wW;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzpw$zzc;->a:LX/2wr;

    iget-object v2, v1, LX/2wr;->r:LX/2wA;

    if-nez v2, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    :goto_0
    move-object v1, v2

    iput-object v1, v0, LX/2wW;->d:Ljava/util/Set;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzpw$zzc;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wJ;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzpw$zzc;->a:LX/2wr;

    iget-object v2, v2, LX/2wr;->o:LX/4st;

    iget-object v3, p0, Lcom/google/android/gms/internal/zzpw$zzc;->a:LX/2wr;

    iget-object v3, v3, LX/2wr;->a:LX/2wm;

    iget-object v3, v3, LX/2wm;->g:LX/2wW;

    iget-object v3, v3, LX/2wW;->d:Ljava/util/Set;

    invoke-interface {v0, v2, v3}, LX/2wJ;->a(LX/4st;Ljava/util/Set;)V

    goto :goto_1

    :cond_0
    return-void

    :cond_1
    new-instance v3, Ljava/util/HashSet;

    iget-object v2, v1, LX/2wr;->r:LX/2wA;

    iget-object v4, v2, LX/2wA;->b:Ljava/util/Set;

    move-object v2, v4

    invoke-direct {v3, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v2, v1, LX/2wr;->r:LX/2wA;

    iget-object v4, v2, LX/2wA;->d:Ljava/util/Map;

    move-object v4, v4

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2vs;

    iget-object v6, v1, LX/2wr;->a:LX/2wm;

    iget-object v6, v6, LX/2wm;->b:Ljava/util/Map;

    invoke-virtual {v2}, LX/2vs;->d()LX/2vo;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Kc;

    iget-object v2, v2, LX/3Kc;->a:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_3
    move-object v2, v3

    goto :goto_0
.end method
