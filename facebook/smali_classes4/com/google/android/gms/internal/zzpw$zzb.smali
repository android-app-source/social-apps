.class public final Lcom/google/android/gms/internal/zzpw$zzb;
.super Lcom/google/android/gms/internal/zzpw$zzf;
.source ""


# instance fields
.field public final synthetic a:LX/2wr;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2wJ;",
            "LX/2ws;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2wr;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/2wJ;",
            "LX/2ws;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/internal/zzpw$zzb;->a:LX/2wr;

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzpw$zzf;-><init>(LX/2wr;)V

    iput-object p2, p0, Lcom/google/android/gms/internal/zzpw$zzb;->c:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/zzpw$zzb;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v4

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wJ;

    invoke-interface {v0}, LX/2wJ;->o()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/internal/zzpw$zzb;->c:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ws;

    iget v0, v0, LX/2ws;->c:I

    if-nez v0, :cond_7

    move v0, v2

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/zzpw$zzb;->a:LX/2wr;

    iget-object v2, v2, LX/2wr;->d:LX/1od;

    iget-object v3, p0, Lcom/google/android/gms/internal/zzpw$zzb;->a:LX/2wr;

    iget-object v3, v3, LX/2wr;->c:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/1od;->a(Landroid/content/Context;)I

    move-result v4

    :cond_0
    if-eqz v4, :cond_4

    if-nez v0, :cond_1

    if-eqz v1, :cond_4

    :cond_1
    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/4 v1, 0x0

    invoke-direct {v0, v4, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/zzpw$zzb;->a:LX/2wr;

    iget-object v1, v1, LX/2wr;->a:LX/2wm;

    new-instance v2, LX/4ub;

    iget-object v3, p0, Lcom/google/android/gms/internal/zzpw$zzb;->a:LX/2wr;

    invoke-direct {v2, p0, v3, v0}, LX/4ub;-><init>(Lcom/google/android/gms/internal/zzpw$zzb;LX/2wq;Lcom/google/android/gms/common/ConnectionResult;)V

    invoke-virtual {v1, v2}, LX/2wm;->a(LX/4uY;)V

    :cond_2
    return-void

    :cond_3
    move v0, v4

    move v1, v3

    :goto_2
    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/zzpw$zzb;->a:LX/2wr;

    iget-boolean v0, v0, LX/2wr;->m:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/internal/zzpw$zzb;->a:LX/2wr;

    iget-object v0, v0, LX/2wr;->k:LX/3KI;

    invoke-interface {v0}, LX/3KI;->g()V

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/zzpw$zzb;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wJ;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzpw$zzb;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2wt;

    invoke-interface {v0}, LX/2wJ;->o()Z

    move-result v3

    if-eqz v3, :cond_6

    if-eqz v4, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/internal/zzpw$zzb;->a:LX/2wr;

    iget-object v0, v0, LX/2wr;->a:LX/2wm;

    new-instance v3, LX/3Ka;

    iget-object v5, p0, Lcom/google/android/gms/internal/zzpw$zzb;->a:LX/2wr;

    invoke-direct {v3, p0, v5, v1}, LX/3Ka;-><init>(Lcom/google/android/gms/internal/zzpw$zzb;LX/2wq;LX/2wt;)V

    invoke-virtual {v0, v3}, LX/2wm;->a(LX/4uY;)V

    goto :goto_3

    :cond_6
    invoke-interface {v0, v1}, LX/2wJ;->a(LX/2wt;)V

    goto :goto_3

    :cond_7
    move v0, v1

    move v1, v2

    goto :goto_2

    :cond_8
    move v2, v3

    move v0, v4

    goto :goto_1
.end method
