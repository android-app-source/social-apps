.class public final Landroid/support/v7/widget/RecyclerView$ViewFlinger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/support/v7/widget/RecyclerView;

.field private b:I

.field private c:I

.field private d:LX/1Og;

.field private e:Landroid/view/animation/Interpolator;

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 241948
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241949
    sget-object v0, Landroid/support/v7/widget/RecyclerView;->an:Landroid/view/animation/Interpolator;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->e:Landroid/view/animation/Interpolator;

    .line 241950
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->f:Z

    .line 241951
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->g:Z

    .line 241952
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/widget/RecyclerView;->an:Landroid/view/animation/Interpolator;

    invoke-static {v0, v1}, LX/1Og;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)LX/1Og;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->d:LX/1Og;

    .line 241953
    return-void
.end method

.method private static a(F)F
    .locals 4

    .prologue
    .line 241945
    const/high16 v0, 0x3f000000    # 0.5f

    sub-float v0, p0, v0

    .line 241946
    float-to-double v0, v0

    const-wide v2, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 241947
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(IIII)V
    .locals 1

    .prologue
    .line 241943
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->b(IIII)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a(III)V

    .line 241944
    return-void
.end method

.method private b(IIII)I
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 241926
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 241927
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 241928
    if-le v2, v3, :cond_0

    const/4 v0, 0x1

    .line 241929
    :goto_0
    mul-int v1, p3, p3

    mul-int v4, p4, p4

    add-int/2addr v1, v4

    int-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 241930
    mul-int v1, p1, p1

    mul-int v5, p2, p2

    add-int/2addr v1, v5

    int-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-int v5, v6

    .line 241931
    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    .line 241932
    :goto_1
    div-int/lit8 v6, v1, 0x2

    .line 241933
    int-to-float v5, v5

    mul-float/2addr v5, v8

    int-to-float v7, v1

    div-float/2addr v5, v7

    invoke-static {v8, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 241934
    int-to-float v7, v6

    int-to-float v6, v6

    invoke-static {v5}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a(F)F

    move-result v5

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    .line 241935
    if-lez v4, :cond_2

    .line 241936
    const/high16 v0, 0x447a0000    # 1000.0f

    int-to-float v1, v4

    div-float v1, v5, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 241937
    :goto_2
    const/16 v1, 0x7d0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    .line 241938
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 241939
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    goto :goto_1

    .line 241940
    :cond_2
    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    int-to-float v0, v0

    .line 241941
    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v8

    const/high16 v1, 0x43960000    # 300.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_2

    :cond_3
    move v0, v3

    .line 241942
    goto :goto_3
.end method

.method private c()V
    .locals 1

    .prologue
    .line 241923
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->g:Z

    .line 241924
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->f:Z

    .line 241925
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 241919
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->f:Z

    .line 241920
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->g:Z

    if-eqz v0, :cond_0

    .line 241921
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a()V

    .line 241922
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 241810
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->f:Z

    if-eqz v0, :cond_0

    .line 241811
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->g:Z

    .line 241812
    :goto_0
    return-void

    .line 241813
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 241814
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 9

    .prologue
    const v6, 0x7fffffff

    const/high16 v5, -0x80000000

    const/4 v1, 0x0

    .line 241914
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setScrollState(Landroid/support/v7/widget/RecyclerView;I)V

    .line 241915
    iput v1, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->c:I

    iput v1, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->b:I

    .line 241916
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->d:LX/1Og;

    move v2, v1

    move v3, p1

    move v4, p2

    move v7, v5

    move v8, v6

    invoke-virtual/range {v0 .. v8}, LX/1Og;->a(IIIIIIII)V

    .line 241917
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a()V

    .line 241918
    return-void
.end method

.method public final a(III)V
    .locals 1

    .prologue
    .line 241912
    sget-object v0, Landroid/support/v7/widget/RecyclerView;->an:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a(IIILandroid/view/animation/Interpolator;)V

    .line 241913
    return-void
.end method

.method public final a(IIILandroid/view/animation/Interpolator;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 241904
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->e:Landroid/view/animation/Interpolator;

    if-eq v0, p4, :cond_0

    .line 241905
    iput-object p4, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->e:Landroid/view/animation/Interpolator;

    .line 241906
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p4}, LX/1Og;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)LX/1Og;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->d:LX/1Og;

    .line 241907
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setScrollState(Landroid/support/v7/widget/RecyclerView;I)V

    .line 241908
    iput v1, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->c:I

    iput v1, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->b:I

    .line 241909
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->d:LX/1Og;

    move v2, v1

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, LX/1Og;->a(IIIII)V

    .line 241910
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a()V

    .line 241911
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 241901
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 241902
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->d:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->h()V

    .line 241903
    return-void
.end method

.method public final b(II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 241899
    invoke-direct {p0, p1, p2, v0, v0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a(IIII)V

    .line 241900
    return-void
.end method

.method public final run()V
    .locals 23

    .prologue
    const/16 v4, 0x8

    const/16 v5, 0x1e

    const v6, 0x18762c10    # 3.1817E-24f

    invoke-static {v4, v5, v6}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 241815
    invoke-direct/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->c()V

    .line 241816
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/support/v7/widget/RecyclerView;)V

    .line 241817
    move-object/from16 v0, p0

    iget-object v12, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->d:LX/1Og;

    .line 241818
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v13, v4, LX/1OR;->s:LX/25Y;

    .line 241819
    invoke-virtual {v12}, LX/1Og;->g()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 241820
    invoke-virtual {v12}, LX/1Og;->b()I

    move-result v14

    .line 241821
    invoke-virtual {v12}, LX/1Og;->c()I

    move-result v15

    .line 241822
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->b:I

    sub-int v16, v14, v4

    .line 241823
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->c:I

    sub-int v17, v15, v4

    .line 241824
    const/4 v7, 0x0

    .line 241825
    const/4 v5, 0x0

    .line 241826
    move-object/from16 v0, p0

    iput v14, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->b:I

    .line 241827
    move-object/from16 v0, p0

    iput v15, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->c:I

    .line 241828
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 241829
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v8, v8, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v8, :cond_18

    .line 241830
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v8}, Landroid/support/v7/widget/RecyclerView;->b()V

    .line 241831
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v8}, Landroid/support/v7/widget/RecyclerView;->x(Landroid/support/v7/widget/RecyclerView;)V

    .line 241832
    const-string v8, "RV Scroll"

    const v9, -0x60f5b164

    invoke-static {v8, v9}, LX/03q;->a(Ljava/lang/String;I)V

    .line 241833
    if-eqz v16, :cond_0

    .line 241834
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v6, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v7, v7, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v8, v8, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    move/from16 v0, v16

    invoke-virtual {v6, v0, v7, v8}, LX/1OR;->a(ILX/1Od;LX/1Ok;)I

    move-result v7

    .line 241835
    sub-int v6, v16, v7

    .line 241836
    :cond_0
    if-eqz v17, :cond_1

    .line 241837
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v8, v8, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    move/from16 v0, v17

    invoke-virtual {v4, v0, v5, v8}, LX/1OR;->b(ILX/1Od;LX/1Ok;)I

    move-result v5

    .line 241838
    sub-int v4, v17, v5

    .line 241839
    :cond_1
    const v8, -0x21637ebb

    invoke-static {v8}, LX/03q;->a(I)V

    .line 241840
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v8}, Landroid/support/v7/widget/RecyclerView;->A(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 241841
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v8, v8, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v8}, LX/1Os;->b()I

    move-result v9

    .line 241842
    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_4

    .line 241843
    move-object/from16 v0, p0

    iget-object v10, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v10, v10, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v10, v8}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v10

    .line 241844
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v18

    .line 241845
    if-eqz v18, :cond_3

    move-object/from16 v0, v18

    iget-object v0, v0, LX/1a1;->h:LX/1a1;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    .line 241846
    move-object/from16 v0, v18

    iget-object v0, v0, LX/1a1;->h:LX/1a1;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    move-object/from16 v18, v0

    .line 241847
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v19

    .line 241848
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v10

    .line 241849
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLeft()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getTop()I

    move-result v20

    move/from16 v0, v20

    if-eq v10, v0, :cond_3

    .line 241850
    :cond_2
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getWidth()I

    move-result v20

    add-int v20, v20, v19

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getHeight()I

    move-result v21

    add-int v21, v21, v10

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v10, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 241851
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 241852
    :cond_4
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v8}, Landroid/support/v7/widget/RecyclerView;->y(Landroid/support/v7/widget/RecyclerView;)V

    .line 241853
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 241854
    if-eqz v13, :cond_18

    invoke-virtual {v13}, LX/25Y;->f()Z

    move-result v8

    if-nez v8, :cond_18

    invoke-virtual {v13}, LX/25Y;->g()Z

    move-result v8

    if-eqz v8, :cond_18

    .line 241855
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v8, v8, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v8}, LX/1Ok;->e()I

    move-result v8

    .line 241856
    if-nez v8, :cond_16

    .line 241857
    invoke-virtual {v13}, LX/25Y;->e()V

    move/from16 v22, v6

    move v6, v5

    move/from16 v5, v22

    .line 241858
    :goto_1
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v8, v8, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_5

    .line 241859
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v8}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 241860
    :cond_5
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v8}, LX/0vv;->a(Landroid/view/View;)I

    move-result v8

    const/4 v9, 0x2

    if-eq v8, v9, :cond_6

    .line 241861
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v8, v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 241862
    :cond_6
    if-nez v5, :cond_7

    if-eqz v4, :cond_c

    .line 241863
    :cond_7
    invoke-virtual {v12}, LX/1Og;->f()F

    move-result v8

    float-to-int v9, v8

    .line 241864
    const/4 v8, 0x0

    .line 241865
    if-eq v5, v14, :cond_21

    .line 241866
    if-gez v5, :cond_19

    neg-int v8, v9

    :goto_2
    move v10, v8

    .line 241867
    :goto_3
    const/4 v8, 0x0

    .line 241868
    if-eq v4, v15, :cond_20

    .line 241869
    if-gez v4, :cond_1b

    neg-int v9, v9

    .line 241870
    :cond_8
    :goto_4
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v8}, LX/0vv;->a(Landroid/view/View;)I

    move-result v8

    const/16 v18, 0x2

    move/from16 v0, v18

    if-eq v8, v0, :cond_9

    .line 241871
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v8, v10, v9}, Landroid/support/v7/widget/RecyclerView;->c(II)V

    .line 241872
    :cond_9
    if-nez v10, :cond_a

    if-eq v5, v14, :cond_a

    invoke-virtual {v12}, LX/1Og;->d()I

    move-result v5

    if-nez v5, :cond_c

    :cond_a
    if-nez v9, :cond_b

    if-eq v4, v15, :cond_b

    invoke-virtual {v12}, LX/1Og;->e()I

    move-result v4

    if-nez v4, :cond_c

    .line 241873
    :cond_b
    invoke-virtual {v12}, LX/1Og;->h()V

    .line 241874
    :cond_c
    if-nez v7, :cond_d

    if-eqz v6, :cond_e

    .line 241875
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v7, v6}, Landroid/support/v7/widget/RecyclerView;->f(II)V

    .line 241876
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->k(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 241877
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 241878
    :cond_f
    if-eqz v17, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v4}, LX/1OR;->h()Z

    move-result v4

    if-eqz v4, :cond_1c

    move/from16 v0, v17

    if-ne v6, v0, :cond_1c

    const/4 v4, 0x1

    move v5, v4

    .line 241879
    :goto_5
    if-eqz v16, :cond_1d

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v4}, LX/1OR;->g()Z

    move-result v4

    if-eqz v4, :cond_1d

    move/from16 v0, v16

    if-ne v7, v0, :cond_1d

    const/4 v4, 0x1

    .line 241880
    :goto_6
    if-nez v16, :cond_10

    if-eqz v17, :cond_11

    :cond_10
    if-nez v4, :cond_11

    if-eqz v5, :cond_1e

    :cond_11
    const/4 v4, 0x1

    .line 241881
    :goto_7
    invoke-virtual {v12}, LX/1Og;->a()Z

    move-result v5

    if-nez v5, :cond_12

    if-nez v4, :cond_1f

    .line 241882
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setScrollState(Landroid/support/v7/widget/RecyclerView;I)V

    .line 241883
    :cond_13
    :goto_8
    if-eqz v13, :cond_15

    .line 241884
    invoke-virtual {v13}, LX/25Y;->f()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 241885
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v13, v4, v5}, LX/25Y;->a(LX/25Y;II)V

    .line 241886
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v4, v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->g:Z

    if-nez v4, :cond_15

    .line 241887
    invoke-virtual {v13}, LX/25Y;->e()V

    .line 241888
    :cond_15
    invoke-direct/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->d()V

    .line 241889
    const v4, 0x4485f98b

    invoke-static {v4, v11}, LX/02F;->h(II)V

    return-void

    .line 241890
    :cond_16
    invoke-virtual {v13}, LX/25Y;->h()I

    move-result v9

    if-lt v9, v8, :cond_17

    .line 241891
    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v13, v8}, LX/25Y;->c(I)V

    .line 241892
    :cond_17
    sub-int v8, v16, v6

    sub-int v9, v17, v4

    invoke-static {v13, v8, v9}, LX/25Y;->a(LX/25Y;II)V

    :cond_18
    move/from16 v22, v6

    move v6, v5

    move/from16 v5, v22

    goto/16 :goto_1

    .line 241893
    :cond_19
    if-lez v5, :cond_1a

    move v8, v9

    goto/16 :goto_2

    :cond_1a
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 241894
    :cond_1b
    if-gtz v4, :cond_8

    const/4 v9, 0x0

    goto/16 :goto_4

    .line 241895
    :cond_1c
    const/4 v4, 0x0

    move v5, v4

    goto :goto_5

    .line 241896
    :cond_1d
    const/4 v4, 0x0

    goto :goto_6

    .line 241897
    :cond_1e
    const/4 v4, 0x0

    goto :goto_7

    .line 241898
    :cond_1f
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a()V

    goto :goto_8

    :cond_20
    move v9, v8

    goto/16 :goto_4

    :cond_21
    move v10, v8

    goto/16 :goto_3
.end method
