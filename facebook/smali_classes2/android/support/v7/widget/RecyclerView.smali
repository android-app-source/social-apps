.class public Landroid/support/v7/widget/RecyclerView;
.super Landroid/view/ViewGroup;
.source ""

# interfaces
.implements LX/1O9;
.implements LX/1OA;


# static fields
.field public static final an:Landroid/view/animation/Interpolator;

.field public static final h:Z

.field private static final i:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:Z

.field private B:I

.field public C:Z

.field public final D:Z

.field private final E:Landroid/view/accessibility/AccessibilityManager;

.field private F:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3x8;",
            ">;"
        }
    .end annotation
.end field

.field public G:Z

.field private H:I

.field private I:LX/0vj;

.field private J:LX/0vj;

.field private K:LX/0vj;

.field private L:LX/0vj;

.field public M:I

.field private N:I

.field private O:Landroid/view/VelocityTracker;

.field private P:I

.field private Q:I

.field private R:I

.field private S:I

.field private T:I

.field public final U:I

.field public final V:I

.field private W:F

.field public final a:LX/1Od;

.field public final aa:Landroid/support/v7/widget/RecyclerView$ViewFlinger;

.field private ab:LX/1OX;

.field private ac:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1OX;",
            ">;"
        }
    .end annotation
.end field

.field private ad:LX/1Om;

.field public ae:Z

.field public af:LX/1Ow;

.field private ag:LX/3x4;

.field private final ah:[I

.field private final ai:LX/1Oy;

.field private final aj:[I

.field private final ak:[I

.field private final al:[I

.field private am:Ljava/lang/Runnable;

.field public b:LX/1On;

.field public c:LX/1Os;

.field public d:LX/1Of;

.field public final e:LX/1Ok;

.field public f:Z

.field public g:Z

.field private final j:LX/1Oc;

.field private k:Landroid/support/v7/widget/RecyclerView$SavedState;

.field public l:Z

.field public final m:Ljava/lang/Runnable;

.field private final n:Landroid/graphics/Rect;

.field public o:LX/1OM;

.field public p:LX/1OR;

.field public q:LX/1OU;

.field public final r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3x6;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1OH;",
            ">;"
        }
    .end annotation
.end field

.field private t:LX/1OH;

.field public u:Z

.field public v:Z

.field public w:Z

.field private x:Z

.field public y:Z

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 240385
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-eq v0, v3, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-eq v0, v3, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x14

    if-ne v0, v3, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->h:Z

    .line 240386
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v0, v1

    const-class v1, Landroid/util/AttributeSet;

    aput-object v1, v0, v2

    const/4 v1, 0x2

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->i:[Ljava/lang/Class;

    .line 240387
    new-instance v0, LX/1Ob;

    invoke-direct {v0}, LX/1Ob;-><init>()V

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->an:Landroid/view/animation/Interpolator;

    return-void

    :cond_1
    move v0, v1

    .line 240388
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 239932
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 239933
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 239934
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 239935
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 239936
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 239937
    new-instance v0, LX/1Oc;

    invoke-direct {v0, p0}, LX/1Oc;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->j:LX/1Oc;

    .line 239938
    new-instance v0, LX/1Od;

    invoke-direct {v0, p0}, LX/1Od;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    .line 239939
    new-instance v0, Landroid/support/v7/widget/RecyclerView$1;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/RecyclerView$1;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Ljava/lang/Runnable;

    .line 239940
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    .line 239941
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    .line 239942
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    .line 239943
    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->G:Z

    .line 239944
    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->H:I

    .line 239945
    new-instance v0, LX/1Oe;

    invoke-direct {v0}, LX/1Oe;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    .line 239946
    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    .line 239947
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 239948
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->W:F

    .line 239949
    new-instance v0, Landroid/support/v7/widget/RecyclerView$ViewFlinger;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Landroid/support/v7/widget/RecyclerView$ViewFlinger;

    .line 239950
    new-instance v0, LX/1Ok;

    invoke-direct {v0}, LX/1Ok;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239951
    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->f:Z

    .line 239952
    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->g:Z

    .line 239953
    new-instance v0, LX/1Ol;

    invoke-direct {v0, p0}, LX/1Ol;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ad:LX/1Om;

    .line 239954
    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->ae:Z

    .line 239955
    new-array v0, v2, [I

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ah:[I

    .line 239956
    new-array v0, v2, [I

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    .line 239957
    new-array v0, v2, [I

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ak:[I

    .line 239958
    new-array v0, v2, [I

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    .line 239959
    new-instance v0, Landroid/support/v7/widget/RecyclerView$2;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/RecyclerView$2;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->am:Ljava/lang/Runnable;

    .line 239960
    invoke-virtual {p0, v6}, Landroid/support/v7/widget/RecyclerView;->setScrollContainer(Z)V

    .line 239961
    invoke-virtual {p0, v6}, Landroid/support/v7/widget/RecyclerView;->setFocusableInTouchMode(Z)V

    .line 239962
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 239963
    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    move v0, v6

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    .line 239964
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 239965
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    .line 239966
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->U:I

    .line 239967
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    .line 239968
    invoke-static {p0}, LX/0vv;->a(Landroid/view/View;)I

    move-result v0

    if-ne v0, v2, :cond_3

    move v0, v6

    :goto_1
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    .line 239969
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->ad:LX/1Om;

    .line 239970
    iput-object v1, v0, LX/1Of;->a:LX/1Om;

    .line 239971
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->m()V

    .line 239972
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->l()V

    .line 239973
    invoke-static {p0}, LX/0vv;->e(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    .line 239974
    invoke-static {p0, v6}, LX/0vv;->d(Landroid/view/View;I)V

    .line 239975
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/view/accessibility/AccessibilityManager;

    .line 239976
    new-instance v0, LX/1Ow;

    invoke-direct {v0, p0}, LX/1Ow;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAccessibilityDelegateCompat(LX/1Ow;)V

    .line 239977
    if-eqz p2, :cond_1

    .line 239978
    sget-object v0, LX/03r;->RecyclerView:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 239979
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 239980
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    .line 239981
    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;II)V

    .line 239982
    :cond_1
    new-instance v0, LX/1Oy;

    invoke-direct {v0, p0}, LX/1Oy;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:LX/1Oy;

    .line 239983
    invoke-virtual {p0, v6}, Landroid/support/v7/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 239984
    return-void

    :cond_2
    move v0, v5

    .line 239985
    goto :goto_0

    :cond_3
    move v0, v5

    .line 239986
    goto :goto_1
.end method

.method public static A(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 239987
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    .line 239988
    iget-boolean p0, v0, LX/1Of;->g:Z

    move v0, p0

    .line 239989
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private B()V
    .locals 1

    .prologue
    .line 239990
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->ae:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    if-eqz v0, :cond_0

    .line 239991
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->am:Ljava/lang/Runnable;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 239992
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->ae:Z

    .line 239993
    :cond_0
    return-void
.end method

.method private C()Z
    .locals 1

    .prologue
    .line 239994
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private D()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 239995
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Z

    if-eqz v0, :cond_0

    .line 239996
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0}, LX/1On;->a()V

    .line 239997
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->I()V

    .line 239998
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->a()V

    .line 239999
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 240000
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0}, LX/1On;->b()V

    .line 240001
    :goto_0
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Z

    if-eqz v0, :cond_6

    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->A(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    move v0, v2

    .line 240002
    :goto_1
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    if-eqz v3, :cond_7

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v3, :cond_7

    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->G:Z

    if-nez v3, :cond_3

    if-nez v0, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-boolean v3, v3, LX/1OR;->a:Z

    if-eqz v3, :cond_7

    :cond_3
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->G:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v3}, LX/1OM;->eC_()Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_4
    move v3, v2

    .line 240003
    :goto_2
    iput-boolean v3, v4, LX/1Ok;->l:Z

    .line 240004
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v4, v4, LX/1Ok;->l:Z

    if-eqz v4, :cond_8

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Z

    if-nez v0, :cond_8

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->C()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 240005
    :goto_3
    iput-boolean v2, v3, LX/1Ok;->m:Z

    .line 240006
    return-void

    .line 240007
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0}, LX/1On;->e()V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 240008
    goto :goto_1

    :cond_7
    move v3, v1

    .line 240009
    goto :goto_2

    :cond_8
    move v2, v1

    .line 240010
    goto :goto_3
.end method

.method private E()V
    .locals 4

    .prologue
    .line 240011
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->c()I

    move-result v2

    .line 240012
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 240013
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0, v1}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v0

    .line 240014
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    const/4 v3, 0x1

    iput-boolean v3, v0, LX/1a3;->c:Z

    .line 240015
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 240016
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0}, LX/1Od;->i()V

    .line 240017
    return-void
.end method

.method private F()V
    .locals 4

    .prologue
    .line 240018
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->c()I

    move-result v1

    .line 240019
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 240020
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v2, v0}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v2

    .line 240021
    invoke-virtual {v2}, LX/1a1;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 240022
    invoke-virtual {v2}, LX/1a1;->mb_()V

    .line 240023
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240024
    :cond_1
    return-void
.end method

.method private G()V
    .locals 4

    .prologue
    .line 239921
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->c()I

    move-result v1

    .line 239922
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 239923
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v2, v0}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v2

    .line 239924
    invoke-virtual {v2}, LX/1a1;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 239925
    invoke-virtual {v2}, LX/1a1;->a()V

    .line 239926
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239927
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0}, LX/1Od;->h()V

    .line 239928
    return-void
.end method

.method public static H(Landroid/support/v7/widget/RecyclerView;)V
    .locals 4

    .prologue
    .line 240034
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Z

    if-eqz v0, :cond_0

    .line 240035
    :goto_0
    return-void

    .line 240036
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Z

    .line 240037
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->c()I

    move-result v1

    .line 240038
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    .line 240039
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v2, v0}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v2

    .line 240040
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/1a1;->c()Z

    move-result v3

    if-nez v3, :cond_1

    .line 240041
    const/16 v3, 0x200

    invoke-virtual {v2, v3}, LX/1a1;->b(I)V

    .line 240042
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 240043
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0}, LX/1Od;->f()V

    goto :goto_0
.end method

.method private I()V
    .locals 4

    .prologue
    .line 240044
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->c()I

    move-result v1

    .line 240045
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 240046
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v2, v0}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v2

    .line 240047
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/1a1;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 240048
    const/4 v3, 0x6

    invoke-virtual {v2, v3}, LX/1a1;->b(I)V

    .line 240049
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240050
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->E()V

    .line 240051
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0}, LX/1Od;->g()V

    .line 240052
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x2e

    .line 240053
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v2, :cond_1

    .line 240054
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 240055
    :cond_0
    :goto_0
    return-object p1

    .line 240056
    :cond_1
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 240057
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private a(FFFF)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 240058
    const/4 v1, 0x0

    .line 240059
    cmpg-float v2, p2, v5

    if-gez v2, :cond_4

    .line 240060
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->q()V

    .line 240061
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    neg-float v3, p2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v4, p3, v4

    sub-float v4, v6, v4

    invoke-virtual {v2, v3, v4}, LX/0vj;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    .line 240062
    :cond_0
    :goto_0
    cmpg-float v2, p4, v5

    if-gez v2, :cond_5

    .line 240063
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->s()V

    .line 240064
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    neg-float v3, p4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v4, p1, v4

    invoke-virtual {v2, v3, v4}, LX/0vj;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 240065
    :cond_1
    :goto_1
    if-nez v0, :cond_2

    cmpl-float v0, p2, v5

    if-nez v0, :cond_2

    cmpl-float v0, p4, v5

    if-eqz v0, :cond_3

    .line 240066
    :cond_2
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 240067
    :cond_3
    return-void

    .line 240068
    :cond_4
    cmpl-float v2, p2, v5

    if-lez v2, :cond_0

    .line 240069
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->r()V

    .line 240070
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v3, p2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v4, p3, v4

    invoke-virtual {v2, v3, v4}, LX/0vj;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    .line 240071
    goto :goto_0

    .line 240072
    :cond_5
    cmpl-float v2, p4, v5

    if-lez v2, :cond_6

    .line 240073
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->t()V

    .line 240074
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v3, p4, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v4, p1, v4

    sub-float v4, v6, v4

    invoke-virtual {v2, v3, v4}, LX/0vj;->a(FF)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private a(LX/026;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/026",
            "<",
            "Landroid/view/View;",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240075
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v7, v0, LX/1Ok;->d:Ljava/util/List;

    .line 240076
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v6, v0

    :goto_0
    if-ltz v6, :cond_3

    .line 240077
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/View;

    .line 240078
    invoke-static {v5}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v1

    .line 240079
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->a:LX/026;

    invoke-virtual {v0, v1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3x7;

    .line 240080
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 240081
    iget-boolean v3, v2, LX/1Ok;->k:Z

    move v2, v3

    .line 240082
    if-nez v2, :cond_0

    .line 240083
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v2, v2, LX/1Ok;->b:LX/026;

    invoke-virtual {v2, v1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 240084
    :cond_0
    invoke-virtual {p1, v5}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 240085
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, v5, v1}, LX/1OR;->a(Landroid/view/View;LX/1Od;)V

    .line 240086
    :goto_1
    add-int/lit8 v0, v6, -0x1

    move v6, v0

    goto :goto_0

    .line 240087
    :cond_1
    if-eqz v0, :cond_2

    .line 240088
    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x7;)V

    goto :goto_1

    .line 240089
    :cond_2
    new-instance v0, LX/3x7;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/3x7;-><init>(LX/1a1;IIII)V

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x7;)V

    goto :goto_1

    .line 240090
    :cond_3
    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 240091
    return-void
.end method

.method private a(LX/1OM;ZZ)V
    .locals 3

    .prologue
    .line 240092
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v0, :cond_0

    .line 240093
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->j:LX/1Oc;

    invoke-virtual {v0, v1}, LX/1OM;->b(LX/1OD;)V

    .line 240094
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0, p0}, LX/1OM;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 240095
    :cond_0
    if-eqz p2, :cond_1

    if-eqz p3, :cond_4

    .line 240096
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v0, :cond_2

    .line 240097
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    invoke-virtual {v0}, LX/1Of;->c()V

    .line 240098
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_3

    .line 240099
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, v1}, LX/1OR;->c(LX/1Od;)V

    .line 240100
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, v1}, LX/1OR;->b(LX/1Od;)V

    .line 240101
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0}, LX/1Od;->a()V

    .line 240102
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0}, LX/1On;->a()V

    .line 240103
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    .line 240104
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    .line 240105
    if-eqz p1, :cond_5

    .line 240106
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->j:LX/1Oc;

    invoke-virtual {p1, v1}, LX/1OM;->a(LX/1OD;)V

    .line 240107
    invoke-virtual {p1, p0}, LX/1OM;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 240108
    :cond_5
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v1, :cond_6

    .line 240109
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v1, v0, v2}, LX/1OR;->a(LX/1OM;LX/1OM;)V

    .line 240110
    :cond_6
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v1, v0, v2, p2}, LX/1Od;->a(LX/1OM;LX/1OM;Z)V

    .line 240111
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    const/4 v1, 0x1

    .line 240112
    iput-boolean v1, v0, LX/1Ok;->j:Z

    .line 240113
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->I()V

    .line 240114
    return-void
.end method

.method private a(LX/1a1;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 240115
    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    .line 240116
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    move v0, v1

    .line 240117
    :goto_0
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1Od;->b(LX/1a1;)V

    .line 240118
    invoke-virtual {p1}, LX/1a1;->r()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 240119
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    const/4 v3, -0x1

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4, v1}, LX/1Os;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    .line 240120
    :goto_1
    return-void

    .line 240121
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 240122
    :cond_1
    if-nez v0, :cond_2

    .line 240123
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    .line 240124
    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3, v1}, LX/1Os;->a(Landroid/view/View;IZ)V

    .line 240125
    goto :goto_1

    .line 240126
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0, v2}, LX/1Os;->d(Landroid/view/View;)V

    goto :goto_1
.end method

.method private a(LX/1a1;LX/1a1;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 240127
    invoke-virtual {p1, v1}, LX/1a1;->a(Z)V

    .line 240128
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1a1;)V

    .line 240129
    iput-object p2, p1, LX/1a1;->g:LX/1a1;

    .line 240130
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, p1}, LX/1Od;->b(LX/1a1;)V

    .line 240131
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 240132
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    .line 240133
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/1a1;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v6, v4

    move v5, v3

    .line 240134
    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/1Of;->a(LX/1a1;LX/1a1;IIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240135
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->B()V

    .line 240136
    :cond_1
    return-void

    .line 240137
    :cond_2
    iget-object v0, p2, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    .line 240138
    iget-object v0, p2, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    .line 240139
    invoke-virtual {p2, v1}, LX/1a1;->a(Z)V

    .line 240140
    iput-object p1, p2, LX/1a1;->h:LX/1a1;

    goto :goto_0
.end method

.method private a(LX/1a1;Landroid/graphics/Rect;II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 240141
    if-eqz p2, :cond_2

    iget v0, p2, Landroid/graphics/Rect;->left:I

    if-ne v0, p3, :cond_0

    iget v0, p2, Landroid/graphics/Rect;->top:I

    if-eq v0, p4, :cond_2

    .line 240142
    :cond_0
    invoke-virtual {p1, v1}, LX/1a1;->a(Z)V

    .line 240143
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    iget v2, p2, Landroid/graphics/Rect;->left:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    move-object v1, p1

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/1Of;->a(LX/1a1;IIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240144
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->B()V

    .line 240145
    :cond_1
    :goto_0
    return-void

    .line 240146
    :cond_2
    invoke-virtual {p1, v1}, LX/1a1;->a(Z)V

    .line 240147
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    invoke-virtual {v0, p1}, LX/1Of;->b(LX/1a1;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240148
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->B()V

    goto :goto_0
.end method

.method private a(LX/3x6;I)V
    .locals 2

    .prologue
    .line 240149
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_0

    .line 240150
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    const-string v1, "Cannot add item decoration during a scroll  or layout"

    invoke-virtual {v0, v1}, LX/1OR;->a(Ljava/lang/String;)V

    .line 240151
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240152
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    .line 240153
    :cond_1
    if-gez p2, :cond_2

    .line 240154
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240155
    :goto_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->E()V

    .line 240156
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 240157
    return-void

    .line 240158
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private a(LX/3x7;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 239842
    iget-object v0, p1, LX/3x7;->a:LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    .line 239843
    iget-object v1, p1, LX/3x7;->a:LX/1a1;

    invoke-direct {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1a1;)V

    .line 239844
    iget v2, p1, LX/3x7;->b:I

    .line 239845
    iget v3, p1, LX/3x7;->c:I

    .line 239846
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 239847
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    .line 239848
    iget-object v1, p1, LX/3x7;->a:LX/1a1;

    invoke-virtual {v1}, LX/1a1;->q()Z

    move-result v1

    if-nez v1, :cond_2

    if-ne v2, v4, :cond_0

    if-eq v3, v5, :cond_2

    .line 239849
    :cond_0
    iget-object v1, p1, LX/3x7;->a:LX/1a1;

    invoke-virtual {v1, v6}, LX/1a1;->a(Z)V

    .line 239850
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v1, v4

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v5

    invoke-virtual {v0, v4, v5, v1, v6}, Landroid/view/View;->layout(IIII)V

    .line 239851
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    iget-object v1, p1, LX/3x7;->a:LX/1a1;

    invoke-virtual/range {v0 .. v5}, LX/1Of;->a(LX/1a1;IIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239852
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->B()V

    .line 239853
    :cond_1
    :goto_0
    return-void

    .line 239854
    :cond_2
    iget-object v0, p1, LX/3x7;->a:LX/1a1;

    invoke-virtual {v0, v6}, LX/1a1;->a(Z)V

    .line 239855
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    iget-object v1, p1, LX/3x7;->a:LX/1a1;

    invoke-virtual {v0, v1}, LX/1Of;->a(LX/1a1;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239856
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->B()V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;II)V
    .locals 7

    .prologue
    .line 239718
    if-eqz p2, :cond_0

    .line 239719
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 239720
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 239721
    invoke-static {p1, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 239722
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239723
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 239724
    :goto_0
    invoke-virtual {v0, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, LX/1OR;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_6

    move-result-object v4

    .line 239725
    const/4 v2, 0x0

    .line 239726
    :try_start_1
    sget-object v0, Landroid/support/v7/widget/RecyclerView;->i:[Ljava/lang/Class;

    invoke-virtual {v4, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 239727
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v1, v5

    const/4 v5, 0x1

    aput-object p3, v1, v5

    const/4 v5, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v5

    const/4 v5, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v5
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_6

    .line 239728
    :goto_1
    const/4 v2, 0x1

    :try_start_2
    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 239729
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OR;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 239730
    :cond_0
    return-void

    .line 239731
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_6

    move-result-object v0

    goto :goto_0

    .line 239732
    :catch_0
    move-exception v0

    .line 239733
    const/4 v1, 0x0

    :try_start_3
    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v4, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_6

    move-result-object v0

    move-object v1, v2

    .line 239734
    goto :goto_1

    .line 239735
    :catch_1
    move-exception v1

    .line 239736
    :try_start_4
    invoke-virtual {v1, v0}, Ljava/lang/NoSuchMethodException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 239737
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": Error creating LayoutManager "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_4} :catch_6

    .line 239738
    :catch_2
    move-exception v0

    .line 239739
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": Unable to find LayoutManager "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 239740
    :catch_3
    move-exception v0

    .line 239741
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": Could not instantiate the LayoutManager: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 239742
    :catch_4
    move-exception v0

    .line 239743
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": Could not instantiate the LayoutManager: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 239744
    :catch_5
    move-exception v0

    .line 239745
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": Cannot access non-public constructor "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 239746
    :catch_6
    move-exception v0

    .line 239747
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": Class is not a LayoutManager "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static synthetic a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    .prologue
    .line 239748
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->detachViewFromParent(I)V

    return-void
.end method

.method public static synthetic a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 239749
    invoke-static {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;II)V

    return-void
.end method

.method public static synthetic a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 239750
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a([I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 239751
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v5

    .line 239752
    if-nez v5, :cond_0

    .line 239753
    aput v4, p1, v4

    .line 239754
    aput v4, p1, v7

    .line 239755
    :goto_0
    return-void

    .line 239756
    :cond_0
    const v2, 0x7fffffff

    .line 239757
    const/high16 v1, -0x80000000

    move v3, v4

    .line 239758
    :goto_1
    if-ge v3, v5, :cond_2

    .line 239759
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0, v3}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 239760
    invoke-virtual {v0}, LX/1a1;->c()Z

    move-result v6

    if-nez v6, :cond_3

    .line 239761
    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v0

    .line 239762
    if-ge v0, v2, :cond_1

    move v2, v0

    .line 239763
    :cond_1
    if-le v0, v1, :cond_3

    move v1, v2

    .line 239764
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 239765
    :cond_2
    aput v2, p1, v4

    .line 239766
    aput v1, p1, v7

    goto :goto_0

    :cond_3
    move v0, v1

    move v1, v2

    goto :goto_2
.end method

.method private a(IILandroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    .line 239767
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 239768
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 239769
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->n(Landroid/support/v7/widget/RecyclerView;)V

    .line 239770
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v4, :cond_6

    .line 239771
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->b()V

    .line 239772
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->x(Landroid/support/v7/widget/RecyclerView;)V

    .line 239773
    const-string v4, "RV Scroll"

    const v5, 0x47128314

    invoke-static {v4, v5}, LX/03q;->a(Ljava/lang/String;I)V

    .line 239774
    if-eqz p1, :cond_0

    .line 239775
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v2, p1, v3, v4}, LX/1OR;->a(ILX/1Od;LX/1Ok;)I

    move-result v2

    .line 239776
    sub-int v3, p1, v2

    .line 239777
    :cond_0
    if-eqz p2, :cond_1

    .line 239778
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, p2, v1, v4}, LX/1OR;->b(ILX/1Od;LX/1Ok;)I

    move-result v0

    .line 239779
    sub-int v1, p2, v0

    .line 239780
    :cond_1
    const v4, -0x7de40f4b

    invoke-static {v4}, LX/03q;->a(I)V

    .line 239781
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->A(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 239782
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v4}, LX/1Os;->b()I

    move-result v6

    .line 239783
    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v6, :cond_5

    .line 239784
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v4, v5}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v7

    .line 239785
    invoke-virtual {p0, v7}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v4

    .line 239786
    if-eqz v4, :cond_3

    iget-object v8, v4, LX/1a1;->h:LX/1a1;

    if-eqz v8, :cond_3

    .line 239787
    iget-object v4, v4, LX/1a1;->h:LX/1a1;

    .line 239788
    if-eqz v4, :cond_4

    iget-object v4, v4, LX/1a1;->a:Landroid/view/View;

    .line 239789
    :goto_1
    if-eqz v4, :cond_3

    .line 239790
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v8

    .line 239791
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v7

    .line 239792
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v9

    if-ne v8, v9, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v9

    if-eq v7, v9, :cond_3

    .line 239793
    :cond_2
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v9, v8

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v10

    add-int/2addr v10, v7

    invoke-virtual {v4, v8, v7, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 239794
    :cond_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 239795
    :cond_4
    const/4 v4, 0x0

    goto :goto_1

    .line 239796
    :cond_5
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->y(Landroid/support/v7/widget/RecyclerView;)V

    .line 239797
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    :cond_6
    move v4, v1

    move v1, v2

    move v2, v0

    .line 239798
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 239799
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 239800
    :cond_7
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/RecyclerView;->dispatchNestedScroll(IIII[I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 239801
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    sub-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    .line 239802
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    sub-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    .line 239803
    if-eqz p3, :cond_8

    .line 239804
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    const/4 v3, 0x0

    aget v0, v0, v3

    int-to-float v0, v0

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-virtual {p3, v0, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 239805
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    const/4 v3, 0x0

    aget v4, v0, v3

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    add-int/2addr v4, v5

    aput v4, v0, v3

    .line 239806
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    const/4 v3, 0x1

    aget v4, v0, v3

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    add-int/2addr v4, v5

    aput v4, v0, v3

    .line 239807
    :cond_9
    :goto_2
    if-nez v1, :cond_a

    if-eqz v2, :cond_b

    .line 239808
    :cond_a
    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/RecyclerView;->f(II)V

    .line 239809
    :cond_b
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    move-result v0

    if-nez v0, :cond_c

    .line 239810
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 239811
    :cond_c
    if-nez v1, :cond_d

    if-eqz v2, :cond_10

    :cond_d
    const/4 v0, 0x1

    :goto_3
    return v0

    .line 239812
    :cond_e
    invoke-static {p0}, LX/0vv;->a(Landroid/view/View;)I

    move-result v0

    const/4 v5, 0x2

    if-eq v0, v5, :cond_9

    .line 239813
    if-eqz p3, :cond_f

    .line 239814
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    int-to-float v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    int-to-float v4, v4

    invoke-direct {p0, v0, v3, v5, v4}, Landroid/support/v7/widget/RecyclerView;->a(FFFF)V

    .line 239815
    :cond_f
    invoke-static {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;II)V

    goto :goto_2

    .line 239816
    :cond_10
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v1, 0x0

    .line 239817
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 239818
    if-eq v3, v6, :cond_0

    if-nez v3, :cond_1

    .line 239819
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:LX/1OH;

    .line 239820
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    .line 239821
    :goto_0
    if-ge v2, v4, :cond_3

    .line 239822
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OH;

    .line 239823
    invoke-interface {v0, p1}, LX/1OH;->a(Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_2

    if-eq v3, v6, :cond_2

    .line 239824
    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:LX/1OH;

    .line 239825
    const/4 v0, 0x1

    .line 239826
    :goto_1
    return v0

    .line 239827
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 239828
    goto :goto_1
.end method

.method private a(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 239829
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239830
    if-eqz p1, :cond_2

    .line 239831
    sget-object v1, LX/2fV;->a:LX/2fY;

    invoke-interface {v1, p1}, LX/2fY;->a(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v1

    move v1, v1

    .line 239832
    :goto_0
    if-nez v1, :cond_1

    .line 239833
    :goto_1
    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->B:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->B:I

    .line 239834
    const/4 v0, 0x1

    .line 239835
    :cond_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method private b(LX/1a1;)J
    .locals 4

    .prologue
    .line 239836
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->eC_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239837
    iget-wide v2, p1, LX/1a1;->d:J

    move-wide v0, v2

    .line 239838
    :goto_0
    return-wide v0

    :cond_0
    iget v0, p1, LX/1a1;->b:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method public static b(Landroid/view/View;)LX/1a1;
    .locals 1

    .prologue
    .line 239839
    if-nez p0, :cond_0

    .line 239840
    const/4 v0, 0x0

    .line 239841
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    iget-object v0, v0, LX/1a3;->a:LX/1a1;

    goto :goto_0
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 239701
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 239702
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->t:LX/1OH;

    if-eqz v3, :cond_0

    .line 239703
    if-nez v0, :cond_1

    .line 239704
    iput-object v4, p0, Landroid/support/v7/widget/RecyclerView;->t:LX/1OH;

    .line 239705
    :cond_0
    if-eqz v0, :cond_5

    .line 239706
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 239707
    :goto_0
    if-ge v3, v4, :cond_5

    .line 239708
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OH;

    .line 239709
    invoke-interface {v0, p1}, LX/1OH;->a(Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 239710
    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:LX/1OH;

    move v0, v1

    .line 239711
    :goto_1
    return v0

    .line 239712
    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->t:LX/1OH;

    invoke-interface {v2, p1}, LX/1OH;->b(Landroid/view/MotionEvent;)V

    .line 239713
    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    if-ne v0, v1, :cond_3

    .line 239714
    :cond_2
    iput-object v4, p0, Landroid/support/v7/widget/RecyclerView;->t:LX/1OH;

    :cond_3
    move v0, v1

    .line 239715
    goto :goto_1

    .line 239716
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_5
    move v0, v2

    .line 239717
    goto :goto_1
.end method

.method public static c(Landroid/support/v7/widget/RecyclerView;LX/1a1;)I
    .locals 6

    .prologue
    .line 239857
    const/16 v0, 0x20c

    invoke-virtual {p1, v0}, LX/1a1;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/1a1;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 239858
    :cond_0
    const/4 v0, -0x1

    .line 239859
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    iget v1, p1, LX/1a1;->b:I

    .line 239860
    iget-object v2, v0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 239861
    const/4 v2, 0x0

    move v4, v2

    move v3, v1

    :goto_1
    if-ge v4, v5, :cond_3

    .line 239862
    iget-object v2, v0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1lw;

    .line 239863
    iget p0, v2, LX/1lw;->a:I

    packed-switch p0, :pswitch_data_0

    .line 239864
    :cond_2
    :goto_2
    :pswitch_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 239865
    :pswitch_1
    iget p0, v2, LX/1lw;->b:I

    if-gt p0, v3, :cond_2

    .line 239866
    iget v2, v2, LX/1lw;->d:I

    add-int/2addr v3, v2

    goto :goto_2

    .line 239867
    :pswitch_2
    iget p0, v2, LX/1lw;->b:I

    if-gt p0, v3, :cond_2

    .line 239868
    iget p0, v2, LX/1lw;->b:I

    iget p1, v2, LX/1lw;->d:I

    add-int/2addr p0, p1

    .line 239869
    if-le p0, v3, :cond_4

    .line 239870
    const/4 v3, -0x1

    .line 239871
    :cond_3
    move v0, v3

    .line 239872
    goto :goto_0

    .line 239873
    :cond_4
    iget v2, v2, LX/1lw;->d:I

    sub-int/2addr v3, v2

    .line 239874
    goto :goto_2

    .line 239875
    :pswitch_3
    iget p0, v2, LX/1lw;->b:I

    if-ne p0, v3, :cond_5

    .line 239876
    iget v3, v2, LX/1lw;->d:I

    goto :goto_2

    .line 239877
    :cond_5
    iget p0, v2, LX/1lw;->b:I

    if-ge p0, v3, :cond_6

    .line 239878
    add-int/lit8 v3, v3, -0x1

    .line 239879
    :cond_6
    iget v2, v2, LX/1lw;->d:I

    if-gt v2, v3, :cond_2

    .line 239880
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static synthetic c(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 239881
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->setMeasuredDimension(II)V

    return-void
.end method

.method private c(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 239882
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 239883
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    if-ne v1, v2, :cond_0

    .line 239884
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 239885
    :goto_0
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 239886
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 239887
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    .line 239888
    :cond_0
    return-void

    .line 239889
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 239890
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->h(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static d(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 239891
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 239892
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/1a1;->e()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static e(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 239893
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 239894
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static synthetic e(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 239895
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->n(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public static f(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 1

    .prologue
    .line 239896
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_0

    .line 239897
    :goto_0
    return-void

    .line 239898
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0, p1}, LX/1OR;->e(I)V

    .line 239899
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    goto :goto_0
.end method

.method private g(I)V
    .locals 2

    .prologue
    .line 239900
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_0

    .line 239901
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0, p1}, LX/1OR;->i(I)V

    .line 239902
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:LX/1OX;

    if-eqz v0, :cond_1

    .line 239903
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:LX/1OX;

    invoke-virtual {v0, p0, p1}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;I)V

    .line 239904
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 239905
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 239906
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OX;

    invoke-virtual {v0, p0, p1}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;I)V

    .line 239907
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 239908
    :cond_2
    return-void
.end method

.method private static g(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    .prologue
    .line 239909
    const/4 v0, 0x0

    .line 239910
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->a()Z

    move-result v1

    if-nez v1, :cond_0

    if-lez p1, :cond_0

    .line 239911
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    invoke-virtual {v0}, LX/0vj;->c()Z

    move-result v0

    .line 239912
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->a()Z

    move-result v1

    if-nez v1, :cond_1

    if-gez p1, :cond_1

    .line 239913
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 239914
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->a()Z

    move-result v1

    if-nez v1, :cond_2

    if-lez p2, :cond_2

    .line 239915
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 239916
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->a()Z

    move-result v1

    if-nez v1, :cond_3

    if-gez p2, :cond_3

    .line 239917
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 239918
    :cond_3
    if-eqz v0, :cond_4

    .line 239919
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 239920
    :cond_4
    return-void
.end method

.method private getScrollFactor()F
    .locals 4

    .prologue
    .line 240306
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->W:F

    const/4 v1, 0x1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 240307
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 240308
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x101004d

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 240309
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->W:F

    .line 240310
    :cond_0
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->W:F

    :goto_0
    return v0

    .line 240311
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    .line 240246
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 240247
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 240248
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 240249
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 240250
    sparse-switch v2, :sswitch_data_0

    .line 240251
    invoke-static {p0}, LX/0vv;->t(Landroid/view/View;)I

    move-result v1

    .line 240252
    :sswitch_0
    sparse-switch v3, :sswitch_data_1

    .line 240253
    invoke-static {p0}, LX/0vv;->u(Landroid/view/View;)I

    move-result v0

    .line 240254
    :sswitch_1
    invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/RecyclerView;->setMeasuredDimension(II)V

    .line 240255
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x40000000 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method private h(Landroid/view/View;)Z
    .locals 4

    .prologue
    .line 240312
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->b()V

    .line 240313
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    const/4 v1, 0x1

    .line 240314
    iget-object v2, v0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v2, p1}, LX/1Ou;->a(Landroid/view/View;)I

    move-result v2

    .line 240315
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 240316
    invoke-static {v0, p1}, LX/1Os;->g(LX/1Os;Landroid/view/View;)Z

    .line 240317
    :goto_0
    move v0, v1

    .line 240318
    if-eqz v0, :cond_0

    .line 240319
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v1

    .line 240320
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v2, v1}, LX/1Od;->b(LX/1a1;)V

    .line 240321
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v2, v1}, LX/1Od;->a(LX/1a1;)V

    .line 240322
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 240323
    return v0

    .line 240324
    :cond_1
    iget-object v3, v0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v3, v2}, LX/1Ov;->b(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 240325
    iget-object v3, v0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v3, v2}, LX/1Ov;->c(I)Z

    .line 240326
    invoke-static {v0, p1}, LX/1Os;->g(LX/1Os;Landroid/view/View;)Z

    .line 240327
    iget-object v3, v0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v3, v2}, LX/1Ou;->a(I)V

    goto :goto_0

    .line 240328
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static i(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 240329
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 240330
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 240331
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v1, v0}, LX/1OM;->d(LX/1a1;)V

    .line 240332
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 240333
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 240334
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 240335
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3x8;

    invoke-interface {v0, p1}, LX/3x8;->a(Landroid/view/View;)V

    .line 240336
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 240337
    :cond_1
    return-void
.end method

.method private i(II)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 240338
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v2}, LX/1Os;->b()I

    move-result v3

    .line 240339
    if-nez v3, :cond_2

    .line 240340
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    move v0, v1

    .line 240341
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v2, v0

    .line 240342
    :goto_1
    if-ge v2, v3, :cond_1

    .line 240343
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v4, v2}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v4

    .line 240344
    invoke-virtual {v4}, LX/1a1;->c()Z

    move-result v5

    if-nez v5, :cond_4

    .line 240345
    invoke-virtual {v4}, LX/1a1;->d()I

    move-result v4

    .line 240346
    if-lt v4, p1, :cond_3

    if-le v4, p2, :cond_4

    :cond_3
    move v0, v1

    .line 240347
    goto :goto_0

    .line 240348
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static j(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 240349
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 240350
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 240351
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v1, v0}, LX/1OM;->c(LX/1a1;)V

    .line 240352
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 240353
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 240354
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 240355
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 240356
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 240357
    :cond_1
    return-void
.end method

.method public static synthetic k(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 240358
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    move-result v0

    return v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 240359
    new-instance v0, LX/1Os;

    new-instance v1, LX/1Ot;

    invoke-direct {v1, p0}, LX/1Ot;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v0, v1}, LX/1Os;-><init>(LX/1Ou;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    .line 240360
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 240361
    new-instance v0, LX/1On;

    new-instance v1, LX/1Op;

    invoke-direct {v1, p0}, LX/1Op;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v0, v1}, LX/1On;-><init>(LX/1Oq;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    .line 240362
    return-void
.end method

.method private static n(Landroid/support/v7/widget/RecyclerView;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x8bdf077

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 240363
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 240364
    const/16 v1, 0x2d

    const v2, -0x374af90d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method private o()V
    .locals 1

    .prologue
    .line 240300
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Landroid/support/v7/widget/RecyclerView$ViewFlinger;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->b()V

    .line 240301
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_0

    .line 240302
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->G()V

    .line 240303
    :cond_0
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    .line 240365
    const/4 v0, 0x0

    .line 240366
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    invoke-virtual {v0}, LX/0vj;->c()Z

    move-result v0

    .line 240367
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 240368
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 240369
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 240370
    :cond_3
    if-eqz v0, :cond_4

    .line 240371
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 240372
    :cond_4
    return-void
.end method

.method private q()V
    .locals 4

    .prologue
    .line 240373
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    if-eqz v0, :cond_0

    .line 240374
    :goto_0
    return-void

    .line 240375
    :cond_0
    new-instance v0, LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0vj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    .line 240376
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_1

    .line 240377
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, LX/0vj;->a(II)V

    goto :goto_0

    .line 240378
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0vj;->a(II)V

    goto :goto_0
.end method

.method private r()V
    .locals 4

    .prologue
    .line 240379
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    if-eqz v0, :cond_0

    .line 240380
    :goto_0
    return-void

    .line 240381
    :cond_0
    new-instance v0, LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0vj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    .line 240382
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_1

    .line 240383
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, LX/0vj;->a(II)V

    goto :goto_0

    .line 240384
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0vj;->a(II)V

    goto :goto_0
.end method

.method private s()V
    .locals 4

    .prologue
    .line 240389
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    if-eqz v0, :cond_0

    .line 240390
    :goto_0
    return-void

    .line 240391
    :cond_0
    new-instance v0, LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0vj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    .line 240392
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_1

    .line 240393
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, LX/0vj;->a(II)V

    goto :goto_0

    .line 240394
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0vj;->a(II)V

    goto :goto_0
.end method

.method public static setScrollState(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 1

    .prologue
    .line 240395
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    if-ne p1, v0, :cond_0

    .line 240396
    :goto_0
    return-void

    .line 240397
    :cond_0
    iput p1, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    .line 240398
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 240399
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->o()V

    .line 240400
    :cond_1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->g(I)V

    goto :goto_0
.end method

.method private t()V
    .locals 4

    .prologue
    .line 240401
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    if-eqz v0, :cond_0

    .line 240402
    :goto_0
    return-void

    .line 240403
    :cond_0
    new-instance v0, LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0vj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    .line 240404
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_1

    .line 240405
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, LX/0vj;->a(II)V

    goto :goto_0

    .line 240406
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0vj;->a(II)V

    goto :goto_0
.end method

.method private u()V
    .locals 1

    .prologue
    .line 240407
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    .line 240408
    return-void
.end method

.method private v()V
    .locals 1

    .prologue
    .line 240409
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 240410
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 240411
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->stopNestedScroll()V

    .line 240412
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->p()V

    .line 240413
    return-void
.end method

.method private w()V
    .locals 1

    .prologue
    .line 240414
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->v()V

    .line 240415
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setScrollState(Landroid/support/v7/widget/RecyclerView;I)V

    .line 240416
    return-void
.end method

.method public static x(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 240304
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->H:I

    .line 240305
    return-void
.end method

.method public static y(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 240164
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->H:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->H:I

    .line 240165
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->H:I

    if-gtz v0, :cond_0

    .line 240166
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->H:I

    .line 240167
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->z()V

    .line 240168
    :cond_0
    return-void
.end method

.method private z()V
    .locals 3

    .prologue
    .line 240169
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->B:I

    .line 240170
    const/4 v1, 0x0

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->B:I

    .line 240171
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240172
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 240173
    const/16 v2, 0x800

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setEventType(I)V

    .line 240174
    sget-object v2, LX/2fV;->a:LX/2fY;

    invoke-interface {v2, v1, v0}, LX/2fY;->a(Landroid/view/accessibility/AccessibilityEvent;I)V

    .line 240175
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 240176
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)LX/1a1;
    .locals 4

    .prologue
    .line 240177
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->c()I

    move-result v2

    .line 240178
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    .line 240179
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0, v1}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 240180
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/1a1;->q()Z

    move-result v3

    if-nez v3, :cond_2

    .line 240181
    if-eqz p2, :cond_1

    .line 240182
    iget v3, v0, LX/1a1;->b:I

    if-ne v3, p1, :cond_2

    .line 240183
    :cond_0
    :goto_1
    return-object v0

    .line 240184
    :cond_1
    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v3

    if-eq v3, p1, :cond_0

    .line 240185
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 240186
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/view/View;)LX/1a1;
    .locals 3

    .prologue
    .line 240187
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 240188
    if-eqz v0, :cond_0

    if-eq v0, p0, :cond_0

    .line 240189
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a direct child of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240190
    :cond_0
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    return-object v0
.end method

.method public final a(FF)Landroid/view/View;
    .locals 5

    .prologue
    .line 240191
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v0

    .line 240192
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 240193
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0, v1}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v0

    .line 240194
    invoke-static {v0}, LX/0vv;->r(Landroid/view/View;)F

    move-result v2

    .line 240195
    invoke-static {v0}, LX/0vv;->s(Landroid/view/View;)F

    move-result v3

    .line 240196
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v2, v4

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_0

    .line 240197
    :goto_1
    return-object v0

    .line 240198
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 240199
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 240200
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 240201
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 240202
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 240203
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v1, :cond_1

    .line 240204
    const-string v0, "RecyclerView"

    const-string v1, "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240205
    :cond_0
    :goto_0
    return-void

    .line 240206
    :cond_1
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-nez v1, :cond_0

    .line 240207
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v1}, LX/1OR;->g()Z

    move-result v1

    if-nez v1, :cond_2

    move p1, v0

    .line 240208
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v1}, LX/1OR;->h()Z

    move-result v1

    if-nez v1, :cond_4

    .line 240209
    :goto_1
    if-nez p1, :cond_3

    if-eqz v0, :cond_0

    .line 240210
    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->aa:Landroid/support/v7/widget/RecyclerView$ViewFlinger;

    invoke-virtual {v1, p1, v0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->b(II)V

    goto :goto_0

    :cond_4
    move v0, p2

    goto :goto_1
.end method

.method public final a(IILjava/lang/Object;)V
    .locals 6

    .prologue
    .line 240211
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->c()I

    move-result v2

    .line 240212
    add-int v3, p1, p2

    .line 240213
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 240214
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0, v1}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v0

    .line 240215
    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v4

    .line 240216
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/1a1;->c()Z

    move-result v5

    if-nez v5, :cond_1

    .line 240217
    iget v5, v4, LX/1a1;->b:I

    if-lt v5, p1, :cond_1

    iget v5, v4, LX/1a1;->b:I

    if-ge v5, v3, :cond_1

    .line 240218
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, LX/1a1;->b(I)V

    .line 240219
    invoke-virtual {v4, p3}, LX/1a1;->a(Ljava/lang/Object;)V

    .line 240220
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->A(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 240221
    const/16 v5, 0x40

    invoke-virtual {v4, v5}, LX/1a1;->b(I)V

    .line 240222
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    const/4 v4, 0x1

    iput-boolean v4, v0, LX/1a3;->c:Z

    .line 240223
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 240224
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, p1, p2}, LX/1Od;->c(II)V

    .line 240225
    return-void
.end method

.method public final a(IIZ)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 240226
    add-int v1, p1, p2

    .line 240227
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->c()I

    move-result v2

    .line 240228
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    .line 240229
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v3, v0}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v3

    .line 240230
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/1a1;->c()Z

    move-result v4

    if-nez v4, :cond_0

    .line 240231
    iget v4, v3, LX/1a1;->b:I

    if-lt v4, v1, :cond_1

    .line 240232
    neg-int v4, p2

    invoke-virtual {v3, v4, p3}, LX/1a1;->a(IZ)V

    .line 240233
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 240234
    iput-boolean v6, v3, LX/1Ok;->j:Z

    .line 240235
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240236
    :cond_1
    iget v4, v3, LX/1a1;->b:I

    if-lt v4, p1, :cond_0

    .line 240237
    add-int/lit8 v4, p1, -0x1

    neg-int v5, p2

    invoke-virtual {v3, v4, v5, p3}, LX/1a1;->a(IIZ)V

    .line 240238
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 240239
    iput-boolean v6, v3, LX/1Ok;->j:Z

    .line 240240
    goto :goto_1

    .line 240241
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, p1, p2, p3}, LX/1Od;->a(IIZ)V

    .line 240242
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 240243
    return-void
.end method

.method public final a(LX/1OH;)V
    .locals 1

    .prologue
    .line 240244
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240245
    return-void
.end method

.method public final a(LX/1OM;Z)V
    .locals 1

    .prologue
    .line 240159
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutFrozen(Z)V

    .line 240160
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OM;ZZ)V

    .line 240161
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->H(Landroid/support/v7/widget/RecyclerView;)V

    .line 240162
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 240163
    return-void
.end method

.method public final a(LX/1OX;)V
    .locals 1

    .prologue
    .line 240256
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    if-nez v0, :cond_0

    .line 240257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    .line 240258
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240259
    return-void
.end method

.method public final a(LX/3x6;)V
    .locals 1

    .prologue
    .line 240260
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;I)V

    .line 240261
    return-void
.end method

.method public final a(LX/3x8;)V
    .locals 1

    .prologue
    .line 240262
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    if-nez v0, :cond_0

    .line 240263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    .line 240264
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240265
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 240266
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240267
    if-nez p1, :cond_0

    .line 240268
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call this method while RecyclerView is computing a layout or scrolling"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240269
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240270
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 240271
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-eqz v0, :cond_1

    .line 240272
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v0, :cond_0

    .line 240273
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->f()V

    .line 240274
    :cond_0
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    .line 240275
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-nez v0, :cond_1

    .line 240276
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    .line 240277
    :cond_1
    return-void
.end method

.method public final addFocusables(Ljava/util/ArrayList;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 240278
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_0

    .line 240279
    const/4 v0, 0x0

    move v0, v0

    .line 240280
    if-nez v0, :cond_1

    .line 240281
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addFocusables(Ljava/util/ArrayList;II)V

    .line 240282
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 240283
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-nez v0, :cond_0

    .line 240284
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    .line 240285
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-nez v0, :cond_0

    .line 240286
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    .line 240287
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 240288
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-eqz v0, :cond_0

    .line 240289
    :goto_0
    return-void

    .line 240290
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_1

    .line 240291
    const-string v0, "RecyclerView"

    const-string v1, "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 240292
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, p0, v1, p1}, LX/1OR;->a(Landroid/support/v7/widget/RecyclerView;LX/1Ok;I)V

    goto :goto_0
.end method

.method public final b(LX/1OH;)V
    .locals 1

    .prologue
    .line 240293
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 240294
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:LX/1OH;

    if-ne v0, p1, :cond_0

    .line 240295
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:LX/1OH;

    .line 240296
    :cond_0
    return-void
.end method

.method public final b(LX/1OX;)V
    .locals 1

    .prologue
    .line 240297
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 240298
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 240299
    :cond_0
    return-void
.end method

.method public final b(LX/3x6;)V
    .locals 2

    .prologue
    .line 240025
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_0

    .line 240026
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    const-string v1, "Cannot remove item decoration during a scroll  or layout"

    invoke-virtual {v0, v1}, LX/1OR;->a(Ljava/lang/String;)V

    .line 240027
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 240028
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240029
    invoke-static {p0}, LX/0vv;->a(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    .line 240030
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->E()V

    .line 240031
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 240032
    return-void

    .line 240033
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/3x8;)V
    .locals 1

    .prologue
    .line 239929
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    if-nez v0, :cond_0

    .line 239930
    :goto_0
    return-void

    .line 239931
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(II)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 239193
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_1

    .line 239194
    const-string v0, "RecyclerView"

    const-string v2, "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239195
    :cond_0
    :goto_0
    return v1

    .line 239196
    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-nez v0, :cond_0

    .line 239197
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->g()Z

    move-result v0

    .line 239198
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v3}, LX/1OR;->h()Z

    move-result v3

    .line 239199
    if-eqz v0, :cond_2

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->U:I

    if-ge v4, v5, :cond_3

    :cond_2
    move p1, v1

    .line 239200
    :cond_3
    if-eqz v3, :cond_4

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->U:I

    if-ge v4, v5, :cond_5

    :cond_4
    move p2, v1

    .line 239201
    :cond_5
    if-nez p1, :cond_6

    if-eqz p2, :cond_0

    .line 239202
    :cond_6
    int-to-float v4, p1

    int-to-float v5, p2

    invoke-virtual {p0, v4, v5}, Landroid/support/v7/widget/RecyclerView;->dispatchNestedPreFling(FF)Z

    move-result v4

    if-nez v4, :cond_0

    .line 239203
    if-nez v0, :cond_7

    if-eqz v3, :cond_8

    :cond_7
    move v0, v2

    .line 239204
    :goto_1
    int-to-float v3, p1

    int-to-float v4, p2

    invoke-virtual {p0, v3, v4, v0}, Landroid/support/v7/widget/RecyclerView;->dispatchNestedFling(FFZ)Z

    .line 239205
    if-eqz v0, :cond_0

    .line 239206
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    neg-int v0, v0

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 239207
    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    neg-int v1, v1

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 239208
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->aa:Landroid/support/v7/widget/RecyclerView$ViewFlinger;

    invoke-virtual {v3, v0, v1}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a(II)V

    move v1, v2

    .line 239209
    goto :goto_0

    :cond_8
    move v0, v1

    .line 239210
    goto :goto_1
.end method

.method public final c(I)LX/1a1;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 239228
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->G:Z

    if-eqz v1, :cond_1

    .line 239229
    :cond_0
    :goto_0
    return-object v0

    .line 239230
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v1}, LX/1Os;->c()I

    move-result v3

    .line 239231
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    .line 239232
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v1, v2}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v1

    .line 239233
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/1a1;->q()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {p0, v1}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/support/v7/widget/RecyclerView;LX/1a1;)I

    move-result v4

    if-ne v4, p1, :cond_2

    move-object v0, v1

    .line 239234
    goto :goto_0

    .line 239235
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 239236
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setScrollState(Landroid/support/v7/widget/RecyclerView;I)V

    .line 239237
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->o()V

    .line 239238
    return-void
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 239239
    if-gez p1, :cond_4

    .line 239240
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->q()V

    .line 239241
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    neg-int v1, p1

    invoke-virtual {v0, v1}, LX/0vj;->a(I)Z

    .line 239242
    :cond_0
    :goto_0
    if-gez p2, :cond_5

    .line 239243
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->s()V

    .line 239244
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    neg-int v1, p2

    invoke-virtual {v0, v1}, LX/0vj;->a(I)Z

    .line 239245
    :cond_1
    :goto_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_3

    .line 239246
    :cond_2
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 239247
    :cond_3
    return-void

    .line 239248
    :cond_4
    if-lez p1, :cond_0

    .line 239249
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->r()V

    .line 239250
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    invoke-virtual {v0, p1}, LX/0vj;->a(I)Z

    goto :goto_0

    .line 239251
    :cond_5
    if-lez p2, :cond_1

    .line 239252
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->t()V

    .line 239253
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    invoke-virtual {v0, p2}, LX/0vj;->a(I)Z

    goto :goto_1
.end method

.method public checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 239254
    instance-of v0, p1, LX/1a3;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    check-cast p1, LX/1a3;

    invoke-virtual {v0, p1}, LX/1OR;->a(LX/1a3;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final computeHorizontalScrollExtent()I
    .locals 2

    .prologue
    .line 239255
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, v1}, LX/1OR;->d(LX/1Ok;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final computeHorizontalScrollOffset()I
    .locals 2

    .prologue
    .line 239256
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, v1}, LX/1OR;->b(LX/1Ok;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final computeHorizontalScrollRange()I
    .locals 2

    .prologue
    .line 239257
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, v1}, LX/1OR;->f(LX/1Ok;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final computeVerticalScrollExtent()I
    .locals 2

    .prologue
    .line 239258
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, v1}, LX/1OR;->e(LX/1Ok;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final computeVerticalScrollOffset()I
    .locals 2

    .prologue
    .line 239192
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, v1}, LX/1OR;->c(LX/1Ok;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final computeVerticalScrollRange()I
    .locals 2

    .prologue
    .line 239302
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, v1}, LX/1OR;->g(LX/1Ok;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)V
    .locals 3

    .prologue
    .line 239303
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v1

    .line 239304
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 239305
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v2, v0}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 239306
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239307
    :cond_0
    return-void
.end method

.method public final d(II)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 239308
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->c()I

    move-result v6

    .line 239309
    if-ge p1, p2, :cond_1

    .line 239310
    const/4 v0, -0x1

    move v2, p2

    move v3, p1

    :goto_0
    move v4, v5

    .line 239311
    :goto_1
    if-ge v4, v6, :cond_3

    .line 239312
    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v7, v4}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v7

    invoke-static {v7}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v7

    .line 239313
    if-eqz v7, :cond_0

    iget v8, v7, LX/1a1;->b:I

    if-lt v8, v3, :cond_0

    iget v8, v7, LX/1a1;->b:I

    if-gt v8, v2, :cond_0

    .line 239314
    iget v8, v7, LX/1a1;->b:I

    if-ne v8, p1, :cond_2

    .line 239315
    sub-int v8, p2, p1

    invoke-virtual {v7, v8, v5}, LX/1a1;->a(IZ)V

    .line 239316
    :goto_2
    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239317
    iput-boolean v1, v7, LX/1Ok;->j:Z

    .line 239318
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    move v2, p1

    move v3, p2

    .line 239319
    goto :goto_0

    .line 239320
    :cond_2
    invoke-virtual {v7, v0, v5}, LX/1a1;->a(IZ)V

    goto :goto_2

    .line 239321
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, p1, p2}, LX/1Od;->a(II)V

    .line 239322
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 239323
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 239324
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchNestedFling(FFZ)Z
    .locals 1

    .prologue
    .line 239325
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:LX/1Oy;

    invoke-virtual {v0, p1, p2, p3}, LX/1Oy;->a(FFZ)Z

    move-result v0

    return v0
.end method

.method public final dispatchNestedPreFling(FF)Z
    .locals 1

    .prologue
    .line 239326
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:LX/1Oy;

    invoke-virtual {v0, p1, p2}, LX/1Oy;->a(FF)Z

    move-result v0

    return v0
.end method

.method public final dispatchNestedPreScroll(II[I[I)Z
    .locals 1

    .prologue
    .line 239327
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:LX/1Oy;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1Oy;->a(II[I[I)Z

    move-result v0

    return v0
.end method

.method public final dispatchNestedScroll(IIII[I)Z
    .locals 6

    .prologue
    .line 239328
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:LX/1Oy;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Oy;->a(IIII[I)Z

    move-result v0

    return v0
.end method

.method public final dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 239329
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 239330
    return-void
.end method

.method public final dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 239331
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 239332
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 239259
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 239260
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v1

    .line 239261
    :goto_0
    if-ge v3, v4, :cond_0

    .line 239262
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3x6;

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, p1, p0, v5}, LX/3x6;->b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 239263
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 239264
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    invoke-virtual {v0}, LX/0vj;->a()Z

    move-result v0

    if-nez v0, :cond_e

    .line 239265
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 239266
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    .line 239267
    :goto_1
    const/high16 v4, 0x43870000    # 270.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 239268
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v4

    neg-int v4, v4

    add-int/2addr v0, v4

    int-to-float v0, v0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 239269
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:LX/0vj;

    invoke-virtual {v0, p1}, LX/0vj;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    .line 239270
    :goto_2
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 239271
    :goto_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    if-eqz v3, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    invoke-virtual {v3}, LX/0vj;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 239272
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 239273
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v3, :cond_1

    .line 239274
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 239275
    :cond_1
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    if-eqz v3, :cond_9

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->J:LX/0vj;

    invoke-virtual {v3, p1}, LX/0vj;->a(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_9

    move v3, v2

    :goto_4
    or-int/2addr v0, v3

    .line 239276
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 239277
    :cond_2
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    invoke-virtual {v3}, LX/0vj;->a()Z

    move-result v3

    if-nez v3, :cond_3

    .line 239278
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 239279
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v5

    .line 239280
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v3, :cond_a

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    .line 239281
    :goto_5
    const/high16 v6, 0x42b40000    # 90.0f

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->rotate(F)V

    .line 239282
    neg-int v3, v3

    int-to-float v3, v3

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 239283
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    if-eqz v3, :cond_b

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->K:LX/0vj;

    invoke-virtual {v3, p1}, LX/0vj;->a(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_b

    move v3, v2

    :goto_6
    or-int/2addr v0, v3

    .line 239284
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 239285
    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    if-eqz v3, :cond_5

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    invoke-virtual {v3}, LX/0vj;->a()Z

    move-result v3

    if-nez v3, :cond_5

    .line 239286
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 239287
    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 239288
    iget-boolean v4, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v4, :cond_c

    .line 239289
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    neg-int v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v5

    neg-int v5, v5

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 239290
    :goto_7
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    if-eqz v4, :cond_4

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->L:LX/0vj;

    invoke-virtual {v4, p1}, LX/0vj;->a(Landroid/graphics/Canvas;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v1, v2

    :cond_4
    or-int/2addr v0, v1

    .line 239291
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 239292
    :cond_5
    if-nez v0, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v1, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    invoke-virtual {v1}, LX/1Of;->b()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 239293
    :goto_8
    if-eqz v2, :cond_6

    .line 239294
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 239295
    :cond_6
    return-void

    :cond_7
    move v0, v1

    .line 239296
    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 239297
    goto/16 :goto_2

    :cond_9
    move v3, v1

    .line 239298
    goto/16 :goto_4

    :cond_a
    move v3, v1

    .line 239299
    goto/16 :goto_5

    :cond_b
    move v3, v1

    .line 239300
    goto :goto_6

    .line 239301
    :cond_c
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_7

    :cond_d
    move v2, v0

    goto :goto_8

    :cond_e
    move v0, v1

    goto/16 :goto_3
.end method

.method public final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 1

    .prologue
    .line 239171
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0
.end method

.method public final e(I)V
    .locals 3

    .prologue
    .line 239165
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v1

    .line 239166
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 239167
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v2, v0}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 239168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239169
    :cond_0
    return-void
.end method

.method public final e(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 239154
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->c()I

    move-result v2

    move v0, v1

    .line 239155
    :goto_0
    if-ge v0, v2, :cond_1

    .line 239156
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v3, v0}, LX/1Os;->c(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v3

    .line 239157
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/1a1;->c()Z

    move-result v4

    if-nez v4, :cond_0

    iget v4, v3, LX/1a1;->b:I

    if-lt v4, p1, :cond_0

    .line 239158
    invoke-virtual {v3, p2, v1}, LX/1a1;->a(IZ)V

    .line 239159
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    const/4 v4, 0x1

    .line 239160
    iput-boolean v4, v3, LX/1Ok;->j:Z

    .line 239161
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239162
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, p1, p2}, LX/1Od;->b(II)V

    .line 239163
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 239164
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 239153
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->H:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Landroid/view/View;)J
    .locals 5

    .prologue
    const-wide/16 v0, -0x1

    .line 239147
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v2}, LX/1OM;->eC_()Z

    move-result v2

    if-nez v2, :cond_1

    .line 239148
    :cond_0
    :goto_0
    return-wide v0

    .line 239149
    :cond_1
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v2

    .line 239150
    if-eqz v2, :cond_0

    .line 239151
    iget-wide v3, v2, LX/1a1;->d:J

    move-wide v0, v3

    .line 239152
    goto :goto_0
.end method

.method public final f()V
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 238997
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-nez v0, :cond_1

    .line 238998
    const-string v0, "RecyclerView"

    const-string v1, "No adapter attached; skipping layout"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238999
    :cond_0
    :goto_0
    return-void

    .line 239000
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_2

    .line 239001
    const-string v0, "RecyclerView"

    const-string v1, "No layout manager attached; skipping layout"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 239002
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 239003
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->b()V

    .line 239004
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->x(Landroid/support/v7/widget/RecyclerView;)V

    .line 239005
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->D()V

    .line 239006
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v0, v0, LX/1Ok;->l:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Z

    if-eqz v0, :cond_5

    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->A(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    :goto_1
    iput-object v0, v1, LX/1Ok;->c:LX/026;

    .line 239007
    iput-boolean v7, p0, Landroid/support/v7/widget/RecyclerView;->g:Z

    iput-boolean v7, p0, Landroid/support/v7/widget/RecyclerView;->f:Z

    .line 239008
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v1, v1, LX/1Ok;->m:Z

    .line 239009
    iput-boolean v1, v0, LX/1Ok;->k:Z

    .line 239010
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    iput v1, v0, LX/1Ok;->e:I

    .line 239011
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ah:[I

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a([I)V

    .line 239012
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v0, v0, LX/1Ok;->l:Z

    if-eqz v0, :cond_6

    .line 239013
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->a:LX/026;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 239014
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->b:LX/026;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 239015
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v10

    move v8, v7

    .line 239016
    :goto_2
    if-ge v8, v10, :cond_6

    .line 239017
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0, v8}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v1

    .line 239018
    invoke-virtual {v1}, LX/1a1;->c()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v1}, LX/1a1;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->eC_()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 239019
    :cond_3
    iget-object v5, v1, LX/1a1;->a:Landroid/view/View;

    .line 239020
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v11, v0, LX/1Ok;->a:LX/026;

    new-instance v0, LX/3x7;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/3x7;-><init>(LX/1a1;IIII)V

    invoke-virtual {v11, v1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239021
    :cond_4
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_2

    :cond_5
    move-object v0, v6

    .line 239022
    goto :goto_1

    .line 239023
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v0, v0, LX/1Ok;->m:Z

    if-eqz v0, :cond_d

    .line 239024
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->F()V

    .line 239025
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->c:LX/026;

    if-eqz v0, :cond_8

    .line 239026
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v1

    move v0, v7

    .line 239027
    :goto_3
    if-ge v0, v1, :cond_8

    .line 239028
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v2, v0}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v2

    .line 239029
    invoke-virtual {v2}, LX/1a1;->o()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v2}, LX/1a1;->q()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v2}, LX/1a1;->c()Z

    move-result v3

    if-nez v3, :cond_7

    .line 239030
    invoke-direct {p0, v2}, Landroid/support/v7/widget/RecyclerView;->b(LX/1a1;)J

    move-result-wide v4

    .line 239031
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v3, v3, LX/1Ok;->c:LX/026;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239032
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v3, v3, LX/1Ok;->a:LX/026;

    invoke-virtual {v3, v2}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 239033
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 239034
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v0, v0, LX/1Ok;->j:Z

    .line 239035
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239036
    iput-boolean v7, v1, LX/1Ok;->j:Z

    .line 239037
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v1, v2, v3}, LX/1OR;->c(LX/1Od;LX/1Ok;)V

    .line 239038
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239039
    iput-boolean v0, v1, LX/1Ok;->j:Z

    .line 239040
    new-instance v3, LX/026;

    invoke-direct {v3}, LX/026;-><init>()V

    move v1, v7

    .line 239041
    :goto_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 239042
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0, v1}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v4

    .line 239043
    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    invoke-virtual {v0}, LX/1a1;->c()Z

    move-result v0

    if-nez v0, :cond_9

    move v2, v7

    .line 239044
    :goto_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->a:LX/026;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v0

    if-ge v2, v0, :cond_21

    .line 239045
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->a:LX/026;

    invoke-virtual {v0, v2}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 239046
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    if-ne v0, v4, :cond_a

    move v0, v9

    .line 239047
    :goto_6
    if-nez v0, :cond_9

    .line 239048
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v8

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v10

    invoke-direct {v0, v2, v5, v8, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v4, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239049
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 239050
    :cond_a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 239051
    :cond_b
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->G()V

    .line 239052
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0}, LX/1On;->c()V

    move-object v8, v3

    .line 239053
    :goto_7
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    iput v1, v0, LX/1Ok;->e:I

    .line 239054
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239055
    iput v7, v0, LX/1Ok;->i:I

    .line 239056
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239057
    iput-boolean v7, v0, LX/1Ok;->k:Z

    .line 239058
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, v1, v2}, LX/1OR;->c(LX/1Od;LX/1Ok;)V

    .line 239059
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239060
    iput-boolean v7, v0, LX/1Ok;->j:Z

    .line 239061
    iput-object v6, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    .line 239062
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v0, v0, LX/1Ok;->l:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v0, :cond_f

    move v0, v9

    .line 239063
    :goto_8
    iput-boolean v0, v1, LX/1Ok;->l:Z

    .line 239064
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v0, v0, LX/1Ok;->l:Z

    if-eqz v0, :cond_1e

    .line 239065
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->c:LX/026;

    if-eqz v0, :cond_10

    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    move-object v10, v0

    .line 239066
    :goto_9
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v12

    move v11, v7

    .line 239067
    :goto_a
    if-ge v11, v12, :cond_12

    .line 239068
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0, v11}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v1

    .line 239069
    invoke-virtual {v1}, LX/1a1;->c()Z

    move-result v0

    if-nez v0, :cond_c

    .line 239070
    iget-object v5, v1, LX/1a1;->a:Landroid/view/View;

    .line 239071
    invoke-direct {p0, v1}, Landroid/support/v7/widget/RecyclerView;->b(LX/1a1;)J

    move-result-wide v2

    .line 239072
    if-eqz v10, :cond_11

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->c:LX/026;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 239073
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v10, v0, v1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239074
    :cond_c
    :goto_b
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_a

    .line 239075
    :cond_d
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->G()V

    .line 239076
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0}, LX/1On;->e()V

    .line 239077
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->c:LX/026;

    if-eqz v0, :cond_20

    .line 239078
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v1

    move v0, v7

    .line 239079
    :goto_c
    if-ge v0, v1, :cond_20

    .line 239080
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v2, v0}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v2

    .line 239081
    invoke-virtual {v2}, LX/1a1;->o()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {v2}, LX/1a1;->q()Z

    move-result v3

    if-nez v3, :cond_e

    invoke-virtual {v2}, LX/1a1;->c()Z

    move-result v3

    if-nez v3, :cond_e

    .line 239082
    invoke-direct {p0, v2}, Landroid/support/v7/widget/RecyclerView;->b(LX/1a1;)J

    move-result-wide v4

    .line 239083
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v3, v3, LX/1Ok;->c:LX/026;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239084
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v3, v3, LX/1Ok;->a:LX/026;

    invoke-virtual {v3, v2}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 239085
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_f
    move v0, v7

    .line 239086
    goto/16 :goto_8

    :cond_10
    move-object v10, v6

    .line 239087
    goto/16 :goto_9

    .line 239088
    :cond_11
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v13, v0, LX/1Ok;->b:LX/026;

    new-instance v0, LX/3x7;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/3x7;-><init>(LX/1a1;IIII)V

    invoke-virtual {v13, v1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    .line 239089
    :cond_12
    invoke-direct {p0, v8}, Landroid/support/v7/widget/RecyclerView;->a(LX/026;)V

    .line 239090
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->a:LX/026;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v0

    .line 239091
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_d
    if-ltz v1, :cond_14

    .line 239092
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->a:LX/026;

    invoke-virtual {v0, v1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 239093
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v2, v2, LX/1Ok;->b:LX/026;

    invoke-virtual {v2, v0}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 239094
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->a:LX/026;

    invoke-virtual {v0, v1}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3x7;

    .line 239095
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v2, v2, LX/1Ok;->a:LX/026;

    invoke-virtual {v2, v1}, LX/01J;->d(I)Ljava/lang/Object;

    .line 239096
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v3, v0, LX/3x7;->a:LX/1a1;

    invoke-virtual {v2, v3}, LX/1Od;->b(LX/1a1;)V

    .line 239097
    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x7;)V

    .line 239098
    :cond_13
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_d

    .line 239099
    :cond_14
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->b:LX/026;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v0

    .line 239100
    if-lez v0, :cond_18

    .line 239101
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_e
    if-ltz v3, :cond_18

    .line 239102
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->b:LX/026;

    invoke-virtual {v0, v3}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 239103
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v1, v1, LX/1Ok;->b:LX/026;

    invoke-virtual {v1, v3}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3x7;

    .line 239104
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v2, v2, LX/1Ok;->a:LX/026;

    invoke-virtual {v2}, LX/01J;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_15

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v2, v2, LX/1Ok;->a:LX/026;

    invoke-virtual {v2, v0}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    .line 239105
    :cond_15
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v2, v2, LX/1Ok;->b:LX/026;

    invoke-virtual {v2, v3}, LX/01J;->d(I)Ljava/lang/Object;

    .line 239106
    if-eqz v8, :cond_17

    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v8, v2}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 239107
    :goto_f
    iget v4, v1, LX/3x7;->b:I

    iget v1, v1, LX/3x7;->c:I

    invoke-direct {p0, v0, v2, v4, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1a1;Landroid/graphics/Rect;II)V

    .line 239108
    :cond_16
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_e

    :cond_17
    move-object v2, v6

    .line 239109
    goto :goto_f

    .line 239110
    :cond_18
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->b:LX/026;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v11

    move v8, v7

    .line 239111
    :goto_10
    if-ge v8, v11, :cond_1b

    .line 239112
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->b:LX/026;

    invoke-virtual {v0, v8}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    .line 239113
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->b:LX/026;

    invoke-virtual {v0, v8}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, LX/3x7;

    .line 239114
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->a:LX/026;

    invoke-virtual {v0, v1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, LX/3x7;

    .line 239115
    if-eqz v3, :cond_1a

    if-eqz v5, :cond_1a

    .line 239116
    iget v0, v3, LX/3x7;->b:I

    iget v2, v5, LX/3x7;->b:I

    if-ne v0, v2, :cond_19

    iget v0, v3, LX/3x7;->c:I

    iget v2, v5, LX/3x7;->c:I

    if-eq v0, v2, :cond_1a

    .line 239117
    :cond_19
    invoke-virtual {v1, v7}, LX/1a1;->a(Z)V

    .line 239118
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    iget v2, v3, LX/3x7;->b:I

    iget v3, v3, LX/3x7;->c:I

    iget v4, v5, LX/3x7;->b:I

    iget v5, v5, LX/3x7;->c:I

    invoke-virtual/range {v0 .. v5}, LX/1Of;->a(LX/1a1;IIII)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 239119
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->B()V

    .line 239120
    :cond_1a
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_10

    .line 239121
    :cond_1b
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->c:LX/026;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->c:LX/026;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v0

    .line 239122
    :goto_11
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_12
    if-ltz v2, :cond_1e

    .line 239123
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->c:LX/026;

    invoke-virtual {v0, v2}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 239124
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v0, v0, LX/1Ok;->c:LX/026;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 239125
    invoke-virtual {v0}, LX/1a1;->c()Z

    move-result v1

    if-nez v1, :cond_1c

    .line 239126
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v1, v1, LX/1Od;->d:Ljava/util/ArrayList;

    if-eqz v1, :cond_1c

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v1, v1, LX/1Od;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 239127
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1a1;LX/1a1;)V

    .line 239128
    :cond_1c
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_12

    :cond_1d
    move v0, v7

    .line 239129
    goto :goto_11

    .line 239130
    :cond_1e
    invoke-virtual {p0, v7}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 239131
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, v1}, LX/1OR;->b(LX/1Od;)V

    .line 239132
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget v1, v1, LX/1Ok;->e:I

    .line 239133
    iput v1, v0, LX/1Ok;->h:I

    .line 239134
    iput-boolean v7, p0, Landroid/support/v7/widget/RecyclerView;->G:Z

    .line 239135
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239136
    iput-boolean v7, v0, LX/1Ok;->l:Z

    .line 239137
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239138
    iput-boolean v7, v0, LX/1Ok;->m:Z

    .line 239139
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->y(Landroid/support/v7/widget/RecyclerView;)V

    .line 239140
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    .line 239141
    iput-boolean v7, v0, LX/1OR;->a:Z

    .line 239142
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v0, v0, LX/1Od;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_1f

    .line 239143
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v0, v0, LX/1Od;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 239144
    :cond_1f
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iput-object v6, v0, LX/1Ok;->c:LX/026;

    .line 239145
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ah:[I

    aget v0, v0, v7

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->ah:[I

    aget v1, v1, v9

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->i(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239146
    invoke-virtual {p0, v7, v7}, Landroid/support/v7/widget/RecyclerView;->f(II)V

    goto/16 :goto_0

    :cond_20
    move-object v8, v6

    goto/16 :goto_7

    :cond_21
    move v0, v7

    goto/16 :goto_6
.end method

.method public final f(II)V
    .locals 2

    .prologue
    .line 238987
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollX()I

    move-result v0

    .line 238988
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollY()I

    move-result v1

    .line 238989
    invoke-virtual {p0, v0, v1, v0, v1}, Landroid/support/v7/widget/RecyclerView;->onScrollChanged(IIII)V

    .line 238990
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:LX/1OX;

    if-eqz v0, :cond_0

    .line 238991
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:LX/1OX;

    invoke-virtual {v0, p0, p1, p2}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 238992
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 238993
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 238994
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OX;

    invoke-virtual {v0, p0, p1, p2}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 238995
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 238996
    :cond_1
    return-void
.end method

.method public final focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 238978
    goto :goto_1

    .line 238979
    :cond_0
    :goto_0
    return-object v0

    .line 238980
    :goto_1
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    .line 238981
    invoke-virtual {v0, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 238982
    if-nez v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->e()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-nez v1, :cond_1

    .line 238983
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->b()V

    .line 238984
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, p2, v1, v2}, LX/1OR;->c(ILX/1Od;LX/1Ok;)Landroid/view/View;

    move-result-object v0

    .line 238985
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 238986
    :cond_1
    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final g(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 239211
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 239212
    iget-boolean v1, v0, LX/1a3;->c:Z

    if-nez v1, :cond_0

    .line 239213
    iget-object v0, v0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 239214
    :goto_0
    return-object v0

    .line 239215
    :cond_0
    iget-object v2, v0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 239216
    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 239217
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v4

    .line 239218
    :goto_1
    if-ge v3, v5, :cond_1

    .line 239219
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 239220
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3x6;

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v1, v6, p1, p0, v7}, LX/3x6;->a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 239221
    iget v1, v2, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->left:I

    .line 239222
    iget v1, v2, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->top:I

    .line 239223
    iget v1, v2, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->right:I

    .line 239224
    iget v1, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->bottom:I

    .line 239225
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 239226
    :cond_1
    iput-boolean v4, v0, LX/1a3;->c:Z

    move-object v0, v2

    .line 239227
    goto :goto_0
.end method

.method public final g()V
    .locals 5

    .prologue
    .line 238962
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v1

    .line 238963
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_6

    .line 238964
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    invoke-virtual {v2, v0}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v2

    .line 238965
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/1a1;->c()Z

    move-result v3

    if-nez v3, :cond_1

    .line 238966
    invoke-virtual {v2}, LX/1a1;->q()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, LX/1a1;->m()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 238967
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 238968
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238969
    :cond_2
    invoke-virtual {v2}, LX/1a1;->n()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 238970
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    iget v4, v2, LX/1a1;->b:I

    invoke-virtual {v3, v4}, LX/1OM;->getItemViewType(I)I

    move-result v3

    .line 238971
    iget v4, v2, LX/1a1;->e:I

    move v4, v4

    .line 238972
    if-ne v4, v3, :cond_5

    .line 238973
    invoke-virtual {v2}, LX/1a1;->o()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->A(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 238974
    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    iget v4, v2, LX/1a1;->b:I

    invoke-virtual {v3, v2, v4}, LX/1OM;->b(LX/1a1;I)V

    goto :goto_1

    .line 238975
    :cond_4
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_1

    .line 238976
    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 238977
    :cond_6
    return-void
.end method

.method public g_(I)V
    .locals 2

    .prologue
    .line 238955
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-eqz v0, :cond_0

    .line 238956
    :goto_0
    return-void

    .line 238957
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->c()V

    .line 238958
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_1

    .line 238959
    const-string v0, "RecyclerView"

    const-string v1, "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 238960
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0, p1}, LX/1OR;->e(I)V

    .line 238961
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    goto :goto_0
.end method

.method public generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 239172
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_0

    .line 239173
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView has no LayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239174
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->b()LX/1a3;

    move-result-object v0

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 239175
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_0

    .line 239176
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView has no LayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239177
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/1OR;->a(Landroid/content/Context;Landroid/util/AttributeSet;)LX/1a3;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 239178
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_0

    .line 239179
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView has no LayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239180
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0, p1}, LX/1OR;->a(Landroid/view/ViewGroup$LayoutParams;)LX/1a3;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()LX/1OM;
    .locals 1

    .prologue
    .line 239181
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    return-object v0
.end method

.method public getBaseline()I
    .locals 1

    .prologue
    .line 239182
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_0

    .line 239183
    const/4 v0, -0x1

    move v0, v0

    .line 239184
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->getBaseline()I

    move-result v0

    goto :goto_0
.end method

.method public final getChildDrawingOrder(II)I
    .locals 1

    .prologue
    .line 239185
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ag:LX/3x4;

    if-nez v0, :cond_0

    .line 239186
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->getChildDrawingOrder(II)I

    move-result v0

    .line 239187
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ag:LX/3x4;

    invoke-interface {v0, p1, p2}, LX/3x4;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method public getCompatAccessibilityDelegate()LX/1Ow;
    .locals 1

    .prologue
    .line 239188
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->af:LX/1Ow;

    return-object v0
.end method

.method public getItemAnimator()LX/1Of;
    .locals 1

    .prologue
    .line 239189
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    return-object v0
.end method

.method public getLayoutManager()LX/1OR;
    .locals 1

    .prologue
    .line 239190
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    return-object v0
.end method

.method public getMaxFlingVelocity()I
    .locals 1

    .prologue
    .line 239191
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    return v0
.end method

.method public getMinFlingVelocity()I
    .locals 1

    .prologue
    .line 239170
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->U:I

    return v0
.end method

.method public getRecycledViewPool()LX/1YF;
    .locals 1

    .prologue
    .line 239549
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0}, LX/1Od;->e()LX/1YF;

    move-result-object v0

    return-object v0
.end method

.method public getScrollState()I
    .locals 1

    .prologue
    .line 239498
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    return v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 239499
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 239500
    :goto_0
    return-void

    .line 239501
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_1

    .line 239502
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    const-string v1, "Cannot invalidate item decorations during a scroll or layout"

    invoke-virtual {v0, v1}, LX/1OR;->a(Ljava/lang/String;)V

    .line 239503
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->E()V

    .line 239504
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_0
.end method

.method public final hasNestedScrollingParent()Z
    .locals 1

    .prologue
    .line 239505
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:LX/1Oy;

    invoke-virtual {v0}, LX/1Oy;->b()Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 239506
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0}, LX/1On;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isAttachedToWindow()Z
    .locals 1

    .prologue
    .line 239507
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    return v0
.end method

.method public final isNestedScrollingEnabled()Z
    .locals 1

    .prologue
    .line 239508
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:LX/1Oy;

    .line 239509
    iget-boolean p0, v0, LX/1Oy;->c:Z

    move v0, p0

    .line 239510
    return v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2c

    const v1, -0xf460c5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 239511
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 239512
    iput v2, p0, Landroid/support/v7/widget/RecyclerView;->H:I

    .line 239513
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    .line 239514
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    .line 239515
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v1, :cond_0

    .line 239516
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v1, p0}, LX/1OR;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 239517
    :cond_0
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->ae:Z

    .line 239518
    const/16 v1, 0x2d

    const v2, 0x464a37dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2c

    const v1, -0x22bc5ab7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 239519
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 239520
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v1, :cond_0

    .line 239521
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    invoke-virtual {v1}, LX/1Of;->c()V

    .line 239522
    :cond_0
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    .line 239523
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->c()V

    .line 239524
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    .line 239525
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v1, :cond_1

    .line 239526
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v1, p0, v2}, LX/1OR;->b(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V

    .line 239527
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->am:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 239528
    const/16 v1, 0x2d

    const v2, 0x6335ade6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 239529
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 239530
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 239531
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 239532
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3x6;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, p1, p0, v3}, LX/3x6;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 239533
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 239534
    :cond_0
    return-void
.end method

.method public final onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 239535
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_1

    .line 239536
    :cond_0
    :goto_0
    return v4

    .line 239537
    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-nez v0, :cond_0

    .line 239538
    invoke-static {p1}, LX/2xd;->d(Landroid/view/MotionEvent;)I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 239539
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_0

    .line 239540
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239541
    const/16 v0, 0x9

    invoke-static {p1, v0}, LX/2xd;->e(Landroid/view/MotionEvent;I)F

    move-result v0

    neg-float v0, v0

    .line 239542
    :goto_1
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v2}, LX/1OR;->g()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 239543
    const/16 v2, 0xa

    invoke-static {p1, v2}, LX/2xd;->e(Landroid/view/MotionEvent;I)F

    move-result v2

    .line 239544
    :goto_2
    cmpl-float v3, v0, v1

    if-nez v3, :cond_2

    cmpl-float v1, v2, v1

    if-eqz v1, :cond_0

    .line 239545
    :cond_2
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollFactor()F

    move-result v1

    .line 239546
    mul-float/2addr v2, v1

    float-to-int v2, v2

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {p0, v2, v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(IILandroid/view/MotionEvent;)Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 239547
    goto :goto_1

    :cond_4
    move v2, v1

    .line 239548
    goto :goto_2
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v1, -0x1

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 239443
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-eqz v0, :cond_1

    move v2, v3

    .line 239444
    :cond_0
    :goto_0
    return v2

    .line 239445
    :cond_1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 239446
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->w()V

    goto :goto_0

    .line 239447
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_3

    move v2, v3

    .line 239448
    goto :goto_0

    .line 239449
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->g()Z

    move-result v0

    .line 239450
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v4}, LX/1OR;->h()Z

    move-result v4

    .line 239451
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    if-nez v5, :cond_4

    .line 239452
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v5

    iput-object v5, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    .line 239453
    :cond_4
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v5, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 239454
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v5

    .line 239455
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v6

    .line 239456
    packed-switch v5, :pswitch_data_0

    .line 239457
    :cond_5
    :goto_1
    :pswitch_0
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    if-eq v0, v2, :cond_0

    move v2, v3

    goto :goto_0

    .line 239458
    :pswitch_1
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Z

    if-eqz v1, :cond_6

    .line 239459
    iput-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->A:Z

    .line 239460
    :cond_6
    invoke-static {p1, v3}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 239461
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    add-float/2addr v1, v7

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 239462
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    add-float/2addr v1, v7

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    .line 239463
    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    const/4 v5, 0x2

    if-ne v1, v5, :cond_7

    .line 239464
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 239465
    invoke-static {p0, v2}, Landroid/support/v7/widget/RecyclerView;->setScrollState(Landroid/support/v7/widget/RecyclerView;I)V

    .line 239466
    :cond_7
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    aput v3, v5, v2

    aput v3, v1, v3

    .line 239467
    if-eqz v0, :cond_f

    move v0, v2

    .line 239468
    :goto_2
    if-eqz v4, :cond_8

    .line 239469
    or-int/lit8 v0, v0, 0x2

    .line 239470
    :cond_8
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->startNestedScroll(I)Z

    goto :goto_1

    .line 239471
    :pswitch_2
    invoke-static {p1, v6}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 239472
    invoke-static {p1, v6}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 239473
    invoke-static {p1, v6}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    goto :goto_1

    .line 239474
    :pswitch_3
    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-static {p1, v5}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v5

    .line 239475
    if-gez v5, :cond_9

    .line 239476
    const-string v0, "RecyclerView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error processing scroll; pointer index for id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 239477
    goto/16 :goto_0

    .line 239478
    :cond_9
    invoke-static {p1, v5}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v6

    add-float/2addr v6, v7

    float-to-int v6, v6

    .line 239479
    invoke-static {p1, v5}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v5

    add-float/2addr v5, v7

    float-to-int v5, v5

    .line 239480
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    if-eq v7, v2, :cond_5

    .line 239481
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    sub-int/2addr v6, v7

    .line 239482
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    sub-int/2addr v5, v7

    .line 239483
    if-eqz v0, :cond_e

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-le v0, v7, :cond_e

    .line 239484
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-gez v6, :cond_c

    move v0, v1

    :goto_3
    mul-int/2addr v0, v8

    add-int/2addr v0, v7

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    move v0, v2

    .line 239485
    :goto_4
    if-eqz v4, :cond_a

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v6, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-le v4, v6, :cond_a

    .line 239486
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-gez v5, :cond_d

    :goto_5
    mul-int/2addr v1, v4

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    move v0, v2

    .line 239487
    :cond_a
    if-eqz v0, :cond_5

    .line 239488
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 239489
    if-eqz v0, :cond_b

    .line 239490
    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 239491
    :cond_b
    invoke-static {p0, v2}, Landroid/support/v7/widget/RecyclerView;->setScrollState(Landroid/support/v7/widget/RecyclerView;I)V

    goto/16 :goto_1

    :cond_c
    move v0, v2

    .line 239492
    goto :goto_3

    :cond_d
    move v1, v2

    .line 239493
    goto :goto_5

    .line 239494
    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 239495
    :pswitch_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 239496
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->stopNestedScroll()V

    goto/16 :goto_1

    .line 239497
    :pswitch_6
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->w()V

    goto/16 :goto_1

    :cond_e
    move v0, v3

    goto :goto_4

    :cond_f
    move v0, v3

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 239550
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->b()V

    .line 239551
    const-string v0, "RV OnLayout"

    const v1, 0x683bbb5c

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 239552
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->f()V

    .line 239553
    const v0, -0xa79872b

    invoke-static {v0}, LX/03q;->a(I)V

    .line 239554
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 239555
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    .line 239556
    return-void
.end method

.method public onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 239557
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    if-eqz v0, :cond_0

    .line 239558
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->b()V

    .line 239559
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->D()V

    .line 239560
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-boolean v0, v0, LX/1Ok;->m:Z

    if-eqz v0, :cond_1

    .line 239561
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    const/4 v1, 0x1

    .line 239562
    iput-boolean v1, v0, LX/1Ok;->k:Z

    .line 239563
    :goto_0
    iput-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    .line 239564
    invoke-virtual {p0, v3}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 239565
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v0, :cond_2

    .line 239566
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    iput v1, v0, LX/1Ok;->e:I

    .line 239567
    :goto_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_3

    .line 239568
    invoke-static {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->h(Landroid/support/v7/widget/RecyclerView;II)V

    .line 239569
    :goto_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239570
    iput-boolean v3, v0, LX/1Ok;->k:Z

    .line 239571
    return-void

    .line 239572
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:LX/1On;

    invoke-virtual {v0}, LX/1On;->e()V

    .line 239573
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 239574
    iput-boolean v3, v0, LX/1Ok;->k:Z

    .line 239575
    goto :goto_0

    .line 239576
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iput v3, v0, LX/1Ok;->e:I

    goto :goto_1

    .line 239577
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, v1, v2, p1, p2}, LX/1OR;->a(LX/1Od;LX/1Ok;II)V

    goto :goto_2
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 239578
    check-cast p1, Landroid/support/v7/widget/RecyclerView$SavedState;

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    .line 239579
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 239580
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$SavedState;->a:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 239581
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$SavedState;->a:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, LX/1OR;->a(Landroid/os/Parcelable;)V

    .line 239582
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 239583
    new-instance v0, Landroid/support/v7/widget/RecyclerView$SavedState;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/RecyclerView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 239584
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    if-eqz v1, :cond_0

    .line 239585
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    .line 239586
    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView$SavedState;->a$redex0(Landroid/support/v7/widget/RecyclerView$SavedState;Landroid/support/v7/widget/RecyclerView$SavedState;)V

    .line 239587
    :goto_0
    return-object v0

    .line 239588
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v1, :cond_1

    .line 239589
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v1}, LX/1OR;->f()Landroid/os/Parcelable;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView$SavedState;->a:Landroid/os/Parcelable;

    goto :goto_0

    .line 239590
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView$SavedState;->a:Landroid/os/Parcelable;

    goto :goto_0
.end method

.method public onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x24f261f8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 239591
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 239592
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 239593
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->u()V

    .line 239594
    :cond_1
    const/16 v1, 0x2d

    const v2, 0x3dfa3793

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/high16 v11, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    const/4 v3, 0x1

    const v0, -0x4807c5b0

    invoke-static {v4, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v5

    .line 239595
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Z

    if-eqz v0, :cond_1

    .line 239596
    :cond_0
    const v0, -0x16117194

    invoke-static {v4, v4, v0, v5}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 239597
    :goto_0
    return v2

    .line 239598
    :cond_1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 239599
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->w()V

    .line 239600
    const v0, 0xbeef0a5

    invoke-static {v0, v5}, LX/02F;->a(II)V

    move v2, v3

    goto :goto_0

    .line 239601
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v0, :cond_3

    .line 239602
    const v0, -0x7311da5c

    invoke-static {v0, v5}, LX/02F;->a(II)V

    goto :goto_0

    .line 239603
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->g()Z

    move-result v6

    .line 239604
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->h()Z

    move-result v7

    .line 239605
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    if-nez v0, :cond_4

    .line 239606
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    .line 239607
    :cond_4
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v8

    .line 239608
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 239609
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v4

    .line 239610
    if-nez v0, :cond_5

    .line 239611
    iget-object v9, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    iget-object v10, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    aput v2, v10, v3

    aput v2, v9, v2

    .line 239612
    :cond_5
    iget-object v9, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    aget v9, v9, v2

    int-to-float v9, v9

    iget-object v10, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    aget v10, v10, v3

    int-to-float v10, v10

    invoke-virtual {v8, v9, v10}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 239613
    packed-switch v0, :pswitch_data_0

    .line 239614
    :cond_6
    :goto_1
    :pswitch_0
    if-nez v2, :cond_7

    .line 239615
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0, v8}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 239616
    :cond_7
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 239617
    const v0, 0x3d3e0f1f

    invoke-static {v0, v5}, LX/02F;->a(II)V

    move v2, v3

    goto :goto_0

    .line 239618
    :pswitch_1
    invoke-static {p1, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 239619
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 239620
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    .line 239621
    if-eqz v6, :cond_18

    move v0, v3

    .line 239622
    :goto_2
    if-eqz v7, :cond_8

    .line 239623
    or-int/lit8 v0, v0, 0x2

    .line 239624
    :cond_8
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->startNestedScroll(I)Z

    goto :goto_1

    .line 239625
    :pswitch_2
    invoke-static {p1, v4}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 239626
    invoke-static {p1, v4}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 239627
    invoke-static {p1, v4}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    goto :goto_1

    .line 239628
    :pswitch_3
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 239629
    if-gez v0, :cond_9

    .line 239630
    const-string v0, "RecyclerView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Error processing scroll; pointer index for id "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239631
    const v0, -0x42a846c3

    invoke-static {v0, v5}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 239632
    :cond_9
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    add-float/2addr v1, v11

    float-to-int v9, v1

    .line 239633
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v10, v0

    .line 239634
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    sub-int v1, v0, v9

    .line 239635
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    sub-int/2addr v0, v10

    .line 239636
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ak:[I

    iget-object v11, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    invoke-virtual {p0, v1, v0, v4, v11}, Landroid/support/v7/widget/RecyclerView;->dispatchNestedPreScroll(II[I[I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 239637
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ak:[I

    aget v4, v4, v2

    sub-int/2addr v1, v4

    .line 239638
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ak:[I

    aget v4, v4, v3

    sub-int/2addr v0, v4

    .line 239639
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    aget v4, v4, v2

    int-to-float v4, v4

    iget-object v11, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    aget v11, v11, v3

    int-to-float v11, v11

    invoke-virtual {v8, v4, v11}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 239640
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    aget v11, v4, v2

    iget-object v12, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    aget v12, v12, v2

    add-int/2addr v11, v12

    aput v11, v4, v2

    .line 239641
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->al:[I

    aget v11, v4, v3

    iget-object v12, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    aget v12, v12, v3

    add-int/2addr v11, v12

    aput v11, v4, v3

    .line 239642
    :cond_a
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    if-eq v4, v3, :cond_d

    .line 239643
    if-eqz v6, :cond_17

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v11, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-le v4, v11, :cond_17

    .line 239644
    if-lez v1, :cond_e

    .line 239645
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    sub-int/2addr v1, v4

    :goto_3
    move v4, v3

    .line 239646
    :goto_4
    if-eqz v7, :cond_b

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v11

    iget v12, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-le v11, v12, :cond_b

    .line 239647
    if-lez v0, :cond_f

    .line 239648
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    sub-int/2addr v0, v4

    :goto_5
    move v4, v3

    .line 239649
    :cond_b
    if-eqz v4, :cond_d

    .line 239650
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    .line 239651
    if-eqz v4, :cond_c

    .line 239652
    invoke-interface {v4, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 239653
    :cond_c
    invoke-static {p0, v3}, Landroid/support/v7/widget/RecyclerView;->setScrollState(Landroid/support/v7/widget/RecyclerView;I)V

    .line 239654
    :cond_d
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    if-ne v4, v3, :cond_6

    .line 239655
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    aget v4, v4, v2

    sub-int v4, v9, v4

    iput v4, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    .line 239656
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->aj:[I

    aget v4, v4, v3

    sub-int v4, v10, v4

    iput v4, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    .line 239657
    if-eqz v6, :cond_10

    :goto_6
    if-eqz v7, :cond_11

    :goto_7
    invoke-direct {p0, v1, v0, v8}, Landroid/support/v7/widget/RecyclerView;->a(IILandroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 239658
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto/16 :goto_1

    .line 239659
    :cond_e
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    add-int/2addr v1, v4

    goto :goto_3

    .line 239660
    :cond_f
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    add-int/2addr v0, v4

    goto :goto_5

    :cond_10
    move v1, v2

    .line 239661
    goto :goto_6

    :cond_11
    move v0, v2

    goto :goto_7

    .line 239662
    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 239663
    :pswitch_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0, v8}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 239664
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    const/16 v4, 0x3e8

    iget v9, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    int-to-float v9, v9

    invoke-virtual {v0, v4, v9}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 239665
    if-eqz v6, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-static {v0, v4}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    neg-float v0, v0

    move v4, v0

    .line 239666
    :goto_8
    if-eqz v7, :cond_16

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    iget v6, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-static {v0, v6}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    neg-float v0, v0

    .line 239667
    :goto_9
    cmpl-float v6, v4, v1

    if-nez v6, :cond_12

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_13

    :cond_12
    float-to-int v1, v4

    float-to-int v0, v0

    invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/RecyclerView;->b(II)Z

    move-result v0

    if-nez v0, :cond_14

    .line 239668
    :cond_13
    invoke-static {p0, v2}, Landroid/support/v7/widget/RecyclerView;->setScrollState(Landroid/support/v7/widget/RecyclerView;I)V

    .line 239669
    :cond_14
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->v()V

    move v2, v3

    .line 239670
    goto/16 :goto_1

    :cond_15
    move v4, v1

    .line 239671
    goto :goto_8

    :cond_16
    move v0, v1

    .line 239672
    goto :goto_9

    .line 239673
    :pswitch_6
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->w()V

    goto/16 :goto_1

    :cond_17
    move v4, v2

    goto/16 :goto_4

    :cond_18
    move v0, v2

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public final removeDetachedView(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 239674
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 239675
    if-eqz v0, :cond_0

    .line 239676
    invoke-virtual {v0}, LX/1a1;->r()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 239677
    invoke-virtual {v0}, LX/1a1;->l()V

    .line 239678
    :cond_0
    invoke-static {p0, p1}, Landroid/support/v7/widget/RecyclerView;->i(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 239679
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->removeDetachedView(Landroid/view/View;Z)V

    .line 239680
    return-void

    .line 239681
    :cond_1
    invoke-virtual {v0}, LX/1a1;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 239682
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called removeDetachedView with a view which is not flagged as tmp detached."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 239683
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0, p0, p1, p2}, LX/1OR;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    .line 239684
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 239685
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 239686
    instance-of v2, v0, LX/1a3;

    if-eqz v2, :cond_0

    .line 239687
    check-cast v0, LX/1a3;

    .line 239688
    iget-boolean v2, v0, LX/1a3;->c:Z

    if-nez v2, :cond_0

    .line 239689
    iget-object v0, v0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 239690
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 239691
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->right:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 239692
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 239693
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v3

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 239694
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    invoke-virtual {p0, p2, v0}, Landroid/support/v7/widget/RecyclerView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 239695
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 239696
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v2, v0}, Landroid/support/v7/widget/RecyclerView;->requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z

    .line 239697
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 239698
    return-void

    :cond_2
    move v0, v1

    .line 239699
    goto :goto_0
.end method

.method public final requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 1

    .prologue
    .line 239700
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0, p0, p1, p2, p3}, LX/1OR;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;Z)Z

    move-result v0

    return v0
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 3

    .prologue
    .line 239380
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 239381
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 239382
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OH;

    .line 239383
    invoke-interface {v0, p1}, LX/1OH;->a(Z)V

    .line 239384
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 239385
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 239386
    return-void
.end method

.method public final requestLayout()V
    .locals 1

    .prologue
    .line 239335
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-nez v0, :cond_0

    .line 239336
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 239337
    :goto_0
    return-void

    .line 239338
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    goto :goto_0
.end method

.method public final scrollBy(II)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 239339
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-nez v1, :cond_1

    .line 239340
    const-string v0, "RecyclerView"

    const-string v1, "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239341
    :cond_0
    :goto_0
    return-void

    .line 239342
    :cond_1
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-nez v1, :cond_0

    .line 239343
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v1}, LX/1OR;->g()Z

    move-result v1

    .line 239344
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v2}, LX/1OR;->h()Z

    move-result v2

    .line 239345
    if-nez v1, :cond_2

    if-eqz v2, :cond_0

    .line 239346
    :cond_2
    if-eqz v1, :cond_3

    :goto_1
    if-eqz v2, :cond_4

    :goto_2
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/RecyclerView;->a(IILandroid/view/MotionEvent;)Z

    goto :goto_0

    :cond_3
    move p1, v0

    goto :goto_1

    :cond_4
    move p2, v0

    goto :goto_2
.end method

.method public final scrollTo(II)V
    .locals 2

    .prologue
    .line 239347
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "RecyclerView does not support scrolling to an absolute position."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 239348
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239349
    :goto_0
    return-void

    .line 239350
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public setAccessibilityDelegateCompat(LX/1Ow;)V
    .locals 1

    .prologue
    .line 239351
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->af:LX/1Ow;

    .line 239352
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->af:LX/1Ow;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 239353
    return-void
.end method

.method public setAdapter(LX/1OM;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 239354
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutFrozen(Z)V

    .line 239355
    const/4 v0, 0x1

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OM;ZZ)V

    .line 239356
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 239357
    return-void
.end method

.method public setChildDrawingOrderCallback(LX/3x4;)V
    .locals 1

    .prologue
    .line 239358
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ag:LX/3x4;

    if-ne p1, v0, :cond_0

    .line 239359
    :goto_0
    return-void

    .line 239360
    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->ag:LX/3x4;

    .line 239361
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ag:LX/3x4;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setChildrenDrawingOrderEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setClipToPadding(Z)V
    .locals 1

    .prologue
    .line 239362
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eq p1, v0, :cond_0

    .line 239363
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->u()V

    .line 239364
    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    .line 239365
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 239366
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    if-eqz v0, :cond_1

    .line 239367
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 239368
    :cond_1
    return-void
.end method

.method public setHasFixedSize(Z)V
    .locals 0

    .prologue
    .line 239369
    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 239370
    return-void
.end method

.method public setItemAnimator(LX/1Of;)V
    .locals 2

    .prologue
    .line 239371
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v0, :cond_0

    .line 239372
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    invoke-virtual {v0}, LX/1Of;->c()V

    .line 239373
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    const/4 v1, 0x0

    .line 239374
    iput-object v1, v0, LX/1Of;->a:LX/1Om;

    .line 239375
    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    .line 239376
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v0, :cond_1

    .line 239377
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->ad:LX/1Om;

    .line 239378
    iput-object v1, v0, LX/1Of;->a:LX/1Om;

    .line 239379
    :cond_1
    return-void
.end method

.method public setItemViewCacheSize(I)V
    .locals 1

    .prologue
    .line 239333
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, p1}, LX/1Od;->a(I)V

    .line 239334
    return-void
.end method

.method public setLayoutFrozen(Z)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 239387
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-eq p1, v0, :cond_1

    .line 239388
    const-string v0, "Do not setLayoutFrozen in layout or scroll"

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 239389
    if-nez p1, :cond_2

    .line 239390
    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    .line 239391
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v0, :cond_0

    .line 239392
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 239393
    :cond_0
    iput-boolean v7, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    .line 239394
    :cond_1
    :goto_0
    return-void

    .line 239395
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 239396
    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 239397
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 239398
    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    .line 239399
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Z

    .line 239400
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->c()V

    goto :goto_0
.end method

.method public setLayoutManager(LX/1OR;)V
    .locals 4

    .prologue
    .line 239401
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-ne p1, v0, :cond_0

    .line 239402
    :goto_0
    return-void

    .line 239403
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    if-eqz v0, :cond_2

    .line 239404
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    if-eqz v0, :cond_1

    .line 239405
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, p0, v1}, LX/1OR;->b(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V

    .line 239406
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1OR;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 239407
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0}, LX/1Od;->a()V

    .line 239408
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    .line 239409
    iget-object v1, v0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {v1}, LX/1Ov;->a()V

    .line 239410
    iget-object v1, v0, LX/1Os;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_1
    if-ltz v2, :cond_3

    .line 239411
    iget-object v3, v0, LX/1Os;->a:LX/1Ou;

    iget-object v1, v0, LX/1Os;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v3, v1}, LX/1Ou;->d(Landroid/view/View;)V

    .line 239412
    iget-object v1, v0, LX/1Os;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 239413
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_1

    .line 239414
    :cond_3
    iget-object v1, v0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v1}, LX/1Ou;->b()V

    .line 239415
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    .line 239416
    if-eqz p1, :cond_5

    .line 239417
    iget-object v0, p1, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_4

    .line 239418
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LayoutManager "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is already attached to a RecyclerView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239419
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0, p0}, LX/1OR;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 239420
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    if-eqz v0, :cond_5

    .line 239421
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0, p0}, LX/1OR;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 239422
    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto/16 :goto_0
.end method

.method public setNestedScrollingEnabled(Z)V
    .locals 1

    .prologue
    .line 239423
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:LX/1Oy;

    invoke-virtual {v0, p1}, LX/1Oy;->a(Z)V

    .line 239424
    return-void
.end method

.method public setOnScrollListener(LX/1OX;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 239425
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->ab:LX/1OX;

    .line 239426
    return-void
.end method

.method public setRecycledViewPool(LX/1YF;)V
    .locals 1

    .prologue
    .line 239427
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    invoke-virtual {v0, p1}, LX/1Od;->a(LX/1YF;)V

    .line 239428
    return-void
.end method

.method public setRecyclerListener(LX/1OU;)V
    .locals 0

    .prologue
    .line 239429
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->q:LX/1OU;

    .line 239430
    return-void
.end method

.method public setScrollingTouchSlop(I)V
    .locals 4

    .prologue
    .line 239431
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 239432
    packed-switch p1, :pswitch_data_0

    .line 239433
    const-string v1, "RecyclerView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setScrollingTouchSlop(): bad argument constant "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; using default value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 239434
    :pswitch_0
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    .line 239435
    :goto_0
    return-void

    .line 239436
    :pswitch_1
    invoke-static {v0}, LX/0iP;->a(Landroid/view/ViewConfiguration;)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setViewCacheExtension(LX/1PB;)V
    .locals 1

    .prologue
    .line 239437
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    .line 239438
    iput-object p1, v0, LX/1Od;->h:LX/1PB;

    .line 239439
    return-void
.end method

.method public final startNestedScroll(I)Z
    .locals 1

    .prologue
    .line 239440
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:LX/1Oy;

    invoke-virtual {v0, p1}, LX/1Oy;->a(I)Z

    move-result v0

    return v0
.end method

.method public final stopNestedScroll()V
    .locals 1

    .prologue
    .line 239441
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:LX/1Oy;

    invoke-virtual {v0}, LX/1Oy;->c()V

    .line 239442
    return-void
.end method
