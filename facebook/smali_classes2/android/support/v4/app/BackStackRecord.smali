.class public final Landroid/support/v4/app/BackStackRecord;
.super LX/0hH;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:LX/0jz;

.field public b:LX/0xN;

.field public c:LX/0xN;

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:Z

.field public l:Z

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:I

.field public p:I

.field public q:Ljava/lang/CharSequence;

.field public r:I

.field public s:Ljava/lang/CharSequence;

.field public t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0jz;)V
    .locals 1

    .prologue
    .line 162696
    invoke-direct {p0}, LX/0hH;-><init>()V

    .line 162697
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->l:Z

    .line 162698
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    .line 162699
    iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    .line 162700
    return-void
.end method

.method private a(Z)I
    .locals 2

    .prologue
    .line 162701
    iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->n:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "commit already called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162702
    :cond_0
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_1

    .line 162703
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Commit: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162704
    new-instance v0, LX/0xJ;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, LX/0xJ;-><init>(Ljava/lang/String;)V

    .line 162705
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 162706
    const-string v0, "  "

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/BackStackRecord;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 162707
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->n:Z

    .line 162708
    iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->k:Z

    if-eqz v0, :cond_2

    .line 162709
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v0, p0}, LX/0jz;->a(Landroid/support/v4/app/BackStackRecord;)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    .line 162710
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v0, p0, p1}, LX/0jz;->a(Ljava/lang/Runnable;Z)V

    .line 162711
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    return v0

    .line 162712
    :cond_2
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    goto :goto_0
.end method

.method private a(LX/0xK;Landroid/support/v4/app/Fragment;Z)LX/026;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0xK;",
            "Landroid/support/v4/app/Fragment;",
            "Z)",
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 162713
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    .line 162714
    iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->t:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 162715
    iget-object v1, p2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 162716
    invoke-static {v0, v1}, LX/0xL;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 162717
    if-eqz p3, :cond_2

    .line 162718
    iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/026;->a(Ljava/util/Collection;)Z

    .line 162719
    :cond_0
    :goto_0
    if-eqz p3, :cond_3

    .line 162720
    iget-object v1, p2, Landroid/support/v4/app/Fragment;->mEnterTransitionCallback:LX/0xM;

    if-eqz v1, :cond_1

    .line 162721
    :cond_1
    invoke-static {p0, p1, v0, v3}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/support/v4/app/BackStackRecord;LX/0xK;LX/026;Z)V

    .line 162722
    :goto_1
    return-object v0

    .line 162723
    :cond_2
    iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->t:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/BackStackRecord;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;LX/026;)LX/026;

    move-result-object v0

    goto :goto_0

    .line 162724
    :cond_3
    iget-object v1, p2, Landroid/support/v4/app/Fragment;->mExitTransitionCallback:LX/0xM;

    if-eqz v1, :cond_4

    .line 162725
    :cond_4
    invoke-static {p1, v0, v3}, Landroid/support/v4/app/BackStackRecord;->b(LX/0xK;LX/026;Z)V

    goto :goto_1
.end method

.method public static a(Ljava/util/ArrayList;Ljava/util/ArrayList;LX/026;)LX/026;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)",
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162726
    invoke-virtual {p2}, LX/01J;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162727
    :goto_0
    return-object p2

    .line 162728
    :cond_0
    new-instance v1, LX/026;

    invoke-direct {v1}, LX/026;-><init>()V

    .line 162729
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 162730
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 162731
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 162732
    if-eqz v0, :cond_1

    .line 162733
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162734
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object p2, v1

    .line 162735
    goto :goto_0
.end method

.method private a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)LX/0xK;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;Z)",
            "LX/0xK;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 162736
    new-instance v2, LX/0xK;

    invoke-direct {v2, p0}, LX/0xK;-><init>(Landroid/support/v4/app/BackStackRecord;)V

    .line 162737
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v1, v1, LX/0jz;->n:LX/0k3;

    invoke-virtual {v1}, LX/0k3;->h()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, v2, LX/0xK;->d:Landroid/view/View;

    move v6, v7

    move v8, v7

    .line 162738
    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 162739
    invoke-virtual {p1, v6}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    move-object v0, p0

    move v3, p3

    move-object v4, p1

    move-object v5, p2

    .line 162740
    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/BackStackRecord;->a(ILX/0xK;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v1, v9

    .line 162741
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v8, v1

    goto :goto_0

    .line 162742
    :cond_0
    :goto_2
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v7, v0, :cond_2

    .line 162743
    invoke-virtual {p2, v7}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 162744
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, p0

    move v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/BackStackRecord;->a(ILX/0xK;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v8, v9

    .line 162745
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 162746
    :cond_2
    if-nez v8, :cond_3

    .line 162747
    const/4 v2, 0x0

    .line 162748
    :cond_3
    return-object v2

    :cond_4
    move v1, v8

    goto :goto_1
.end method

.method private static a(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 162749
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 162750
    :cond_0
    const/4 v0, 0x0

    .line 162751
    :goto_0
    return-object v0

    :cond_1
    if-eqz p2, :cond_2

    .line 162752
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSharedElementReturnTransition:Ljava/lang/Object;

    sget-object p0, Landroid/support/v4/app/Fragment;->USE_DEFAULT_TRANSITION:Ljava/lang/Object;

    if-ne v0, p0, :cond_3

    .line 162753
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSharedElementEnterTransition:Ljava/lang/Object;

    move-object v0, v0

    .line 162754
    :goto_1
    move-object v0, v0

    .line 162755
    :goto_2
    invoke-static {v0}, LX/0xL;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 162756
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mSharedElementEnterTransition:Ljava/lang/Object;

    move-object v0, v0

    .line 162757
    goto :goto_2

    :cond_3
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mSharedElementReturnTransition:Ljava/lang/Object;

    goto :goto_1
.end method

.method private static a(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 162758
    if-nez p0, :cond_0

    .line 162759
    const/4 v0, 0x0

    .line 162760
    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    .line 162761
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mReenterTransition:Ljava/lang/Object;

    sget-object p1, Landroid/support/v4/app/Fragment;->USE_DEFAULT_TRANSITION:Ljava/lang/Object;

    if-ne v0, p1, :cond_2

    .line 162762
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mExitTransition:Ljava/lang/Object;

    move-object v0, v0

    .line 162763
    :goto_1
    move-object v0, v0

    .line 162764
    :goto_2
    invoke-static {v0}, LX/0xL;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 162765
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mEnterTransition:Ljava/lang/Object;

    move-object v0, v0

    .line 162766
    goto :goto_2

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mReenterTransition:Ljava/lang/Object;

    goto :goto_1
.end method

.method private static a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;Ljava/util/ArrayList;LX/026;Landroid/view/View;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Landroid/support/v4/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 162767
    if-eqz p0, :cond_2

    .line 162768
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 162769
    if-eqz p0, :cond_1

    .line 162770
    invoke-static {p2, v0}, LX/0xL;->b(Ljava/util/ArrayList;Landroid/view/View;)V

    .line 162771
    if-eqz p3, :cond_0

    .line 162772
    invoke-interface {p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 162773
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 162774
    const/4 p0, 0x0

    .line 162775
    :cond_1
    :goto_0
    move-object p0, p0

    .line 162776
    :cond_2
    return-object p0

    .line 162777
    :cond_3
    invoke-virtual {p2, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object p1, p0

    .line 162778
    check-cast p1, Landroid/transition/Transition;

    invoke-static {p1, p2}, LX/0xL;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 162779
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iput-object v0, p2, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    .line 162780
    if-eqz p3, :cond_1

    .line 162781
    iget-object v0, p2, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 162782
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t change tag of fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162783
    :cond_0
    iput-object p3, p2, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    .line 162784
    :cond_1
    if-eqz p1, :cond_3

    .line 162785
    iget v0, p2, Landroid/support/v4/app/Fragment;->mFragmentId:I

    if-eqz v0, :cond_2

    iget v0, p2, Landroid/support/v4/app/Fragment;->mFragmentId:I

    if-eq v0, p1, :cond_2

    .line 162786
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t change container ID of fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/support/v4/app/Fragment;->mFragmentId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162787
    :cond_2
    iput p1, p2, Landroid/support/v4/app/Fragment;->mFragmentId:I

    iput p1, p2, Landroid/support/v4/app/Fragment;->mContainerId:I

    .line 162788
    :cond_3
    new-instance v0, LX/0xN;

    invoke-direct {v0}, LX/0xN;-><init>()V

    .line 162789
    iput p4, v0, LX/0xN;->c:I

    .line 162790
    iput-object p2, v0, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162791
    invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->a(LX/0xN;)V

    .line 162792
    return-void
.end method

.method public static a(LX/026;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 162793
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 162794
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/01J;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 162795
    invoke-virtual {p0, v0}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162796
    invoke-virtual {p0, v0, p2}, LX/01J;->a(ILjava/lang/Object;)Ljava/lang/Object;

    .line 162797
    :cond_0
    :goto_1
    return-void

    .line 162798
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162799
    :cond_2
    invoke-virtual {p0, p1, p2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private a(LX/0xK;Landroid/view/View;Ljava/lang/Object;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLjava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0xK;",
            "Landroid/view/View;",
            "Ljava/lang/Object;",
            "Landroid/support/v4/app/Fragment;",
            "Landroid/support/v4/app/Fragment;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162800
    invoke-virtual {p2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v9

    new-instance v0, LX/0xO;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p7

    move-object v5, p1

    move/from16 v6, p6

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, LX/0xO;-><init>(Landroid/support/v4/app/BackStackRecord;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;LX/0xK;ZLandroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v9, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 162801
    return-void
.end method

.method public static a(Landroid/support/v4/app/BackStackRecord;LX/0xK;LX/026;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0xK;",
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 162895
    iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    move v2, v0

    :goto_0
    move v3, v0

    .line 162896
    :goto_1
    if-ge v3, v2, :cond_3

    .line 162897
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 162898
    iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 162899
    invoke-virtual {p2, v1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 162900
    if-eqz v1, :cond_0

    .line 162901
    invoke-static {v1}, LX/0xL;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 162902
    if-eqz p3, :cond_2

    .line 162903
    iget-object v4, p1, LX/0xK;->a:LX/026;

    invoke-static {v4, v0, v1}, Landroid/support/v4/app/BackStackRecord;->a(LX/026;Ljava/lang/String;Ljava/lang/String;)V

    .line 162904
    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 162905
    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    goto :goto_0

    .line 162906
    :cond_2
    iget-object v4, p1, LX/0xK;->a:LX/026;

    invoke-static {v4, v1, v0}, Landroid/support/v4/app/BackStackRecord;->a(LX/026;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 162907
    :cond_3
    return-void
.end method

.method public static a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/support/v4/app/Fragment;",
            ")V"
        }
    .end annotation

    .prologue
    .line 162802
    if-eqz p1, :cond_0

    .line 162803
    iget v0, p1, Landroid/support/v4/app/Fragment;->mContainerId:I

    .line 162804
    if-eqz v0, :cond_0

    .line 162805
    iget-boolean v1, p1, Landroid/support/v4/app/Fragment;->mHidden:Z

    move v1, v1

    .line 162806
    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162807
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 162808
    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 162809
    invoke-virtual {p0, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 162810
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;LX/0xK;ILjava/lang/Object;)V
    .locals 7

    .prologue
    .line 162811
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v6

    new-instance v0, LX/0xP;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/0xP;-><init>(Landroid/support/v4/app/BackStackRecord;Landroid/view/View;LX/0xK;ILjava/lang/Object;)V

    invoke-virtual {v6, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 162812
    return-void
.end method

.method private a(ILX/0xK;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z
    .locals 32
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0xK;",
            "Z",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 162813
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v4, v4, LX/0jz;->o:LX/0k1;

    move/from16 v0, p1

    invoke-interface {v4, v0}, LX/0k1;->a(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 162814
    if-nez v6, :cond_0

    .line 162815
    const/4 v4, 0x0

    .line 162816
    :goto_0
    return v4

    .line 162817
    :cond_0
    move-object/from16 v0, p5

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/support/v4/app/Fragment;

    .line 162818
    move-object/from16 v0, p4

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/support/v4/app/Fragment;

    .line 162819
    move/from16 v0, p3

    invoke-static {v8, v0}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;

    move-result-object v12

    .line 162820
    move/from16 v0, p3

    invoke-static {v8, v9, v0}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;

    move-result-object v7

    .line 162821
    move/from16 v0, p3

    invoke-static {v9, v0}, Landroid/support/v4/app/BackStackRecord;->b(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;

    move-result-object v10

    .line 162822
    if-nez v12, :cond_1

    if-nez v7, :cond_1

    if-nez v10, :cond_1

    .line 162823
    const/4 v4, 0x0

    goto :goto_0

    .line 162824
    :cond_1
    const/4 v4, 0x0

    .line 162825
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 162826
    if-eqz v7, :cond_2

    .line 162827
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v9, v2}, Landroid/support/v4/app/BackStackRecord;->a(LX/0xK;Landroid/support/v4/app/Fragment;Z)LX/026;

    move-result-object v4

    .line 162828
    move-object/from16 v0, p2

    iget-object v5, v0, LX/0xK;->d:Landroid/view/View;

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162829
    invoke-virtual {v4}, LX/026;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 162830
    if-eqz p3, :cond_7

    iget-object v5, v9, Landroid/support/v4/app/Fragment;->mEnterTransitionCallback:LX/0xM;

    .line 162831
    :goto_1
    if-eqz v5, :cond_2

    .line 162832
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v4}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 162833
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v4}, LX/026;->values()Ljava/util/Collection;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 162834
    :cond_2
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 162835
    move-object/from16 v0, p2

    iget-object v5, v0, LX/0xK;->d:Landroid/view/View;

    move-object/from16 v0, v26

    invoke-static {v10, v9, v0, v4, v5}, Landroid/support/v4/app/BackStackRecord;->a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;Ljava/util/ArrayList;LX/026;Landroid/view/View;)Ljava/lang/Object;

    move-result-object v25

    .line 162836
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    if-eqz v5, :cond_4

    if-eqz v4, :cond_4

    .line 162837
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 162838
    if-eqz v4, :cond_4

    .line 162839
    if-eqz v25, :cond_3

    .line 162840
    move-object/from16 v0, v25

    invoke-static {v0, v4}, LX/0xL;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 162841
    :cond_3
    if-eqz v7, :cond_4

    .line 162842
    invoke-static {v7, v4}, LX/0xL;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 162843
    :cond_4
    new-instance v15, LX/0xQ;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v8}, LX/0xQ;-><init>(Landroid/support/v4/app/BackStackRecord;Landroid/support/v4/app/Fragment;)V

    .line 162844
    if-eqz v7, :cond_5

    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move/from16 v10, p3

    .line 162845
    invoke-direct/range {v4 .. v11}, Landroid/support/v4/app/BackStackRecord;->a(LX/0xK;Landroid/view/View;Ljava/lang/Object;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLjava/util/ArrayList;)V

    .line 162846
    :cond_5
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 162847
    new-instance v20, LX/026;

    invoke-direct/range {v20 .. v20}, LX/026;-><init>()V

    .line 162848
    if-eqz p3, :cond_8

    invoke-virtual {v8}, Landroid/support/v4/app/Fragment;->getAllowReturnTransitionOverlap()Z

    move-result v4

    .line 162849
    :goto_2
    move-object/from16 v0, v25

    invoke-static {v12, v0, v7, v4}, LX/0xL;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v29

    .line 162850
    if-eqz v29, :cond_6

    .line 162851
    move-object/from16 v0, p2

    iget-object v0, v0, LX/0xK;->d:Landroid/view/View;

    move-object/from16 v16, v0

    move-object/from16 v0, p2

    iget-object v0, v0, LX/0xK;->c:LX/0xS;

    move-object/from16 v17, v0

    move-object/from16 v0, p2

    iget-object v0, v0, LX/0xK;->a:LX/026;

    move-object/from16 v18, v0

    move-object v13, v7

    move-object v14, v6

    move-object/from16 v21, v11

    invoke-static/range {v12 .. v21}, LX/0xL;->a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;LX/0xR;Landroid/view/View;LX/0xS;Ljava/util/Map;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/ArrayList;)V

    .line 162852
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    move-object/from16 v3, v29

    invoke-direct {v0, v6, v1, v2, v3}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/view/View;LX/0xK;ILjava/lang/Object;)V

    .line 162853
    move-object/from16 v0, p2

    iget-object v4, v0, LX/0xK;->d:Landroid/view/View;

    const/4 v5, 0x1

    move-object/from16 v0, v29

    invoke-static {v0, v4, v5}, LX/0xL;->a(Ljava/lang/Object;Landroid/view/View;Z)V

    .line 162854
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    move-object/from16 v3, v29

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/app/BackStackRecord;->a$redex0(Landroid/support/v4/app/BackStackRecord;LX/0xK;ILjava/lang/Object;)V

    .line 162855
    move-object/from16 v0, v29

    invoke-static {v6, v0}, LX/0xL;->a(Landroid/view/ViewGroup;Ljava/lang/Object;)V

    .line 162856
    move-object/from16 v0, p2

    iget-object v0, v0, LX/0xK;->d:Landroid/view/View;

    move-object/from16 v22, v0

    move-object/from16 v0, p2

    iget-object v0, v0, LX/0xK;->b:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    move-object/from16 v21, v6

    move-object/from16 v23, v12

    move-object/from16 v24, v19

    move-object/from16 v27, v7

    move-object/from16 v28, v11

    move-object/from16 v31, v20

    invoke-static/range {v21 .. v31}, LX/0xL;->a(Landroid/view/View;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/Map;)V

    .line 162857
    :cond_6
    if-eqz v29, :cond_9

    const/4 v4, 0x1

    goto/16 :goto_0

    .line 162858
    :cond_7
    iget-object v5, v8, Landroid/support/v4/app/Fragment;->mEnterTransitionCallback:LX/0xM;

    goto/16 :goto_1

    .line 162859
    :cond_8
    invoke-virtual {v8}, Landroid/support/v4/app/Fragment;->getAllowEnterTransitionOverlap()Z

    move-result v4

    goto :goto_2

    .line 162860
    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method public static a$redex0(Landroid/support/v4/app/BackStackRecord;LX/0xK;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 162683
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v0, v0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    move v1, v2

    .line 162684
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v0, v0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 162685
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v0, v0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 162686
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    iget v3, v0, Landroid/support/v4/app/Fragment;->mContainerId:I

    if-ne v3, p2, :cond_0

    .line 162687
    iget-boolean v3, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    if-eqz v3, :cond_1

    .line 162688
    iget-object v3, p1, LX/0xK;->b:Ljava/util/ArrayList;

    iget-object v4, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 162689
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    const/4 v4, 0x1

    invoke-static {p3, v3, v4}, LX/0xL;->a(Ljava/lang/Object;Landroid/view/View;Z)V

    .line 162690
    iget-object v3, p1, LX/0xK;->b:Ljava/util/ArrayList;

    iget-object v0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162691
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 162692
    :cond_1
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-static {p3, v3, v2}, LX/0xL;->a(Ljava/lang/Object;Landroid/view/View;Z)V

    .line 162693
    iget-object v3, p1, LX/0xK;->b:Ljava/util/ArrayList;

    iget-object v0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 162694
    :cond_2
    return-void
.end method

.method private static b(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 162866
    if-nez p0, :cond_0

    .line 162867
    const/4 v0, 0x0

    .line 162868
    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    .line 162869
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mReturnTransition:Ljava/lang/Object;

    sget-object p1, Landroid/support/v4/app/Fragment;->USE_DEFAULT_TRANSITION:Ljava/lang/Object;

    if-ne v0, p1, :cond_2

    .line 162870
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mEnterTransition:Ljava/lang/Object;

    move-object v0, v0

    .line 162871
    :goto_1
    move-object v0, v0

    .line 162872
    :goto_2
    invoke-static {v0}, LX/0xL;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 162873
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mExitTransition:Ljava/lang/Object;

    move-object v0, v0

    .line 162874
    goto :goto_2

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mReturnTransition:Ljava/lang/Object;

    goto :goto_1
.end method

.method public static b(LX/0xK;LX/026;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0xK;",
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 162875
    invoke-virtual {p1}, LX/01J;->size()I

    move-result v3

    .line 162876
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 162877
    invoke-virtual {p1, v2}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 162878
    invoke-virtual {p1, v2}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, LX/0xL;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v1

    .line 162879
    if-eqz p2, :cond_0

    .line 162880
    iget-object v4, p0, LX/0xK;->a:LX/026;

    invoke-static {v4, v0, v1}, Landroid/support/v4/app/BackStackRecord;->a(LX/026;Ljava/lang/String;Ljava/lang/String;)V

    .line 162881
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 162882
    :cond_0
    iget-object v4, p0, LX/0xK;->a:LX/026;

    invoke-static {v4, v1, v0}, Landroid/support/v4/app/BackStackRecord;->a(LX/026;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 162883
    :cond_1
    return-void
.end method

.method public static b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/support/v4/app/Fragment;",
            ")V"
        }
    .end annotation

    .prologue
    .line 162884
    if-eqz p1, :cond_0

    .line 162885
    iget v0, p1, Landroid/support/v4/app/Fragment;->mContainerId:I

    .line 162886
    if-eqz v0, :cond_0

    .line 162887
    invoke-virtual {p0, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 162888
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/0hH;
    .locals 2

    .prologue
    .line 162889
    iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->k:Z

    if-eqz v0, :cond_0

    .line 162890
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This transaction is already being added to the back stack"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162891
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->l:Z

    .line 162892
    return-object p0
.end method

.method public final a(I)LX/0hH;
    .locals 0

    .prologue
    .line 162893
    iput p1, p0, Landroid/support/v4/app/BackStackRecord;->i:I

    .line 162894
    return-object p0
.end method

.method public final a(II)LX/0hH;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 162695
    invoke-virtual {p0, p1, p2, v0, v0}, Landroid/support/v4/app/BackStackRecord;->a(IIII)LX/0hH;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIII)LX/0hH;
    .locals 0

    .prologue
    .line 162861
    iput p1, p0, Landroid/support/v4/app/BackStackRecord;->e:I

    .line 162862
    iput p2, p0, Landroid/support/v4/app/BackStackRecord;->f:I

    .line 162863
    iput p3, p0, Landroid/support/v4/app/BackStackRecord;->g:I

    .line 162864
    iput p4, p0, Landroid/support/v4/app/BackStackRecord;->h:I

    .line 162865
    return-object p0
.end method

.method public final a(ILandroid/support/v4/app/Fragment;)LX/0hH;
    .locals 2

    .prologue
    .line 162361
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/support/v4/app/BackStackRecord;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    .line 162362
    return-object p0
.end method

.method public final a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;
    .locals 1

    .prologue
    .line 162363
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/app/BackStackRecord;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    .line 162364
    return-object p0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)LX/0hH;
    .locals 2

    .prologue
    .line 162365
    new-instance v0, LX/0xN;

    invoke-direct {v0}, LX/0xN;-><init>()V

    .line 162366
    const/4 v1, 0x3

    iput v1, v0, LX/0xN;->c:I

    .line 162367
    iput-object p1, v0, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162368
    invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->a(LX/0xN;)V

    .line 162369
    return-object p0
.end method

.method public final a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;
    .locals 2

    .prologue
    .line 162370
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Landroid/support/v4/app/BackStackRecord;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    .line 162371
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/0hH;
    .locals 2

    .prologue
    .line 162372
    iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->l:Z

    if-nez v0, :cond_0

    .line 162373
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This FragmentTransaction is not allowed to be added to the back stack."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162374
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->k:Z

    .line 162375
    iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->m:Ljava/lang/String;

    .line 162376
    return-object p0
.end method

.method public final a(ZLX/0xK;Landroid/util/SparseArray;Landroid/util/SparseArray;)LX/0xK;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0xK;",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;)",
            "LX/0xK;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, -0x1

    const/4 v2, 0x0

    .line 162377
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_0

    .line 162378
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "popFromBackStack: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162379
    new-instance v0, LX/0xJ;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, LX/0xJ;-><init>(Ljava/lang/String;)V

    .line 162380
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 162381
    const-string v0, "  "

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/BackStackRecord;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 162382
    :cond_0
    if-nez p2, :cond_3

    .line 162383
    invoke-virtual {p3}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p4}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 162384
    :cond_1
    invoke-direct {p0, p3, p4, v9}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)LX/0xK;

    move-result-object p2

    .line 162385
    :cond_2
    :goto_0
    invoke-virtual {p0, v8}, Landroid/support/v4/app/BackStackRecord;->b(I)V

    .line 162386
    if-eqz p2, :cond_5

    move v6, v2

    .line 162387
    :goto_1
    if-eqz p2, :cond_6

    move v1, v2

    .line 162388
    :goto_2
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->c:LX/0xN;

    move-object v5, v0

    .line 162389
    :goto_3
    if-eqz v5, :cond_b

    .line 162390
    if-eqz p2, :cond_7

    move v4, v2

    .line 162391
    :goto_4
    if-eqz p2, :cond_8

    move v0, v2

    .line 162392
    :goto_5
    iget v3, v5, LX/0xN;->c:I

    packed-switch v3, :pswitch_data_0

    .line 162393
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown cmd: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v5, LX/0xN;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162394
    :cond_3
    if-nez p1, :cond_2

    .line 162395
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->t:Ljava/util/ArrayList;

    .line 162396
    if-eqz v0, :cond_4

    .line 162397
    const/4 v3, 0x0

    move v5, v3

    :goto_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v5, v3, :cond_4

    .line 162398
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 162399
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 162400
    iget-object v6, p2, LX/0xK;->a:LX/026;

    invoke-static {v6, v3, v4}, Landroid/support/v4/app/BackStackRecord;->a(LX/026;Ljava/lang/String;Ljava/lang/String;)V

    .line 162401
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_6

    .line 162402
    :cond_4
    goto :goto_0

    .line 162403
    :cond_5
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->j:I

    move v6, v0

    goto :goto_1

    .line 162404
    :cond_6
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->i:I

    move v1, v0

    goto :goto_2

    .line 162405
    :cond_7
    iget v0, v5, LX/0xN;->g:I

    move v4, v0

    goto :goto_4

    .line 162406
    :cond_8
    iget v0, v5, LX/0xN;->h:I

    goto :goto_5

    .line 162407
    :pswitch_0
    iget-object v3, v5, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162408
    iput v0, v3, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162409
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-static {v1}, LX/0jz;->e(I)I

    move-result v4

    invoke-virtual {v0, v3, v4, v6}, LX/0jz;->a(Landroid/support/v4/app/Fragment;II)V

    .line 162410
    :cond_9
    :goto_7
    iget-object v0, v5, LX/0xN;->b:LX/0xN;

    move-object v5, v0

    .line 162411
    goto :goto_3

    .line 162412
    :pswitch_1
    iget-object v3, v5, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162413
    if-eqz v3, :cond_a

    .line 162414
    iput v0, v3, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162415
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-static {v1}, LX/0jz;->e(I)I

    move-result v7

    invoke-virtual {v0, v3, v7, v6}, LX/0jz;->a(Landroid/support/v4/app/Fragment;II)V

    .line 162416
    :cond_a
    iget-object v0, v5, LX/0xN;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_9

    move v3, v2

    .line 162417
    :goto_8
    iget-object v0, v5, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_9

    .line 162418
    iget-object v0, v5, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 162419
    iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162420
    iget-object v7, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v7, v0, v2}, LX/0jz;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 162421
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    .line 162422
    :pswitch_2
    iget-object v0, v5, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162423
    iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162424
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v3, v0, v2}, LX/0jz;->a(Landroid/support/v4/app/Fragment;Z)V

    goto :goto_7

    .line 162425
    :pswitch_3
    iget-object v0, v5, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162426
    iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162427
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-static {v1}, LX/0jz;->e(I)I

    move-result v4

    invoke-virtual {v3, v0, v4, v6}, LX/0jz;->c(Landroid/support/v4/app/Fragment;II)V

    goto :goto_7

    .line 162428
    :pswitch_4
    iget-object v3, v5, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162429
    iput v0, v3, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162430
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-static {v1}, LX/0jz;->e(I)I

    move-result v4

    invoke-virtual {v0, v3, v4, v6}, LX/0jz;->b(Landroid/support/v4/app/Fragment;II)V

    goto :goto_7

    .line 162431
    :pswitch_5
    iget-object v0, v5, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162432
    iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162433
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-static {v1}, LX/0jz;->e(I)I

    move-result v4

    invoke-virtual {v3, v0, v4, v6}, LX/0jz;->e(Landroid/support/v4/app/Fragment;II)V

    goto :goto_7

    .line 162434
    :pswitch_6
    iget-object v0, v5, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162435
    iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162436
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-static {v1}, LX/0jz;->e(I)I

    move-result v4

    invoke-virtual {v3, v0, v4, v6}, LX/0jz;->d(Landroid/support/v4/app/Fragment;II)V

    goto :goto_7

    .line 162437
    :cond_b
    if-eqz p1, :cond_c

    .line 162438
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v2, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget v2, v2, LX/0jz;->m:I

    invoke-static {v1}, LX/0jz;->e(I)I

    move-result v1

    invoke-virtual {v0, v2, v1, v6, v9}, LX/0jz;->a(IIIZ)V

    .line 162439
    const/4 p2, 0x0

    .line 162440
    :cond_c
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    if-ltz v0, :cond_d

    .line 162441
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget v1, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    invoke-virtual {v0, v1}, LX/0jz;->d(I)V

    .line 162442
    iput v8, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    .line 162443
    :cond_d
    return-object p2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a(LX/0xN;)V
    .locals 1

    .prologue
    .line 162444
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->b:LX/0xN;

    if-nez v0, :cond_0

    .line 162445
    iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->c:LX/0xN;

    iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->b:LX/0xN;

    .line 162446
    :goto_0
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->e:I

    iput v0, p1, LX/0xN;->e:I

    .line 162447
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->f:I

    iput v0, p1, LX/0xN;->f:I

    .line 162448
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->g:I

    iput v0, p1, LX/0xN;->g:I

    .line 162449
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->h:I

    iput v0, p1, LX/0xN;->h:I

    .line 162450
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/app/BackStackRecord;->d:I

    .line 162451
    return-void

    .line 162452
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->c:LX/0xN;

    iput-object v0, p1, LX/0xN;->b:LX/0xN;

    .line 162453
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->c:LX/0xN;

    iput-object p1, v0, LX/0xN;->a:LX/0xN;

    .line 162454
    iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->c:LX/0xN;

    goto :goto_0
.end method

.method public final a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162455
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v0, v0, LX/0jz;->o:LX/0k1;

    invoke-interface {v0}, LX/0k1;->s()Z

    move-result v0

    if-nez v0, :cond_1

    .line 162456
    :cond_0
    return-void

    .line 162457
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->b:LX/0xN;

    move-object v2, v0

    .line 162458
    :goto_0
    if-eqz v2, :cond_0

    .line 162459
    iget v0, v2, LX/0xN;->c:I

    packed-switch v0, :pswitch_data_0

    .line 162460
    :goto_1
    iget-object v0, v2, LX/0xN;->a:LX/0xN;

    move-object v2, v0

    goto :goto_0

    .line 162461
    :pswitch_0
    iget-object v0, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 162462
    :pswitch_1
    iget-object v0, v2, LX/0xN;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 162463
    iget-object v0, v2, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_2

    .line 162464
    iget-object v0, v2, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/BackStackRecord;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    .line 162465
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 162466
    :cond_2
    iget-object v0, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 162467
    :pswitch_2
    iget-object v0, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/BackStackRecord;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 162468
    :pswitch_3
    iget-object v0, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/BackStackRecord;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 162469
    :pswitch_4
    iget-object v0, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 162470
    :pswitch_5
    iget-object v0, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/BackStackRecord;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 162471
    :pswitch_6
    iget-object v0, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 162472
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v4/app/BackStackRecord;->a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    .line 162473
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 162474
    if-eqz p3, :cond_8

    .line 162475
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mName="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->m:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162476
    const-string v0, " mIndex="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 162477
    const-string v0, " mCommitted="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->n:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 162478
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->i:I

    if-eqz v0, :cond_0

    .line 162479
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTransition=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162480
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162481
    const-string v0, " mTransitionStyle=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162482
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->j:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 162483
    :cond_0
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->e:I

    if-nez v0, :cond_1

    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->f:I

    if-eqz v0, :cond_2

    .line 162484
    :cond_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162485
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162486
    const-string v0, " mExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162487
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 162488
    :cond_2
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->g:I

    if-nez v0, :cond_3

    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->h:I

    if-eqz v0, :cond_4

    .line 162489
    :cond_3
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mPopEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162490
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162491
    const-string v0, " mPopExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162492
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 162493
    :cond_4
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->p:I

    if-nez v0, :cond_5

    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->q:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    .line 162494
    :cond_5
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162495
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->p:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162496
    const-string v0, " mBreadCrumbTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162497
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->q:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 162498
    :cond_6
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->r:I

    if-nez v0, :cond_7

    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->s:Ljava/lang/CharSequence;

    if-eqz v0, :cond_8

    .line 162499
    :cond_7
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbShortTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162500
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162501
    const-string v0, " mBreadCrumbShortTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162502
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->s:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 162503
    :cond_8
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->b:LX/0xN;

    if-eqz v0, :cond_10

    .line 162504
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Operations:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 162505
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 162506
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->b:LX/0xN;

    move v2, v1

    move-object v3, v0

    .line 162507
    :goto_0
    if-eqz v3, :cond_10

    .line 162508
    iget v0, v3, LX/0xN;->c:I

    packed-switch v0, :pswitch_data_0

    .line 162509
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "cmd="

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v3, LX/0xN;->c:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 162510
    :goto_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  Op #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 162511
    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162512
    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, v3, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 162513
    if-eqz p3, :cond_c

    .line 162514
    iget v0, v3, LX/0xN;->e:I

    if-nez v0, :cond_9

    iget v0, v3, LX/0xN;->f:I

    if-eqz v0, :cond_a

    .line 162515
    :cond_9
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "enterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162516
    iget v0, v3, LX/0xN;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162517
    const-string v0, " exitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162518
    iget v0, v3, LX/0xN;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 162519
    :cond_a
    iget v0, v3, LX/0xN;->g:I

    if-nez v0, :cond_b

    iget v0, v3, LX/0xN;->h:I

    if-eqz v0, :cond_c

    .line 162520
    :cond_b
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "popEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162521
    iget v0, v3, LX/0xN;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162522
    const-string v0, " popExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162523
    iget v0, v3, LX/0xN;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 162524
    :cond_c
    iget-object v0, v3, LX/0xN;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_f

    iget-object v0, v3, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    move v0, v1

    .line 162525
    :goto_2
    iget-object v5, v3, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_f

    .line 162526
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162527
    iget-object v5, v3, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_d

    .line 162528
    const-string v5, "Removed: "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 162529
    :goto_3
    iget-object v5, v3, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 162530
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 162531
    :pswitch_0
    const-string v0, "NULL"

    goto/16 :goto_1

    .line 162532
    :pswitch_1
    const-string v0, "ADD"

    goto/16 :goto_1

    .line 162533
    :pswitch_2
    const-string v0, "REPLACE"

    goto/16 :goto_1

    .line 162534
    :pswitch_3
    const-string v0, "REMOVE"

    goto/16 :goto_1

    .line 162535
    :pswitch_4
    const-string v0, "HIDE"

    goto/16 :goto_1

    .line 162536
    :pswitch_5
    const-string v0, "SHOW"

    goto/16 :goto_1

    .line 162537
    :pswitch_6
    const-string v0, "DETACH"

    goto/16 :goto_1

    .line 162538
    :pswitch_7
    const-string v0, "ATTACH"

    goto/16 :goto_1

    .line 162539
    :cond_d
    if-nez v0, :cond_e

    .line 162540
    const-string v5, "Removed:"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 162541
    :cond_e
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 162542
    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_3

    .line 162543
    :cond_f
    iget-object v3, v3, LX/0xN;->a:LX/0xN;

    .line 162544
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 162545
    goto/16 :goto_0

    .line 162546
    :cond_10
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 162547
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/app/BackStackRecord;->a(Z)I

    move-result v0

    return v0
.end method

.method public final b(ILandroid/support/v4/app/Fragment;)LX/0hH;
    .locals 1

    .prologue
    .line 162548
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v4/app/BackStackRecord;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    return-object v0
.end method

.method public final b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;
    .locals 2

    .prologue
    .line 162357
    if-nez p1, :cond_0

    .line 162358
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must use non-zero containerViewId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162359
    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/app/BackStackRecord;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    .line 162360
    return-object p0
.end method

.method public final b(Landroid/support/v4/app/Fragment;)LX/0hH;
    .locals 2

    .prologue
    .line 162549
    new-instance v0, LX/0xN;

    invoke-direct {v0}, LX/0xN;-><init>()V

    .line 162550
    const/4 v1, 0x4

    iput v1, v0, LX/0xN;->c:I

    .line 162551
    iput-object p1, v0, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162552
    invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->a(LX/0xN;)V

    .line 162553
    return-object p0
.end method

.method public final b(I)V
    .locals 5

    .prologue
    .line 162554
    iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->k:Z

    if-nez v0, :cond_1

    .line 162555
    :cond_0
    return-void

    .line 162556
    :cond_1
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bump nesting in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " by "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 162557
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->b:LX/0xN;

    move-object v2, v0

    .line 162558
    :goto_0
    if-eqz v2, :cond_0

    .line 162559
    iget-object v0, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_3

    .line 162560
    iget-object v0, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    iget v1, v0, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    add-int/2addr v1, p1

    iput v1, v0, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    .line 162561
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bump nesting of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v2, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    iget v1, v1, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 162562
    :cond_3
    iget-object v0, v2, LX/0xN;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 162563
    iget-object v0, v2, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_5

    .line 162564
    iget-object v0, v2, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 162565
    iget v3, v0, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    add-int/2addr v3, p1

    iput v3, v0, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    .line 162566
    sget-boolean v3, LX/0jz;->a:Z

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bump nesting of "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 162567
    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 162568
    :cond_5
    iget-object v0, v2, LX/0xN;->a:LX/0xN;

    move-object v2, v0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 162569
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/app/BackStackRecord;->a(Z)I

    move-result v0

    return v0
.end method

.method public final c(Landroid/support/v4/app/Fragment;)LX/0hH;
    .locals 2

    .prologue
    .line 162570
    new-instance v0, LX/0xN;

    invoke-direct {v0}, LX/0xN;-><init>()V

    .line 162571
    const/4 v1, 0x5

    iput v1, v0, LX/0xN;->c:I

    .line 162572
    iput-object p1, v0, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162573
    invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->a(LX/0xN;)V

    .line 162574
    return-object p0
.end method

.method public final d(Landroid/support/v4/app/Fragment;)LX/0hH;
    .locals 2

    .prologue
    .line 162575
    new-instance v0, LX/0xN;

    invoke-direct {v0}, LX/0xN;-><init>()V

    .line 162576
    const/4 v1, 0x6

    iput v1, v0, LX/0xN;->c:I

    .line 162577
    iput-object p1, v0, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162578
    invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->a(LX/0xN;)V

    .line 162579
    return-object p0
.end method

.method public final e(Landroid/support/v4/app/Fragment;)LX/0hH;
    .locals 2

    .prologue
    .line 162580
    new-instance v0, LX/0xN;

    invoke-direct {v0}, LX/0xN;-><init>()V

    .line 162581
    const/4 v1, 0x7

    iput v1, v0, LX/0xN;->c:I

    .line 162582
    iput-object p1, v0, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162583
    invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->a(LX/0xN;)V

    .line 162584
    return-object p0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 162585
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->d:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final run()V
    .locals 14

    .prologue
    const/4 v6, 0x0

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 162586
    sget-boolean v0, LX/0jz;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Run: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162587
    :cond_0
    iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->k:Z

    if-eqz v0, :cond_1

    .line 162588
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    if-gez v0, :cond_1

    .line 162589
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "addToBackStack() called after commit()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162590
    :cond_1
    invoke-virtual {p0, v13}, Landroid/support/v4/app/BackStackRecord;->b(I)V

    .line 162591
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_12

    .line 162592
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 162593
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 162594
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v3, v3, LX/0jz;->o:LX/0k1;

    invoke-interface {v3}, LX/0k1;->s()Z

    move-result v3

    if-nez v3, :cond_13

    .line 162595
    :cond_2
    invoke-direct {p0, v0, v1, v2}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)LX/0xK;

    move-result-object v0

    move-object v10, v0

    .line 162596
    :goto_0
    if-eqz v10, :cond_3

    move v9, v2

    .line 162597
    :goto_1
    if-eqz v10, :cond_4

    move v1, v2

    .line 162598
    :goto_2
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->b:LX/0xN;

    move-object v8, v0

    .line 162599
    :goto_3
    if-eqz v8, :cond_10

    .line 162600
    if-eqz v10, :cond_5

    move v7, v2

    .line 162601
    :goto_4
    if-eqz v10, :cond_6

    move v3, v2

    .line 162602
    :goto_5
    iget v0, v8, LX/0xN;->c:I

    packed-switch v0, :pswitch_data_0

    .line 162603
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown cmd: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v8, LX/0xN;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162604
    :cond_3
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->j:I

    move v9, v0

    goto :goto_1

    .line 162605
    :cond_4
    iget v0, p0, Landroid/support/v4/app/BackStackRecord;->i:I

    move v1, v0

    goto :goto_2

    .line 162606
    :cond_5
    iget v0, v8, LX/0xN;->e:I

    move v7, v0

    goto :goto_4

    .line 162607
    :cond_6
    iget v0, v8, LX/0xN;->f:I

    move v3, v0

    goto :goto_5

    .line 162608
    :pswitch_0
    iget-object v0, v8, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162609
    iput v7, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162610
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v3, v0, v2}, LX/0jz;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 162611
    :cond_7
    :goto_6
    iget-object v0, v8, LX/0xN;->a:LX/0xN;

    move-object v8, v0

    .line 162612
    goto :goto_3

    .line 162613
    :pswitch_1
    iget-object v0, v8, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162614
    iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v4, v4, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v4, :cond_e

    move v4, v2

    move-object v5, v0

    .line 162615
    :goto_7
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v0, v0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_f

    .line 162616
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v0, v0, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 162617
    sget-boolean v11, LX/0jz;->a:Z

    if-eqz v11, :cond_8

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "OP_REPLACE: adding="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " old="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162618
    :cond_8
    if-eqz v5, :cond_9

    iget v11, v0, Landroid/support/v4/app/Fragment;->mContainerId:I

    iget v12, v5, Landroid/support/v4/app/Fragment;->mContainerId:I

    if-ne v11, v12, :cond_a

    .line 162619
    :cond_9
    if-ne v0, v5, :cond_b

    .line 162620
    iput-object v6, v8, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    move-object v5, v6

    .line 162621
    :cond_a
    :goto_8
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_7

    .line 162622
    :cond_b
    iget-object v11, v8, LX/0xN;->i:Ljava/util/ArrayList;

    if-nez v11, :cond_c

    .line 162623
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v8, LX/0xN;->i:Ljava/util/ArrayList;

    .line 162624
    :cond_c
    iget-object v11, v8, LX/0xN;->i:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162625
    iput v3, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162626
    iget-boolean v11, p0, Landroid/support/v4/app/BackStackRecord;->k:Z

    if-eqz v11, :cond_d

    .line 162627
    iget v11, v0, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    add-int/lit8 v11, v11, 0x1

    iput v11, v0, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    .line 162628
    sget-boolean v11, LX/0jz;->a:Z

    if-eqz v11, :cond_d

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Bump nesting of "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " to "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Landroid/support/v4/app/Fragment;->mBackStackNesting:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 162629
    :cond_d
    iget-object v11, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v11, v0, v1, v9}, LX/0jz;->a(Landroid/support/v4/app/Fragment;II)V

    goto :goto_8

    :cond_e
    move-object v5, v0

    .line 162630
    :cond_f
    if-eqz v5, :cond_7

    .line 162631
    iput v7, v5, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162632
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v0, v5, v2}, LX/0jz;->a(Landroid/support/v4/app/Fragment;Z)V

    goto/16 :goto_6

    .line 162633
    :pswitch_2
    iget-object v0, v8, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162634
    iput v3, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162635
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v3, v0, v1, v9}, LX/0jz;->a(Landroid/support/v4/app/Fragment;II)V

    goto/16 :goto_6

    .line 162636
    :pswitch_3
    iget-object v0, v8, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162637
    iput v3, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162638
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v3, v0, v1, v9}, LX/0jz;->b(Landroid/support/v4/app/Fragment;II)V

    goto/16 :goto_6

    .line 162639
    :pswitch_4
    iget-object v0, v8, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162640
    iput v7, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162641
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v3, v0, v1, v9}, LX/0jz;->c(Landroid/support/v4/app/Fragment;II)V

    goto/16 :goto_6

    .line 162642
    :pswitch_5
    iget-object v0, v8, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162643
    iput v3, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162644
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v3, v0, v1, v9}, LX/0jz;->d(Landroid/support/v4/app/Fragment;II)V

    goto/16 :goto_6

    .line 162645
    :pswitch_6
    iget-object v0, v8, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162646
    iput v7, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I

    .line 162647
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v3, v0, v1, v9}, LX/0jz;->e(Landroid/support/v4/app/Fragment;II)V

    goto/16 :goto_6

    .line 162648
    :cond_10
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v2, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget v2, v2, LX/0jz;->m:I

    invoke-virtual {v0, v2, v1, v9, v13}, LX/0jz;->a(IIIZ)V

    .line 162649
    iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->k:Z

    if-eqz v0, :cond_11

    .line 162650
    iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    invoke-virtual {v0, p0}, LX/0jz;->b(Landroid/support/v4/app/BackStackRecord;)V

    .line 162651
    :cond_11
    return-void

    :cond_12
    move-object v10, v6

    goto/16 :goto_0

    .line 162652
    :cond_13
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->b:LX/0xN;

    move-object v7, v3

    .line 162653
    :goto_9
    if-eqz v7, :cond_2

    .line 162654
    iget v3, v7, LX/0xN;->c:I

    packed-switch v3, :pswitch_data_1

    .line 162655
    :goto_a
    iget-object v3, v7, LX/0xN;->a:LX/0xN;

    move-object v7, v3

    goto :goto_9

    .line 162656
    :pswitch_7
    iget-object v3, v7, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {v1, v3}, Landroid/support/v4/app/BackStackRecord;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_a

    .line 162657
    :pswitch_8
    iget-object v4, v7, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    .line 162658
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v3, v3, LX/0jz;->mAdded:Ljava/util/ArrayList;

    if-eqz v3, :cond_17

    .line 162659
    const/4 v3, 0x0

    move-object v5, v4

    move v4, v3

    :goto_b
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v3, v3, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v4, v3, :cond_18

    .line 162660
    iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->a:LX/0jz;

    iget-object v3, v3, LX/0jz;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/Fragment;

    .line 162661
    if-eqz v5, :cond_14

    iget v8, v3, Landroid/support/v4/app/Fragment;->mContainerId:I

    iget v9, v5, Landroid/support/v4/app/Fragment;->mContainerId:I

    if-ne v8, v9, :cond_15

    .line 162662
    :cond_14
    if-ne v3, v5, :cond_16

    .line 162663
    const/4 v5, 0x0

    .line 162664
    :cond_15
    :goto_c
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_b

    .line 162665
    :cond_16
    invoke-static {v0, v3}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_c

    :cond_17
    move-object v5, v4

    .line 162666
    :cond_18
    invoke-static {v1, v5}, Landroid/support/v4/app/BackStackRecord;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_a

    .line 162667
    :pswitch_9
    iget-object v3, v7, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v3}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_a

    .line 162668
    :pswitch_a
    iget-object v3, v7, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v3}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_a

    .line 162669
    :pswitch_b
    iget-object v3, v7, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {v1, v3}, Landroid/support/v4/app/BackStackRecord;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_a

    .line 162670
    :pswitch_c
    iget-object v3, v7, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v3}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_a

    .line 162671
    :pswitch_d
    iget-object v3, v7, LX/0xN;->d:Landroid/support/v4/app/Fragment;

    invoke-static {v1, v3}, Landroid/support/v4/app/BackStackRecord;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_a

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 162672
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 162673
    const-string v1, "BackStackEntry{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162674
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162675
    iget v1, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    if-ltz v1, :cond_0

    .line 162676
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162677
    iget v1, p0, Landroid/support/v4/app/BackStackRecord;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 162678
    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->m:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 162679
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162680
    iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162681
    :cond_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162682
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
