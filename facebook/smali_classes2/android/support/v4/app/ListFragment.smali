.class public Landroid/support/v4/app/ListFragment;
.super Landroid/support/v4/app/Fragment;
.source ""


# instance fields
.field public a:Landroid/widget/ListAdapter;

.field public b:Landroid/widget/ListView;

.field public c:Landroid/view/View;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/view/View;

.field public f:Landroid/view/View;

.field public g:Ljava/lang/CharSequence;

.field public h:Z

.field private final i:Landroid/os/Handler;

.field private final j:Ljava/lang/Runnable;

.field private final k:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 114115
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 114116
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ListFragment;->i:Landroid/os/Handler;

    .line 114117
    new-instance v0, Landroid/support/v4/app/ListFragment$1;

    invoke-direct {v0, p0}, Landroid/support/v4/app/ListFragment$1;-><init>(Landroid/support/v4/app/ListFragment;)V

    iput-object v0, p0, Landroid/support/v4/app/ListFragment;->j:Ljava/lang/Runnable;

    .line 114118
    new-instance v0, LX/2hD;

    invoke-direct {v0, p0}, LX/2hD;-><init>(Landroid/support/v4/app/ListFragment;)V

    iput-object v0, p0, Landroid/support/v4/app/ListFragment;->k:Landroid/widget/AdapterView$OnItemClickListener;

    .line 114119
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 114082
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->b:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 114083
    :goto_0
    return-void

    .line 114084
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 114085
    if-nez v0, :cond_1

    .line 114086
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Content view not yet created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114087
    :cond_1
    instance-of v1, v0, Landroid/widget/ListView;

    if-eqz v1, :cond_4

    .line 114088
    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Landroid/support/v4/app/ListFragment;->b:Landroid/widget/ListView;

    .line 114089
    :cond_2
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ListFragment;->h:Z

    .line 114090
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->b:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v4/app/ListFragment;->k:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 114091
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->a:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_9

    .line 114092
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->a:Landroid/widget/ListAdapter;

    .line 114093
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v4/app/ListFragment;->a:Landroid/widget/ListAdapter;

    .line 114094
    invoke-virtual {p0, v0}, Landroid/support/v4/app/ListFragment;->a(Landroid/widget/ListAdapter;)V

    .line 114095
    :cond_3
    :goto_2
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->i:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v4/app/ListFragment;->j:Ljava/lang/Runnable;

    const v2, 0x4ff66e77    # 8.2688691E9f

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 114096
    :cond_4
    const v1, 0xff0001

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Landroid/support/v4/app/ListFragment;->d:Landroid/widget/TextView;

    .line 114097
    iget-object v1, p0, Landroid/support/v4/app/ListFragment;->d:Landroid/widget/TextView;

    if-nez v1, :cond_5

    .line 114098
    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/app/ListFragment;->c:Landroid/view/View;

    .line 114099
    :goto_3
    const v1, 0xff0002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/app/ListFragment;->e:Landroid/view/View;

    .line 114100
    const v1, 0xff0003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/app/ListFragment;->f:Landroid/view/View;

    .line 114101
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 114102
    instance-of v1, v0, Landroid/widget/ListView;

    if-nez v1, :cond_7

    .line 114103
    if-nez v0, :cond_6

    .line 114104
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your content must have a ListView whose id attribute is \'android.R.id.list\'"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114105
    :cond_5
    iget-object v1, p0, Landroid/support/v4/app/ListFragment;->d:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 114106
    :cond_6
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Content has view with id attribute \'android.R.id.list\' that is not a ListView class"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114107
    :cond_7
    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Landroid/support/v4/app/ListFragment;->b:Landroid/widget/ListView;

    .line 114108
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->c:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 114109
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->b:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v4/app/ListFragment;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_1

    .line 114110
    :cond_8
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->g:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    .line 114111
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->d:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v4/app/ListFragment;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114112
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->b:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v4/app/ListFragment;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 114113
    :cond_9
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->e:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 114114
    invoke-direct {p0, v3, v3}, Landroid/support/v4/app/ListFragment;->a(ZZ)V

    goto/16 :goto_2
.end method

.method private a(ZZ)V
    .locals 6

    .prologue
    const v5, 0x10a0001

    const/high16 v4, 0x10a0000

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 114061
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;->a()V

    .line 114062
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->e:Landroid/view/View;

    if-nez v0, :cond_0

    .line 114063
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a custom content view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114064
    :cond_0
    iget-boolean v0, p0, Landroid/support/v4/app/ListFragment;->h:Z

    if-ne v0, p1, :cond_1

    .line 114065
    :goto_0
    return-void

    .line 114066
    :cond_1
    iput-boolean p1, p0, Landroid/support/v4/app/ListFragment;->h:Z

    .line 114067
    if-eqz p1, :cond_3

    .line 114068
    if-eqz p2, :cond_2

    .line 114069
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->e:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 114070
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->f:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 114071
    :goto_1
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 114072
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 114073
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 114074
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_1

    .line 114075
    :cond_3
    if-eqz p2, :cond_4

    .line 114076
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->e:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 114077
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->f:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 114078
    :goto_2
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 114079
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 114080
    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 114081
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_2
.end method


# virtual methods
.method public a(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 114060
    return-void
.end method

.method public final a(Landroid/widget/ListAdapter;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 114051
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->a:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_2

    move v0, v1

    .line 114052
    :goto_0
    iput-object p1, p0, Landroid/support/v4/app/ListFragment;->a:Landroid/widget/ListAdapter;

    .line 114053
    iget-object v3, p0, Landroid/support/v4/app/ListFragment;->b:Landroid/widget/ListView;

    if-eqz v3, :cond_1

    .line 114054
    iget-object v3, p0, Landroid/support/v4/app/ListFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v3, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 114055
    iget-boolean v3, p0, Landroid/support/v4/app/ListFragment;->h:Z

    if-nez v3, :cond_1

    if-nez v0, :cond_1

    .line 114056
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 114057
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    invoke-direct {p0, v1, v2}, Landroid/support/v4/app/ListFragment;->a(ZZ)V

    .line 114058
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 114059
    goto :goto_0
.end method

.method public final ld_()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 114049
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;->a()V

    .line 114050
    iget-object v0, p0, Landroid/support/v4/app/ListFragment;->b:Landroid/widget/ListView;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/16 v10, 0x11

    const/4 v9, 0x2

    const/4 v8, -0x2

    const/4 v7, -0x1

    const/16 v0, 0x2a

    const v1, -0xac451c4

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 114026
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 114027
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 114028
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 114029
    const v4, 0xff0002

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setId(I)V

    .line 114030
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 114031
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 114032
    invoke-virtual {v3, v10}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 114033
    new-instance v4, Landroid/widget/ProgressBar;

    const/4 v5, 0x0

    const v6, 0x101007a

    invoke-direct {v4, v1, v5, v6}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114034
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v8, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 114035
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 114036
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-direct {v3, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 114037
    const v1, 0xff0003

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setId(I)V

    .line 114038
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 114039
    const v4, 0xff0001

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setId(I)V

    .line 114040
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setGravity(I)V

    .line 114041
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v1, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 114042
    new-instance v1, Landroid/widget/ListView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 114043
    const v4, 0x102000a

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setId(I)V

    .line 114044
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setDrawSelectorOnTop(Z)V

    .line 114045
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v1, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 114046
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 114047
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 114048
    const/16 v1, 0x2b

    const v3, 0x758806df

    invoke-static {v9, v1, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x5659bf02

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 114019
    iget-object v1, p0, Landroid/support/v4/app/ListFragment;->i:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/v4/app/ListFragment;->j:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 114020
    iput-object v3, p0, Landroid/support/v4/app/ListFragment;->b:Landroid/widget/ListView;

    .line 114021
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/app/ListFragment;->h:Z

    .line 114022
    iput-object v3, p0, Landroid/support/v4/app/ListFragment;->f:Landroid/view/View;

    iput-object v3, p0, Landroid/support/v4/app/ListFragment;->e:Landroid/view/View;

    iput-object v3, p0, Landroid/support/v4/app/ListFragment;->c:Landroid/view/View;

    .line 114023
    iput-object v3, p0, Landroid/support/v4/app/ListFragment;->d:Landroid/widget/TextView;

    .line 114024
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 114025
    const/16 v1, 0x2b

    const v2, -0x1b7911b1

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 114016
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 114017
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;->a()V

    .line 114018
    return-void
.end method
