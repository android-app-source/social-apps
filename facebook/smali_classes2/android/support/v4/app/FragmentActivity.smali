.class public Landroid/support/v4/app/FragmentActivity;
.super Landroid/app/Activity;
.source ""


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:LX/0jz;

.field public c:LX/0jz;

.field public final d:LX/0k1;

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "LX/0k4;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0k4;

.field public o:LX/0k2;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 105943
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 105944
    new-instance v0, LX/0jy;

    invoke-direct {v0, p0}, LX/0jy;-><init>(Landroid/support/v4/app/FragmentActivity;)V

    iput-object v0, p0, Landroid/support/v4/app/FragmentActivity;->a:Landroid/os/Handler;

    .line 105945
    new-instance v0, LX/0jz;

    invoke-direct {v0}, LX/0jz;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    .line 105946
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/FragmentActivity;->c:LX/0jz;

    .line 105947
    new-instance v0, LX/0k0;

    invoke-direct {v0, p0}, LX/0k0;-><init>(Landroid/support/v4/app/FragmentActivity;)V

    iput-object v0, p0, Landroid/support/v4/app/FragmentActivity;->d:LX/0k1;

    .line 105948
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 105949
    invoke-virtual {p1, p0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 105950
    if-nez p2, :cond_1

    .line 105951
    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 105952
    :cond_0
    return-void

    .line 105953
    :cond_1
    const/16 v3, 0x56

    const/16 v1, 0x46

    const/16 v6, 0x2c

    const/16 v5, 0x20

    const/16 v2, 0x2e

    .line 105954
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 105955
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105956
    const/16 v0, 0x7b

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105957
    invoke-static {p2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105958
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105959
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 105960
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105961
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105962
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x45

    :goto_2
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105963
    invoke-virtual {p2}, Landroid/view/View;->willNotDraw()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105964
    invoke-virtual {p2}, Landroid/view/View;->isHorizontalScrollBarEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x48

    :goto_4
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105965
    invoke-virtual {p2}, Landroid/view/View;->isVerticalScrollBarEnabled()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v3

    :goto_5
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105966
    invoke-virtual {p2}, Landroid/view/View;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0x43

    :goto_6
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105967
    invoke-virtual {p2}, Landroid/view/View;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x4c

    :goto_7
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105968
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105969
    invoke-virtual {p2}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_b

    :goto_8
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105970
    invoke-virtual {p2}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0x53

    :goto_9
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105971
    invoke-virtual {p2}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v2, 0x50

    :cond_2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105972
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105973
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105974
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105975
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105976
    const/16 v0, 0x2d

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105977
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105978
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105979
    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105980
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    .line 105981
    const/4 v0, -0x1

    if-eq v1, v0, :cond_3

    .line 105982
    const-string v0, " #"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105983
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105984
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 105985
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    .line 105986
    const/high16 v0, -0x1000000

    and-int/2addr v0, v1

    sparse-switch v0, :sswitch_data_1

    .line 105987
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v0

    .line 105988
    :goto_a
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v3

    .line 105989
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    .line 105990
    const-string v2, " "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105991
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105992
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105993
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105994
    const-string v0, "/"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105995
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105996
    :cond_3
    :goto_b
    const-string v0, "}"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105997
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 105998
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 105999
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 106000
    check-cast p2, Landroid/view/ViewGroup;

    .line 106001
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 106002
    if-lez v1, :cond_0

    .line 106003
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 106004
    const/4 v0, 0x0

    :goto_c
    if-ge v0, v1, :cond_0

    .line 106005
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v2, p1, v3}, Landroid/support/v4/app/FragmentActivity;->a(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V

    .line 106006
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 106007
    :sswitch_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 106008
    :sswitch_1
    const/16 v0, 0x49

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 106009
    :sswitch_2
    const/16 v0, 0x47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 106010
    goto/16 :goto_1

    :cond_5
    move v0, v2

    .line 106011
    goto/16 :goto_2

    .line 106012
    :cond_6
    const/16 v0, 0x44

    goto/16 :goto_3

    :cond_7
    move v0, v2

    .line 106013
    goto/16 :goto_4

    :cond_8
    move v0, v2

    .line 106014
    goto/16 :goto_5

    :cond_9
    move v0, v2

    .line 106015
    goto/16 :goto_6

    :cond_a
    move v0, v2

    .line 106016
    goto/16 :goto_7

    :cond_b
    move v1, v2

    .line 106017
    goto/16 :goto_8

    :cond_c
    move v0, v2

    .line 106018
    goto/16 :goto_9

    .line 106019
    :sswitch_3
    :try_start_1
    const-string v0, "app"

    goto/16 :goto_a

    .line 106020
    :sswitch_4
    const-string v0, "android"
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_a

    :catch_0
    goto :goto_b

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1000000 -> :sswitch_4
        0x7f000000 -> :sswitch_3
    .end sparse-switch
.end method

.method private n()LX/0k3;
    .locals 1

    .prologue
    .line 106021
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->o:LX/0k2;

    if-nez v0, :cond_0

    .line 106022
    new-instance v0, LX/0k2;

    invoke-direct {v0, p0}, LX/0k2;-><init>(Landroid/support/v4/app/FragmentActivity;)V

    iput-object v0, p0, Landroid/support/v4/app/FragmentActivity;->o:LX/0k2;

    .line 106023
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->o:LX/0k2;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZZ)LX/0k4;
    .locals 2

    .prologue
    .line 106024
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    if-nez v0, :cond_0

    .line 106025
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    .line 106026
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k4;

    .line 106027
    if-nez v0, :cond_2

    .line 106028
    if-eqz p3, :cond_1

    .line 106029
    new-instance v0, LX/0k4;

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;->n()LX/0k3;

    move-result-object v1

    invoke-direct {v0, p1, v1, p2}, LX/0k4;-><init>(Ljava/lang/String;LX/0k3;Z)V

    .line 106030
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    invoke-virtual {v1, p1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106031
    :cond_1
    :goto_0
    return-object v0

    .line 106032
    :cond_2
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;->n()LX/0k3;

    move-result-object v1

    .line 106033
    iput-object v1, v0, LX/0k4;->e:LX/0k3;

    .line 106034
    goto :goto_0
.end method

.method public a(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 106035
    return-void
.end method

.method public a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/high16 v1, -0x10000

    .line 106036
    if-ne p3, v0, :cond_0

    .line 106037
    invoke-super {p0, p2, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 106038
    :goto_0
    return-void

    .line 106039
    :cond_0
    and-int v0, p3, v1

    if-eqz v0, :cond_1

    .line 106040
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can only use lower 16 bits for requestCode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106041
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0jz;->b(I)I

    .line 106042
    iget v0, p1, Landroid/support/v4/app/Fragment;->mGlobalIndex:I

    move v0, v0

    .line 106043
    and-int/2addr v1, v0

    if-eqz v1, :cond_2

    .line 106044
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can only use lower 16 bits for fragment ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106045
    :cond_2
    shl-int/lit8 v0, v0, 0x10

    const v1, 0xffff

    and-int/2addr v1, p3

    add-int/2addr v0, v1

    invoke-super {p0, p2, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 105927
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->h:Z

    if-nez v0, :cond_1

    .line 105928
    iput-boolean v1, p0, Landroid/support/v4/app/FragmentActivity;->h:Z

    .line 105929
    iput-boolean p1, p0, Landroid/support/v4/app/FragmentActivity;->i:Z

    .line 105930
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 105931
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->l:Z

    if-eqz v0, :cond_0

    .line 105932
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->l:Z

    .line 105933
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    if-eqz v0, :cond_0

    .line 105934
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->i:Z

    if-nez v0, :cond_2

    .line 105935
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    invoke-virtual {v0}, LX/0k4;->c()V

    .line 105936
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->s()V

    .line 105937
    :cond_1
    return-void

    .line 105938
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    invoke-virtual {v0}, LX/0k4;->d()V

    goto :goto_0
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 106046
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Local FragmentActivity "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 106047
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 106048
    const-string v0, " State:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 106049
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 106050
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "mCreated="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 106051
    iget-boolean v1, p0, Landroid/support/v4/app/FragmentActivity;->e:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, "mResumed="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 106052
    iget-boolean v1, p0, Landroid/support/v4/app/FragmentActivity;->f:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, " mStopped="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 106053
    iget-boolean v1, p0, Landroid/support/v4/app/FragmentActivity;->g:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, " mReallyStopped="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 106054
    iget-boolean v1, p0, Landroid/support/v4/app/FragmentActivity;->h:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    .line 106055
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mLoadersStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 106056
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->l:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 106057
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    if-eqz v0, :cond_0

    .line 106058
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Loader Manager "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 106059
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 106060
    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 106061
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, LX/0k4;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 106062
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/0jz;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 106063
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "View Hierarchy:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 106064
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, p3, v1}, Landroid/support/v4/app/FragmentActivity;->a(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V

    .line 106065
    return-void
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 106066
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->y()Landroid/view/MenuInflater;

    move-result-object v0

    .line 106067
    if-eqz v0, :cond_0

    .line 106068
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    goto :goto_0
.end method

.method public iA_()V
    .locals 2

    .prologue
    .line 106069
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 106070
    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 106071
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->x()V

    .line 106072
    return-void

    .line 106073
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->j:Z

    goto :goto_0
.end method

.method public iC_()LX/0gc;
    .locals 1

    .prologue
    .line 106074
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    return-object v0
.end method

.method public lj_()V
    .locals 1

    .prologue
    .line 106075
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->p()V

    .line 106076
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 105794
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->l()V

    .line 105795
    shr-int/lit8 v0, p1, 0x10

    .line 105796
    if-eqz v0, :cond_2

    .line 105797
    if-gez v0, :cond_0

    .line 105798
    const-string v0, "FragmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Activity result fragment index out of range: 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 105799
    :goto_0
    return-void

    .line 105800
    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v1, v0}, LX/0jz;->c(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    move-object v0, v1

    .line 105801
    if-nez v0, :cond_1

    .line 105802
    const-string v0, "FragmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Activity result no fragment exists for index: 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 105803
    :cond_1
    const v1, 0xffff

    and-int/2addr v1, p1

    invoke-virtual {v0, v1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 105804
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 106077
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106078
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 106079
    invoke-virtual {p0}, Landroid/app/Activity;->finishAfterTransition()V

    .line 106080
    :cond_0
    :goto_0
    return-void

    .line 106081
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 106082
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 106083
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0, p1}, LX/0jz;->a(Landroid/content/res/Configuration;)V

    .line 106084
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x22

    const v3, 0x258d32cd

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 106085
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;->n()LX/0k3;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v4/app/FragmentActivity;->d:LX/0k1;

    invoke-virtual {v0, v3, v4, v1}, LX/0jz;->a(LX/0k3;LX/0k1;Landroid/support/v4/app/Fragment;)V

    .line 106086
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getFactory()Landroid/view/LayoutInflater$Factory;

    move-result-object v0

    if-nez v0, :cond_0

    .line 106087
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/LayoutInflater;->setFactory(Landroid/view/LayoutInflater$Factory;)V

    .line 106088
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 106089
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;->n()LX/0k3;

    move-result-object v0

    invoke-virtual {v0}, LX/0k3;->q()Z

    move-result v0

    if-nez v0, :cond_3

    .line 106090
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k6;

    .line 106091
    if-eqz v0, :cond_1

    .line 106092
    iget-object v3, v0, LX/0k6;->e:LX/01J;

    iput-object v3, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    .line 106093
    :cond_1
    if-eqz p1, :cond_2

    .line 106094
    const-string v3, "android:support:fragments"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    .line 106095
    iget-object v4, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    if-eqz v0, :cond_4

    iget-object v0, v0, LX/0k6;->d:Ljava/util/ArrayList;

    :goto_0
    invoke-virtual {v4, v3, v0}, LX/0jz;->a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    .line 106096
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->m()V

    .line 106097
    :cond_3
    const v0, -0x4433b28c

    invoke-static {v0, v2}, LX/02F;->c(II)V

    return-void

    :cond_4
    move-object v0, v1

    .line 106098
    goto :goto_0
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 106099
    if-nez p1, :cond_1

    .line 106100
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    .line 106101
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, LX/0jz;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 106102
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 106103
    :goto_0
    return v0

    .line 106104
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 106105
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 105939
    if-nez p1, :cond_0

    .line 105940
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->w()Landroid/view/View;

    move-result-object v0

    .line 105941
    if-eqz v0, :cond_0

    .line 105942
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1
    .param p2    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 105773
    const-string v0, "fragment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 105774
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 105775
    :cond_0
    :goto_0
    return-object v0

    .line 105776
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->c:LX/0jz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->c:LX/0jz;

    .line 105777
    :goto_1
    invoke-virtual {v0, p1, p2, p3}, LX/0jz;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 105778
    if-nez v0, :cond_0

    .line 105779
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 105780
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x74985819

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 105781
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 105782
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/support/v4/app/FragmentActivity;->a(Z)V

    .line 105783
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v1}, LX/0jz;->u()V

    .line 105784
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    if-eqz v1, :cond_0

    .line 105785
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    invoke-virtual {v1}, LX/0k4;->h()V

    .line 105786
    :cond_0
    const/16 v1, 0x23

    const v2, 0x1647015f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 105787
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 105788
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 105789
    const/4 v0, 0x1

    .line 105790
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 105791
    invoke-super {p0}, Landroid/app/Activity;->onLowMemory()V

    .line 105792
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->v()V

    .line 105793
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 105805
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105806
    const/4 v0, 0x1

    .line 105807
    :goto_0
    return v0

    .line 105808
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 105809
    const/4 v0, 0x0

    goto :goto_0

    .line 105810
    :sswitch_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0, p2}, LX/0jz;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 105811
    :sswitch_1
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0, p2}, LX/0jz;->b(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 105812
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 105813
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->l()V

    .line 105814
    return-void
.end method

.method public final onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    .prologue
    .line 105815
    packed-switch p1, :pswitch_data_0

    .line 105816
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onPanelClosed(ILandroid/view/Menu;)V

    .line 105817
    return-void

    .line 105818
    :pswitch_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0, p2}, LX/0jz;->b(Landroid/view/Menu;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x4b549fa7    # 1.3934503E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 105819
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 105820
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/app/FragmentActivity;->f:Z

    .line 105821
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->a:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105822
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->a:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 105823
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->lj_()V

    .line 105824
    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v1}, LX/0jz;->q()V

    .line 105825
    const/16 v1, 0x23

    const v2, 0x4b3cfe93    # 1.2385939E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPostResume()V
    .locals 2

    .prologue
    .line 105826
    invoke-super {p0}, Landroid/app/Activity;->onPostResume()V

    .line 105827
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 105828
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->lj_()V

    .line 105829
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->i()Z

    .line 105830
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 105831
    if-nez p1, :cond_2

    if-eqz p3, :cond_2

    .line 105832
    iget-boolean v1, p0, Landroid/support/v4/app/FragmentActivity;->j:Z

    if-eqz v1, :cond_0

    .line 105833
    iput-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->j:Z

    .line 105834
    invoke-interface {p3}, Landroid/view/Menu;->clear()V

    .line 105835
    invoke-virtual {p0, p1, p3}, Landroid/support/v4/app/FragmentActivity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    .line 105836
    :cond_0
    const/4 v1, 0x0

    invoke-super {p0, v1, p2, p3}, Landroid/app/Activity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v1

    move v1, v1

    .line 105837
    iget-object v2, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v2, p3}, LX/0jz;->a(Landroid/view/Menu;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 105838
    if-eqz v1, :cond_1

    invoke-interface {p3}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 105839
    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x1feceb3f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 105840
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 105841
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->a:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 105842
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v4/app/FragmentActivity;->f:Z

    .line 105843
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v1}, LX/0jz;->i()Z

    .line 105844
    const/16 v1, 0x23

    const v2, 0x1b5d8a39

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 105845
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->g:Z

    if-eqz v0, :cond_0

    .line 105846
    invoke-virtual {p0, v1}, Landroid/support/v4/app/FragmentActivity;->a(Z)V

    .line 105847
    :cond_0
    const/4 v0, 0x0

    move-object v5, v0

    .line 105848
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->j()Ljava/util/ArrayList;

    move-result-object v6

    .line 105849
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    if-eqz v0, :cond_3

    .line 105850
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v7

    .line 105851
    new-array v8, v7, [LX/0k4;

    .line 105852
    add-int/lit8 v0, v7, -0x1

    move v4, v0

    :goto_0
    if-ltz v4, :cond_1

    .line 105853
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    invoke-virtual {v0, v4}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k4;

    aput-object v0, v8, v4

    .line 105854
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 105855
    :goto_1
    if-ge v3, v7, :cond_4

    .line 105856
    aget-object v4, v8, v3

    .line 105857
    iget-boolean v9, v4, LX/0k4;->g:Z

    if-eqz v9, :cond_2

    move v0, v1

    .line 105858
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 105859
    :cond_2
    invoke-virtual {v4}, LX/0k4;->h()V

    .line 105860
    iget-object v9, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    iget-object v4, v4, LX/0k4;->d:Ljava/lang/String;

    invoke-virtual {v9, v4}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    move v0, v3

    .line 105861
    :cond_4
    if-nez v6, :cond_5

    if-nez v0, :cond_5

    if-nez v5, :cond_5

    move-object v0, v2

    .line 105862
    :goto_3
    return-object v0

    .line 105863
    :cond_5
    new-instance v0, LX/0k6;

    invoke-direct {v0}, LX/0k6;-><init>()V

    .line 105864
    iput-object v2, v0, LX/0k6;->a:Ljava/lang/Object;

    .line 105865
    iput-object v5, v0, LX/0k6;->b:Ljava/lang/Object;

    .line 105866
    iput-object v2, v0, LX/0k6;->c:LX/01J;

    .line 105867
    iput-object v6, v0, LX/0k6;->d:Ljava/util/ArrayList;

    .line 105868
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    iput-object v1, v0, LX/0k6;->e:LX/01J;

    goto :goto_3
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105869
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 105870
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->k()Landroid/os/Parcelable;

    move-result-object v0

    .line 105871
    if-eqz v0, :cond_0

    .line 105872
    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 105873
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x22

    const v3, 0x2adc5b0a

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 105874
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 105875
    iput-boolean v1, p0, Landroid/support/v4/app/FragmentActivity;->g:Z

    .line 105876
    iput-boolean v1, p0, Landroid/support/v4/app/FragmentActivity;->h:Z

    .line 105877
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 105878
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->e:Z

    if-nez v0, :cond_0

    .line 105879
    iput-boolean v4, p0, Landroid/support/v4/app/FragmentActivity;->e:Z

    .line 105880
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->n()V

    .line 105881
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->l()V

    .line 105882
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->i()Z

    .line 105883
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->l:Z

    if-nez v0, :cond_2

    .line 105884
    iput-boolean v4, p0, Landroid/support/v4/app/FragmentActivity;->l:Z

    .line 105885
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    if-eqz v0, :cond_3

    .line 105886
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    invoke-virtual {v0}, LX/0k4;->b()V

    .line 105887
    :cond_1
    :goto_0
    iput-boolean v4, p0, Landroid/support/v4/app/FragmentActivity;->k:Z

    .line 105888
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v0}, LX/0jz;->o()V

    .line 105889
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    if-eqz v0, :cond_a

    .line 105890
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v4

    .line 105891
    new-array v5, v4, [LX/0k4;

    .line 105892
    add-int/lit8 v0, v4, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_4

    .line 105893
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->m:LX/01J;

    invoke-virtual {v0, v2}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k4;

    aput-object v0, v5, v2

    .line 105894
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    .line 105895
    :cond_3
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentActivity;->k:Z

    if-nez v0, :cond_1

    .line 105896
    const-string v0, "(root)"

    iget-boolean v2, p0, Landroid/support/v4/app/FragmentActivity;->l:Z

    invoke-virtual {p0, v0, v2, v1}, Landroid/support/v4/app/FragmentActivity;->a(Ljava/lang/String;ZZ)LX/0k4;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    .line 105897
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    iget-boolean v0, v0, LX/0k4;->f:Z

    if-nez v0, :cond_1

    .line 105898
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->n:LX/0k4;

    invoke-virtual {v0}, LX/0k4;->b()V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 105899
    :goto_2
    if-ge v0, v4, :cond_a

    .line 105900
    aget-object v1, v5, v0

    .line 105901
    iget-boolean v2, v1, LX/0k4;->g:Z

    if-eqz v2, :cond_9

    .line 105902
    sget-boolean v2, LX/0k4;->a:Z

    if-eqz v2, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "Finished Retaining in "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105903
    :cond_5
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/0k4;->g:Z

    .line 105904
    iget-object v2, v1, LX/0k4;->b:LX/0YU;

    invoke-virtual {v2}, LX/0YU;->a()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_3
    if-ltz v6, :cond_9

    .line 105905
    iget-object v2, v1, LX/0k4;->b:LX/0YU;

    invoke-virtual {v2, v6}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0k7;

    .line 105906
    iget-boolean v7, v2, LX/0k7;->i:Z

    if-eqz v7, :cond_7

    .line 105907
    sget-boolean v7, LX/0k4;->a:Z

    if-eqz v7, :cond_6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string p0, "  Finished Retaining: "

    invoke-direct {v7, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105908
    :cond_6
    const/4 v7, 0x0

    iput-boolean v7, v2, LX/0k7;->i:Z

    .line 105909
    iget-boolean v7, v2, LX/0k7;->h:Z

    iget-boolean p0, v2, LX/0k7;->j:Z

    if-eq v7, p0, :cond_7

    .line 105910
    iget-boolean v7, v2, LX/0k7;->h:Z

    if-nez v7, :cond_7

    .line 105911
    invoke-virtual {v2}, LX/0k7;->e()V

    .line 105912
    :cond_7
    iget-boolean v7, v2, LX/0k7;->h:Z

    if-eqz v7, :cond_8

    iget-boolean v7, v2, LX/0k7;->e:Z

    if-eqz v7, :cond_8

    iget-boolean v7, v2, LX/0k7;->k:Z

    if-nez v7, :cond_8

    .line 105913
    iget-object v7, v2, LX/0k7;->d:LX/0k9;

    iget-object p0, v2, LX/0k7;->g:Ljava/lang/Object;

    invoke-virtual {v2, v7, p0}, LX/0k7;->b(LX/0k9;Ljava/lang/Object;)V

    .line 105914
    :cond_8
    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_3

    .line 105915
    :cond_9
    invoke-virtual {v1}, LX/0k4;->g()V

    .line 105916
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 105917
    :cond_a
    const v0, 0x4d4559ce    # 2.06937312E8f

    invoke-static {v0, v3}, LX/02F;->c(II)V

    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x22

    const v1, -0x8d45830

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 105918
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 105919
    iput-boolean v2, p0, Landroid/support/v4/app/FragmentActivity;->g:Z

    .line 105920
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->a:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 105921
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:LX/0jz;

    invoke-virtual {v1}, LX/0jz;->r()V

    .line 105922
    const/16 v1, 0x23

    const v2, 0x27326704

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 105923
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    const/high16 v0, -0x10000

    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    .line 105924
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can only use lower 16 bits for requestCode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105925
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 105926
    return-void
.end method
