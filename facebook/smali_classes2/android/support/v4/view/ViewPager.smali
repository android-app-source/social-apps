.class public Landroid/support/v4/view/ViewPager;
.super Landroid/view/ViewGroup;
.source ""


# static fields
.field public static final a:[I

.field private static final am:LX/0vc;

.field private static final e:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/0vd;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Landroid/view/animation/Interpolator;


# instance fields
.field private A:Z

.field public B:I

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:I

.field private G:I

.field private H:I

.field private I:F

.field private J:F

.field private K:F

.field private L:F

.field private M:I

.field private N:Landroid/view/VelocityTracker;

.field private O:I

.field private P:I

.field private Q:I

.field private R:I

.field public S:Z

.field private T:J

.field private U:LX/0vj;

.field private V:LX/0vj;

.field private W:Z

.field private aa:Z

.field private ab:Z

.field private ac:I

.field public ad:Z

.field private ae:LX/0hc;

.field private af:LX/0hc;

.field public ag:LX/3ru;

.field private ah:LX/3sI;

.field private ai:Ljava/lang/reflect/Method;

.field private aj:I

.field private ak:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private al:Z

.field private final an:Ljava/lang/Runnable;

.field private ao:I

.field private b:I

.field public c:I

.field public d:F

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0vd;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0vd;

.field private final i:Landroid/graphics/Rect;

.field public j:LX/0gG;

.field public k:I

.field private l:I

.field private m:Landroid/os/Parcelable;

.field private n:Ljava/lang/ClassLoader;

.field private o:Landroid/widget/Scroller;

.field private p:LX/10K;

.field public q:I

.field private r:Landroid/graphics/drawable/Drawable;

.field private s:I

.field private t:I

.field private u:F

.field private v:F

.field private w:I

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 117350
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100b3

    aput v2, v0, v1

    sput-object v0, Landroid/support/v4/view/ViewPager;->a:[I

    .line 117351
    new-instance v0, LX/0va;

    invoke-direct {v0}, LX/0va;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewPager;->e:Ljava/util/Comparator;

    .line 117352
    new-instance v0, LX/0vb;

    invoke-direct {v0}, LX/0vb;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewPager;->f:Landroid/view/animation/Interpolator;

    .line 117353
    new-instance v0, LX/0vc;

    invoke-direct {v0}, LX/0vc;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewPager;->am:LX/0vc;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 117331
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 117332
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    .line 117333
    new-instance v0, LX/0vd;

    invoke-direct {v0}, LX/0vd;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->h:LX/0vd;

    .line 117334
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->i:Landroid/graphics/Rect;

    .line 117335
    iput v3, p0, Landroid/support/v4/view/ViewPager;->l:I

    .line 117336
    iput-object v4, p0, Landroid/support/v4/view/ViewPager;->m:Landroid/os/Parcelable;

    .line 117337
    iput-object v4, p0, Landroid/support/v4/view/ViewPager;->n:Ljava/lang/ClassLoader;

    .line 117338
    const v0, -0x800001

    iput v0, p0, Landroid/support/v4/view/ViewPager;->u:F

    .line 117339
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Landroid/support/v4/view/ViewPager;->v:F

    .line 117340
    iput v2, p0, Landroid/support/v4/view/ViewPager;->B:I

    .line 117341
    iput v3, p0, Landroid/support/v4/view/ViewPager;->M:I

    .line 117342
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->W:Z

    .line 117343
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->aa:Z

    .line 117344
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->ad:Z

    .line 117345
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->al:Z

    .line 117346
    new-instance v0, Landroid/support/v4/view/ViewPager$3;

    invoke-direct {v0, p0}, Landroid/support/v4/view/ViewPager$3;-><init>(Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->an:Ljava/lang/Runnable;

    .line 117347
    iput v1, p0, Landroid/support/v4/view/ViewPager;->ao:I

    .line 117348
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->g()V

    .line 117349
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 117312
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 117313
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    .line 117314
    new-instance v0, LX/0vd;

    invoke-direct {v0}, LX/0vd;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->h:LX/0vd;

    .line 117315
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->i:Landroid/graphics/Rect;

    .line 117316
    iput v3, p0, Landroid/support/v4/view/ViewPager;->l:I

    .line 117317
    iput-object v4, p0, Landroid/support/v4/view/ViewPager;->m:Landroid/os/Parcelable;

    .line 117318
    iput-object v4, p0, Landroid/support/v4/view/ViewPager;->n:Ljava/lang/ClassLoader;

    .line 117319
    const v0, -0x800001

    iput v0, p0, Landroid/support/v4/view/ViewPager;->u:F

    .line 117320
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Landroid/support/v4/view/ViewPager;->v:F

    .line 117321
    iput v2, p0, Landroid/support/v4/view/ViewPager;->B:I

    .line 117322
    iput v3, p0, Landroid/support/v4/view/ViewPager;->M:I

    .line 117323
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->W:Z

    .line 117324
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->aa:Z

    .line 117325
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->ad:Z

    .line 117326
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->al:Z

    .line 117327
    new-instance v0, Landroid/support/v4/view/ViewPager$3;

    invoke-direct {v0, p0}, Landroid/support/v4/view/ViewPager$3;-><init>(Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->an:Ljava/lang/Runnable;

    .line 117328
    iput v1, p0, Landroid/support/v4/view/ViewPager;->ao:I

    .line 117329
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->g()V

    .line 117330
    return-void
.end method

.method private a(IFII)I
    .locals 3

    .prologue
    .line 117301
    invoke-static {p4}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->Q:I

    if-le v0, v1, :cond_2

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->O:I

    if-le v0, v1, :cond_2

    .line 117302
    if-lez p3, :cond_1

    .line 117303
    :goto_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 117304
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 117305
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0vd;

    .line 117306
    iget v0, v0, LX/0vd;->b:I

    iget v1, v1, LX/0vd;->b:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 117307
    :cond_0
    return p1

    .line 117308
    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 117309
    :cond_2
    iget v0, p0, Landroid/support/v4/view/ViewPager;->k:I

    if-lt p1, v0, :cond_3

    const v0, 0x3ecccccd    # 0.4f

    .line 117310
    :goto_1
    int-to-float v1, p1

    add-float/2addr v1, p2

    add-float/2addr v0, v1

    float-to-int p1, v0

    goto :goto_0

    .line 117311
    :cond_3
    const v0, 0x3f19999a    # 0.6f

    goto :goto_1
.end method

.method private a(II)LX/0vd;
    .locals 2

    .prologue
    .line 117293
    new-instance v0, LX/0vd;

    invoke-direct {v0}, LX/0vd;-><init>()V

    .line 117294
    iput p1, v0, LX/0vd;->b:I

    .line 117295
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v1, p0, p1}, LX/0gG;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, LX/0vd;->a:Ljava/lang/Object;

    .line 117296
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v1, p1}, LX/0gG;->d(I)F

    move-result v1

    iput v1, v0, LX/0vd;->d:F

    .line 117297
    if-ltz p2, :cond_0

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p2, v1, :cond_1

    .line 117298
    :cond_0
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117299
    :goto_0
    return-object v0

    .line 117300
    :cond_1
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)LX/0vd;
    .locals 4

    .prologue
    .line 117287
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 117288
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 117289
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget-object v3, v0, LX/0vd;->a:Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, LX/0gG;->a(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 117290
    :goto_1
    return-object v0

    .line 117291
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 117292
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 117269
    if-nez p1, :cond_2

    .line 117270
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 117271
    :goto_0
    if-nez p2, :cond_0

    .line 117272
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    move-object v0, v1

    .line 117273
    :goto_1
    return-object v0

    .line 117274
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 117275
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 117276
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 117277
    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 117278
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 117279
    :goto_2
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    if-eq v0, p0, :cond_1

    .line 117280
    check-cast v0, Landroid/view/ViewGroup;

    .line 117281
    iget v2, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 117282
    iget v2, v1, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getRight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 117283
    iget v2, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 117284
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 117285
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_2

    :cond_1
    move-object v0, v1

    .line 117286
    goto :goto_1

    :cond_2
    move-object v1, p1

    goto :goto_0
.end method

.method private a(III)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 117243
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 117244
    invoke-direct {p0, v5}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 117245
    :goto_0
    return-void

    .line 117246
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    .line 117247
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v2

    .line 117248
    sub-int v3, p1, v1

    .line 117249
    sub-int v4, p2, v2

    .line 117250
    if-nez v3, :cond_1

    if-nez v4, :cond_1

    .line 117251
    invoke-direct {p0, v5}, Landroid/support/v4/view/ViewPager;->a(Z)V

    .line 117252
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 117253
    invoke-static {p0, v5}, Landroid/support/v4/view/ViewPager;->setScrollState(Landroid/support/v4/view/ViewPager;I)V

    goto :goto_0

    .line 117254
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 117255
    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/support/v4/view/ViewPager;->setScrollState(Landroid/support/v4/view/ViewPager;I)V

    .line 117256
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v0

    .line 117257
    div-int/lit8 v5, v0, 0x2

    .line 117258
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v8

    int-to-float v7, v0

    div-float/2addr v6, v7

    invoke-static {v8, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 117259
    int-to-float v7, v5

    int-to-float v5, v5

    invoke-static {v6}, Landroid/support/v4/view/ViewPager;->b(F)F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    .line 117260
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 117261
    if-lez v6, :cond_2

    .line 117262
    const/high16 v0, 0x447a0000    # 1000.0f

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    mul-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 117263
    :goto_1
    const/16 v5, 0x258

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 117264
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 117265
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    goto :goto_0

    .line 117266
    :cond_2
    int-to-float v0, v0

    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget v6, p0, Landroid/support/v4/view/ViewPager;->k:I

    invoke-virtual {v5, v6}, LX/0gG;->d(I)F

    move-result v5

    mul-float/2addr v0, v5

    .line 117267
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Landroid/support/v4/view/ViewPager;->q:I

    int-to-float v6, v6

    add-float/2addr v0, v6

    div-float v0, v5, v0

    .line 117268
    add-float/2addr v0, v8

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v0, v5

    float-to-int v0, v0

    goto :goto_1
.end method

.method private a(IIII)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 117224
    if-lez p2, :cond_1

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 117225
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/2addr v0, p3

    .line 117226
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    sub-int v1, p2, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v3

    sub-int/2addr v1, v3

    add-int/2addr v1, p4

    .line 117227
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v3

    .line 117228
    int-to-float v3, v3

    int-to-float v1, v1

    div-float v1, v3, v1

    .line 117229
    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 117230
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 117231
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 117232
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getDuration()I

    move-result v0

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->timePassed()I

    move-result v3

    sub-int v5, v0, v3

    .line 117233
    iget v0, p0, Landroid/support/v4/view/ViewPager;->k:I

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->c(I)LX/0vd;

    move-result-object v3

    .line 117234
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    iget v3, v3, LX/0vd;->e:F

    int-to-float v4, p1

    mul-float/2addr v3, v4

    float-to-int v3, v3

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 117235
    :cond_0
    :goto_0
    return-void

    .line 117236
    :cond_1
    iget v0, p0, Landroid/support/v4/view/ViewPager;->k:I

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->c(I)LX/0vd;

    move-result-object v0

    .line 117237
    if-eqz v0, :cond_2

    iget v0, v0, LX/0vd;->e:F

    iget v1, p0, Landroid/support/v4/view/ViewPager;->v:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 117238
    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v3

    sub-int/2addr v1, v3

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 117239
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 117240
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->a(Z)V

    .line 117241
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    goto :goto_0

    .line 117242
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(IZIZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 116920
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->c(I)LX/0vd;

    move-result-object v0

    .line 116921
    if-eqz v0, :cond_5

    .line 116922
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v2

    .line 116923
    int-to-float v2, v2

    iget v3, p0, Landroid/support/v4/view/ViewPager;->u:F

    iget v0, v0, LX/0vd;->e:F

    iget v4, p0, Landroid/support/v4/view/ViewPager;->v:F

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 116924
    :goto_0
    if-eqz p2, :cond_2

    .line 116925
    invoke-direct {p0, v0, v1, p3}, Landroid/support/v4/view/ViewPager;->a(III)V

    .line 116926
    if-eqz p4, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    if-eqz v0, :cond_0

    .line 116927
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    .line 116928
    :cond_0
    if-eqz p4, :cond_1

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->af:LX/0hc;

    if-eqz v0, :cond_1

    .line 116929
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->af:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    .line 116930
    :cond_1
    :goto_1
    return-void

    .line 116931
    :cond_2
    if-eqz p4, :cond_3

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    if-eqz v2, :cond_3

    .line 116932
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    invoke-interface {v2, p1}, LX/0hc;->B_(I)V

    .line 116933
    :cond_3
    if-eqz p4, :cond_4

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->af:LX/0hc;

    if-eqz v2, :cond_4

    .line 116934
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->af:LX/0hc;

    invoke-interface {v2, p1}, LX/0hc;->B_(I)V

    .line 116935
    :cond_4
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->a(Z)V

    .line 116936
    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private a(IZZ)V
    .locals 1

    .prologue
    .line 117206
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/view/ViewPager;->a(IZZI)V

    .line 117207
    return-void
.end method

.method private a(IZZI)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 117179
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-gtz v0, :cond_1

    .line 117180
    :cond_0
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 117181
    :goto_0
    return-void

    .line 117182
    :cond_1
    if-nez p3, :cond_2

    iget v0, p0, Landroid/support/v4/view/ViewPager;->k:I

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 117183
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    goto :goto_0

    .line 117184
    :cond_2
    if-gez p1, :cond_5

    move p1, v1

    .line 117185
    :cond_3
    :goto_1
    iget v0, p0, Landroid/support/v4/view/ViewPager;->B:I

    .line 117186
    iget v2, p0, Landroid/support/v4/view/ViewPager;->k:I

    add-int/2addr v2, v0

    if-gt p1, v2, :cond_4

    iget v2, p0, Landroid/support/v4/view/ViewPager;->k:I

    sub-int v0, v2, v0

    if-ge p1, v0, :cond_6

    :cond_4
    move v2, v1

    .line 117187
    :goto_2
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 117188
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    iput-boolean v3, v0, LX/0vd;->c:Z

    .line 117189
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 117190
    :cond_5
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-lt p1, v0, :cond_3

    .line 117191
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    goto :goto_1

    .line 117192
    :cond_6
    iget v0, p0, Landroid/support/v4/view/ViewPager;->k:I

    if-eq v0, p1, :cond_9

    move v0, v3

    .line 117193
    :goto_3
    iget-boolean v2, p0, Landroid/support/v4/view/ViewPager;->W:Z

    if-eqz v2, :cond_a

    .line 117194
    iput p1, p0, Landroid/support/v4/view/ViewPager;->k:I

    .line 117195
    if-eqz v0, :cond_7

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    if-eqz v1, :cond_7

    .line 117196
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    invoke-interface {v1, p1}, LX/0hc;->B_(I)V

    .line 117197
    :cond_7
    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->af:LX/0hc;

    if-eqz v0, :cond_8

    .line 117198
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->af:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    .line 117199
    :cond_8
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    goto :goto_0

    :cond_9
    move v0, v1

    .line 117200
    goto :goto_3

    .line 117201
    :cond_a
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->b(I)V

    .line 117202
    invoke-direct {p0, p1, p2, p4, v0}, Landroid/support/v4/view/ViewPager;->a(IZIZ)V

    .line 117203
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 117204
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->j()LX/0vd;

    move-result-object v0

    iget v0, v0, LX/0vd;->b:I

    iput v0, p0, Landroid/support/v4/view/ViewPager;->c:I

    goto/16 :goto_0

    .line 117205
    :cond_b
    iput v1, p0, Landroid/support/v4/view/ViewPager;->c:I

    goto/16 :goto_0
.end method

.method private a(LX/0vd;ILX/0vd;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 117116
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v7

    .line 117117
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v0

    .line 117118
    if-lez v0, :cond_0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->q:I

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    move v6, v0

    .line 117119
    :goto_0
    if-eqz p3, :cond_4

    .line 117120
    iget v0, p3, LX/0vd;->b:I

    .line 117121
    iget v1, p1, LX/0vd;->b:I

    if-ge v0, v1, :cond_2

    .line 117122
    iget v1, p3, LX/0vd;->e:F

    iget v2, p3, LX/0vd;->d:F

    add-float/2addr v1, v2

    add-float/2addr v1, v6

    .line 117123
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v3, v4

    move v1, v0

    .line 117124
    :goto_1
    iget v0, p1, LX/0vd;->b:I

    if-gt v1, v0, :cond_4

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 117125
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 117126
    :goto_2
    iget v5, v0, LX/0vd;->b:I

    if-le v1, v5, :cond_e

    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v3, v5, :cond_e

    .line 117127
    add-int/lit8 v3, v3, 0x1

    .line 117128
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    goto :goto_2

    .line 117129
    :cond_0
    const/4 v0, 0x0

    move v6, v0

    goto :goto_0

    .line 117130
    :goto_3
    iget v5, v0, LX/0vd;->b:I

    if-ge v2, v5, :cond_1

    .line 117131
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v5, v2}, LX/0gG;->d(I)F

    move-result v5

    add-float/2addr v5, v6

    add-float/2addr v5, v1

    .line 117132
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v5

    goto :goto_3

    .line 117133
    :cond_1
    iput v1, v0, LX/0vd;->e:F

    .line 117134
    iget v0, v0, LX/0vd;->d:F

    add-float/2addr v0, v6

    add-float/2addr v1, v0

    .line 117135
    add-int/lit8 v0, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 117136
    :cond_2
    iget v1, p1, LX/0vd;->b:I

    if-le v0, v1, :cond_4

    .line 117137
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    .line 117138
    iget v1, p3, LX/0vd;->e:F

    .line 117139
    add-int/lit8 v0, v0, -0x1

    move v3, v2

    move v2, v1

    move v1, v0

    .line 117140
    :goto_4
    iget v0, p1, LX/0vd;->b:I

    if-lt v1, v0, :cond_4

    if-ltz v3, :cond_4

    .line 117141
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 117142
    :goto_5
    iget v5, v0, LX/0vd;->b:I

    if-ge v1, v5, :cond_d

    if-lez v3, :cond_d

    .line 117143
    add-int/lit8 v3, v3, -0x1

    .line 117144
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    goto :goto_5

    .line 117145
    :goto_6
    iget v5, v0, LX/0vd;->b:I

    if-le v2, v5, :cond_3

    .line 117146
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v5, v2}, LX/0gG;->d(I)F

    move-result v5

    add-float/2addr v5, v6

    sub-float v5, v1, v5

    .line 117147
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v5

    goto :goto_6

    .line 117148
    :cond_3
    iget v5, v0, LX/0vd;->d:F

    add-float/2addr v5, v6

    sub-float/2addr v1, v5

    .line 117149
    iput v1, v0, LX/0vd;->e:F

    .line 117150
    add-int/lit8 v0, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_4

    .line 117151
    :cond_4
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 117152
    iget v2, p1, LX/0vd;->e:F

    .line 117153
    iget v0, p1, LX/0vd;->b:I

    add-int/lit8 v1, v0, -0x1

    .line 117154
    iget v0, p1, LX/0vd;->b:I

    if-nez v0, :cond_5

    iget v0, p1, LX/0vd;->e:F

    :goto_7
    iput v0, p0, Landroid/support/v4/view/ViewPager;->u:F

    .line 117155
    iget v0, p1, LX/0vd;->b:I

    add-int/lit8 v3, v7, -0x1

    if-ne v0, v3, :cond_6

    iget v0, p1, LX/0vd;->e:F

    iget v3, p1, LX/0vd;->d:F

    add-float/2addr v0, v3

    sub-float/2addr v0, v10

    :goto_8
    iput v0, p0, Landroid/support/v4/view/ViewPager;->v:F

    .line 117156
    add-int/lit8 v0, p2, -0x1

    move v5, v0

    :goto_9
    if-ltz v5, :cond_9

    .line 117157
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    move v3, v2

    .line 117158
    :goto_a
    iget v2, v0, LX/0vd;->b:I

    if-le v1, v2, :cond_7

    .line 117159
    iget-object v9, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v9, v1}, LX/0gG;->d(I)F

    move-result v1

    add-float/2addr v1, v6

    sub-float v1, v3, v1

    move v3, v1

    move v1, v2

    goto :goto_a

    .line 117160
    :cond_5
    const v0, -0x800001

    goto :goto_7

    .line 117161
    :cond_6
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_8

    .line 117162
    :cond_7
    iget v2, v0, LX/0vd;->d:F

    add-float/2addr v2, v6

    sub-float v2, v3, v2

    .line 117163
    iput v2, v0, LX/0vd;->e:F

    .line 117164
    iget v0, v0, LX/0vd;->b:I

    if-nez v0, :cond_8

    iput v2, p0, Landroid/support/v4/view/ViewPager;->u:F

    .line 117165
    :cond_8
    add-int/lit8 v0, v5, -0x1

    add-int/lit8 v1, v1, -0x1

    move v5, v0

    goto :goto_9

    .line 117166
    :cond_9
    iget v0, p1, LX/0vd;->e:F

    iget v1, p1, LX/0vd;->d:F

    add-float/2addr v0, v1

    add-float v2, v0, v6

    .line 117167
    iget v0, p1, LX/0vd;->b:I

    add-int/lit8 v1, v0, 0x1

    .line 117168
    add-int/lit8 v0, p2, 0x1

    move v5, v0

    :goto_b
    if-ge v5, v8, :cond_c

    .line 117169
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    move v3, v2

    .line 117170
    :goto_c
    iget v2, v0, LX/0vd;->b:I

    if-ge v1, v2, :cond_a

    .line 117171
    iget-object v9, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v9, v1}, LX/0gG;->d(I)F

    move-result v1

    add-float/2addr v1, v6

    add-float/2addr v1, v3

    move v3, v1

    move v1, v2

    goto :goto_c

    .line 117172
    :cond_a
    iget v2, v0, LX/0vd;->b:I

    add-int/lit8 v9, v7, -0x1

    if-ne v2, v9, :cond_b

    .line 117173
    iget v2, v0, LX/0vd;->d:F

    add-float/2addr v2, v3

    sub-float/2addr v2, v10

    iput v2, p0, Landroid/support/v4/view/ViewPager;->v:F

    .line 117174
    :cond_b
    iput v3, v0, LX/0vd;->e:F

    .line 117175
    iget v0, v0, LX/0vd;->d:F

    add-float/2addr v0, v6

    add-float v2, v3, v0

    .line 117176
    add-int/lit8 v0, v5, 0x1

    add-int/lit8 v1, v1, 0x1

    move v5, v0

    goto :goto_b

    .line 117177
    :cond_c
    iput-boolean v4, p0, Landroid/support/v4/view/ViewPager;->aa:Z

    .line 117178
    return-void

    :cond_d
    move v11, v1

    move v1, v2

    move v2, v11

    goto/16 :goto_6

    :cond_e
    move v11, v1

    move v1, v2

    move v2, v11

    goto/16 :goto_3
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 117106
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 117107
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 117108
    iget v2, p0, Landroid/support/v4/view/ViewPager;->M:I

    if-ne v1, v2, :cond_0

    .line 117109
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 117110
    :goto_0
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 117111
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->M:I

    .line 117112
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 117113
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 117114
    :cond_0
    return-void

    .line 117115
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 117084
    iget v0, p0, Landroid/support/v4/view/ViewPager;->ao:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    move v0, v4

    .line 117085
    :goto_0
    if-eqz v0, :cond_1

    .line 117086
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 117087
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    .line 117088
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    .line 117089
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v3

    .line 117090
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrX()I

    move-result v5

    .line 117091
    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v6

    .line 117092
    if-ne v1, v5, :cond_0

    if-eq v3, v6, :cond_1

    .line 117093
    :cond_0
    invoke-virtual {p0, v5, v6}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 117094
    :cond_1
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->A:Z

    move v1, v2

    move v3, v0

    .line 117095
    :goto_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 117096
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 117097
    iget-boolean v5, v0, LX/0vd;->c:Z

    if-eqz v5, :cond_2

    .line 117098
    iput-boolean v2, v0, LX/0vd;->c:Z

    move v3, v4

    .line 117099
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 117100
    goto :goto_0

    .line 117101
    :cond_4
    if-eqz v3, :cond_5

    .line 117102
    if-eqz p1, :cond_6

    .line 117103
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->an:Ljava/lang/Runnable;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 117104
    :cond_5
    :goto_2
    return-void

    .line 117105
    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->an:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_2
.end method

.method private a(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 117073
    const/4 v0, 0x0

    .line 117074
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 117075
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 117076
    :cond_0
    :goto_0
    return v0

    .line 117077
    :sswitch_0
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->a(I)Z

    move-result v0

    goto :goto_0

    .line 117078
    :sswitch_1
    const/16 v0, 0x42

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->a(I)Z

    move-result v0

    goto :goto_0

    .line 117079
    :sswitch_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 117080
    invoke-static {p1}, LX/3rb;->a(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117081
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->a(I)Z

    move-result v0

    goto :goto_0

    .line 117082
    :cond_1
    invoke-static {p1, v3}, LX/3rb;->a(Landroid/view/KeyEvent;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117083
    invoke-virtual {p0, v3}, Landroid/support/v4/view/ViewPager;->a(I)Z

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x3d -> :sswitch_2
    .end sparse-switch
.end method

.method private static b(F)F
    .locals 4

    .prologue
    .line 117070
    const/high16 v0, 0x3f000000    # 0.5f

    sub-float v0, p0, v0

    .line 117071
    float-to-double v0, v0

    const-wide v2, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 117072
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private b(Landroid/view/View;)LX/0vd;
    .locals 2

    .prologue
    .line 117064
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_2

    .line 117065
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_1

    .line 117066
    :cond_0
    const/4 v0, 0x0

    .line 117067
    :goto_1
    return-object v0

    .line 117068
    :cond_1
    check-cast v0, Landroid/view/View;

    move-object p1, v0

    goto :goto_0

    .line 117069
    :cond_2
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)LX/0vd;

    move-result-object v0

    goto :goto_1
.end method

.method private b(I)V
    .locals 20

    .prologue
    .line 116950
    const/4 v3, 0x0

    .line 116951
    const/4 v2, 0x2

    .line 116952
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v4/view/ViewPager;->k:I

    move/from16 v0, p1

    if-eq v4, v0, :cond_23

    .line 116953
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/ViewPager;->k:I

    move/from16 v0, p1

    if-ge v2, v0, :cond_1

    const/16 v2, 0x42

    .line 116954
    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/view/ViewPager;->k:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Landroid/support/v4/view/ViewPager;->c(I)LX/0vd;

    move-result-object v3

    .line 116955
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/ViewPager;->k:I

    move-object v4, v3

    move v3, v2

    .line 116956
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    if-nez v2, :cond_2

    .line 116957
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->i()V

    .line 116958
    :cond_0
    :goto_2
    return-void

    .line 116959
    :cond_1
    const/16 v2, 0x11

    goto :goto_0

    .line 116960
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Landroid/support/v4/view/ViewPager;->A:Z

    if-eqz v2, :cond_3

    .line 116961
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->i()V

    goto :goto_2

    .line 116962
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Landroid/support/v4/view/ViewPager;->al:Z

    if-eqz v2, :cond_0

    .line 116963
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, LX/0gG;->a(Landroid/view/ViewGroup;)V

    .line 116964
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/ViewPager;->B:I

    .line 116965
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v4/view/ViewPager;->k:I

    sub-int/2addr v6, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 116966
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v5}, LX/0gG;->b()I

    move-result v15

    .line 116967
    add-int/lit8 v5, v15, -0x1

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v4/view/ViewPager;->k:I

    add-int/2addr v2, v6

    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 116968
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/ViewPager;->b:I

    if-eq v15, v2, :cond_4

    .line 116969
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 116970
    :goto_3
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "The application\'s PagerAdapter changed the adapter\'s contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v4/view/ViewPager;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Pager id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " Pager class: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " Problematic adapter: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 116971
    :catch_0
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 116972
    :cond_4
    const/4 v6, 0x0

    .line 116973
    const/4 v2, 0x0

    move v5, v2

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v5, v2, :cond_22

    .line 116974
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0vd;

    .line 116975
    iget v7, v2, LX/0vd;->b:I

    move-object/from16 v0, p0

    iget v9, v0, Landroid/support/v4/view/ViewPager;->k:I

    if-lt v7, v9, :cond_6

    .line 116976
    iget v7, v2, LX/0vd;->b:I

    move-object/from16 v0, p0

    iget v9, v0, Landroid/support/v4/view/ViewPager;->k:I

    if-ne v7, v9, :cond_22

    .line 116977
    :goto_5
    if-nez v2, :cond_21

    if-lez v15, :cond_21

    .line 116978
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/ViewPager;->k:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, Landroid/support/v4/view/ViewPager;->a(II)LX/0vd;

    move-result-object v2

    move-object v14, v2

    .line 116979
    :goto_6
    if-eqz v14, :cond_19

    .line 116980
    const/4 v13, 0x0

    .line 116981
    add-int/lit8 v12, v5, -0x1

    .line 116982
    if-ltz v12, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0vd;

    .line 116983
    :goto_7
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v16

    .line 116984
    if-gtz v16, :cond_8

    const/4 v6, 0x0

    .line 116985
    :goto_8
    move-object/from16 v0, p0

    iget-boolean v7, v0, Landroid/support/v4/view/ViewPager;->C:Z

    if-eqz v7, :cond_9

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Landroid/support/v4/view/ViewPager;->k:I

    add-int/lit8 v9, v9, -0x1

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 116986
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v9, v0, Landroid/support/v4/view/ViewPager;->C:Z

    if-eqz v9, :cond_a

    add-int/lit8 v9, v15, -0x1

    move-object/from16 v0, p0

    iget v11, v0, Landroid/support/v4/view/ViewPager;->k:I

    add-int/lit8 v11, v11, 0x1

    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 116987
    :goto_a
    move-object/from16 v0, p0

    iget v11, v0, Landroid/support/v4/view/ViewPager;->k:I

    add-int/lit8 v11, v11, -0x1

    move/from16 v18, v11

    move v11, v13

    move/from16 v13, v18

    move/from16 v19, v12

    move v12, v5

    move/from16 v5, v19

    :goto_b
    if-ltz v13, :cond_10

    .line 116988
    cmpl-float v17, v11, v6

    if-ltz v17, :cond_c

    if-ge v13, v8, :cond_c

    .line 116989
    if-eqz v2, :cond_10

    .line 116990
    iget v0, v2, LX/0vd;->b:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v13, v0, :cond_5

    iget-boolean v0, v2, LX/0vd;->c:Z

    move/from16 v17, v0

    if-nez v17, :cond_5

    .line 116991
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 116992
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    move-object/from16 v17, v0

    iget-object v2, v2, LX/0vd;->a:Ljava/lang/Object;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v13, v2}, LX/0gG;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 116993
    add-int/lit8 v5, v5, -0x1

    .line 116994
    add-int/lit8 v12, v12, -0x1

    .line 116995
    if-ltz v5, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0vd;

    .line 116996
    :cond_5
    :goto_c
    add-int/lit8 v13, v13, -0x1

    goto :goto_b

    .line 116997
    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_4

    .line 116998
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 116999
    :cond_8
    const/high16 v6, 0x40000000    # 2.0f

    iget v7, v14, LX/0vd;->d:F

    sub-float/2addr v6, v7

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v7

    int-to-float v7, v7

    move/from16 v0, v16

    int-to-float v9, v0

    div-float/2addr v7, v9

    add-float/2addr v6, v7

    goto/16 :goto_8

    :cond_9
    move v7, v8

    .line 117000
    goto/16 :goto_9

    :cond_a
    move v9, v10

    .line 117001
    goto :goto_a

    .line 117002
    :cond_b
    const/4 v2, 0x0

    goto :goto_c

    .line 117003
    :cond_c
    if-eqz v2, :cond_e

    iget v0, v2, LX/0vd;->b:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v13, v0, :cond_e

    .line 117004
    iget v2, v2, LX/0vd;->d:F

    add-float/2addr v11, v2

    .line 117005
    add-int/lit8 v5, v5, -0x1

    .line 117006
    if-ltz v5, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0vd;

    goto :goto_c

    :cond_d
    const/4 v2, 0x0

    goto :goto_c

    .line 117007
    :cond_e
    if-lt v13, v7, :cond_5

    .line 117008
    add-int/lit8 v2, v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v2}, Landroid/support/v4/view/ViewPager;->a(II)LX/0vd;

    move-result-object v2

    .line 117009
    iget v2, v2, LX/0vd;->d:F

    add-float/2addr v11, v2

    .line 117010
    add-int/lit8 v12, v12, 0x1

    .line 117011
    if-ltz v5, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0vd;

    goto :goto_c

    :cond_f
    const/4 v2, 0x0

    goto :goto_c

    .line 117012
    :cond_10
    iget v6, v14, LX/0vd;->d:F

    .line 117013
    add-int/lit8 v8, v12, 0x1

    .line 117014
    const/high16 v2, 0x40000000    # 2.0f

    cmpg-float v2, v6, v2

    if-gez v2, :cond_18

    .line 117015
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v8, v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0vd;

    move-object v7, v2

    .line 117016
    :goto_d
    if-gtz v16, :cond_12

    const/4 v2, 0x0

    move v5, v2

    .line 117017
    :goto_e
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/ViewPager;->k:I

    add-int/lit8 v2, v2, 0x1

    move/from16 v18, v2

    move-object v2, v7

    move v7, v8

    move/from16 v8, v18

    :goto_f
    if-ge v8, v15, :cond_18

    .line 117018
    cmpl-float v11, v6, v5

    if-ltz v11, :cond_14

    if-le v8, v10, :cond_14

    .line 117019
    if-eqz v2, :cond_18

    .line 117020
    iget v11, v2, LX/0vd;->b:I

    if-ne v8, v11, :cond_20

    iget-boolean v11, v2, LX/0vd;->c:Z

    if-nez v11, :cond_20

    .line 117021
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 117022
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget-object v2, v2, LX/0vd;->a:Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-virtual {v11, v0, v8, v2}, LX/0gG;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 117023
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v7, v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0vd;

    :goto_10
    move/from16 v18, v6

    move-object v6, v2

    move/from16 v2, v18

    .line 117024
    :goto_11
    add-int/lit8 v8, v8, 0x1

    move/from16 v18, v2

    move-object v2, v6

    move/from16 v6, v18

    goto :goto_f

    .line 117025
    :cond_11
    const/4 v7, 0x0

    goto :goto_d

    .line 117026
    :cond_12
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v2

    int-to-float v2, v2

    move/from16 v0, v16

    int-to-float v5, v0

    div-float/2addr v2, v5

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v2, v5

    move v5, v2

    goto :goto_e

    .line 117027
    :cond_13
    const/4 v2, 0x0

    goto :goto_10

    .line 117028
    :cond_14
    if-eqz v2, :cond_16

    iget v11, v2, LX/0vd;->b:I

    if-ne v8, v11, :cond_16

    .line 117029
    iget v2, v2, LX/0vd;->d:F

    add-float/2addr v6, v2

    .line 117030
    add-int/lit8 v7, v7, 0x1

    .line 117031
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v7, v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0vd;

    :goto_12
    move/from16 v18, v6

    move-object v6, v2

    move/from16 v2, v18

    goto :goto_11

    :cond_15
    const/4 v2, 0x0

    goto :goto_12

    .line 117032
    :cond_16
    if-gt v8, v9, :cond_20

    .line 117033
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v7}, Landroid/support/v4/view/ViewPager;->a(II)LX/0vd;

    move-result-object v2

    .line 117034
    add-int/lit8 v7, v7, 0x1

    .line 117035
    iget v2, v2, LX/0vd;->d:F

    add-float/2addr v6, v2

    .line 117036
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v7, v2, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0vd;

    :goto_13
    move/from16 v18, v6

    move-object v6, v2

    move/from16 v2, v18

    goto :goto_11

    :cond_17
    const/4 v2, 0x0

    goto :goto_13

    .line 117037
    :cond_18
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v12, v4}, Landroid/support/v4/view/ViewPager;->a(LX/0vd;ILX/0vd;)V

    .line 117038
    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v4/view/ViewPager;->k:I

    if-eqz v14, :cond_1b

    iget-object v2, v14, LX/0vd;->a:Ljava/lang/Object;

    :goto_14
    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v5, v2}, LX/0gG;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 117039
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, LX/0gG;->b(Landroid/view/ViewGroup;)V

    .line 117040
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v5

    .line 117041
    const/4 v2, 0x0

    move v4, v2

    :goto_15
    if-ge v4, v5, :cond_1c

    .line 117042
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 117043
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/18u;

    .line 117044
    iput v4, v2, LX/18u;->f:I

    .line 117045
    iget-boolean v7, v2, LX/18u;->a:Z

    if-nez v7, :cond_1a

    iget v7, v2, LX/18u;->c:F

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-nez v7, :cond_1a

    .line 117046
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)LX/0vd;

    move-result-object v6

    .line 117047
    if-eqz v6, :cond_1a

    .line 117048
    iget v7, v6, LX/0vd;->d:F

    iput v7, v2, LX/18u;->c:F

    .line 117049
    iget v6, v6, LX/0vd;->b:I

    iput v6, v2, LX/18u;->e:I

    .line 117050
    :cond_1a
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_15

    .line 117051
    :cond_1b
    const/4 v2, 0x0

    goto :goto_14

    .line 117052
    :cond_1c
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->i()V

    .line 117053
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 117054
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 117055
    if-eqz v2, :cond_1f

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Landroid/support/v4/view/ViewPager;->b(Landroid/view/View;)LX/0vd;

    move-result-object v2

    .line 117056
    :goto_16
    if-eqz v2, :cond_1d

    iget v2, v2, LX/0vd;->b:I

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v4/view/ViewPager;->k:I

    if-eq v2, v4, :cond_0

    .line 117057
    :cond_1d
    const/4 v2, 0x0

    :goto_17
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 117058
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 117059
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)LX/0vd;

    move-result-object v5

    .line 117060
    if-eqz v5, :cond_1e

    iget v5, v5, LX/0vd;->b:I

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v4/view/ViewPager;->k:I

    if-ne v5, v6, :cond_1e

    .line 117061
    invoke-virtual {v4, v3}, Landroid/view/View;->requestFocus(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 117062
    :cond_1e
    add-int/lit8 v2, v2, 0x1

    goto :goto_17

    .line 117063
    :cond_1f
    const/4 v2, 0x0

    goto :goto_16

    :cond_20
    move/from16 v18, v6

    move-object v6, v2

    move/from16 v2, v18

    goto/16 :goto_11

    :cond_21
    move-object v14, v2

    goto/16 :goto_6

    :cond_22
    move-object v2, v6

    goto/16 :goto_5

    :cond_23
    move-object v4, v3

    move v3, v2

    goto/16 :goto_1
.end method

.method private b(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 116943
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v3

    move v2, v1

    .line 116944
    :goto_0
    if-ge v2, v3, :cond_1

    .line 116945
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    .line 116946
    :goto_1
    invoke-virtual {p0, v2}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 116947
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 116948
    goto :goto_1

    .line 116949
    :cond_1
    return-void
.end method

.method private c(I)LX/0vd;
    .locals 3

    .prologue
    .line 116937
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 116938
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 116939
    iget v2, v0, LX/0vd;->b:I

    if-ne v2, p1, :cond_0

    .line 116940
    :goto_1
    return-object v0

    .line 116941
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 116942
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c(Z)V
    .locals 1

    .prologue
    .line 117561
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 117562
    if-eqz v0, :cond_0

    .line 117563
    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 117564
    :cond_0
    return-void
.end method

.method private c(F)Z
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 117362
    iget v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    sub-float/2addr v0, p1

    .line 117363
    iput p1, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 117364
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    .line 117365
    add-float v5, v1, v0

    .line 117366
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v7

    .line 117367
    int-to-float v0, v7

    iget v1, p0, Landroid/support/v4/view/ViewPager;->u:F

    mul-float v4, v0, v1

    .line 117368
    int-to-float v0, v7

    iget v1, p0, Landroid/support/v4/view/ViewPager;->v:F

    mul-float v6, v0, v1

    .line 117369
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 117370
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    iget-object v8, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0vd;

    .line 117371
    iget v8, v0, LX/0vd;->b:I

    if-eqz v8, :cond_5

    .line 117372
    iget v0, v0, LX/0vd;->e:F

    int-to-float v4, v7

    mul-float/2addr v0, v4

    move v4, v0

    move v0, v2

    .line 117373
    :goto_0
    iget v8, v1, LX/0vd;->b:I

    iget-object v9, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v9}, LX/0gG;->b()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-eq v8, v9, :cond_4

    .line 117374
    iget v1, v1, LX/0vd;->e:F

    int-to-float v3, v7

    mul-float/2addr v1, v3

    move v3, v2

    .line 117375
    :goto_1
    cmpg-float v6, v5, v4

    if-gez v6, :cond_1

    .line 117376
    if-eqz v0, :cond_0

    .line 117377
    sub-float v0, v4, v5

    .line 117378
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->U:LX/0vj;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v2, v7

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, LX/0vj;->a(F)Z

    move-result v2

    .line 117379
    :cond_0
    :goto_2
    iget v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    float-to-int v1, v4

    int-to-float v1, v1

    sub-float v1, v4, v1

    add-float/2addr v0, v1

    iput v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 117380
    float-to-int v0, v4

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 117381
    float-to-int v0, v4

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->d(I)Z

    .line 117382
    return v2

    .line 117383
    :cond_1
    cmpl-float v0, v5, v1

    if-lez v0, :cond_3

    .line 117384
    if-eqz v3, :cond_2

    .line 117385
    sub-float v0, v5, v1

    .line 117386
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->V:LX/0vj;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v3, v7

    div-float/2addr v0, v3

    invoke-virtual {v2, v0}, LX/0vj;->a(F)Z

    move-result v2

    :cond_2
    move v4, v1

    .line 117387
    goto :goto_2

    :cond_3
    move v4, v5

    goto :goto_2

    :cond_4
    move v1, v6

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_0
.end method

.method private d(I)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 117565
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 117566
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ab:Z

    .line 117567
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v0}, Landroid/support/v4/view/ViewPager;->a(IFI)V

    .line 117568
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->ab:Z

    if-nez v1, :cond_2

    .line 117569
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onPageScrolled did not call superclass implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117570
    :cond_0
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->j()LX/0vd;

    move-result-object v1

    .line 117571
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v2

    .line 117572
    iget v3, p0, Landroid/support/v4/view/ViewPager;->q:I

    add-int/2addr v3, v2

    .line 117573
    iget v4, p0, Landroid/support/v4/view/ViewPager;->q:I

    int-to-float v4, v4

    int-to-float v5, v2

    div-float/2addr v4, v5

    .line 117574
    iget v5, v1, LX/0vd;->b:I

    .line 117575
    int-to-float v6, p1

    int-to-float v2, v2

    div-float v2, v6, v2

    iget v6, v1, LX/0vd;->e:F

    sub-float/2addr v2, v6

    iget v1, v1, LX/0vd;->d:F

    add-float/2addr v1, v4

    div-float v1, v2, v1

    .line 117576
    int-to-float v2, v3

    mul-float/2addr v2, v1

    float-to-int v2, v2

    .line 117577
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ab:Z

    .line 117578
    invoke-virtual {p0, v5, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IFI)V

    .line 117579
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ab:Z

    if-nez v0, :cond_1

    .line 117580
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onPageScrolled did not call superclass implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117581
    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 117582
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setWillNotDraw(Z)V

    .line 117583
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setDescendantFocusability(I)V

    .line 117584
    invoke-virtual {p0, v4}, Landroid/support/v4/view/ViewPager;->setFocusable(Z)V

    .line 117585
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 117586
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Landroid/support/v4/view/ViewPager;->f:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v0, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    .line 117587
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 117588
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 117589
    invoke-static {v1}, LX/0iP;->a(Landroid/view/ViewConfiguration;)I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/ViewPager;->H:I

    .line 117590
    const/high16 v3, 0x43c80000    # 400.0f

    mul-float/2addr v3, v2

    float-to-int v3, v3

    iput v3, p0, Landroid/support/v4/view/ViewPager;->O:I

    .line 117591
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->P:I

    .line 117592
    new-instance v1, LX/0vj;

    invoke-direct {v1, v0}, LX/0vj;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v4/view/ViewPager;->U:LX/0vj;

    .line 117593
    new-instance v1, LX/0vj;

    invoke-direct {v1, v0}, LX/0vj;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v4/view/ViewPager;->V:LX/0vj;

    .line 117594
    const/high16 v0, 0x41c80000    # 25.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->Q:I

    .line 117595
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->R:I

    .line 117596
    const/high16 v0, 0x41800000    # 16.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->F:I

    .line 117597
    new-instance v0, LX/0vm;

    invoke-direct {v0, p0}, LX/0vm;-><init>(Landroid/support/v4/view/ViewPager;)V

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 117598
    invoke-static {p0}, LX/0vv;->e(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    .line 117599
    invoke-static {p0, v4}, LX/0vv;->d(Landroid/view/View;I)V

    .line 117600
    :cond_0
    return-void
.end method

.method private getClientWidth()I
    .locals 2

    .prologue
    .line 117601
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 117602
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 117603
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 117604
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/18u;

    .line 117605
    iget-boolean v0, v0, LX/18u;->a:Z

    if-nez v0, :cond_0

    .line 117606
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->removeViewAt(I)V

    .line 117607
    add-int/lit8 v1, v1, -0x1

    .line 117608
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 117609
    :cond_1
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    .line 117610
    iget v0, p0, Landroid/support/v4/view/ViewPager;->aj:I

    if-eqz v0, :cond_2

    .line 117611
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ak:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 117612
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->ak:Ljava/util/ArrayList;

    .line 117613
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v1

    .line 117614
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 117615
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 117616
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->ak:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117617
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 117618
    :cond_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 117619
    :cond_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ak:Ljava/util/ArrayList;

    sget-object v1, Landroid/support/v4/view/ViewPager;->am:LX/0vc;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 117620
    :cond_2
    return-void
.end method

.method private j()LX/0vd;
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 117621
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v1

    .line 117622
    if-lez v1, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    int-to-float v4, v1

    div-float/2addr v0, v4

    move v9, v0

    .line 117623
    :goto_0
    if-lez v1, :cond_4

    iget v0, p0, Landroid/support/v4/view/ViewPager;->q:I

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    move v1, v0

    .line 117624
    :goto_1
    const/4 v5, -0x1

    .line 117625
    const/4 v4, 0x1

    .line 117626
    const/4 v0, 0x0

    move v6, v2

    move v7, v2

    move v8, v5

    move v2, v3

    move v5, v4

    move-object v4, v0

    .line 117627
    :goto_2
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 117628
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 117629
    if-nez v5, :cond_6

    iget v10, v0, LX/0vd;->b:I

    add-int/lit8 v11, v8, 0x1

    if-eq v10, v11, :cond_6

    .line 117630
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->h:LX/0vd;

    .line 117631
    add-float/2addr v6, v7

    add-float/2addr v6, v1

    iput v6, v0, LX/0vd;->e:F

    .line 117632
    add-int/lit8 v6, v8, 0x1

    iput v6, v0, LX/0vd;->b:I

    .line 117633
    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget v7, v0, LX/0vd;->b:I

    invoke-virtual {v6, v7}, LX/0gG;->d(I)F

    move-result v6

    iput v6, v0, LX/0vd;->d:F

    .line 117634
    add-int/lit8 v2, v2, -0x1

    move-object v12, v0

    move v0, v2

    move-object v2, v12

    .line 117635
    :goto_3
    iget v6, v2, LX/0vd;->e:F

    .line 117636
    iget v7, v2, LX/0vd;->d:F

    add-float/2addr v7, v6

    add-float/2addr v7, v1

    .line 117637
    if-nez v5, :cond_0

    cmpl-float v5, v9, v6

    if-ltz v5, :cond_2

    .line 117638
    :cond_0
    cmpg-float v4, v9, v7

    if-ltz v4, :cond_1

    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_5

    :cond_1
    move-object v4, v2

    .line 117639
    :cond_2
    return-object v4

    :cond_3
    move v9, v2

    .line 117640
    goto :goto_0

    :cond_4
    move v1, v2

    .line 117641
    goto :goto_1

    .line 117642
    :cond_5
    iget v5, v2, LX/0vd;->b:I

    .line 117643
    iget v4, v2, LX/0vd;->d:F

    .line 117644
    add-int/lit8 v0, v0, 0x1

    move v7, v6

    move v8, v5

    move v5, v3

    move v6, v4

    move-object v4, v2

    move v2, v0

    goto :goto_2

    :cond_6
    move-object v12, v0

    move v0, v2

    move-object v2, v12

    goto :goto_3
.end method

.method private k()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 117645
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->D:Z

    .line 117646
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->E:Z

    .line 117647
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 117648
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 117649
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    .line 117650
    :cond_0
    return-void
.end method

.method private l()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 117651
    iget v1, p0, Landroid/support/v4/view/ViewPager;->k:I

    if-lez v1, :cond_0

    .line 117652
    iget v1, p0, Landroid/support/v4/view/ViewPager;->k:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 117653
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 117558
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    if-eqz v1, :cond_0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->k:I

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v2}, LX/0gG;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 117559
    iget v1, p0, Landroid/support/v4/view/ViewPager;->k:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 117560
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setScrollState(Landroid/support/v4/view/ViewPager;I)V
    .locals 1

    .prologue
    .line 117654
    iget v0, p0, Landroid/support/v4/view/ViewPager;->ao:I

    if-ne v0, p1, :cond_1

    .line 117655
    :cond_0
    :goto_0
    return-void

    .line 117656
    :cond_1
    iput p1, p0, Landroid/support/v4/view/ViewPager;->ao:I

    .line 117657
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ah:LX/3sI;

    if-eqz v0, :cond_2

    .line 117658
    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->b(Z)V

    .line 117659
    :cond_2
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    if-eqz v0, :cond_0

    .line 117660
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    goto :goto_0

    .line 117661
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private setScrollingCacheEnabled(Z)V
    .locals 1

    .prologue
    .line 117555
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->z:Z

    if-eq v0, p1, :cond_0

    .line 117556
    iput-boolean p1, p0, Landroid/support/v4/view/ViewPager;->z:Z

    .line 117557
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0hc;)LX/0hc;
    .locals 1

    .prologue
    .line 117552
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->af:LX/0hc;

    .line 117553
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->af:LX/0hc;

    .line 117554
    return-object v0
.end method

.method public final a()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 117516
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v8

    .line 117517
    iput v8, p0, Landroid/support/v4/view/ViewPager;->b:I

    .line 117518
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v3, p0, Landroid/support/v4/view/ViewPager;->B:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1

    if-ge v0, v3, :cond_1

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v8, :cond_1

    move v0, v1

    .line 117519
    :goto_0
    iget v3, p0, Landroid/support/v4/view/ViewPager;->k:I

    move v4, v2

    move v5, v3

    move v6, v0

    move v3, v2

    .line 117520
    :goto_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 117521
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 117522
    iget-object v7, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget-object v9, v0, LX/0vd;->a:Ljava/lang/Object;

    invoke-virtual {v7, v9}, LX/0gG;->a(Ljava/lang/Object;)I

    move-result v7

    .line 117523
    const/4 v9, -0x1

    if-eq v7, v9, :cond_9

    .line 117524
    const/4 v9, -0x2

    if-ne v7, v9, :cond_2

    .line 117525
    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 117526
    add-int/lit8 v3, v3, -0x1

    .line 117527
    if-nez v4, :cond_0

    .line 117528
    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v4, p0}, LX/0gG;->a(Landroid/view/ViewGroup;)V

    move v4, v1

    .line 117529
    :cond_0
    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget v7, v0, LX/0vd;->b:I

    iget-object v9, v0, LX/0vd;->a:Ljava/lang/Object;

    invoke-virtual {v6, p0, v7, v9}, LX/0gG;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 117530
    iget v6, p0, Landroid/support/v4/view/ViewPager;->k:I

    iget v0, v0, LX/0vd;->b:I

    if-ne v6, v0, :cond_a

    .line 117531
    iget v0, p0, Landroid/support/v4/view/ViewPager;->k:I

    add-int/lit8 v5, v8, -0x1

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v1

    .line 117532
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 117533
    goto :goto_0

    .line 117534
    :cond_2
    iget v9, v0, LX/0vd;->b:I

    if-eq v9, v7, :cond_9

    .line 117535
    iget v6, v0, LX/0vd;->b:I

    iget v9, p0, Landroid/support/v4/view/ViewPager;->k:I

    if-ne v6, v9, :cond_3

    move v5, v7

    .line 117536
    :cond_3
    iput v7, v0, LX/0vd;->b:I

    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v1

    .line 117537
    goto :goto_2

    .line 117538
    :cond_4
    if-eqz v4, :cond_5

    .line 117539
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v0, p0}, LX/0gG;->b(Landroid/view/ViewGroup;)V

    .line 117540
    :cond_5
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    sget-object v3, Landroid/support/v4/view/ViewPager;->e:Ljava/util/Comparator;

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 117541
    if-eqz v6, :cond_8

    .line 117542
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v4

    move v3, v2

    .line 117543
    :goto_3
    if-ge v3, v4, :cond_7

    .line 117544
    invoke-virtual {p0, v3}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 117545
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/18u;

    .line 117546
    iget-boolean v6, v0, LX/18u;->a:Z

    if-nez v6, :cond_6

    .line 117547
    const/4 v6, 0x0

    iput v6, v0, LX/18u;->c:F

    .line 117548
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 117549
    :cond_7
    invoke-direct {p0, v5, v2, v1}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 117550
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    .line 117551
    :cond_8
    return-void

    :cond_9
    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v6

    goto :goto_2

    :cond_a
    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v1

    goto :goto_2
.end method

.method public a(F)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 117491
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->S:Z

    if-nez v0, :cond_0

    .line 117492
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No fake drag in progress. Call beginFakeDrag first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117493
    :cond_0
    iget v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    add-float/2addr v0, p1

    iput v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 117494
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    .line 117495
    sub-float v3, v0, p1

    .line 117496
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v5

    .line 117497
    int-to-float v0, v5

    iget v1, p0, Landroid/support/v4/view/ViewPager;->u:F

    mul-float v2, v0, v1

    .line 117498
    int-to-float v0, v5

    iget v1, p0, Landroid/support/v4/view/ViewPager;->v:F

    mul-float v4, v0, v1

    .line 117499
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 117500
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0vd;

    .line 117501
    iget v6, v0, LX/0vd;->b:I

    if-eqz v6, :cond_4

    .line 117502
    iget v0, v0, LX/0vd;->e:F

    int-to-float v2, v5

    mul-float/2addr v0, v2

    .line 117503
    :goto_0
    iget v2, v1, LX/0vd;->b:I

    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v6}, LX/0gG;->b()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-eq v2, v6, :cond_3

    .line 117504
    iget v1, v1, LX/0vd;->e:F

    int-to-float v2, v5

    mul-float/2addr v1, v2

    .line 117505
    :goto_1
    cmpg-float v2, v3, v0

    if-gez v2, :cond_1

    .line 117506
    :goto_2
    iget v1, p0, Landroid/support/v4/view/ViewPager;->I:F

    float-to-int v2, v0

    int-to-float v2, v2

    sub-float v2, v0, v2

    add-float/2addr v1, v2

    iput v1, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 117507
    float-to-int v1, v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 117508
    float-to-int v0, v0

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->d(I)Z

    .line 117509
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 117510
    iget-wide v0, p0, Landroid/support/v4/view/ViewPager;->T:J

    const/4 v4, 0x2

    iget v5, p0, Landroid/support/v4/view/ViewPager;->I:F

    const/4 v6, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 117511
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    invoke-virtual {v1, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 117512
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 117513
    return-void

    .line 117514
    :cond_1
    cmpl-float v0, v3, v1

    if-lez v0, :cond_2

    move v0, v1

    .line 117515
    goto :goto_2

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    move v1, v4

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public a(IFI)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 117450
    iget v0, p0, Landroid/support/v4/view/ViewPager;->ac:I

    if-lez v0, :cond_1

    .line 117451
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v5

    .line 117452
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    .line 117453
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v2

    .line 117454
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v6

    .line 117455
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v7

    move v4, v3

    .line 117456
    :goto_0
    if-ge v4, v7, :cond_1

    .line 117457
    invoke-virtual {p0, v4}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 117458
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/18u;

    .line 117459
    iget-boolean v9, v0, LX/18u;->a:Z

    if-eqz v9, :cond_6

    .line 117460
    iget v0, v0, LX/18u;->b:I

    and-int/lit8 v0, v0, 0x7

    .line 117461
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    move v10, v2

    move v2, v1

    move v1, v10

    .line 117462
    :goto_1
    add-int/2addr v0, v5

    .line 117463
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v9

    sub-int/2addr v0, v9

    .line 117464
    if-eqz v0, :cond_0

    .line 117465
    invoke-virtual {v8, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 117466
    :cond_0
    :goto_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v10, v1

    move v1, v2

    move v2, v10

    goto :goto_0

    .line 117467
    :pswitch_1
    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    move v10, v1

    move v1, v2

    move v2, v0

    move v0, v10

    .line 117468
    goto :goto_1

    .line 117469
    :pswitch_2
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v6, v0

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v10, v2

    move v2, v1

    move v1, v10

    .line 117470
    goto :goto_1

    .line 117471
    :pswitch_3
    sub-int v0, v6, v2

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    sub-int/2addr v0, v9

    .line 117472
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v2, v9

    move v10, v2

    move v2, v1

    move v1, v10

    goto :goto_1

    .line 117473
    :cond_1
    iput p1, p0, Landroid/support/v4/view/ViewPager;->c:I

    .line 117474
    iput p2, p0, Landroid/support/v4/view/ViewPager;->d:F

    .line 117475
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    if-eqz v0, :cond_2

    .line 117476
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    invoke-interface {v0, p1, p2, p3}, LX/0hc;->a(IFI)V

    .line 117477
    :cond_2
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->af:LX/0hc;

    if-eqz v0, :cond_3

    .line 117478
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->af:LX/0hc;

    invoke-interface {v0, p1, p2, p3}, LX/0hc;->a(IFI)V

    .line 117479
    :cond_3
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ah:LX/3sI;

    if-eqz v0, :cond_5

    .line 117480
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v2

    .line 117481
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v4

    move v1, v3

    .line 117482
    :goto_3
    if-ge v1, v4, :cond_5

    .line 117483
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 117484
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/18u;

    .line 117485
    iget-boolean v0, v0, LX/18u;->a:Z

    if-nez v0, :cond_4

    .line 117486
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int/2addr v0, v2

    int-to-float v0, v0

    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v0, v5

    .line 117487
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->ah:LX/3sI;

    invoke-interface {v5, v3, v0}, LX/3sI;->a(Landroid/view/View;F)V

    .line 117488
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 117489
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ab:Z

    .line 117490
    return-void

    :cond_6
    move v10, v2

    move v2, v1

    move v1, v10

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 117447
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->A:Z

    .line 117448
    invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 117449
    return-void
.end method

.method public final a(ZLX/3sI;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 117435
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_1

    .line 117436
    if-eqz p2, :cond_2

    move v0, v1

    .line 117437
    :goto_0
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->ah:LX/3sI;

    if-eqz v3, :cond_3

    move v3, v1

    :goto_1
    if-eq v0, v3, :cond_4

    move v3, v1

    .line 117438
    :goto_2
    iput-object p2, p0, Landroid/support/v4/view/ViewPager;->ah:LX/3sI;

    .line 117439
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setChildrenDrawingOrderEnabledCompat(Z)V

    .line 117440
    if-eqz v0, :cond_5

    .line 117441
    if-eqz p1, :cond_0

    const/4 v1, 0x2

    :cond_0
    iput v1, p0, Landroid/support/v4/view/ViewPager;->aj:I

    .line 117442
    :goto_3
    if-eqz v3, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 117443
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 117444
    goto :goto_0

    :cond_3
    move v3, v2

    .line 117445
    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2

    .line 117446
    :cond_5
    iput v2, p0, Landroid/support/v4/view/ViewPager;->aj:I

    goto :goto_3
.end method

.method public a(FF)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 117434
    iget v0, p0, Landroid/support/v4/view/ViewPager;->G:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    cmpl-float v0, p2, v2

    if-gtz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->G:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    cmpg-float v0, p2, v2

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x42

    const/16 v7, 0x11

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 117399
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 117400
    if-ne v2, p0, :cond_1

    move-object v0, v1

    .line 117401
    :goto_0
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 117402
    if-eqz v1, :cond_7

    if-eq v1, v0, :cond_7

    .line 117403
    if-ne p1, v7, :cond_5

    .line 117404
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->i:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->left:I

    .line 117405
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->i:Landroid/graphics/Rect;

    invoke-direct {p0, v3, v0}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    .line 117406
    if-eqz v0, :cond_4

    if-lt v2, v3, :cond_4

    .line 117407
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->l()Z

    move-result v0

    .line 117408
    :goto_1
    if-eqz v0, :cond_0

    .line 117409
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->playSoundEffect(I)V

    .line 117410
    :cond_0
    return v0

    .line 117411
    :cond_1
    if-eqz v2, :cond_c

    .line 117412
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_2
    instance-of v5, v0, Landroid/view/ViewGroup;

    if-eqz v5, :cond_d

    .line 117413
    if-ne v0, p0, :cond_2

    move v0, v4

    .line 117414
    :goto_3
    if-nez v0, :cond_c

    .line 117415
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 117416
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117417
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_4
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    .line 117418
    const-string v2, " => "

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117419
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_4

    .line 117420
    :cond_2
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_2

    .line 117421
    :cond_3
    const-string v0, "ViewPager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "arrowScroll tried to find focus based on non-child current focused view "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 117422
    goto/16 :goto_0

    .line 117423
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto :goto_1

    .line 117424
    :cond_5
    if-ne p1, v8, :cond_b

    .line 117425
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->i:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->left:I

    .line 117426
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->i:Landroid/graphics/Rect;

    invoke-direct {p0, v3, v0}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    .line 117427
    if-eqz v0, :cond_6

    if-gt v2, v3, :cond_6

    .line 117428
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->m()Z

    move-result v0

    goto/16 :goto_1

    .line 117429
    :cond_6
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto/16 :goto_1

    .line 117430
    :cond_7
    if-eq p1, v7, :cond_8

    if-ne p1, v4, :cond_9

    .line 117431
    :cond_8
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->l()Z

    move-result v0

    goto/16 :goto_1

    .line 117432
    :cond_9
    if-eq p1, v8, :cond_a

    const/4 v0, 0x2

    if-ne p1, v0, :cond_b

    .line 117433
    :cond_a
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->m()Z

    move-result v0

    goto/16 :goto_1

    :cond_b
    move v0, v3

    goto/16 :goto_1

    :cond_c
    move-object v0, v2

    goto/16 :goto_0

    :cond_d
    move v0, v3

    goto/16 :goto_3
.end method

.method public a(Landroid/view/View;ZIII)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 117388
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v6, p1

    .line 117389
    check-cast v6, Landroid/view/ViewGroup;

    .line 117390
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v8

    .line 117391
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v9

    .line 117392
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 117393
    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_0
    if-ltz v7, :cond_2

    .line 117394
    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 117395
    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt v0, v3, :cond_1

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    if-ge v0, v3, :cond_1

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt v0, v3, :cond_1

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge v0, v3, :cond_1

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v4, v0, v3

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v5, v0, v3

    move-object v0, p0

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117396
    :cond_0
    :goto_1
    return v2

    .line 117397
    :cond_1
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    goto :goto_0

    .line 117398
    :cond_2
    if-eqz p2, :cond_3

    neg-int v0, p3

    invoke-static {p1, v0}, LX/0vv;->a(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final addFocusables(Ljava/util/ArrayList;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 117208
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 117209
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getDescendantFocusability()I

    move-result v2

    .line 117210
    const/high16 v0, 0x60000

    if-eq v2, v0, :cond_1

    .line 117211
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 117212
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 117213
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 117214
    invoke-direct {p0, v3}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)LX/0vd;

    move-result-object v4

    .line 117215
    if-eqz v4, :cond_0

    iget v4, v4, LX/0vd;->b:I

    iget v5, p0, Landroid/support/v4/view/ViewPager;->k:I

    if-ne v4, v5, :cond_0

    .line 117216
    invoke-virtual {v3, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 117217
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117218
    :cond_1
    const/high16 v0, 0x40000

    if-ne v2, v0, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v1, v0, :cond_3

    .line 117219
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->isFocusable()Z

    move-result v0

    if-nez v0, :cond_4

    .line 117220
    :cond_3
    :goto_1
    return-void

    .line 117221
    :cond_4
    and-int/lit8 v0, p3, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->isFocusableInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 117222
    :cond_5
    if-eqz p1, :cond_3

    .line 117223
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final addTouchables(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117354
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 117355
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 117356
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 117357
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)LX/0vd;

    move-result-object v2

    .line 117358
    if-eqz v2, :cond_0

    iget v2, v2, LX/0vd;->b:I

    iget v3, p0, Landroid/support/v4/view/ViewPager;->k:I

    if-ne v2, v3, :cond_0

    .line 117359
    invoke-virtual {v1, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 117360
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117361
    :cond_1
    return-void
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 4

    .prologue
    .line 116515
    invoke-virtual {p0, p3}, Landroid/support/v4/view/ViewPager;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 116516
    invoke-virtual {p0, p3}, Landroid/support/v4/view/ViewPager;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 116517
    check-cast v0, LX/18u;

    .line 116518
    iget-boolean v2, v0, LX/18u;->a:Z

    instance-of v3, p1, LX/18z;

    or-int/2addr v2, v3

    iput-boolean v2, v0, LX/18u;->a:Z

    .line 116519
    iget-boolean v2, p0, Landroid/support/v4/view/ViewPager;->y:Z

    if-eqz v2, :cond_1

    .line 116520
    if-eqz v0, :cond_0

    iget-boolean v2, v0, LX/18u;->a:Z

    if-eqz v2, :cond_0

    .line 116521
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add pager decor view during layout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116522
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/18u;->d:Z

    .line 116523
    invoke-virtual {p0, p1, p2, v1}, Landroid/support/v4/view/ViewPager;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 116524
    :goto_1
    return-void

    .line 116525
    :cond_1
    invoke-super {p0, p1, p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_2
    move-object v1, p3

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 116513
    iget v0, p0, Landroid/support/v4/view/ViewPager;->k:I

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->b(I)V

    .line 116514
    return-void
.end method

.method public final c()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 116499
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->D:Z

    if-eqz v0, :cond_0

    .line 116500
    :goto_0
    return v4

    .line 116501
    :cond_0
    iput-boolean v8, p0, Landroid/support/v4/view/ViewPager;->S:Z

    .line 116502
    invoke-static {p0, v8}, Landroid/support/v4/view/ViewPager;->setScrollState(Landroid/support/v4/view/ViewPager;I)V

    .line 116503
    iput v5, p0, Landroid/support/v4/view/ViewPager;->I:F

    iput v5, p0, Landroid/support/v4/view/ViewPager;->K:F

    .line 116504
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    if-nez v0, :cond_1

    .line 116505
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    .line 116506
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    move-wide v2, v0

    move v6, v5

    move v7, v4

    .line 116507
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v2

    .line 116508
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    invoke-virtual {v3, v2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 116509
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 116510
    iput-wide v0, p0, Landroid/support/v4/view/ViewPager;->T:J

    move v4, v8

    .line 116511
    goto :goto_0

    .line 116512
    :cond_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_1
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 116498
    instance-of v0, p1, LX/18u;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final computeScroll()V
    .locals 4

    .prologue
    .line 116485
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 116486
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v0

    .line 116487
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v1

    .line 116488
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    .line 116489
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrY()I

    move-result v3

    .line 116490
    if-ne v0, v2, :cond_0

    if-eq v1, v3, :cond_1

    .line 116491
    :cond_0
    invoke-virtual {p0, v2, v3}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 116492
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->d(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 116493
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 116494
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v3}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 116495
    :cond_1
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 116496
    :goto_0
    return-void

    .line 116497
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->a(Z)V

    goto :goto_0
.end method

.method public final d()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 116468
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->S:Z

    if-nez v0, :cond_0

    .line 116469
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No fake drag in progress. Call beginFakeDrag first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116470
    :cond_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    .line 116471
    const/16 v1, 0x3e8

    iget v2, p0, Landroid/support/v4/view/ViewPager;->P:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 116472
    iget v1, p0, Landroid/support/v4/view/ViewPager;->M:I

    invoke-static {v0, v1}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v0, v0

    .line 116473
    iput-boolean v5, p0, Landroid/support/v4/view/ViewPager;->A:Z

    .line 116474
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v1

    .line 116475
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v2

    .line 116476
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->j()LX/0vd;

    move-result-object v3

    .line 116477
    iget v4, v3, LX/0vd;->b:I

    .line 116478
    int-to-float v2, v2

    int-to-float v1, v1

    div-float v1, v2, v1

    iget v2, v3, LX/0vd;->e:F

    sub-float/2addr v1, v2

    iget v2, v3, LX/0vd;->d:F

    div-float/2addr v1, v2

    .line 116479
    iget v2, p0, Landroid/support/v4/view/ViewPager;->I:F

    iget v3, p0, Landroid/support/v4/view/ViewPager;->K:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 116480
    invoke-direct {p0, v4, v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IFII)I

    move-result v1

    .line 116481
    invoke-direct {p0, v1, v5, v5, v0}, Landroid/support/v4/view/ViewPager;->a(IZZI)V

    .line 116482
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->k()V

    .line 116483
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->S:Z

    .line 116484
    return-void
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 116467
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 116456
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_1

    .line 116457
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 116458
    :cond_0
    :goto_0
    return v0

    .line 116459
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    move v1, v0

    .line 116460
    :goto_1
    if-ge v1, v2, :cond_0

    .line 116461
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 116462
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    .line 116463
    invoke-direct {p0, v3}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)LX/0vd;

    move-result-object v4

    .line 116464
    if-eqz v4, :cond_2

    iget v4, v4, LX/0vd;->b:I

    iget v5, p0, Landroid/support/v4/view/ViewPager;->k:I

    if-ne v4, v5, :cond_2

    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 116465
    const/4 v0, 0x1

    goto :goto_0

    .line 116466
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 116429
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 116430
    const/4 v0, 0x0

    .line 116431
    invoke-static {p0}, LX/0vv;->a(Landroid/view/View;)I

    move-result v1

    .line 116432
    if-eqz v1, :cond_0

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-le v1, v2, :cond_4

    .line 116433
    :cond_0
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->U:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 116434
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 116435
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    .line 116436
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v2

    .line 116437
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    .line 116438
    neg-int v3, v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Landroid/support/v4/view/ViewPager;->u:F

    int-to-float v5, v2

    mul-float/2addr v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 116439
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->U:LX/0vj;

    invoke-virtual {v3, v0, v2}, LX/0vj;->a(II)V

    .line 116440
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->U:LX/0vj;

    invoke-virtual {v0, p1}, LX/0vj;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 116441
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 116442
    :cond_1
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->V:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 116443
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 116444
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v2

    .line 116445
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    .line 116446
    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 116447
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    iget v5, p0, Landroid/support/v4/view/ViewPager;->v:F

    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v5, v6

    neg-float v5, v5

    int-to-float v6, v2

    mul-float/2addr v5, v6

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 116448
    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->V:LX/0vj;

    invoke-virtual {v4, v3, v2}, LX/0vj;->a(II)V

    .line 116449
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->V:LX/0vj;

    invoke-virtual {v2, p1}, LX/0vj;->a(Landroid/graphics/Canvas;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 116450
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 116451
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    .line 116452
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 116453
    :cond_3
    return-void

    .line 116454
    :cond_4
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->U:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->b()V

    .line 116455
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->V:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->b()V

    goto :goto_0
.end method

.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 116424
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 116425
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->r:Landroid/graphics/drawable/Drawable;

    .line 116426
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116427
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 116428
    :cond_0
    return-void
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 116423
    new-instance v0, LX/18u;

    invoke-direct {v0}, LX/18u;-><init>()V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 116422
    new-instance v0, LX/18u;

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/18u;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 116421
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()LX/0gG;
    .locals 1

    .prologue
    .line 116420
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    return-object v0
.end method

.method public final getChildDrawingOrder(II)I
    .locals 2

    .prologue
    .line 116417
    iget v0, p0, Landroid/support/v4/view/ViewPager;->aj:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, p1, -0x1

    sub-int p2, v0, p2

    .line 116418
    :cond_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/18u;

    iget v0, v0, LX/18u;->f:I

    .line 116419
    return v0
.end method

.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 116416
    iget v0, p0, Landroid/support/v4/view/ViewPager;->k:I

    return v0
.end method

.method public getOffscreenPageLimit()I
    .locals 1

    .prologue
    .line 116415
    iget v0, p0, Landroid/support/v4/view/ViewPager;->B:I

    return v0
.end method

.method public getOnlyCreatePagesImmediatelyOffscreen()Z
    .locals 1

    .prologue
    .line 116414
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->C:Z

    return v0
.end method

.method public getPageMargin()I
    .locals 1

    .prologue
    .line 116413
    iget v0, p0, Landroid/support/v4/view/ViewPager;->q:I

    return v0
.end method

.method public getScrollOffset()F
    .locals 1

    .prologue
    .line 116412
    iget v0, p0, Landroid/support/v4/view/ViewPager;->d:F

    return v0
.end method

.method public getScrollPosition()I
    .locals 1

    .prologue
    .line 116407
    iget v0, p0, Landroid/support/v4/view/ViewPager;->c:I

    return v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x2c

    const v1, -0x407eeebc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 116408
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 116409
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->al:Z

    .line 116410
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->W:Z

    .line 116411
    const/16 v1, 0x2d

    const v2, 0x3c9eda98

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x30909751

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 116627
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->an:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 116628
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->al:Z

    .line 116629
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 116630
    const/16 v1, 0x2d

    const v2, -0x44e45e80

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 16

    .prologue
    .line 116894
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 116895
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/view/ViewPager;->q:I

    if-lez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    if-eqz v1, :cond_3

    .line 116896
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v6

    .line 116897
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v7

    .line 116898
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/view/ViewPager;->q:I

    int-to-float v1, v1

    int-to-float v2, v7

    div-float v8, v1, v2

    .line 116899
    const/4 v5, 0x0

    .line 116900
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0vd;

    .line 116901
    iget v4, v1, LX/0vd;->e:F

    .line 116902
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 116903
    iget v3, v1, LX/0vd;->b:I

    .line 116904
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    add-int/lit8 v10, v9, -0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0vd;

    iget v10, v2, LX/0vd;->b:I

    move v2, v5

    move v5, v3

    .line 116905
    :goto_0
    if-ge v5, v10, :cond_3

    .line 116906
    :goto_1
    iget v3, v1, LX/0vd;->b:I

    if-le v5, v3, :cond_0

    if-ge v2, v9, :cond_0

    .line 116907
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0vd;

    goto :goto_1

    .line 116908
    :cond_0
    iget v3, v1, LX/0vd;->b:I

    if-ne v5, v3, :cond_2

    .line 116909
    iget v3, v1, LX/0vd;->e:F

    iget v4, v1, LX/0vd;->d:F

    add-float/2addr v3, v4

    int-to-float v4, v7

    mul-float/2addr v3, v4

    .line 116910
    iget v4, v1, LX/0vd;->e:F

    iget v11, v1, LX/0vd;->d:F

    add-float/2addr v4, v11

    add-float/2addr v4, v8

    .line 116911
    :goto_2
    move-object/from16 v0, p0

    iget v11, v0, Landroid/support/v4/view/ViewPager;->q:I

    int-to-float v11, v11

    add-float/2addr v11, v3

    int-to-float v12, v6

    cmpl-float v11, v11, v12

    if-lez v11, :cond_1

    .line 116912
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/view/ViewPager;->r:Landroid/graphics/drawable/Drawable;

    float-to-int v12, v3

    move-object/from16 v0, p0

    iget v13, v0, Landroid/support/v4/view/ViewPager;->s:I

    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/v4/view/ViewPager;->q:I

    int-to-float v14, v14

    add-float/2addr v14, v3

    const/high16 v15, 0x3f000000    # 0.5f

    add-float/2addr v14, v15

    float-to-int v14, v14

    move-object/from16 v0, p0

    iget v15, v0, Landroid/support/v4/view/ViewPager;->t:I

    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 116913
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/view/ViewPager;->r:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 116914
    :cond_1
    add-int v11, v6, v7

    int-to-float v11, v11

    cmpl-float v3, v3, v11

    if-gtz v3, :cond_3

    .line 116915
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 116916
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v3, v5}, LX/0gG;->d(I)F

    move-result v11

    .line 116917
    add-float v3, v4, v11

    int-to-float v12, v7

    mul-float/2addr v3, v12

    .line 116918
    add-float/2addr v11, v8

    add-float/2addr v4, v11

    goto :goto_2

    .line 116919
    :cond_3
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v3, -0x1

    const/4 v12, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 116836
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 116837
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    if-ne v0, v6, :cond_2

    .line 116838
    :cond_0
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->D:Z

    .line 116839
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->E:Z

    .line 116840
    iput v3, p0, Landroid/support/v4/view/ViewPager;->M:I

    .line 116841
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 116842
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 116843
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    .line 116844
    :cond_1
    :goto_0
    return v2

    .line 116845
    :cond_2
    if-eqz v0, :cond_4

    .line 116846
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->D:Z

    if-eqz v1, :cond_3

    move v2, v6

    .line 116847
    goto :goto_0

    .line 116848
    :cond_3
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->E:Z

    if-nez v1, :cond_1

    .line 116849
    :cond_4
    sparse-switch v0, :sswitch_data_0

    .line 116850
    :cond_5
    :goto_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    if-nez v0, :cond_6

    .line 116851
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    .line 116852
    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 116853
    iget-boolean v2, p0, Landroid/support/v4/view/ViewPager;->D:Z

    goto :goto_0

    .line 116854
    :sswitch_0
    iget v0, p0, Landroid/support/v4/view/ViewPager;->M:I

    .line 116855
    if-eq v0, v3, :cond_5

    .line 116856
    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 116857
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v7

    .line 116858
    iget v1, p0, Landroid/support/v4/view/ViewPager;->I:F

    sub-float v8, v7, v1

    .line 116859
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 116860
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v10

    .line 116861
    iget v0, p0, Landroid/support/v4/view/ViewPager;->L:F

    sub-float v0, v10, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v11

    .line 116862
    cmpl-float v0, v8, v12

    if-eqz v0, :cond_7

    iget v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    invoke-virtual {p0, v0, v8}, Landroid/support/v4/view/ViewPager;->a(FF)Z

    move-result v0

    if-nez v0, :cond_7

    float-to-int v3, v8

    float-to-int v4, v7

    float-to-int v5, v10

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 116863
    iput v7, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 116864
    iput v10, p0, Landroid/support/v4/view/ViewPager;->J:F

    .line 116865
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->E:Z

    goto :goto_0

    .line 116866
    :cond_7
    iget v0, p0, Landroid/support/v4/view/ViewPager;->H:I

    int-to-float v0, v0

    cmpl-float v0, v9, v0

    if-lez v0, :cond_a

    const/high16 v0, 0x3f000000    # 0.5f

    mul-float/2addr v0, v9

    cmpl-float v0, v0, v11

    if-lez v0, :cond_a

    .line 116867
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->D:Z

    .line 116868
    invoke-direct {p0, v6}, Landroid/support/v4/view/ViewPager;->c(Z)V

    .line 116869
    invoke-static {p0, v6}, Landroid/support/v4/view/ViewPager;->setScrollState(Landroid/support/v4/view/ViewPager;I)V

    .line 116870
    cmpl-float v0, v8, v12

    if-lez v0, :cond_9

    iget v0, p0, Landroid/support/v4/view/ViewPager;->K:F

    iget v1, p0, Landroid/support/v4/view/ViewPager;->H:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    :goto_2
    iput v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 116871
    iput v10, p0, Landroid/support/v4/view/ViewPager;->J:F

    .line 116872
    invoke-direct {p0, v6}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 116873
    :cond_8
    :goto_3
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->D:Z

    if-eqz v0, :cond_5

    .line 116874
    invoke-direct {p0, v7}, Landroid/support/v4/view/ViewPager;->c(F)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 116875
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    goto/16 :goto_1

    .line 116876
    :cond_9
    iget v0, p0, Landroid/support/v4/view/ViewPager;->K:F

    iget v1, p0, Landroid/support/v4/view/ViewPager;->H:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    goto :goto_2

    .line 116877
    :cond_a
    iget v0, p0, Landroid/support/v4/view/ViewPager;->H:I

    int-to-float v0, v0

    cmpl-float v0, v11, v0

    if-lez v0, :cond_8

    .line 116878
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->E:Z

    goto :goto_3

    .line 116879
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->K:F

    iput v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 116880
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->L:F

    iput v0, p0, Landroid/support/v4/view/ViewPager;->J:F

    .line 116881
    invoke-static {p1, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->M:I

    .line 116882
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->E:Z

    .line 116883
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 116884
    iget v0, p0, Landroid/support/v4/view/ViewPager;->ao:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalX()I

    move-result v0

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->R:I

    if-le v0, v1, :cond_b

    .line 116885
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 116886
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->A:Z

    .line 116887
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 116888
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ad:Z

    if-eqz v0, :cond_5

    .line 116889
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->D:Z

    .line 116890
    invoke-static {p0, v6}, Landroid/support/v4/view/ViewPager;->setScrollState(Landroid/support/v4/view/ViewPager;I)V

    goto/16 :goto_1

    .line 116891
    :cond_b
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->a(Z)V

    .line 116892
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->D:Z

    goto/16 :goto_1

    .line 116893
    :sswitch_2
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_0
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method public final onLayout(ZIIII)V
    .locals 17

    .prologue
    .line 116535
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v9

    .line 116536
    sub-int v10, p4, p2

    .line 116537
    sub-int v11, p5, p3

    .line 116538
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v6

    .line 116539
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v2

    .line 116540
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v5

    .line 116541
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v3

    .line 116542
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v12

    .line 116543
    const/4 v4, 0x0

    .line 116544
    const/4 v1, 0x0

    move v8, v1

    :goto_0
    if-ge v8, v9, :cond_0

    .line 116545
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 116546
    invoke-virtual {v13}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v7, 0x8

    if-eq v1, v7, :cond_5

    .line 116547
    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/18u;

    .line 116548
    iget-boolean v7, v1, LX/18u;->a:Z

    if-eqz v7, :cond_5

    .line 116549
    iget v7, v1, LX/18u;->b:I

    and-int/lit8 v7, v7, 0x7

    .line 116550
    iget v1, v1, LX/18u;->b:I

    and-int/lit8 v14, v1, 0x70

    .line 116551
    packed-switch v7, :pswitch_data_0

    :pswitch_0
    move v7, v6

    .line 116552
    :goto_1
    sparse-switch v14, :sswitch_data_0

    move v1, v2

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 116553
    :goto_2
    add-int/2addr v7, v12

    .line 116554
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    add-int/2addr v14, v7

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v15, v1

    invoke-virtual {v13, v7, v1, v14, v15}, Landroid/view/View;->layout(IIII)V

    .line 116555
    add-int/lit8 v1, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v5

    move v5, v6

    .line 116556
    :goto_3
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    move v6, v5

    move v5, v2

    move v2, v4

    move v4, v1

    goto :goto_0

    .line 116557
    :pswitch_1
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v6

    move v7, v6

    move v6, v1

    .line 116558
    goto :goto_1

    .line 116559
    :pswitch_2
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v10, v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v7, v1

    .line 116560
    goto :goto_1

    .line 116561
    :pswitch_3
    sub-int v1, v10, v5

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v1, v7

    .line 116562
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v5, v7

    move v7, v1

    goto :goto_1

    .line 116563
    :sswitch_0
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v2

    move/from16 v16, v2

    move v2, v3

    move v3, v1

    move/from16 v1, v16

    .line 116564
    goto :goto_2

    .line 116565
    :sswitch_1
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v11, v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 116566
    goto :goto_2

    .line 116567
    :sswitch_2
    sub-int v1, v11, v3

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    sub-int/2addr v1, v14

    .line 116568
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    add-int/2addr v3, v14

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    goto :goto_2

    .line 116569
    :cond_0
    sub-int v1, v10, v6

    sub-int v7, v1, v5

    .line 116570
    const/4 v1, 0x0

    move v5, v1

    :goto_4
    if-ge v5, v9, :cond_3

    .line 116571
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 116572
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v10, 0x8

    if-eq v1, v10, :cond_2

    .line 116573
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/18u;

    .line 116574
    iget-boolean v10, v1, LX/18u;->a:Z

    if-nez v10, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)LX/0vd;

    move-result-object v10

    if-eqz v10, :cond_2

    .line 116575
    int-to-float v12, v7

    iget v10, v10, LX/0vd;->e:F

    mul-float/2addr v10, v12

    float-to-int v10, v10

    .line 116576
    add-int/2addr v10, v6

    .line 116577
    iget-boolean v12, v1, LX/18u;->d:Z

    if-eqz v12, :cond_1

    .line 116578
    const/4 v12, 0x0

    iput-boolean v12, v1, LX/18u;->d:Z

    .line 116579
    int-to-float v12, v7

    iget v1, v1, LX/18u;->c:F

    mul-float/2addr v1, v12

    float-to-int v1, v1

    const/high16 v12, 0x40000000    # 2.0f

    invoke-static {v1, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 116580
    sub-int v12, v11, v2

    sub-int/2addr v12, v3

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    .line 116581
    invoke-virtual {v8, v1, v12}, Landroid/view/View;->measure(II)V

    .line 116582
    :cond_1
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v10

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v2

    invoke-virtual {v8, v10, v2, v1, v12}, Landroid/view/View;->layout(IIII)V

    .line 116583
    :cond_2
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_4

    .line 116584
    :cond_3
    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v4/view/ViewPager;->s:I

    .line 116585
    sub-int v1, v11, v3

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/view/ViewPager;->t:I

    .line 116586
    move-object/from16 v0, p0

    iput v4, v0, Landroid/support/v4/view/ViewPager;->ac:I

    .line 116587
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v4/view/ViewPager;->W:Z

    if-eqz v1, :cond_4

    .line 116588
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/view/ViewPager;->k:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/support/v4/view/ViewPager;->a(IZIZ)V

    .line 116589
    :cond_4
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v4/view/ViewPager;->W:Z

    .line 116590
    return-void

    :cond_5
    move v1, v4

    move v4, v2

    move v2, v5

    move v5, v6

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x30 -> :sswitch_0
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method public onMeasure(II)V
    .locals 13

    .prologue
    .line 116781
    const/4 v0, 0x0

    invoke-static {v0, p1}, Landroid/support/v4/view/ViewPager;->getDefaultSize(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, p2}, Landroid/support/v4/view/ViewPager;->getDefaultSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->setMeasuredDimension(II)V

    .line 116782
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getMeasuredWidth()I

    move-result v0

    .line 116783
    div-int/lit8 v1, v0, 0xa

    .line 116784
    iget v2, p0, Landroid/support/v4/view/ViewPager;->F:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->G:I

    .line 116785
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v1

    sub-int v3, v0, v1

    .line 116786
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v1

    sub-int v5, v0, v1

    .line 116787
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v9

    .line 116788
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v9, :cond_8

    .line 116789
    invoke-virtual {p0, v8}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 116790
    invoke-virtual {v10}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    .line 116791
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/18u;

    .line 116792
    if-eqz v0, :cond_3

    iget-boolean v1, v0, LX/18u;->a:Z

    if-eqz v1, :cond_3

    .line 116793
    iget v1, v0, LX/18u;->b:I

    and-int/lit8 v6, v1, 0x7

    .line 116794
    iget v1, v0, LX/18u;->b:I

    and-int/lit8 v4, v1, 0x70

    .line 116795
    const/high16 v2, -0x80000000

    .line 116796
    const/high16 v1, -0x80000000

    .line 116797
    const/16 v7, 0x30

    if-eq v4, v7, :cond_0

    const/16 v7, 0x50

    if-ne v4, v7, :cond_4

    :cond_0
    const/4 v4, 0x1

    move v7, v4

    .line 116798
    :goto_1
    const/4 v4, 0x3

    if-eq v6, v4, :cond_1

    const/4 v4, 0x5

    if-ne v6, v4, :cond_5

    :cond_1
    const/4 v4, 0x1

    move v6, v4

    .line 116799
    :goto_2
    if-eqz v7, :cond_6

    .line 116800
    const/high16 v2, 0x40000000    # 2.0f

    .line 116801
    :cond_2
    :goto_3
    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v11, -0x2

    if-eq v4, v11, :cond_e

    .line 116802
    const/high16 v4, 0x40000000    # 2.0f

    .line 116803
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v11, -0x1

    if-eq v2, v11, :cond_d

    .line 116804
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 116805
    :goto_4
    iget v11, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v12, -0x2

    if-eq v11, v12, :cond_c

    .line 116806
    const/high16 v1, 0x40000000    # 2.0f

    .line 116807
    iget v11, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v12, -0x1

    if-eq v11, v12, :cond_c

    .line 116808
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 116809
    :goto_5
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 116810
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 116811
    invoke-virtual {v10, v2, v0}, Landroid/view/View;->measure(II)V

    .line 116812
    if-eqz v7, :cond_7

    .line 116813
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v5, v0

    .line 116814
    :cond_3
    :goto_6
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 116815
    :cond_4
    const/4 v4, 0x0

    move v7, v4

    goto :goto_1

    .line 116816
    :cond_5
    const/4 v4, 0x0

    move v6, v4

    goto :goto_2

    .line 116817
    :cond_6
    if-eqz v6, :cond_2

    .line 116818
    const/high16 v1, 0x40000000    # 2.0f

    goto :goto_3

    .line 116819
    :cond_7
    if-eqz v6, :cond_3

    .line 116820
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v3, v0

    goto :goto_6

    .line 116821
    :cond_8
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->w:I

    .line 116822
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->x:I

    .line 116823
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->y:Z

    .line 116824
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 116825
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->y:Z

    .line 116826
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    .line 116827
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v2, :cond_b

    .line 116828
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 116829
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-eq v0, v5, :cond_a

    .line 116830
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/18u;

    .line 116831
    if-eqz v0, :cond_9

    iget-boolean v5, v0, LX/18u;->a:Z

    if-nez v5, :cond_a

    .line 116832
    :cond_9
    int-to-float v5, v3

    iget v0, v0, LX/18u;->c:F

    mul-float/2addr v0, v5

    float-to-int v0, v0

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 116833
    iget v5, p0, Landroid/support/v4/view/ViewPager;->x:I

    invoke-virtual {v4, v0, v5}, Landroid/view/View;->measure(II)V

    .line 116834
    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 116835
    :cond_b
    return-void

    :cond_c
    move v0, v5

    goto :goto_5

    :cond_d
    move v2, v3

    goto/16 :goto_4

    :cond_e
    move v4, v2

    move v2, v3

    goto/16 :goto_4
.end method

.method public final onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 116768
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    .line 116769
    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_0

    move v1, v2

    move v3, v4

    .line 116770
    :goto_0
    if-eq v3, v0, :cond_2

    .line 116771
    invoke-virtual {p0, v3}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 116772
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    .line 116773
    invoke-direct {p0, v5}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)LX/0vd;

    move-result-object v6

    .line 116774
    if-eqz v6, :cond_1

    iget v6, v6, LX/0vd;->b:I

    iget v7, p0, Landroid/support/v4/view/ViewPager;->k:I

    if-ne v6, v7, :cond_1

    .line 116775
    invoke-virtual {v5, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 116776
    :goto_1
    return v2

    .line 116777
    :cond_0
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v0, v1

    .line 116778
    goto :goto_0

    .line 116779
    :cond_1
    add-int/2addr v3, v1

    goto :goto_0

    :cond_2
    move v2, v4

    .line 116780
    goto :goto_1
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 116757
    instance-of v0, p1, Landroid/support/v4/view/ViewPager$SavedState;

    if-nez v0, :cond_0

    .line 116758
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 116759
    :goto_0
    return-void

    .line 116760
    :cond_0
    check-cast p1, Landroid/support/v4/view/ViewPager$SavedState;

    .line 116761
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 116762
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    if-eqz v0, :cond_1

    .line 116763
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget-object v1, p1, Landroid/support/v4/view/ViewPager$SavedState;->b:Landroid/os/Parcelable;

    iget-object v2, p1, Landroid/support/v4/view/ViewPager$SavedState;->c:Ljava/lang/ClassLoader;

    invoke-virtual {v0, v1, v2}, LX/0gG;->a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 116764
    iget v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->a:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    goto :goto_0

    .line 116765
    :cond_1
    iget v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->a:I

    iput v0, p0, Landroid/support/v4/view/ViewPager;->l:I

    .line 116766
    iget-object v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->b:Landroid/os/Parcelable;

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->m:Landroid/os/Parcelable;

    .line 116767
    iget-object v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->c:Ljava/lang/ClassLoader;

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->n:Ljava/lang/ClassLoader;

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 116751
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 116752
    new-instance v1, Landroid/support/v4/view/ViewPager$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v4/view/ViewPager$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 116753
    iget v0, p0, Landroid/support/v4/view/ViewPager;->k:I

    iput v0, v1, Landroid/support/v4/view/ViewPager$SavedState;->a:I

    .line 116754
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    if-eqz v0, :cond_0

    .line 116755
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->lf_()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, v1, Landroid/support/v4/view/ViewPager$SavedState;->b:Landroid/os/Parcelable;

    .line 116756
    :cond_0
    return-object v1
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x27a7f1ab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 116747
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 116748
    if-eq p1, p3, :cond_0

    .line 116749
    iget v1, p0, Landroid/support/v4/view/ViewPager;->q:I

    iget v2, p0, Landroid/support/v4/view/ViewPager;->q:I

    invoke-direct {p0, p1, p3, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IIII)V

    .line 116750
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x7fe4f1b3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const v0, 0x490af5e1

    invoke-static {v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 116670
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->S:Z

    if-eqz v0, :cond_0

    .line 116671
    const v0, 0x273de56b

    invoke-static {v4, v4, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move v0, v1

    .line 116672
    :goto_0
    return v0

    .line 116673
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v0

    if-eqz v0, :cond_1

    .line 116674
    const v0, 0x4bd8871e    # 2.8380732E7f

    invoke-static {v0, v3}, LX/02F;->a(II)V

    move v0, v2

    goto :goto_0

    .line 116675
    :cond_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-nez v0, :cond_3

    .line 116676
    :cond_2
    const v0, 0x76a3288

    invoke-static {v0, v3}, LX/02F;->a(II)V

    move v0, v2

    goto :goto_0

    .line 116677
    :cond_3
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    if-nez v0, :cond_4

    .line 116678
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    .line 116679
    :cond_4
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 116680
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 116681
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 116682
    :cond_5
    :goto_1
    :pswitch_0
    if-eqz v2, :cond_6

    .line 116683
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 116684
    :cond_6
    const v0, -0x27de33bc

    invoke-static {v0, v3}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0

    .line 116685
    :pswitch_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->o:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 116686
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->A:Z

    .line 116687
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 116688
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->ad:Z

    if-eqz v0, :cond_7

    .line 116689
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->D:Z

    .line 116690
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->c(Z)V

    .line 116691
    invoke-static {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollState(Landroid/support/v4/view/ViewPager;I)V

    .line 116692
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->K:F

    iput v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 116693
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->L:F

    iput v0, p0, Landroid/support/v4/view/ViewPager;->J:F

    .line 116694
    invoke-static {p1, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->M:I

    goto :goto_1

    .line 116695
    :pswitch_2
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->D:Z

    if-nez v0, :cond_8

    .line 116696
    iget v0, p0, Landroid/support/v4/view/ViewPager;->M:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 116697
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 116698
    iget v5, p0, Landroid/support/v4/view/ViewPager;->I:F

    sub-float v5, v4, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 116699
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v6

    .line 116700
    iget v0, p0, Landroid/support/v4/view/ViewPager;->J:F

    sub-float v0, v6, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 116701
    iget v7, p0, Landroid/support/v4/view/ViewPager;->H:I

    int-to-float v7, v7

    cmpl-float v7, v5, v7

    if-lez v7, :cond_8

    cmpl-float v0, v5, v0

    if-lez v0, :cond_8

    .line 116702
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->D:Z

    .line 116703
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->c(Z)V

    .line 116704
    iget v0, p0, Landroid/support/v4/view/ViewPager;->K:F

    sub-float v0, v4, v0

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_9

    iget v0, p0, Landroid/support/v4/view/ViewPager;->K:F

    iget v4, p0, Landroid/support/v4/view/ViewPager;->H:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    :goto_2
    iput v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 116705
    iput v6, p0, Landroid/support/v4/view/ViewPager;->J:F

    .line 116706
    invoke-static {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollState(Landroid/support/v4/view/ViewPager;I)V

    .line 116707
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 116708
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 116709
    if-eqz v0, :cond_8

    .line 116710
    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 116711
    :cond_8
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->D:Z

    if-eqz v0, :cond_5

    .line 116712
    iget v0, p0, Landroid/support/v4/view/ViewPager;->M:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 116713
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 116714
    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->c(F)Z

    move-result v0

    or-int/lit8 v2, v0, 0x0

    .line 116715
    goto/16 :goto_1

    .line 116716
    :cond_9
    iget v0, p0, Landroid/support/v4/view/ViewPager;->K:F

    iget v4, p0, Landroid/support/v4/view/ViewPager;->H:I

    int-to-float v4, v4

    sub-float/2addr v0, v4

    goto :goto_2

    .line 116717
    :pswitch_3
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->D:Z

    if-eqz v0, :cond_5

    .line 116718
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->N:Landroid/view/VelocityTracker;

    .line 116719
    const/16 v2, 0x3e8

    iget v4, p0, Landroid/support/v4/view/ViewPager;->P:I

    int-to-float v4, v4

    invoke-virtual {v0, v2, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 116720
    iget v2, p0, Landroid/support/v4/view/ViewPager;->M:I

    invoke-static {v0, v2}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v0, v0

    .line 116721
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->A:Z

    .line 116722
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->getClientWidth()I

    move-result v2

    .line 116723
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v4

    .line 116724
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->j()LX/0vd;

    move-result-object v5

    .line 116725
    iget v6, v5, LX/0vd;->b:I

    .line 116726
    int-to-float v4, v4

    int-to-float v2, v2

    div-float v2, v4, v2

    iget v4, v5, LX/0vd;->e:F

    sub-float/2addr v2, v4

    iget v4, v5, LX/0vd;->d:F

    div-float/2addr v2, v4

    .line 116727
    iget v4, p0, Landroid/support/v4/view/ViewPager;->M:I

    invoke-static {p1, v4}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 116728
    invoke-static {p1, v4}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 116729
    iget v5, p0, Landroid/support/v4/view/ViewPager;->K:F

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 116730
    invoke-direct {p0, v6, v2, v0, v4}, Landroid/support/v4/view/ViewPager;->a(IFII)I

    move-result v2

    .line 116731
    invoke-direct {p0, v2, v1, v1, v0}, Landroid/support/v4/view/ViewPager;->a(IZZI)V

    .line 116732
    iput v7, p0, Landroid/support/v4/view/ViewPager;->M:I

    .line 116733
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->k()V

    .line 116734
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->U:LX/0vj;

    invoke-virtual {v0}, LX/0vj;->c()Z

    move-result v0

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->V:LX/0vj;

    invoke-virtual {v2}, LX/0vj;->c()Z

    move-result v2

    or-int/2addr v2, v0

    .line 116735
    goto/16 :goto_1

    .line 116736
    :pswitch_4
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->D:Z

    if-eqz v0, :cond_5

    .line 116737
    iget v0, p0, Landroid/support/v4/view/ViewPager;->k:I

    invoke-direct {p0, v0, v1, v2, v2}, Landroid/support/v4/view/ViewPager;->a(IZIZ)V

    .line 116738
    iput v7, p0, Landroid/support/v4/view/ViewPager;->M:I

    .line 116739
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->k()V

    .line 116740
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->U:LX/0vj;

    invoke-virtual {v0}, LX/0vj;->c()Z

    move-result v0

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->V:LX/0vj;

    invoke-virtual {v2}, LX/0vj;->c()Z

    move-result v2

    or-int/2addr v2, v0

    goto/16 :goto_1

    .line 116741
    :pswitch_5
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 116742
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 116743
    iput v4, p0, Landroid/support/v4/view/ViewPager;->I:F

    .line 116744
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->M:I

    goto/16 :goto_1

    .line 116745
    :pswitch_6
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/MotionEvent;)V

    .line 116746
    iget v0, p0, Landroid/support/v4/view/ViewPager;->M:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->I:F

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final removeView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 116666
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->y:Z

    if-eqz v0, :cond_0

    .line 116667
    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->removeViewInLayout(Landroid/view/View;)V

    .line 116668
    :goto_0
    return-void

    .line 116669
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setAdapter(LX/0gG;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 116631
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    if-eqz v0, :cond_1

    .line 116632
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->p:LX/10K;

    invoke-virtual {v0, v1}, LX/0gG;->b(Landroid/database/DataSetObserver;)V

    .line 116633
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v0, p0}, LX/0gG;->a(Landroid/view/ViewGroup;)V

    move v1, v2

    .line 116634
    :goto_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 116635
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0vd;

    .line 116636
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget v4, v0, LX/0vd;->b:I

    iget-object v0, v0, LX/0vd;->a:Ljava/lang/Object;

    invoke-virtual {v3, p0, v4, v0}, LX/0gG;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 116637
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 116638
    :cond_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v0, p0}, LX/0gG;->b(Landroid/view/ViewGroup;)V

    .line 116639
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 116640
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->h()V

    .line 116641
    iput v2, p0, Landroid/support/v4/view/ViewPager;->k:I

    .line 116642
    invoke-virtual {p0, v2, v2}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 116643
    :cond_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    .line 116644
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    .line 116645
    iput v2, p0, Landroid/support/v4/view/ViewPager;->b:I

    .line 116646
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    if-eqz v1, :cond_3

    .line 116647
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->p:LX/10K;

    if-nez v1, :cond_2

    .line 116648
    new-instance v1, LX/10K;

    invoke-direct {v1, p0}, LX/10K;-><init>(Landroid/support/v4/view/ViewPager;)V

    iput-object v1, p0, Landroid/support/v4/view/ViewPager;->p:LX/10K;

    .line 116649
    :cond_2
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->p:LX/10K;

    invoke-virtual {v1, v3}, LX/0gG;->a(Landroid/database/DataSetObserver;)V

    .line 116650
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->A:Z

    .line 116651
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->W:Z

    .line 116652
    iput-boolean v5, p0, Landroid/support/v4/view/ViewPager;->W:Z

    .line 116653
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    invoke-virtual {v3}, LX/0gG;->b()I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/ViewPager;->b:I

    .line 116654
    iget v3, p0, Landroid/support/v4/view/ViewPager;->l:I

    if-ltz v3, :cond_5

    .line 116655
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->j:LX/0gG;

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->m:Landroid/os/Parcelable;

    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->n:Ljava/lang/ClassLoader;

    invoke-virtual {v1, v3, v4}, LX/0gG;->a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 116656
    iget v1, p0, Landroid/support/v4/view/ViewPager;->l:I

    invoke-direct {p0, v1, v2, v5}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 116657
    const/4 v1, -0x1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->l:I

    .line 116658
    iput-object v6, p0, Landroid/support/v4/view/ViewPager;->m:Landroid/os/Parcelable;

    .line 116659
    iput-object v6, p0, Landroid/support/v4/view/ViewPager;->n:Ljava/lang/ClassLoader;

    .line 116660
    :cond_3
    :goto_1
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->ag:LX/3ru;

    if-eqz v1, :cond_4

    if-eq v0, p1, :cond_4

    .line 116661
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->ag:LX/3ru;

    invoke-interface {v1, v0, p1}, LX/3ru;->a(LX/0gG;LX/0gG;)V

    .line 116662
    :cond_4
    return-void

    .line 116663
    :cond_5
    if-nez v1, :cond_6

    .line 116664
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    goto :goto_1

    .line 116665
    :cond_6
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    goto :goto_1
.end method

.method public setChildrenDrawingOrderEnabledCompat(Z)V
    .locals 5

    .prologue
    .line 116526
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x7

    if-lt v0, v1, :cond_1

    .line 116527
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ai:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 116528
    :try_start_0
    const-class v0, Landroid/view/ViewGroup;

    const-string v1, "setChildrenDrawingOrderEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->ai:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116529
    :cond_0
    :goto_0
    :try_start_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->ai:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 116530
    :cond_1
    :goto_1
    return-void

    .line 116531
    :catch_0
    move-exception v0

    .line 116532
    const-string v1, "ViewPager"

    const-string v2, "Can\'t find setChildrenDrawingOrderEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 116533
    :catch_1
    move-exception v0

    .line 116534
    const-string v1, "ViewPager"

    const-string v2, "Error changing children drawing order"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setCurrentItem(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116623
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->A:Z

    .line 116624
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->W:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 116625
    return-void

    :cond_0
    move v0, v1

    .line 116626
    goto :goto_0
.end method

.method public setIgnoreTouchSlop(Z)V
    .locals 0

    .prologue
    .line 116621
    iput-boolean p1, p0, Landroid/support/v4/view/ViewPager;->ad:Z

    .line 116622
    return-void
.end method

.method public setOffscreenPageLimit(I)V
    .locals 3

    .prologue
    .line 116614
    if-gtz p1, :cond_0

    .line 116615
    const-string v0, "ViewPager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested offscreen page limit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " too small; defaulting to 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 116616
    const/4 p1, 0x1

    .line 116617
    :cond_0
    iget v0, p0, Landroid/support/v4/view/ViewPager;->B:I

    if-eq p1, v0, :cond_1

    .line 116618
    iput p1, p0, Landroid/support/v4/view/ViewPager;->B:I

    .line 116619
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 116620
    :cond_1
    return-void
.end method

.method public setOnAdapterChangeListener(LX/3ru;)V
    .locals 0

    .prologue
    .line 116612
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->ag:LX/3ru;

    .line 116613
    return-void
.end method

.method public setOnPageChangeListener(LX/0hc;)V
    .locals 0

    .prologue
    .line 116610
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->ae:LX/0hc;

    .line 116611
    return-void
.end method

.method public setOnlyCreatePagesImmediatelyOffscreen(Z)V
    .locals 1

    .prologue
    .line 116606
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->C:Z

    if-eq p1, v0, :cond_0

    .line 116607
    iput-boolean p1, p0, Landroid/support/v4/view/ViewPager;->C:Z

    .line 116608
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->b()V

    .line 116609
    :cond_0
    return-void
.end method

.method public setPageMargin(I)V
    .locals 2

    .prologue
    .line 116600
    iget v0, p0, Landroid/support/v4/view/ViewPager;->q:I

    .line 116601
    iput p1, p0, Landroid/support/v4/view/ViewPager;->q:I

    .line 116602
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v1

    .line 116603
    invoke-direct {p0, v1, v1, p1, v0}, Landroid/support/v4/view/ViewPager;->a(IIII)V

    .line 116604
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    .line 116605
    return-void
.end method

.method public setPageMarginDrawable(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 116598
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 116599
    return-void
.end method

.method public setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 116592
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->r:Landroid/graphics/drawable/Drawable;

    .line 116593
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->refreshDrawableState()V

    .line 116594
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setWillNotDraw(Z)V

    .line 116595
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->invalidate()V

    .line 116596
    return-void

    .line 116597
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 116591
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->r:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
