.class public Landroid/support/v4/widget/SwipeRefreshLayout;
.super Landroid/view/ViewGroup;
.source ""


# static fields
.field private static final c:Ljava/lang/String;

.field private static final s:[I


# instance fields
.field private A:Landroid/view/animation/Animation;

.field private B:Landroid/view/animation/Animation;

.field public C:F

.field public D:Z

.field private E:I

.field private F:I

.field public G:Z

.field private H:Landroid/view/animation/Animation$AnimationListener;

.field private final I:Landroid/view/animation/Animation;

.field private final J:Landroid/view/animation/Animation;

.field public a:I

.field public b:I

.field private d:Landroid/view/View;

.field public e:LX/1PH;

.field public f:Z

.field private g:I

.field private h:F

.field private i:I

.field public j:I

.field private k:Z

.field private l:F

.field private m:F

.field private n:Z

.field private o:I

.field public p:Z

.field private q:Z

.field private final r:Landroid/view/animation/DecelerateInterpolator;

.field public t:LX/1Nz;

.field private u:I

.field public v:F

.field public w:LX/1O1;

.field private x:Landroid/view/animation/Animation;

.field private y:Landroid/view/animation/Animation;

.field private z:Landroid/view/animation/Animation;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 238278
    const-class v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Ljava/lang/String;

    .line 238279
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101000e

    aput v2, v0, v1

    sput-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->s:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 238280
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 238281
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, -0x1

    const/high16 v3, 0x42200000    # 40.0f

    const/4 v2, 0x0

    .line 238282
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 238283
    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    .line 238284
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->h:F

    .line 238285
    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->k:Z

    .line 238286
    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    .line 238287
    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->u:I

    .line 238288
    new-instance v0, LX/1Nw;

    invoke-direct {v0, p0}, LX/1Nw;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->H:Landroid/view/animation/Animation$AnimationListener;

    .line 238289
    new-instance v0, LX/1Nx;

    invoke-direct {v0, p0}, LX/1Nx;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->I:Landroid/view/animation/Animation;

    .line 238290
    new-instance v0, LX/1Ny;

    invoke-direct {v0, p0}, LX/1Ny;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->J:Landroid/view/animation/Animation;

    .line 238291
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->g:I

    .line 238292
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->i:I

    .line 238293
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setWillNotDraw(Z)V

    .line 238294
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->r:Landroid/view/animation/DecelerateInterpolator;

    .line 238295
    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->s:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 238296
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 238297
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 238298
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 238299
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->E:I

    .line 238300
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->F:I

    .line 238301
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->c()V

    .line 238302
    sget-object v1, LX/0vv;->a:LX/0w2;

    invoke-interface {v1, p0, v4}, LX/0w2;->a(Landroid/view/ViewGroup;Z)V

    .line 238303
    const/high16 v1, 0x42800000    # 64.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->C:F

    .line 238304
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->C:F

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->h:F

    .line 238305
    return-void
.end method

.method private static a(Landroid/view/MotionEvent;I)F
    .locals 1

    .prologue
    .line 238306
    invoke-static {p0, p1}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 238307
    if-gez v0, :cond_0

    .line 238308
    const/high16 v0, -0x40800000    # -1.0f

    .line 238309
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    goto :goto_0
.end method

.method private a(II)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 238310
    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:Z

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/support/v4/widget/SwipeRefreshLayout;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238311
    :goto_0
    return-object v0

    .line 238312
    :cond_0
    new-instance v1, LX/3tu;

    invoke-direct {v1, p0, p1, p2}, LX/3tu;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;II)V

    .line 238313
    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 238314
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    .line 238315
    iput-object v0, v2, LX/1Nz;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 238316
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->clearAnimation()V

    .line 238317
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0, v1}, LX/1Nz;->startAnimation(Landroid/view/animation/Animation;)V

    move-object v0, v1

    .line 238318
    goto :goto_0
.end method

.method private a(ILandroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    .prologue
    .line 238319
    iput p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:I

    .line 238320
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->I:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 238321
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->I:Landroid/view/animation/Animation;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 238322
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->I:Landroid/view/animation/Animation;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->r:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 238323
    if-eqz p2, :cond_0

    .line 238324
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    .line 238325
    iput-object p2, v0, LX/1Nz;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 238326
    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->clearAnimation()V

    .line 238327
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->I:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, LX/1Nz;->startAnimation(Landroid/view/animation/Animation;)V

    .line 238328
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 238329
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 238330
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 238331
    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    if-ne v1, v2, :cond_0

    .line 238332
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 238333
    :goto_0
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    .line 238334
    :cond_0
    return-void

    .line 238335
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    .prologue
    .line 238336
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Nz;->setVisibility(I)V

    .line 238337
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 238338
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, LX/1O1;->setAlpha(I)V

    .line 238339
    :cond_0
    new-instance v0, LX/2de;

    invoke-direct {v0, p0}, LX/2de;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->x:Landroid/view/animation/Animation;

    .line 238340
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->x:Landroid/view/animation/Animation;

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->i:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 238341
    if-eqz p1, :cond_1

    .line 238342
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    .line 238343
    iput-object p1, v0, LX/1Nz;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 238344
    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->clearAnimation()V

    .line 238345
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->x:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, LX/1Nz;->startAnimation(Landroid/view/animation/Animation;)V

    .line 238346
    return-void
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    .line 238347
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    if-eq v0, p1, :cond_0

    .line 238348
    iput-boolean p2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->D:Z

    .line 238349
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->g()V

    .line 238350
    iput-boolean p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    .line 238351
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    if-eqz v0, :cond_1

    .line 238352
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->H:Landroid/view/animation/Animation$AnimationListener;

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(ILandroid/view/animation/Animation$AnimationListener;)V

    .line 238353
    :cond_0
    :goto_0
    return-void

    .line 238354
    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->H:Landroid/view/animation/Animation$AnimationListener;

    invoke-static {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->b(Landroid/support/v4/widget/SwipeRefreshLayout;Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0
.end method

.method private static a(Landroid/view/animation/Animation;)Z
    .locals 1

    .prologue
    .line 238410
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Landroid/support/v4/widget/SwipeRefreshLayout;F)V
    .locals 3

    .prologue
    .line 238355
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:I

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 238356
    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v1}, LX/1Nz;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 238357
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a$redex0(Landroid/support/v4/widget/SwipeRefreshLayout;IZ)V

    .line 238358
    return-void
.end method

.method public static a$redex0(Landroid/support/v4/widget/SwipeRefreshLayout;IZ)V
    .locals 2

    .prologue
    .line 238359
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->bringToFront()V

    .line 238360
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0, p1}, LX/1Nz;->offsetTopAndBottom(I)V

    .line 238361
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->getTop()I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    .line 238362
    if-eqz p2, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 238363
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->invalidate()V

    .line 238364
    :cond_0
    return-void
.end method

.method private b(ILandroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    .prologue
    .line 238365
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:Z

    if-eqz v0, :cond_0

    .line 238366
    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;->c(ILandroid/view/animation/Animation$AnimationListener;)V

    .line 238367
    :goto_0
    return-void

    .line 238368
    :cond_0
    iput p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:I

    .line 238369
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->J:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 238370
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->J:Landroid/view/animation/Animation;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 238371
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->J:Landroid/view/animation/Animation;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->r:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 238372
    if-eqz p2, :cond_1

    .line 238373
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    .line 238374
    iput-object p2, v0, LX/1Nz;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 238375
    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->clearAnimation()V

    .line 238376
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->J:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, LX/1Nz;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public static b(Landroid/support/v4/widget/SwipeRefreshLayout;Landroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    .prologue
    .line 238377
    new-instance v0, LX/2dh;

    invoke-direct {v0, p0}, LX/2dh;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->y:Landroid/view/animation/Animation;

    .line 238378
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->y:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 238379
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    .line 238380
    iput-object p1, v0, LX/1Nz;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 238381
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->clearAnimation()V

    .line 238382
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->y:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, LX/1Nz;->startAnimation(Landroid/view/animation/Animation;)V

    .line 238383
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    const v3, -0x50506

    .line 238384
    new-instance v0, LX/1Nz;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-direct {v0, v1, v3, v2}, LX/1Nz;-><init>(Landroid/content/Context;IF)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    .line 238385
    new-instance v0, LX/1O1;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/1O1;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    .line 238386
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0, v3}, LX/1O1;->b(I)V

    .line 238387
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0, v1}, LX/1Nz;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 238388
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/1Nz;->setVisibility(I)V

    .line 238389
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->addView(Landroid/view/View;)V

    .line 238390
    return-void
.end method

.method private c(ILandroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    .prologue
    .line 238391
    iput p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:I

    .line 238392
    invoke-static {}, Landroid/support/v4/widget/SwipeRefreshLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238393
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0}, LX/1O1;->getAlpha()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->v:F

    .line 238394
    :goto_0
    new-instance v0, LX/3tw;

    invoke-direct {v0, p0}, LX/3tw;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->B:Landroid/view/animation/Animation;

    .line 238395
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->B:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 238396
    if-eqz p2, :cond_0

    .line 238397
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    .line 238398
    iput-object p2, v0, LX/1Nz;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 238399
    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->clearAnimation()V

    .line 238400
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->B:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, LX/1Nz;->startAnimation(Landroid/view/animation/Animation;)V

    .line 238401
    return-void

    .line 238402
    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    .line 238403
    sget-object v1, LX/0vv;->a:LX/0w2;

    invoke-interface {v1, v0}, LX/0w2;->t(Landroid/view/View;)F

    move-result v1

    move v0, v1

    .line 238404
    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->v:F

    goto :goto_0
.end method

.method private static d()Z
    .locals 2

    .prologue
    .line 238405
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 238406
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0}, LX/1O1;->getAlpha()I

    move-result v0

    const/16 v1, 0x4c

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(II)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->z:Landroid/view/animation/Animation;

    .line 238407
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 238408
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0}, LX/1O1;->getAlpha()I

    move-result v0

    const/16 v1, 0xff

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(II)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->A:Landroid/view/animation/Animation;

    .line 238409
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 238266
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    if-nez v0, :cond_0

    .line 238267
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 238268
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 238269
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 238270
    iput-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    .line 238271
    :cond_0
    return-void

    .line 238272
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static setAnimationProgress(Landroid/support/v4/widget/SwipeRefreshLayout;F)V
    .locals 1

    .prologue
    .line 238273
    invoke-static {}, Landroid/support/v4/widget/SwipeRefreshLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238274
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    invoke-static {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorViewAlpha(Landroid/support/v4/widget/SwipeRefreshLayout;I)V

    .line 238275
    :goto_0
    return-void

    .line 238276
    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-static {v0, p1}, LX/0vv;->d(Landroid/view/View;F)V

    .line 238277
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-static {v0, p1}, LX/0vv;->e(Landroid/view/View;F)V

    goto :goto_0
.end method

.method public static setColorViewAlpha(Landroid/support/v4/widget/SwipeRefreshLayout;I)V
    .locals 1

    .prologue
    .line 238138
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 238139
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0, p1}, LX/1O1;->setAlpha(I)V

    .line 238140
    return-void
.end method


# virtual methods
.method public final a(ZII)V
    .locals 2

    .prologue
    .line 238074
    iput-boolean p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:Z

    .line 238075
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/1Nz;->setVisibility(I)V

    .line 238076
    iput p2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    iput p2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    .line 238077
    int-to-float v0, p3

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->C:F

    .line 238078
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->G:Z

    .line 238079
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->invalidate()V

    .line 238080
    return-void
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 238081
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v0, v3, :cond_4

    .line 238082
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/AbsListView;

    if-eqz v0, :cond_2

    .line 238083
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    check-cast v0, Landroid/widget/AbsListView;

    .line 238084
    invoke-virtual {v0}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v3

    if-gtz v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getPaddingTop()I

    move-result v0

    if-ge v3, v0, :cond_1

    :cond_0
    move v0, v1

    .line 238085
    :goto_0
    return v0

    :cond_1
    move v0, v2

    .line 238086
    goto :goto_0

    .line 238087
    :cond_2
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    .line 238088
    :cond_4
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    const/4 v1, -0x1

    invoke-static {v0, v1}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final getChildDrawingOrder(II)I
    .locals 1

    .prologue
    .line 238089
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->u:I

    if-gez v0, :cond_1

    .line 238090
    :cond_0
    :goto_0
    return p2

    .line 238091
    :cond_1
    add-int/lit8 v0, p1, -0x1

    if-ne p2, v0, :cond_2

    .line 238092
    iget p2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->u:I

    goto :goto_0

    .line 238093
    :cond_2
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->u:I

    if-lt p2, v0, :cond_0

    .line 238094
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method

.method public getProgressCircleDiameter()I
    .locals 1

    .prologue
    .line 238095
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->getMeasuredHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v0, 0x0

    .line 238096
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->g()V

    .line 238097
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v1

    .line 238098
    iget-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->q:Z

    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    .line 238099
    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->q:Z

    .line 238100
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->q:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->b()Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    if-eqz v2, :cond_2

    .line 238101
    :cond_1
    :goto_0
    return v0

    .line 238102
    :cond_2
    packed-switch v1, :pswitch_data_0

    .line 238103
    :cond_3
    :goto_1
    :pswitch_0
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:Z

    goto :goto_0

    .line 238104
    :pswitch_1
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v2}, LX/1Nz;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {p0, v1, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->a$redex0(Landroid/support/v4/widget/SwipeRefreshLayout;IZ)V

    .line 238105
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    .line 238106
    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:Z

    .line 238107
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    invoke-static {p1, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 238108
    cmpl-float v2, v1, v3

    if-eqz v2, :cond_1

    .line 238109
    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->m:F

    goto :goto_1

    .line 238110
    :pswitch_2
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    if-ne v1, v4, :cond_4

    .line 238111
    sget-object v1, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Ljava/lang/String;

    const-string v2, "Got ACTION_MOVE event but don\'t have an active pointer id."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 238112
    :cond_4
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    invoke-static {p1, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 238113
    cmpl-float v2, v1, v3

    if-eqz v2, :cond_1

    .line 238114
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->m:F

    sub-float v0, v1, v0

    .line 238115
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->g:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:Z

    if-nez v0, :cond_3

    .line 238116
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->m:F

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->g:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->l:F

    .line 238117
    iput-boolean v5, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:Z

    .line 238118
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    const/16 v1, 0x4c

    invoke-virtual {v0, v1}, LX/1O1;->setAlpha(I)V

    goto :goto_1

    .line 238119
    :pswitch_3
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 238120
    :pswitch_4
    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:Z

    .line 238121
    iput v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 238122
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredWidth()I

    move-result v0

    .line 238123
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredHeight()I

    move-result v1

    .line 238124
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 238125
    :cond_0
    :goto_0
    return-void

    .line 238126
    :cond_1
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    if-nez v2, :cond_2

    .line 238127
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->g()V

    .line 238128
    :cond_2
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 238129
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    .line 238130
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v3

    .line 238131
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingTop()I

    move-result v4

    .line 238132
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v5

    sub-int v5, v0, v5

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    .line 238133
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingTop()I

    move-result v6

    sub-int/2addr v1, v6

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v1, v6

    .line 238134
    add-int/2addr v5, v3

    add-int/2addr v1, v4

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/view/View;->layout(IIII)V

    .line 238135
    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v1}, LX/1Nz;->getMeasuredWidth()I

    move-result v1

    .line 238136
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v2}, LX/1Nz;->getMeasuredHeight()I

    move-result v2

    .line 238137
    iget-object v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    div-int/lit8 v4, v0, 0x2

    div-int/lit8 v5, v1, 0x2

    sub-int/2addr v4, v5

    iget v5, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    add-int/2addr v1, v2

    invoke-virtual {v3, v4, v5, v0, v1}, LX/1Nz;->layout(IIII)V

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 238141
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 238142
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    if-nez v0, :cond_0

    .line 238143
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->g()V

    .line 238144
    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    if-nez v0, :cond_2

    .line 238145
    :cond_1
    :goto_0
    return-void

    .line 238146
    :cond_2
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 238147
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->E:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->F:I

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1Nz;->measure(II)V

    .line 238148
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->G:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->k:Z

    if-nez v0, :cond_3

    .line 238149
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->k:Z

    .line 238150
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0}, LX/1Nz;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    .line 238151
    :cond_3
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->u:I

    .line 238152
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 238153
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    if-ne v1, v2, :cond_4

    .line 238154
    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->u:I

    goto :goto_0

    .line 238155
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x3265c565

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 238156
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 238157
    iget-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->q:Z

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    .line 238158
    const/4 v2, 0x0

    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->q:Z

    .line 238159
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->q:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 238160
    :cond_1
    const/4 v0, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x2

    const v4, 0xc057bb9

    invoke-static {v2, v3, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 238161
    :goto_0
    return v0

    .line 238162
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 238163
    :cond_3
    :goto_1
    :pswitch_0
    const/4 v0, 0x1

    const v2, 0x57a72b7c

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 238164
    :pswitch_1
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    .line 238165
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:Z

    goto :goto_1

    .line 238166
    :pswitch_2
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 238167
    if-gez v0, :cond_4

    .line 238168
    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Ljava/lang/String;

    const-string v2, "Got ACTION_MOVE event but have an invalid active pointer id."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238169
    const/4 v0, 0x0

    const v2, 0x739874f9

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 238170
    :cond_4
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 238171
    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->l:F

    sub-float/2addr v0, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v2, v0

    .line 238172
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:Z

    if-eqz v0, :cond_3

    .line 238173
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/1O1;->a(Z)V

    .line 238174
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->h:F

    div-float v0, v2, v0

    .line 238175
    const/4 v3, 0x0

    cmpg-float v3, v0, v3

    if-gez v3, :cond_5

    .line 238176
    const/4 v0, 0x0

    const v2, 0x70a8b452

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 238177
    :cond_5
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 238178
    float-to-double v4, v3

    const-wide v6, 0x3fd999999999999aL    # 0.4

    sub-double/2addr v4, v6

    const-wide/16 v6, 0x0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    double-to-float v0, v4

    const/high16 v4, 0x40a00000    # 5.0f

    mul-float/2addr v0, v4

    const/high16 v4, 0x40400000    # 3.0f

    div-float v4, v0, v4

    .line 238179
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v5, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->h:F

    sub-float v5, v0, v5

    .line 238180
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->G:Z

    if-eqz v0, :cond_b

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->C:F

    iget v6, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    int-to-float v6, v6

    sub-float/2addr v0, v6

    .line 238181
    :goto_2
    const/4 v6, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v7, v0

    invoke-static {v5, v7}, Ljava/lang/Math;->min(FF)F

    move-result v5

    div-float/2addr v5, v0

    invoke-static {v6, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 238182
    const/high16 v6, 0x40800000    # 4.0f

    div-float v6, v5, v6

    float-to-double v6, v6

    const/high16 v8, 0x40800000    # 4.0f

    div-float/2addr v5, v8

    float-to-double v8, v5

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    sub-double/2addr v6, v8

    double-to-float v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v5, v6

    .line 238183
    mul-float v6, v0, v5

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v6, v7

    .line 238184
    iget v7, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    mul-float/2addr v0, v3

    add-float/2addr v0, v6

    float-to-int v0, v0

    add-int/2addr v0, v7

    .line 238185
    iget-object v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v3}, LX/1Nz;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_6

    .line 238186
    iget-object v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, LX/1Nz;->setVisibility(I)V

    .line 238187
    :cond_6
    iget-boolean v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:Z

    if-nez v3, :cond_7

    .line 238188
    iget-object v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v3, v6}, LX/0vv;->d(Landroid/view/View;F)V

    .line 238189
    iget-object v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v3, v6}, LX/0vv;->e(Landroid/view/View;F)V

    .line 238190
    :cond_7
    iget v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->h:F

    cmpg-float v3, v2, v3

    if-gez v3, :cond_c

    .line 238191
    iget-boolean v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:Z

    if-eqz v3, :cond_8

    .line 238192
    iget v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->h:F

    div-float/2addr v2, v3

    invoke-static {p0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setAnimationProgress(Landroid/support/v4/widget/SwipeRefreshLayout;F)V

    .line 238193
    :cond_8
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v2}, LX/1O1;->getAlpha()I

    move-result v2

    const/16 v3, 0x4c

    if-le v2, v3, :cond_9

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->z:Landroid/view/animation/Animation;

    invoke-static {v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/view/animation/Animation;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 238194
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->e()V

    .line 238195
    :cond_9
    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v2, v4

    .line 238196
    iget-object v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    const/4 v6, 0x0

    const v7, 0x3f4ccccd    # 0.8f

    invoke-static {v7, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {v3, v6, v2}, LX/1O1;->a(FF)V

    .line 238197
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-virtual {v2, v3}, LX/1O1;->a(F)V

    .line 238198
    :cond_a
    :goto_3
    const/high16 v2, -0x41800000    # -0.25f

    const v3, 0x3ecccccd    # 0.4f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v5

    add-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    .line 238199
    iget-object v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    .line 238200
    iget-object v4, v3, LX/1O1;->h:LX/1O5;

    invoke-virtual {v4, v2}, LX/1O5;->d(F)V

    .line 238201
    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    sub-int/2addr v0, v2

    const/4 v2, 0x1

    invoke-static {p0, v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a$redex0(Landroid/support/v4/widget/SwipeRefreshLayout;IZ)V

    goto/16 :goto_1

    .line 238202
    :cond_b
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->C:F

    goto/16 :goto_2

    .line 238203
    :cond_c
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v2}, LX/1O1;->getAlpha()I

    move-result v2

    const/16 v3, 0xff

    if-ge v2, v3, :cond_a

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->A:Landroid/view/animation/Animation;

    invoke-static {v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/view/animation/Animation;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 238204
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->f()V

    goto :goto_3

    .line 238205
    :pswitch_3
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 238206
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    goto/16 :goto_1

    .line 238207
    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 238208
    :pswitch_5
    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_e

    .line 238209
    const/4 v2, 0x1

    if-ne v0, v2, :cond_d

    .line 238210
    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Ljava/lang/String;

    const-string v2, "Got ACTION_UP event but don\'t have an active pointer id."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238211
    :cond_d
    const/4 v0, 0x0

    const v2, 0x22ad15ec

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 238212
    :cond_e
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 238213
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 238214
    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->l:F

    sub-float/2addr v0, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    .line 238215
    const/4 v2, 0x0

    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:Z

    .line 238216
    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->h:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_f

    .line 238217
    const/4 v0, 0x1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(ZZ)V

    .line 238218
    :goto_4
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:I

    .line 238219
    const/4 v0, 0x0

    const v2, -0x7dd431c8

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 238220
    :cond_f
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    .line 238221
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/1O1;->a(FF)V

    .line 238222
    const/4 v0, 0x0

    .line 238223
    iget-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:Z

    if-nez v2, :cond_10

    .line 238224
    new-instance v0, LX/3tv;

    invoke-direct {v0, p0}, LX/3tv;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    .line 238225
    :cond_10
    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    invoke-direct {p0, v2, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->b(ILandroid/view/animation/Animation$AnimationListener;)V

    .line 238226
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/1O1;->a(Z)V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .prologue
    .line 238227
    return-void
.end method

.method public varargs setColorScheme([I)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 238228
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 238229
    return-void
.end method

.method public varargs setColorSchemeColors([I)V
    .locals 2

    .prologue
    .line 238230
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->g()V

    .line 238231
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    .line 238232
    iget-object v1, v0, LX/1O1;->h:LX/1O5;

    invoke-virtual {v1, p1}, LX/1O5;->a([I)V

    .line 238233
    iget-object v1, v0, LX/1O1;->h:LX/1O5;

    const/4 p0, 0x0

    .line 238234
    iput p0, v1, LX/1O5;->k:I

    .line 238235
    return-void
.end method

.method public varargs setColorSchemeResources([I)V
    .locals 4

    .prologue
    .line 238067
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 238068
    array-length v0, p1

    new-array v2, v0, [I

    .line 238069
    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 238070
    aget v3, p1, v0

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v2, v0

    .line 238071
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238072
    :cond_0
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    .line 238073
    return-void
.end method

.method public setDistanceToTriggerSync(I)V
    .locals 1

    .prologue
    .line 238236
    int-to-float v0, p1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->h:F

    .line 238237
    return-void
.end method

.method public setOnRefreshListener(LX/1PH;)V
    .locals 0

    .prologue
    .line 238238
    iput-object p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->e:LX/1PH;

    .line 238239
    return-void
.end method

.method public setProgressBackgroundColor(I)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 238240
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setProgressBackgroundColorSchemeResource(I)V

    .line 238241
    return-void
.end method

.method public setProgressBackgroundColorSchemeColor(I)V
    .locals 1

    .prologue
    .line 238242
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v0, p1}, LX/1Nz;->setBackgroundColor(I)V

    .line 238243
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0, p1}, LX/1O1;->b(I)V

    .line 238244
    return-void
.end method

.method public setProgressBackgroundColorSchemeResource(I)V
    .locals 1

    .prologue
    .line 238245
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setProgressBackgroundColorSchemeColor(I)V

    .line 238246
    return-void
.end method

.method public setRefreshing(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 238247
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    if-eq v0, p1, :cond_1

    .line 238248
    iput-boolean p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    .line 238249
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->G:Z

    if-nez v0, :cond_0

    .line 238250
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->C:F

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 238251
    :goto_0
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:I

    sub-int/2addr v0, v1

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a$redex0(Landroid/support/v4/widget/SwipeRefreshLayout;IZ)V

    .line 238252
    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->D:Z

    .line 238253
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->H:Landroid/view/animation/Animation$AnimationListener;

    invoke-direct {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/view/animation/Animation$AnimationListener;)V

    .line 238254
    :goto_1
    return-void

    .line 238255
    :cond_0
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->C:F

    float-to-int v0, v0

    goto :goto_0

    .line 238256
    :cond_1
    invoke-direct {p0, p1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(ZZ)V

    goto :goto_1
.end method

.method public setSize(I)V
    .locals 2

    .prologue
    .line 238257
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 238258
    :goto_0
    return-void

    .line 238259
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 238260
    if-nez p1, :cond_1

    .line 238261
    const/high16 v1, 0x42600000    # 56.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->E:I

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->F:I

    .line 238262
    :goto_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Nz;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 238263
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0, p1}, LX/1O1;->a(I)V

    .line 238264
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    invoke-virtual {v0, v1}, LX/1Nz;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 238265
    :cond_1
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->E:I

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->F:I

    goto :goto_1
.end method
