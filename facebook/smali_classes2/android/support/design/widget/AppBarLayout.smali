.class public Landroid/support/design/widget/AppBarLayout;
.super Landroid/widget/LinearLayout;
.source ""


# annotations
.annotation runtime Landroid/support/design/widget/CoordinatorLayout$DefaultBehavior;
    value = Landroid/support/design/widget/AppBarLayout$Behavior;
.end annotation


# instance fields
.field public a:Z

.field private b:I

.field private c:I

.field private d:I

.field public e:F

.field public f:I

.field public g:LX/3sc;

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3nw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 340623
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/AppBarLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 340624
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 340625
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 340626
    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 340627
    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->c:I

    .line 340628
    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->d:I

    .line 340629
    iput v2, p0, Landroid/support/design/widget/AppBarLayout;->f:I

    .line 340630
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/design/widget/AppBarLayout;->setOrientation(I)V

    .line 340631
    invoke-static {p1}, LX/1uI;->a(Landroid/content/Context;)V

    .line 340632
    sget-object v0, LX/03r;->AppBarLayout:[I

    const v1, 0x7f0e027a

    invoke-virtual {p1, p2, v0, v2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 340633
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/design/widget/AppBarLayout;->e:F

    .line 340634
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/AppBarLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 340635
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 340636
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/AppBarLayout;->setExpanded(Z)V

    .line 340637
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 340638
    sget-object v0, LX/1uL;->b:LX/1uP;

    invoke-interface {v0, p0}, LX/1uP;->a(Landroid/view/View;)V

    .line 340639
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout;->h:Ljava/util/List;

    .line 340640
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->e:F

    invoke-static {p0, v0}, LX/0vv;->f(Landroid/view/View;F)V

    .line 340641
    new-instance v0, LX/1uQ;

    invoke-direct {v0, p0}, LX/1uQ;-><init>(Landroid/support/design/widget/AppBarLayout;)V

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/1uR;)V

    .line 340642
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)LX/1uS;
    .locals 2

    .prologue
    .line 340643
    new-instance v0, LX/1uS;

    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/1uS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)LX/1uS;
    .locals 1

    .prologue
    .line 340644
    instance-of v0, p0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_0

    .line 340645
    new-instance v0, LX/1uS;

    check-cast p0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p0}, LX/1uS;-><init>(Landroid/widget/LinearLayout$LayoutParams;)V

    .line 340646
    :goto_0
    return-object v0

    .line 340647
    :cond_0
    instance-of v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    .line 340648
    new-instance v0, LX/1uS;

    check-cast p0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p0}, LX/1uS;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    .line 340649
    :cond_1
    new-instance v0, LX/1uS;

    invoke-direct {v0, p0}, LX/1uS;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 340616
    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 340617
    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->c:I

    .line 340618
    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->d:I

    .line 340619
    return-void
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    .line 340650
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-eqz p2, :cond_1

    const/4 v0, 0x4

    :goto_1
    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->f:I

    .line 340651
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->requestLayout()V

    .line 340652
    return-void

    .line 340653
    :cond_0
    const/4 v0, 0x2

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b()LX/1uS;
    .locals 3

    .prologue
    .line 340654
    new-instance v0, LX/1uS;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, LX/1uS;-><init>(II)V

    return-object v0
.end method

.method public static d(Landroid/support/design/widget/AppBarLayout;)Z
    .locals 1

    .prologue
    .line 340655
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Landroid/support/design/widget/AppBarLayout;)V
    .locals 1

    .prologue
    .line 340656
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->f:I

    .line 340657
    return-void
.end method

.method public static getDownNestedPreScrollRange(Landroid/support/design/widget/AppBarLayout;)I
    .locals 8

    .prologue
    .line 340658
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 340659
    iget v1, p0, Landroid/support/design/widget/AppBarLayout;->c:I

    .line 340660
    :goto_0
    return v1

    .line 340661
    :cond_0
    const/4 v1, 0x0

    .line 340662
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_3

    .line 340663
    invoke-virtual {p0, v2}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 340664
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1uS;

    .line 340665
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 340666
    iget v5, v0, LX/1uS;->a:I

    .line 340667
    and-int/lit8 v6, v5, 0x5

    const/4 v7, 0x5

    if-ne v6, v7, :cond_2

    .line 340668
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v6

    add-int/2addr v0, v1

    .line 340669
    and-int/lit8 v1, v5, 0x8

    if-eqz v1, :cond_1

    .line 340670
    invoke-static {v3}, LX/0vv;->u(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    .line 340671
    :goto_2
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 340672
    :cond_1
    add-int/2addr v0, v4

    goto :goto_2

    .line 340673
    :cond_2
    if-gtz v1, :cond_3

    move v0, v1

    goto :goto_2

    .line 340674
    :cond_3
    iput v1, p0, Landroid/support/design/widget/AppBarLayout;->c:I

    goto :goto_0
.end method

.method public static getDownNestedScrollRange(Landroid/support/design/widget/AppBarLayout;)I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 340675
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 340676
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->d:I

    .line 340677
    :goto_0
    return v0

    .line 340678
    :cond_0
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v4

    move v2, v3

    move v1, v3

    :goto_1
    if-ge v2, v4, :cond_2

    .line 340679
    invoke-virtual {p0, v2}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 340680
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1uS;

    .line 340681
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 340682
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v8, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v7, v8

    add-int/2addr v6, v7

    .line 340683
    iget v0, v0, LX/1uS;->a:I

    .line 340684
    and-int/lit8 v7, v0, 0x1

    if-eqz v7, :cond_2

    .line 340685
    add-int/2addr v1, v6

    .line 340686
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 340687
    invoke-static {v5}, LX/0vv;->u(Landroid/view/View;)I

    move-result v0

    invoke-static {p0}, Landroid/support/design/widget/AppBarLayout;->getTopInset(Landroid/support/design/widget/AppBarLayout;)I

    move-result v2

    add-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 340688
    :goto_2
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->d:I

    goto :goto_0

    .line 340689
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public static getPendingAction(Landroid/support/design/widget/AppBarLayout;)I
    .locals 1

    .prologue
    .line 340690
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->f:I

    return v0
.end method

.method public static getTopInset(Landroid/support/design/widget/AppBarLayout;)I
    .locals 1

    .prologue
    .line 340691
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->g:LX/3sc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->g:LX/3sc;

    invoke-virtual {v0}, LX/3sc;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getUpNestedPreScrollRange(Landroid/support/design/widget/AppBarLayout;)I
    .locals 1

    .prologue
    .line 340565
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v0

    return v0
.end method

.method public static setWindowInsets(Landroid/support/design/widget/AppBarLayout;LX/3sc;)V
    .locals 3

    .prologue
    .line 340692
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 340693
    iput-object p1, p0, Landroid/support/design/widget/AppBarLayout;->g:LX/3sc;

    .line 340694
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 340695
    invoke-virtual {p0, v0}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 340696
    invoke-static {v2, p1}, LX/0vv;->b(Landroid/view/View;LX/3sc;)LX/3sc;

    move-result-object p1

    .line 340697
    invoke-virtual {p1}, LX/3sc;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 340698
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 340699
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/3nw;)V
    .locals 1

    .prologue
    .line 340700
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 340701
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340702
    :cond_0
    return-void
.end method

.method public final b(LX/3nw;)V
    .locals 1

    .prologue
    .line 340620
    if-eqz p1, :cond_0

    .line 340621
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 340622
    :cond_0
    return-void
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 340563
    instance-of v0, p1, LX/1uS;

    return v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 340564
    invoke-static {}, Landroid/support/design/widget/AppBarLayout;->b()LX/1uS;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    .prologue
    .line 340566
    invoke-static {}, Landroid/support/design/widget/AppBarLayout;->b()LX/1uS;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 340567
    invoke-direct {p0, p1}, Landroid/support/design/widget/AppBarLayout;->a(Landroid/util/AttributeSet;)LX/1uS;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 340568
    invoke-static {p1}, Landroid/support/design/widget/AppBarLayout;->a(Landroid/view/ViewGroup$LayoutParams;)LX/1uS;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    .prologue
    .line 340569
    invoke-direct {p0, p1}, Landroid/support/design/widget/AppBarLayout;->a(Landroid/util/AttributeSet;)LX/1uS;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    .prologue
    .line 340570
    invoke-static {p1}, Landroid/support/design/widget/AppBarLayout;->a(Landroid/view/ViewGroup$LayoutParams;)LX/1uS;

    move-result-object v0

    return-object v0
.end method

.method public final getMinimumHeightForVisibleOverlappingContent()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 340571
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->g:LX/3sc;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->g:LX/3sc;

    invoke-virtual {v0}, LX/3sc;->b()I

    move-result v0

    .line 340572
    :goto_0
    invoke-static {p0}, LX/0vv;->u(Landroid/view/View;)I

    move-result v2

    .line 340573
    if-eqz v2, :cond_2

    .line 340574
    mul-int/lit8 v1, v2, 0x2

    add-int/2addr v1, v0

    .line 340575
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v1

    .line 340576
    goto :goto_0

    .line 340577
    :cond_2
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v2

    .line 340578
    if-lez v2, :cond_0

    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, LX/0vv;->u(Landroid/view/View;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    goto :goto_1
.end method

.method public getTargetElevation()F
    .locals 1

    .prologue
    .line 340579
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->e:F

    return v0
.end method

.method public final getTotalScrollRange()I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 340580
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 340581
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 340582
    :goto_0
    return v0

    .line 340583
    :cond_0
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v4

    move v2, v3

    move v1, v3

    :goto_1
    if-ge v2, v4, :cond_2

    .line 340584
    invoke-virtual {p0, v2}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 340585
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1uS;

    .line 340586
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 340587
    iget v7, v0, LX/1uS;->a:I

    .line 340588
    and-int/lit8 v8, v7, 0x1

    if-eqz v8, :cond_2

    .line 340589
    iget v8, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v6, v8

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v6

    add-int/2addr v1, v0

    .line 340590
    and-int/lit8 v0, v7, 0x2

    if-eqz v0, :cond_1

    .line 340591
    invoke-static {v5}, LX/0vv;->u(Landroid/view/View;)I

    move-result v0

    sub-int v0, v1, v0

    .line 340592
    :goto_2
    invoke-static {p0}, Landroid/support/design/widget/AppBarLayout;->getTopInset(Landroid/support/design/widget/AppBarLayout;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    goto :goto_0

    .line 340593
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 340594
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 340595
    invoke-direct {p0}, Landroid/support/design/widget/AppBarLayout;->a()V

    .line 340596
    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout;->a:Z

    .line 340597
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 340598
    invoke-virtual {p0, v1}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 340599
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1uS;

    .line 340600
    iget-object p1, v0, LX/1uS;->b:Landroid/view/animation/Interpolator;

    move-object v0, p1

    .line 340601
    if-eqz v0, :cond_1

    .line 340602
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout;->a:Z

    .line 340603
    :cond_0
    return-void

    .line 340604
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 340605
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 340606
    invoke-direct {p0}, Landroid/support/design/widget/AppBarLayout;->a()V

    .line 340607
    return-void
.end method

.method public setExpanded(Z)V
    .locals 1

    .prologue
    .line 340608
    invoke-static {p0}, LX/0vv;->E(Landroid/view/View;)Z

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/AppBarLayout;->a(ZZ)V

    .line 340609
    return-void
.end method

.method public setOrientation(I)V
    .locals 2

    .prologue
    .line 340610
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 340611
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AppBarLayout is always vertical and does not support horizontal orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340612
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 340613
    return-void
.end method

.method public setTargetElevation(F)V
    .locals 0

    .prologue
    .line 340614
    iput p1, p0, Landroid/support/design/widget/AppBarLayout;->e:F

    .line 340615
    return-void
.end method
