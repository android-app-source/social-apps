.class public abstract Lcom/google/common/util/concurrent/AggregateFuture$RunningState;
.super LX/1l4;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1ki;

.field public b:LX/0Py;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Py",
            "<+",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TInputT;>;>;"
        }
    .end annotation
.end field

.field public final c:Z

.field public final d:Z


# direct methods
.method public constructor <init>(LX/1ki;LX/0Py;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Py",
            "<+",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TInputT;>;>;ZZ)V"
        }
    .end annotation

    .prologue
    .line 310963
    iput-object p1, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a:LX/1ki;

    .line 310964
    invoke-virtual {p2}, LX/0Py;->size()I

    move-result v0

    invoke-direct {p0, v0}, LX/1l4;-><init>(I)V

    .line 310965
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Py;

    iput-object v0, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->b:LX/0Py;

    .line 310966
    iput-boolean p3, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->c:Z

    .line 310967
    iput-boolean p4, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->d:Z

    .line 310968
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 311003
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 311004
    iget-boolean v0, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->c:Z

    if-eqz v0, :cond_5

    .line 311005
    iget-object v0, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a:LX/1ki;

    invoke-virtual {v0, p1}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z

    move-result v2

    .line 311006
    if-eqz v2, :cond_1

    .line 311007
    invoke-virtual {p0}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a()V

    move v0, v1

    .line 311008
    :goto_0
    instance-of v4, p1, Ljava/lang/Error;

    iget-boolean v5, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->c:Z

    if-nez v2, :cond_3

    :goto_1
    and-int/2addr v1, v5

    and-int/2addr v0, v1

    or-int/2addr v0, v4

    if-eqz v0, :cond_0

    .line 311009
    instance-of v0, p1, Ljava/lang/Error;

    if-eqz v0, :cond_4

    const-string v0, "Input Future failed with Error"

    .line 311010
    :goto_2
    sget-object v1, LX/1ki;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 311011
    :cond_0
    return-void

    .line 311012
    :cond_1
    iget-object v0, p0, LX/1l4;->c:Ljava/util/Set;

    .line 311013
    if-nez v0, :cond_2

    .line 311014
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    .line 311015
    invoke-virtual {p0, v0}, LX/1l4;->a(Ljava/util/Set;)V

    .line 311016
    sget-object v4, LX/1l4;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    const/4 v5, 0x0

    invoke-virtual {v4, p0, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 311017
    iget-object v0, p0, LX/1l4;->c:Ljava/util/Set;

    .line 311018
    :cond_2
    move-object v0, v0

    .line 311019
    invoke-static {v0, p1}, LX/1ki;->b(Ljava/util/Set;Ljava/lang/Throwable;)Z

    move-result v0

    goto :goto_0

    :cond_3
    move v1, v3

    .line 311020
    goto :goto_1

    .line 311021
    :cond_4
    const-string v0, "Got more than one input Future failure. Logging failures after the first"

    goto :goto_2

    :cond_5
    move v0, v1

    move v2, v3

    goto :goto_0
.end method

.method public static a$redex0(Lcom/google/common/util/concurrent/AggregateFuture$RunningState;ILjava/util/concurrent/Future;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/concurrent/Future",
            "<+TInputT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 310988
    iget-boolean v1, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->c:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a:LX/1ki;

    invoke-virtual {v1}, LX/0SQ;->isDone()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a:LX/1ki;

    invoke-virtual {v1}, LX/0SQ;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    const-string v1, "Future was done before all dependencies completed"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 310989
    :try_start_0
    invoke-interface {p2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    const-string v1, "Tried to set value from future which is not done"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 310990
    iget-boolean v0, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->c:Z

    if-eqz v0, :cond_4

    .line 310991
    invoke-interface {p2}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 310992
    iget-object v0, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a:LX/1ki;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1ki;->a(LX/1ki;Z)Z

    .line 310993
    :cond_2
    :goto_0
    return-void

    .line 310994
    :cond_3
    invoke-static {p2}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    .line 310995
    iget-boolean v1, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->d:Z

    if-eqz v1, :cond_2

    .line 310996
    iget-boolean v1, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->c:Z

    invoke-virtual {p0, v1, p1, v0}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a(ZILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 310997
    :catch_0
    move-exception v0

    .line 310998
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 310999
    :cond_4
    :try_start_1
    iget-boolean v0, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->d:Z

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 311000
    iget-boolean v0, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->c:Z

    invoke-static {p2}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a(ZILjava/lang/Object;)V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 311001
    :catch_1
    move-exception v0

    .line 311002
    invoke-direct {p0, v0}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static g(Lcom/google/common/util/concurrent/AggregateFuture$RunningState;)V
    .locals 4

    .prologue
    .line 310976
    sget-object v0, LX/1l4;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->decrementAndGet(Ljava/lang/Object;)I

    move-result v0

    move v1, v0

    .line 310977
    if-ltz v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Less than 0 remaining futures"

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 310978
    if-nez v1, :cond_2

    .line 310979
    const/4 v0, 0x0

    .line 310980
    iget-boolean v2, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->d:Z

    iget-boolean v1, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->c:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    .line 310981
    iget-object v1, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->b:LX/0Py;

    invoke-virtual {v1}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 310982
    add-int/lit8 v2, v1, 0x1

    invoke-static {p0, v1, v0}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a$redex0(Lcom/google/common/util/concurrent/AggregateFuture$RunningState;ILjava/util/concurrent/Future;)V

    move v1, v2

    .line 310983
    goto :goto_2

    :cond_0
    move v1, v0

    .line 310984
    goto :goto_1

    .line 310985
    :cond_1
    invoke-virtual {p0}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->b()V

    .line 310986
    :cond_2
    return-void

    .line 310987
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 311022
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->b:LX/0Py;

    .line 311023
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 310971
    iget-object v0, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a:LX/1ki;

    invoke-virtual {v0}, LX/0SQ;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 310972
    iget-object v0, p0, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a:LX/1ki;

    .line 310973
    iget-object p0, v0, LX/0SQ;->value:Ljava/lang/Object;

    check-cast p0, LX/0Sc;

    iget-object p0, p0, LX/0Sc;->b:Ljava/lang/Throwable;

    move-object v0, p0

    .line 310974
    invoke-static {p1, v0}, LX/1ki;->b(Ljava/util/Set;Ljava/lang/Throwable;)Z

    .line 310975
    :cond_0
    return-void
.end method

.method public abstract a(ZILjava/lang/Object;)V
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZITInputT;)V"
        }
    .end annotation
.end method

.method public abstract b()V
.end method

.method public final run()V
    .locals 0

    .prologue
    .line 310969
    invoke-static {p0}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->g(Lcom/google/common/util/concurrent/AggregateFuture$RunningState;)V

    .line 310970
    return-void
.end method
