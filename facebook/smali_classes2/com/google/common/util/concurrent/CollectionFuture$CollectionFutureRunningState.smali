.class public abstract Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;
.super Lcom/google/common/util/concurrent/AggregateFuture$RunningState;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ki",
        "<TV;TC;>.RunningState;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1kh;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0am",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1kh;LX/0Py;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Py",
            "<+",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;>;Z)V"
        }
    .end annotation

    .prologue
    .line 310953
    iput-object p1, p0, Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;->b:LX/1kh;

    .line 310954
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;-><init>(LX/1ki;LX/0Py;ZZ)V

    .line 310955
    invoke-virtual {p2}, LX/0Py;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310956
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 310957
    :goto_0
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;->c:Ljava/util/List;

    .line 310958
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p2}, LX/0Py;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 310959
    iget-object v1, p0, Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;->c:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310960
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 310961
    :cond_0
    invoke-virtual {p2}, LX/0Py;->size()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 310962
    :cond_1
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/util/List;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0am",
            "<TV;>;>;)TC;"
        }
    .end annotation
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 310950
    invoke-super {p0}, Lcom/google/common/util/concurrent/AggregateFuture$RunningState;->a()V

    .line 310951
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;->c:Ljava/util/List;

    .line 310952
    return-void
.end method

.method public final a(ZILjava/lang/Object;)V
    .locals 2
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZITV;)V"
        }
    .end annotation

    .prologue
    .line 310945
    iget-object v0, p0, Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;->c:Ljava/util/List;

    .line 310946
    if-eqz v0, :cond_0

    .line 310947
    invoke-static {p3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 310948
    :goto_0
    return-void

    .line 310949
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;->b:LX/1kh;

    invoke-virtual {v0}, LX/0SQ;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Future was done before all dependencies completed"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 310940
    iget-object v0, p0, Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;->c:Ljava/util/List;

    .line 310941
    if-eqz v0, :cond_0

    .line 310942
    iget-object v1, p0, Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;->b:LX/1kh;

    invoke-virtual {p0, v0}, Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0SQ;->set(Ljava/lang/Object;)Z

    .line 310943
    :goto_0
    return-void

    .line 310944
    :cond_0
    iget-object v0, p0, Lcom/google/common/util/concurrent/CollectionFuture$CollectionFutureRunningState;->b:LX/1kh;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    goto :goto_0
.end method
