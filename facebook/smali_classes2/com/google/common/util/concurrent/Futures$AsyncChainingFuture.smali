.class public final Lcom/google/common/util/concurrent/Futures$AsyncChainingFuture;
.super Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture",
        "<TI;TO;",
        "LX/0Vj",
        "<-TI;+TO;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TI;>;",
            "LX/0Vj",
            "<-TI;+TO;>;)V"
        }
    .end annotation

    .prologue
    .line 310865
    invoke-direct {p0, p1, p2}, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Object;)V

    .line 310866
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 310867
    check-cast p1, LX/0Vj;

    .line 310868
    invoke-interface {p1, p2}, LX/0Vj;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 310869
    const-string v1, "AsyncFunction.apply returned null instead of a Future. Did you mean to return immediateFuture(null)?"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310870
    invoke-virtual {p0, v0}, LX/0SQ;->setFuture(Lcom/google/common/util/concurrent/ListenableFuture;)Z

    .line 310871
    return-void
.end method
