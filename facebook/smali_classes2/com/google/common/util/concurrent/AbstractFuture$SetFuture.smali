.class public final Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/0SQ;


# direct methods
.method public constructor <init>(LX/0SQ;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 61511
    iput-object p1, p0, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;->b:LX/0SQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61512
    iput-object p2, p0, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 61513
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 61514
    iget-object v0, p0, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;->b:LX/0SQ;

    iget-object v0, v0, LX/0SQ;->value:Ljava/lang/Object;

    if-eq v0, p0, :cond_0

    .line 61515
    :goto_0
    return-void

    .line 61516
    :cond_0
    iget-object v0, p0, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;->b:LX/0SQ;

    iget-object v1, p0, Lcom/google/common/util/concurrent/AbstractFuture$SetFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0, v1, p0}, LX/0SQ;->completeWithFuture(LX/0SQ;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Object;)Z

    goto :goto_0
.end method
