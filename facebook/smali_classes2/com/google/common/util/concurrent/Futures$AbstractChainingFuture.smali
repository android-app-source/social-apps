.class public abstract Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;
.super LX/0SP;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        "F:",
        "Ljava/lang/Object;",
        ">",
        "LX/0SP",
        "<TO;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field public a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TI;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TF;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TI;>;TF;)V"
        }
    .end annotation

    .prologue
    .line 247924
    invoke-direct {p0}, LX/0SP;-><init>()V

    .line 247925
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v0, p0, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 247926
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;->b:Ljava/lang/Object;

    .line 247927
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;TI;)V"
        }
    .end annotation
.end method

.method public final done()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 247920
    iget-object v0, p0, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-virtual {p0, v0}, LX/0SQ;->maybePropagateCancellation(Ljava/util/concurrent/Future;)V

    .line 247921
    iput-object v1, p0, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 247922
    iput-object v1, p0, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;->b:Ljava/lang/Object;

    .line 247923
    return-void
.end method

.method public final run()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 247904
    :try_start_0
    iget-object v3, p0, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 247905
    iget-object v4, p0, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;->b:Ljava/lang/Object;

    .line 247906
    invoke-virtual {p0}, LX/0SQ;->isCancelled()Z

    move-result v5

    if-nez v3, :cond_0

    move v2, v0

    :goto_0
    or-int/2addr v2, v5

    if-nez v4, :cond_1

    :goto_1
    or-int/2addr v0, v2

    if-eqz v0, :cond_2

    .line 247907
    :goto_2
    return-void

    :cond_0
    move v2, v1

    .line 247908
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 247909
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 247910
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;->b:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 247911
    :try_start_1
    invoke-static {v3}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 247912
    :try_start_2
    invoke-virtual {p0, v4, v0}, Lcom/google/common/util/concurrent/Futures$AbstractChainingFuture;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 247913
    :catch_0
    move-exception v0

    .line 247914
    invoke-virtual {v0}, Ljava/lang/reflect/UndeclaredThrowableException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z

    goto :goto_2

    .line 247915
    :catch_1
    const/4 v0, 0x0

    :try_start_3
    invoke-virtual {p0, v0}, LX/0SQ;->cancel(Z)Z
    :try_end_3
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 247916
    :catch_2
    move-exception v0

    .line 247917
    invoke-virtual {p0, v0}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z

    goto :goto_2

    .line 247918
    :catch_3
    move-exception v0

    .line 247919
    :try_start_4
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z
    :try_end_4
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2
.end method
