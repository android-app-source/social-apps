.class public final Lcom/google/common/util/concurrent/Futures$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:LX/0TF;


# direct methods
.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    .locals 0

    .prologue
    .line 68026
    iput-object p1, p0, Lcom/google/common/util/concurrent/Futures$6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p2, p0, Lcom/google/common/util/concurrent/Futures$6;->b:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 68027
    :try_start_0
    iget-object v0, p0, Lcom/google/common/util/concurrent/Futures$6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 68028
    iget-object v1, p0, Lcom/google/common/util/concurrent/Futures$6;->b:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 68029
    :goto_0
    return-void

    .line 68030
    :catch_0
    move-exception v0

    .line 68031
    iget-object v1, p0, Lcom/google/common/util/concurrent/Futures$6;->b:LX/0TF;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 68032
    :catch_1
    move-exception v0

    .line 68033
    iget-object v1, p0, Lcom/google/common/util/concurrent/Futures$6;->b:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 68034
    :catch_2
    move-exception v0

    .line 68035
    iget-object v1, p0, Lcom/google/common/util/concurrent/Futures$6;->b:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
