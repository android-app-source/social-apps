.class public abstract Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements LX/0fh;
.implements LX/0nz;
.implements LX/0fv;


# static fields
.field private static final q:Ljava/util/regex/Pattern;

.field private static x:Ljava/lang/String;


# instance fields
.field public a:LX/0go;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2l3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2l9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2l5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/14x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/2lE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0id;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2SX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FUf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0fO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:I

.field public o:LX/2ld;

.field public p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public r:Z

.field private s:LX/0fu;

.field private final t:I

.field private u:Lcom/facebook/widget/listview/BetterListView;

.field private v:I

.field private w:I

.field private y:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 137300
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[^?]+)(\\?.*)?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->q:Ljava/util/regex/Pattern;

    .line 137301
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->x:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 137292
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 137293
    iput-boolean v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->r:Z

    .line 137294
    iput v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->v:I

    .line 137295
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->w:I

    .line 137296
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->p:Ljava/util/Map;

    .line 137297
    iput p1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->n:I

    .line 137298
    iput p2, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->t:I

    .line 137299
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;

    invoke-static {p0}, LX/0go;->a(LX/0QB;)LX/0go;

    move-result-object v2

    check-cast v2, LX/0go;

    invoke-static {p0}, LX/2l3;->b(LX/0QB;)LX/2l3;

    move-result-object v3

    check-cast v3, LX/2l3;

    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v5

    check-cast v5, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v6

    check-cast v6, LX/0W9;

    invoke-static {p0}, LX/2l8;->a(LX/0QB;)LX/2l8;

    move-result-object v7

    check-cast v7, LX/2l9;

    invoke-static {p0}, Lcom/facebook/bookmark/client/BookmarkClient;->a(LX/0QB;)Lcom/facebook/bookmark/client/BookmarkClient;

    move-result-object v8

    check-cast v8, LX/2l5;

    invoke-static {p0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v9

    check-cast v9, LX/14x;

    invoke-static {p0}, LX/2lE;->a(LX/0QB;)LX/2lE;

    move-result-object v10

    check-cast v10, LX/2lE;

    invoke-static {p0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v11

    check-cast v11, LX/0id;

    invoke-static {p0}, LX/2SX;->b(LX/0QB;)LX/2SX;

    move-result-object v12

    check-cast v12, LX/2SX;

    const/16 v13, 0x302e

    invoke-static {p0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {p0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object p0

    check-cast p0, LX/0fO;

    iput-object v2, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->a:LX/0go;

    iput-object v3, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->b:LX/2l3;

    iput-object v4, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->c:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object v5, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->d:Lcom/facebook/performancelogger/PerformanceLogger;

    iput-object v6, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->e:LX/0W9;

    iput-object v7, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->f:LX/2l9;

    iput-object v8, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->g:LX/2l5;

    iput-object v9, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->h:LX/14x;

    iput-object v10, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->i:LX/2lE;

    iput-object v11, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->j:LX/0id;

    iput-object v12, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->k:LX/2SX;

    iput-object v13, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->l:LX/0Ot;

    iput-object p0, v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->m:LX/0fO;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137291
    sget-object v0, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    .line 137275
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 137276
    const-class v0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;

    invoke-static {v0, p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 137277
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 137278
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->a:LX/0go;

    invoke-virtual {v0}, LX/0go;->a()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v0

    iget-object v3, v0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    .line 137279
    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    const-string v6, "_tab"

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 137280
    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 137281
    const-string v6, "/"

    invoke-virtual {v0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 137282
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "/"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 137283
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 137284
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->y:LX/0Rf;

    .line 137285
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->d:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    const-string v3, "load_type"

    iget-object v4, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->g:LX/2l5;

    invoke-interface {v4}, LX/2l5;->d()LX/0ta;

    move-result-object v4

    sget-object v6, LX/0ta;->NO_DATA:LX/0ta;

    if-ne v4, v6, :cond_2

    const-string v4, "cold"

    :goto_1
    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 137286
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->d:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    const-string v3, "load_type"

    iget-object v4, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->g:LX/2l5;

    invoke-interface {v4}, LX/2l5;->d()LX/0ta;

    move-result-object v4

    sget-object v6, LX/0ta;->NO_DATA:LX/0ta;

    if-ne v4, v6, :cond_3

    const-string v4, "cold"

    :goto_2
    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 137287
    new-instance v0, LX/2lF;

    invoke-direct {v0, p0}, LX/2lF;-><init>(Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;)V

    iput-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->s:LX/0fu;

    .line 137288
    return-void

    .line 137289
    :cond_2
    const-string v4, "warm"

    goto :goto_1

    .line 137290
    :cond_3
    const-string v4, "warm"

    goto :goto_2
.end method

.method public final a(Lcom/facebook/bookmark/model/Bookmark;)V
    .locals 4

    .prologue
    .line 137125
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->p:Ljava/util/Map;

    iget-wide v2, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137126
    :goto_0
    return-void

    .line 137127
    :cond_0
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->p:Ljava/util/Map;

    iget-wide v2, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 137128
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->o:LX/2ld;

    invoke-virtual {v1, v0}, LX/2ld;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lH;

    .line 137129
    iget-object v1, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 137130
    instance-of v1, v1, Lcom/facebook/bookmark/model/Bookmark;

    if-nez v1, :cond_1

    .line 137131
    const-class v0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;

    const-string v1, "could not update unread count in the list view because the position went wrong!"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 137132
    :cond_1
    iget-object v1, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 137133
    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    .line 137134
    invoke-virtual {p1}, Lcom/facebook/bookmark/model/Bookmark;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/bookmark/model/Bookmark;->a(I)V

    .line 137135
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->o:LX/2ld;

    const v1, -0x3bc708e

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/bookmark/model/Bookmark;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 137261
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->j:LX/0id;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->a(Ljava/lang/String;)V

    .line 137262
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->f:LX/2l9;

    invoke-static {}, LX/EhY;->newBuilder()LX/EhZ;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 137263
    iput-object v0, v2, LX/EhZ;->a:Landroid/app/Activity;

    .line 137264
    move-object v0, v2

    .line 137265
    iput-object p1, v0, LX/EhZ;->b:Lcom/facebook/bookmark/model/Bookmark;

    .line 137266
    move-object v0, v0

    .line 137267
    if-eqz p2, :cond_1

    .line 137268
    :goto_0
    iput-object p2, v0, LX/EhZ;->c:Ljava/lang/String;

    .line 137269
    move-object v0, v0

    .line 137270
    invoke-virtual {v0}, LX/EhZ;->a()LX/EhY;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2l9;->a(LX/EhY;)Z

    move-result v0

    .line 137271
    if-nez v0, :cond_0

    .line 137272
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->j:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    .line 137273
    :cond_0
    return-void

    .line 137274
    :cond_1
    iget-object p2, p1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lcom/facebook/bookmark/model/BookmarksGroup;)V
    .locals 0

    .prologue
    .line 137260
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2lI;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137217
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137218
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137219
    :goto_0
    return-void

    .line 137220
    :cond_0
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->k:LX/2SX;

    .line 137221
    invoke-static {v0}, LX/2SX;->d(LX/2SX;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 137222
    const/4 v1, 0x0

    .line 137223
    :goto_1
    move v0, v1

    .line 137224
    if-nez v0, :cond_4

    .line 137225
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 137226
    const/4 v6, 0x0

    move v7, v6

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v7, v6, :cond_2

    .line 137227
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/2lI;

    invoke-interface {v6}, LX/2lI;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v6

    .line 137228
    if-eqz v6, :cond_1

    iget-wide v10, v6, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v12, 0xc45a72969193L

    cmp-long v6, v10, v12

    if-nez v6, :cond_1

    .line 137229
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137230
    :cond_1
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_2

    .line 137231
    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move v7, v6

    :goto_3
    if-ltz v7, :cond_3

    .line 137232
    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {p1, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 137233
    add-int/lit8 v6, v7, -0x1

    move v7, v6

    goto :goto_3

    .line 137234
    :cond_3
    move-object p1, p1

    .line 137235
    :cond_4
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 137236
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lI;

    invoke-interface {v0}, LX/2lI;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    .line 137237
    if-eqz v0, :cond_5

    .line 137238
    iget-object v2, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->p:Ljava/util/Map;

    iget-wide v4, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137239
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 137240
    :cond_6
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->o:LX/2ld;

    .line 137241
    iget-object v1, v0, LX/2ld;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 137242
    iget-object v2, v0, LX/2ld;->a:Ljava/util/List;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 137243
    const v1, 0x1a978c7b

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 137244
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->b:LX/2l3;

    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->o:LX/2ld;

    .line 137245
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 137246
    invoke-virtual {v1}, LX/2ld;->getCount()I

    move-result v5

    .line 137247
    const/4 v2, 0x0

    move v3, v2

    :goto_5
    if-ge v3, v5, :cond_9

    .line 137248
    iget-object v2, v1, LX/2ld;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2lI;

    .line 137249
    invoke-interface {v2}, LX/2lI;->a()LX/2lb;

    move-result-object v6

    sget-object v7, LX/2la;->Bookmark:LX/2la;

    if-eq v6, v7, :cond_7

    invoke-interface {v2}, LX/2lI;->a()LX/2lb;

    move-result-object v6

    sget-object v7, LX/2la;->FamilyBridgesProfile:LX/2la;

    if-ne v6, v7, :cond_8

    const-string v6, "app"

    invoke-interface {v2}, LX/2lI;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v7

    iget-object v7, v7, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 137250
    :cond_7
    invoke-interface {v2}, LX/2lI;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v2

    .line 137251
    if-eqz v2, :cond_8

    .line 137252
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137253
    :cond_8
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    .line 137254
    :cond_9
    move-object v1, v4

    .line 137255
    iget-object v6, v0, LX/2l3;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/2l3;->a:LX/0Tn;

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 137256
    iget-object v8, v0, LX/2l3;->f:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    .line 137257
    sub-long v6, v8, v6

    const-wide/32 v8, 0x2932e00

    cmp-long v6, v6, v8

    if-gez v6, :cond_b

    .line 137258
    :goto_6
    goto/16 :goto_0

    :cond_a
    iget-object v1, v0, LX/2SX;->b:LX/0ad;

    sget-short v2, LX/FUh;->a:S

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    goto/16 :goto_1

    .line 137259
    :cond_b
    iget-object v6, v0, LX/2l3;->c:LX/0Zb;

    new-instance v7, Lcom/facebook/bookmark/ui/analytics/BookmarkImpressionEvent;

    iget-object v8, v0, LX/2l3;->d:LX/2l4;

    invoke-direct {v7, v8, v1}, Lcom/facebook/bookmark/ui/analytics/BookmarkImpressionEvent;-><init>(LX/2l4;Ljava/util/List;)V

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_6
.end method

.method public abstract b()I
.end method

.method public final b(Lcom/facebook/bookmark/model/Bookmark;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 137209
    iget-wide v2, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v4, 0x268a3ef6875cfL

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 137210
    :cond_0
    :goto_0
    return v0

    .line 137211
    :cond_1
    iget-wide v2, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v4, 0xc63f291b142bL

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v0, v1

    .line 137212
    goto :goto_0

    .line 137213
    :cond_2
    iget-wide v2, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v4, 0xdeef707893a1L

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->m:LX/0fO;

    invoke-virtual {v2}, LX/0fO;->p()Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 137214
    goto :goto_0

    .line 137215
    :cond_3
    sget-object v2, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->q:Ljava/util/regex/Pattern;

    iget-object v3, p1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 137216
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->y:LX/0Rf;

    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide v2, 0x229cf4f51d6aaL

    iget-wide v4, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public abstract c()V
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 137102
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_0

    .line 137103
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->smoothScrollToPosition(I)V

    .line 137104
    :cond_0
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 137105
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()LX/0g8;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 137106
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2iI;

    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v0, v1}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v0, 0x2a

    const v1, -0x7ac1d237

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137107
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 137108
    new-instance v1, LX/2ld;

    invoke-virtual {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->b()I

    move-result v2

    invoke-direct {v1, v2}, LX/2ld;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->o:LX/2ld;

    .line 137109
    invoke-virtual {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->c()V

    .line 137110
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v7}, Lcom/facebook/widget/listview/BetterListView;->setEnabled(Z)V

    .line 137111
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 137112
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 137113
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->o:LX/2ld;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 137114
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v4, LX/2lf;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->g:LX/2l5;

    invoke-direct {v4, v5, v6, v7, p0}, LX/2lf;-><init>(Landroid/content/Context;LX/2l5;ZLX/0nz;)V

    invoke-virtual {v1, v2, v3, v4}, LX/0k5;->a(ILandroid/os/Bundle;LX/1jv;)LX/0k9;

    .line 137115
    const/16 v1, 0x2b

    const v2, -0x4776cfd8

    invoke-static {v8, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3bb16e03

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 137116
    iget v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->n:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 137117
    const-string v3, "sidebar_menu"

    invoke-static {v2, v3, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 137118
    iget v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->t:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    .line 137119
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->s:LX/0fu;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->a(LX/0fu;)V

    .line 137120
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 137121
    const/16 v0, 0x2b

    const v3, -0x29caa05e

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x70f540e8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137122
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 137123
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    .line 137124
    const/16 v1, 0x2b

    const v2, 0x773a35c0    # 3.7767867E33f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 137136
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_1

    .line 137137
    :cond_0
    :goto_0
    return-void

    .line 137138
    :cond_1
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/listview/BetterListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    .line 137139
    instance-of v1, v0, LX/2lH;

    if-eqz v1, :cond_0

    .line 137140
    check-cast v0, LX/2lH;

    .line 137141
    iget-object v1, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 137142
    instance-of v1, v1, Lcom/facebook/bookmark/model/Bookmark;

    if-eqz v1, :cond_0

    .line 137143
    invoke-virtual {v0}, LX/2lH;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v3

    .line 137144
    iget-object v1, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 137145
    check-cast v1, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v1, v1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    .line 137146
    const-wide v4, 0xc63f291b142bL

    iget-wide v6, v3, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v2, v4, v6

    if-nez v2, :cond_2

    .line 137147
    iget-object v2, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->i:LX/2lE;

    sget-object v4, LX/3zK;->BOOKMARKS:LX/3zK;

    invoke-virtual {v2, v4}, LX/2lE;->a(LX/3zK;)V

    .line 137148
    iget-object v2, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->h:LX/14x;

    invoke-virtual {v2}, LX/14x;->d()Z

    move-result v2

    if-nez v2, :cond_2

    .line 137149
    const-string v1, "https://m.facebook.com/messages/"

    .line 137150
    :cond_2
    const-wide v4, 0xd16b5fe18c70L

    iget-wide v6, v3, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v2, v4, v6

    if-nez v2, :cond_3

    .line 137151
    sget-object v1, LX/0ax;->O:Ljava/lang/String;

    const-string v2, "bookmarks"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 137152
    :cond_3
    const-wide v4, 0x37ea85788e65bL

    iget-wide v6, v3, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v2, v4, v6

    if-nez v2, :cond_4

    .line 137153
    sget-object v1, LX/0ax;->H:Ljava/lang/String;

    const-string v2, "bookmarks"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 137154
    :cond_4
    const-wide v4, 0x1df0f89143217L

    iget-wide v6, v3, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v2, v4, v6

    if-nez v2, :cond_5

    .line 137155
    sget-object v1, LX/0ax;->hE:Ljava/lang/String;

    .line 137156
    :cond_5
    const-wide v4, 0xab8c511ca3acL

    iget-wide v6, v3, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v2, v4, v6

    if-nez v2, :cond_b

    .line 137157
    sget-object v1, LX/0ax;->bi:Ljava/lang/String;

    move-object v2, v1

    .line 137158
    :goto_1
    const-wide v4, 0xc45a72969193L

    iget-wide v6, v3, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_6

    .line 137159
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FUf;

    sget-object v4, LX/FUk;->BOOKMARK:LX/FUk;

    invoke-virtual {v4}, LX/FUk;->name()Ljava/lang/String;

    move-result-object v4

    const-string v5, "START_INITIAL"

    invoke-virtual {v1, v4, v5}, LX/FUf;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 137160
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FUf;

    .line 137161
    iget-object v4, v1, LX/FUf;->a:LX/0if;

    sget-object v5, LX/0ig;->H:LX/0ih;

    const-string v6, "QR_BOOKMARK_CLICKED"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 137162
    :cond_6
    const-wide v4, 0x24acc4f2e22e4L

    iget-wide v6, v3, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_7

    .line 137163
    sget-object v1, LX/0ax;->bQ:Ljava/lang/String;

    const-string v2, "BOOKMARK"

    const-string v4, "645191315628772"

    invoke-static {v1, v2, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 137164
    :cond_7
    const-wide v4, 0xa5480a7d24faL

    iget-wide v6, v3, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_8

    .line 137165
    sget-object v1, LX/0ax;->fW:Ljava/lang/String;

    const-string v2, "0"

    const-string v4, "bookmarks"

    invoke-static {v1, v2, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 137166
    :cond_8
    const-wide v4, 0x5fbf587aaafd9L

    iget-wide v6, v3, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_9

    .line 137167
    sget-object v2, LX/0ax;->hK:Ljava/lang/String;

    .line 137168
    :cond_9
    const-wide v4, 0x378ae22b932d7L

    iget-wide v6, v3, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_a

    .line 137169
    sget-object v2, LX/0ax;->iT:Ljava/lang/String;

    .line 137170
    :cond_a
    iget-object v1, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 137171
    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->a(Lcom/facebook/bookmark/model/Bookmark;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    move-object v2, v1

    goto/16 :goto_1
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 137172
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/listview/BetterListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    .line 137173
    instance-of v2, v0, LX/2lH;

    if-nez v2, :cond_0

    move v0, v1

    .line 137174
    :goto_0
    return v0

    .line 137175
    :cond_0
    check-cast v0, LX/2lH;

    .line 137176
    iget-object v2, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 137177
    instance-of v2, v2, Lcom/facebook/bookmark/model/Bookmark;

    if-eqz v2, :cond_1

    .line 137178
    invoke-virtual {v0}, LX/2lH;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    .line 137179
    invoke-virtual {v0}, LX/2lH;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v2

    iget-wide v2, v2, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v4, 0xc63f291b142bL

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 137180
    iget-object v2, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->i:LX/2lE;

    sget-object v3, LX/3zK;->BOOKMARKS:LX/3zK;

    invoke-virtual {v2, v3}, LX/2lE;->a(LX/3zK;)V

    .line 137181
    iget-object v2, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->h:LX/14x;

    invoke-virtual {v2}, LX/14x;->d()Z

    move-result v2

    if-nez v2, :cond_2

    .line 137182
    const-string v1, "https://m.facebook.com/messages/"

    move-object v2, v1

    .line 137183
    :goto_1
    iget-object v3, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->f:LX/2l9;

    invoke-static {}, LX/EhY;->newBuilder()LX/EhZ;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 137184
    iput-object v1, v4, LX/EhZ;->a:Landroid/app/Activity;

    .line 137185
    move-object v1, v4

    .line 137186
    iget-object v4, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v0, v4

    .line 137187
    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    .line 137188
    iput-object v0, v1, LX/EhZ;->b:Lcom/facebook/bookmark/model/Bookmark;

    .line 137189
    move-object v0, v1

    .line 137190
    const/4 v1, 0x1

    .line 137191
    iput-boolean v1, v0, LX/EhZ;->d:Z

    .line 137192
    move-object v0, v0

    .line 137193
    iput-object v2, v0, LX/EhZ;->c:Ljava/lang/String;

    .line 137194
    move-object v0, v0

    .line 137195
    invoke-virtual {v0}, LX/EhZ;->a()LX/EhY;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/2l9;->a(LX/EhY;)Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 137196
    goto :goto_0

    :cond_2
    move-object v2, v1

    goto :goto_1
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x769e13a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137197
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 137198
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->e:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v1

    .line 137199
    sget-object v2, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->x:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 137200
    sput-object v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->x:Ljava/lang/String;

    .line 137201
    :cond_0
    sget-object v2, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->x:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 137202
    sput-object v1, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->x:Ljava/lang/String;

    .line 137203
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->g:LX/2l5;

    invoke-interface {v1}, LX/2l5;->a()V

    .line 137204
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x6e8cc093

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 137205
    iput p2, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->v:I

    .line 137206
    iput p3, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->w:I

    .line 137207
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 137208
    return-void
.end method
