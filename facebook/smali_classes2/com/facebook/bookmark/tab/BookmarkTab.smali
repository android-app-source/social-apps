.class public Lcom/facebook/bookmark/tab/BookmarkTab;
.super Lcom/facebook/apptab/state/TabTag;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/bookmark/tab/BookmarkTab;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/facebook/bookmark/tab/BookmarkTab;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163354
    new-instance v0, Lcom/facebook/bookmark/tab/BookmarkTab;

    invoke-direct {v0}, Lcom/facebook/bookmark/tab/BookmarkTab;-><init>()V

    sput-object v0, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    .line 163355
    new-instance v0, LX/0xe;

    invoke-direct {v0}, LX/0xe;-><init>()V

    sput-object v0, Lcom/facebook/bookmark/tab/BookmarkTab;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    .prologue
    .line 163356
    sget-object v1, LX/0ax;->cj:Ljava/lang/String;

    sget-object v2, LX/0cQ;->BOOKMARKS_FRAGMENT:LX/0cQ;

    const v3, 0x7f021078

    const/4 v4, 0x0

    const-string v5, "bookmarks"

    const v6, 0x63000d

    const v7, 0x63000c

    const-string v8, "LoadTab_Bookmark"

    const-string v9, "LoadTab_Bookmark_NoAnim"

    const v10, 0x7f080a2e

    const v11, 0x7f0d0037

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, Lcom/facebook/apptab/state/TabTag;-><init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V

    .line 163357
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163358
    const-string v0, "Bookmark"

    return-object v0
.end method
