.class public Lcom/facebook/bookmark/provider/BookmarkProvider;
.super LX/0Ov;
.source ""


# static fields
.field private static final a:Landroid/content/UriMatcher;


# instance fields
.field private b:LX/2ma;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 54525
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 54526
    sput-object v0, Lcom/facebook/bookmark/provider/BookmarkProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, LX/0Ow;->a:Ljava/lang/String;

    const-string v2, "bookmarks"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 54527
    sget-object v0, Lcom/facebook/bookmark/provider/BookmarkProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, LX/0Ow;->a:Ljava/lang/String;

    const-string v2, "BookmarkUnreadCount"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 54528
    sget-object v0, Lcom/facebook/bookmark/provider/BookmarkProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, LX/0Ow;->a:Ljava/lang/String;

    const-string v2, "bookmark_sync_status"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 54529
    sget-object v0, Lcom/facebook/bookmark/provider/BookmarkProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, LX/0Ow;->a:Ljava/lang/String;

    const-string v2, "bookmark_group"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 54530
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54531
    invoke-direct {p0}, LX/0Ov;-><init>()V

    return-void
.end method

.method private static c(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 54472
    sget-object v0, Lcom/facebook/bookmark/provider/BookmarkProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 54473
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The uri "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid for bookmark content provider."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54474
    :pswitch_0
    const-string v0, "bookmarks"

    .line 54475
    :goto_0
    return-object v0

    .line 54476
    :pswitch_1
    const-string v0, "bookmarks"

    goto :goto_0

    .line 54477
    :pswitch_2
    const-string v0, "bookmark_sync_status"

    goto :goto_0

    .line 54478
    :pswitch_3
    const-string v0, "bookmark_group"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54522
    iget-object v0, p0, Lcom/facebook/bookmark/provider/BookmarkProvider;->b:LX/2ma;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 54523
    invoke-static {p1}, Lcom/facebook/bookmark/provider/BookmarkProvider;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 54524
    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54512
    sget-object v0, Lcom/facebook/bookmark/provider/BookmarkProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 54513
    sget-object v0, LX/0Ow;->b:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54514
    iget-object v0, p0, Lcom/facebook/bookmark/provider/BookmarkProvider;->b:LX/2ma;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 54515
    const/4 v0, 0x0

    .line 54516
    :goto_0
    return v0

    .line 54517
    :pswitch_0
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Could only delete record from sync_status table."

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54518
    :pswitch_1
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is only used for notification and updating."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54519
    :cond_0
    iget-object v0, p0, Lcom/facebook/bookmark/provider/BookmarkProvider;->b:LX/2ma;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 54520
    invoke-static {p1}, Lcom/facebook/bookmark/provider/BookmarkProvider;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 54521
    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 54532
    sget-object v1, Lcom/facebook/bookmark/provider/BookmarkProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 54533
    iget-object v1, p0, Lcom/facebook/bookmark/provider/BookmarkProvider;->b:LX/2ma;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 54534
    invoke-static {p1}, Lcom/facebook/bookmark/provider/BookmarkProvider;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 54535
    const v1, 0x425a6f1a

    invoke-static {v2, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 54536
    :try_start_0
    array-length v4, p2

    move v1, v0

    .line 54537
    :goto_0
    if-ge v1, v4, :cond_1

    .line 54538
    const/4 v5, 0x0

    aget-object v6, p2, v1

    const v7, 0x267c2675

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v2, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    const v5, 0x5b18d403

    invoke-static {v5}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gez v5, :cond_0

    .line 54539
    const v1, -0x1461bee4

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 54540
    :goto_1
    return v0

    .line 54541
    :pswitch_0
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Could only insert records into sync_status table."

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54542
    :pswitch_1
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is only used for notification and updating."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54543
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54544
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 54545
    const v0, -0x16781b2a

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 54546
    array-length v0, p2

    goto :goto_1

    .line 54547
    :catchall_0
    move-exception v0

    const v1, 0x659cd52c

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 54506
    sget-object v0, Lcom/facebook/bookmark/provider/BookmarkProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 54507
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is only used for notification and updating."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54508
    :cond_0
    iget-object v0, p0, Lcom/facebook/bookmark/provider/BookmarkProvider;->b:LX/2ma;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 54509
    invoke-static {p1}, Lcom/facebook/bookmark/provider/BookmarkProvider;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    .line 54510
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 54511
    return-object v0
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 54497
    sget-object v0, Lcom/facebook/bookmark/provider/BookmarkProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 54498
    iget-object v0, p0, Lcom/facebook/bookmark/provider/BookmarkProvider;->b:LX/2ma;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 54499
    invoke-static {p1}, Lcom/facebook/bookmark/provider/BookmarkProvider;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 54500
    const/4 v2, 0x0

    const v3, -0x67c40db5

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {v0, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const v2, 0x1d1f5c97

    invoke-static {v2}, LX/03h;->a(I)V

    .line 54501
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 54502
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to insert row into "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54503
    :pswitch_0
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Could only insert record into sync_status table."

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54504
    :pswitch_1
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is only used for notification and updating."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54505
    :cond_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 54490
    sget-object v0, Lcom/facebook/bookmark/provider/BookmarkProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 54491
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The uri "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid for bookmark content provider."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54492
    :pswitch_0
    const-string v0, "vnd.android.cursor.item/vnd.facebook.katana.bookmark"

    .line 54493
    :goto_0
    return-object v0

    .line 54494
    :pswitch_1
    const-string v0, "vnd.android.cursor.item/vnd.facebook.katana.bookmark_unread_count"

    goto :goto_0

    .line 54495
    :pswitch_2
    const-string v0, "vnd.android.cursor.item/vnd.facebook.katana.bookmark_sync_status"

    goto :goto_0

    .line 54496
    :pswitch_3
    const-string v0, "vnd.android.cursor.item/vnd.facebook.katana.bookmark_group_order"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 54486
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/bookmark/provider/BookmarkProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 54487
    invoke-static {v0}, LX/2ma;->a(LX/0QB;)LX/2ma;

    move-result-object v0

    check-cast v0, LX/2ma;

    iput-object v0, p0, Lcom/facebook/bookmark/provider/BookmarkProvider;->b:LX/2ma;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54488
    monitor-exit p0

    return-void

    .line 54489
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .prologue
    .line 54479
    iget-object v0, p0, Lcom/facebook/bookmark/provider/BookmarkProvider;->b:LX/2ma;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 54480
    const v0, 0x4f359a00    # 3.04676864E9f

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 54481
    :try_start_0
    invoke-super {p0, p1}, LX/0Ov;->a(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v0

    .line 54482
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54483
    const v2, -0x48d7972c

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 54484
    return-object v0

    .line 54485
    :catchall_0
    move-exception v0

    const v2, 0x257ff675

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
