.class public Lcom/facebook/mobileconfig/init/MobileConfigInit;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile o:Lcom/facebook/mobileconfig/init/MobileConfigInit;


# instance fields
.field private final b:LX/0Uh;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/29a;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/contextual/ContextualResolver;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91884
    const-class v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;

    sput-object v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .param p7    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .param p12    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/29a;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/contextual/ContextualResolver;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91870
    iput-object p1, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->b:LX/0Uh;

    .line 91871
    iput-object p2, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->c:LX/0Ot;

    .line 91872
    iput-object p3, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->d:LX/0Ot;

    .line 91873
    iput-object p4, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->e:LX/0Ot;

    .line 91874
    iput-object p5, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->f:LX/0Ot;

    .line 91875
    iput-object p6, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->g:LX/0Ot;

    .line 91876
    iput-object p7, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->h:LX/0Ot;

    .line 91877
    iput-object p8, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->i:LX/0Or;

    .line 91878
    iput-object p9, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->j:LX/0Or;

    .line 91879
    iput-object p10, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->k:LX/0Ot;

    .line 91880
    iput-object p11, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->m:LX/0Ot;

    .line 91881
    iput-object p12, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->n:LX/0Ot;

    .line 91882
    iput-object p13, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->l:LX/0Ot;

    .line 91883
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/mobileconfig/init/MobileConfigInit;
    .locals 3

    .prologue
    .line 91859
    sget-object v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->o:Lcom/facebook/mobileconfig/init/MobileConfigInit;

    if-nez v0, :cond_1

    .line 91860
    const-class v1, Lcom/facebook/mobileconfig/init/MobileConfigInit;

    monitor-enter v1

    .line 91861
    :try_start_0
    sget-object v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->o:Lcom/facebook/mobileconfig/init/MobileConfigInit;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 91862
    if-eqz v2, :cond_0

    .line 91863
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->b(LX/0QB;)Lcom/facebook/mobileconfig/init/MobileConfigInit;

    move-result-object v0

    sput-object v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->o:Lcom/facebook/mobileconfig/init/MobileConfigInit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91864
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 91865
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 91866
    :cond_1
    sget-object v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->o:Lcom/facebook/mobileconfig/init/MobileConfigInit;

    return-object v0

    .line 91867
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 91868
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Ww;)Z
    .locals 1

    .prologue
    .line 91852
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, LX/0Ww;->d()LX/0Wx;

    move-result-object v0

    .line 91853
    instance-of v0, v0, LX/0eR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 91854
    const/4 v0, 0x0

    .line 91855
    :goto_0
    monitor-exit p0

    return v0

    .line 91856
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/0Wx;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Ww;->a(LX/0Wx;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91857
    const/4 v0, 0x1

    goto :goto_0

    .line 91858
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/mobileconfig/init/MobileConfigInit;
    .locals 14

    .prologue
    .line 91850
    new-instance v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x19e

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x3e5

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x4cd

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x13b7

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x29d

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xdf3

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0xdf4

    invoke-static {p0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x259

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2be

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x15ec

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x45e

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, Lcom/facebook/mobileconfig/init/MobileConfigInit;-><init>(LX/0Uh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 91851
    return-object v0
.end method

.method private declared-synchronized b()V
    .locals 6

    .prologue
    .line 91832
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    .line 91833
    iget-object v1, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0, v1}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->c(Lcom/facebook/mobileconfig/init/MobileConfigInit;Lcom/facebook/auth/viewercontext/ViewerContext;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91834
    invoke-static {p0}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->c(Lcom/facebook/mobileconfig/init/MobileConfigInit;)LX/0Wx;

    move-result-object v2

    .line 91835
    if-nez v2, :cond_2

    .line 91836
    iget-object v1, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {p0, v1}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/0Wx;

    move-result-object v1

    move-object v2, v1

    .line 91837
    :goto_0
    iget-object v1, v0, LX/0W3;->a:LX/0Wx;

    move-object v1, v1

    .line 91838
    check-cast v1, LX/0Ww;

    invoke-virtual {v1, v2}, LX/0Ww;->a(LX/0Wx;)V

    .line 91839
    invoke-virtual {v0}, LX/0W3;->c()V

    .line 91840
    :cond_0
    iget-object v1, v0, LX/0W3;->a:LX/0Wx;

    move-object v0, v1

    .line 91841
    invoke-interface {v0}, LX/0Wx;->isValid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91842
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 91843
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 91844
    iget-object v3, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Wd;

    new-instance v4, Lcom/facebook/mobileconfig/init/MobileConfigInit$1;

    invoke-direct {v4, p0, v1}, Lcom/facebook/mobileconfig/init/MobileConfigInit$1;-><init>(Lcom/facebook/mobileconfig/init/MobileConfigInit;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    const v5, 0x4ea689ca    # 1.397024E9f

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91845
    goto :goto_0

    .line 91846
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 91847
    :try_start_2
    instance-of v0, v1, Ljava/io/IOException;

    if-nez v0, :cond_1

    .line 91848
    iget-object v0, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 91849
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static c(Lcom/facebook/mobileconfig/init/MobileConfigInit;)LX/0Wx;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 91885
    iget-object v0, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 91886
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, p0

    .line 91887
    new-instance v2, LX/0eR;

    invoke-direct {v2, v1, v0}, LX/0eR;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 91888
    invoke-static {v2}, LX/0eR;->h(LX/0eR;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 91889
    new-instance p0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v2, LX/0eR;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, "overrides.json"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 91890
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result p0

    move p0, p0

    .line 91891
    if-eqz p0, :cond_1

    :cond_0
    const/4 v2, 0x0

    :cond_1
    move-object v0, v2

    .line 91892
    return-object v0
.end method

.method private static c(Lcom/facebook/mobileconfig/init/MobileConfigInit;Lcom/facebook/auth/viewercontext/ViewerContext;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 91820
    iget-object v0, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->b:LX/0Uh;

    const/16 v2, 0xd

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 91821
    if-nez v0, :cond_0

    move v0, v1

    .line 91822
    :goto_0
    return v0

    .line 91823
    :cond_0
    iget-object v0, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 91824
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/facebook/mobileconfig/init/MobileConfigEnableReceiver;

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91825
    const/4 v3, 0x2

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v0

    if-ne v3, v0, :cond_1

    move v0, v1

    .line 91826
    goto :goto_0

    .line 91827
    :cond_1
    if-eqz p1, :cond_2

    .line 91828
    iget-object v0, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v0

    .line 91829
    if-eqz v0, :cond_2

    .line 91830
    iget-object v0, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v0

    .line 91831
    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/0Wx;
    .locals 17
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 91806
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->b:LX/0Uh;

    const/16 v3, 0x45

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 91807
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->b:LX/0Uh;

    const/16 v4, 0x47

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 91808
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->b:LX/0Uh;

    const/16 v5, 0x46

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    .line 91809
    new-instance v15, Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;

    invoke-direct {v15}, Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;-><init>()V

    .line 91810
    invoke-virtual {v15, v2}, Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;->setConsistencyLoggingEnabled(Z)V

    .line 91811
    const-wide/32 v6, 0x278d00

    invoke-virtual {v15, v6, v7}, Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;->setConsistencyLoggingEveryNSec(J)V

    .line 91812
    invoke-virtual {v15, v3}, Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;->setOmnistoreUpdaterExpected(Z)V

    .line 91813
    if-nez v4, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v15, v2}, Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;->setShadowAlternativeUpdater(Z)V

    .line 91814
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/content/Context;

    move-object v3, v0

    .line 91815
    invoke-static {v3}, LX/2Yd;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    .line 91816
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0W9;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->n:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v2, v4}, LX/2Ye;->a(LX/0W9;Ljava/lang/String;)LX/0P1;

    move-result-object v16

    .line 91817
    new-instance v2, Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0WV;

    invoke-virtual {v5}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0dC;

    invoke-virtual {v6}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->g:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/29a;

    invoke-virtual {v9}, LX/29a;->a()Lcom/facebook/xanalytics/XAnalyticsNative;

    move-result-object v9

    const-string v10, "foreground"

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x64

    const/4 v14, 0x0

    invoke-direct/range {v2 .. v16}, Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;-><init>(Ljava/io/File;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/tigon/iface/TigonServiceHolder;ZLcom/facebook/xanalytics/XAnalyticsHolder;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/facebook/mobileconfig/MobileConfigParamsMapHolder;Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v2

    .line 91818
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 91819
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 91797
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    .line 91798
    iget-object v1, v0, LX/0W3;->a:LX/0Wx;

    move-object v1, v1

    .line 91799
    instance-of v1, v1, LX/0Ww;

    if-eqz v1, :cond_0

    .line 91800
    iget-object v1, v0, LX/0W3;->a:LX/0Wx;

    move-object v0, v1

    .line 91801
    check-cast v0, LX/0Ww;

    .line 91802
    new-instance v1, LX/0Wy;

    invoke-direct {v1}, LX/0Wy;-><init>()V

    invoke-virtual {v0, v1}, LX/0Ww;->a(LX/0Wx;)V

    .line 91803
    iget-object v0, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0uf;

    invoke-virtual {v0}, LX/0uf;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91804
    :cond_0
    monitor-exit p0

    return-void

    .line 91805
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V
    .locals 2

    .prologue
    .line 91784
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->c(Lcom/facebook/mobileconfig/init/MobileConfigInit;Lcom/facebook/auth/viewercontext/ViewerContext;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 91785
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 91786
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    .line 91787
    iget-object v1, v0, LX/0W3;->a:LX/0Wx;

    move-object v1, v1

    .line 91788
    instance-of v1, v1, LX/0Ww;

    if-eqz v1, :cond_0

    .line 91789
    iget-object v1, v0, LX/0W3;->a:LX/0Wx;

    move-object v0, v1

    .line 91790
    check-cast v0, LX/0Ww;

    .line 91791
    invoke-virtual {p0, p1}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/0Wx;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Ww;->a(LX/0Wx;)V

    .line 91792
    invoke-virtual {v0, p2, p3}, LX/0Ww;->setTigonService(Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V

    .line 91793
    iget-object v1, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0uf;

    invoke-virtual {v1}, LX/0uf;->b()V

    .line 91794
    invoke-virtual {v0}, LX/0Ww;->isValid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 91795
    iget-object v1, p0, Lcom/facebook/mobileconfig/init/MobileConfigInit;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;

    invoke-virtual {v0, v1}, LX/0Ww;->registerConfigChangeListener(Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 91796
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/0W3;)Z
    .locals 1

    .prologue
    .line 91777
    monitor-enter p0

    .line 91778
    :try_start_0
    iget-object v0, p2, LX/0W3;->a:LX/0Wx;

    move-object v0, v0

    .line 91779
    instance-of v0, v0, LX/0Ww;

    if-eqz v0, :cond_0

    .line 91780
    iget-object v0, p2, LX/0W3;->a:LX/0Wx;

    move-object v0, v0

    .line 91781
    check-cast v0, LX/0Ww;

    invoke-direct {p0, p1, v0}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Ww;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 91782
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 91783
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final init()V
    .locals 0

    .prologue
    .line 91775
    invoke-direct {p0}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->b()V

    .line 91776
    return-void
.end method
