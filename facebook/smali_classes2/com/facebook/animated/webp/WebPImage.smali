.class public Lcom/facebook/animated/webp/WebPImage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GB;
.implements LX/1GC;


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private mNativeContext:J
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 225014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225015
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 225011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225012
    iput-wide p1, p0, Lcom/facebook/animated/webp/WebPImage;->mNativeContext:J

    .line 225013
    return-void
.end method

.method public static a([B)Lcom/facebook/animated/webp/WebPImage;
    .locals 1

    .prologue
    .line 225005
    invoke-static {}, LX/4ej;->a()V

    .line 225006
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 225007
    array-length v0, p0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 225008
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 225009
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 225010
    invoke-static {v0}, Lcom/facebook/animated/webp/WebPImage;->nativeCreateFromDirectByteBuffer(Ljava/nio/ByteBuffer;)Lcom/facebook/animated/webp/WebPImage;

    move-result-object v0

    return-object v0
.end method

.method private static b(JI)Lcom/facebook/animated/webp/WebPImage;
    .locals 2

    .prologue
    .line 225001
    invoke-static {}, LX/4ej;->a()V

    .line 225002
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 225003
    invoke-static {p0, p1, p2}, Lcom/facebook/animated/webp/WebPImage;->nativeCreateFromNativeMemory(JI)Lcom/facebook/animated/webp/WebPImage;

    move-result-object v0

    return-object v0

    .line 225004
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Lcom/facebook/animated/webp/WebPFrame;
    .locals 1

    .prologue
    .line 225000
    invoke-direct {p0, p1}, Lcom/facebook/animated/webp/WebPImage;->nativeGetFrame(I)Lcom/facebook/animated/webp/WebPFrame;

    move-result-object v0

    return-object v0
.end method

.method private static native nativeCreateFromDirectByteBuffer(Ljava/nio/ByteBuffer;)Lcom/facebook/animated/webp/WebPImage;
.end method

.method private static native nativeCreateFromNativeMemory(JI)Lcom/facebook/animated/webp/WebPImage;
.end method

.method private native nativeDispose()V
.end method

.method private native nativeFinalize()V
.end method

.method private native nativeGetDuration()I
.end method

.method private native nativeGetFrame(I)Lcom/facebook/animated/webp/WebPFrame;
.end method

.method private native nativeGetFrameCount()I
.end method

.method private native nativeGetFrameDurations()[I
.end method

.method private native nativeGetHeight()I
.end method

.method private native nativeGetLoopCount()I
.end method

.method private native nativeGetSizeInBytes()I
.end method

.method private native nativeGetWidth()I
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 224982
    invoke-direct {p0}, Lcom/facebook/animated/webp/WebPImage;->nativeGetWidth()I

    move-result v0

    return v0
.end method

.method public final a(JI)LX/1GB;
    .locals 1

    .prologue
    .line 224983
    invoke-static {p1, p2, p3}, Lcom/facebook/animated/webp/WebPImage;->b(JI)Lcom/facebook/animated/webp/WebPImage;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(I)LX/40I;
    .locals 1

    .prologue
    .line 224984
    invoke-direct {p0, p1}, Lcom/facebook/animated/webp/WebPImage;->c(I)Lcom/facebook/animated/webp/WebPFrame;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 224985
    invoke-direct {p0}, Lcom/facebook/animated/webp/WebPImage;->nativeGetHeight()I

    move-result v0

    return v0
.end method

.method public final b(I)LX/4dL;
    .locals 9

    .prologue
    .line 224986
    invoke-direct {p0, p1}, Lcom/facebook/animated/webp/WebPImage;->c(I)Lcom/facebook/animated/webp/WebPFrame;

    move-result-object v8

    .line 224987
    :try_start_0
    new-instance v0, LX/4dL;

    invoke-virtual {v8}, Lcom/facebook/animated/webp/WebPFrame;->d()I

    move-result v2

    invoke-virtual {v8}, Lcom/facebook/animated/webp/WebPFrame;->e()I

    move-result v3

    invoke-virtual {v8}, Lcom/facebook/animated/webp/WebPFrame;->b()I

    move-result v4

    invoke-virtual {v8}, Lcom/facebook/animated/webp/WebPFrame;->c()I

    move-result v5

    invoke-virtual {v8}, Lcom/facebook/animated/webp/WebPFrame;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v6, LX/4dJ;->BLEND_WITH_PREVIOUS:LX/4dJ;

    :goto_0
    invoke-virtual {v8}, Lcom/facebook/animated/webp/WebPFrame;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v7, LX/4dK;->DISPOSE_TO_BACKGROUND:LX/4dK;

    :goto_1
    move v1, p1

    invoke-direct/range {v0 .. v7}, LX/4dL;-><init>(IIIIILX/4dJ;LX/4dK;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224988
    invoke-virtual {v8}, Lcom/facebook/animated/webp/WebPFrame;->a()V

    return-object v0

    .line 224989
    :cond_0
    :try_start_1
    sget-object v6, LX/4dJ;->NO_BLEND:LX/4dJ;

    goto :goto_0

    :cond_1
    sget-object v7, LX/4dK;->DISPOSE_DO_NOT:LX/4dK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 224990
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Lcom/facebook/animated/webp/WebPFrame;->a()V

    throw v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 224991
    invoke-direct {p0}, Lcom/facebook/animated/webp/WebPImage;->nativeGetFrameCount()I

    move-result v0

    return v0
.end method

.method public final d()[I
    .locals 1

    .prologue
    .line 224992
    invoke-direct {p0}, Lcom/facebook/animated/webp/WebPImage;->nativeGetFrameDurations()[I

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 224993
    invoke-direct {p0}, Lcom/facebook/animated/webp/WebPImage;->nativeGetLoopCount()I

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 224994
    const/4 v0, 0x1

    return v0
.end method

.method public final finalize()V
    .locals 0

    .prologue
    .line 224995
    invoke-direct {p0}, Lcom/facebook/animated/webp/WebPImage;->nativeFinalize()V

    .line 224996
    return-void
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 224997
    invoke-direct {p0}, Lcom/facebook/animated/webp/WebPImage;->nativeGetSizeInBytes()I

    move-result v0

    return v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 224998
    invoke-direct {p0}, Lcom/facebook/animated/webp/WebPImage;->nativeDispose()V

    .line 224999
    return-void
.end method
