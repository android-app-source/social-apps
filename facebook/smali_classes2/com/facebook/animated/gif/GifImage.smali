.class public Lcom/facebook/animated/gif/GifImage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GB;
.implements LX/1GC;


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static volatile a:Z


# instance fields
.field private mNativeContext:J
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 224951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224952
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 224953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224954
    iput-wide p1, p0, Lcom/facebook/animated/gif/GifImage;->mNativeContext:J

    .line 224955
    return-void
.end method

.method public static a([B)Lcom/facebook/animated/gif/GifImage;
    .locals 1

    .prologue
    .line 224956
    invoke-static {}, Lcom/facebook/animated/gif/GifImage;->i()V

    .line 224957
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224958
    array-length v0, p0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 224959
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 224960
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 224961
    invoke-static {v0}, Lcom/facebook/animated/gif/GifImage;->nativeCreateFromDirectByteBuffer(Ljava/nio/ByteBuffer;)Lcom/facebook/animated/gif/GifImage;

    move-result-object v0

    return-object v0
.end method

.method private static b(JI)Lcom/facebook/animated/gif/GifImage;
    .locals 2

    .prologue
    .line 224962
    invoke-static {}, Lcom/facebook/animated/gif/GifImage;->i()V

    .line 224963
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 224964
    invoke-static {p0, p1, p2}, Lcom/facebook/animated/gif/GifImage;->nativeCreateFromNativeMemory(JI)Lcom/facebook/animated/gif/GifImage;

    move-result-object v0

    return-object v0

    .line 224965
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Lcom/facebook/animated/gif/GifFrame;
    .locals 1

    .prologue
    .line 224966
    invoke-direct {p0, p1}, Lcom/facebook/animated/gif/GifImage;->nativeGetFrame(I)Lcom/facebook/animated/gif/GifFrame;

    move-result-object v0

    return-object v0
.end method

.method private static d(I)LX/4dK;
    .locals 1

    .prologue
    .line 224967
    if-nez p0, :cond_0

    .line 224968
    sget-object v0, LX/4dK;->DISPOSE_DO_NOT:LX/4dK;

    .line 224969
    :goto_0
    return-object v0

    .line 224970
    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 224971
    sget-object v0, LX/4dK;->DISPOSE_DO_NOT:LX/4dK;

    goto :goto_0

    .line 224972
    :cond_1
    const/4 v0, 0x2

    if-ne p0, v0, :cond_2

    .line 224973
    sget-object v0, LX/4dK;->DISPOSE_TO_BACKGROUND:LX/4dK;

    goto :goto_0

    .line 224974
    :cond_2
    const/4 v0, 0x3

    if-ne p0, v0, :cond_3

    .line 224975
    sget-object v0, LX/4dK;->DISPOSE_TO_PREVIOUS:LX/4dK;

    goto :goto_0

    .line 224976
    :cond_3
    sget-object v0, LX/4dK;->DISPOSE_DO_NOT:LX/4dK;

    goto :goto_0
.end method

.method private static declared-synchronized i()V
    .locals 2

    .prologue
    .line 224977
    const-class v1, Lcom/facebook/animated/gif/GifImage;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/facebook/animated/gif/GifImage;->a:Z

    if-nez v0, :cond_0

    .line 224978
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/animated/gif/GifImage;->a:Z

    .line 224979
    const-string v0, "gifimage"

    invoke-static {v0}, LX/02C;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224980
    :cond_0
    monitor-exit v1

    return-void

    .line 224981
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static native nativeCreateFromDirectByteBuffer(Ljava/nio/ByteBuffer;)Lcom/facebook/animated/gif/GifImage;
.end method

.method private static native nativeCreateFromNativeMemory(JI)Lcom/facebook/animated/gif/GifImage;
.end method

.method private native nativeDispose()V
.end method

.method private native nativeFinalize()V
.end method

.method private native nativeGetDuration()I
.end method

.method private native nativeGetFrame(I)Lcom/facebook/animated/gif/GifFrame;
.end method

.method private native nativeGetFrameCount()I
.end method

.method private native nativeGetFrameDurations()[I
.end method

.method private native nativeGetHeight()I
.end method

.method private native nativeGetLoopCount()I
.end method

.method private native nativeGetSizeInBytes()I
.end method

.method private native nativeGetWidth()I
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 224931
    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetWidth()I

    move-result v0

    return v0
.end method

.method public final a(JI)LX/1GB;
    .locals 1

    .prologue
    .line 224932
    invoke-static {p1, p2, p3}, Lcom/facebook/animated/gif/GifImage;->b(JI)Lcom/facebook/animated/gif/GifImage;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(I)LX/40I;
    .locals 1

    .prologue
    .line 224933
    invoke-direct {p0, p1}, Lcom/facebook/animated/gif/GifImage;->c(I)Lcom/facebook/animated/gif/GifFrame;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 224934
    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetHeight()I

    move-result v0

    return v0
.end method

.method public final b(I)LX/4dL;
    .locals 9

    .prologue
    .line 224935
    invoke-direct {p0, p1}, Lcom/facebook/animated/gif/GifImage;->c(I)Lcom/facebook/animated/gif/GifFrame;

    move-result-object v8

    .line 224936
    :try_start_0
    new-instance v0, LX/4dL;

    invoke-virtual {v8}, Lcom/facebook/animated/gif/GifFrame;->d()I

    move-result v2

    invoke-virtual {v8}, Lcom/facebook/animated/gif/GifFrame;->e()I

    move-result v3

    invoke-virtual {v8}, Lcom/facebook/animated/gif/GifFrame;->b()I

    move-result v4

    invoke-virtual {v8}, Lcom/facebook/animated/gif/GifFrame;->c()I

    move-result v5

    sget-object v6, LX/4dJ;->BLEND_WITH_PREVIOUS:LX/4dJ;

    invoke-virtual {v8}, Lcom/facebook/animated/gif/GifFrame;->f()I

    move-result v1

    invoke-static {v1}, Lcom/facebook/animated/gif/GifImage;->d(I)LX/4dK;

    move-result-object v7

    move v1, p1

    invoke-direct/range {v0 .. v7}, LX/4dL;-><init>(IIIIILX/4dJ;LX/4dK;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224937
    invoke-virtual {v8}, Lcom/facebook/animated/gif/GifFrame;->a()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Lcom/facebook/animated/gif/GifFrame;->a()V

    throw v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 224938
    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetFrameCount()I

    move-result v0

    return v0
.end method

.method public final d()[I
    .locals 1

    .prologue
    .line 224939
    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetFrameDurations()[I

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 224940
    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetLoopCount()I

    move-result v0

    .line 224941
    packed-switch v0, :pswitch_data_0

    .line 224942
    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    .line 224943
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 224944
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 224945
    const/4 v0, 0x0

    return v0
.end method

.method public final finalize()V
    .locals 0

    .prologue
    .line 224946
    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeFinalize()V

    .line 224947
    return-void
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 224948
    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetSizeInBytes()I

    move-result v0

    return v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 224949
    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeDispose()V

    .line 224950
    return-void
.end method
