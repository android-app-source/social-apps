.class public Lcom/facebook/fbservice/service/OperationResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;


# instance fields
.field public final errorCode:LX/1nY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final errorDescription:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final errorThrowable:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final resultDataBundle:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final resultDataString:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final success:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 316680
    new-instance v0, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {v0}, Lcom/facebook/fbservice/service/OperationResult;-><init>()V

    sput-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    .line 316681
    new-instance v0, LX/1nZ;

    invoke-direct {v0}, LX/1nZ;-><init>()V

    sput-object v0, Lcom/facebook/fbservice/service/OperationResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 316616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316617
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    .line 316618
    iput-object v1, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    .line 316619
    iput-object v1, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    .line 316620
    sget-object v0, LX/1nY;->NO_ERROR:LX/1nY;

    iput-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    .line 316621
    iput-object v1, p0, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    .line 316622
    iput-object v1, p0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    .line 316623
    return-void
.end method

.method private constructor <init>(LX/1nY;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Throwable;)V
    .locals 1
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 316624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316625
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    .line 316626
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    .line 316627
    iput-object p3, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    .line 316628
    iput-object p1, p0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    .line 316629
    iput-object p2, p0, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    .line 316630
    iput-object p4, p0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    .line 316631
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 316632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316633
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    .line 316634
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    .line 316635
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    .line 316636
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1nY;->valueOf(Ljava/lang/String;)LX/1nY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    .line 316637
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    .line 316638
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    iput-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    .line 316639
    return-void

    .line 316640
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 316641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316642
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    .line 316643
    iput-object p1, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    .line 316644
    iput-object p2, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    .line 316645
    sget-object v0, LX/1nY;->NO_ERROR:LX/1nY;

    iput-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    .line 316646
    iput-object v1, p0, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    .line 316647
    iput-object v1, p0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    .line 316648
    return-void
.end method

.method private constructor <init>(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 316649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316650
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    .line 316651
    iput-object v1, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    .line 316652
    iput-object v1, p0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    .line 316653
    iput-object v1, p0, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    .line 316654
    iput-object p1, p0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    .line 316655
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    .line 316656
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    const-string v1, "resultType"

    sget-object v2, LX/2Bh;->PARCELABLE:LX/2Bh;

    invoke-virtual {v2}, LX/2Bh;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316657
    return-void
.end method

.method public static forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 316658
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 316659
    const-string v1, "resultType"

    sget-object v2, LX/2Bh;->PARCELABLE:LX/2Bh;

    invoke-virtual {v2}, LX/2Bh;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316660
    new-instance v1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/facebook/fbservice/service/OperationResult;-><init>(LX/1nY;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Throwable;)V

    return-object v1
.end method

.method public static forError(LX/1nY;Landroid/os/Bundle;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 316661
    const-string v0, "resultType"

    sget-object v1, LX/2Bh;->PARCELABLE:LX/2Bh;

    invoke-virtual {v1}, LX/2Bh;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316662
    new-instance v0, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/facebook/fbservice/service/OperationResult;-><init>(LX/1nY;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static forError(LX/1nY;Landroid/os/Bundle;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 316663
    const-string v0, "resultType"

    sget-object v1, LX/2Bh;->PARCELABLE:LX/2Bh;

    invoke-virtual {v1}, LX/2Bh;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316664
    new-instance v0, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/facebook/fbservice/service/OperationResult;-><init>(LX/1nY;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 316665
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 316666
    const-string v1, "resultType"

    sget-object v2, LX/2Bh;->PARCELABLE:LX/2Bh;

    invoke-virtual {v2}, LX/2Bh;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316667
    new-instance v1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v0, v2}, Lcom/facebook/fbservice/service/OperationResult;-><init>(LX/1nY;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Throwable;)V

    return-object v1
.end method

.method public static forError(LX/1nY;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 316668
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 316669
    const-string v1, "resultType"

    sget-object v2, LX/2Bh;->PARCELABLE:LX/2Bh;

    invoke-virtual {v2}, LX/2Bh;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316670
    new-instance v1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0, p1}, Lcom/facebook/fbservice/service/OperationResult;-><init>(LX/1nY;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Throwable;)V

    return-object v1
.end method

.method public static forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 316671
    new-instance v0, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {v0, p0}, Lcom/facebook/fbservice/service/OperationResult;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static forSuccess()Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 316672
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    return-object v0
.end method

.method public static varargs forSuccess(Landroid/os/Parcelable;[Landroid/util/Pair;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcelable;",
            "[",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;)",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 316673
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 316674
    const-string v0, "resultType"

    sget-object v1, LX/2Bh;->PARCELABLE:LX/2Bh;

    invoke-virtual {v1}, LX/2Bh;->ordinal()I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316675
    const-string v0, "result"

    invoke-virtual {v3, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 316676
    array-length v4, p1

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, p1, v2

    .line 316677
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 316678
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 316679
    :cond_0
    new-instance v0, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v3}, Lcom/facebook/fbservice/service/OperationResult;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 316682
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 316683
    check-cast p0, Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 316684
    :goto_0
    return-object v0

    .line 316685
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 316686
    const-string v0, "resultType"

    invoke-static {p0}, LX/2Bh;->fromObject(Ljava/lang/Object;)LX/2Bh;

    move-result-object v2

    invoke-virtual {v2}, LX/2Bh;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316687
    instance-of v0, p0, Landroid/os/Parcelable;

    if-eqz v0, :cond_2

    .line 316688
    const-string v0, "result"

    check-cast p0, Landroid/os/Parcelable;

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 316689
    :cond_1
    :goto_1
    new-instance v0, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lcom/facebook/fbservice/service/OperationResult;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 316690
    :cond_2
    instance-of v0, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v0, :cond_3

    .line 316691
    const-string v0, "result"

    invoke-static {v1, v0, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 316692
    :cond_3
    if-eqz p0, :cond_1

    .line 316693
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not create result for object "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 316610
    new-instance v0, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/facebook/fbservice/service/OperationResult;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static varargs forSuccess(Ljava/lang/String;[Landroid/util/Pair;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;)",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 316611
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 316612
    array-length v4, p1

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, p1, v2

    .line 316613
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 316614
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 316615
    :cond_0
    new-instance v0, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {v0, p0, v3}, Lcom/facebook/fbservice/service/OperationResult;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<*>;)",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 316521
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 316522
    const-string v3, "resultNull"

    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 316523
    if-eqz p0, :cond_3

    .line 316524
    const-string v0, "resultSize"

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316525
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 316526
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "resultType"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, LX/2Bh;->fromObject(Ljava/lang/Object;)LX/2Bh;

    move-result-object v5

    invoke-virtual {v5}, LX/2Bh;->ordinal()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316527
    instance-of v4, v0, Landroid/os/Parcelable;

    if-eqz v4, :cond_1

    .line 316528
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "result"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 316529
    :goto_2
    add-int/lit8 v1, v1, 0x1

    .line 316530
    goto :goto_1

    :cond_0
    move v0, v1

    .line 316531
    goto :goto_0

    .line 316532
    :cond_1
    instance-of v4, v0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v4, :cond_2

    .line 316533
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "result"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    .line 316534
    :cond_2
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can not create result for object "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 316535
    :cond_3
    new-instance v0, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2}, Lcom/facebook/fbservice/service/OperationResult;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static forSuccess(Ljava/util/HashMap;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "+",
            "Landroid/os/Parcelable;",
            ">;)",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 316537
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 316538
    const-string v1, "resultType"

    sget-object v2, LX/2Bh;->PARCELABLE:LX/2Bh;

    invoke-virtual {v2}, LX/2Bh;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 316539
    const-string v1, "result"

    invoke-static {p0}, LX/0PM;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 316540
    new-instance v1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/facebook/fbservice/service/OperationResult;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 316541
    const/4 v0, 0x0

    return v0
.end method

.method public getErrorCode()LX/1nY;
    .locals 1

    .prologue
    .line 316536
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    return-object v0
.end method

.method public getResultData()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 316542
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 316543
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 316544
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public getResultDataAs(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 316545
    const-class v0, Landroid/os/Parcelable;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316546
    invoke-virtual {p0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    .line 316547
    :goto_0
    return-object v0

    .line 316548
    :cond_0
    const-class v0, Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316549
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 316550
    goto :goto_0

    .line 316551
    :cond_1
    const-class v0, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 316552
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 316553
    goto :goto_0

    .line 316554
    :cond_2
    const-class v0, Ljava/util/List;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 316555
    invoke-virtual {p0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 316556
    :cond_3
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 316557
    invoke-virtual {p0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableMap()Ljava/util/HashMap;

    move-result-object v0

    goto :goto_0

    .line 316558
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid result data type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getResultDataParcelable()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 316559
    invoke-virtual {p0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    .line 316560
    if-nez v0, :cond_0

    .line 316561
    new-instance v0, LX/4BK;

    invoke-direct {v0}, LX/4BK;-><init>()V

    throw v0

    .line 316562
    :cond_0
    return-object v0
.end method

.method public getResultDataParcelable(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 316563
    invoke-virtual {p0, p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 316564
    if-nez v0, :cond_0

    .line 316565
    new-instance v0, LX/4BK;

    invoke-direct {v0}, LX/4BK;-><init>()V

    throw v0

    .line 316566
    :cond_0
    return-object v0
.end method

.method public getResultDataParcelableList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 316567
    invoke-virtual {p0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableListNullOk()Ljava/util/ArrayList;

    move-result-object v0

    .line 316568
    if-nez v0, :cond_0

    .line 316569
    new-instance v0, LX/4BK;

    invoke-direct {v0}, LX/4BK;-><init>()V

    throw v0

    .line 316570
    :cond_0
    return-object v0
.end method

.method public getResultDataParcelableListNullOk()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 316571
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    const-string v2, "resultNull"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 316572
    :cond_1
    return-object v0

    .line 316573
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    const-string v2, "resultSize"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 316574
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 316575
    invoke-static {}, LX/2Bh;->values()[LX/2Bh;

    move-result-object v4

    .line 316576
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 316577
    iget-object v5, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "resultType"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    aget-object v5, v4, v5

    .line 316578
    sget-object v6, LX/2Bh;->NULL:LX/2Bh;

    invoke-virtual {v6, v5}, LX/2Bh;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 316579
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316580
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 316581
    :cond_3
    sget-object v6, LX/2Bh;->FLATTENABLE:LX/2Bh;

    invoke-virtual {v6, v5}, LX/2Bh;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 316582
    iget-object v5, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "result"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 316583
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "result"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getResultDataParcelableMap()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .prologue
    .line 316584
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    const-string v1, "result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    :goto_0
    move-object v0, v0

    .line 316585
    if-nez v0, :cond_0

    .line 316586
    new-instance v0, LX/4BK;

    invoke-direct {v0}, LX/4BK;-><init>()V

    throw v0

    .line 316587
    :cond_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResultDataParcelableNullOk()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 316588
    iget-object v1, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    if-nez v1, :cond_1

    .line 316589
    :cond_0
    :goto_0
    return-object v0

    .line 316590
    :cond_1
    invoke-static {}, LX/2Bh;->values()[LX/2Bh;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    const-string v3, "resultType"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    .line 316591
    sget-object v2, LX/2Bh;->NULL:LX/2Bh;

    invoke-virtual {v2, v1}, LX/2Bh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 316592
    sget-object v0, LX/2Bh;->FLATTENABLE:LX/2Bh;

    invoke-virtual {v0, v1}, LX/2Bh;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 316593
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    const-string v1, "result"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 316594
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 316595
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    const-string v1, "result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getResultDataParcelableNullOk(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 316596
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 316597
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 316598
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResultDataString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316599
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    return-object v0
.end method

.method public hasResultData(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 316600
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public success()Z
    .locals 1

    .prologue
    .line 316601
    iget-boolean v0, p0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 316602
    iget-boolean v0, p0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 316603
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 316604
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 316605
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    invoke-virtual {v0}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 316606
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 316607
    iget-object v0, p0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 316608
    return-void

    .line 316609
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
