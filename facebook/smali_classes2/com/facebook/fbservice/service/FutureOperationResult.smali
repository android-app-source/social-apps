.class public Lcom/facebook/fbservice/service/FutureOperationResult;
.super Lcom/facebook/fbservice/service/OperationResult;
.source ""


# instance fields
.field private final future:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 331044
    invoke-direct {p0}, Lcom/facebook/fbservice/service/OperationResult;-><init>()V

    .line 331045
    iput-object p1, p0, Lcom/facebook/fbservice/service/FutureOperationResult;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 331046
    return-void
.end method


# virtual methods
.method public getFuture()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331047
    iget-object v0, p0, Lcom/facebook/fbservice/service/FutureOperationResult;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method
