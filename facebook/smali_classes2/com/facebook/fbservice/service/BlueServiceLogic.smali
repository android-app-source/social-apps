.class public Lcom/facebook/fbservice/service/BlueServiceLogic;
.super LX/1mK;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static volatile sInstance__com_facebook_fbservice_service_BlueServiceLogic__INJECTED_BY_TemplateInjector:Lcom/facebook/fbservice/service/BlueServiceLogic;


# instance fields
.field private final mAllQueues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "LX/1qD;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final mAnalyticsLogger:LX/0Zb;

.field private final mAppInitLock:LX/0Uq;

.field private final mBackgroundWorkLogger:LX/0Sj;

.field private final mBlueServiceQueueManager:LX/1mM;

.field private final mContext:Landroid/content/Context;

.field private final mErrorReporter:LX/03V;

.field private final mFbSharedPreferencesInitLock:LX/0cZ;

.field private final mGatekeeperInitLock:LX/0YO;

.field private final mHandlerExecutorServiceFactory:LX/0YF;

.field private final mHookSetProvider:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/fbservice/service/BlueServiceQueueHook;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private final mNextId:Ljava/util/concurrent/atomic/AtomicLong;

.field private final mSoftErrorHelper:LX/1mP;

.field private final mSoftErrorReportingGkProvider:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mViewerContextManager:LX/0SI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 313496
    const-class v0, Lcom/facebook/fbservice/service/BlueServiceLogic;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/service/BlueServiceLogic;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1mM;LX/0SI;LX/03V;LX/0Zb;LX/0YF;LX/0Sj;LX/0Or;LX/1mP;LX/0Or;LX/0YO;LX/0cZ;LX/0Uq;)V
    .locals 6
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/fbservice/service/OrcaServiceSoftErrorReportingGk;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1mM;",
            "LX/0SI;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Zb;",
            "Lcom/facebook/common/executors/HandlerExecutorServiceFactory;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/fbservice/service/BlueServiceQueueHook;",
            ">;>;",
            "LX/1mP;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0YO;",
            "LX/0cZ;",
            "LX/0Uq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 313600
    invoke-direct {p0}, LX/1mK;-><init>()V

    .line 313601
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mLock:Ljava/lang/Object;

    .line 313602
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    .line 313603
    new-instance v2, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v2, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mNextId:Ljava/util/concurrent/atomic/AtomicLong;

    .line 313604
    iput-object p1, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mContext:Landroid/content/Context;

    .line 313605
    iput-object p2, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mBlueServiceQueueManager:LX/1mM;

    .line 313606
    iput-object p3, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mViewerContextManager:LX/0SI;

    .line 313607
    iput-object p4, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mErrorReporter:LX/03V;

    .line 313608
    iput-object p5, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAnalyticsLogger:LX/0Zb;

    .line 313609
    iput-object p6, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mHandlerExecutorServiceFactory:LX/0YF;

    .line 313610
    iput-object p7, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mBackgroundWorkLogger:LX/0Sj;

    .line 313611
    iput-object p8, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mHookSetProvider:LX/0Or;

    .line 313612
    iput-object p9, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mSoftErrorHelper:LX/1mP;

    .line 313613
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mSoftErrorReportingGkProvider:LX/0Or;

    .line 313614
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mGatekeeperInitLock:LX/0YO;

    .line 313615
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mFbSharedPreferencesInitLock:LX/0cZ;

    .line 313616
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAppInitLock:LX/0Uq;

    .line 313617
    return-void
.end method

.method private createBlueServiceQueue(Ljava/lang/Class;LX/0Or;LX/0TP;)LX/1qD;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1qM;",
            ">;",
            "LX/0TP;",
            ")",
            "LX/1qD;"
        }
    .end annotation

    .prologue
    .line 313599
    new-instance v0, LX/1qD;

    iget-object v1, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mHookSetProvider:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    iget-object v4, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mHandlerExecutorServiceFactory:LX/0YF;

    iget-object v5, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mBlueServiceQueueManager:LX/1mM;

    iget-object v6, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mViewerContextManager:LX/0SI;

    iget-object v7, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mErrorReporter:LX/03V;

    iget-object v8, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAnalyticsLogger:LX/0Zb;

    invoke-static {}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->get()Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v9

    iget-object v10, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mBackgroundWorkLogger:LX/0Sj;

    iget-object v12, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mSoftErrorHelper:LX/1mP;

    iget-object v13, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mSoftErrorReportingGkProvider:LX/0Or;

    iget-object v14, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAppInitLock:LX/0Uq;

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v11, p3

    invoke-direct/range {v0 .. v14}, LX/1qD;-><init>(Ljava/lang/Class;LX/0Or;Ljava/util/Set;LX/0YF;LX/1mM;LX/0SI;LX/03V;LX/0Zb;LX/0So;LX/0Sj;LX/0TP;LX/1mP;LX/0Or;LX/0Uq;)V

    return-object v0
.end method

.method private static createInstance__com_facebook_fbservice_service_BlueServiceLogic__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/fbservice/service/BlueServiceLogic;
    .locals 14

    .prologue
    .line 313597
    new-instance v0, Lcom/facebook/fbservice/service/BlueServiceLogic;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/1mM;->getInstance__com_facebook_fbservice_service_BlueServiceQueueManager__INJECTED_BY_TemplateInjector(LX/0QB;)LX/1mM;

    move-result-object v2

    check-cast v2, LX/1mM;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v3

    check-cast v3, LX/0SI;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {p0}, LX/0YF;->a(LX/0QB;)LX/0YF;

    move-result-object v6

    check-cast v6, LX/0YF;

    invoke-static {p0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v7

    check-cast v7, LX/0Sj;

    invoke-static {p0}, LX/1mN;->getSetProvider(LX/0QB;)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/1mP;->a(LX/0QB;)LX/1mP;

    move-result-object v9

    check-cast v9, LX/1mP;

    const/16 v10, 0x1490

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {p0}, LX/0YO;->b(LX/0QB;)LX/0YO;

    move-result-object v11

    check-cast v11, LX/0YO;

    invoke-static {p0}, LX/0cZ;->b(LX/0QB;)LX/0cZ;

    move-result-object v12

    check-cast v12, LX/0cZ;

    invoke-static {p0}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v13

    check-cast v13, LX/0Uq;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/fbservice/service/BlueServiceLogic;-><init>(Landroid/content/Context;LX/1mM;LX/0SI;LX/03V;LX/0Zb;LX/0YF;LX/0Sj;LX/0Or;LX/1mP;LX/0Or;LX/0YO;LX/0cZ;LX/0Uq;)V

    .line 313598
    return-object v0
.end method

.method private getBlueServiceQueueExecutingOperation(Ljava/lang/String;)LX/1qD;
    .locals 4

    .prologue
    .line 313591
    iget-object v1, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 313592
    :try_start_0
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qD;

    .line 313593
    invoke-virtual {v0, p1}, LX/1qD;->hasOperation(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 313594
    monitor-exit v1

    .line 313595
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 313596
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance__com_facebook_fbservice_service_BlueServiceLogic__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/fbservice/service/BlueServiceLogic;
    .locals 3

    .prologue
    .line 313581
    sget-object v0, Lcom/facebook/fbservice/service/BlueServiceLogic;->sInstance__com_facebook_fbservice_service_BlueServiceLogic__INJECTED_BY_TemplateInjector:Lcom/facebook/fbservice/service/BlueServiceLogic;

    if-nez v0, :cond_1

    .line 313582
    const-class v1, Lcom/facebook/fbservice/service/BlueServiceLogic;

    monitor-enter v1

    .line 313583
    :try_start_0
    sget-object v0, Lcom/facebook/fbservice/service/BlueServiceLogic;->sInstance__com_facebook_fbservice_service_BlueServiceLogic__INJECTED_BY_TemplateInjector:Lcom/facebook/fbservice/service/BlueServiceLogic;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 313584
    if-eqz v2, :cond_0

    .line 313585
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/fbservice/service/BlueServiceLogic;->createInstance__com_facebook_fbservice_service_BlueServiceLogic__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/fbservice/service/BlueServiceLogic;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/service/BlueServiceLogic;->sInstance__com_facebook_fbservice_service_BlueServiceLogic__INJECTED_BY_TemplateInjector:Lcom/facebook/fbservice/service/BlueServiceLogic;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313586
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 313587
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 313588
    :cond_1
    sget-object v0, Lcom/facebook/fbservice/service/BlueServiceLogic;->sInstance__com_facebook_fbservice_service_BlueServiceLogic__INJECTED_BY_TemplateInjector:Lcom/facebook/fbservice/service/BlueServiceLogic;

    return-object v0

    .line 313589
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 313590
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public cancel(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 313577
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/service/BlueServiceLogic;->getBlueServiceQueueExecutingOperation(Ljava/lang/String;)LX/1qD;

    move-result-object v0

    .line 313578
    if-eqz v0, :cond_0

    .line 313579
    invoke-virtual {v0, p1}, LX/1qD;->cancelOperation(Ljava/lang/String;)Z

    move-result v0

    .line 313580
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public changePriority(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)Z
    .locals 1

    .prologue
    .line 313573
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/service/BlueServiceLogic;->getBlueServiceQueueExecutingOperation(Ljava/lang/String;)LX/1qD;

    move-result-object v0

    .line 313574
    if-eqz v0, :cond_0

    .line 313575
    invoke-virtual {v0, p1, p2}, LX/1qD;->changePriority(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)Z

    move-result v0

    .line 313576
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public drainQueue()V
    .locals 7

    .prologue
    .line 313559
    iget-object v2, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 313560
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 313561
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qD;

    .line 313562
    invoke-static {v0}, LX/1mM;->queueShouldBeIgnored(LX/1qD;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 313563
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 313564
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 313565
    :cond_0
    :try_start_1
    invoke-virtual {v0}, LX/1qD;->stop()V

    goto :goto_0

    .line 313566
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 313567
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qD;

    .line 313568
    iget-object v5, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    .line 313569
    iget-object v6, v0, LX/1qD;->mQueueName:Ljava/lang/Class;

    move-object v6, v6

    .line 313570
    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313571
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 313572
    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public registerCompletionHandler(Ljava/lang/String;LX/1qB;)Z
    .locals 5

    .prologue
    .line 313547
    const/4 v1, 0x0

    .line 313548
    iget-object v2, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 313549
    :try_start_0
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qD;

    .line 313550
    invoke-virtual {v0, p1}, LX/1qD;->hasOperation(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 313551
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313552
    if-eqz v0, :cond_1

    .line 313553
    invoke-virtual {v0, p1, p2}, LX/1qD;->registerCompletionHandlerInternal(Ljava/lang/String;LX/1qB;)Z

    move-result v0

    .line 313554
    if-eqz v0, :cond_1

    .line 313555
    const/4 v0, 0x1

    .line 313556
    :goto_1
    return v0

    .line 313557
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 313558
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public startOperation(Ljava/lang/String;Landroid/os/Bundle;ZLcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
    .locals 6
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 313546
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/fbservice/service/BlueServiceLogic;->startOperationWithCompletionHandler(Ljava/lang/String;Landroid/os/Bundle;ZLX/1qB;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public startOperationWithCompletionHandler(Ljava/lang/String;Landroid/os/Bundle;ZLX/1qB;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
    .locals 7
    .param p4    # LX/1qB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 313545
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/fbservice/service/BlueServiceLogic;->startOperationWithCompletionHandlerAppInit(Ljava/lang/String;Landroid/os/Bundle;ZZLX/1qB;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public startOperationWithCompletionHandlerAppInit(Ljava/lang/String;Landroid/os/Bundle;ZZLX/1qB;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
    .locals 8
    .param p5    # LX/1qB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 313503
    iget-object v7, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mLock:Ljava/lang/Object;

    monitor-enter v7

    .line 313504
    :try_start_0
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mBlueServiceQueueManager:LX/1mM;

    invoke-virtual {v0}, LX/1mM;->isInLameDuckMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313505
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 313506
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 313507
    :cond_0
    if-eqz p2, :cond_1

    .line 313508
    :try_start_1
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 313509
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mGatekeeperInitLock:LX/0YO;

    invoke-virtual {v0}, LX/0Uq;->b()V

    .line 313510
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mFbSharedPreferencesInitLock:LX/0cZ;

    invoke-virtual {v0}, LX/0Uq;->b()V

    .line 313511
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mContext:Landroid/content/Context;

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    .line 313512
    invoke-static {p1}, LX/38I;->a(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 313513
    iget-object v1, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1qD;

    .line 313514
    if-nez v1, :cond_4

    .line 313515
    new-instance v3, LX/1qC;

    invoke-interface {v2}, LX/0QB;->getScopeUnawareInjector()LX/0QD;

    move-result-object v4

    invoke-direct {v3, v4, v0}, LX/1qC;-><init>(LX/0QB;Ljava/lang/Class;)V

    move-object v3, v3

    .line 313516
    sget-object v1, LX/0TP;->NORMAL:LX/0TP;

    .line 313517
    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    const/4 v4, 0x0

    :goto_0
    move v4, v4

    .line 313518
    if-eqz v4, :cond_3

    .line 313519
    invoke-static {v2, v0}, LX/38I;->a(LX/0QB;Ljava/lang/Class;)LX/0TP;

    move-result-object v1

    check-cast v1, LX/0TP;

    .line 313520
    :cond_3
    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/fbservice/service/BlueServiceLogic;->createBlueServiceQueue(Ljava/lang/Class;LX/0Or;LX/0TP;)LX/1qD;

    move-result-object v1

    .line 313521
    const/4 v3, 0x0

    .line 313522
    iget-object v2, v1, LX/1qD;->mStoppedCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x1

    :goto_1
    const-string v4, "Queue cannot be started after stopped"

    invoke-static {v2, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 313523
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Blue_"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, LX/1qD;->mQueueName:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 313524
    iget-object v4, v1, LX/1qD;->mHandlerExecutorServiceFactory:LX/0YF;

    iget-object v5, v1, LX/1qD;->mThreadPriority:LX/0TP;

    invoke-virtual {v4, v2, v5, v3}, LX/0YF;->a(Ljava/lang/String;LX/0TP;Z)LX/0Te;

    move-result-object v2

    iput-object v2, v1, LX/1qD;->mHandlerExecutorService:LX/0Te;

    .line 313525
    iget-object v2, v1, LX/1qD;->mBlueServiceQueueManager:LX/1mM;

    invoke-virtual {v2, v1}, LX/1mM;->onServiceQueueStarted(LX/1qD;)V

    .line 313526
    iget-object v2, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    move-object v6, v1

    .line 313527
    iget-object v1, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mNextId:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 313528
    if-nez p6, :cond_6

    .line 313529
    const/4 v4, 0x0

    .line 313530
    new-instance v2, Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v4, v4, v4}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v2
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313531
    :goto_2
    :try_start_2
    new-instance v0, LX/1qE;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, LX/1qE;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 313532
    if-eqz p4, :cond_5

    .line 313533
    const/4 v2, 0x1

    .line 313534
    iput-boolean v2, v0, LX/1qE;->mCanRunBeforeAppInit:Z

    .line 313535
    :cond_5
    invoke-virtual {v6, v0, p5}, LX/1qD;->addOperation(LX/1qE;LX/1qB;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 313536
    :try_start_3
    monitor-exit v7

    return-object v1

    .line 313537
    :catch_0
    move-exception v0

    .line 313538
    :goto_3
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 313539
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error occurred in startOperation("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), callerContext: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 313540
    iget-object v1, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mErrorReporter:LX/03V;

    const-string v2, "BlueService"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313541
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 313542
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 313543
    :catch_1
    move-exception v0

    move-object p6, v5

    goto :goto_3

    :cond_6
    move-object v5, p6

    goto :goto_2

    :sswitch_0
    :try_start_4
    const-string v6, "com.facebook.messaging.send.service.PendingSendQueue"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_0

    :sswitch_1
    const-string v6, "com.facebook.feed.protocol.NewsFeedFetchQueue"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_0

    :sswitch_2
    const-string v6, "com.facebook.fbservice.service.AuthQueue"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_0

    :sswitch_3
    const-string v6, "com.facebook.messaging.send.service.SendQueue"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_0

    :sswitch_4
    const-string v6, "com.facebook.messaging.media.upload.PhotoUploadQueue"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_0

    :sswitch_5
    const-string v6, "com.facebook.feed.protocol.NewsFeedPostingQueue"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_0

    :sswitch_6
    const-string v6, "com.facebook.messaging.media.upload.PhotoUploadParallelQueue"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_0

    :sswitch_7
    const-string v6, "com.facebook.feed.protocol.NewsFeedMainQueue"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_0

    :sswitch_8
    const-string v6, "com.facebook.messaging.service.multicache.MultiCacheThreadsQueue"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_7
    move v2, v3

    .line 313544
    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7b949b54 -> :sswitch_0
        -0x714cbab7 -> :sswitch_1
        -0x610716e0 -> :sswitch_2
        -0x59b90e71 -> :sswitch_3
        -0x2c5fd564 -> :sswitch_4
        -0x1ef46bf -> :sswitch_5
        0x35b0fdd5 -> :sswitch_6
        0x4f25adc6 -> :sswitch_7
        0x4fa49fd8 -> :sswitch_8
    .end sparse-switch
.end method

.method public stopQueues()V
    .locals 3

    .prologue
    .line 313497
    iget-object v1, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 313498
    :try_start_0
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qD;

    .line 313499
    invoke-virtual {v0}, LX/1qD;->stop()V

    goto :goto_0

    .line 313500
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 313501
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/fbservice/service/BlueServiceLogic;->mAllQueues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 313502
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
