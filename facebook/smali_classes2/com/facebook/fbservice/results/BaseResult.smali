.class public Lcom/facebook/fbservice/results/BaseResult;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# instance fields
.field public final clientTimeMs:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "clientTimeMs"
    .end annotation
.end field

.field public final freshness:LX/0ta;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "freshness"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 155007
    const-class v0, Lcom/facebook/fbservice/results/BaseResultDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 155005
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 155006
    return-void
.end method

.method public constructor <init>(LX/0ta;J)V
    .locals 0

    .prologue
    .line 155001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155002
    iput-object p1, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    .line 155003
    iput-wide p2, p0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    .line 155004
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 154997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154998
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0ta;

    iput-object v0, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    .line 154999
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    .line 155000
    return-void
.end method


# virtual methods
.method public getClientTimeMs()J
    .locals 2

    .prologue
    .line 154991
    iget-wide v0, p0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    return-wide v0
.end method

.method public getFreshness()LX/0ta;
    .locals 1

    .prologue
    .line 154996
    iget-object v0, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 154992
    iget-object v0, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 154993
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 154994
    iget-wide v0, p0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 154995
    return-void
.end method
