.class public final Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic this$0:LX/1MG;

.field public final synthetic val$result:Lcom/facebook/fbservice/service/OperationResult;


# direct methods
.method public constructor <init>(LX/1MG;Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 0

    .prologue
    .line 331048
    iput-object p1, p0, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$5;->this$0:LX/1MG;

    iput-object p2, p0, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$5;->val$result:Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 331049
    iget-object v0, p0, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$5;->this$0:LX/1MG;

    invoke-virtual {v0}, LX/1MG;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 331050
    iget-object v0, p0, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$5;->this$0:LX/1MG;

    iget-object v1, p0, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$5;->val$result:Lcom/facebook/fbservice/service/OperationResult;

    .line 331051
    invoke-static {v0}, LX/1MG;->stopShowingProgress(LX/1MG;)V

    .line 331052
    iget-boolean v2, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 331053
    if-eqz v2, :cond_1

    .line 331054
    iget-object v2, v0, LX/1MG;->mResult:LX/1MK;

    invoke-virtual {v2, v1}, LX/1MK;->set(Lcom/facebook/fbservice/service/OperationResult;)Z

    .line 331055
    :goto_0
    invoke-virtual {v0}, LX/1MG;->dispose()V

    .line 331056
    :cond_0
    return-void

    .line 331057
    :cond_1
    iget-object v2, v0, LX/1MG;->mErrorPropagation:LX/1ME;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    if-ne v2, v3, :cond_3

    .line 331058
    iget-object v2, v1, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    move-object v2, v2

    .line 331059
    if-eqz v2, :cond_3

    .line 331060
    iget-object v2, v1, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    move-object v2, v2

    .line 331061
    :goto_1
    const/4 p0, 0x0

    .line 331062
    iget-object v3, v0, LX/1MG;->mContext:Landroid/content/Context;

    const-class v1, LX/0ev;

    invoke-static {v3, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ev;

    .line 331063
    if-eqz v3, :cond_4

    .line 331064
    invoke-interface {v3, v2}, LX/0ev;->a(Ljava/lang/Throwable;)Z

    move-result v3

    .line 331065
    :goto_2
    if-nez v3, :cond_2

    .line 331066
    iget-object v3, v0, LX/1MG;->mResult:LX/1MK;

    invoke-virtual {v3, v2}, LX/1MK;->setException(Ljava/lang/Throwable;)Z

    .line 331067
    :cond_2
    goto :goto_0

    .line 331068
    :cond_3
    new-instance v2, Lcom/facebook/fbservice/service/ServiceException;

    invoke-direct {v2, v1}, Lcom/facebook/fbservice/service/ServiceException;-><init>(Lcom/facebook/fbservice/service/OperationResult;)V

    goto :goto_1

    .line 331069
    :cond_4
    iget-object v3, v0, LX/1MG;->mCriticalServiceExceptionChecker:LX/0l4;

    invoke-virtual {v3, v2}, LX/0l4;->isInvalidSessionException(Ljava/lang/Throwable;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 331070
    new-instance v3, Landroid/content/Intent;

    const-string v1, "BLUESERVICE_NO_AUTH"

    invoke-direct {v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 331071
    iget-object v1, v0, LX/1MG;->mCrossFbProcessBroadcast:LX/0Xl;

    invoke-interface {v1, v3}, LX/0Xl;->a(Landroid/content/Intent;)V

    :cond_5
    move v3, p0

    goto :goto_2
.end method
