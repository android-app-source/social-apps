.class public final Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0WS;


# direct methods
.method public constructor <init>(LX/0WS;)V
    .locals 0

    .prologue
    .line 76387
    iput-object p1, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 76388
    :try_start_0
    iget-object v0, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    iget-object v1, v0, LX/0WS;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48a; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 76389
    :try_start_1
    iget-object v0, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    iget-object v0, v0, LX/0WS;->f:LX/0WR;

    iget-object v2, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    iget-object v2, v2, LX/0WS;->i:Ljava/util/Map;

    invoke-virtual {v0, v2}, LX/0WR;->a(Ljava/util/Map;)V

    .line 76390
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76391
    iget-object v0, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    .line 76392
    iput-boolean v3, v0, LX/0WS;->l:Z

    .line 76393
    iget-object v0, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    iget-object v0, v0, LX/0WS;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 76394
    :goto_0
    return-void

    .line 76395
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch LX/48a; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 76396
    :catch_0
    move-exception v0

    .line 76397
    :try_start_4
    const-string v1, "LightSharedPreferencesImpl"

    const-string v2, "Failed to load preference file from Disk!"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 76398
    iget-object v0, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    .line 76399
    iput-boolean v3, v0, LX/0WS;->l:Z

    .line 76400
    iget-object v0, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    iget-object v0, v0, LX/0WS;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 76401
    :catch_1
    move-exception v0

    .line 76402
    :try_start_5
    const-string v1, "LightSharedPreferencesImpl"

    const-string v2, "Failed to parse the preference file!"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 76403
    iget-object v0, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    .line 76404
    iput-boolean v3, v0, LX/0WS;->l:Z

    .line 76405
    iget-object v0, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    iget-object v0, v0, LX/0WS;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 76406
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    .line 76407
    iput-boolean v3, v1, LX/0WS;->l:Z

    .line 76408
    iget-object v1, p0, Lcom/facebook/crudolib/prefs/LightSharedPreferencesImpl$1;->a:LX/0WS;

    iget-object v1, v1, LX/0WS;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
