.class public Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;


# instance fields
.field private final a:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 122930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122931
    iput-object p1, p0, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 122932
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;
    .locals 4

    .prologue
    .line 122933
    sget-object v0, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->b:Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    if-nez v0, :cond_1

    .line 122934
    const-class v1, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    monitor-enter v1

    .line 122935
    :try_start_0
    sget-object v0, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->b:Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 122936
    if-eqz v2, :cond_0

    .line 122937
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 122938
    new-instance p0, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 122939
    move-object v0, p0

    .line 122940
    sput-object v0, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->b:Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122941
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 122942
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 122943
    :cond_1
    sget-object v0, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->b:Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    return-object v0

    .line 122944
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 122945
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 122946
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x380008

    const-string v2, "MainActivityIntentToFragmentCreate"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122947
    monitor-exit p0

    return-void

    .line 122948
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 122949
    monitor-enter p0

    .line 122950
    :try_start_0
    const-string v0, "extra_launch_uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122951
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122952
    const/4 v0, 0x0

    .line 122953
    :goto_0
    move v0, v0

    .line 122954
    if-eqz v0, :cond_0

    .line 122955
    iget-object v0, p0, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x380008

    const-string v2, "MainActivityIntentToFragmentCreate"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 122956
    iget-object v0, p0, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x380008

    const-string v2, "MainActivityIntentToFragmentCreate"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122957
    :cond_0
    monitor-exit p0

    return-void

    .line 122958
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    sget-object v1, LX/0ax;->dg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
