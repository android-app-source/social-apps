.class public Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/13v;
.implements LX/0f2;
.implements LX/0fh;
.implements LX/0hF;
.implements LX/0fj;
.implements LX/145;
.implements LX/0o2;


# annotations
.annotation runtime Lcom/facebook/search/interfaces/GraphSearchTitleSupport;
.end annotation


# instance fields
.field public A:Landroid/content/Context;

.field public B:LX/8EC;

.field public C:LX/CXj;

.field public D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1PJ;",
            ">;"
        }
    .end annotation
.end field

.field public E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public G:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/0ad;

.field private I:LX/CXv;

.field public J:LX/0gc;

.field public K:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

.field public a:Lcom/facebook/base/fragment/FbFragment;

.field public b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

.field public c:Landroid/support/v4/app/Fragment;

.field public d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

.field public e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

.field public f:Landroid/view/ViewGroup;

.field public g:LX/HW4;

.field private h:Landroid/os/ParcelUuid;

.field public i:Ljava/lang/String;

.field public j:J

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public n:Z

.field public o:Z

.field public p:I

.field public q:Z

.field public r:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/HW5;",
            ">;"
        }
    .end annotation
.end field

.field public s:I

.field public t:Landroid/view/ViewGroup;

.field private u:Landroid/view/LayoutInflater;

.field public v:LX/44t;

.field public w:LX/17Y;

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/2U1;

.field public z:LX/0Sg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 178042
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 178043
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->r:LX/0am;

    .line 178044
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    invoke-static {p0}, LX/44t;->a(LX/0QB;)LX/44t;

    move-result-object v2

    check-cast v2, LX/44t;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v3

    check-cast v3, LX/17Y;

    const/16 v4, 0x259

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v5

    check-cast v5, LX/2U1;

    invoke-static {p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v6

    check-cast v6, LX/0Sg;

    const-class v7, Landroid/content/Context;

    invoke-interface {p0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {p0}, LX/8EC;->a(LX/0QB;)LX/8EC;

    move-result-object v8

    check-cast v8, LX/8EC;

    invoke-static {p0}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v9

    check-cast v9, LX/CXj;

    const/16 v10, 0x699

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xafd

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xac0

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x1430

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v2, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->v:LX/44t;

    iput-object v3, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->w:LX/17Y;

    iput-object v4, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->x:LX/0Ot;

    iput-object v5, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->y:LX/2U1;

    iput-object v6, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->z:LX/0Sg;

    iput-object v7, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->A:Landroid/content/Context;

    iput-object v8, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->B:LX/8EC;

    iput-object v9, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->C:LX/CXj;

    iput-object v10, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->D:LX/0Ot;

    iput-object v11, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->E:LX/0Ot;

    iput-object v13, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->F:LX/0Ot;

    iput-object v12, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->G:LX/0Ot;

    iput-object p0, v1, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->H:LX/0ad;

    return-void
.end method

.method public static a(LX/0Px;LX/8A3;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/8A3;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 177885
    if-eqz p0, :cond_0

    .line 177886
    new-instance v0, LX/8A4;

    invoke-direct {v0, p0}, LX/8A4;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, p1}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    .line 177887
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;)V
    .locals 3

    .prologue
    .line 178036
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1PJ;

    const/4 v1, 0x0

    .line 178037
    iput v1, v0, LX/1PJ;->b:I

    .line 178038
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1PJ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0406

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 178039
    iput v1, v0, LX/1PJ;->a:I

    .line 178040
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 178033
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    instance-of v0, v0, LX/0fj;

    if-eqz v0, :cond_0

    .line 178034
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    move-result v0

    .line 178035
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178032
    const-string v0, "pages_public_view"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 177992
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 177993
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 177994
    if-nez v0, :cond_0

    .line 177995
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Hosting activity is null."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 177996
    :goto_0
    return-void

    .line 177997
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v2, 0x7f0e06ea

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 177998
    const-class v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 177999
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 178000
    const-string v0, "page_fragment_uuid"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->h:Landroid/os/ParcelUuid;

    .line 178001
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->h:Landroid/os/ParcelUuid;

    if-nez v0, :cond_7

    .line 178002
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UUID is not passed in as args"

    invoke-virtual {v0, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 178003
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->B:LX/8EC;

    .line 178004
    iget-object v3, v0, LX/8EC;->c:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, v5, :cond_1

    iget-object v3, v0, LX/8EC;->c:LX/0Px;

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Pq;

    .line 178005
    iget-object v6, v0, LX/8EC;->a:LX/11i;

    const/4 v7, 0x0

    invoke-interface {v6, v3, v7}, LX/11i;->a(LX/0Pq;LX/0P1;)LX/11o;

    .line 178006
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 178007
    :cond_1
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    .line 178008
    iget-wide v4, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->k:Ljava/lang/String;

    .line 178009
    iget-wide v4, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_8

    move v0, v1

    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid page id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 178010
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->K:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    .line 178011
    const-string v0, "fbpage_new_message"

    const-string v3, "tracking_notification_type"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178012
    iput v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->s:I

    .line 178013
    :cond_2
    iget-wide v8, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-static {v8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178014
    iget-object v8, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->y:LX/2U1;

    iget-object v9, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->k:Ljava/lang/String;

    invoke-virtual {v8, v9}, LX/2U1;->c(Ljava/lang/String;)LX/8Dk;

    move-result-object v8

    .line 178015
    if-eqz v8, :cond_3

    .line 178016
    iget-object v9, v8, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v8, v9

    .line 178017
    invoke-virtual {v8}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->l:Ljava/lang/String;

    .line 178018
    :cond_3
    if-eqz p1, :cond_5

    .line 178019
    const-string v0, "extra_viewer_profile_permissions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 178020
    const-string v0, "extra_viewer_profile_permissions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->m:LX/0Px;

    .line 178021
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->m:LX/0Px;

    sget-object v3, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-static {v0, v3}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a(LX/0Px;LX/8A3;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->q:Z

    .line 178022
    :cond_4
    const-string v0, "extra_page_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 178023
    const-string v0, "extra_page_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->l:Ljava/lang/String;

    .line 178024
    :cond_5
    const-string v0, "extra_page_tab"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/BEQ;

    .line 178025
    sget-object v2, LX/BEQ;->ACTIVITY:LX/BEQ;

    if-ne v0, v2, :cond_9

    .line 178026
    iput v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->s:I

    .line 178027
    :cond_6
    :goto_4
    new-instance v0, LX/HW1;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->h:Landroid/os/ParcelUuid;

    invoke-direct {v0, p0, v1}, LX/HW1;-><init>(Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;Landroid/os/ParcelUuid;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->I:LX/CXv;

    goto/16 :goto_0

    .line 178028
    :cond_7
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->h:Landroid/os/ParcelUuid;

    invoke-virtual {v0}, Landroid/os/ParcelUuid;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->i:Ljava/lang/String;

    goto/16 :goto_1

    .line 178029
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 178030
    :cond_9
    sget-object v1, LX/BEQ;->INSIGHTS:LX/BEQ;

    if-ne v0, v1, :cond_6

    .line 178031
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->s:I

    goto :goto_4
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;LX/HPm;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;",
            "LX/HPm;",
            ")V"
        }
    .end annotation

    .prologue
    .line 177958
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 177959
    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 177960
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    .line 177961
    if-eqz v2, :cond_5

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->m:LX/0Px;

    .line 177962
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->m:LX/0Px;

    sget-object v3, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-static {v1, v3}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a(LX/0Px;LX/8A3;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->q:Z

    .line 177963
    iget-boolean v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->q:Z

    if-eqz v1, :cond_6

    .line 177964
    const/4 v4, 0x0

    .line 177965
    iget-boolean v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->n:Z

    if-nez v1, :cond_0

    .line 177966
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-virtual {v1, v4}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->setVisibility(I)V

    .line 177967
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->g:LX/HW4;

    invoke-virtual {v1}, LX/0gG;->kV_()V

    .line 177968
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->n:Z

    .line 177969
    :cond_0
    iget v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->s:I

    if-eqz v1, :cond_1

    .line 177970
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iget v3, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->s:I

    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 177971
    iput v4, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->s:I

    .line 177972
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->o:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->q:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->A:Landroid/content/Context;

    invoke-static {v1}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v1

    const/16 v3, 0x7dc

    if-lt v1, v3, :cond_7

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 177973
    if-eqz v1, :cond_2

    .line 177974
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->z:LX/0Sg;

    const-string v3, "Preloading Page admin tabs"

    new-instance v4, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment$2;

    invoke-direct {v4, p0}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment$2;-><init>(Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;)V

    sget-object p1, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    sget-object p2, LX/0Vm;->UI:LX/0Vm;

    invoke-virtual {v1, v3, v4, p1, p2}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 177975
    :cond_2
    :goto_3
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v0

    .line 177976
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 177977
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->l:Ljava/lang/String;

    .line 177978
    :cond_3
    new-instance v0, LX/HRD;

    invoke-direct {v0}, LX/HRD;-><init>()V

    move-object v0, v0

    .line 177979
    const-string v1, "page_id"

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/HRD;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 177980
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 177981
    move-object v1, v0

    .line 177982
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 177983
    new-instance v2, LX/HW2;

    invoke-direct {v2, p0}, LX/HW2;-><init>(Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;)V

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 177984
    return-void

    .line 177985
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 177986
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 177987
    :cond_6
    iget-boolean v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->n:Z

    if-eqz v1, :cond_2

    .line 177988
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->setVisibility(I)V

    .line 177989
    const/4 v1, 0x2

    iput v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->p:I

    .line 177990
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->g:LX/HW4;

    invoke-virtual {v1}, LX/0gG;->kV_()V

    .line 177991
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->n:Z

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 177957
    return-void
.end method

.method public final b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 177954
    if-eqz p1, :cond_0

    const-class v0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177955
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    .line 177956
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177951
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 177952
    const-string v1, "profile_id"

    iget-wide v2, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177953
    return-object v0
.end method

.method public final c()LX/3zk;
    .locals 1

    .prologue
    .line 178041
    sget-object v0, LX/3zk;->PAGES:LX/3zk;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177950
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 177947
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/2SU;->u:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177948
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    sget-object v1, LX/103;->PAGE:LX/103;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->l:Ljava/lang/String;

    sget-object v4, LX/7B5;->TAB:LX/7B5;

    invoke-static/range {v0 .. v5}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    .line 177949
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    sget-object v1, LX/103;->PAGE:LX/103;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->l:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 177936
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 177937
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 177938
    if-eqz p3, :cond_0

    const-string v0, "show_snackbar_extra"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 177939
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->f:Landroid/view/ViewGroup;

    const-string v1, "show_snackbar_extra"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/HQz;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/3oW;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 177940
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    if-eqz v0, :cond_2

    .line 177941
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 177942
    :cond_1
    :goto_0
    return-void

    .line 177943
    :cond_2
    new-instance v0, LX/HW5;

    invoke-direct {v0, p1, p2, p3}, LX/HW5;-><init>(IILandroid/content/Intent;)V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->r:LX/0am;

    goto :goto_0

    .line 177944
    :cond_3
    if-nez p2, :cond_1

    .line 177945
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    if-eqz v0, :cond_1

    .line 177946
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x79c16a05

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 177920
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->A:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->u:Landroid/view/LayoutInflater;

    .line 177921
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->u:Landroid/view/LayoutInflater;

    const v2, 0x7f0306a6

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->f:Landroid/view/ViewGroup;

    .line 177922
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->f:Landroid/view/ViewGroup;

    const/4 v7, 0x0

    .line 177923
    iput-boolean v7, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->n:Z

    .line 177924
    iput-boolean v7, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->o:Z

    .line 177925
    iput v7, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->p:I

    .line 177926
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v5

    iput-object v5, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->J:LX/0gc;

    .line 177927
    new-instance v5, LX/HW4;

    iget-object v6, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->J:LX/0gc;

    invoke-direct {v5, p0, v6}, LX/HW4;-><init>(Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;LX/0gc;)V

    iput-object v5, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->g:LX/HW4;

    .line 177928
    const v5, 0x7f0d122e

    invoke-static {v0, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iput-object v5, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    .line 177929
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iget-object v6, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->g:LX/HW4;

    invoke-virtual {v5, v6}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 177930
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    .line 177931
    iput-boolean v7, v5, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 177932
    const v5, 0x7f0d122d

    invoke-static {v0, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    iput-object v5, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    .line 177933
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    iget-object v6, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v5, v6}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->setViewPager(Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;)V

    .line 177934
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    iget-wide v7, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    iget-object v6, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->i:Ljava/lang/String;

    invoke-virtual {v5, v7, v8, v6}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a(JLjava/lang/String;)V

    .line 177935
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->f:Landroid/view/ViewGroup;

    const/16 v2, 0x2b

    const v3, 0x67a79ff1

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x222178ae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 177911
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    if-eqz v0, :cond_0

    .line 177912
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 177913
    iput-object v2, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    .line 177914
    :cond_0
    iput-object v2, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->g:LX/HW4;

    .line 177915
    iput-object v2, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    .line 177916
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->D:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->t:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 177917
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1PJ;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, LX/1PJ;->a(Landroid/view/ViewGroup;)V

    .line 177918
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 177919
    const/16 v0, 0x2b

    const v2, -0x2668e9f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1bd0ae31

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 177908
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 177909
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->C:LX/CXj;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->I:LX/CXv;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 177910
    const/16 v1, 0x2b

    const v2, 0x6e30b76b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x70707436

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 177905
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 177906
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->C:LX/CXj;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->I:LX/CXv;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 177907
    const/16 v1, 0x2b

    const v2, 0x3b527641

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 177899
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 177900
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->m:LX/0Px;

    if-eqz v0, :cond_0

    .line 177901
    const-string v0, "extra_viewer_profile_permissions"

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->m:LX/0Px;

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 177902
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->l:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 177903
    const-string v0, "extra_page_name"

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177904
    :cond_1
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7582de02

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 177897
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 177898
    const/16 v1, 0x2b

    const v2, 0x4ca53cc3    # 8.663196E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 4

    .prologue
    .line 177888
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 177889
    if-nez p1, :cond_0

    .line 177890
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 177891
    if-eqz v0, :cond_0

    .line 177892
    invoke-virtual {v0}, Landroid/app/Activity;->closeContextMenu()V

    .line 177893
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->D:LX/0Ot;

    if-eqz v0, :cond_1

    .line 177894
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1PJ;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->t:Landroid/view/ViewGroup;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_STORY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-virtual {v0, v1, v2}, LX/1PJ;->a(Landroid/view/ViewGroup;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 177895
    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->k(Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;)V

    .line 177896
    :cond_1
    return-void
.end method
