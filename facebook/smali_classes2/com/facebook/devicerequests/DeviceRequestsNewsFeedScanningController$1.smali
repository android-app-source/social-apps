.class public final Lcom/facebook/devicerequests/DeviceRequestsNewsFeedScanningController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Zl;


# instance fields
.field public final synthetic a:LX/1Zk;


# direct methods
.method public constructor <init>(LX/1Zk;)V
    .locals 0

    .prologue
    .line 275307
    iput-object p1, p0, Lcom/facebook/devicerequests/DeviceRequestsNewsFeedScanningController$1;->a:LX/1Zk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/246;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 275289
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    .line 275290
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275291
    iget-object v0, p0, Lcom/facebook/devicerequests/DeviceRequestsNewsFeedScanningController$1;->a:LX/1Zk;

    iget-object v0, v0, LX/1Zk;->f:LX/0if;

    sget-object v1, LX/0ig;->aV:LX/0ih;

    const-wide/16 v4, 0x1

    const-string v3, "feed_discovered_service"

    invoke-virtual {v0, v1, v4, v5, v3}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 275292
    iget-object v0, p0, Lcom/facebook/devicerequests/DeviceRequestsNewsFeedScanningController$1;->a:LX/1Zk;

    iget-object v0, v0, LX/1Zk;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DAx;

    iget-object v1, p0, Lcom/facebook/devicerequests/DeviceRequestsNewsFeedScanningController$1;->a:LX/1Zk;

    iget-object v1, v1, LX/1Zk;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/246;

    const/4 v12, 0x1

    .line 275293
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 275294
    new-instance v6, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    sget-object v9, LX/DAx;->b:Landroid/net/Uri;

    invoke-direct {v6, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 275295
    new-instance v8, Landroid/content/Intent;

    const-class v9, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;

    invoke-direct {v8, v1, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v9, "android.intent.extra.INTENT"

    invoke-virtual {v8, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v8

    .line 275296
    new-instance v6, LX/3pm;

    invoke-direct {v6}, LX/3pm;-><init>()V

    const v9, 0x7f0f0132

    invoke-virtual {v7, v9, v12}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, LX/3pm;->a(Ljava/lang/CharSequence;)LX/3pm;

    move-result-object v6

    invoke-static {v7, v2}, LX/DAx;->a(Landroid/content/res/Resources;LX/246;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, LX/3pm;->c(Ljava/lang/CharSequence;)LX/3pm;

    move-result-object v9

    .line 275297
    iget-object v6, v0, LX/DAx;->c:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BAa;

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10}, Ljava/util/Date;-><init>()V

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, LX/BAa;->a(J)LX/BAa;

    move-result-object v6

    invoke-virtual {v6, v12}, LX/BAa;->b(Z)LX/BAa;

    move-result-object v6

    const v10, 0x7f0f0132

    invoke-virtual {v7, v10, v12}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v6, v10}, LX/BAa;->d(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v6

    const v10, 0x7f0f0133

    invoke-virtual {v7, v10, v12}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v6, v10}, LX/BAa;->c(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v6

    const v10, 0x7f0218e4

    invoke-virtual {v6, v10}, LX/BAa;->a(I)LX/BAa;

    move-result-object v6

    invoke-virtual {v6, v9}, LX/BAa;->a(LX/3pc;)LX/BAa;

    move-result-object v6

    invoke-static {v7, v2}, LX/DAx;->a(Landroid/content/res/Resources;LX/246;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/BAa;->a(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v7

    .line 275298
    iget-object v6, v2, LX/246;->e:Ljava/lang/String;

    move-object v6, v6

    .line 275299
    invoke-static {v6}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v9

    .line 275300
    const-class v6, LX/DAx;

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v10

    .line 275301
    iget-object v6, v0, LX/DAx;->e:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1HI;

    invoke-virtual {v6, v9, v10}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v6

    .line 275302
    new-instance v9, LX/DAw;

    invoke-direct {v9, v0, v7, v8}, LX/DAw;-><init>(LX/DAx;LX/BAa;Landroid/content/Intent;)V

    .line 275303
    sget-object v7, LX/131;->INSTANCE:LX/131;

    move-object v7, v7

    .line 275304
    invoke-interface {v6, v9, v7}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 275305
    iget-object v0, p0, Lcom/facebook/devicerequests/DeviceRequestsNewsFeedScanningController$1;->a:LX/1Zk;

    iget-object v0, v0, LX/1Zk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Zm;

    invoke-virtual {v0, p0}, LX/1Zm;->b(LX/1Zl;)Z

    .line 275306
    :cond_0
    return-void
.end method
