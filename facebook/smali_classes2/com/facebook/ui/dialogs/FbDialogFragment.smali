.class public Lcom/facebook/ui/dialogs/FbDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""

# interfaces
.implements LX/0f0;
.implements LX/02k;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public j:LX/4mg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:LX/31Z;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 115485
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 115486
    return-void
.end method

.method private b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 115483
    invoke-virtual {p1, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115484
    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public S_()Z
    .locals 1

    .prologue
    .line 115482
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 115443
    new-instance v0, LX/4mf;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/4mf;-><init>(Lcom/facebook/ui/dialogs/FbDialogFragment;Landroid/content/Context;I)V

    .line 115444
    invoke-static {v0}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 115445
    return-object v0
.end method

.method public final a(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 115480
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 115481
    invoke-static {v0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 115470
    invoke-direct {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 115471
    if-eqz v0, :cond_0

    .line 115472
    :goto_0
    return-object v0

    .line 115473
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 115474
    instance-of v1, v0, LX/0f0;

    if-eqz v1, :cond_1

    .line 115475
    check-cast v0, LX/0f0;

    invoke-interface {v0, p1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 115476
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 115477
    instance-of v1, v0, LX/0f0;

    if-eqz v1, :cond_2

    .line 115478
    check-cast v0, LX/0f0;

    invoke-interface {v0, p1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 115479
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 115463
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/app/DialogFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 115464
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 115465
    if-eqz v0, :cond_0

    .line 115466
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 115467
    const-string v1, "FbDialogFragment View Hierarchy:\n"

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 115468
    invoke-static {p1, p3, v0}, Landroid/support/v4/app/FragmentActivity;->a(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V

    .line 115469
    :cond_0
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 2

    .prologue
    .line 115487
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 115488
    iget-boolean v1, p0, Landroid/support/v4/app/DialogFragment;->d:Z

    move v1, v1

    .line 115489
    if-eqz v1, :cond_2

    .line 115490
    iget-object v1, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->m:LX/31Z;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->m:LX/31Z;

    invoke-virtual {v1}, LX/31Z;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    if-eq v1, v0, :cond_1

    .line 115491
    :cond_0
    new-instance v1, LX/31Z;

    invoke-direct {v1, v0}, LX/31Z;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->m:LX/31Z;

    .line 115492
    :cond_1
    iget-object v0, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->m:LX/31Z;

    .line 115493
    :goto_0
    return-object v0

    .line 115494
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->m:LX/31Z;

    goto :goto_0
.end method

.method public final h()Landroid/app/Activity;
    .locals 2

    .prologue
    .line 115462
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 115458
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mRemoving:Z

    move v0, v0

    .line 115459
    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115460
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v0

    .line 115461
    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 115455
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/DialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 115456
    iget-object v0, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->j:LX/4mg;

    invoke-virtual {v0, p1, p2, p3}, LX/4mg;->a(IILandroid/content/Intent;)V

    .line 115457
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x56b9f08f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115452
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 115453
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/ui/dialogs/FbDialogFragment;

    invoke-static {p1}, LX/4mg;->a(LX/0QB;)LX/4mg;

    move-result-object v4

    check-cast v4, LX/4mg;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    const/16 v1, 0x259

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v4, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->j:LX/4mg;

    iput-object v5, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->k:LX/0ad;

    iput-object p1, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->l:LX/0Ot;

    .line 115454
    const/16 v1, 0x2b

    const v2, -0x400a6399

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1b159c2f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115449
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroyView()V

    .line 115450
    iget-object v1, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->j:LX/4mg;

    invoke-virtual {v1, p0}, LX/4mg;->b(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    .line 115451
    const/16 v1, 0x2b

    const v2, 0x5f76dcb2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 115446
    iget-object v0, p0, Lcom/facebook/ui/dialogs/FbDialogFragment;->j:LX/4mg;

    invoke-virtual {v0, p0}, LX/4mg;->a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    .line 115447
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 115448
    return-void
.end method
