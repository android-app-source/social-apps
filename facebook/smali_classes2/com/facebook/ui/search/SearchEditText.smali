.class public Lcom/facebook/ui/search/SearchEditText;
.super Lcom/facebook/widget/text/BetterEditTextView;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private final b:Landroid/os/ResultReceiver;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View$OnTouchListener;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/view/inputmethod/InputMethodManager;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Z

.field public f:LX/7HK;

.field public g:LX/7HL;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/CharSequence;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 167621
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterEditTextView;-><init>(Landroid/content/Context;)V

    .line 167622
    new-instance v0, Lcom/facebook/ui/search/SearchEditText$1;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/facebook/ui/search/SearchEditText$1;-><init>(Lcom/facebook/ui/search/SearchEditText;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->b:Landroid/os/ResultReceiver;

    .line 167623
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->c:Ljava/util/Set;

    .line 167624
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->e()V

    .line 167625
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 167633
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 167634
    new-instance v0, Lcom/facebook/ui/search/SearchEditText$1;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/facebook/ui/search/SearchEditText$1;-><init>(Lcom/facebook/ui/search/SearchEditText;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->b:Landroid/os/ResultReceiver;

    .line 167635
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->c:Ljava/util/Set;

    .line 167636
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->e()V

    .line 167637
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 167638
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 167639
    new-instance v0, Lcom/facebook/ui/search/SearchEditText$1;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/facebook/ui/search/SearchEditText$1;-><init>(Lcom/facebook/ui/search/SearchEditText;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->b:Landroid/os/ResultReceiver;

    .line 167640
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->c:Ljava/util/Set;

    .line 167641
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->e()V

    .line 167642
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 167643
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167644
    :goto_0
    return-object p1

    .line 167645
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 167646
    :cond_1
    const-string p1, ""

    goto :goto_0

    .line 167647
    :cond_2
    const v0, 0x7f081e82

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 167648
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 167649
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 167650
    const-string v2, " "

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 167651
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 167652
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object p1, v0

    .line 167653
    goto :goto_0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 167654
    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 167655
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/facebook/ui/search/SearchEditText;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/ui/search/SearchEditText;Z)Z
    .locals 1

    .prologue
    .line 167656
    invoke-direct {p0, p1}, Lcom/facebook/ui/search/SearchEditText;->a(Z)Z

    move-result v0

    return v0
.end method

.method private a(Z)Z
    .locals 4

    .prologue
    .line 167657
    invoke-virtual {p0}, Lcom/facebook/ui/search/SearchEditText;->requestFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 167658
    :goto_0
    if-nez v0, :cond_2

    if-nez p1, :cond_2

    .line 167659
    new-instance v1, Lcom/facebook/ui/search/SearchEditText$4;

    invoke-direct {v1, p0}, Lcom/facebook/ui/search/SearchEditText$4;-><init>(Lcom/facebook/ui/search/SearchEditText;)V

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v3}, Lcom/facebook/ui/search/SearchEditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 167660
    :cond_0
    :goto_1
    return v0

    .line 167661
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 167662
    :cond_2
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ui/search/SearchEditText;->j:Z

    if-eqz v1, :cond_0

    .line 167663
    iget-object v1, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 167664
    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(I)Z
    .locals 1

    .prologue
    .line 167690
    const/16 v0, 0x17

    if-eq p0, v0, :cond_0

    const/16 v0, 0x42

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa0

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 167665
    invoke-virtual {p0, p0}, Lcom/facebook/ui/search/SearchEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 167666
    new-instance v0, LX/0zm;

    invoke-direct {v0, p0}, LX/0zm;-><init>(Lcom/facebook/ui/search/SearchEditText;)V

    invoke-super {p0, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 167667
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 167668
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    if-eqz v0, :cond_0

    .line 167669
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    invoke-interface {v0}, LX/7HK;->a()V

    .line 167670
    :cond_0
    invoke-static {p0}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 167671
    return-void
.end method

.method private declared-synchronized getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;
    .locals 2

    .prologue
    .line 167672
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->d:Landroid/view/inputmethod/InputMethodManager;

    if-nez v0, :cond_0

    .line 167673
    invoke-virtual {p0}, Lcom/facebook/ui/search/SearchEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->d:Landroid/view/inputmethod/InputMethodManager;

    .line 167674
    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->d:Landroid/view/inputmethod/InputMethodManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 167675
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 167676
    iget-object v1, p0, Lcom/facebook/ui/search/SearchEditText;->g:LX/7HL;

    if-nez v1, :cond_1

    .line 167677
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result v1

    .line 167678
    :goto_0
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/facebook/ui/search/SearchEditText;->e:Z

    .line 167679
    return v1

    .line 167680
    :cond_1
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/ui/search/SearchEditText;->b:Landroid/os/ResultReceiver;

    invoke-virtual {v1, p0, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    move-result v1

    goto :goto_0
.end method

.method public static i(Lcom/facebook/ui/search/SearchEditText;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 167681
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ui/search/SearchEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 167682
    iput-boolean v2, p0, Lcom/facebook/ui/search/SearchEditText;->e:Z

    .line 167683
    invoke-virtual {p0}, Lcom/facebook/ui/search/SearchEditText;->clearFocus()V

    .line 167684
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 167685
    invoke-virtual {p0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/ui/search/SearchEditText;->setSelection(I)V

    .line 167686
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 167687
    iget-boolean v0, p0, Lcom/facebook/ui/search/SearchEditText;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 167688
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 167689
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 167626
    invoke-super {p0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 167627
    iput-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    .line 167628
    iput-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->h:Ljava/lang/String;

    .line 167629
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ui/search/SearchEditText;->j:Z

    .line 167630
    return-void
.end method

.method public final a(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 167631
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167632
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 167588
    invoke-virtual {p0}, Lcom/facebook/ui/search/SearchEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ui/search/SearchEditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    invoke-static {v0, p1, p2, v1}, Lcom/facebook/ui/search/SearchEditText;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 167589
    iput-object p1, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    .line 167590
    iput-object p2, p0, Lcom/facebook/ui/search/SearchEditText;->h:Ljava/lang/String;

    .line 167591
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/ui/search/SearchEditText;->j:Z

    .line 167592
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 167593
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/ui/search/SearchEditText;->setSelection(I)V

    .line 167594
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 167586
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 167587
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 167585
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/ui/search/SearchEditText;->a(Z)Z

    move-result v0

    return v0
.end method

.method public final clearFocus()V
    .locals 2

    .prologue
    .line 167579
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/ui/search/SearchEditText;->setFocusableInTouchMode(Z)V

    .line 167580
    invoke-super {p0}, Lcom/facebook/widget/text/BetterEditTextView;->clearFocus()V

    .line 167581
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/ui/search/SearchEditText;->setFocusableInTouchMode(Z)V

    .line 167582
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 167583
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/facebook/ui/search/SearchEditText;->h:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/ui/search/SearchEditText;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 167584
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 167577
    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 167578
    return-void
.end method

.method public getText()Landroid/text/Editable;
    .locals 2

    .prologue
    .line 167574
    iget-boolean v0, p0, Lcom/facebook/ui/search/SearchEditText;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 167575
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v0

    .line 167576
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 167573
    invoke-virtual {p0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public final onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3

    .prologue
    .line 167570
    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    .line 167571
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v2, -0x40000001    # -1.9999999f

    and-int/2addr v1, v2

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 167572
    return-object v0
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 167566
    invoke-static {p2}, Lcom/facebook/ui/search/SearchEditText;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167567
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->f()V

    .line 167568
    const/4 v0, 0x1

    .line 167569
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 167595
    invoke-static {p1, p2}, Lcom/facebook/ui/search/SearchEditText;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167596
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->f()V

    .line 167597
    const/4 v0, 0x1

    .line 167598
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterEditTextView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    .line 167599
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-static {v0}, Lcom/facebook/ui/search/SearchEditText;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167600
    new-instance v0, Lcom/facebook/ui/search/SearchEditText$3;

    invoke-direct {v0, p0}, Lcom/facebook/ui/search/SearchEditText$3;-><init>(Lcom/facebook/ui/search/SearchEditText;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/ui/search/SearchEditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 167601
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterEditTextView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 167602
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/text/BetterEditTextView;->onLayout(ZIIII)V

    .line 167603
    iget-boolean v0, p0, Lcom/facebook/ui/search/SearchEditText;->e:Z

    if-eqz v0, :cond_0

    .line 167604
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ui/search/SearchEditText;->e:Z

    .line 167605
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->h()Z

    .line 167606
    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x408ea0de

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 167607
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 167608
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->k()V

    .line 167609
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x6b084579

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setOnSubmitListener(LX/7HK;)V
    .locals 0

    .prologue
    .line 167610
    iput-object p1, p0, Lcom/facebook/ui/search/SearchEditText;->f:LX/7HK;

    .line 167611
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 2

    .prologue
    .line 167612
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use addOnTouchListener instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setSoftKeyboardListener(LX/7HL;)V
    .locals 0

    .prologue
    .line 167613
    iput-object p1, p0, Lcom/facebook/ui/search/SearchEditText;->g:LX/7HL;

    .line 167614
    return-void
.end method

.method public final setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    .prologue
    .line 167615
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 167616
    invoke-direct {p0}, Lcom/facebook/ui/search/SearchEditText;->j()V

    .line 167617
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ui/search/SearchEditText;->j:Z

    .line 167618
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ui/search/SearchEditText;->h:Ljava/lang/String;

    .line 167619
    iput-object p1, p0, Lcom/facebook/ui/search/SearchEditText;->i:Ljava/lang/CharSequence;

    .line 167620
    return-void
.end method
