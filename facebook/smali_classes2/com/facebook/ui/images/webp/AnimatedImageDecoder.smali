.class public Lcom/facebook/ui/images/webp/AnimatedImageDecoder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:Lcom/facebook/ui/images/webp/AnimatedImageDecoder;


# instance fields
.field private final b:Lcom/facebook/bitmaps/NativeImageLibraries;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 225367
    const-class v0, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    sput-object v0, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/bitmaps/NativeImageLibraries;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225365
    iput-object p1, p0, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->b:Lcom/facebook/bitmaps/NativeImageLibraries;

    .line 225366
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/ui/images/webp/AnimatedImageDecoder;
    .locals 3

    .prologue
    .line 225354
    sget-object v0, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->c:Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    if-nez v0, :cond_1

    .line 225355
    const-class v1, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    monitor-enter v1

    .line 225356
    :try_start_0
    sget-object v0, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->c:Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225357
    if-eqz v2, :cond_0

    .line 225358
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->b(LX/0QB;)Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    move-result-object v0

    sput-object v0, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->c:Lcom/facebook/ui/images/webp/AnimatedImageDecoder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225359
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225360
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225361
    :cond_1
    sget-object v0, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->c:Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    return-object v0

    .line 225362
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225363
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a([B)Z
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v0, 0x0

    .line 225347
    new-array v2, v5, [B

    fill-array-data v2, :array_0

    .line 225348
    array-length v1, p0

    const/16 v3, 0x8

    if-ge v1, v3, :cond_1

    .line 225349
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 225350
    :goto_1
    if-ge v1, v5, :cond_2

    .line 225351
    aget-byte v3, v2, v1

    aget-byte v4, p0, v1

    if-ne v3, v4, :cond_0

    .line 225352
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 225353
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    nop

    :array_0
    .array-data 1
        0x46t
        0x42t
        0x41t
        0x31t
    .end array-data
.end method

.method private static b([B)I
    .locals 2

    .prologue
    .line 225346
    const/4 v0, 0x4

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    const/4 v1, 0x5

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/ui/images/webp/AnimatedImageDecoder;
    .locals 2

    .prologue
    .line 225289
    new-instance v1, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;

    invoke-static {p0}, Lcom/facebook/bitmaps/NativeImageLibraries;->a(LX/0QB;)Lcom/facebook/bitmaps/NativeImageLibraries;

    move-result-object v0

    check-cast v0, Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-direct {v1, v0}, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;-><init>(Lcom/facebook/bitmaps/NativeImageLibraries;)V

    .line 225290
    return-object v1
.end method

.method private static c([B)I
    .locals 2

    .prologue
    .line 225345
    const/4 v0, 0x6

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    const/4 v1, 0x7

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method private static native nativeDecode([BIII)Lcom/facebook/ui/images/webp/AnimatedImageDecoder$TranscodedWebPImageWrapper;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method


# virtual methods
.method public final a([BIII)LX/4nB;
    .locals 16

    .prologue
    .line 225292
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->a()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 225293
    :goto_0
    return-object v2

    .line 225294
    :cond_0
    const/4 v3, -0x1

    .line 225295
    const/4 v2, -0x1

    .line 225296
    invoke-static/range {p1 .. p1}, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->a([B)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 225297
    invoke-static/range {p1 .. p1}, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->b([B)I

    move-result v3

    .line 225298
    invoke-static/range {p1 .. p1}, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->c([B)I

    move-result v2

    .line 225299
    move-object/from16 v0, p1

    array-length v4, v0

    add-int/lit8 v4, v4, -0x8

    new-array v4, v4, [B

    .line 225300
    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object/from16 v0, p1

    array-length v7, v0

    add-int/lit8 v7, v7, -0x8

    move-object/from16 v0, p1

    invoke-static {v0, v5, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v6, v2

    move-object/from16 p1, v4

    move v2, v3

    .line 225301
    :goto_1
    move-object/from16 v0, p1

    array-length v3, v0

    const/16 v4, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v3, v1, v4}, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->nativeDecode([BIII)Lcom/facebook/ui/images/webp/AnimatedImageDecoder$TranscodedWebPImageWrapper;

    move-result-object v3

    .line 225302
    if-nez v3, :cond_1

    .line 225303
    const/4 v2, 0x0

    goto :goto_0

    .line 225304
    :cond_1
    iget v5, v3, Lcom/facebook/ui/images/webp/AnimatedImageDecoder$TranscodedWebPImageWrapper;->width:I

    .line 225305
    iget v4, v3, Lcom/facebook/ui/images/webp/AnimatedImageDecoder$TranscodedWebPImageWrapper;->height:I

    .line 225306
    iget-boolean v8, v3, Lcom/facebook/ui/images/webp/AnimatedImageDecoder$TranscodedWebPImageWrapper;->isAnimated:Z

    .line 225307
    iget-boolean v7, v3, Lcom/facebook/ui/images/webp/AnimatedImageDecoder$TranscodedWebPImageWrapper;->isLooped:Z

    .line 225308
    invoke-virtual {v3}, Lcom/facebook/ui/images/webp/AnimatedImageDecoder$TranscodedWebPImageWrapper;->getJpegFrames()Ljava/util/List;

    move-result-object v10

    .line 225309
    iget-object v11, v3, Lcom/facebook/ui/images/webp/AnimatedImageDecoder$TranscodedWebPImageWrapper;->frameDurations:[I

    .line 225310
    const/high16 v9, 0x3f800000    # 1.0f

    .line 225311
    const/high16 v3, 0x3f800000    # 1.0f

    .line 225312
    if-lez p3, :cond_2

    move/from16 v0, p3

    if-ge v0, v5, :cond_2

    .line 225313
    move/from16 v0, p3

    int-to-float v9, v0

    int-to-float v12, v5

    div-float/2addr v9, v12

    .line 225314
    :cond_2
    if-lez p4, :cond_3

    move/from16 v0, p4

    if-ge v0, v4, :cond_3

    .line 225315
    move/from16 v0, p4

    int-to-float v3, v0

    int-to-float v12, v4

    div-float/2addr v3, v12

    .line 225316
    :cond_3
    invoke-static {v9, v3}, Ljava/lang/Math;->min(FF)F

    move-result v9

    .line 225317
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v12

    .line 225318
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v13

    .line 225319
    if-ltz v2, :cond_a

    move v3, v2

    .line 225320
    :goto_2
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    const/4 v5, 0x1

    if-le v2, v5, :cond_5

    .line 225321
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 225322
    const/4 v2, 0x1

    iput-boolean v2, v6, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 225323
    const/4 v2, 0x1

    iput-boolean v2, v6, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 225324
    const/high16 v2, 0x3f800000    # 1.0f

    div-float/2addr v2, v9

    float-to-int v2, v2

    iput v2, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 225325
    const/4 v2, 0x0

    move v5, v2

    :goto_3
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_4

    .line 225326
    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 225327
    const/4 v9, 0x0

    array-length v14, v2

    invoke-static {v2, v9, v14, v6}, LX/436;->a([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v12, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 225328
    aget v2, v11, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v13, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 225329
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    :cond_4
    move v6, v7

    move v5, v8

    .line 225330
    :goto_4
    new-instance v2, LX/4nB;

    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v13}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, LX/4nB;-><init>(IIZZLjava/util/List;Ljava/util/List;)V

    goto/16 :goto_0

    .line 225331
    :cond_5
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    const/4 v7, 0x0

    const/4 v5, 0x0

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    array-length v5, v5

    invoke-static {v2, v7, v5}, LX/436;->a([BII)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 225332
    if-ltz v6, :cond_7

    const/4 v2, 0x1

    .line 225333
    :goto_5
    const/4 v5, -0x1

    if-ne v6, v5, :cond_6

    .line 225334
    const/16 v6, 0xc

    .line 225335
    :cond_6
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int v8, v5, v3

    .line 225336
    const/4 v5, 0x0

    :goto_6
    if-ge v5, v8, :cond_9

    .line 225337
    mul-int v10, v3, v5

    const/4 v11, 0x0

    invoke-static {v7, v10, v11, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 225338
    const/high16 v11, 0x3f800000    # 1.0f

    cmpg-float v11, v9, v11

    if-gez v11, :cond_8

    .line 225339
    int-to-float v11, v3

    mul-float/2addr v11, v9

    float-to-int v11, v11

    int-to-float v14, v4

    mul-float/2addr v14, v9

    float-to-int v14, v14

    const/4 v15, 0x1

    invoke-static {v10, v11, v14, v15}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 225340
    invoke-virtual {v12, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 225341
    :goto_7
    const/16 v10, 0x3e8

    div-int/2addr v10, v6

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v13, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 225342
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 225343
    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    .line 225344
    :cond_8
    invoke-virtual {v12, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    :cond_9
    move v6, v2

    move v5, v2

    goto :goto_4

    :cond_a
    move v3, v5

    goto/16 :goto_2

    :cond_b
    move v6, v2

    move v2, v3

    goto/16 :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 225291
    iget-object v0, p0, Lcom/facebook/ui/images/webp/AnimatedImageDecoder;->b:Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-virtual {v0}, LX/03m;->T_()Z

    move-result v0

    return v0
.end method
