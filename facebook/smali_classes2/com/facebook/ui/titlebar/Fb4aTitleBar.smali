.class public Lcom/facebook/ui/titlebar/Fb4aTitleBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0h4;
.implements LX/0h5;


# static fields
.field private static final n:Z


# instance fields
.field public a:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0wL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0hI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/view/ViewGroup;

.field public final g:Landroid/widget/LinearLayout;

.field public final h:Landroid/widget/LinearLayout;

.field public i:Lcom/facebook/fbui/glyph/GlyphView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0wI;

.field public k:Landroid/view/View$OnClickListener;

.field public l:LX/0zE;

.field public m:Landroid/widget/FrameLayout;

.field private o:Z

.field private final p:LX/0wP;

.field private final q:LX/0wP;

.field private final r:LX/0wP;

.field private s:Landroid/widget/ImageView;

.field private t:LX/7HM;

.field public final u:LX/0wJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 114999
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->n:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 114959
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 114960
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 114961
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114962
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 114935
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114936
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->o:Z

    .line 114937
    sget-object v0, LX/0wI;->TEXT:LX/0wI;

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->j:LX/0wI;

    .line 114938
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->t:LX/7HM;

    .line 114939
    new-instance v0, LX/0wJ;

    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wJ;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->u:LX/0wJ;

    .line 114940
    const-class v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {v0, p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 114941
    const-string v1, "titlebar"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V

    .line 114942
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0314ff

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 114943
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    .line 114944
    const v0, 0x7f0d2f70

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->f:Landroid/view/ViewGroup;

    .line 114945
    const v0, 0x7f0d2f72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->g:Landroid/widget/LinearLayout;

    .line 114946
    const v0, 0x7f0d2f6e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->h:Landroid/widget/LinearLayout;

    .line 114947
    new-instance v0, LX/0wP;

    const v1, 0x7f031503

    const v2, 0x7f031504

    invoke-direct {v0, p0, v1, v2}, LX/0wP;-><init>(Lcom/facebook/ui/titlebar/Fb4aTitleBar;II)V

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    .line 114948
    new-instance v0, LX/0wP;

    const v1, 0x7f031506

    const v2, 0x7f031507

    invoke-direct {v0, p0, v1, v2}, LX/0wP;-><init>(Lcom/facebook/ui/titlebar/Fb4aTitleBar;II)V

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    .line 114949
    new-instance v0, LX/0wP;

    const v1, 0x7f031501

    const v2, 0x7f031502

    invoke-direct {v0, p0, v1, v2}, LX/0wP;-><init>(Lcom/facebook/ui/titlebar/Fb4aTitleBar;II)V

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->r:LX/0wP;

    .line 114950
    const v0, 0x7f0d2f6d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    .line 114951
    const v0, 0x7f0d2f6f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->m:Landroid/widget/FrameLayout;

    .line 114952
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->c:LX/0wL;

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LX/0wL;->a(Landroid/view/View;I)V

    .line 114953
    invoke-direct {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i()V

    .line 114954
    sget-object v0, LX/0wI;->TEXT:LX/0wI;

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitleBarState(LX/0wI;)V

    .line 114955
    return-void
.end method

.method private static a(Lcom/facebook/ui/titlebar/Fb4aTitleBar;LX/0hL;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0wL;LX/0hI;)V
    .locals 0

    .prologue
    .line 114963
    iput-object p1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a:LX/0hL;

    iput-object p2, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->b:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p3, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->c:LX/0wL;

    iput-object p4, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->d:LX/0hI;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {v3}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v0

    check-cast v0, LX/0hL;

    invoke-static {v3}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static {v3}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v2

    check-cast v2, LX/0wL;

    invoke-static {v3}, LX/0hI;->a(LX/0QB;)LX/0hI;

    move-result-object v3

    check-cast v3, LX/0hI;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Lcom/facebook/ui/titlebar/Fb4aTitleBar;LX/0hL;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0wL;LX/0hI;)V

    return-void
.end method

.method private b(Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114964
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f02196a

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundResource(I)V

    .line 114965
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b103f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setMinimumWidth(I)V

    .line 114966
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->c:LX/0wL;

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0wL;->a(Landroid/view/View;I)V

    .line 114967
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114968
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 114969
    return-void
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 114970
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getHeightSupportTransparentStatusBar()I
    .locals 2

    .prologue
    .line 114971
    sget-boolean v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->n:Z

    if-eqz v0, :cond_0

    .line 114972
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->d:LX/0hI;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v0

    .line 114973
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 114974
    invoke-direct {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i()V

    .line 114975
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114976
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundResource(I)V

    .line 114977
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 114978
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 114979
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 114980
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setMinimumWidth(I)V

    .line 114981
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 114982
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->t:LX/7HM;

    if-nez v0, :cond_0

    .line 114983
    new-instance v0, LX/7HM;

    invoke-direct {v0, p0}, LX/7HM;-><init>(Lcom/facebook/ui/titlebar/Fb4aTitleBar;)V

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->t:LX/7HM;

    .line 114984
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->t:LX/7HM;

    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getTitle()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7HM;->a:Ljava/lang/String;

    .line 114985
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->t:LX/7HM;

    invoke-direct {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->g()Z

    move-result v1

    iput-boolean v1, v0, LX/7HM;->b:Z

    .line 114986
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114987
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v0, :cond_0

    .line 114988
    invoke-direct {p0, p1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->b(Landroid/view/View$OnClickListener;)V

    .line 114989
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a:LX/0hL;

    const v2, 0x7f020756

    invoke-virtual {v1, v2}, LX/0hL;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114990
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 115019
    sget-boolean v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->n:Z

    if-eqz v0, :cond_0

    .line 115020
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->o:Z

    if-nez v0, :cond_1

    .line 115021
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->o:Z

    .line 115022
    invoke-direct {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getHeightSupportTransparentStatusBar()I

    move-result v0

    invoke-virtual {p0, v1, v0, v1, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPadding(IIII)V

    .line 115023
    :cond_0
    :goto_0
    return-void

    .line 115024
    :cond_1
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->o:Z

    if-eqz v0, :cond_0

    .line 115025
    iput-boolean v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->o:Z

    .line 115026
    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPadding(IIII)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 114991
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->t:LX/7HM;

    if-eqz v0, :cond_0

    .line 114992
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->t:LX/7HM;

    iget-object v0, v0, LX/7HM;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 114993
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->t:LX/7HM;

    iget-boolean v0, v0, LX/7HM;->b:Z

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 114994
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->t:LX/7HM;

    .line 114995
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 115027
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->r:LX/0wP;

    .line 115028
    iget-object v1, v0, LX/0wP;->f:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-object v0, v1

    .line 115029
    if-eqz v0, :cond_0

    .line 115030
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e()V

    .line 115031
    :goto_0
    return-void

    .line 115032
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->d()V

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 115033
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    if-nez v0, :cond_0

    .line 115034
    :goto_0
    return-void

    .line 115035
    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 115036
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setMinimumWidth(I)V

    .line 115037
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 115038
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->c:LX/0wL;

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LX/0wL;->a(Landroid/view/View;I)V

    .line 115039
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115040
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public final d_(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 115041
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->f:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 115042
    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setCustomTitleView(Landroid/view/View;)V

    .line 115043
    return-object v0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 115014
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    if-nez v0, :cond_0

    .line 115015
    :goto_0
    return-void

    .line 115016
    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 115017
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->c:LX/0wL;

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LX/0wL;->a(Landroid/view/View;I)V

    .line 115018
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public getBadgablePrimaryActionButtonView()Lcom/facebook/apptab/glyph/BadgableGlyphView;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 115044
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    .line 115045
    iget-object v1, v0, LX/0wP;->b:Landroid/view/View;

    move-object v0, v1

    .line 115046
    instance-of v0, v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    if-eqz v0, :cond_0

    .line 115047
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    .line 115048
    iget-object v1, v0, LX/0wP;->b:Landroid/view/View;

    move-object v0, v1

    .line 115049
    check-cast v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    .line 115050
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBadgableSecondaryActionButtonView()Lcom/facebook/apptab/glyph/BadgableGlyphView;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 115007
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    .line 115008
    iget-object v1, v0, LX/0wP;->b:Landroid/view/View;

    move-object v0, v1

    .line 115009
    instance-of v0, v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    if-eqz v0, :cond_0

    .line 115010
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    .line 115011
    iget-object v1, v0, LX/0wP;->b:Landroid/view/View;

    move-object v0, v1

    .line 115012
    check-cast v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    .line 115013
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getButtonWidths()I
    .locals 1

    .prologue
    .line 115006
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    return v0
.end method

.method public getLeftActionButton()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 115003
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->r:LX/0wP;

    iget-object v0, v0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-eqz v0, :cond_0

    .line 115004
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->r:LX/0wP;

    iget-object v0, v0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 115005
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->r:LX/0wP;

    iget-object v0, v0, LX/0wP;->b:Landroid/view/View;

    goto :goto_0
.end method

.method public getPrimaryActionButton()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 115000
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    iget-object v0, v0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-eqz v0, :cond_0

    .line 115001
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    iget-object v0, v0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 115002
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    iget-object v0, v0, LX/0wP;->b:Landroid/view/View;

    goto :goto_0
.end method

.method public getPrimaryActionButtonTextView()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 114934
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    iget-object v0, v0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    return-object v0
.end method

.method public getPrimaryButtonSpec()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 1

    .prologue
    .line 114996
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    .line 114997
    iget-object p0, v0, LX/0wP;->f:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-object v0, p0

    .line 114998
    return-object v0
.end method

.method public getSecondaryActionButton()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 114956
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    iget-object v0, v0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-eqz v0, :cond_0

    .line 114957
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    iget-object v0, v0, LX/0wP;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 114958
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    iget-object v0, v0, LX/0wP;->b:Landroid/view/View;

    goto :goto_0
.end method

.method public getSecondaryActionButtonOnClickListener()LX/107;
    .locals 1

    .prologue
    .line 114826
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    .line 114827
    iget-object p0, v0, LX/0wP;->h:LX/107;

    move-object v0, p0

    .line 114828
    return-object v0
.end method

.method public getSecondaryButtonSpec()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 1

    .prologue
    .line 114829
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    .line 114830
    iget-object p0, v0, LX/0wP;->f:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-object v0, p0

    .line 114831
    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114832
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 114833
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->onLayout(ZIIII)V

    .line 114834
    if-eqz p1, :cond_0

    .line 114835
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->l:LX/0zE;

    if-eqz v0, :cond_0

    .line 114836
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->l:LX/0zE;

    invoke-interface {v0}, LX/0zE;->a()V

    .line 114837
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 114838
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0429

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-boolean v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->o:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getHeightSupportTransparentStatusBar()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 114839
    return-void

    .line 114840
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setActionButtonOnClickListener(LX/107;)V
    .locals 1

    .prologue
    .line 114841
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    .line 114842
    iput-object p1, v0, LX/0wP;->h:LX/107;

    .line 114843
    return-void
.end method

.method public setButtonSpecs(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 114844
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_0

    .line 114845
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 114846
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 114847
    const/4 v3, 0x2

    invoke-static {p1, v3, v2}, LX/0Ph;->a(Ljava/lang/Iterable;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 114848
    :goto_0
    iget-object v3, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    .line 114849
    invoke-static {v3, v0, v4}, LX/0wP;->a$redex0(LX/0wP;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;Z)V

    .line 114850
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    .line 114851
    invoke-static {v0, v1, v4}, LX/0wP;->a$redex0(LX/0wP;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;Z)V

    .line 114852
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->r:LX/0wP;

    .line 114853
    invoke-static {v0, v2, v5}, LX/0wP;->a$redex0(LX/0wP;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;Z)V

    .line 114854
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 114855
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 114856
    return-void

    .line 114857
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 114858
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-object v1, v2

    goto :goto_0

    :cond_1
    move-object v1, v2

    move-object v0, v2

    goto :goto_0
.end method

.method public setCustomTitleView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114859
    goto :goto_1

    .line 114860
    :goto_0
    return-void

    .line 114861
    :goto_1
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 114862
    if-nez p1, :cond_0

    .line 114863
    sget-object v0, LX/0wI;->TEXT:LX/0wI;

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitleBarState(LX/0wI;)V

    goto :goto_0

    .line 114864
    :cond_0
    sget-object v0, LX/0wI;->CUSTOM:LX/0wI;

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitleBarState(LX/0wI;)V

    .line 114865
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setHasBackButton(Z)V
    .locals 0

    .prologue
    .line 114866
    return-void
.end method

.method public setHasFbLogo(Z)V
    .locals 0

    .prologue
    .line 114867
    if-eqz p1, :cond_0

    .line 114868
    invoke-direct {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->h()V

    .line 114869
    :goto_0
    return-void

    .line 114870
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->c()V

    goto :goto_0
.end method

.method public setLeftActionButtonOnClickListener(LX/107;)V
    .locals 1

    .prologue
    .line 114871
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->r:LX/0wP;

    .line 114872
    iput-object p1, v0, LX/0wP;->h:LX/107;

    .line 114873
    return-void
.end method

.method public setLeftButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2
    .param p1    # Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114874
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->r:LX/0wP;

    const/4 v1, 0x1

    .line 114875
    invoke-static {v0, p1, v1}, LX/0wP;->a$redex0(LX/0wP;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;Z)V

    .line 114876
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 114877
    return-void
.end method

.method public setOnBackPressedListener(LX/63J;)V
    .locals 0

    .prologue
    .line 114878
    return-void
.end method

.method public setOnSearchClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 114879
    iput-object p1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->k:Landroid/view/View$OnClickListener;

    .line 114880
    return-void
.end method

.method public setOnSizeChangedListener(LX/0zE;)V
    .locals 0

    .prologue
    .line 114881
    iput-object p1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->l:LX/0zE;

    .line 114882
    return-void
.end method

.method public setOnToolbarButtonListener(LX/63W;)V
    .locals 1
    .param p1    # LX/63W;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114883
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    .line 114884
    iput-object p1, v0, LX/0wP;->g:LX/63W;

    .line 114885
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    .line 114886
    iput-object p1, v0, LX/0wP;->g:LX/63W;

    .line 114887
    return-void
.end method

.method public setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3
    .param p1    # Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 114888
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->p:LX/0wP;

    .line 114889
    invoke-static {v0, p1, v2}, LX/0wP;->a$redex0(LX/0wP;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;Z)V

    .line 114890
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    const/4 v1, 0x0

    .line 114891
    invoke-static {v0, v1, v2}, LX/0wP;->a$redex0(LX/0wP;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;Z)V

    .line 114892
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 114893
    return-void
.end method

.method public setSearchButtonVisible(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 114894
    if-eqz p1, :cond_2

    .line 114895
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 114896
    :cond_0
    :goto_0
    return-void

    .line 114897
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031505

    iget-object v2, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    .line 114898
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114899
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->g:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 114900
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 114901
    :cond_2
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 114902
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->g:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 114903
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->s:Landroid/widget/ImageView;

    goto :goto_0
.end method

.method public setSecondaryActionButtonOnClickListener(LX/107;)V
    .locals 1

    .prologue
    .line 114904
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    .line 114905
    iput-object p1, v0, LX/0wP;->h:LX/107;

    .line 114906
    return-void
.end method

.method public setSecondaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2
    .param p1    # Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114907
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->q:LX/0wP;

    const/4 v1, 0x0

    .line 114908
    invoke-static {v0, p1, v1}, LX/0wP;->a$redex0(LX/0wP;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;Z)V

    .line 114909
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 114910
    return-void
.end method

.method public setShowDividers(Z)V
    .locals 0

    .prologue
    .line 114911
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 114912
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 114913
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 114914
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114915
    sget-object v0, LX/0wI;->TEXT:LX/0wI;

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitleBarState(LX/0wI;)V

    .line 114916
    return-void
.end method

.method public setTitleBarState(LX/0wI;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 114917
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->j:LX/0wI;

    .line 114918
    if-ne v0, p1, :cond_0

    .line 114919
    :goto_0
    return-void

    .line 114920
    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 114921
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114922
    sget-object v0, LX/0zJ;->a:[I

    invoke-virtual {p1}, LX/0wI;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 114923
    :goto_1
    iput-object p1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->j:LX/0wI;

    goto :goto_0

    .line 114924
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 114925
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setTitlebarAsModal(Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114926
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setHasBackButton(Z)V

    .line 114927
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v0, :cond_0

    .line 114928
    invoke-direct {p0, p1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->b(Landroid/view/View$OnClickListener;)V

    .line 114929
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a:LX/0hL;

    const v2, 0x7f02020b

    invoke-virtual {v1, v2}, LX/0hL;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114930
    :cond_0
    return-void
.end method

.method public setUpButtonColor(I)V
    .locals 1

    .prologue
    .line 114931
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v0, :cond_0

    .line 114932
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 114933
    :cond_0
    return-void
.end method
