.class public Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;
.super LX/0h3;
.source ""

# interfaces
.implements LX/0h9;
.implements LX/0hA;


# instance fields
.field public n:LX/0wS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cvp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 114810
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 114811
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 114808
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114809
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 114805
    invoke-direct {p0, p1, p2, p3}, LX/0h3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114806
    const-class v0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-static {v0, p0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 114807
    return-void
.end method

.method public static synthetic a(Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;)Landroid/view/View;
    .locals 1

    .prologue
    .line 114804
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getPrimaryActionButton()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;LX/0wS;LX/0hL;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;",
            "LX/0wS;",
            "LX/0hL;",
            "LX/0Ot",
            "<",
            "LX/Cvp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114803
    iput-object p1, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->n:LX/0wS;

    iput-object p2, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->o:LX/0hL;

    iput-object p3, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->p:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-static {v2}, LX/0wS;->b(LX/0QB;)LX/0wS;

    move-result-object v0

    check-cast v0, LX/0wS;

    invoke-static {v2}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v1

    check-cast v1, LX/0hL;

    const/16 v3, 0x32d8

    invoke-static {v2, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->a(Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;LX/0wS;LX/0hL;LX/0Ot;)V

    return-void
.end method

.method private static a(Ljava/util/List;)Z
    .locals 3
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 114777
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static synthetic b(Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;)Landroid/view/View;
    .locals 1

    .prologue
    .line 114791
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getSecondaryActionButton()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 114790
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final D_(I)V
    .locals 3

    .prologue
    .line 114778
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    if-eqz v0, :cond_1

    .line 114779
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->i()Z

    move-result v0

    if-nez v0, :cond_0

    if-lez p1, :cond_0

    .line 114780
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    .line 114781
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "graph_search_search_field_badge_impression"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "typeahead"

    .line 114782
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 114783
    move-object v1, v1

    .line 114784
    const-string v2, "search_field_unread_badge_count"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 114785
    iget-object v2, v0, LX/Cvp;->b:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 114786
    const/4 v2, 0x3

    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114787
    invoke-virtual {v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 114788
    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v0, p1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setNullStateBadge(I)V

    .line 114789
    :cond_1
    return-void
.end method

.method public final a(LX/0zI;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 114792
    sget-object v0, LX/0wI;->SEARCH_TITLES_APP:LX/0wI;

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->setTitleBarState(LX/0wI;)V

    .line 114793
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 114794
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 114795
    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->setFocusable(Z)V

    .line 114796
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Landroid/view/View$OnClickListener;)V

    .line 114797
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 114798
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 114799
    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->setLongClickable(Z)V

    .line 114800
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x104000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 114801
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v0, p1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setSearchBoxType(LX/0zI;)V

    .line 114802
    return-void
.end method

.method public final b(F)V
    .locals 1

    .prologue
    .line 114734
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    if-eqz v0, :cond_0

    .line 114735
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v0, p1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(F)V

    .line 114736
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 114737
    if-eqz p1, :cond_0

    .line 114738
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->n:LX/0wS;

    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getButtonWidths()I

    move-result v1

    new-instance v2, LX/Heb;

    invoke-direct {v2, p0}, LX/Heb;-><init>(Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;)V

    .line 114739
    sget-object p1, LX/Fib;->SHOW:LX/Fib;

    invoke-static {v0, p0, p1, v1, v2}, LX/0wS;->a(LX/0wS;Landroid/view/View;LX/Fib;ILandroid/animation/Animator$AnimatorListener;)V

    .line 114740
    :goto_0
    return-void

    .line 114741
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 114742
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->n:LX/0wS;

    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getButtonWidths()I

    move-result v1

    new-instance v2, LX/Hec;

    invoke-direct {v2, p0}, LX/Hec;-><init>(Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;)V

    .line 114743
    sget-object p1, LX/Fib;->HIDE:LX/Fib;

    invoke-static {v0, p0, p1, v1, v2}, LX/0wS;->a(LX/0wS;Landroid/view/View;LX/Fib;ILandroid/animation/Animator$AnimatorListener;)V

    .line 114744
    goto :goto_0
.end method

.method public final c(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 114745
    iget-object v3, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setVisibility(I)V

    .line 114746
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114747
    return-void

    :cond_0
    move v0, v2

    .line 114748
    goto :goto_0

    :cond_1
    move v2, v1

    .line 114749
    goto :goto_1
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 114750
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g()V

    .line 114751
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 114752
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->e()V

    .line 114753
    return-void
.end method

.method public getSearchBox()Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;
    .locals 1

    .prologue
    .line 114754
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 114755
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->f()V

    .line 114756
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x759d1f40

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 114757
    iget-object v1, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->n:LX/0wS;

    invoke-virtual {v1}, LX/0wS;->a()V

    .line 114758
    invoke-super {p0}, LX/0h3;->onDetachedFromWindow()V

    .line 114759
    const/16 v1, 0x2d

    const v2, 0x5b74315d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setButtonSpecsWithAnimation(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114760
    invoke-static {p1}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114761
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->n:LX/0wS;

    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getButtonWidths()I

    move-result v1

    invoke-virtual {v0, p0, v1}, LX/0wS;->a(Landroid/view/View;I)V

    .line 114762
    :goto_0
    return-void

    .line 114763
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 114764
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->n:LX/0wS;

    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getButtonWidths()I

    move-result v1

    invoke-virtual {v0, p0, v1}, LX/0wS;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public setTitleBarState(LX/0wI;)V
    .locals 2

    .prologue
    .line 114765
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->j:LX/0wI;

    if-ne p1, v0, :cond_1

    .line 114766
    :cond_0
    :goto_0
    return-void

    .line 114767
    :cond_1
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->j:LX/0wI;

    sget-object v1, LX/0wI;->SEARCH_TITLES_APP:LX/0wI;

    if-eq v0, v1, :cond_0

    .line 114768
    invoke-direct {p0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114769
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setVisibility(I)V

    .line 114770
    :cond_2
    invoke-super {p0, p1}, LX/0h3;->setTitleBarState(LX/0wI;)V

    .line 114771
    sget-object v0, LX/0zK;->a:[I

    invoke-virtual {p1}, LX/0wI;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 114772
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->i()Z

    move-result v0

    if-nez v0, :cond_3

    .line 114773
    const v0, 0x7f0d2f71

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 114774
    const v1, 0x7f0307ea

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 114775
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iput-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 114776
    :cond_3
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
