.class public final Lcom/facebook/ui/emoji/model/Emoji;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 338665
    new-instance v0, LX/1zV;

    invoke-direct {v0}, LX/1zV;-><init>()V

    sput-object v0, Lcom/facebook/ui/emoji/model/Emoji;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 338658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338659
    iput p1, p0, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    .line 338660
    iput p2, p0, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    .line 338661
    iput v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    .line 338662
    iput v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->d:I

    .line 338663
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    .line 338664
    return-void
.end method

.method public constructor <init>(III)V
    .locals 1

    .prologue
    .line 338651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338652
    iput p1, p0, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    .line 338653
    iput p2, p0, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    .line 338654
    iput p3, p0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    .line 338655
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->d:I

    .line 338656
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    .line 338657
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 1

    .prologue
    .line 338644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338645
    iput p1, p0, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    .line 338646
    iput p2, p0, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    .line 338647
    iput p3, p0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    .line 338648
    iput p4, p0, Lcom/facebook/ui/emoji/model/Emoji;->d:I

    .line 338649
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    .line 338650
    return-void
.end method

.method public constructor <init>(IIILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338638
    iput p1, p0, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    .line 338639
    iput p2, p0, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    .line 338640
    iput p3, p0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    .line 338641
    invoke-static {p4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    .line 338642
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->d:I

    .line 338643
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 338628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338629
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    .line 338630
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    .line 338631
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    .line 338632
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->d:I

    .line 338633
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 338634
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    .line 338635
    return-void

    .line 338636
    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 338597
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 338624
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/ui/emoji/model/Emoji;

    if-nez v1, :cond_1

    .line 338625
    :cond_0
    :goto_0
    return v0

    .line 338626
    :cond_1
    check-cast p1, Lcom/facebook/ui/emoji/model/Emoji;

    .line 338627
    iget v1, p0, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    iget v2, p1, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    iget v2, p1, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    iget v2, p1, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    iget-object v2, p1, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 338618
    iget v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    .line 338619
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    add-int/2addr v0, v1

    .line 338620
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    add-int/2addr v0, v1

    .line 338621
    iget-object v1, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    if-eqz v1, :cond_0

    .line 338622
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 338623
    :cond_0
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x5f

    .line 338604
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v0, 0xb

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 338605
    iget v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    move v0, v0

    .line 338606
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338607
    iget v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    move v0, v0

    .line 338608
    if-eqz v0, :cond_0

    .line 338609
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 338610
    iget v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    move v0, v0

    .line 338611
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338612
    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    if-eqz v0, :cond_1

    .line 338613
    iget-object v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 338614
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 338615
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338616
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 338617
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 338598
    iget v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 338599
    iget v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 338600
    iget v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 338601
    iget v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 338602
    iget-object v0, p0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 338603
    return-void
.end method
