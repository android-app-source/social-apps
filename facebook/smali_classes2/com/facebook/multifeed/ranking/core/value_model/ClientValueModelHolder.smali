.class public Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164168
    const-string v0, "value_model_holder-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 164169
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # [Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # [Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164166
    invoke-static {p1, p2, p3, p4}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->initHybrid(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 164167
    return-void
.end method

.method private static native initHybrid(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Lcom/facebook/jni/HybridData;
.end method

.method private native scoreNative([D[D)D
.end method

.method private native scoreNativeWithMaps([D[DLjava/lang/String;)D
.end method


# virtual methods
.method public final a([D[D)D
    .locals 2

    .prologue
    .line 164164
    invoke-direct {p0, p1, p2}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->scoreNative([D[D)D

    move-result-wide v0

    return-wide v0
.end method

.method public final a([D[DLjava/lang/String;)D
    .locals 2

    .prologue
    .line 164161
    if-nez p3, :cond_0

    .line 164162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "rankerFeatures is empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164163
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->scoreNativeWithMaps([D[DLjava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method public native getClientFeatures()[Ljava/lang/String;
.end method

.method public native getRequiredEvents()[Ljava/lang/String;
.end method

.method public native getRequiredFeatures()[Ljava/lang/String;
.end method

.method public native shouldUseVectorInput()Z
.end method
