.class public Lcom/facebook/marketplace/tab/MarketplaceTab;
.super Lcom/facebook/apptab/state/TabTag;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/marketplace/tab/MarketplaceTab;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lcom/facebook/marketplace/tab/MarketplaceTab;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120524
    new-instance v0, Lcom/facebook/marketplace/tab/MarketplaceTab;

    invoke-direct {v0}, Lcom/facebook/marketplace/tab/MarketplaceTab;-><init>()V

    sput-object v0, Lcom/facebook/marketplace/tab/MarketplaceTab;->m:Lcom/facebook/marketplace/tab/MarketplaceTab;

    .line 120525
    new-instance v0, LX/0zC;

    invoke-direct {v0}, LX/0zC;-><init>()V

    sput-object v0, Lcom/facebook/marketplace/tab/MarketplaceTab;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const v6, 0x63000e

    .line 120526
    sget-object v1, LX/0ax;->hx:Ljava/lang/String;

    sget-object v2, LX/0cQ;->MARKETPLACE_TAB_FRAGMENT:LX/0cQ;

    const v3, 0x7f020920

    const/4 v4, 0x0

    const-string v5, "marketplace"

    const v10, 0x7f080a33

    const v11, 0x7f0d003e

    move-object v0, p0

    move v7, v6

    move-object v9, v8

    invoke-direct/range {v0 .. v11}, Lcom/facebook/apptab/state/TabTag;-><init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V

    .line 120527
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p2    # LX/0cQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 120528
    invoke-direct/range {p0 .. p11}, Lcom/facebook/apptab/state/TabTag;-><init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V

    .line 120529
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120530
    const-string v0, "Marketplace"

    return-object v0
.end method
