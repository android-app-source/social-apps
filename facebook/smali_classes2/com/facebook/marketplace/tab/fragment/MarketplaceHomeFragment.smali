.class public Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;
.super Lcom/facebook/fbreact/fragment/FbReactFragment;
.source ""

# interfaces
.implements LX/0o2;


# annotations
.annotation runtime Lcom/facebook/navigation/annotations/NavigationComponent;
    scrollableContent = .subannotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
        id = "R.id.root_view_container"
    .end subannotation
.end annotation


# static fields
.field private static final i:Lcom/facebook/search/api/GraphSearchQuery;


# instance fields
.field public f:LX/0xB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Z

.field private h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 137306
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    sget-object v1, LX/103;->MARKETPLACE:LX/103;

    const-string v2, ""

    const-string v3, ""

    sget-object v4, LX/7B5;->SCOPED_ONLY:LX/7B5;

    invoke-static/range {v0 .. v5}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    .line 137307
    sput-object v0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->i:Lcom/facebook/search/api/GraphSearchQuery;

    sget-object v1, LX/7B4;->SCOPED_TAB:LX/7B4;

    new-instance v2, LX/7BC;

    invoke-direct {v2}, LX/7BC;-><init>()V

    .line 137308
    iput-boolean v5, v2, LX/7BC;->b:Z

    .line 137309
    move-object v2, v2

    .line 137310
    invoke-virtual {v2}, LX/7BC;->a()Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;Landroid/os/Parcelable;)V

    .line 137311
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 137343
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;-><init>()V

    .line 137344
    iput-boolean v0, p0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->g:Z

    .line 137345
    iput-boolean v0, p0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->h:Z

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-static {p0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object p0

    check-cast p0, LX/0xB;

    iput-object p0, p1, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->f:LX/0xB;

    return-void
.end method

.method public static c(Landroid/os/Bundle;)Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;
    .locals 1

    .prologue
    .line 137346
    new-instance v0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-direct {v0}, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;-><init>()V

    .line 137347
    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 137348
    return-object v0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 137335
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 137336
    const-class v0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-static {v0, p0}, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 137337
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 137338
    const-string v1, "route_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137339
    const-string v1, "MarketplaceHomeRoute"

    if-ne v0, v1, :cond_0

    .line 137340
    invoke-static {}, Lcom/facebook/quicklog/QuickPerformanceLoggerProvider;->getQPLInstance()Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    const v1, 0xa90009

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 137341
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a(Landroid/os/Bundle;)V

    .line 137342
    return-void
.end method

.method public final j()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 1

    .prologue
    .line 137334
    sget-object v0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->i:Lcom/facebook/search/api/GraphSearchQuery;

    return-object v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 137332
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    move-object v0, v0

    .line 137333
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/react/ReactRootView;->getId()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4255bf7d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137329
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onPause()V

    .line 137330
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->g:Z

    .line 137331
    const/16 v1, 0x2b

    const v2, 0x6245b0a3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6c08a05

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137326
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onResume()V

    .line 137327
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->g:Z

    .line 137328
    const/16 v1, 0x2b

    const v2, -0x14def02f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 3

    .prologue
    .line 137312
    invoke-super {p0, p1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->setUserVisibleHint(Z)V

    .line 137313
    iget-boolean v0, p0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->g:Z

    if-eqz v0, :cond_1

    .line 137314
    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->h:Z

    if-nez v0, :cond_2

    .line 137315
    iget-object v0, p0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->f:LX/0xB;

    sget-object v1, LX/12j;->MARKETPLACE:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    .line 137316
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 137317
    const-string v2, "jewelBadgeCount"

    invoke-interface {v1, v2, v0}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 137318
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 137319
    const-string v2, "body"

    invoke-interface {v0, v2, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 137320
    const-string v1, "MarketplaceTabDidAppear"

    invoke-virtual {p0, v1, v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 137321
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->h:Z

    .line 137322
    :cond_1
    return-void

    .line 137323
    :cond_2
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->h:Z

    if-eqz v0, :cond_0

    .line 137324
    const-string v0, "MarketplaceTabDidDisappear"

    invoke-virtual {p0, v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a(Ljava/lang/String;)V

    .line 137325
    goto :goto_0
.end method
