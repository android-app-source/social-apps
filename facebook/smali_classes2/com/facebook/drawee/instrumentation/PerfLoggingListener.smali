.class public Lcom/facebook/drawee/instrumentation/PerfLoggingListener;
.super LX/1cC;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final f:LX/1cD;

.field private static volatile g:Lcom/facebook/drawee/instrumentation/PerfLoggingListener;


# instance fields
.field private final a:LX/11i;

.field private final b:LX/0So;

.field private final c:Lcom/facebook/common/perftest/PerfTestConfig;

.field private final d:LX/0Zm;

.field private final e:LX/0kb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 281593
    new-instance v0, LX/1cD;

    invoke-direct {v0}, LX/1cD;-><init>()V

    sput-object v0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->f:LX/1cD;

    return-void
.end method

.method public constructor <init>(LX/11i;Lcom/facebook/common/perftest/PerfTestConfig;LX/0Zm;LX/0So;LX/0kb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 281594
    invoke-direct {p0}, LX/1cC;-><init>()V

    .line 281595
    iput-object p1, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->a:LX/11i;

    .line 281596
    iput-object p2, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->c:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 281597
    iput-object p3, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->d:LX/0Zm;

    .line 281598
    iput-object p4, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    .line 281599
    iput-object p5, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->e:LX/0kb;

    .line 281600
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/drawee/instrumentation/PerfLoggingListener;
    .locals 9

    .prologue
    .line 281601
    sget-object v0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->g:Lcom/facebook/drawee/instrumentation/PerfLoggingListener;

    if-nez v0, :cond_1

    .line 281602
    const-class v1, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;

    monitor-enter v1

    .line 281603
    :try_start_0
    sget-object v0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->g:Lcom/facebook/drawee/instrumentation/PerfLoggingListener;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 281604
    if-eqz v2, :cond_0

    .line 281605
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 281606
    new-instance v3, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v4

    check-cast v4, LX/11i;

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v5

    check-cast v5, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v6

    check-cast v6, LX/0Zm;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;-><init>(LX/11i;Lcom/facebook/common/perftest/PerfTestConfig;LX/0Zm;LX/0So;LX/0kb;)V

    .line 281607
    move-object v0, v3

    .line 281608
    sput-object v0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->g:Lcom/facebook/drawee/instrumentation/PerfLoggingListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 281609
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 281610
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 281611
    :cond_1
    sget-object v0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->g:Lcom/facebook/drawee/instrumentation/PerfLoggingListener;

    return-object v0

    .line 281612
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 281613
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 281614
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->n:Z

    move v0, v0

    .line 281615
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->d:LX/0Zm;

    invoke-virtual {v0}, LX/0Zm;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281616
    invoke-static {p1}, LX/1bi;->a(Ljava/lang/Object;)I

    move-result v0

    rem-int/lit16 v0, v0, 0x3e8

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 281617
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 281618
    invoke-direct {p0, p1}, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 281619
    :cond_0
    :goto_0
    return-void

    .line 281620
    :cond_1
    iget-object v0, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->a:LX/11i;

    sget-object v1, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->f:LX/1cD;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 281621
    if-eqz v0, :cond_0

    .line 281622
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 281623
    const-string v1, "cancelled"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    .line 281624
    :goto_1
    const-string v1, "wait_time"

    iget-object v4, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x66dca41

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 281625
    const-string v1, "good_enough_wait_time"

    iget-object v4, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x4411067e

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 281626
    const-string v1, "preview_wait_time"

    iget-object v4, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x443e57cb

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 281627
    iget-object v4, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->a:LX/11i;

    sget-object v5, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->f:LX/1cD;

    iget-object v0, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v8

    move-object v6, p1

    move-object v7, v2

    invoke-interface/range {v4 .. v9}, LX/11i;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    goto :goto_0

    :cond_2
    move-object v3, v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 281628
    invoke-direct {p0, p1}, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281629
    :goto_0
    return-void

    .line 281630
    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->a:LX/11i;

    sget-object v1, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->f:LX/1cD;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 281631
    if-nez v0, :cond_1

    .line 281632
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-nez v0, :cond_2

    instance-of v0, p2, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    if-eqz v0, :cond_2

    .line 281633
    check-cast p2, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    .line 281634
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "callerContextClass"

    .line 281635
    iget-object v2, p2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 281636
    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "callerContextTag"

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "analyticsTag"

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "moduleAnalyticsTag"

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "network_type"

    iget-object v2, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->e:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "network_subtype"

    iget-object v2, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->e:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "ttl_enabled"

    .line 281637
    iget-boolean v2, p2, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;->c:Z

    move v2, v2

    .line 281638
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    .line 281639
    :goto_1
    iget-object v0, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->a:LX/11i;

    sget-object v1, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->f:LX/1cD;

    iget-object v2, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, LX/11i;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    .line 281640
    :cond_1
    const-string v1, "wait_time"

    iget-object v2, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x7dd269ce

    move-object v2, v7

    move-object v3, v7

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 281641
    const-string v1, "good_enough_wait_time"

    iget-object v2, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x1d838aa4

    move-object v2, v7

    move-object v3, v7

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 281642
    const-string v1, "preview_wait_time"

    iget-object v2, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x4fc86921

    move-object v2, v7

    move-object v3, v7

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    goto/16 :goto_0

    :cond_2
    move-object v3, v7

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 7
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 281643
    const/4 v2, 0x0

    .line 281644
    invoke-direct {p0, p1}, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 281645
    :cond_0
    :goto_0
    return-void

    .line 281646
    :cond_1
    iget-object v0, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->a:LX/11i;

    sget-object v1, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->f:LX/1cD;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 281647
    if-eqz v0, :cond_0

    .line 281648
    const-string v1, "wait_time"

    iget-object v3, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x71b6abd

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 281649
    const-string v1, "good_enough_wait_time"

    iget-object v3, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x4261196b

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 281650
    const-string v1, "preview_wait_time"

    iget-object v3, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x13868578

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 281651
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 7
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 281652
    check-cast p2, LX/1ln;

    const/4 v2, 0x0

    .line 281653
    invoke-direct {p0, p1}, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 281654
    :cond_0
    :goto_0
    return-void

    .line 281655
    :cond_1
    iget-object v0, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->a:LX/11i;

    sget-object v1, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->f:LX/1cD;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 281656
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 281657
    const-string v1, "preview_wait_time"

    iget-object v3, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x7901844e

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 281658
    invoke-virtual {p2}, LX/1ln;->d()LX/1lk;

    move-result-object v1

    .line 281659
    iget-boolean v3, v1, LX/1lk;->c:Z

    move v1, v3

    .line 281660
    if-eqz v1, :cond_0

    .line 281661
    const-string v1, "good_enough_wait_time"

    iget-object v3, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x3f3cbbc

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 281662
    invoke-direct {p0, p1}, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 281663
    :cond_0
    :goto_0
    return-void

    .line 281664
    :cond_1
    iget-object v0, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->a:LX/11i;

    sget-object v1, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->f:LX/1cD;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 281665
    if-eqz v0, :cond_0

    .line 281666
    const-string v1, "wait_time"

    iget-object v3, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x2d0fe8d4

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 281667
    const-string v1, "good_enough_wait_time"

    iget-object v3, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x1239ad9b

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 281668
    const-string v1, "preview_wait_time"

    iget-object v3, p0, Lcom/facebook/drawee/instrumentation/PerfLoggingListener;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x72db49b1

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    goto :goto_0
.end method
