.class public Lcom/facebook/drawee/drawable/AutoRotateDrawable;
.super LX/1ah;
.source ""

# interfaces
.implements LX/1en;
.implements Ljava/lang/Runnable;


# instance fields
.field public a:F
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public c:I

.field private d:Z

.field public e:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 288962
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;IZ)V

    .line 288963
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/Drawable;IZ)V
    .locals 1

    .prologue
    .line 288964
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 288965
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->a:F

    .line 288966
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->e:Z

    .line 288967
    iput p2, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->c:I

    .line 288968
    iput-boolean p3, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->d:Z

    .line 288969
    return-void
.end method


# virtual methods
.method public final b()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 288970
    invoke-virtual {p0}, LX/1ah;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, LX/1am;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 288971
    new-instance v1, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    iget v2, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->c:I

    iget-boolean v3, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->d:Z

    invoke-direct {v1, v0, v2, v3}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;IZ)V

    return-object v1
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    .line 288972
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 288973
    invoke-virtual {p0}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 288974
    iget v0, v2, Landroid/graphics/Rect;->right:I

    iget v3, v2, Landroid/graphics/Rect;->left:I

    sub-int v3, v0, v3

    .line 288975
    iget v0, v2, Landroid/graphics/Rect;->bottom:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    sub-int v4, v0, v4

    .line 288976
    iget v0, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->a:F

    .line 288977
    iget-boolean v5, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->d:Z

    if-nez v5, :cond_0

    .line 288978
    const/high16 v0, 0x43b40000    # 360.0f

    iget v5, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->a:F

    sub-float/2addr v0, v5

    .line 288979
    :cond_0
    iget v5, v2, Landroid/graphics/Rect;->left:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v5

    int-to-float v3, v3

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {p1, v0, v3, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 288980
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    .line 288981
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 288982
    iget-boolean v6, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->e:Z

    if-nez v6, :cond_1

    .line 288983
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->e:Z

    .line 288984
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x14

    add-long/2addr v6, v8

    invoke-virtual {p0, p0, v6, v7}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 288985
    :cond_1
    return-void
.end method

.method public final run()V
    .locals 3

    .prologue
    .line 288986
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->e:Z

    .line 288987
    iget v0, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->a:F

    .line 288988
    const/high16 v1, 0x41a00000    # 20.0f

    iget v2, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->c:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x43b40000    # 360.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move v1, v1

    .line 288989
    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->a:F

    .line 288990
    invoke-virtual {p0}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->invalidateSelf()V

    .line 288991
    return-void
.end method
