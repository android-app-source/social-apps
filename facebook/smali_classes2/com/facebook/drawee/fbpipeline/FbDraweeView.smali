.class public Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.super Lcom/facebook/drawee/view/GenericDraweeView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 277622
    const-class v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 277618
    invoke-direct {p0, p1}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;)V

    .line 277619
    invoke-direct {p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->c()V

    .line 277620
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1af;)V
    .locals 0

    .prologue
    .line 277615
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    .line 277616
    invoke-direct {p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->c()V

    .line 277617
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 277612
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 277613
    invoke-direct {p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->c()V

    .line 277614
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 277609
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 277610
    invoke-direct {p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->c()V

    .line 277611
    return-void
.end method

.method private static a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/1Ad;)V
    .locals 0

    .prologue
    .line 277621
    iput-object p1, p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p2, p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->b:LX/1Ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/1Ad;)V

    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 277607
    const-class v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 277608
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 277605
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;Z)V

    .line 277606
    return-void
.end method

.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;Z)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 277596
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->b:LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    .line 277597
    if-eqz p3, :cond_0

    .line 277598
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->b:LX/1Ad;

    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    const/4 v2, 0x1

    .line 277599
    iput-boolean v2, v1, LX/1bX;->g:Z

    .line 277600
    move-object v1, v1

    .line 277601
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 277602
    :goto_0
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->b:LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 277603
    return-void

    .line 277604
    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->b:LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    goto :goto_0
.end method

.method public setImageURI(Landroid/net/Uri;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 277592
    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 277593
    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 277594
    return-void

    .line 277595
    :cond_0
    sget-object v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->c:Lcom/facebook/common/callercontext/CallerContext;

    goto :goto_0
.end method
