.class public Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Gd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1Gd",
        "<",
        "LX/1ca",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1bf;

.field private c:LX/1bf;

.field public d:Ljava/lang/Object;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final f:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(LX/0Ot;LX/1bf;LX/1bf;Ljava/lang/Object;)V
    .locals 2
    .param p2    # LX/1bf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1bf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Object;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;",
            "LX/1bf;",
            "LX/1bf;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 282446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282447
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 282448
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 282449
    iput-object p1, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->a:LX/0Ot;

    .line 282450
    iput-object p2, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->b:LX/1bf;

    .line 282451
    iput-object p3, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->c:LX/1bf;

    .line 282452
    iput-object p4, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->d:Ljava/lang/Object;

    .line 282453
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->b:LX/1bf;

    invoke-static {v0}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 282454
    iput-object v1, v0, LX/1bX;->b:LX/1bY;

    .line 282455
    move-object v0, v0

    .line 282456
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 282457
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    iget-object p1, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, LX/1HI;->c(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 282458
    new-instance v1, LX/5NV;

    invoke-direct {v1, p0}, LX/5NV;-><init>(Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;)V

    .line 282459
    sget-object p1, LX/1fo;->a:LX/1fo;

    move-object p1, p1

    .line 282460
    invoke-interface {v0, v1, p1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 282461
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 282462
    const/4 v4, 0x0

    .line 282463
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 282464
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->c:LX/1bf;

    if-eqz v0, :cond_0

    .line 282465
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    iget-object v1, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->c:LX/1bf;

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 282466
    :goto_0
    return-object v0

    .line 282467
    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->b:LX/1bf;

    invoke-static {v0}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 282468
    iput-object v1, v0, LX/1bX;->b:LX/1bY;

    .line 282469
    move-object v0, v0

    .line 282470
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 282471
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    goto :goto_0

    .line 282472
    :cond_1
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 282473
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    const-string v1, "lazy_load"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 282474
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->b:LX/1bf;

    invoke-virtual {v0, v2, v1}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    .line 282475
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->c:LX/1bf;

    if-nez v0, :cond_2

    .line 282476
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    iget-object v1, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->b:LX/1bf;

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    goto :goto_0

    .line 282477
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 282478
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->b:LX/1bf;

    iget-object v3, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->d:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3, v4}, LX/1HI;->a(LX/1bf;Ljava/lang/Object;Z)LX/1Gd;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282479
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->c:LX/1bf;

    iget-object v3, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->d:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3, v4}, LX/1HI;->a(LX/1bf;Ljava/lang/Object;Z)LX/1Gd;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282480
    invoke-static {v1}, LX/1fC;->a(Ljava/util/List;)LX/1fC;

    move-result-object v0

    invoke-virtual {v0}, LX/1fC;->b()LX/1ca;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 282481
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 282482
    return-void
.end method
