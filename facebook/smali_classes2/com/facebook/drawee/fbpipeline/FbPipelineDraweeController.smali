.class public Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;
.super LX/1bo;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/1cB;

.field public c:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public d:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1br;LX/1c3;LX/0Zb;Ljava/util/Set;Ljava/util/concurrent/Executor;LX/1Fh;LX/1Gd;Ljava/lang/String;LX/1bh;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)V
    .locals 12
    .param p5    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .param p7    # LX/1Fh;
        .annotation runtime Lcom/facebook/imagepipeline/module/BitmapMemoryCache;
        .end annotation
    .end param
    .param p8    # LX/1Gd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/1bh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/1br;",
            "Lcom/facebook/imagepipeline/animated/factory/AnimatedDrawableFactory;",
            "LX/0Zb;",
            "Ljava/util/Set",
            "<",
            "LX/1c8;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "LX/1Fh;",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "Ljava/lang/String;",
            "LX/1bh;",
            "Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 281014
    if-nez p5, :cond_0

    const/4 v11, 0x0

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    invoke-direct/range {v1 .. v11}, LX/1bo;-><init>(Landroid/content/res/Resources;LX/1br;LX/1c3;Ljava/util/concurrent/Executor;LX/1Fh;LX/1Gd;Ljava/lang/String;LX/1bh;Ljava/lang/Object;LX/1c9;)V

    .line 281015
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->a:LX/0Zb;

    .line 281016
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->c:LX/1Gd;

    .line 281017
    new-instance v1, LX/1cB;

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->a:LX/0Zb;

    invoke-direct {v1, v2}, LX/1cB;-><init>(LX/0Zb;)V

    iput-object v1, p0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->b:LX/1cB;

    .line 281018
    return-void

    .line 281019
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    move-object/from16 v0, p5

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v11

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;)Z
    .locals 1

    .prologue
    .line 281011
    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->t(Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->c:LX/1Gd;

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;

    .line 281012
    iget-object p0, v0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p0

    move v0, p0

    .line 281013
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;)Z
    .locals 1

    .prologue
    .line 281010
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->c:LX/1Gd;

    instance-of v0, v0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;

    return v0
.end method


# virtual methods
.method public final a(LX/1FJ;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 280990
    const-string v0, "FbPipelineDraweeController.createDrawable"

    const v1, 0x3d756c27

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 280991
    :try_start_0
    invoke-static {p1}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    invoke-static {v0}, LX/03g;->b(Z)V

    .line 280992
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 280993
    instance-of v1, v0, LX/1q7;

    if-eqz v1, :cond_0

    .line 280994
    check-cast v0, LX/1q7;

    .line 280995
    new-instance v1, LX/4nD;

    .line 280996
    iget-object v2, p0, LX/1bo;->b:Landroid/content/res/Resources;

    move-object v2, v2

    .line 280997
    iget-object v3, v0, LX/1q7;->b:Ljava/util/List;

    move-object v3, v3

    .line 280998
    iget-object v4, v0, LX/1q7;->c:Ljava/util/List;

    move-object v0, v4

    .line 280999
    invoke-direct {v1, v2, v3, v0}, LX/4nD;-><init>(Landroid/content/res/Resources;Ljava/util/List;Ljava/util/List;)V

    .line 281000
    :goto_0
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x3f2a36e2eb1c432dL    # 2.0E-4

    cmpg-double v0, v2, v4

    if-gtz v0, :cond_2

    .line 281001
    new-instance v0, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;

    iget-object v2, p0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->a:LX/0Zb;

    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->s()Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/InstrumentedDrawable;-><init>(Landroid/graphics/drawable/Drawable;LX/0Zb;Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281002
    :goto_1
    const v1, 0x1e14f837

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 281003
    :cond_0
    :try_start_1
    instance-of v1, v0, LX/1q8;

    if-eqz v1, :cond_1

    .line 281004
    check-cast v0, LX/1q8;

    .line 281005
    new-instance v1, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;

    .line 281006
    iget-object v2, v0, LX/1q8;->b:LX/52j;

    move-object v0, v2

    .line 281007
    invoke-direct {v1, v0}, Lgifdrawable/pl/droidsonroids/gif/GifDrawable;-><init>(LX/52j;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 281008
    :catchall_0
    move-exception v0

    const v1, -0x61ee812e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 281009
    :cond_1
    :try_start_2
    invoke-super {p0, p1}, LX/1bo;->a(LX/1FJ;)Landroid/graphics/drawable/Drawable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 280989
    check-cast p1, LX/1FJ;

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->a(LX/1FJ;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 280988
    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->s()Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v0

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 280964
    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->a$redex0(Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, LX/1bo;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()LX/1ca;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 280983
    const-string v0, "FbPipelineDraweeController.getDataSource"

    const v1, 0x6c2dee97

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 280984
    :try_start_0
    invoke-super {p0}, LX/1bo;->o()LX/1ca;

    move-result-object v0

    .line 280985
    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->t(Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280986
    new-instance v1, LX/5NW;

    invoke-direct {v1, p0}, LX/5NW;-><init>(Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;)V

    invoke-static {}, LX/1by;->b()LX/1by;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280987
    :cond_0
    const v1, -0x5698d9e3

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0xa04851c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public onClick()Z
    .locals 4

    .prologue
    .line 280966
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1bp;->a(Landroid/graphics/drawable/Drawable;)V

    .line 280967
    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->a$redex0(Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280968
    invoke-static {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->t(Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280969
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->c:LX/1Gd;

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbLazyDataSourceSupplier;->c()V

    .line 280970
    :cond_0
    invoke-virtual {p0}, LX/1bp;->b()V

    .line 280971
    invoke-virtual {p0}, LX/1bp;->n()V

    .line 280972
    iget-object v0, p0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->b:LX/1cB;

    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->s()Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    move-result-object v1

    .line 280973
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "tap_to_load_image"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 280974
    const-string v3, "calling_class"

    .line 280975
    iget-object p0, v1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object p0, p0

    .line 280976
    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 280977
    const-string v3, "analytics_tag"

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 280978
    const-string v3, "module_tag"

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->d()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 280979
    const-string v3, "feature_tag"

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 280980
    iget-object v3, v0, LX/1cB;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 280981
    const/4 v0, 0x1

    .line 280982
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, LX/1bo;->onClick()Z

    move-result v0

    goto :goto_0
.end method

.method public final s()Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;
    .locals 1

    .prologue
    .line 280965
    invoke-super {p0}, LX/1bo;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    return-object v0
.end method
