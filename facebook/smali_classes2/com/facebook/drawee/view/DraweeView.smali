.class public Lcom/facebook/drawee/view/DraweeView;
.super Landroid/widget/ImageView;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DH::",
        "LX/1aY;",
        ">",
        "Landroid/widget/ImageView;"
    }
.end annotation


# instance fields
.field private final a:LX/1aW;

.field public b:F

.field private c:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<TDH;>;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 277704
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 277705
    new-instance v0, LX/1aW;

    invoke-direct {v0}, LX/1aW;-><init>()V

    iput-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->a:LX/1aW;

    .line 277706
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/drawee/view/DraweeView;->b:F

    .line 277707
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/view/DraweeView;->d:Z

    .line 277708
    invoke-direct {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->a(Landroid/content/Context;)V

    .line 277709
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 277710
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 277711
    new-instance v0, LX/1aW;

    invoke-direct {v0}, LX/1aW;-><init>()V

    iput-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->a:LX/1aW;

    .line 277712
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/drawee/view/DraweeView;->b:F

    .line 277713
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/view/DraweeView;->d:Z

    .line 277714
    invoke-direct {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->a(Landroid/content/Context;)V

    .line 277715
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 277716
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 277717
    new-instance v0, LX/1aW;

    invoke-direct {v0}, LX/1aW;-><init>()V

    iput-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->a:LX/1aW;

    .line 277718
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/drawee/view/DraweeView;->b:F

    .line 277719
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/view/DraweeView;->d:Z

    .line 277720
    invoke-direct {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->a(Landroid/content/Context;)V

    .line 277721
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 277722
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 277723
    new-instance v0, LX/1aW;

    invoke-direct {v0}, LX/1aW;-><init>()V

    iput-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->a:LX/1aW;

    .line 277724
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/drawee/view/DraweeView;->b:F

    .line 277725
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/view/DraweeView;->d:Z

    .line 277726
    invoke-direct {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->a(Landroid/content/Context;)V

    .line 277727
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 277728
    iget-boolean v0, p0, Lcom/facebook/drawee/view/DraweeView;->d:Z

    if-eqz v0, :cond_1

    .line 277729
    :cond_0
    :goto_0
    return-void

    .line 277730
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/drawee/view/DraweeView;->d:Z

    .line 277731
    const/4 v0, 0x0

    invoke-static {v0, p1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    .line 277732
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 277733
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getImageTintList()Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 277734
    if-eqz v0, :cond_0

    .line 277735
    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setColorFilter(I)V

    goto :goto_0
.end method

.method public static e(Lcom/facebook/drawee/view/DraweeView;)V
    .locals 1

    .prologue
    .line 277736
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 277737
    return-void
.end method

.method public static f(Lcom/facebook/drawee/view/DraweeView;)V
    .locals 1

    .prologue
    .line 277738
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 277739
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 277740
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    .line 277741
    iget-object p0, v0, LX/1aX;->e:LX/1aY;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 277742
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 277743
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    .line 277744
    iget-object p0, v0, LX/1aX;->f:LX/1aZ;

    move-object v0, p0

    .line 277745
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAspectRatio()F
    .locals 1

    .prologue
    .line 277746
    iget v0, p0, Lcom/facebook/drawee/view/DraweeView;->b:F

    return v0
.end method

.method public getController()LX/1aZ;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 277747
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    .line 277748
    iget-object p0, v0, LX/1aX;->f:LX/1aZ;

    move-object v0, p0

    .line 277749
    return-object v0
.end method

.method public getHierarchy()LX/1aY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TDH;"
        }
    .end annotation

    .prologue
    .line 277750
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    return-object v0
.end method

.method public getTopLevelDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 277700
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x377ad857

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 277701
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 277702
    invoke-static {p0}, Lcom/facebook/drawee/view/DraweeView;->e(Lcom/facebook/drawee/view/DraweeView;)V

    .line 277703
    const/16 v1, 0x2d

    const v2, -0x3675a9e2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x503ded23

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 277672
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 277673
    invoke-static {p0}, Lcom/facebook/drawee/view/DraweeView;->f(Lcom/facebook/drawee/view/DraweeView;)V

    .line 277674
    const/16 v1, 0x2d

    const v2, -0x62d6f6b6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 0

    .prologue
    .line 277669
    invoke-super {p0}, Landroid/widget/ImageView;->onFinishTemporaryDetach()V

    .line 277670
    invoke-static {p0}, Lcom/facebook/drawee/view/DraweeView;->e(Lcom/facebook/drawee/view/DraweeView;)V

    .line 277671
    return-void
.end method

.method public onMeasure(II)V
    .locals 6

    .prologue
    .line 277653
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->a:LX/1aW;

    iput p1, v0, LX/1aW;->a:I

    .line 277654
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->a:LX/1aW;

    iput p2, v0, LX/1aW;->b:I

    .line 277655
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->a:LX/1aW;

    iget v1, p0, Lcom/facebook/drawee/view/DraweeView;->b:F

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    const/high16 p2, 0x40000000    # 2.0f

    .line 277656
    const/4 v5, 0x0

    cmpg-float v5, v1, v5

    if-lez v5, :cond_0

    if-nez v2, :cond_1

    .line 277657
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->a:LX/1aW;

    iget v0, v0, LX/1aW;->a:I

    iget-object v1, p0, Lcom/facebook/drawee/view/DraweeView;->a:LX/1aW;

    iget v1, v1, LX/1aW;->b:I

    invoke-super {p0, v0, v1}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 277658
    return-void

    .line 277659
    :cond_1
    iget v5, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v5}, LX/1aa;->a(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 277660
    iget v5, v0, LX/1aW;->a:I

    invoke-static {v5}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 277661
    sub-int/2addr v5, v3

    int-to-float v5, v5

    div-float/2addr v5, v1

    int-to-float p1, v4

    add-float/2addr v5, p1

    float-to-int v5, v5

    .line 277662
    iget p1, v0, LX/1aW;->b:I

    invoke-static {v5, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v5

    .line 277663
    invoke-static {v5, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iput v5, v0, LX/1aW;->b:I

    goto :goto_0

    .line 277664
    :cond_2
    iget v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v5}, LX/1aa;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 277665
    iget v5, v0, LX/1aW;->b:I

    invoke-static {v5}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 277666
    sub-int/2addr v5, v4

    int-to-float v5, v5

    mul-float/2addr v5, v1

    int-to-float p1, v3

    add-float/2addr v5, p1

    float-to-int v5, v5

    .line 277667
    iget p1, v0, LX/1aW;->a:I

    invoke-static {v5, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v5

    .line 277668
    invoke-static {v5, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iput v5, v0, LX/1aW;->a:I

    goto :goto_0
.end method

.method public final onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 277650
    invoke-super {p0}, Landroid/widget/ImageView;->onStartTemporaryDetach()V

    .line 277651
    invoke-static {p0}, Lcom/facebook/drawee/view/DraweeView;->f(Lcom/facebook/drawee/view/DraweeView;)V

    .line 277652
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, -0x7ef4b4ed

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 277697
    iget-object v2, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    invoke-virtual {v2, p1}, LX/1aX;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 277698
    const v2, -0x6e6a1318

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 277699
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x751a7754

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setAspectRatio(F)V
    .locals 1

    .prologue
    .line 277645
    iget v0, p0, Lcom/facebook/drawee/view/DraweeView;->b:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 277646
    :goto_0
    return-void

    .line 277647
    :cond_0
    iput p1, p0, Lcom/facebook/drawee/view/DraweeView;->b:F

    .line 277648
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->requestLayout()V

    goto :goto_0
.end method

.method public setController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 277675
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    invoke-virtual {v0, p1}, LX/1aX;->a(LX/1aZ;)V

    .line 277676
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277677
    return-void
.end method

.method public setHierarchy(LX/1aY;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDH;)V"
        }
    .end annotation

    .prologue
    .line 277678
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    invoke-virtual {v0, p1}, LX/1aX;->a(LX/1aY;)V

    .line 277679
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277680
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 277681
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->a(Landroid/content/Context;)V

    .line 277682
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 277683
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 277684
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 277685
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->a(Landroid/content/Context;)V

    .line 277686
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 277687
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277688
    return-void
.end method

.method public setImageResource(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 277689
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->a(Landroid/content/Context;)V

    .line 277690
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 277691
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 277692
    return-void
.end method

.method public setImageURI(Landroid/net/Uri;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 277693
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->a(Landroid/content/Context;)V

    .line 277694
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 277695
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 277696
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 277649
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v1

    const-string v2, "holder"

    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeView;->c:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "<no holder set>"

    goto :goto_0
.end method
