.class public Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;
.super Lcom/facebook/common/callercontext/CallerContext;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;


# instance fields
.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 280955
    new-instance v0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    sget-object v1, Lcom/facebook/common/callercontext/CallerContext;->a:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;-><init>(Lcom/facebook/common/callercontext/CallerContext;Z)V

    sput-object v0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;->b:Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    .line 280956
    new-instance v0, LX/1bn;

    invoke-direct {v0}, LX/1bn;-><init>()V

    sput-object v0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 280957
    invoke-direct {p0, p1}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Landroid/os/Parcel;)V

    .line 280958
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;->c:Z

    .line 280959
    return-void

    .line 280960
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/common/callercontext/CallerContext;Z)V
    .locals 0

    .prologue
    .line 280951
    invoke-direct {p0, p1}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 280952
    iput-boolean p2, p0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;->c:Z

    .line 280953
    return-void
.end method


# virtual methods
.method public final e()LX/15g;
    .locals 3

    .prologue
    .line 280954
    invoke-super {p0}, Lcom/facebook/common/callercontext/CallerContext;->e()LX/15g;

    move-result-object v0

    const-string v1, "Is TTL Enabled"

    iget-boolean v2, p0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;->c:Z

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Z)LX/15g;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 280936
    if-ne p0, p1, :cond_1

    .line 280937
    :cond_0
    :goto_0
    return v0

    .line 280938
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 280939
    goto :goto_0

    .line 280940
    :cond_3
    invoke-super {p0, p1}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 280941
    goto :goto_0

    .line 280942
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;->c:Z

    check-cast p1, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;

    iget-boolean v3, p1, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 280943
    invoke-super {p0}, Lcom/facebook/common/callercontext/CallerContext;->hashCode()I

    move-result v0

    .line 280944
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 280945
    return v0

    .line 280946
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 280947
    invoke-super {p0, p1, p2}, Lcom/facebook/common/callercontext/CallerContext;->writeToParcel(Landroid/os/Parcel;I)V

    .line 280948
    iget-boolean v0, p0, Lcom/facebook/drawee/callercontext/FbDraweeCallerContext;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 280949
    return-void

    .line 280950
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
