.class public Lcom/facebook/zero/logging/FbZeroLogger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1p0;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/regex/Pattern;


# instance fields
.field private final c:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 327713
    const-class v0, Lcom/facebook/zero/logging/FbZeroLogger;

    sput-object v0, Lcom/facebook/zero/logging/FbZeroLogger;->a:Ljava/lang/Class;

    .line 327714
    const-string v0, "^(https?)://((api|graph)\\\\.([0-9a-zA-Z\\\\.-]*)?facebook\\\\.com(:?[0-9]{0,5}))($|\\\\?.*$|/.*$)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/logging/FbZeroLogger;->b:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 327665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327666
    iput-object p1, p0, Lcom/facebook/zero/logging/FbZeroLogger;->c:LX/0Zb;

    .line 327667
    return-void
.end method

.method private static a(LX/0oG;Ljava/util/Map;)V
    .locals 3
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0oG;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327709
    if-eqz p1, :cond_0

    .line 327710
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 327711
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    goto :goto_0

    .line 327712
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/zero/logging/FbZeroLogger;
    .locals 2

    .prologue
    .line 327707
    new-instance v1, Lcom/facebook/zero/logging/FbZeroLogger;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, Lcom/facebook/zero/logging/FbZeroLogger;-><init>(LX/0Zb;)V

    .line 327708
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;Ljava/util/Map;)V
    .locals 3
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327701
    iget-object v0, p0, Lcom/facebook/zero/logging/FbZeroLogger;->c:LX/0Zb;

    const-string v1, "zero_rewrite_rules_applied"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 327702
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327703
    const-string v1, "caller_context"

    invoke-virtual {p1}, Lcom/facebook/common/callercontext/CallerContext;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 327704
    invoke-static {v0, p2}, Lcom/facebook/zero/logging/FbZeroLogger;->a(LX/0oG;Ljava/util/Map;)V

    .line 327705
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 327706
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327693
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/zero/logging/FbZeroLogger;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327694
    :cond_0
    :goto_0
    return-void

    .line 327695
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/logging/FbZeroLogger;->c:LX/0Zb;

    const-string v1, "zero_url_rewrite"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 327696
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327697
    const-string v1, "original_uri"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 327698
    const-string v1, "rewritten_uri"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 327699
    invoke-static {v0, p3}, Lcom/facebook/zero/logging/FbZeroLogger;->a(LX/0oG;Ljava/util/Map;)V

    .line 327700
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327687
    iget-object v0, p0, Lcom/facebook/zero/logging/FbZeroLogger;->c:LX/0Zb;

    const-string v1, "zero_url_not_rewritten"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 327688
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327689
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 327690
    invoke-static {v0, p2}, Lcom/facebook/zero/logging/FbZeroLogger;->a(LX/0oG;Ljava/util/Map;)V

    .line 327691
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 327692
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Throwable;Ljava/util/Map;)V
    .locals 3
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327680
    iget-object v0, p0, Lcom/facebook/zero/logging/FbZeroLogger;->c:LX/0Zb;

    const-string v1, "zero_token_fetch_failed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 327681
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327682
    const-string v1, "zero_module"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 327683
    const-string v1, "exception_message"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 327684
    invoke-static {v0, p2}, Lcom/facebook/zero/logging/FbZeroLogger;->a(LX/0oG;Ljava/util/Map;)V

    .line 327685
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 327686
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327674
    iget-object v0, p0, Lcom/facebook/zero/logging/FbZeroLogger;->c:LX/0Zb;

    const-string v1, "zero_campiagn_not_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 327675
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327676
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 327677
    invoke-static {v0, p2}, Lcom/facebook/zero/logging/FbZeroLogger;->a(LX/0oG;Ljava/util/Map;)V

    .line 327678
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 327679
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327668
    iget-object v0, p0, Lcom/facebook/zero/logging/FbZeroLogger;->c:LX/0Zb;

    const-string v1, "zero_invalid_mcc_mnc"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 327669
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327670
    const-string v1, "mccmnc"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 327671
    invoke-static {v0, p2}, Lcom/facebook/zero/logging/FbZeroLogger;->a(LX/0oG;Ljava/util/Map;)V

    .line 327672
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 327673
    :cond_0
    return-void
.end method
