.class public Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0Pm;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRuleDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/util/regex/Pattern;

.field public final mMatcher:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "matcher"
    .end annotation
.end field

.field public final mReplacer:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "replacer"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327659
    const-class v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRuleDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327660
    const-class v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRuleSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 327629
    new-instance v0, LX/1oz;

    invoke-direct {v0}, LX/1oz;-><init>()V

    sput-object v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 327655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327656
    iput-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    .line 327657
    iput-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    .line 327658
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 327650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327651
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    .line 327652
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    .line 327653
    iget-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->a:Ljava/util/regex/Pattern;

    .line 327654
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 327645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327646
    iput-object p1, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    .line 327647
    iput-object p2, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    .line 327648
    invoke-direct {p0}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->d()Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    .line 327649
    return-void
.end method

.method private d()Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;
    .locals 1

    .prologue
    .line 327642
    iget-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 327643
    iget-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->a:Ljava/util/regex/Pattern;

    .line 327644
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 327641
    invoke-direct {p0}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->d()Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 327661
    iget-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327640
    iget-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 327639
    iget-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327638
    iget-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 327637
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 327635
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 327636
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    iget-object v0, v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    check-cast p1, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    iget-object v2, p1, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 327634
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 327633
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "matcher"

    iget-object v2, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "replacer"

    iget-object v2, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 327630
    iget-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 327631
    iget-object v0, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 327632
    return-void
.end method
