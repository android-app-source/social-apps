.class public final Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 340411
    new-instance v0, LX/1uF;

    invoke-direct {v0}, LX/1uF;-><init>()V

    sput-object v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 340412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340413
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    .line 340414
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    .line 340415
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 340407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340408
    iput-object p1, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    .line 340409
    iput-object p2, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    .line 340410
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 340416
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 340401
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 340402
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340403
    iget-object v3, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    move-object v0, v3

    .line 340404
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    check-cast p1, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340405
    iget-object v2, p1, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    move-object v2, v2

    .line 340406
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 340397
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 340398
    iget-object v0, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 340399
    iget-object v0, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 340400
    return-void
.end method
