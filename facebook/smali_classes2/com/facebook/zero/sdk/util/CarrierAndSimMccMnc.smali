.class public Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;


# instance fields
.field public final b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

.field public final c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 340392
    new-instance v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    invoke-direct {v0, v1, v1}, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;)V

    sput-object v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    .line 340393
    new-instance v0, LX/1uE;

    invoke-direct {v0}, LX/1uE;-><init>()V

    sput-object v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 340388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340389
    const-class v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    iput-object v0, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340390
    const-class v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    iput-object v0, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340391
    return-void
.end method

.method public constructor <init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;)V
    .locals 0

    .prologue
    .line 340384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340385
    iput-object p1, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340386
    iput-object p2, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340387
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 340363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340364
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 340365
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 340366
    iput-object v3, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340367
    iput-object v3, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340368
    :goto_0
    return-void

    .line 340369
    :cond_0
    new-instance v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v0, v1}, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340370
    new-instance v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    const/4 v0, 0x2

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v0, v1}, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 340383
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 340375
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    if-nez v1, :cond_1

    .line 340376
    :cond_0
    :goto_0
    return v0

    .line 340377
    :cond_1
    check-cast p1, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    .line 340378
    iget-object v1, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340379
    iget-object v2, p1, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v2, v2

    .line 340380
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 340381
    iget-object v2, p1, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v2, v2

    .line 340382
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 340374
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 340371
    iget-object v0, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 340372
    iget-object v0, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 340373
    return-void
.end method
