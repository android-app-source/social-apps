.class public Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;
.super Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2gy;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0yP;

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1p0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164456
    const-class v0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;

    sput-object v0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->p:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0yN;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0yP;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;)V
    .locals 9
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0yN;",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0yU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1p3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2gy;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/zero/sdk/token/ZeroTokenFetcher;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0yi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1p0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164541
    move-object v1, p0

    move-object v2, p2

    move-object v3, p5

    move-object v4, p6

    move-object/from16 v5, p13

    move-object/from16 v6, p11

    move-object/from16 v7, p12

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;-><init>(LX/0yN;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    .line 164542
    iput-object p1, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->i:LX/0Ot;

    .line 164543
    iput-object p3, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->j:LX/0Ot;

    .line 164544
    iput-object p4, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->k:LX/0Ot;

    .line 164545
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->m:LX/0Or;

    .line 164546
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->n:LX/0yP;

    .line 164547
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->o:LX/0Ot;

    .line 164548
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->l:LX/0Ot;

    .line 164549
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->n:LX/0yP;

    invoke-virtual {v1, p0}, LX/0yP;->a(LX/0yJ;)V

    .line 164550
    return-void
.end method


# virtual methods
.method public a(LX/0yi;)V
    .locals 1

    .prologue
    .line 164551
    sget-object v0, LX/32P;->UNKNOWN_STATE:LX/32P;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/0yi;LX/32P;)V

    .line 164552
    return-void
.end method

.method public a(LX/0yi;LX/32P;)V
    .locals 1

    .prologue
    .line 164553
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->n:LX/0yP;

    invoke-virtual {v0, p1, p2}, LX/0yP;->a(LX/0yi;LX/32P;)V

    .line 164554
    return-void
.end method

.method public final a(LX/32P;)V
    .locals 1

    .prologue
    .line 164555
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    invoke-virtual {p0, v0, p1}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/0yi;LX/32P;)V

    .line 164556
    return-void
.end method

.method public a(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2

    .prologue
    .line 164557
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1p0;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/1p0;->a(Lcom/facebook/common/callercontext/CallerContext;Ljava/util/Map;)V

    .line 164558
    return-void
.end method

.method public a(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 164559
    sget-object v0, Lcom/facebook/zero/sdk/token/ZeroToken;->a:Lcom/facebook/zero/sdk/token/ZeroToken;

    invoke-virtual {p1, v0}, Lcom/facebook/zero/sdk/token/ZeroToken;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164560
    invoke-virtual {p0, v6}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->b(Z)V

    .line 164561
    const-string v0, "enabled"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/String;)V

    .line 164562
    :cond_0
    :goto_0
    return-void

    .line 164563
    :cond_1
    iget-object v2, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0yU;

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1p3;

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2gy;

    move-object v0, p2

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/zero/sdk/token/ZeroToken;->a(LX/0yi;Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yN;LX/0yU;LX/1p3;LX/2gy;)V

    .line 164564
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne p2, v0, :cond_0

    .line 164565
    iget-object v0, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->i:LX/0Rf;

    move-object v0, v0

    .line 164566
    iput-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->i:LX/0Rf;

    .line 164567
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e()LX/0Rf;

    .line 164568
    iget-object v0, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->j:LX/0Px;

    move-object v0, v0

    .line 164569
    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0Px;)V

    .line 164570
    iget-object v0, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    move-object v0, v0

    .line 164571
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 164572
    iget-object v0, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    move-object v0, v0

    .line 164573
    iput-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->k:LX/0Px;

    .line 164574
    const-class v1, LX/0yK;

    const-string v2, "set_backup_rules"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 164575
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->g()LX/0Px;

    .line 164576
    :cond_2
    invoke-virtual {p0, v6}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->b(Z)V

    .line 164577
    const-string v0, "enabled"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 164578
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-interface {v0}, LX/0yN;->a()LX/1p2;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    invoke-virtual {v0}, LX/0yi;->getStatusKey()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, p1}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v0

    invoke-interface {v0}, LX/1p2;->a()V

    .line 164579
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->d()V

    .line 164580
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->o()V

    .line 164581
    return-void
.end method

.method public a(Ljava/lang/Throwable;LX/0yi;)V
    .locals 3

    .prologue
    .line 164535
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 164536
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne p2, v0, :cond_0

    .line 164537
    const-string v0, "unknown"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/String;)V

    .line 164538
    :goto_0
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1p0;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/1p0;->a(Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 164539
    return-void

    .line 164540
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-interface {v0}, LX/0yN;->a()LX/1p2;

    move-result-object v0

    invoke-virtual {p2}, LX/0yi;->getStatusKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "unknown"

    invoke-interface {v0, v1, v2}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v0

    invoke-interface {v0}, LX/1p2;->a()V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 164582
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-interface {v0}, LX/0yN;->a()LX/1p2;

    move-result-object v0

    const-string v1, "zero_rating2/clearable/zero_unknown_state"

    invoke-interface {v0, v1, p1}, LX/1p2;->a(Ljava/lang/String;Z)LX/1p2;

    move-result-object v0

    invoke-interface {v0}, LX/1p2;->a()V

    .line 164583
    if-nez p1, :cond_0

    .line 164584
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->h:Z

    .line 164585
    :cond_0
    const-class v0, LX/0yK;

    const-string v1, "update_unk_state"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 164586
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 164534
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    const-string v1, "zero_rating2/clearable/zero_unknown_state"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0yN;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 164532
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    const-string v1, "com.facebook.zero.ZERO_RATING_STATE_CHANGED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 164533
    return-void
.end method

.method public final j()V
    .locals 13

    .prologue
    .line 164488
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    .line 164489
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-eq v1, v2, :cond_1

    .line 164490
    const-string v1, "disabled"

    invoke-virtual {p0, v1}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/String;)V

    .line 164491
    :cond_0
    :goto_0
    return-void

    .line 164492
    :cond_1
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1pA;

    invoke-virtual {v1}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v2

    .line 164493
    const-string v1, "none"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 164494
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 164495
    const-string v1, "disabled"

    invoke-virtual {p0, v1}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/String;)V

    .line 164496
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Xl;

    const-string v2, "com.facebook.zero.ZERO_RATING_DISABLED_ON_WIFI"

    invoke-interface {v1, v2}, LX/0Xl;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 164497
    :cond_2
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v7

    const-wide v9, 0x40c1940000000000L    # 9000.0

    mul-double/2addr v7, v9

    const-wide v9, 0x408f400000000000L    # 1000.0

    add-double/2addr v7, v9

    double-to-int v7, v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, ".(?!$)"

    const-string v9, "$0:"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v1, v7

    .line 164498
    iget-object v3, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    const-string v4, "zero_rating2/clearable/code_pairs"

    invoke-interface {v3, v4, v1}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 164499
    new-instance v4, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    invoke-direct {v4, v3}, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;-><init>(Ljava/lang/String;)V

    .line 164500
    sget-object v1, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    invoke-virtual {v4, v1}, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 164501
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1p0;

    const/4 v5, 0x0

    invoke-interface {v1, v3, v5}, LX/1p0;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 164502
    :cond_3
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1pA;

    invoke-virtual {v1}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v3

    .line 164503
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    const-string v5, "zero_rating2/clearable/network_type"

    const-string v6, "none"

    invoke-interface {v1, v5, v6}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 164504
    const-wide/16 v11, 0x3e8

    .line 164505
    iget-object v7, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    const-string v8, "free_data_capping/clearable/free_data_capping_wallet_expiration_timestamp"

    const-wide/16 v9, 0x0

    invoke-interface {v7, v8, v9, v10}, LX/0yN;->a(Ljava/lang/String;J)J

    move-result-wide v9

    .line 164506
    iget-object v7, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->i:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    div-long/2addr v7, v11

    cmp-long v7, v9, v7

    if-gez v7, :cond_8

    iget-object v7, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->i:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    div-long/2addr v7, v11

    const-wide/32 v11, 0x15180

    sub-long/2addr v7, v11

    cmp-long v7, v9, v7

    if-lez v7, :cond_8

    .line 164507
    sget-object v7, LX/32P;->FREE_DATA_CAPPING_WALLET_EXPIRED:LX/32P;

    invoke-virtual {p0, v0, v7}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/0yi;LX/32P;)V

    .line 164508
    const/4 v7, 0x1

    .line 164509
    :goto_1
    move v1, v7

    .line 164510
    if-nez v1, :cond_0

    .line 164511
    iget-object v7, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getLastTimeCheckedKey()Ljava/lang/String;

    move-result-object v8

    const-wide/16 v9, 0x0

    invoke-interface {v7, v8, v9, v10}, LX/0yN;->a(Ljava/lang/String;J)J

    move-result-wide v9

    .line 164512
    iget-object v7, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getTokenTTLKey()Ljava/lang/String;

    move-result-object v8

    const/16 v11, 0xe10

    invoke-interface {v7, v8, v11}, LX/0yN;->a(Ljava/lang/String;I)I

    move-result v8

    .line 164513
    iget-object v7, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->i:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v11

    sub-long v9, v11, v9

    int-to-long v7, v8

    const-wide/16 v11, 0x3e8

    mul-long/2addr v7, v11

    cmp-long v7, v9, v7

    if-gez v7, :cond_9

    const/4 v7, 0x1

    :goto_2
    move v1, v7

    .line 164514
    if-nez v1, :cond_4

    .line 164515
    sget-object v1, LX/32P;->TTL_EXPIRED:LX/32P;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/0yi;LX/32P;)V

    goto/16 :goto_0

    .line 164516
    :cond_4
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 164517
    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/0yi;)V

    goto/16 :goto_0

    .line 164518
    :cond_5
    invoke-virtual {v3, v4}, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 164519
    :cond_6
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-interface {v1}, LX/0yN;->a()LX/1p2;

    move-result-object v1

    const-string v4, "zero_rating2/clearable/code_pairs"

    .line 164520
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 164521
    iget-object v7, v6, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    move-object v6, v7

    .line 164522
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 164523
    iget-object v7, v6, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    move-object v6, v7

    .line 164524
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 164525
    iget-object v7, v6, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    move-object v6, v7

    .line 164526
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    .line 164527
    iget-object v7, v6, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    move-object v6, v7

    .line 164528
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v3, v5

    .line 164529
    invoke-interface {v1, v4, v3}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v1

    const-string v3, "zero_rating2/clearable/network_type"

    invoke-interface {v1, v3, v2}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v1

    invoke-interface {v1}, LX/1p2;->a()V

    .line 164530
    sget-object v1, LX/32P;->MCCMNC_CHANGED:LX/32P;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/0yi;LX/32P;)V

    goto/16 :goto_0

    .line 164531
    :cond_7
    const-string v1, "enabled"

    invoke-virtual {p0, v1}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const/4 v7, 0x0

    goto/16 :goto_1

    :cond_9
    const/4 v7, 0x0

    goto/16 :goto_2
.end method

.method public k()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164476
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    .line 164477
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getStatusKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "unknown"

    invoke-interface {v1, v2, v3}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164478
    iget-object v2, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getRegistrationStatusKey()Ljava/lang/String;

    move-result-object v3

    const-string v4, "unknown"

    invoke-interface {v2, v3, v4}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 164479
    iget-object v3, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getUnregisteredReasonKey()Ljava/lang/String;

    move-result-object v4

    const-string v5, "unavailable"

    invoke-interface {v3, v4, v5}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 164480
    iget-object v4, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getCampaignIdKey()Ljava/lang/String;

    move-result-object v0

    const-string v5, ""

    invoke-interface {v4, v0, v5}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164481
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 164482
    const-string v5, "campaign_id"

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164483
    const-string v0, "zero_rating_status"

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164484
    const-string v0, "registration_status"

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164485
    const-string v0, "unregistered_reason"

    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164486
    const-string v0, "zero_unknown_state"

    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164487
    return-object v4
.end method

.method public l()V
    .locals 6

    .prologue
    .line 164466
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 164467
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-interface {v1}, LX/0yN;->a()LX/1p2;

    move-result-object v2

    .line 164468
    invoke-static {}, LX/0yi;->values()[LX/0yi;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 164469
    invoke-virtual {v5}, LX/0yi;->getClearablePreferencesRoot()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1p2;->a(Ljava/lang/String;)LX/1p2;

    .line 164470
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164471
    :cond_0
    invoke-interface {v2}, LX/1p2;->a()V

    .line 164472
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->m:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 164473
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->d()V

    .line 164474
    :cond_1
    sget-object v0, LX/32P;->DEBUG:LX/32P;

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/32P;)V

    .line 164475
    return-void
.end method

.method public final m()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 164464
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v0

    .line 164465
    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    const-string v2, "zero_rating2/allow_zero_rating_on_wifi"

    invoke-interface {v0, v2, v1}, LX/0yN;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public n()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 164462
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->b(Z)V

    .line 164463
    return-void
.end method

.method public o()V
    .locals 3

    .prologue
    .line 164457
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    invoke-virtual {v0}, LX/0yi;->getUnregisteredReasonKey()Ljava/lang/String;

    move-result-object v0

    const-string v2, "unavailable"

    invoke-interface {v1, v0, v2}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164458
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ZERO_RATING_STATE_UNREGISTERED_REASON"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164459
    const-string v2, "unregistered_reason"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164460
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 164461
    return-void
.end method
