.class public abstract Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0yJ;
.implements LX/0yK;


# static fields
.field private static final l:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0yN;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yU;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1p3;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0yi;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z

.field public volatile i:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation
.end field

.field private volatile j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation
.end field

.field public volatile k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164622
    const-class v0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;

    sput-object v0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->l:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0yN;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yN;",
            "LX/0Ot",
            "<",
            "LX/0yU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1p3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0yi;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164588
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->h:Z

    .line 164589
    iput-object p1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    .line 164590
    iput-object p2, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->b:LX/0Ot;

    .line 164591
    iput-object p3, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->c:LX/0Ot;

    .line 164592
    iput-object p4, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    .line 164593
    iput-object p5, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e:LX/0Or;

    .line 164594
    iput-object p6, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->f:LX/0Or;

    .line 164595
    iput-object p7, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->g:LX/0Or;

    .line 164596
    return-void
.end method

.method public static declared-synchronized a(Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;)V
    .locals 2

    .prologue
    .line 164623
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yU;

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0yi;

    invoke-virtual {v1}, LX/0yi;->getUIFeaturesKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0yU;->a(Ljava/lang/String;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->i:LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164624
    monitor-exit p0

    return-void

    .line 164625
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;)V
    .locals 6

    .prologue
    .line 164626
    monitor-enter p0

    .line 164627
    :try_start_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 164628
    iput-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->j:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164629
    :try_start_1
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    invoke-virtual {v0}, LX/0yi;->getRewriteRulesKey()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v0, v2}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164630
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164631
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1p3;

    invoke-virtual {v0, v1}, LX/1p3;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0Px;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164632
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 164633
    :catch_0
    move-exception v0

    .line 164634
    :try_start_2
    sget-object v1, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->l:Ljava/lang/Class;

    const-string v2, "Error deserializing rewrite rules: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 164635
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized l(Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;)V
    .locals 6

    .prologue
    .line 164636
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->k:LX/0Px;

    .line 164637
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yi;

    .line 164638
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getBackupRewriteRulesKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0yN;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 164639
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 164640
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a:LX/0yN;

    invoke-virtual {v0}, LX/0yi;->getBackupRewriteRulesKey()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v0, v2}, LX/0yN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164641
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164642
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1p3;

    invoke-virtual {v0, v1}, LX/1p3;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->k:LX/0Px;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 164643
    :catch_0
    move-exception v0

    .line 164644
    :try_start_2
    sget-object v1, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->l:Ljava/lang/Class;

    const-string v2, "Error deserializing backup rewrite rules: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 164645
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164646
    iput-object p1, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->j:LX/0Px;

    .line 164647
    const-class v0, LX/0yK;

    const-string v1, "set_rules"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 164648
    return-void
.end method

.method public abstract a(Lcom/facebook/common/callercontext/CallerContext;)V
.end method

.method public final a(LX/0yY;)Z
    .locals 1

    .prologue
    .line 164621
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public abstract b(Z)V
.end method

.method public abstract c()Z
.end method

.method public abstract d()V
.end method

.method public e()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164618
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->i:LX/0Rf;

    if-nez v0, :cond_0

    .line 164619
    invoke-static {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;)V

    .line 164620
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->i:LX/0Rf;

    return-object v0
.end method

.method public final declared-synchronized f()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164614
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->j:LX/0Px;

    if-nez v0, :cond_0

    .line 164615
    invoke-static {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->b(Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;)V

    .line 164616
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->j:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 164617
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164610
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->k:LX/0Px;

    if-nez v0, :cond_0

    .line 164611
    invoke-static {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->l(Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;)V

    .line 164612
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->k:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 164613
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164597
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 164598
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->h:Z

    if-eqz v0, :cond_1

    .line 164599
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->h:Z

    .line 164600
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->b(Z)V

    .line 164601
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->j()V

    .line 164602
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 164603
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->k:LX/0Px;

    if-nez v0, :cond_2

    .line 164604
    invoke-static {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->l(Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;)V

    .line 164605
    :cond_2
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_3

    .line 164606
    sget-object v0, LX/1oy;->a:LX/0Px;

    .line 164607
    :goto_0
    return-object v0

    .line 164608
    :cond_3
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->k:LX/0Px;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->k:LX/0Px;

    goto :goto_0

    :cond_4
    sget-object v0, LX/1oy;->a:LX/0Px;

    goto :goto_0

    .line 164609
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->f()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
