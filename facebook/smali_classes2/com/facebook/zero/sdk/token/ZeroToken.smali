.class public Lcom/facebook/zero/sdk/token/ZeroToken;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/sdk/token/ZeroToken;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/zero/sdk/token/ZeroToken;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:I

.field public final i:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/lang/String;

.field public final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/lang/String;

.field public final n:I

.field public final o:Ljava/lang/String;

.field public final p:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 165888
    const-class v0, Lcom/facebook/zero/sdk/token/ZeroToken;

    sput-object v0, Lcom/facebook/zero/sdk/token/ZeroToken;->b:Ljava/lang/Class;

    .line 165889
    new-instance v0, Lcom/facebook/zero/sdk/token/ZeroToken;

    .line 165890
    sget-object v2, LX/0Re;->a:LX/0Re;

    move-object v7, v2

    .line 165891
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v8, v2

    .line 165892
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v10, v2

    .line 165893
    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v9, v1

    move-object v11, v1

    move v12, v6

    move-object v13, v1

    move-object v14, v1

    invoke-direct/range {v0 .. v14}, Lcom/facebook/zero/sdk/token/ZeroToken;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Rf;LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;ILjava/lang/String;LX/0P1;)V

    sput-object v0, Lcom/facebook/zero/sdk/token/ZeroToken;->a:Lcom/facebook/zero/sdk/token/ZeroToken;

    .line 165894
    new-instance v0, LX/0yp;

    invoke-direct {v0}, LX/0yp;-><init>()V

    sput-object v0, Lcom/facebook/zero/sdk/token/ZeroToken;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 166006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166007
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->c:Ljava/lang/String;

    .line 166008
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->d:Ljava/lang/String;

    .line 166009
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->f:Ljava/lang/String;

    .line 166010
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->e:Ljava/lang/String;

    .line 166011
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->g:Ljava/lang/String;

    .line 166012
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->h:I

    .line 166013
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0yY;->fromStrings(Ljava/lang/Iterable;)LX/0Rf;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->i:LX/0Rf;

    .line 166014
    sget-object v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->j:LX/0Px;

    .line 166015
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->k:Ljava/lang/String;

    .line 166016
    sget-object v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    .line 166017
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->m:Ljava/lang/String;

    .line 166018
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->n:I

    .line 166019
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->o:Ljava/lang/String;

    .line 166020
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 166021
    const-string v1, "zero_pool_pricing_map_serializable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 166022
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 166023
    if-eqz v0, :cond_1

    .line 166024
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 166025
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 166026
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->p:LX/0P1;

    .line 166027
    :goto_1
    return-void

    .line 166028
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->p:LX/0P1;

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Rf;LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;ILjava/lang/String;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/0Rf",
            "<",
            "LX/0yY;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165991
    iput-object p1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->c:Ljava/lang/String;

    .line 165992
    iput-object p2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->d:Ljava/lang/String;

    .line 165993
    iput-object p3, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->f:Ljava/lang/String;

    .line 165994
    iput-object p4, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->e:Ljava/lang/String;

    .line 165995
    iput-object p5, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->g:Ljava/lang/String;

    .line 165996
    iput p6, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->h:I

    .line 165997
    iput-object p7, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->i:LX/0Rf;

    .line 165998
    iput-object p8, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->j:LX/0Px;

    .line 165999
    iput-object p9, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->k:Ljava/lang/String;

    .line 166000
    iput-object p10, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    .line 166001
    iput-object p11, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->m:Ljava/lang/String;

    .line 166002
    iput p12, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->n:I

    .line 166003
    iput-object p13, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->o:Ljava/lang/String;

    .line 166004
    iput-object p14, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->p:LX/0P1;

    .line 166005
    return-void
.end method

.method public static a(LX/0yi;Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yN;LX/0yU;LX/1p3;LX/2gy;)V
    .locals 8

    .prologue
    .line 165946
    const-string v2, ""

    .line 165947
    const-string v3, ""

    .line 165948
    const-string v0, ""

    .line 165949
    :try_start_0
    iget-object v1, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->i:LX/0Rf;

    move-object v1, v1

    .line 165950
    invoke-virtual {p3, v1}, LX/0yU;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    .line 165951
    iget-object v1, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->j:LX/0Px;

    move-object v1, v1

    .line 165952
    invoke-virtual {p4, v1}, LX/1p3;->a(LX/0Px;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 165953
    :try_start_1
    iget-object v3, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->p:LX/0P1;

    move-object v3, v3

    .line 165954
    iget-object v4, p5, LX/2gy;->a:LX/0lC;

    invoke-virtual {v4, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v0, v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 165955
    :goto_0
    invoke-interface {p2}, LX/0yN;->a()LX/1p2;

    move-result-object v3

    .line 165956
    invoke-virtual {p0}, LX/0yi;->getCampaignIdKey()Ljava/lang/String;

    move-result-object v4

    .line 165957
    iget-object v5, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->c:Ljava/lang/String;

    move-object v5, v5

    .line 165958
    invoke-interface {v3, v4, v5}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v4

    invoke-virtual {p0}, LX/0yi;->getStatusKey()Ljava/lang/String;

    move-result-object v5

    const-string v6, "enabled"

    invoke-interface {v4, v5, v6}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v4

    invoke-virtual {p0}, LX/0yi;->getRegistrationStatusKey()Ljava/lang/String;

    move-result-object v5

    .line 165959
    iget-object v6, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->d:Ljava/lang/String;

    move-object v6, v6

    .line 165960
    invoke-interface {v4, v5, v6}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v4

    invoke-virtual {p0}, LX/0yi;->getCarrierNameKey()Ljava/lang/String;

    move-result-object v5

    .line 165961
    iget-object v6, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->f:Ljava/lang/String;

    move-object v6, v6

    .line 165962
    invoke-interface {v4, v5, v6}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v4

    invoke-virtual {p0}, LX/0yi;->getCarrierIdKey()Ljava/lang/String;

    move-result-object v5

    .line 165963
    iget-object v6, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->e:Ljava/lang/String;

    move-object v6, v6

    .line 165964
    invoke-interface {v4, v5, v6}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v4

    invoke-virtual {p0}, LX/0yi;->getCarrierLogoUrlKey()Ljava/lang/String;

    move-result-object v5

    .line 165965
    iget-object v6, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->g:Ljava/lang/String;

    move-object v6, v6

    .line 165966
    invoke-interface {v4, v5, v6}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v4

    invoke-virtual {p0}, LX/0yi;->getTokenTTLKey()Ljava/lang/String;

    move-result-object v5

    .line 165967
    iget v6, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->h:I

    move v6, v6

    .line 165968
    invoke-interface {v4, v5, v6}, LX/1p2;->a(Ljava/lang/String;I)LX/1p2;

    move-result-object v4

    invoke-virtual {p0}, LX/0yi;->getUIFeaturesKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v2

    invoke-virtual {p0}, LX/0yi;->getRewriteRulesKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v1}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v1

    invoke-virtual {p0}, LX/0yi;->getUnregisteredReasonKey()Ljava/lang/String;

    move-result-object v2

    .line 165969
    iget-object v4, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->k:Ljava/lang/String;

    move-object v4, v4

    .line 165970
    invoke-interface {v1, v2, v4}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v1

    invoke-virtual {p0}, LX/0yi;->getTokenHashKey()Ljava/lang/String;

    move-result-object v2

    .line 165971
    iget-object v4, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->m:Ljava/lang/String;

    move-object v4, v4

    .line 165972
    invoke-interface {v1, v2, v4}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v1

    invoke-virtual {p0}, LX/0yi;->getTokenRequestTimeKey()Ljava/lang/String;

    move-result-object v2

    .line 165973
    iget v4, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->n:I

    move v4, v4

    .line 165974
    invoke-interface {v1, v2, v4}, LX/1p2;->a(Ljava/lang/String;I)LX/1p2;

    move-result-object v1

    invoke-virtual {p0}, LX/0yi;->getTokenFastHashKey()Ljava/lang/String;

    move-result-object v2

    .line 165975
    iget-object v4, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->o:Ljava/lang/String;

    move-object v4, v4

    .line 165976
    invoke-interface {v1, v2, v4}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    move-result-object v1

    invoke-virtual {p0}, LX/0yi;->getPoolPricingMapKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    .line 165977
    iget-object v0, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    move-object v0, v0

    .line 165978
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165979
    const-string v0, ""

    .line 165980
    :try_start_2
    iget-object v1, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    move-object v1, v1

    .line 165981
    invoke-virtual {p4, v1}, LX/1p3;->a(LX/0Px;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    .line 165982
    :goto_1
    invoke-virtual {p0}, LX/0yi;->getBackupRewriteRulesKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v0}, LX/1p2;->a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;

    .line 165983
    :cond_0
    invoke-interface {v3}, LX/1p2;->a()V

    .line 165984
    return-void

    .line 165985
    :catch_0
    move-exception v1

    move-object v7, v1

    move-object v1, v3

    move-object v3, v7

    .line 165986
    :goto_2
    sget-object v4, Lcom/facebook/zero/sdk/token/ZeroToken;->b:Ljava/lang/Class;

    const-string v5, "Error serializing enabled ui features, rewrite rules, and pool pricing map."

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 165987
    :catch_1
    move-exception v1

    .line 165988
    sget-object v2, Lcom/facebook/zero/sdk/token/ZeroToken;->b:Ljava/lang/Class;

    const-string v4, "Error serializing backup rewrite rules."

    invoke-static {v2, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 165989
    :catch_2
    move-exception v3

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 165945
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "-1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 165944
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 165914
    instance-of v1, p1, Lcom/facebook/zero/sdk/token/ZeroToken;

    if-nez v1, :cond_1

    .line 165915
    :cond_0
    :goto_0
    return v0

    .line 165916
    :cond_1
    check-cast p1, Lcom/facebook/zero/sdk/token/ZeroToken;

    .line 165917
    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->c:Ljava/lang/String;

    .line 165918
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->c:Ljava/lang/String;

    move-object v2, v2

    .line 165919
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->d:Ljava/lang/String;

    .line 165920
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->d:Ljava/lang/String;

    move-object v2, v2

    .line 165921
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->f:Ljava/lang/String;

    .line 165922
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->f:Ljava/lang/String;

    move-object v2, v2

    .line 165923
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->e:Ljava/lang/String;

    .line 165924
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->e:Ljava/lang/String;

    move-object v2, v2

    .line 165925
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->g:Ljava/lang/String;

    .line 165926
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->g:Ljava/lang/String;

    move-object v2, v2

    .line 165927
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 165928
    iget v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->h:I

    move v2, v2

    .line 165929
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->i:LX/0Rf;

    .line 165930
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->i:LX/0Rf;

    move-object v2, v2

    .line 165931
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->j:LX/0Px;

    .line 165932
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->j:LX/0Px;

    move-object v2, v2

    .line 165933
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->k:Ljava/lang/String;

    .line 165934
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->k:Ljava/lang/String;

    move-object v2, v2

    .line 165935
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    .line 165936
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    move-object v2, v2

    .line 165937
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->m:Ljava/lang/String;

    .line 165938
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->m:Ljava/lang/String;

    move-object v2, v2

    .line 165939
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->o:Ljava/lang/String;

    .line 165940
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->o:Ljava/lang/String;

    move-object v2, v2

    .line 165941
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->p:LX/0P1;

    .line 165942
    iget-object v2, p1, Lcom/facebook/zero/sdk/token/ZeroToken;->p:LX/0P1;

    move-object v2, v2

    .line 165943
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 165913
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->i:LX/0Rf;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->j:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->m:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->o:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->p:LX/0P1;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 165912
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "campaignId"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "regStatus"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "carrierName"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "carrierId"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "carrierLogoUrl"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "ttl"

    iget v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->h:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "enabledUiFeatures"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->i:LX/0Rf;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "rewriteRules"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->j:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "unregistered_reason"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "backupRewriteRules"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "tokenHash"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "requestTime"

    iget v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->n:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "fastTokenHash"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "poolPricingMap"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->p:LX/0P1;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 165895
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165896
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165897
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165898
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165899
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165900
    iget v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 165901
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->i:LX/0Rf;

    invoke-static {v0}, LX/0yY;->toStrings(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 165902
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->j:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 165903
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165904
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->l:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 165905
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165906
    iget v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->n:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 165907
    iget-object v0, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165908
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 165909
    const-string v1, "zero_pool_pricing_map_serializable"

    iget-object v2, p0, Lcom/facebook/zero/sdk/token/ZeroToken;->p:LX/0P1;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 165910
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 165911
    return-void
.end method
