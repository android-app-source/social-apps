.class public Lcom/facebook/fbreact/fragment/FbReactFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fj;
.implements LX/0ic;
.implements LX/0o1;


# static fields
.field private static final f:LX/1tk;


# instance fields
.field private A:I

.field private B:Ljava/lang/String;

.field public C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field public a:LX/33u;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/98j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/98q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0mh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:LX/98l;

.field private final h:LX/98n;

.field private final i:LX/33z;

.field public volatile j:Landroid/os/Bundle;

.field public k:Lcom/facebook/widget/CustomFrameLayout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/react/ReactRootView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/1ZF;

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:I

.field private t:Z

.field public u:Landroid/os/Bundle;

.field private v:I

.field private w:Ljava/lang/String;

.field private x:Z

.field public y:Ljava/lang/String;

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 137467
    const-string v0, "react_native"

    const-string v1, "immersive_react_fragment_start_react_application"

    sget-object v2, LX/0mq;->CLIENT_EVENT:LX/0mq;

    invoke-static {v0, v1, v3, v2, v3}, LX/1tk;->a(Ljava/lang/String;Ljava/lang/String;ZLX/0mq;Z)LX/1tk;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbreact/fragment/FbReactFragment;->f:LX/1tk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 137468
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 137469
    new-instance v0, LX/98m;

    invoke-direct {v0, p0}, LX/98m;-><init>(Lcom/facebook/fbreact/fragment/FbReactFragment;)V

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->g:LX/98l;

    .line 137470
    new-instance v0, LX/98n;

    invoke-direct {v0, p0}, LX/98n;-><init>(Lcom/facebook/fbreact/fragment/FbReactFragment;)V

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->h:LX/98n;

    .line 137471
    new-instance v0, LX/33z;

    invoke-direct {v0, p0}, LX/33z;-><init>(Lcom/facebook/fbreact/fragment/FbReactFragment;)V

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->i:LX/33z;

    .line 137472
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->w:Ljava/lang/String;

    .line 137473
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->x:Z

    return-void
.end method

.method public static a$redex0(Lcom/facebook/fbreact/fragment/FbReactFragment;LX/5pG;)V
    .locals 4

    .prologue
    .line 137474
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->m:LX/1ZF;

    if-nez v0, :cond_1

    .line 137475
    :cond_0
    :goto_0
    return-void

    .line 137476
    :cond_1
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->m:LX/1ZF;

    iget-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->t:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, LX/1ZF;->k_(Z)V

    .line 137477
    if-eqz p1, :cond_4

    .line 137478
    const-string v0, "title"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137479
    const-string v0, "title"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->B:Ljava/lang/String;

    .line 137480
    :cond_2
    const-string v0, "icon"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 137481
    const-string v0, "icon"

    invoke-interface {p1, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    .line 137482
    if-eqz v0, :cond_3

    const-string v1, "glyphName"

    invoke-interface {v0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 137483
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 137484
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "glyphName"

    invoke-interface {v0, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "drawable"

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->z:I

    .line 137485
    :cond_3
    const-string v0, "enabled"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 137486
    const-string v0, "enabled"

    invoke-interface {p1, v0}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->x:Z

    .line 137487
    :cond_4
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    .line 137488
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->B:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 137489
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->B:Ljava/lang/String;

    .line 137490
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 137491
    :goto_2
    iget-boolean v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->x:Z

    .line 137492
    iput-boolean v1, v0, LX/108;->d:Z

    .line 137493
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 137494
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->m:LX/1ZF;

    invoke-interface {v1, v0}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 137495
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->m:LX/1ZF;

    new-instance v1, LX/98o;

    invoke-direct {v1, p0}, LX/98o;-><init>(Lcom/facebook/fbreact/fragment/FbReactFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto/16 :goto_0

    .line 137496
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 137497
    :cond_6
    iget v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->z:I

    if-eqz v1, :cond_7

    .line 137498
    iget v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->z:I

    .line 137499
    iput v1, v0, LX/108;->i:I

    .line 137500
    goto :goto_2

    .line 137501
    :cond_7
    iget v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->A:I

    if-lez v1, :cond_0

    .line 137502
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->A:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 137503
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 137504
    goto :goto_2
.end method

.method public static b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;
    .locals 1

    .prologue
    .line 137505
    new-instance v0, Lcom/facebook/fbreact/fragment/FbReactFragment;

    invoke-direct {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;-><init>()V

    .line 137506
    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 137507
    return-object v0
.end method

.method public static d(Lcom/facebook/fbreact/fragment/FbReactFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 137508
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137509
    :cond_0
    :goto_0
    return-void

    .line 137510
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 137511
    if-eqz v0, :cond_0

    .line 137512
    const v1, 0x7f0d00bc

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 137513
    if-eqz v0, :cond_0

    .line 137514
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private m()V
    .locals 9

    .prologue
    .line 137515
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    invoke-static {v0}, LX/5pd;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137516
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->d:LX/0mh;

    sget-object v1, Lcom/facebook/fbreact/fragment/FbReactFragment;->f:LX/1tk;

    invoke-virtual {v0, v1}, LX/0mh;->a(LX/1tk;)LX/0mj;

    move-result-object v0

    .line 137517
    invoke-virtual {v0}, LX/0mj;->a()Z

    move-result v1

    .line 137518
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->q:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 137519
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    if-nez v2, :cond_0

    .line 137520
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    .line 137521
    :cond_0
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    const-string v3, "uri"

    .line 137522
    iget-object v4, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->p:Ljava/lang/String;

    .line 137523
    iget-object v5, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    if-nez v5, :cond_6

    .line 137524
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    iput-object v5, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    .line 137525
    :cond_1
    :goto_0
    move-object v4, v4

    .line 137526
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137527
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    const-string v3, "routeName"

    iget-object v4, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->q:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137528
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v3

    const-string v4, "FacebookAppRouteHandler"

    iget-object v5, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/react/ReactRootView;->a(LX/33y;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 137529
    if-eqz v1, :cond_2

    .line 137530
    const-string v2, "routeName"

    iget-object v3, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->q:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 137531
    :cond_2
    :goto_1
    if-eqz v1, :cond_4

    .line 137532
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->p:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 137533
    const-string v1, "uri"

    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 137534
    :cond_3
    invoke-virtual {v0}, LX/0mj;->e()V

    .line 137535
    :cond_4
    return-void

    .line 137536
    :cond_5
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->r:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 137537
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->r:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/react/ReactRootView;->a(LX/33y;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 137538
    if-eqz v1, :cond_2

    .line 137539
    const-string v2, "moduleName"

    iget-object v3, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    goto :goto_1

    .line 137540
    :cond_6
    iget-object v5, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    invoke-virtual {v5}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 137541
    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 137542
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 137543
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 137544
    iget-object v7, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    invoke-virtual {v7, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 137545
    if-eqz v7, :cond_7

    const-string v8, "uri"

    if-eq v4, v8, :cond_7

    .line 137546
    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_2

    .line 137547
    :cond_8
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public static n(Lcom/facebook/fbreact/fragment/FbReactFragment;)LX/5pX;
    .locals 1

    .prologue
    .line 137548
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v0

    .line 137549
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Lcom/facebook/react/ReactRootView;
    .locals 2

    .prologue
    .line 137550
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    if-nez v0, :cond_0

    .line 137551
    new-instance v0, Lcom/facebook/react/ReactRootView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/react/ReactRootView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    .line 137552
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    const v1, 0x7f0d01bd

    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactRootView;->setId(I)V

    .line 137553
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 137554
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "nav_bar_tint_color"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137555
    invoke-static {p0, v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->d(Lcom/facebook/fbreact/fragment/FbReactFragment;Ljava/lang/String;)V

    .line 137556
    iget-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->C:Z

    if-eqz v0, :cond_1

    .line 137557
    :cond_0
    :goto_0
    return-void

    .line 137558
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->m:LX/1ZF;

    if-nez v0, :cond_2

    .line 137559
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->m:LX/1ZF;

    .line 137560
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->m:LX/1ZF;

    if-eqz v0, :cond_0

    .line 137561
    iget v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->s:I

    if-eqz v0, :cond_3

    .line 137562
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->m:LX/1ZF;

    iget v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->s:I

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 137563
    :cond_3
    invoke-static {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->r(Lcom/facebook/fbreact/fragment/FbReactFragment;)V

    goto :goto_0
.end method

.method public static r(Lcom/facebook/fbreact/fragment/FbReactFragment;)V
    .locals 1

    .prologue
    .line 137564
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a$redex0(Lcom/facebook/fbreact/fragment/FbReactFragment;LX/5pG;)V

    .line 137565
    return-void
.end method

.method private t()V
    .locals 3

    .prologue
    .line 137566
    iget-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->C:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->D:Z

    if-nez v0, :cond_0

    .line 137567
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->D:Z

    .line 137568
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->c:LX/98q;

    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->h:LX/98n;

    .line 137569
    iget-object v2, v0, LX/98q;->c:LX/98n;

    if-ne v1, v2, :cond_1

    .line 137570
    :cond_0
    :goto_0
    return-void

    .line 137571
    :cond_1
    iget-object v2, v0, LX/98q;->c:LX/98n;

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/98q;->c:LX/98n;

    if-eq v2, v1, :cond_2

    .line 137572
    const-string v2, "FbReactFragmentHooks"

    const-string p0, "New listener registered before unresgister is called on previous listener."

    invoke-static {v2, p0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    .line 137573
    iget-object p0, v0, LX/98q;->b:LX/03V;

    invoke-virtual {p0, v2}, LX/03V;->a(LX/0VG;)V

    .line 137574
    :cond_2
    iput-object v1, v0, LX/98q;->c:LX/98n;

    goto :goto_0
.end method

.method public static u(Lcom/facebook/fbreact/fragment/FbReactFragment;)V
    .locals 3

    .prologue
    .line 137575
    iget-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->C:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->D:Z

    if-eqz v0, :cond_0

    .line 137576
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->D:Z

    .line 137577
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->c:LX/98q;

    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->h:LX/98n;

    .line 137578
    iget-object v2, v0, LX/98q;->c:LX/98n;

    if-nez v2, :cond_1

    .line 137579
    :cond_0
    :goto_0
    return-void

    .line 137580
    :cond_1
    iget-object v2, v0, LX/98q;->c:LX/98n;

    if-eq v1, v2, :cond_2

    .line 137581
    const-string v2, "FbReactFragmentHooks"

    const-string p0, "Unregister listener called with the wrong listener passed in. A different fragmentthan the current one listening is calling unregister."

    invoke-static {v2, p0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    .line 137582
    iget-object p0, v0, LX/98q;->b:LX/03V;

    invoke-virtual {p0, v2}, LX/03V;->a(LX/0VG;)V

    .line 137583
    :cond_2
    const/4 v2, 0x0

    iput-object v2, v0, LX/98q;->c:LX/98n;

    goto :goto_0
.end method


# virtual methods
.method public S_()Z
    .locals 3

    .prologue
    .line 137584
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v0

    .line 137585
    invoke-static {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->n(Lcom/facebook/fbreact/fragment/FbReactFragment;)LX/5pX;

    move-result-object v1

    .line 137586
    iget-boolean v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->o:Z

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/5pX;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137587
    invoke-virtual {v0}, LX/33y;->e()V

    .line 137588
    const/4 v0, 0x1

    .line 137589
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137590
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->w:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 137591
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v4, p0

    check-cast v4, Lcom/facebook/fbreact/fragment/FbReactFragment;

    invoke-static {v0}, LX/33u;->a(LX/0QB;)LX/33u;

    move-result-object v5

    check-cast v5, LX/33u;

    invoke-static {v0}, LX/98j;->a(LX/0QB;)LX/98j;

    move-result-object v6

    check-cast v6, LX/98j;

    invoke-static {v0}, LX/98q;->a(LX/0QB;)LX/98q;

    move-result-object v7

    check-cast v7, LX/98q;

    invoke-static {v0}, LX/0mF;->a(LX/0QB;)LX/0mh;

    move-result-object v8

    check-cast v8, LX/0mh;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v5, v4, Lcom/facebook/fbreact/fragment/FbReactFragment;->a:LX/33u;

    iput-object v6, v4, Lcom/facebook/fbreact/fragment/FbReactFragment;->b:LX/98j;

    iput-object v7, v4, Lcom/facebook/fbreact/fragment/FbReactFragment;->c:LX/98q;

    iput-object v8, v4, Lcom/facebook/fbreact/fragment/FbReactFragment;->d:LX/0mh;

    iput-object v0, v4, Lcom/facebook/fbreact/fragment/FbReactFragment;->e:LX/0Uh;

    .line 137592
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 137593
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v0

    .line 137594
    const-string v0, "uri"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->p:Ljava/lang/String;

    .line 137595
    const-string v0, "route_name"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->q:Ljava/lang/String;

    .line 137596
    const-string v0, "module_name"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->r:Ljava/lang/String;

    .line 137597
    const-string v0, "title_res"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->s:I

    .line 137598
    const-string v0, "show_search"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "parent_control_title_bar"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->t:Z

    .line 137599
    const-string v0, "init_props"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->u:Landroid/os/Bundle;

    .line 137600
    const-string v0, "analytics_tag"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->w:Ljava/lang/String;

    .line 137601
    const-string v0, "requested_orientation"

    const/4 v4, -0x1

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->v:I

    .line 137602
    const-string v0, "button_res"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->A:I

    .line 137603
    const-string v0, "button_event"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->y:Ljava/lang/String;

    .line 137604
    const-string v0, "non_immersive"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->C:Z

    .line 137605
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->q:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->r:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 137606
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->w:Ljava/lang/String;

    const-string v2, "unknown"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137607
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->q:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 137608
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "react_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->q:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->w:Ljava/lang/String;

    .line 137609
    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->v:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 137610
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->c()V

    .line 137611
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->b:LX/98j;

    invoke-virtual {v0, p0}, LX/98j;->a(LX/0o1;)V

    .line 137612
    iput-boolean v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->E:Z

    .line 137613
    return-void

    :cond_3
    move v0, v2

    .line 137614
    goto :goto_0

    .line 137615
    :cond_4
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->r:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 137616
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "react_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->w:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 137460
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    if-eqz v0, :cond_0

    .line 137461
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomFrameLayout;->removeAllViews()V

    .line 137462
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0310fe

    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 137463
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;)V

    .line 137464
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    .line 137465
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->o:Z

    .line 137466
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 137349
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 137350
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 137617
    invoke-static {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->n(Lcom/facebook/fbreact/fragment/FbReactFragment;)LX/5pX;

    move-result-object v0

    .line 137618
    if-nez v0, :cond_0

    .line 137619
    :goto_0
    return-void

    .line 137620
    :cond_0
    const-class v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-interface {v0, p1, p2}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 137351
    const/4 v0, 0x0

    .line 137352
    invoke-static {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->n(Lcom/facebook/fbreact/fragment/FbReactFragment;)LX/5pX;

    move-result-object v1

    .line 137353
    if-nez v1, :cond_1

    .line 137354
    :cond_0
    :goto_0
    return-void

    .line 137355
    :cond_1
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    move-object v2, v2

    .line 137356
    if-eqz v2, :cond_0

    .line 137357
    const-string v3, "%d/%s"

    invoke-virtual {v2}, Lcom/facebook/react/ReactRootView;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3, v2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 137358
    const-class v3, Lcom/facebook/fbreact/interfaces/RCTViewEventEmitter;

    invoke-virtual {v1, v3}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbreact/interfaces/RCTViewEventEmitter;

    invoke-interface {v1, v2, v0}, Lcom/facebook/fbreact/interfaces/RCTViewEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 137359
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v0

    .line 137360
    if-eqz v0, :cond_0

    .line 137361
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 137362
    if-eqz v1, :cond_0

    .line 137363
    invoke-virtual {v0}, LX/33y;->h()V

    .line 137364
    const/4 v0, 0x1

    .line 137365
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 137366
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 137367
    return-void
.end method

.method public final l()LX/33y;
    .locals 1

    .prologue
    .line 137368
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->a:LX/33u;

    invoke-virtual {v0}, LX/33u;->c()LX/33y;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xfcb9c7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137369
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 137370
    iput-object p1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->j:Landroid/os/Bundle;

    .line 137371
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->q()V

    .line 137372
    const/16 v1, 0x2b

    const v2, -0x28771c0e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 137373
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 137374
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->t()V

    .line 137375
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v0

    .line 137376
    if-eqz v0, :cond_0

    .line 137377
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p3}, LX/33y;->a(Landroid/app/Activity;IILandroid/content/Intent;)V

    .line 137378
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/4 v4, -0x1

    const/16 v0, 0x2a

    const v1, 0x1290d0a

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137379
    new-instance v1, Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    .line 137380
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomFrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 137381
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->o()Lcom/facebook/react/ReactRootView;

    move-result-object v2

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 137382
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    const v2, 0x7f0d01be

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomFrameLayout;->setId(I)V

    .line 137383
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    const/16 v2, 0x2b

    const v3, -0x6f4ed4fa

    invoke-static {v5, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x75a85e6e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137384
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->a:LX/33u;

    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->i:LX/33z;

    invoke-virtual {v1, v2}, LX/33u;->b(LX/33z;)V

    .line 137385
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->a:LX/33u;

    .line 137386
    iget-object v2, v1, LX/33u;->g:Ljava/util/Set;

    invoke-interface {v2, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 137387
    iget-object v2, v1, LX/33u;->h:Ljava/util/Set;

    invoke-interface {v2, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 137388
    iget-object v2, v1, LX/33u;->g:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v1, LX/33u;->h:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 137389
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->a:LX/33u;

    invoke-virtual {v1}, LX/33u;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137390
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v1

    invoke-virtual {v1}, LX/33y;->g()V

    .line 137391
    :cond_0
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->e:LX/0Uh;

    const/16 v2, 0x43f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137392
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->a:LX/33u;

    invoke-virtual {v1}, LX/33u;->b()V

    .line 137393
    :cond_1
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->b:LX/98j;

    if-eqz v1, :cond_2

    .line 137394
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->b:LX/98j;

    invoke-virtual {v1, p0}, LX/98j;->b(LX/0o1;)V

    .line 137395
    :cond_2
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 137396
    const/16 v1, 0x2b

    const v2, -0x1b6a042

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x42f54048

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137397
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 137398
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    if-eqz v1, :cond_0

    .line 137399
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    invoke-virtual {v1}, Lcom/facebook/react/ReactRootView;->a()V

    .line 137400
    iput-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    .line 137401
    :cond_0
    iput-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    .line 137402
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->o:Z

    .line 137403
    const/16 v1, 0x2b

    const v2, 0x27a55428

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3b698b8a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137404
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v1

    .line 137405
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->a:LX/33u;

    .line 137406
    iget-object v4, v2, LX/33u;->g:Ljava/util/Set;

    invoke-interface {v4, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 137407
    iget-object v4, v2, LX/33u;->h:Ljava/util/Set;

    invoke-interface {v4, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 137408
    iget-object v4, v2, LX/33u;->g:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    move v2, v4

    .line 137409
    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->o:Z

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 137410
    invoke-virtual {v1}, LX/33y;->f()V

    .line 137411
    :cond_0
    invoke-static {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->u(Lcom/facebook/fbreact/fragment/FbReactFragment;)V

    .line 137412
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 137413
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 137414
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->a:LX/33u;

    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->i:LX/33z;

    .line 137415
    iget-object v4, v1, LX/33u;->k:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 137416
    :cond_1
    const-string v1, "viewDidDisappear"

    invoke-virtual {p0, v1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Ljava/lang/String;)V

    .line 137417
    const/16 v1, 0x2b

    const v2, 0x2c4150b2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v5, -0x1

    const/16 v0, 0x2a

    const v1, 0x177c892d

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137418
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 137419
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->t()V

    .line 137420
    iget-object v1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->a:LX/33u;

    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->i:LX/33z;

    invoke-virtual {v1, v2}, LX/33u;->b(LX/33z;)V

    .line 137421
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v1

    .line 137422
    iget-boolean v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->o:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->n:Z

    if-eqz v2, :cond_2

    .line 137423
    :cond_0
    iput-boolean v3, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->o:Z

    .line 137424
    iput-boolean v3, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->n:Z

    .line 137425
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    if-eqz v2, :cond_1

    .line 137426
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomFrameLayout;->removeAllViews()V

    .line 137427
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->k:Lcom/facebook/widget/CustomFrameLayout;

    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->o()Lcom/facebook/react/ReactRootView;

    move-result-object v3

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v4}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 137428
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->d()V

    .line 137429
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->m()V

    .line 137430
    :cond_2
    iget-object v2, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->a:LX/33u;

    .line 137431
    iget-object v3, v2, LX/33u;->g:Ljava/util/Set;

    invoke-interface {v3, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 137432
    iget-object v3, v2, LX/33u;->h:Ljava/util/Set;

    invoke-interface {v3, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 137433
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->g:LX/98l;

    invoke-virtual {v1, v2, v3}, LX/33y;->a(Landroid/app/Activity;LX/98l;)V

    .line 137434
    const-string v1, "viewDidAppear"

    invoke-virtual {p0, v1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Ljava/lang/String;)V

    .line 137435
    const/16 v1, 0x2b

    const v2, -0x7a67ea9f

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 137436
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 137437
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->j:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 137438
    iget-object v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->j:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 137439
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x52a5b5ab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137440
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 137441
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->q()V

    .line 137442
    const/16 v1, 0x2b

    const v2, -0x5594872f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 137443
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 137444
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->d()V

    .line 137445
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->m()V

    .line 137446
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 137447
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 137448
    iget-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->E:Z

    if-nez v0, :cond_1

    .line 137449
    :cond_0
    :goto_0
    return-void

    .line 137450
    :cond_1
    if-eqz p1, :cond_3

    .line 137451
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->t()V

    .line 137452
    :goto_1
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 137453
    if-eqz v0, :cond_0

    .line 137454
    if-eqz p1, :cond_4

    iget-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->F:Z

    if-nez v0, :cond_4

    .line 137455
    const-string v0, "viewDidAppear"

    invoke-virtual {p0, v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Ljava/lang/String;)V

    .line 137456
    :cond_2
    :goto_2
    iput-boolean p1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->F:Z

    goto :goto_0

    .line 137457
    :cond_3
    invoke-static {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->u(Lcom/facebook/fbreact/fragment/FbReactFragment;)V

    goto :goto_1

    .line 137458
    :cond_4
    if-nez p1, :cond_2

    iget-boolean v0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->F:Z

    if-eqz v0, :cond_2

    .line 137459
    const-string v0, "viewDidDisappear"

    invoke-virtual {p0, v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Ljava/lang/String;)V

    goto :goto_2
.end method
