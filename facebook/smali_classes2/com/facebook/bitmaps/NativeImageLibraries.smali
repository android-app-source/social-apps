.class public Lcom/facebook/bitmaps/NativeImageLibraries;
.super LX/03m;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:Ljava/lang/String;

.field private static volatile c:Lcom/facebook/bitmaps/NativeImageLibraries;


# instance fields
.field private b:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 225390
    const-class v0, Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/bitmaps/NativeImageLibraries;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 1
    .param p1    # LX/03V;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225387
    const-string v0, "fb_imgproc"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/03m;-><init>(Ljava/util/List;)V

    .line 225388
    iput-object p1, p0, Lcom/facebook/bitmaps/NativeImageLibraries;->b:LX/03V;

    .line 225389
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/bitmaps/NativeImageLibraries;
    .locals 3

    .prologue
    .line 225377
    sget-object v0, Lcom/facebook/bitmaps/NativeImageLibraries;->c:Lcom/facebook/bitmaps/NativeImageLibraries;

    if-nez v0, :cond_1

    .line 225378
    const-class v1, Lcom/facebook/bitmaps/NativeImageLibraries;

    monitor-enter v1

    .line 225379
    :try_start_0
    sget-object v0, Lcom/facebook/bitmaps/NativeImageLibraries;->c:Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225380
    if-eqz v2, :cond_0

    .line 225381
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/bitmaps/NativeImageLibraries;->b(LX/0QB;)Lcom/facebook/bitmaps/NativeImageLibraries;

    move-result-object v0

    sput-object v0, Lcom/facebook/bitmaps/NativeImageLibraries;->c:Lcom/facebook/bitmaps/NativeImageLibraries;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225382
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225383
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225384
    :cond_1
    sget-object v0, Lcom/facebook/bitmaps/NativeImageLibraries;->c:Lcom/facebook/bitmaps/NativeImageLibraries;

    return-object v0

    .line 225385
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225386
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/bitmaps/NativeImageLibraries;
    .locals 2

    .prologue
    .line 225375
    new-instance v1, Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v0}, Lcom/facebook/bitmaps/NativeImageLibraries;-><init>(LX/03V;)V

    .line 225376
    return-object v1
.end method

.method private d()V
    .locals 4

    .prologue
    .line 225391
    const-string v0, "UklGRiIAAABXRUJQVlA4IBYAAAAwAQCdASoBAAEADsD+JaQAA3AAAAAA"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 225392
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 225393
    sget-object v0, LX/0hW;->b:Ljava/io/OutputStream;

    move-object v0, v0

    .line 225394
    const/16 v2, 0x50

    :try_start_0
    invoke-virtual {p0, v1, v0, v2}, Lcom/facebook/bitmaps/NativeImageLibraries;->transcode2Jpeg(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225395
    :goto_0
    return-void

    .line 225396
    :catch_0
    move-exception v0

    .line 225397
    iget-object v1, p0, Lcom/facebook/bitmaps/NativeImageLibraries;->b:LX/03V;

    if-eqz v1, :cond_0

    .line 225398
    iget-object v1, p0, Lcom/facebook/bitmaps/NativeImageLibraries;->b:LX/03V;

    sget-object v2, Lcom/facebook/bitmaps/NativeImageLibraries;->a:Ljava/lang/String;

    const-string v3, "IOException thrown while testing if library is loaded"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 225399
    :cond_0
    sget-object v1, Lcom/facebook/bitmaps/NativeImageLibraries;->a:Ljava/lang/String;

    const-string v2, "IOException thrown while testing if library is loaded"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private native nativeCheck()I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method


# virtual methods
.method public final c()V
    .locals 4

    .prologue
    .line 225368
    invoke-direct {p0}, Lcom/facebook/bitmaps/NativeImageLibraries;->nativeCheck()I

    .line 225369
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/bitmaps/NativeImageLibraries;->d()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 225370
    return-void

    .line 225371
    :catch_0
    move-exception v0

    .line 225372
    iget-object v1, p0, Lcom/facebook/bitmaps/NativeImageLibraries;->b:LX/03V;

    if-eqz v1, :cond_0

    .line 225373
    iget-object v1, p0, Lcom/facebook/bitmaps/NativeImageLibraries;->b:LX/03V;

    sget-object v2, Lcom/facebook/bitmaps/NativeImageLibraries;->a:Ljava/lang/String;

    const-string v3, "LIBRARY_HALF_LOADED - Native didn\'t load all methods"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 225374
    :cond_0
    throw v0
.end method

.method public native extractXmpMetadata(Ljava/io/InputStream;)[B
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public native transcode2Jpeg(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public native transcode2Png(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method
