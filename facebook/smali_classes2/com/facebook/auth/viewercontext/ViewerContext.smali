.class public Lcom/facebook/auth/viewercontext/ViewerContext;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final d:Z

.field public final e:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61014
    new-instance v0, LX/0SJ;

    invoke-direct {v0}, LX/0SJ;-><init>()V

    sput-object v0, Lcom/facebook/auth/viewercontext/ViewerContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61006
    iput-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    .line 61007
    iput-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    .line 61008
    iput-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    .line 61009
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    .line 61010
    iput-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    .line 61011
    iput-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    .line 61012
    iput-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    .line 61013
    return-void
.end method

.method public constructor <init>(LX/0SK;)V
    .locals 1

    .prologue
    .line 60989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60990
    iget-object v0, p1, LX/0SK;->a:Ljava/lang/String;

    move-object v0, v0

    .line 60991
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    .line 60992
    iget-object v0, p1, LX/0SK;->b:Ljava/lang/String;

    move-object v0, v0

    .line 60993
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    .line 60994
    iget-object v0, p1, LX/0SK;->c:Ljava/lang/String;

    move-object v0, v0

    .line 60995
    iput-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    .line 60996
    iget-boolean v0, p1, LX/0SK;->d:Z

    move v0, v0

    .line 60997
    iput-boolean v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    .line 60998
    iget-object v0, p1, LX/0SK;->e:Ljava/lang/String;

    move-object v0, v0

    .line 60999
    iput-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    .line 61000
    iget-object v0, p1, LX/0SK;->f:Ljava/lang/String;

    move-object v0, v0

    .line 61001
    iput-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    .line 61002
    iget-object v0, p1, LX/0SK;->g:Ljava/lang/String;

    move-object v0, v0

    .line 61003
    iput-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    .line 61004
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 60979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60980
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    .line 60981
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    .line 60982
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    .line 60983
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    .line 60984
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    .line 60985
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    .line 60986
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    .line 60987
    return-void

    .line 60988
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newBuilder()LX/0SK;
    .locals 1

    .prologue
    .line 61015
    new-instance v0, LX/0SK;

    invoke-direct {v0}, LX/0SK;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60978
    iget-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60977
    iget-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 60976
    iget-boolean v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 60975
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 60966
    iget-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60967
    iget-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60968
    iget-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60969
    iget-boolean v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60970
    iget-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60971
    iget-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60972
    iget-object v0, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60973
    return-void

    .line 60974
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
