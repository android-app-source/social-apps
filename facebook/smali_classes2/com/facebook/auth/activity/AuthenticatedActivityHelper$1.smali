.class public final Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:LX/0l3;


# direct methods
.method public constructor <init>(LX/0l3;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 169508
    iput-object p1, p0, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;->b:LX/0l3;

    iput-object p2, p0, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;->a:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 169509
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 169510
    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGOUT_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 169511
    iget-object v1, p0, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;->b:LX/0l3;

    new-instance v2, LX/14k;

    iget-object v3, p0, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;->a:Landroid/app/Activity;

    invoke-direct {v2, p0, v3, v0}, LX/14k;-><init>(Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;Landroid/content/Context;Landroid/content/IntentFilter;)V

    iput-object v2, v1, LX/0l3;->a:LX/14l;

    .line 169512
    iget-object v0, p0, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;->b:LX/0l3;

    iget-object v0, v0, LX/0l3;->a:LX/14l;

    invoke-virtual {v0}, LX/14l;->a()V

    .line 169513
    iget-object v0, p0, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;->b:LX/0l3;

    iget-object v1, p0, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;->b:LX/0l3;

    iget-object v1, v1, LX/0l3;->h:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "ACTION_MQTT_NO_AUTH"

    new-instance v3, LX/14n;

    invoke-direct {v3, p0}, LX/14n;-><init>(Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    .line 169514
    iput-object v1, v0, LX/0l3;->l:LX/0Yb;

    .line 169515
    iget-object v0, p0, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;->b:LX/0l3;

    iget-object v0, v0, LX/0l3;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 169516
    return-void
.end method
