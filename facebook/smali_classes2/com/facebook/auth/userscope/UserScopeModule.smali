.class public Lcom/facebook/auth/userscope/UserScopeModule;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60593
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 60594
    return-void
.end method

.method public static a()LX/0S2;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 60595
    invoke-static {}, LX/0nE;->a()Ljava/lang/AssertionError;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final configure()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NontrivialConfigureMethod"
        }
    .end annotation

    .prologue
    .line 60596
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    move-object v0, v0

    .line 60597
    const-class v1, LX/0S2;

    invoke-virtual {v0, v1}, LX/0RH;->b(Ljava/lang/Class;)LX/0RO;

    move-result-object v1

    new-instance v2, LX/0S3;

    invoke-direct {v2}, LX/0S3;-><init>()V

    invoke-interface {v1, v2}, LX/0RS;->a(LX/0Or;)LX/0RR;

    .line 60598
    new-instance v0, LX/0S2;

    invoke-direct {v0}, LX/0S2;-><init>()V

    .line 60599
    const-class v1, Lcom/facebook/auth/userscope/UserScoped;

    invoke-virtual {p0, v1, v0}, LX/0Q3;->bindScope(Ljava/lang/Class;LX/0RC;)V

    .line 60600
    const-class v1, LX/0S2;

    invoke-virtual {p0, v1}, LX/0Q3;->bind(Ljava/lang/Class;)LX/0RO;

    move-result-object v1

    invoke-interface {v1, v0}, LX/0RS;->a(Ljava/lang/Object;)V

    .line 60601
    return-void
.end method
