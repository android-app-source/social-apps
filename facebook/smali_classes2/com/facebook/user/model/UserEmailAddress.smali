.class public Lcom/facebook/user/model/UserEmailAddress;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/user/model/UserEmailAddress;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 331287
    new-instance v0, LX/1uB;

    invoke-direct {v0}, LX/1uB;-><init>()V

    sput-object v0, Lcom/facebook/user/model/UserEmailAddress;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 331288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331289
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/UserEmailAddress;->a:Ljava/lang/String;

    .line 331290
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/user/model/UserEmailAddress;->b:I

    .line 331291
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 331292
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;IZ)V

    .line 331293
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 331294
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;IZ)V

    .line 331295
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 331296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331297
    if-eqz p3, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lcom/facebook/user/model/UserEmailAddress;->a:Ljava/lang/String;

    .line 331298
    iput p2, p0, Lcom/facebook/user/model/UserEmailAddress;->b:I

    .line 331299
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 331300
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 331301
    iget-object v0, p0, Lcom/facebook/user/model/UserEmailAddress;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 331302
    iget v0, p0, Lcom/facebook/user/model/UserEmailAddress;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 331303
    return-void
.end method
