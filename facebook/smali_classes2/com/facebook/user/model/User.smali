.class public Lcom/facebook/user/model/User;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Z

.field public final B:Z

.field public final C:I

.field public final D:I

.field public final E:I

.field public final F:Z

.field public final G:Z

.field public final H:LX/03R;

.field public final I:Z

.field public final J:Z

.field public final K:Z

.field public final L:Z

.field public M:J

.field public final N:J

.field public final O:Z

.field public final P:Z

.field public final Q:F

.field public final R:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final S:Z

.field public final T:Z

.field public final U:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final V:Z

.field public final W:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final X:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final Y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final Z:I

.field public final a:Ljava/lang/String;

.field public final aa:Z

.field public final ab:Z

.field public final ac:LX/0XN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ad:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ae:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final af:LX/0XK;

.field public final ag:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ah:Lcom/facebook/user/model/UserKey;

.field public final ai:Lcom/facebook/user/model/UserIdentifier;

.field private aj:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserPhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field public ak:Ljava/lang/String;

.field public al:Lcom/facebook/user/model/PicSquare;

.field public am:Ljava/lang/String;

.field public final b:LX/0XG;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserEmailAddress;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserCustomTag;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/user/model/Name;

.field private final f:Lcom/facebook/user/model/Name;

.field public final g:Ljava/lang/String;

.field public final h:LX/0XJ;

.field private final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:F

.field public final n:LX/03R;

.field public final o:Z

.field public final p:Z

.field public final q:Ljava/lang/String;

.field public final r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:Z

.field public final t:Z

.field public final u:LX/4nY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/4nX;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final w:J

.field public final x:J

.field public final y:Z

.field public final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78387
    new-instance v0, LX/0XO;

    invoke-direct {v0}, LX/0XO;-><init>()V

    sput-object v0, Lcom/facebook/user/model/User;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0XI;)V
    .locals 5

    .prologue
    .line 78388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78389
    iget-object v0, p1, LX/0XI;->b:Ljava/lang/String;

    move-object v0, v0

    .line 78390
    const-string v1, "id must not be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    .line 78391
    iget-object v0, p1, LX/0XI;->a:LX/0XG;

    move-object v0, v0

    .line 78392
    const-string v1, "type must not be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0XG;

    iput-object v0, p0, Lcom/facebook/user/model/User;->b:LX/0XG;

    .line 78393
    new-instance v0, Lcom/facebook/user/model/UserKey;

    iget-object v1, p0, Lcom/facebook/user/model/User;->b:LX/0XG;

    iget-object v2, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    .line 78394
    iget-object v0, p1, LX/0XI;->c:Ljava/util/List;

    move-object v0, v0

    .line 78395
    if-nez v0, :cond_0

    .line 78396
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 78397
    iput-object v0, p0, Lcom/facebook/user/model/User;->c:LX/0Px;

    .line 78398
    :goto_0
    iget-object v0, p1, LX/0XI;->e:LX/0Px;

    move-object v0, v0

    .line 78399
    if-nez v0, :cond_1

    .line 78400
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 78401
    iput-object v0, p0, Lcom/facebook/user/model/User;->d:LX/0Px;

    .line 78402
    :goto_1
    iget-object v0, p1, LX/0XI;->d:Ljava/util/List;

    move-object v0, v0

    .line 78403
    if-nez v0, :cond_2

    .line 78404
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 78405
    iput-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    .line 78406
    :goto_2
    iget-object v0, p1, LX/0XI;->g:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 78407
    if-eqz v0, :cond_3

    .line 78408
    iget-object v0, p1, LX/0XI;->g:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 78409
    :goto_3
    move-object v0, v0

    .line 78410
    iput-object v0, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    .line 78411
    iget-object v0, p1, LX/0XI;->k:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 78412
    iput-object v0, p0, Lcom/facebook/user/model/User;->f:Lcom/facebook/user/model/Name;

    .line 78413
    iget-object v0, p1, LX/0XI;->l:Ljava/lang/String;

    move-object v0, v0

    .line 78414
    iput-object v0, p0, Lcom/facebook/user/model/User;->g:Ljava/lang/String;

    .line 78415
    iget-object v0, p1, LX/0XI;->m:LX/0XJ;

    move-object v0, v0

    .line 78416
    iput-object v0, p0, Lcom/facebook/user/model/User;->h:LX/0XJ;

    .line 78417
    iget-object v0, p1, LX/0XI;->n:Ljava/lang/String;

    move-object v0, v0

    .line 78418
    iput-object v0, p0, Lcom/facebook/user/model/User;->i:Ljava/lang/String;

    .line 78419
    iget-object v0, p1, LX/0XI;->o:Ljava/lang/String;

    move-object v0, v0

    .line 78420
    iput-object v0, p0, Lcom/facebook/user/model/User;->j:Ljava/lang/String;

    .line 78421
    iget-object v0, p1, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    move-object v0, v0

    .line 78422
    iput-object v0, p0, Lcom/facebook/user/model/User;->al:Lcom/facebook/user/model/PicSquare;

    .line 78423
    iget-object v0, p1, LX/0XI;->r:Ljava/lang/String;

    move-object v0, v0

    .line 78424
    iput-object v0, p0, Lcom/facebook/user/model/User;->k:Ljava/lang/String;

    .line 78425
    iget-object v0, p1, LX/0XI;->s:Ljava/lang/String;

    move-object v0, v0

    .line 78426
    iput-object v0, p0, Lcom/facebook/user/model/User;->l:Ljava/lang/String;

    .line 78427
    iget v0, p1, LX/0XI;->t:F

    move v0, v0

    .line 78428
    iput v0, p0, Lcom/facebook/user/model/User;->m:F

    .line 78429
    iget-object v0, p1, LX/0XI;->u:LX/03R;

    move-object v0, v0

    .line 78430
    iput-object v0, p0, Lcom/facebook/user/model/User;->n:LX/03R;

    .line 78431
    iget-boolean v0, p1, LX/0XI;->v:Z

    move v0, v0

    .line 78432
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->o:Z

    .line 78433
    iget-boolean v0, p1, LX/0XI;->w:Z

    move v0, v0

    .line 78434
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->p:Z

    .line 78435
    iget-object v0, p1, LX/0XI;->x:Ljava/lang/String;

    move-object v0, v0

    .line 78436
    iput-object v0, p0, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    .line 78437
    iget-object v0, p1, LX/0XI;->y:Ljava/lang/String;

    move-object v0, v0

    .line 78438
    iput-object v0, p0, Lcom/facebook/user/model/User;->r:Ljava/lang/String;

    .line 78439
    iget-boolean v0, p1, LX/0XI;->z:Z

    move v0, v0

    .line 78440
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->s:Z

    .line 78441
    iget-boolean v0, p1, LX/0XI;->A:Z

    move v0, v0

    .line 78442
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->t:Z

    .line 78443
    iget-object v0, p1, LX/0XI;->B:LX/4nY;

    move-object v0, v0

    .line 78444
    iput-object v0, p0, Lcom/facebook/user/model/User;->u:LX/4nY;

    .line 78445
    iget-object v0, p1, LX/0XI;->C:LX/0Px;

    move-object v0, v0

    .line 78446
    iput-object v0, p0, Lcom/facebook/user/model/User;->v:LX/0Px;

    .line 78447
    iget-wide v3, p1, LX/0XI;->D:J

    move-wide v0, v3

    .line 78448
    iput-wide v0, p0, Lcom/facebook/user/model/User;->w:J

    .line 78449
    iget-wide v3, p1, LX/0XI;->E:J

    move-wide v0, v3

    .line 78450
    iput-wide v0, p0, Lcom/facebook/user/model/User;->x:J

    .line 78451
    iget-boolean v0, p1, LX/0XI;->F:Z

    move v0, v0

    .line 78452
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->y:Z

    .line 78453
    iget-boolean v0, p1, LX/0XI;->G:Z

    move v0, v0

    .line 78454
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->z:Z

    .line 78455
    iget-boolean v0, p1, LX/0XI;->H:Z

    move v0, v0

    .line 78456
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->A:Z

    .line 78457
    iget-boolean v0, p1, LX/0XI;->I:Z

    move v0, v0

    .line 78458
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->B:Z

    .line 78459
    invoke-direct {p0}, Lcom/facebook/user/model/User;->ay()Lcom/facebook/user/model/UserIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->ai:Lcom/facebook/user/model/UserIdentifier;

    .line 78460
    iget v0, p1, LX/0XI;->J:I

    move v0, v0

    .line 78461
    iput v0, p0, Lcom/facebook/user/model/User;->C:I

    .line 78462
    iget v0, p1, LX/0XI;->K:I

    move v0, v0

    .line 78463
    iput v0, p0, Lcom/facebook/user/model/User;->D:I

    .line 78464
    iget v0, p1, LX/0XI;->L:I

    move v0, v0

    .line 78465
    iput v0, p0, Lcom/facebook/user/model/User;->E:I

    .line 78466
    iget-boolean v0, p1, LX/0XI;->M:Z

    move v0, v0

    .line 78467
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->F:Z

    .line 78468
    iget-boolean v0, p1, LX/0XI;->N:Z

    move v0, v0

    .line 78469
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->G:Z

    .line 78470
    iget-object v0, p1, LX/0XI;->O:LX/03R;

    move-object v0, v0

    .line 78471
    iput-object v0, p0, Lcom/facebook/user/model/User;->H:LX/03R;

    .line 78472
    iget-boolean v0, p1, LX/0XI;->P:Z

    move v0, v0

    .line 78473
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->I:Z

    .line 78474
    iget-boolean v0, p1, LX/0XI;->Q:Z

    move v0, v0

    .line 78475
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->J:Z

    .line 78476
    iget-boolean v0, p1, LX/0XI;->R:Z

    move v0, v0

    .line 78477
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->K:Z

    .line 78478
    iget-boolean v0, p1, LX/0XI;->am:Z

    move v0, v0

    .line 78479
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->L:Z

    .line 78480
    iget-wide v3, p1, LX/0XI;->an:J

    move-wide v0, v3

    .line 78481
    iput-wide v0, p0, Lcom/facebook/user/model/User;->M:J

    .line 78482
    iget-wide v3, p1, LX/0XI;->X:J

    move-wide v0, v3

    .line 78483
    iput-wide v0, p0, Lcom/facebook/user/model/User;->N:J

    .line 78484
    iget-boolean v0, p1, LX/0XI;->Y:Z

    move v0, v0

    .line 78485
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->O:Z

    .line 78486
    iget-boolean v0, p1, LX/0XI;->S:Z

    move v0, v0

    .line 78487
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->P:Z

    .line 78488
    iget-object v0, p1, LX/0XI;->f:Ljava/lang/String;

    move-object v0, v0

    .line 78489
    iput-object v0, p0, Lcom/facebook/user/model/User;->ak:Ljava/lang/String;

    .line 78490
    iget-object v0, p1, LX/0XI;->q:Ljava/lang/String;

    move-object v0, v0

    .line 78491
    iput-object v0, p0, Lcom/facebook/user/model/User;->am:Ljava/lang/String;

    .line 78492
    iget v0, p1, LX/0XI;->T:F

    move v0, v0

    .line 78493
    iput v0, p0, Lcom/facebook/user/model/User;->Q:F

    .line 78494
    iget-object v0, p1, LX/0XI;->U:LX/0Px;

    move-object v0, v0

    .line 78495
    iput-object v0, p0, Lcom/facebook/user/model/User;->R:LX/0Px;

    .line 78496
    iget-boolean v0, p1, LX/0XI;->V:Z

    move v0, v0

    .line 78497
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->S:Z

    .line 78498
    iget-boolean v0, p1, LX/0XI;->W:Z

    move v0, v0

    .line 78499
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->T:Z

    .line 78500
    iget-object v0, p1, LX/0XI;->Z:LX/0Px;

    move-object v0, v0

    .line 78501
    iput-object v0, p0, Lcom/facebook/user/model/User;->U:LX/0Px;

    .line 78502
    iget-boolean v0, p1, LX/0XI;->aa:Z

    move v0, v0

    .line 78503
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->V:Z

    .line 78504
    iget-object v0, p1, LX/0XI;->ab:Landroid/net/Uri;

    move-object v0, v0

    .line 78505
    iput-object v0, p0, Lcom/facebook/user/model/User;->W:Landroid/net/Uri;

    .line 78506
    iget-object v0, p1, LX/0XI;->ac:Ljava/lang/String;

    move-object v0, v0

    .line 78507
    iput-object v0, p0, Lcom/facebook/user/model/User;->X:Ljava/lang/String;

    .line 78508
    iget-object v0, p1, LX/0XI;->ad:Ljava/lang/String;

    move-object v0, v0

    .line 78509
    iput-object v0, p0, Lcom/facebook/user/model/User;->Y:Ljava/lang/String;

    .line 78510
    iget v0, p1, LX/0XI;->ae:I

    move v0, v0

    .line 78511
    iput v0, p0, Lcom/facebook/user/model/User;->Z:I

    .line 78512
    iget-boolean v0, p1, LX/0XI;->af:Z

    move v0, v0

    .line 78513
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->aa:Z

    .line 78514
    iget-boolean v0, p1, LX/0XI;->ag:Z

    move v0, v0

    .line 78515
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->ab:Z

    .line 78516
    iget-object v0, p1, LX/0XI;->ah:LX/0XN;

    move-object v0, v0

    .line 78517
    iput-object v0, p0, Lcom/facebook/user/model/User;->ac:LX/0XN;

    .line 78518
    iget-object v0, p1, LX/0XI;->ai:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    move-object v0, v0

    .line 78519
    iput-object v0, p0, Lcom/facebook/user/model/User;->ad:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    .line 78520
    iget-object v0, p1, LX/0XI;->aj:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 78521
    iput-object v0, p0, Lcom/facebook/user/model/User;->ae:Lcom/facebook/user/model/User;

    .line 78522
    iget-object v0, p1, LX/0XI;->ak:LX/0XK;

    move-object v0, v0

    .line 78523
    iput-object v0, p0, Lcom/facebook/user/model/User;->af:LX/0XK;

    .line 78524
    iget-object v0, p1, LX/0XI;->al:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 78525
    iput-object v0, p0, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    .line 78526
    return-void

    .line 78527
    :cond_0
    iget-object v0, p1, LX/0XI;->c:Ljava/util/List;

    move-object v0, v0

    .line 78528
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->c:LX/0Px;

    goto/16 :goto_0

    .line 78529
    :cond_1
    iget-object v0, p1, LX/0XI;->e:LX/0Px;

    move-object v0, v0

    .line 78530
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->d:LX/0Px;

    goto/16 :goto_1

    .line 78531
    :cond_2
    iget-object v0, p1, LX/0XI;->d:Ljava/util/List;

    move-object v0, v0

    .line 78532
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    goto/16 :goto_2

    :cond_3
    new-instance v0, Lcom/facebook/user/model/Name;

    .line 78533
    iget-object v1, p1, LX/0XI;->i:Ljava/lang/String;

    move-object v1, v1

    .line 78534
    iget-object v2, p1, LX/0XI;->j:Ljava/lang/String;

    move-object v2, v2

    .line 78535
    iget-object v3, p1, LX/0XI;->h:Ljava/lang/String;

    move-object v3, v3

    .line 78536
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78538
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    .line 78539
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XG;->valueOf(Ljava/lang/String;)LX/0XG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->b:LX/0XG;

    .line 78540
    new-instance v0, Lcom/facebook/user/model/UserKey;

    iget-object v4, p0, Lcom/facebook/user/model/User;->b:LX/0XG;

    iget-object v5, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    invoke-direct {v0, v4, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    .line 78541
    const-class v0, Lcom/facebook/user/model/UserEmailAddress;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->c:LX/0Px;

    .line 78542
    const-class v0, Lcom/facebook/user/model/UserCustomTag;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->d:LX/0Px;

    .line 78543
    const-class v0, Lcom/facebook/user/model/UserPhoneNumber;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    .line 78544
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    iput-object v0, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    .line 78545
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    iput-object v0, p0, Lcom/facebook/user/model/User;->f:Lcom/facebook/user/model/Name;

    .line 78546
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->g:Ljava/lang/String;

    .line 78547
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XJ;->valueOf(Ljava/lang/String;)LX/0XJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->h:LX/0XJ;

    .line 78548
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->i:Ljava/lang/String;

    .line 78549
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->j:Ljava/lang/String;

    .line 78550
    const-class v0, Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/PicSquare;

    iput-object v0, p0, Lcom/facebook/user/model/User;->al:Lcom/facebook/user/model/PicSquare;

    .line 78551
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->k:Ljava/lang/String;

    .line 78552
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->l:Ljava/lang/String;

    .line 78553
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/user/model/User;->m:F

    .line 78554
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/03R;->valueOf(Ljava/lang/String;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->n:LX/03R;

    .line 78555
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->o:Z

    .line 78556
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->p:Z

    .line 78557
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    .line 78558
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->r:Ljava/lang/String;

    .line 78559
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->s:Z

    .line 78560
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/user/model/User;->w:J

    .line 78561
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/user/model/User;->x:J

    .line 78562
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->y:Z

    .line 78563
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->z:Z

    .line 78564
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->A:Z

    .line 78565
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->B:Z

    .line 78566
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/user/model/User;->C:I

    .line 78567
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/user/model/User;->D:I

    .line 78568
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/user/model/User;->E:I

    .line 78569
    invoke-direct {p0}, Lcom/facebook/user/model/User;->ay()Lcom/facebook/user/model/UserIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->ai:Lcom/facebook/user/model/UserIdentifier;

    .line 78570
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->F:Z

    .line 78571
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->G:Z

    .line 78572
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->H:LX/03R;

    .line 78573
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->t:Z

    .line 78574
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->I:Z

    .line 78575
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->J:Z

    .line 78576
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->K:Z

    .line 78577
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    .line 78578
    if-nez v0, :cond_18

    .line 78579
    :goto_d
    move-object v0, v4

    .line 78580
    iput-object v0, p0, Lcom/facebook/user/model/User;->u:LX/4nY;

    .line 78581
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->L:Z

    .line 78582
    const-class v0, LX/4nX;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 78583
    if-nez v0, :cond_e

    move-object v0, v3

    :goto_f
    iput-object v0, p0, Lcom/facebook/user/model/User;->v:LX/0Px;

    .line 78584
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/user/model/User;->M:J

    .line 78585
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/user/model/User;->N:J

    .line 78586
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_f

    move v0, v1

    :goto_10
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->O:Z

    .line 78587
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_10

    move v0, v1

    :goto_11
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->P:Z

    .line 78588
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->ak:Ljava/lang/String;

    .line 78589
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->am:Ljava/lang/String;

    .line 78590
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/user/model/User;->Q:F

    .line 78591
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 78592
    if-nez v0, :cond_11

    move-object v0, v3

    :goto_12
    iput-object v0, p0, Lcom/facebook/user/model/User;->R:LX/0Px;

    .line 78593
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_12

    move v0, v1

    :goto_13
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->S:Z

    .line 78594
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_13

    move v0, v1

    :goto_14
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->T:Z

    .line 78595
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 78596
    if-nez v0, :cond_14

    :goto_15
    iput-object v3, p0, Lcom/facebook/user/model/User;->U:LX/0Px;

    .line 78597
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_15

    move v0, v1

    :goto_16
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->V:Z

    .line 78598
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/user/model/User;->W:Landroid/net/Uri;

    .line 78599
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->X:Ljava/lang/String;

    .line 78600
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->Y:Ljava/lang/String;

    .line 78601
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/user/model/User;->Z:I

    .line 78602
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_16

    move v0, v1

    :goto_17
    iput-boolean v0, p0, Lcom/facebook/user/model/User;->aa:Z

    .line 78603
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_17

    :goto_18
    iput-boolean v1, p0, Lcom/facebook/user/model/User;->ab:Z

    .line 78604
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 78605
    :try_start_0
    invoke-static {v0}, LX/0XN;->valueOf(Ljava/lang/String;)LX/0XN;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 78606
    :goto_19
    move-object v0, v1

    .line 78607
    iput-object v0, p0, Lcom/facebook/user/model/User;->ac:LX/0XN;

    .line 78608
    const-class v0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    iput-object v0, p0, Lcom/facebook/user/model/User;->ad:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    .line 78609
    const-class v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/user/model/User;->ae:Lcom/facebook/user/model/User;

    .line 78610
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 78611
    invoke-static {v0}, LX/0XK;->valueOf(Ljava/lang/String;)LX/0XK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/User;->af:LX/0XK;

    .line 78612
    const-class v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    .line 78613
    return-void

    :cond_0
    move v0, v2

    .line 78614
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 78615
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 78616
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 78617
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 78618
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 78619
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 78620
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 78621
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 78622
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 78623
    goto/16 :goto_9

    :cond_a
    move v0, v2

    .line 78624
    goto/16 :goto_a

    :cond_b
    move v0, v2

    .line 78625
    goto/16 :goto_b

    :cond_c
    move v0, v2

    .line 78626
    goto/16 :goto_c

    :cond_d
    move v0, v2

    .line 78627
    goto/16 :goto_e

    .line 78628
    :cond_e
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto/16 :goto_f

    :cond_f
    move v0, v2

    .line 78629
    goto/16 :goto_10

    :cond_10
    move v0, v2

    .line 78630
    goto/16 :goto_11

    .line 78631
    :cond_11
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto/16 :goto_12

    :cond_12
    move v0, v2

    .line 78632
    goto/16 :goto_13

    :cond_13
    move v0, v2

    .line 78633
    goto/16 :goto_14

    .line 78634
    :cond_14
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    goto/16 :goto_15

    :cond_15
    move v0, v2

    .line 78635
    goto/16 :goto_16

    :cond_16
    move v0, v2

    .line 78636
    goto/16 :goto_17

    :cond_17
    move v1, v2

    .line 78637
    goto/16 :goto_18

    .line 78638
    :cond_18
    :try_start_1
    invoke-static {v0}, LX/4nY;->valueOf(Ljava/lang/String;)LX/4nY;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    goto/16 :goto_d

    .line 78639
    :catch_0
    goto/16 :goto_d

    :catch_1
    const/4 v1, 0x0

    goto/16 :goto_19
.end method

.method private aA()LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserPhoneNumber;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 78640
    iget-object v0, p0, Lcom/facebook/user/model/User;->ak:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78641
    const/4 v0, 0x0

    .line 78642
    :goto_0
    return-object v0

    .line 78643
    :cond_0
    :try_start_0
    new-instance v7, Lorg/json/JSONArray;

    iget-object v0, p0, Lcom/facebook/user/model/User;->ak:Ljava/lang/String;

    invoke-direct {v7, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78644
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 78645
    const/4 v0, 0x0

    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v9

    move v6, v0

    :goto_1
    if-ge v6, v9, :cond_2

    .line 78646
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 78647
    const-string v1, "phone_full_number"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 78648
    const-string v1, "phone_display_number"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 78649
    const-string v3, "phone_is_verified"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "phone_is_verified"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v5

    .line 78650
    :goto_2
    const-string v3, "phone_android_type"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    .line 78651
    new-instance v0, Lcom/facebook/user/model/UserPhoneNumber;

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/03R;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 78652
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 78653
    :catch_0
    move-exception v0

    .line 78654
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 78655
    :cond_1
    sget-object v5, LX/03R;->UNSET:LX/03R;

    goto :goto_2

    .line 78656
    :cond_2
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private ay()Lcom/facebook/user/model/UserIdentifier;
    .locals 3

    .prologue
    .line 78657
    iget-object v0, p0, Lcom/facebook/user/model/User;->b:LX/0XG;

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v0, v1, :cond_0

    .line 78658
    new-instance v0, Lcom/facebook/user/model/UserFbidIdentifier;

    iget-object v1, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/user/model/UserFbidIdentifier;-><init>(Ljava/lang/String;)V

    .line 78659
    :goto_0
    return-object v0

    .line 78660
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/model/User;->b:LX/0XG;

    invoke-virtual {v0}, LX/0XG;->isPhoneContact()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78661
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v1

    .line 78662
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->s()Ljava/lang/String;

    move-result-object v2

    .line 78663
    if-eqz v1, :cond_1

    .line 78664
    new-instance v0, Lcom/facebook/user/model/UserSmsIdentifier;

    .line 78665
    iget-object v2, v1, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    move-object v2, v2

    .line 78666
    iget-object p0, v1, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v1, p0

    .line 78667
    invoke-direct {v0, v2, v1}, Lcom/facebook/user/model/UserSmsIdentifier;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 78668
    :cond_1
    if-eqz v2, :cond_2

    .line 78669
    new-instance v0, Lcom/facebook/user/model/UserSmsIdentifier;

    invoke-direct {v0, v2}, Lcom/facebook/user/model/UserSmsIdentifier;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 78670
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static az(Lcom/facebook/user/model/User;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 78676
    iget-object v1, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    if-nez v1, :cond_0

    .line 78677
    const/4 v0, 0x0

    .line 78678
    :goto_0
    return-object v0

    .line 78679
    :cond_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 78680
    iget-object v1, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    .line 78681
    iget-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserPhoneNumber;

    .line 78682
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 78683
    const-string v5, "phone_full_number"

    .line 78684
    iget-object v6, v0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    move-object v6, v6

    .line 78685
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 78686
    const-string v5, "phone_display_number"

    .line 78687
    iget-object v6, v0, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    move-object v6, v6

    .line 78688
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 78689
    iget-object v5, v0, Lcom/facebook/user/model/UserPhoneNumber;->e:LX/03R;

    move-object v5, v5

    .line 78690
    sget-object v6, LX/03R;->UNSET:LX/03R;

    if-eq v5, v6, :cond_1

    .line 78691
    const-string v5, "phone_is_verified"

    .line 78692
    iget-object v6, v0, Lcom/facebook/user/model/UserPhoneNumber;->e:LX/03R;

    move-object v6, v6

    .line 78693
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/03R;->asBoolean(Z)Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 78694
    :cond_1
    const-string v5, "phone_android_type"

    .line 78695
    iget v6, v0, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    move v0, v6

    .line 78696
    invoke-virtual {v4, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 78697
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78698
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 78699
    :catch_0
    move-exception v0

    .line 78700
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 78701
    :cond_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final L()Z
    .locals 1

    .prologue
    .line 78671
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->t:Z

    return v0
.end method

.method public final P()Z
    .locals 2

    .prologue
    .line 78672
    const-string v0, "page"

    iget-object v1, p0, Lcom/facebook/user/model/User;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final aw()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 78673
    iget-object v0, p0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ax()Z
    .locals 1

    .prologue
    .line 78674
    iget-object v0, p0, Lcom/facebook/user/model/User;->b:LX/0XG;

    invoke-virtual {v0}, LX/0XG;->isPhoneContact()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 78675
    iget-object v0, p0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->e()Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78385
    iget-object v0, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/facebook/user/model/UserKey;
    .locals 1

    .prologue
    .line 78386
    iget-object v0, p0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 78248
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78249
    iget-object v0, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78250
    iget-object v0, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78251
    iget-object v0, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78252
    iget-object v0, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Lcom/facebook/user/model/UserFbidIdentifier;
    .locals 1

    .prologue
    .line 78253
    iget-object v0, p0, Lcom/facebook/user/model/User;->ai:Lcom/facebook/user/model/UserIdentifier;

    instance-of v0, v0, Lcom/facebook/user/model/UserFbidIdentifier;

    if-eqz v0, :cond_0

    .line 78254
    iget-object v0, p0, Lcom/facebook/user/model/User;->ai:Lcom/facebook/user/model/UserIdentifier;

    check-cast v0, Lcom/facebook/user/model/UserFbidIdentifier;

    .line 78255
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78256
    iget-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78257
    :cond_0
    invoke-direct {p0}, Lcom/facebook/user/model/User;->aA()LX/0Px;

    move-result-object v0

    .line 78258
    if-eqz v0, :cond_1

    .line 78259
    iput-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    .line 78260
    :cond_1
    iget-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    if-nez v0, :cond_2

    .line 78261
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 78262
    iput-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    .line 78263
    :cond_2
    iget-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78264
    iget-object v0, p0, Lcom/facebook/user/model/User;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/user/model/User;->c:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserEmailAddress;

    .line 78265
    iget-object v1, v0, Lcom/facebook/user/model/UserEmailAddress;->a:Ljava/lang/String;

    move-object v0, v1

    .line 78266
    goto :goto_0
.end method

.method public final t()Lcom/facebook/user/model/UserPhoneNumber;
    .locals 2

    .prologue
    .line 78267
    iget-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserPhoneNumber;

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 78268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 78269
    iget-object v1, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78270
    iget-object v1, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/user/model/User;->b:LX/0XG;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78271
    iget-object v1, p0, Lcom/facebook/user/model/User;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 78272
    iget-object v1, p0, Lcom/facebook/user/model/User;->c:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78273
    :cond_0
    iget-object v1, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 78274
    iget-object v1, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78275
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78276
    iget-object v0, p0, Lcom/facebook/user/model/User;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 78277
    iget-object v0, p0, Lcom/facebook/user/model/User;->i:Ljava/lang/String;

    .line 78278
    :goto_0
    return-object v0

    .line 78279
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/model/User;->al:Lcom/facebook/user/model/PicSquare;

    if-eqz v0, :cond_1

    .line 78280
    iget-object v0, p0, Lcom/facebook/user/model/User;->al:Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v0}, Lcom/facebook/user/model/PicSquare;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    iget-object v0, v0, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    goto :goto_0

    .line 78281
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Lcom/facebook/user/model/PicSquare;
    .locals 7

    .prologue
    .line 78282
    iget-object v0, p0, Lcom/facebook/user/model/User;->al:Lcom/facebook/user/model/PicSquare;

    if-nez v0, :cond_0

    .line 78283
    const/4 v0, 0x0

    .line 78284
    iget-object v1, p0, Lcom/facebook/user/model/User;->am:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78285
    :goto_0
    move-object v0, v0

    .line 78286
    iput-object v0, p0, Lcom/facebook/user/model/User;->al:Lcom/facebook/user/model/PicSquare;

    .line 78287
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/model/User;->al:Lcom/facebook/user/model/PicSquare;

    return-object v0

    .line 78288
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    iget-object v2, p0, Lcom/facebook/user/model/User;->am:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78289
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 78290
    const/4 v0, 0x0

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    :goto_1
    if-ge v0, v3, :cond_2

    .line 78291
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 78292
    const-string v5, "profile_pic_size"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v5

    .line 78293
    const-string v6, "profile_pic_url"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 78294
    new-instance v6, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-direct {v6, v5, v4}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 78295
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 78296
    :cond_2
    new-instance v0, Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/user/model/PicSquare;-><init>(LX/0Px;)V

    goto :goto_0

    .line 78297
    :catch_0
    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78298
    iget-object v0, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78299
    iget-object v0, p0, Lcom/facebook/user/model/User;->b:LX/0XG;

    invoke-virtual {v0}, LX/0XG;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78300
    iget-object v0, p0, Lcom/facebook/user/model/User;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 78301
    iget-object v0, p0, Lcom/facebook/user/model/User;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 78302
    iget-object v0, p0, Lcom/facebook/user/model/User;->aj:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 78303
    iget-object v0, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 78304
    iget-object v0, p0, Lcom/facebook/user/model/User;->f:Lcom/facebook/user/model/Name;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 78305
    iget-object v0, p0, Lcom/facebook/user/model/User;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78306
    iget-object v0, p0, Lcom/facebook/user/model/User;->h:LX/0XJ;

    invoke-virtual {v0}, LX/0XJ;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78307
    iget-object v0, p0, Lcom/facebook/user/model/User;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78308
    iget-object v0, p0, Lcom/facebook/user/model/User;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78309
    iget-object v0, p0, Lcom/facebook/user/model/User;->al:Lcom/facebook/user/model/PicSquare;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 78310
    iget-object v0, p0, Lcom/facebook/user/model/User;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78311
    iget-object v0, p0, Lcom/facebook/user/model/User;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78312
    iget v0, p0, Lcom/facebook/user/model/User;->m:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 78313
    iget-object v0, p0, Lcom/facebook/user/model/User;->n:LX/03R;

    invoke-virtual {v0}, LX/03R;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78314
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->o:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78315
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->p:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78316
    iget-object v0, p0, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78317
    iget-object v0, p0, Lcom/facebook/user/model/User;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78318
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->s:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78319
    iget-wide v4, p0, Lcom/facebook/user/model/User;->w:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 78320
    iget-wide v4, p0, Lcom/facebook/user/model/User;->x:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 78321
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->y:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78322
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->z:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78323
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->A:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78324
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->B:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78325
    iget v0, p0, Lcom/facebook/user/model/User;->C:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78326
    iget v0, p0, Lcom/facebook/user/model/User;->D:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78327
    iget v0, p0, Lcom/facebook/user/model/User;->E:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78328
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->F:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78329
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->G:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78330
    iget-object v0, p0, Lcom/facebook/user/model/User;->H:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78331
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->t:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78332
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->I:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78333
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->J:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78334
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->K:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78335
    iget-object v0, p0, Lcom/facebook/user/model/User;->u:LX/4nY;

    if-nez v0, :cond_d

    move-object v0, v3

    :goto_d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78336
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->L:Z

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78337
    iget-object v0, p0, Lcom/facebook/user/model/User;->v:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 78338
    iget-wide v4, p0, Lcom/facebook/user/model/User;->M:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 78339
    iget-wide v4, p0, Lcom/facebook/user/model/User;->N:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 78340
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->O:Z

    if-eqz v0, :cond_f

    move v0, v1

    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78341
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->P:Z

    if-eqz v0, :cond_10

    move v0, v1

    :goto_10
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78342
    iget-object v0, p0, Lcom/facebook/user/model/User;->ak:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78343
    iget-object v0, p0, Lcom/facebook/user/model/User;->am:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78344
    iget v0, p0, Lcom/facebook/user/model/User;->Q:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 78345
    iget-object v0, p0, Lcom/facebook/user/model/User;->R:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 78346
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->S:Z

    if-eqz v0, :cond_11

    move v0, v1

    :goto_11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78347
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->T:Z

    if-eqz v0, :cond_12

    move v0, v1

    :goto_12
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78348
    iget-object v0, p0, Lcom/facebook/user/model/User;->U:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 78349
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->V:Z

    if-eqz v0, :cond_13

    move v0, v1

    :goto_13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78350
    iget-object v0, p0, Lcom/facebook/user/model/User;->W:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 78351
    iget-object v0, p0, Lcom/facebook/user/model/User;->X:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78352
    iget-object v0, p0, Lcom/facebook/user/model/User;->Y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78353
    iget v0, p0, Lcom/facebook/user/model/User;->Z:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78354
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->aa:Z

    if-eqz v0, :cond_14

    move v0, v1

    :goto_14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78355
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->ab:Z

    if-eqz v0, :cond_15

    :goto_15
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 78356
    iget-object v0, p0, Lcom/facebook/user/model/User;->ac:LX/0XN;

    if-nez v0, :cond_16

    :goto_16
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78357
    iget-object v0, p0, Lcom/facebook/user/model/User;->ad:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 78358
    iget-object v0, p0, Lcom/facebook/user/model/User;->ae:Lcom/facebook/user/model/User;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 78359
    iget-object v0, p0, Lcom/facebook/user/model/User;->af:LX/0XK;

    invoke-virtual {v0}, LX/0XK;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78360
    iget-object v0, p0, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 78361
    return-void

    :cond_0
    move v0, v2

    .line 78362
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 78363
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 78364
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 78365
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 78366
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 78367
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 78368
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 78369
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 78370
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 78371
    goto/16 :goto_9

    :cond_a
    move v0, v2

    .line 78372
    goto/16 :goto_a

    :cond_b
    move v0, v2

    .line 78373
    goto/16 :goto_b

    :cond_c
    move v0, v2

    .line 78374
    goto/16 :goto_c

    .line 78375
    :cond_d
    iget-object v0, p0, Lcom/facebook/user/model/User;->u:LX/4nY;

    invoke-virtual {v0}, LX/4nY;->name()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_d

    :cond_e
    move v0, v2

    .line 78376
    goto/16 :goto_e

    :cond_f
    move v0, v2

    .line 78377
    goto/16 :goto_f

    :cond_10
    move v0, v2

    .line 78378
    goto/16 :goto_10

    :cond_11
    move v0, v2

    .line 78379
    goto/16 :goto_11

    :cond_12
    move v0, v2

    .line 78380
    goto/16 :goto_12

    :cond_13
    move v0, v2

    .line 78381
    goto/16 :goto_13

    :cond_14
    move v0, v2

    .line 78382
    goto :goto_14

    :cond_15
    move v1, v2

    .line 78383
    goto :goto_15

    .line 78384
    :cond_16
    iget-object v0, p0, Lcom/facebook/user/model/User;->ac:LX/0XN;

    invoke-virtual {v0}, LX/0XN;->name()Ljava/lang/String;

    move-result-object v3

    goto :goto_16
.end method
