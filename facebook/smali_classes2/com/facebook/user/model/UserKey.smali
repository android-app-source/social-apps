.class public Lcom/facebook/user/model/UserKey;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/user/model/UserKeyDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field private final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final type:LX/0XG;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 78770
    const-class v0, Lcom/facebook/user/model/UserKeyDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78769
    new-instance v0, LX/0XP;

    invoke-direct {v0}, LX/0XP;-><init>()V

    sput-object v0, Lcom/facebook/user/model/UserKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 78765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78766
    sget-object v0, LX/0XG;->FACEBOOK:LX/0XG;

    iput-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    .line 78767
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    .line 78768
    return-void
.end method

.method public constructor <init>(LX/0XG;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78762
    iput-object p1, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    .line 78763
    iput-object p2, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    .line 78764
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 78759
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0XG;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 78760
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 78751
    if-nez p0, :cond_0

    .line 78752
    const/4 v0, 0x0

    .line 78753
    :goto_0
    return-object v0

    .line 78754
    :cond_0
    const-string v0, ":"

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 78755
    array-length v0, v1

    if-eq v0, v2, :cond_1

    .line 78756
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot parse user key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78757
    :cond_1
    const/4 v0, 0x0

    aget-object v0, v1, v0

    invoke-static {v0}, LX/0XG;->valueOf(Ljava/lang/String;)LX/0XG;

    move-result-object v2

    .line 78758
    new-instance v0, Lcom/facebook/user/model/UserKey;

    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-direct {v0, v2, v1}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78750
    new-instance v0, LX/3Oi;

    invoke-direct {v0}, LX/3Oi;-><init>()V

    invoke-static {p0, v0}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;
    .locals 2

    .prologue
    .line 78749
    new-instance v0, Lcom/facebook/user/model/UserKey;

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v0, v1, p0}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78707
    new-instance v0, LX/4na;

    invoke-direct {v0}, LX/4na;-><init>()V

    invoke-static {p0, v0}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78748
    new-instance v0, LX/3Ok;

    invoke-direct {v0}, LX/3Ok;-><init>()V

    invoke-static {p0, v0}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 78745
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 78746
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/UserKey;->a:Ljava/lang/String;

    .line 78747
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/0XG;
    .locals 1

    .prologue
    .line 78744
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 78743
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78705
    invoke-direct {p0}, Lcom/facebook/user/model/UserKey;->j()V

    .line 78706
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 78708
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 78709
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 78710
    invoke-virtual {p0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    invoke-virtual {v0}, LX/0XG;->isPhoneContact()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    sget-object v1, LX/0XG;->EMAIL:LX/0XG;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78711
    if-ne p0, p1, :cond_1

    .line 78712
    :cond_0
    :goto_0
    return v0

    .line 78713
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 78714
    goto :goto_0

    .line 78715
    :cond_3
    check-cast p1, Lcom/facebook/user/model/UserKey;

    .line 78716
    iget-object v2, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 78717
    goto :goto_0

    .line 78718
    :cond_4
    iget-object v2, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    iget-object v3, p1, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 78719
    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 78720
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    sget-object v1, LX/0XG;->ADDRESS_BOOK:LX/0XG;

    if-ne v0, v1, :cond_0

    .line 78721
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    .line 78722
    :goto_0
    return-object v0

    .line 78723
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    sget-object v1, LX/0XG;->PHONE_NUMBER:LX/0XG;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    sget-object v1, LX/0XG;->EMAIL:LX/0XG;

    if-ne v0, v1, :cond_2

    .line 78724
    :cond_1
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    invoke-static {v0}, LX/0XI;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 78725
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 78726
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    sget-object v1, LX/0XG;->PHONE_NUMBER:LX/0XG;

    if-ne v0, v1, :cond_0

    .line 78727
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    invoke-static {v0}, LX/0XI;->p(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78728
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 78729
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    sget-object v1, LX/0XG;->EMAIL:LX/0XG;

    if-ne v0, v1, :cond_0

    .line 78730
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    invoke-static {v0}, LX/0XI;->p(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78731
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 78732
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 78733
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    invoke-virtual {v1}, LX/0XG;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 78734
    return v0

    .line 78735
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 78736
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    sget-object v1, LX/0XG;->EMAIL:LX/0XG;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    sget-object v1, LX/0XG;->PHONE_NUMBER:LX/0XG;

    if-ne v0, v1, :cond_1

    .line 78737
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    invoke-static {v0}, LX/0XI;->p(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78738
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78739
    invoke-virtual {p0}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 78740
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->type:LX/0XG;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 78741
    iget-object v0, p0, Lcom/facebook/user/model/UserKey;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78742
    return-void
.end method
