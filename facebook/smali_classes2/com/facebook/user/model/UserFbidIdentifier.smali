.class public Lcom/facebook/user/model/UserFbidIdentifier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/user/model/UserIdentifier;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/user/model/UserFbidIdentifier;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78791
    new-instance v0, LX/0XQ;

    invoke-direct {v0}, LX/0XQ;-><init>()V

    sput-object v0, Lcom/facebook/user/model/UserFbidIdentifier;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 78788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78789
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/UserFbidIdentifier;->a:Ljava/lang/String;

    .line 78790
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 78785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78786
    iput-object p1, p0, Lcom/facebook/user/model/UserFbidIdentifier;->a:Ljava/lang/String;

    .line 78787
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78784
    iget-object v0, p0, Lcom/facebook/user/model/UserFbidIdentifier;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 78783
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78777
    if-ne p0, p1, :cond_1

    .line 78778
    :cond_0
    :goto_0
    return v0

    .line 78779
    :cond_1
    instance-of v2, p1, Lcom/facebook/user/model/UserFbidIdentifier;

    if-nez v2, :cond_2

    move v0, v1

    .line 78780
    goto :goto_0

    .line 78781
    :cond_2
    check-cast p1, Lcom/facebook/user/model/UserFbidIdentifier;

    .line 78782
    iget-object v2, p0, Lcom/facebook/user/model/UserFbidIdentifier;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/user/model/UserFbidIdentifier;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/user/model/UserFbidIdentifier;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p1, Lcom/facebook/user/model/UserFbidIdentifier;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 78776
    iget-object v0, p0, Lcom/facebook/user/model/UserFbidIdentifier;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/user/model/UserFbidIdentifier;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 78774
    invoke-virtual {p0}, Lcom/facebook/user/model/UserFbidIdentifier;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78775
    return-void
.end method
