.class public final Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;
.super Lcom/facebook/common/executors/NamedRunnable;
.source ""


# instance fields
.field public final synthetic c:LX/195;


# direct methods
.method public constructor <init>(LX/195;)V
    .locals 2

    .prologue
    .line 207507
    iput-object p1, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    .line 207508
    const-class v0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;

    const-string v1, "ScrollPerfRunnable"

    invoke-direct {p0, v0, v1}, Lcom/facebook/common/executors/NamedRunnable;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 207509
    return-void
.end method

.method private a(LX/0oG;Ljava/util/List;)V
    .locals 3
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0oG;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 207510
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 207511
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v0, v0, LX/195;->j:LX/19A;

    .line 207512
    invoke-static {v0}, LX/19A;->e(LX/19A;)V

    .line 207513
    iget v2, v0, LX/19A;->c:F

    iget p0, v0, LX/19A;->d:F

    cmpl-float v2, v2, p0

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 207514
    if-eqz v0, :cond_0

    .line 207515
    const-string v0, "fps_guessed"

    invoke-static {v1, v0}, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 207516
    :cond_0
    sget-boolean v0, LX/008;->deoptTaint:Z

    if-eqz v0, :cond_1

    .line 207517
    const-string v0, "dex_unopt"

    invoke-static {v1, v0}, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 207518
    :cond_1
    if-eqz p2, :cond_2

    .line 207519
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 207520
    invoke-static {v1, v0}, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_1

    .line 207521
    :cond_2
    const-string v0, "trace_tags"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 207522
    return-void

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 207523
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 207524
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207525
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207526
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 207527
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v0, v0, LX/195;->l:LX/0oG;

    .line 207528
    iget-object v3, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v3, v3, LX/195;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 207529
    const-string v3, "total_time_spent"

    iget-object v4, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-wide v5, v4, LX/195;->m:J

    invoke-virtual {v0, v3, v5, v6}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 207530
    const-string v3, "time_since_startup"

    iget-object v4, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v4, v4, LX/195;->i:LX/199;

    invoke-virtual {v4}, LX/199;->a()J

    move-result-wide v5

    invoke-virtual {v0, v3, v5, v6}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 207531
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v0, v0, LX/195;->l:LX/0oG;

    .line 207532
    const-string v3, "total_skipped_frames"

    iget-object v4, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v4, v4, LX/195;->h:LX/19C;

    iget v4, v4, LX/19C;->a:I

    invoke-virtual {v0, v3, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 207533
    const-string v3, "1_frame_drop"

    iget-object v4, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v4, v4, LX/195;->h:LX/19C;

    iget v4, v4, LX/19C;->b:F

    float-to-double v5, v4

    invoke-virtual {v0, v3, v5, v6}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 207534
    const-string v3, "4_frame_drop"

    iget-object v4, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v4, v4, LX/195;->h:LX/19C;

    iget v4, v4, LX/19C;->c:F

    float-to-double v5, v4

    invoke-virtual {v0, v3, v5, v6}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 207535
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v0, v0, LX/195;->l:LX/0oG;

    .line 207536
    const-string v3, "display_refresh_rate"

    iget-object v4, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v4, v4, LX/195;->j:LX/19A;

    .line 207537
    invoke-static {v4}, LX/19A;->e(LX/19A;)V

    .line 207538
    iget v5, v4, LX/19A;->c:F

    move v4, v5

    .line 207539
    float-to-double v5, v4

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v4, v5

    invoke-virtual {v0, v3, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 207540
    const-string v3, "sanitized_display_refresh_rate"

    iget-object v4, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v4, v4, LX/195;->j:LX/19A;

    .line 207541
    invoke-static {v4}, LX/19A;->e(LX/19A;)V

    .line 207542
    iget v5, v4, LX/19A;->d:F

    move v4, v5

    .line 207543
    float-to-double v5, v4

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v4, v5

    invoke-virtual {v0, v3, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 207544
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v0, v0, LX/195;->l:LX/0oG;

    .line 207545
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 207546
    iget-object v1, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v1, v1, LX/195;->h:LX/19C;

    iget v1, v1, LX/19C;->a:I

    .line 207547
    iget-object v3, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v3, v3, LX/195;->o:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v1

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1YD;

    .line 207548
    iget-object v3, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v3, v3, LX/195;->o:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 207549
    invoke-interface {v1}, LX/1YD;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 207550
    sub-int v1, v4, v3

    move v4, v1

    .line 207551
    goto :goto_0

    .line 207552
    :cond_0
    if-lez v4, :cond_1

    .line 207553
    const-string v1, "unknown"

    invoke-virtual {v5, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 207554
    :cond_1
    const-string v1, "frame_drop_by_autoblame"

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 207555
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 207556
    iget-object v1, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v1, v1, LX/195;->p:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1YD;

    .line 207557
    invoke-interface {v1}, LX/1YD;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v6, v6, LX/195;->p:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207558
    :catch_0
    :goto_2
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v0, v0, LX/195;->k:LX/1KG;

    if-eqz v0, :cond_3

    .line 207559
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v0, v0, LX/195;->k:LX/1KG;

    iget-object v1, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v1, v1, LX/195;->l:LX/0oG;

    invoke-interface {v0, v1}, LX/1KG;->a(LX/0oG;)V

    .line 207560
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v0, v0, LX/195;->l:LX/0oG;

    iget-object v1, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v1, v1, LX/195;->k:LX/1KG;

    invoke-interface {v1}, LX/1KG;->c()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->a(LX/0oG;Ljava/util/List;)V

    .line 207561
    :goto_3
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v0, v0, LX/195;->l:LX/0oG;

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 207562
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    .line 207563
    iput-object v2, v0, LX/195;->l:LX/0oG;

    .line 207564
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    .line 207565
    const-wide/16 v3, 0x0

    iput-wide v3, v0, LX/195;->m:J

    .line 207566
    iget-object v3, v0, LX/195;->o:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 207567
    iget-object v3, v0, LX/195;->p:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 207568
    iget-object v3, v0, LX/195;->h:LX/19C;

    const/4 v5, 0x0

    .line 207569
    const/4 v4, 0x0

    iput v4, v3, LX/19C;->a:I

    .line 207570
    iput v5, v3, LX/19C;->b:F

    .line 207571
    iput v5, v3, LX/19C;->c:F

    .line 207572
    iget-object v3, v0, LX/195;->k:LX/1KG;

    if-eqz v3, :cond_2

    .line 207573
    iget-object v3, v0, LX/195;->k:LX/1KG;

    invoke-interface {v3}, LX/1KG;->d()V

    .line 207574
    :cond_2
    return-void

    .line 207575
    :cond_3
    iget-object v0, p0, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->c:LX/195;

    iget-object v0, v0, LX/195;->l:LX/0oG;

    invoke-direct {p0, v0, v2}, Lcom/facebook/debug/fps/FrameRateLogger$ScrollPerfRunnable;->a(LX/0oG;Ljava/util/List;)V

    goto :goto_3

    .line 207576
    :cond_4
    :try_start_1
    const-string v1, "frame_drop_by_autoblame_overlapped"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
