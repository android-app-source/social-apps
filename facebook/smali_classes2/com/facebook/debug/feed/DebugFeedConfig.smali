.class public Lcom/facebook/debug/feed/DebugFeedConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:Lcom/facebook/debug/feed/DebugFeedConfig;


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 157329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157330
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/debug/feed/DebugFeedConfig;->b:Z

    .line 157331
    iput-object p1, p0, Lcom/facebook/debug/feed/DebugFeedConfig;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 157332
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/debug/feed/DebugFeedConfig;
    .locals 4

    .prologue
    .line 157313
    sget-object v0, Lcom/facebook/debug/feed/DebugFeedConfig;->c:Lcom/facebook/debug/feed/DebugFeedConfig;

    if-nez v0, :cond_1

    .line 157314
    const-class v1, Lcom/facebook/debug/feed/DebugFeedConfig;

    monitor-enter v1

    .line 157315
    :try_start_0
    sget-object v0, Lcom/facebook/debug/feed/DebugFeedConfig;->c:Lcom/facebook/debug/feed/DebugFeedConfig;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 157316
    if-eqz v2, :cond_0

    .line 157317
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 157318
    new-instance p0, Lcom/facebook/debug/feed/DebugFeedConfig;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, Lcom/facebook/debug/feed/DebugFeedConfig;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 157319
    move-object v0, p0

    .line 157320
    sput-object v0, Lcom/facebook/debug/feed/DebugFeedConfig;->c:Lcom/facebook/debug/feed/DebugFeedConfig;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157321
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 157322
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 157323
    :cond_1
    sget-object v0, Lcom/facebook/debug/feed/DebugFeedConfig;->c:Lcom/facebook/debug/feed/DebugFeedConfig;

    return-object v0

    .line 157324
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 157325
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 157328
    const-string v1, "enable_debug_feed"

    const-string v2, "false"

    invoke-static {v1, v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/debug/feed/DebugFeedConfig;->b:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/debug/feed/DebugFeedConfig;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0pP;->t:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public setDebugFeedEnabled(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 157326
    iput-boolean p1, p0, Lcom/facebook/debug/feed/DebugFeedConfig;->b:Z

    .line 157327
    return-void
.end method
