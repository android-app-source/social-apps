.class public final Lcom/facebook/debug/mainlooper/MainLooperLogMessagesDispatcher$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0eM;


# direct methods
.method public constructor <init>(LX/0eM;)V
    .locals 0

    .prologue
    .line 91762
    iput-object p1, p0, Lcom/facebook/debug/mainlooper/MainLooperLogMessagesDispatcher$2;->a:LX/0eM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 91763
    iget-object v0, p0, Lcom/facebook/debug/mainlooper/MainLooperLogMessagesDispatcher$2;->a:LX/0eM;

    iget-object v1, v0, LX/0eM;->d:Ljava/util/List;

    monitor-enter v1

    .line 91764
    :try_start_0
    iget-object v0, p0, Lcom/facebook/debug/mainlooper/MainLooperLogMessagesDispatcher$2;->a:LX/0eM;

    iget-object v0, v0, LX/0eM;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 91765
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91766
    if-eqz v0, :cond_0

    .line 91767
    iget-object v0, p0, Lcom/facebook/debug/mainlooper/MainLooperLogMessagesDispatcher$2;->a:LX/0eM;

    iget-object v0, v0, LX/0eM;->c:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Looper;->setMessageLogging(Landroid/util/Printer;)V

    .line 91768
    :goto_0
    return-void

    .line 91769
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 91770
    :cond_0
    iget-object v0, p0, Lcom/facebook/debug/mainlooper/MainLooperLogMessagesDispatcher$2;->a:LX/0eM;

    iget-object v0, v0, LX/0eM;->c:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/debug/mainlooper/MainLooperLogMessagesDispatcher$2;->a:LX/0eM;

    iget-object v1, v1, LX/0eM;->a:Landroid/util/Printer;

    invoke-virtual {v0, v1}, Landroid/os/Looper;->setMessageLogging(Landroid/util/Printer;)V

    goto :goto_0
.end method
