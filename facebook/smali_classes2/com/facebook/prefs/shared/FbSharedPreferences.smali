.class public interface abstract Lcom/facebook/prefs/shared/FbSharedPreferences;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# virtual methods
.method public abstract a(LX/0Tn;D)D
.end method

.method public abstract a(LX/0Tn;F)F
.end method

.method public abstract a(LX/0Tn;I)I
.end method

.method public abstract a(LX/0Tn;J)J
.end method

.method public abstract a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(LX/0Tn;LX/0dN;)V
.end method

.method public abstract a(Ljava/lang/Runnable;)V
.end method

.method public abstract a(Ljava/lang/String;LX/0dN;)V
.end method

.method public abstract a(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/Set;LX/0dN;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;",
            "LX/0dN;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a()Z
.end method

.method public abstract a(LX/0Tn;)Z
.end method

.method public abstract a(LX/0Tn;Z)Z
.end method

.method public abstract b(LX/0Tn;)LX/03R;
.end method

.method public abstract b()V
.end method

.method public abstract b(LX/0Tn;LX/0dN;)V
.end method

.method public abstract b(Ljava/util/Set;LX/0dN;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;",
            "LX/0dN;",
            ")V"
        }
    .end annotation
.end method

.method public abstract c(LX/0Tn;)Ljava/lang/Object;
.end method

.method public abstract c()V
.end method

.method public abstract c(LX/0Tn;LX/0dN;)V
.end method

.method public abstract d(LX/0Tn;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end method

.method public abstract e(LX/0Tn;)Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            ")",
            "Ljava/util/SortedMap",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract edit()LX/0hN;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method
