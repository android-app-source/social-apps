.class public Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Landroid/widget/ToggleButton;

.field public f:LX/124;

.field public g:LX/0hs;

.field public h:Z

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 174032
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 174033
    invoke-direct {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->j()V

    .line 174034
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 174035
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 174036
    invoke-direct {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->j()V

    .line 174037
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 174038
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 174039
    invoke-direct {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->j()V

    .line 174040
    return-void
.end method

.method private a(LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174041
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    if-nez v0, :cond_0

    .line 174042
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    .line 174043
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    const/4 v1, -0x1

    .line 174044
    iput v1, v0, LX/0hs;->t:I

    .line 174045
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 174046
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    const/16 v1, 0x14

    .line 174047
    iput v1, v0, LX/0hs;->r:I

    .line 174048
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    new-instance v1, LX/Bhk;

    invoke-direct {v1, p0, p1}, LX/Bhk;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;LX/0Ot;)V

    .line 174049
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 174050
    :cond_0
    return-void
.end method

.method private a(LX/0hs;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 174051
    new-instance v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$7;

    invoke-direct {v0, p0, p2, p1}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$7;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;Landroid/view/View;LX/0hs;)V

    .line 174052
    const-wide/16 v2, 0x3e8

    invoke-virtual {p2, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 174053
    return-void
.end method

.method private a(LX/0hs;Landroid/view/View;LX/0Ot;LX/0Ot;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hs;",
            "Landroid/view/View;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174054
    new-instance v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;Landroid/view/View;LX/0hs;LX/0Ot;LX/0Ot;)V

    .line 174055
    const-wide/16 v2, 0x3e8

    invoke-virtual {p2, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 174056
    return-void
.end method

.method private static a(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;LX/0W3;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174057
    iput-object p1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a:LX/0W3;

    iput-object p2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->b:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-static {v1}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    const/16 v2, 0x4e0

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;LX/0W3;LX/0Ot;)V

    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 174058
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020d7e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 174059
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getLineHeight()I

    move-result v1

    .line 174060
    invoke-virtual {v0, v3, v3, v1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 174061
    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 174062
    const-string v2, "  "

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 174063
    const-string v2, " "

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 174064
    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-direct {v2, v0, v3}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 174065
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 174066
    return-void
.end method

.method private j()V
    .locals 6

    .prologue
    const/16 v5, 0xf

    const/4 v4, 0x0

    .line 174067
    const-class v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-static {v0, p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 174068
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a:LX/0W3;

    sget-wide v2, LX/0X5;->bP:J

    const/16 v1, 0x14

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->i:I

    .line 174069
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a:LX/0W3;

    sget-wide v2, LX/0X5;->bQ:J

    const/16 v1, 0x23

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->j:I

    .line 174070
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a:LX/0W3;

    sget-wide v2, LX/0X5;->bR:J

    invoke-interface {v0, v2, v3, v5}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->k:I

    .line 174071
    const v0, 0x7f030414

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 174072
    const v0, 0x7f0d0be5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 174073
    const v0, 0x7f0d0764

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    .line 174074
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08062c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->c:Ljava/lang/String;

    .line 174075
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    new-instance v1, LX/Bhh;

    invoke-direct {v1, p0}, LX/Bhh;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174076
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/Bhi;

    invoke-direct {v1, p0}, LX/Bhi;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174077
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203c8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 174078
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setGravity(I)V

    .line 174079
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 174080
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v0, v4, v0, v4}, Landroid/widget/ToggleButton;->setPadding(IIII)V

    .line 174081
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b02db

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 174082
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 174083
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 174084
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v0}, Landroid/widget/ToggleButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 174085
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08063a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setTextOn(Ljava/lang/CharSequence;)V

    .line 174086
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174087
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08063c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setTextOff(Ljava/lang/CharSequence;)V

    .line 174088
    :goto_0
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 174089
    return-void

    .line 174090
    :cond_0
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08063b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setTextOff(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 174021
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    if-nez v0, :cond_0

    .line 174022
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    .line 174023
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    const/4 v1, -0x1

    .line 174024
    iput v1, v0, LX/0hs;->t:I

    .line 174025
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 174026
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    new-instance v1, LX/Bhj;

    invoke-direct {v1, p0}, LX/Bhj;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;)V

    .line 174027
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 174028
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 174029
    new-instance v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$1;

    invoke-direct {v0, p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$1;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;)V

    .line 174030
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v0, p1, p2}, Landroid/widget/ToggleButton;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 174031
    return-void
.end method

.method public final a(LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173955
    invoke-direct {p0, p1}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a(LX/0Ot;)V

    .line 173956
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a(LX/0hs;Landroid/view/View;LX/0Ot;LX/0Ot;)V

    .line 173957
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0Ot;LX/0Ot;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 173960
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    if-nez v0, :cond_0

    .line 173961
    invoke-direct {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->k()V

    .line 173962
    :cond_0
    new-instance v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;Ljava/lang/String;Ljava/lang/String;LX/0Ot;LX/0Ot;)V

    .line 173963
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    int-to-long v2, p5

    invoke-virtual {v1, v0, v2, v3}, Landroid/widget/ToggleButton;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 173964
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 173965
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 173966
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->setVisibility(I)V

    .line 173967
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 173968
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->setVisibility(I)V

    .line 173969
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 173970
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08061f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173971
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->j:I

    if-le v1, v2, :cond_4

    .line 173972
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080621

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 173973
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08063a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 173974
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 173975
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08065c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 173976
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08065e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173977
    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setTextOn(Ljava/lang/CharSequence;)V

    .line 173978
    :goto_1
    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 173979
    iget-object v3, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    iget v4, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->i:I

    if-le v2, v4, :cond_0

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b004e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    :goto_2
    invoke-virtual {v3, v5, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 173980
    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 173981
    invoke-direct {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->i()V

    .line 173982
    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget v3, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->k:I

    if-le v1, v3, :cond_1

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b004b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    :goto_3
    invoke-virtual {v2, v5, v1}, Landroid/widget/ToggleButton;->setTextSize(IF)V

    .line 173983
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->k:I

    if-le v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 173984
    :goto_4
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v0, v5, v0, v5}, Landroid/widget/ToggleButton;->setPadding(IIII)V

    .line 173985
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 173986
    const v0, 0x7f0a02d3

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->setBackgroundResource(I)V

    .line 173987
    return-void

    .line 173988
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b0050

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    goto :goto_2

    .line 173989
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b004e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    goto :goto_3

    .line 173990
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_4

    :cond_3
    move-object v0, v2

    goto/16 :goto_1

    :cond_4
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final e()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 173991
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08061e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173992
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->j:I

    if-le v1, v2, :cond_4

    .line 173993
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080620

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 173994
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08063b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 173995
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 173996
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08065b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 173997
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08065d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173998
    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setTextOff(Ljava/lang/CharSequence;)V

    .line 173999
    :goto_1
    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 174000
    iget-object v3, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    iget v4, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->i:I

    if-le v2, v4, :cond_0

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b004e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    :goto_2
    invoke-virtual {v3, v5, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 174001
    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 174002
    invoke-direct {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->i()V

    .line 174003
    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget v3, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->k:I

    if-le v1, v3, :cond_1

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b004b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    :goto_3
    invoke-virtual {v2, v5, v1}, Landroid/widget/ToggleButton;->setTextSize(IF)V

    .line 174004
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->k:I

    if-le v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 174005
    :goto_4
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v0, v5, v0, v5}, Landroid/widget/ToggleButton;->setPadding(IIII)V

    .line 174006
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v5}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 174007
    const v0, 0x7f0a02cf

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->setBackgroundResource(I)V

    .line 174008
    return-void

    .line 174009
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b0050

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    goto :goto_2

    .line 174010
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b004e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    goto :goto_3

    .line 174011
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_4

    :cond_3
    move-object v0, v2

    goto/16 :goto_1

    :cond_4
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 173958
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setClickable(Z)V

    .line 173959
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 174012
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setClickable(Z)V

    .line 174013
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 174014
    invoke-direct {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->k()V

    .line 174015
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-direct {p0, v0, v1}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a(LX/0hs;Landroid/view/View;)V

    .line 174016
    return-void
.end method

.method public setCarrierName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 174017
    iput-object p1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->c:Ljava/lang/String;

    .line 174018
    return-void
.end method

.method public setOnClickListener(LX/124;)V
    .locals 0

    .prologue
    .line 174019
    iput-object p1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->f:LX/124;

    .line 174020
    return-void
.end method
