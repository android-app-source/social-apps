.class public Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# instance fields
.field public p:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/zero/service/FbZeroRequestHandler;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 175350
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Xl;Lcom/facebook/content/SecureContextHelper;LX/0ad;LX/0Sh;LX/0yc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;",
            "LX/0Zb;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/zero/service/FbZeroRequestHandler;",
            ">;",
            "LX/0Xl;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0ad;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0yc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 175349
    iput-object p1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->p:LX/0Zb;

    iput-object p2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->r:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->s:LX/0Xl;

    iput-object p5, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iput-object p6, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->u:LX/0ad;

    iput-object p7, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->v:LX/0Sh;

    iput-object p8, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->w:LX/0yc;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;

    invoke-static {v8}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {v8}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v3, 0x13f1

    invoke-static {v8, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v8}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {v8}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v8}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v8}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v8}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v8

    check-cast v8, LX/0yc;

    invoke-static/range {v0 .. v8}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->a(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Xl;Lcom/facebook/content/SecureContextHelper;LX/0ad;LX/0Sh;LX/0yc;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 175347
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->v:LX/0Sh;

    new-instance v1, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity$3;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 175348
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 175292
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->u:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175293
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 175294
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    invoke-static {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->l(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;)V

    .line 175295
    :goto_0
    return-void

    .line 175296
    :cond_0
    const v0, 0x7f0d0c8e

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 175297
    const v1, 0x7f0d0c8f

    invoke-virtual {p0, v1}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 175298
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 175299
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 175300
    new-instance v2, LX/Bhn;

    invoke-direct {v2, p0, v0, v1}, LX/Bhn;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;Landroid/view/View;Landroid/view/View;)V

    .line 175301
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/service/FbZeroRequestHandler;

    invoke-virtual {v0, v2}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public static b(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 175341
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 175342
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 175343
    move-object v0, v0

    .line 175344
    const-string v1, "carrier_id"

    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 175345
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->p:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 175346
    return-void
.end method

.method public static l(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;)V
    .locals 5

    .prologue
    .line 175326
    const v0, 0x7f0d0c92

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 175327
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 175328
    new-instance v2, LX/47x;

    invoke-direct {v2, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 175329
    const v3, 0x7f080638

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 175330
    const-string v3, " "

    invoke-virtual {v2, v3}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 175331
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const v4, 0x7f0a00d1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 175332
    const/16 v4, 0x21

    invoke-virtual {v2, v3, v4}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    .line 175333
    const v3, 0x7f080639

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 175334
    invoke-virtual {v2}, LX/47x;->a()LX/47x;

    .line 175335
    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    .line 175336
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 175337
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 175338
    new-instance v1, LX/Bho;

    invoke-direct {v1, p0}, LX/Bho;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175339
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 175340
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175325
    const-string v0, "dialtone_switcher_nux_interstitial"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 175312
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 175313
    invoke-static {p0, p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 175314
    const v0, 0x7f030417

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->setContentView(I)V

    .line 175315
    const v0, 0x7f0d0c8d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 175316
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08062f

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Facebook Flex"

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 175317
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->w:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175318
    const v0, 0x7f0d0c90

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 175319
    invoke-virtual {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080630

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 175320
    :cond_0
    const v0, 0x7f0d0c91

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 175321
    new-instance v1, LX/Bhm;

    invoke-direct {v1, p0}, LX/Bhm;-><init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175322
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->u:LX/0ad;

    sget-short v1, LX/Bhp;->b:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 175323
    invoke-direct {p0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->b()V

    .line 175324
    :cond_1
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 175309
    const-string v0, "dialtone_switcher_nux_interstitial_back_pressed"

    invoke-static {p0, v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->b(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;Ljava/lang/String;)V

    .line 175310
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 175311
    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, -0x55236781

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 175302
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 175303
    const-string v1, "dialtone_switcher_nux_interstitial_impression"

    invoke-static {p0, v1}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->b(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;Ljava/lang/String;)V

    .line 175304
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0dQ;->C:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 175305
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ACTION_ZERO_UPDATE_STATUS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 175306
    const-string v2, "zero_status_to_update"

    const-string v3, "dialtone_nux_impression"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175307
    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->s:LX/0Xl;

    invoke-interface {v2, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 175308
    const/16 v1, 0x23

    const v2, -0x2bb4cff8

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
