.class public Lcom/facebook/contacts/provider/ContactsConnectionsProvider;
.super LX/0Ov;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Landroid/content/UriMatcher;


# instance fields
.field private b:LX/3fr;

.field private c:LX/3fh;

.field private d:LX/2Rd;

.field private e:LX/2RQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 54698
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->a:Landroid/content/UriMatcher;

    .line 54699
    invoke-static {}, LX/0Oz;->values()[LX/0Oz;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 54700
    sget-object v4, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->a:Landroid/content/UriMatcher;

    sget-object v5, LX/0P0;->a:Ljava/lang/String;

    invoke-virtual {v3}, LX/0Oz;->getMatcherPartialUri()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, LX/0Oz;->uriMatcherIndex()I

    move-result v3

    invoke-virtual {v4, v5, v6, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 54701
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54702
    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54763
    invoke-direct {p0}, LX/0Ov;-><init>()V

    return-void
.end method

.method private a(LX/0Px;Ljava/lang/String;)LX/6N1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/6N1;"
        }
    .end annotation

    .prologue
    .line 54759
    iget-object v0, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->b:LX/3fr;

    iget-object v1, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->e:LX/2RQ;

    invoke-virtual {v1, p2}, LX/2RQ;->a(Ljava/lang/String;)LX/2RR;

    move-result-object v1

    .line 54760
    iput-object p1, v1, LX/2RR;->c:Ljava/util/Collection;

    .line 54761
    move-object v1, v1

    .line 54762
    invoke-virtual {v0, v1}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/3Oq;)LX/6N1;
    .locals 3

    .prologue
    .line 54752
    iget-object v0, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->b:LX/3fr;

    iget-object v1, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->e:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 54753
    iput-object v2, v1, LX/2RR;->c:Ljava/util/Collection;

    .line 54754
    move-object v1, v1

    .line 54755
    sget-object v2, LX/2RS;->NAME:LX/2RS;

    .line 54756
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 54757
    move-object v1, v1

    .line 54758
    invoke-virtual {v0, v1}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/0Px;Ljava/lang/String;)LX/6N1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/6N1;"
        }
    .end annotation

    .prologue
    .line 54743
    iget-object v0, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->b:LX/3fr;

    iget-object v1, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->e:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    .line 54744
    iput-object p2, v1, LX/2RR;->e:Ljava/lang/String;

    .line 54745
    move-object v1, v1

    .line 54746
    iput-object p1, v1, LX/2RR;->c:Ljava/util/Collection;

    .line 54747
    move-object v1, v1

    .line 54748
    sget-object v2, LX/2RS;->NAME:LX/2RS;

    .line 54749
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 54750
    move-object v1, v1

    .line 54751
    invoke-virtual {v0, v1}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 54742
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 54741
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 54715
    invoke-virtual {p0}, LX/0On;->c()V

    .line 54716
    sget-object v0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 54717
    sget-object v1, LX/0Oz;->CONTACTS_CONTENT:LX/0Oz;

    invoke-virtual {v1}, LX/0Oz;->uriMatcherIndex()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 54718
    iget-object v0, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->b:LX/3fr;

    iget-object v1, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->e:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    sget-object v2, LX/3Oq;->CONNECTIONS:LX/0Px;

    .line 54719
    iput-object v2, v1, LX/2RR;->c:Ljava/util/Collection;

    .line 54720
    move-object v1, v1

    .line 54721
    invoke-virtual {v0, v1}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v0

    .line 54722
    :goto_0
    instance-of v1, v0, LX/6N2;

    const-string v2, "ContactsConnectionProvider only supports ContactDatabaseCursorIterator."

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 54723
    new-instance v1, LX/6O4;

    check-cast v0, LX/6N2;

    .line 54724
    iget-object v2, v0, LX/6N2;->c:Landroid/database/Cursor;

    move-object v0, v2

    .line 54725
    iget-object v2, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->c:LX/3fh;

    iget-object v3, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->d:LX/2Rd;

    invoke-direct {v1, v0, v2, v3}, LX/6O4;-><init>(Landroid/database/Cursor;LX/3fh;LX/2Rd;)V

    return-object v1

    .line 54726
    :cond_0
    sget-object v1, LX/0Oz;->CONTACT_ID:LX/0Oz;

    invoke-virtual {v1}, LX/0Oz;->uriMatcherIndex()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 54727
    sget-object v1, LX/3Oq;->CONNECTIONS:LX/0Px;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->a(LX/0Px;Ljava/lang/String;)LX/6N1;

    move-result-object v0

    goto :goto_0

    .line 54728
    :cond_1
    sget-object v1, LX/0Oz;->FRIENDS_CONTENT:LX/0Oz;

    invoke-virtual {v1}, LX/0Oz;->uriMatcherIndex()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 54729
    sget-object v0, LX/3Oq;->FRIEND:LX/3Oq;

    invoke-direct {p0, v0}, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->a(LX/3Oq;)LX/6N1;

    move-result-object v0

    goto :goto_0

    .line 54730
    :cond_2
    sget-object v1, LX/0Oz;->FRIEND_UID:LX/0Oz;

    invoke-virtual {v1}, LX/0Oz;->uriMatcherIndex()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 54731
    sget-object v1, LX/3Oq;->FRIENDS:LX/0Px;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->a(LX/0Px;Ljava/lang/String;)LX/6N1;

    move-result-object v0

    goto :goto_0

    .line 54732
    :cond_3
    sget-object v1, LX/0Oz;->FRIENDS_PREFIX_SEARCH:LX/0Oz;

    invoke-virtual {v1}, LX/0Oz;->uriMatcherIndex()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 54733
    sget-object v1, LX/3Oq;->FRIENDS:LX/0Px;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->b(LX/0Px;Ljava/lang/String;)LX/6N1;

    move-result-object v0

    goto :goto_0

    .line 54734
    :cond_4
    sget-object v1, LX/0Oz;->PAGES_CONTENT:LX/0Oz;

    invoke-virtual {v1}, LX/0Oz;->uriMatcherIndex()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 54735
    sget-object v0, LX/3Oq;->PAGE:LX/3Oq;

    invoke-direct {p0, v0}, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->a(LX/3Oq;)LX/6N1;

    move-result-object v0

    goto :goto_0

    .line 54736
    :cond_5
    sget-object v1, LX/0Oz;->PAGE_ID:LX/0Oz;

    invoke-virtual {v1}, LX/0Oz;->uriMatcherIndex()I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 54737
    sget-object v1, LX/3Oq;->PAGES:LX/0Px;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->a(LX/0Px;Ljava/lang/String;)LX/6N1;

    move-result-object v0

    goto/16 :goto_0

    .line 54738
    :cond_6
    sget-object v1, LX/0Oz;->PAGES_SEARCH:LX/0Oz;

    invoke-virtual {v1}, LX/0Oz;->uriMatcherIndex()I

    move-result v1

    if-ne v0, v1, :cond_7

    .line 54739
    sget-object v1, LX/3Oq;->PAGES:LX/0Px;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->b(LX/0Px;Ljava/lang/String;)LX/6N1;

    move-result-object v0

    goto/16 :goto_0

    .line 54740
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 54714
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 54710
    sget-object v0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 54711
    if-lez v0, :cond_0

    .line 54712
    const-string v0, "vnd.android.cursor.item/vnd.com.facebook.katana.provider.contacts"

    return-object v0

    .line 54713
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 54703
    invoke-super {p0}, LX/0Ov;->a()V

    .line 54704
    invoke-virtual {p0}, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 54705
    invoke-static {v1}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v0

    check-cast v0, LX/3fr;

    iput-object v0, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->b:LX/3fr;

    .line 54706
    invoke-static {v1}, LX/3fh;->a(LX/0QB;)LX/3fh;

    move-result-object v0

    check-cast v0, LX/3fh;

    iput-object v0, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->c:LX/3fh;

    .line 54707
    invoke-static {v1}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v0

    check-cast v0, LX/2Rd;

    iput-object v0, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->d:LX/2Rd;

    .line 54708
    invoke-static {v1}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v0

    check-cast v0, LX/2RQ;

    iput-object v0, p0, Lcom/facebook/contacts/provider/ContactsConnectionsProvider;->e:LX/2RQ;

    .line 54709
    return-void
.end method
