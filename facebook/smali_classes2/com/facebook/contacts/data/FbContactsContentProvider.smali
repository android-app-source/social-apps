.class public Lcom/facebook/contacts/data/FbContactsContentProvider;
.super LX/0Ov;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/0PH;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public f:LX/2Iv;

.field public g:LX/2Rd;

.field public h:LX/2RE;

.field public i:LX/2Re;

.field private j:LX/0PL;

.field private k:LX/2Rf;

.field private l:LX/2Rh;

.field private m:LX/2Ri;

.field private n:LX/2Rj;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 55525
    const-class v0, Lcom/facebook/contacts/data/FbContactsContentProvider;

    sput-object v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->a:Ljava/lang/Class;

    .line 55526
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "fbid"

    const-string v2, "fbid"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "link_type"

    const-string v2, "link_type"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "communication_rank"

    const-string v2, "communication_rank"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "with_tagging_rank"

    const-string v2, "with_tagging_rank"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "is_messenger_user"

    const-string v2, "is_messenger_user"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "is_on_viewer_contact_list"

    const-string v2, "is_on_viewer_contact_list"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "viewer_connection_status"

    const-string v2, "viewer_connection_status"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "sort_name_key"

    const-string v2, "sort_name_key"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "phonebook_section_key"

    const-string v2, "phonebook_section_key"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "data"

    const-string v2, "data"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "first_name"

    const-string v2, "first_name"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "last_name"

    const-string v2, "last_name"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "display_name"

    const-string v2, "display_name"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "small_picture_url"

    const-string v2, "small_picture_url"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "big_picture_url"

    const-string v2, "big_picture_url"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "huge_picture_url"

    const-string v2, "huge_picture_url"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "small_picture_size"

    const-string v2, "small_picture_size"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "big_picture_size"

    const-string v2, "big_picture_size"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "huge_picture_size"

    const-string v2, "huge_picture_size"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "is_mobile_pushable"

    const-string v2, "is_mobile_pushable"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "messenger_install_time_ms"

    const-string v2, "messenger_install_time_ms"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "added_time_ms"

    const-string v2, "added_time_ms"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "last_fetch_time_ms"

    const-string v2, "last_fetch_time_ms"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "is_indexed"

    const-string v2, "is_indexed"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "bday_month"

    const-string v2, "bday_month"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "bday_day"

    const-string v2, "bday_day"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "is_partial"

    const-string v2, "is_partial"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "messenger_invite_priority"

    const-string v2, "messenger_invite_priority"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "is_memorialized"

    const-string v2, "is_memorialized"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->b:LX/0P1;

    .line 55527
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "fbid"

    const-string v2, "fbid"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "display_order"

    const-string v2, "display_order"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->c:LX/0P1;

    .line 55528
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "raw_phone_number"

    const-string v2, "raw_phone_number"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "display_order"

    const-string v2, "display_order"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->d:LX/0P1;

    .line 55529
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, LX/0PH;->PHONE_LOCAL:LX/0PH;

    const-string v2, "phone_local"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/0PH;->PHONE_NATIONAL:LX/0PH;

    const-string v2, "phone_national"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/0PH;->PHONE_E164:LX/0PH;

    const-string v2, "phone_e164"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/0PH;->PHONE_VERIFIED:LX/0PH;

    const-string v2, "phone_verified"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->e:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55604
    invoke-direct {p0}, LX/0Ov;-><init>()V

    .line 55605
    return-void
.end method

.method public static synthetic a(Lcom/facebook/contacts/data/FbContactsContentProvider;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55603
    invoke-static {p1}, Lcom/facebook/contacts/data/FbContactsContentProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/contacts/data/FbContactsContentProvider;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55602
    invoke-static {p1, p2}, Lcom/facebook/contacts/data/FbContactsContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 55598
    invoke-static {p0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55599
    sget-object v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 55600
    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55601
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 55593
    if-eqz p1, :cond_0

    const-string v0, "_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55594
    :cond_0
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55595
    const-string p0, "is_indexed = 1"

    .line 55596
    :cond_1
    :goto_0
    return-object p0

    .line 55597
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND is_indexed = 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 55585
    if-eqz p0, :cond_0

    sget-object v0, LX/2RK;->b:LX/0Rf;

    invoke-virtual {v0, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 55586
    if-eqz p0, :cond_1

    const-string v0, "_id"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 55587
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS c "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55588
    :goto_1
    return-object v0

    .line 55589
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 55590
    :cond_3
    sget-object v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->b:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 55591
    const-string v1, ", idx.indexed_data AS "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55592
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AS c INNER JOIN contacts_indexed_data AS idx ON (c.internal_id = idx.contact_internal_id AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "idx.type = \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\') "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static final a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 55571
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 55572
    invoke-static {v1, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 55573
    invoke-static {p2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55574
    sget-object v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 55575
    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 55576
    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 55577
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 55578
    const-string v0, "c.internal_id AS _id"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55579
    invoke-static {p3, v2, p0}, Lcom/facebook/contacts/data/FbContactsContentProvider;->a(Ljava/lang/String;Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 55580
    if-eqz p3, :cond_2

    .line 55581
    invoke-virtual {v1, p3}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    .line 55582
    :cond_2
    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 55583
    invoke-static {v0, v2}, Lcom/facebook/contacts/data/FbContactsContentProvider;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    goto :goto_1

    .line 55584
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(SELECT "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 55566
    const-string v0, "_id"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 55567
    const-string v0, "data"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "is_indexed"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/2RK;->a:LX/0Rf;

    invoke-virtual {v0, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55568
    :cond_0
    const-string v0, ", c."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55569
    :cond_1
    return-void

    .line 55570
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown field: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 55565
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 55561
    iget-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->h:LX/2RE;

    iget-object v0, v0, LX/2RE;->g:LX/2RP;

    iget-object v0, v0, LX/2RP;->a:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55562
    iget-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->f:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 55563
    const/4 v0, 0x0

    return v0

    .line 55564
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 55554
    const-string v0, "FbContactsContentProvider.doQuery"

    const v1, -0x4ac69518

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 55555
    :try_start_0
    iget-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->j:LX/0PL;

    invoke-virtual {v0, p1}, LX/0PL;->a(Landroid/net/Uri;)LX/2Rg;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 55556
    invoke-virtual/range {v0 .. v5}, LX/2Rg;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 55557
    const v1, 0xfeaecb9

    invoke-static {v1}, LX/02m;->b(I)J

    .line 55558
    return-object v0

    .line 55559
    :catchall_0
    move-exception v0

    const v1, -0x220faa5b

    invoke-static {v1}, LX/02m;->b(I)J

    .line 55560
    throw v0
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 55553
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55552
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 55530
    monitor-enter p0

    :try_start_0
    const-string v0, "ContactsContentProvider.onInitialize"

    const v1, 0x646c805a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 55531
    :try_start_1
    invoke-virtual {p0}, Lcom/facebook/contacts/data/FbContactsContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 55532
    invoke-static {v1}, LX/2Iv;->a(LX/0QB;)LX/2Iv;

    move-result-object v0

    check-cast v0, LX/2Iv;

    iput-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->f:LX/2Iv;

    .line 55533
    invoke-static {v1}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v0

    check-cast v0, LX/2Rd;

    iput-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->g:LX/2Rd;

    .line 55534
    invoke-static {v1}, LX/2RE;->a(LX/0QB;)LX/2RE;

    move-result-object v0

    check-cast v0, LX/2RE;

    iput-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->h:LX/2RE;

    .line 55535
    invoke-static {v1}, LX/2Re;->a(LX/0QB;)LX/2Re;

    move-result-object v0

    check-cast v0, LX/2Re;

    iput-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->i:LX/2Re;

    .line 55536
    new-instance v0, LX/2Rf;

    invoke-direct {v0, p0}, LX/2Rf;-><init>(Lcom/facebook/contacts/data/FbContactsContentProvider;)V

    iput-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->k:LX/2Rf;

    .line 55537
    new-instance v0, LX/2Rh;

    invoke-direct {v0, p0}, LX/2Rh;-><init>(Lcom/facebook/contacts/data/FbContactsContentProvider;)V

    iput-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->l:LX/2Rh;

    .line 55538
    new-instance v0, LX/2Ri;

    invoke-direct {v0, p0}, LX/2Ri;-><init>(Lcom/facebook/contacts/data/FbContactsContentProvider;)V

    iput-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->m:LX/2Ri;

    .line 55539
    new-instance v0, LX/2Rj;

    invoke-direct {v0, p0}, LX/2Rj;-><init>(Lcom/facebook/contacts/data/FbContactsContentProvider;)V

    iput-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->n:LX/2Rj;

    .line 55540
    new-instance v0, LX/0PL;

    invoke-direct {v0}, LX/0PL;-><init>()V

    iput-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->j:LX/0PL;

    .line 55541
    iget-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->j:LX/0PL;

    iget-object v1, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->h:LX/2RE;

    iget-object v1, v1, LX/2RE;->a:Ljava/lang/String;

    const-string v2, "contacts_with_fbids"

    iget-object v3, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->k:LX/2Rf;

    invoke-virtual {v0, v1, v2, v3}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V

    .line 55542
    iget-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->j:LX/0PL;

    iget-object v1, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->h:LX/2RE;

    iget-object v1, v1, LX/2RE;->a:Ljava/lang/String;

    const-string v2, "favorites"

    iget-object v3, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->l:LX/2Rh;

    invoke-virtual {v0, v1, v2, v3}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V

    .line 55543
    iget-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->j:LX/0PL;

    iget-object v1, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->h:LX/2RE;

    iget-object v1, v1, LX/2RE;->a:Ljava/lang/String;

    const-string v2, "sms_favorites"

    iget-object v3, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->m:LX/2Ri;

    invoke-virtual {v0, v1, v2, v3}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V

    .line 55544
    iget-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->j:LX/0PL;

    iget-object v1, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->h:LX/2RE;

    iget-object v1, v1, LX/2RE;->a:Ljava/lang/String;

    sget-object v2, LX/2RV;->CONTACT:LX/2RV;

    iget-object v2, v2, LX/2RV;->searchTableContentPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->n:LX/2Rj;

    invoke-virtual {v0, v1, v2, v3}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V

    .line 55545
    iget-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->j:LX/0PL;

    iget-object v1, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->h:LX/2RE;

    iget-object v1, v1, LX/2RE;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/2RV;->CONTACT:LX/2RV;

    iget-object v3, v3, LX/2RV;->searchTableContentPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->n:LX/2Rj;

    invoke-virtual {v0, v1, v2, v3}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V

    .line 55546
    iget-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->j:LX/0PL;

    iget-object v1, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->h:LX/2RE;

    iget-object v1, v1, LX/2RE;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/2RV;->CONTACT:LX/2RV;

    iget-object v3, v3, LX/2RV;->searchTableContentPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->n:LX/2Rj;

    invoke-virtual {v0, v1, v2, v3}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V

    .line 55547
    iget-object v0, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->j:LX/0PL;

    iget-object v1, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->h:LX/2RE;

    iget-object v1, v1, LX/2RE;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/2RV;->CONTACT:LX/2RV;

    iget-object v3, v3, LX/2RV;->searchTableContentPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/*/*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/contacts/data/FbContactsContentProvider;->n:LX/2Rj;

    invoke-virtual {v0, v1, v2, v3}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55548
    const v0, -0x686cead9

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 55549
    monitor-exit p0

    return-void

    .line 55550
    :catchall_0
    move-exception v0

    const v1, -0xd50a452

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 55551
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
