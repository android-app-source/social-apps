.class public Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:Landroid/graphics/drawable/Drawable;

.field private b:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 339372
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 339373
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 339374
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 339375
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 339376
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 339377
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0215f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->a:Landroid/graphics/drawable/Drawable;

    .line 339378
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->a:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 339379
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 339380
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 339381
    iget v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->b:F

    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 339382
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 339383
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    .line 339384
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 339385
    int-to-float v2, v1

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->b:F

    add-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 339386
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 339387
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 339388
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 339389
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 339390
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->getMeasuredWidth()I

    move-result v0

    .line 339391
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    mul-int/lit8 v1, v1, 0xa

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x41300000    # 11.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->b:F

    .line 339392
    return-void
.end method
