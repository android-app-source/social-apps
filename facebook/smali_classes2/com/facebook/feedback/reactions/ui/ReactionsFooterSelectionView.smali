.class public Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;

.field private final b:Landroid/view/View;

.field public c:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 339370
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 339371
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 339368
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 339369
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 339362
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 339363
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 339364
    const v1, 0x7f031186

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 339365
    const v0, 0x7f0d293b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->a:Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;

    .line 339366
    const v0, 0x7f0d293c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->b:Landroid/view/View;

    .line 339367
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 339357
    iget-object v3, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->a:Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/feedback/reactions/ui/ReactionsScrubberView;->setVisibility(I)V

    .line 339358
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->b:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 339359
    return-void

    :cond_0
    move v0, v2

    .line 339360
    goto :goto_0

    :cond_1
    move v1, v2

    .line 339361
    goto :goto_1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    const v0, -0x606c3f16

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 339356
    const v1, 0x42706d71

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v3
.end method

.method public final onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 339351
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 339352
    if-ne p1, p0, :cond_0

    .line 339353
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->c:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-static {p1, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 339354
    :cond_0
    return-void

    .line 339355
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 339349
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->c:Landroid/graphics/drawable/Drawable;

    .line 339350
    return-void
.end method
