.class public Lcom/facebook/resources/ui/FbEditText;
.super LX/0zk;
.source ""


# instance fields
.field public a:LX/0jk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 167839
    invoke-direct {p0, p1}, LX/0zk;-><init>(Landroid/content/Context;)V

    .line 167840
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 167841
    invoke-direct {p0, p1, p2}, LX/0zk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 167842
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbEditText;->a()V

    .line 167843
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 167844
    invoke-direct {p0, p1, p2, p3}, LX/0zk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 167845
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbEditText;->a()V

    .line 167846
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 167847
    const-class v0, Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v0, p0}, Lcom/facebook/resources/ui/FbEditText;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 167848
    iget-object v0, p0, Lcom/facebook/resources/ui/FbEditText;->a:LX/0jk;

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 167849
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/resources/ui/FbEditText;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v0}, LX/0jk;->a(LX/0QB;)LX/0jk;

    move-result-object v0

    check-cast v0, LX/0jk;

    iput-object v0, p0, Lcom/facebook/resources/ui/FbEditText;->a:LX/0jk;

    return-void
.end method
