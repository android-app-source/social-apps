.class public Lcom/facebook/resources/ui/FbTextView;
.super LX/0wN;
.source ""

# interfaces
.implements LX/0wO;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 159864
    invoke-direct {p0, p1}, LX/0wN;-><init>(Landroid/content/Context;)V

    .line 159865
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/resources/ui/FbTextView;->a:Z

    .line 159866
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 159861
    invoke-direct {p0, p1, p2}, LX/0wN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 159862
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/resources/ui/FbTextView;->a:Z

    .line 159863
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 159858
    invoke-direct {p0, p1, p2, p3}, LX/0wN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 159859
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/resources/ui/FbTextView;->a:Z

    .line 159860
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 159853
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 159854
    if-eqz v4, :cond_0

    .line 159855
    invoke-virtual {v4, p1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 159856
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 159857
    :cond_1
    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const/16 v2, 0x2c

    const v3, 0x6df9b60

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 159848
    invoke-super {p0}, LX/0wN;->onAttachedToWindow()V

    .line 159849
    iput-boolean v0, p0, Lcom/facebook/resources/ui/FbTextView;->a:Z

    .line 159850
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->a(Z)V

    .line 159851
    const v0, -0x7a22bc25

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 159852
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2c

    const v1, 0x29805309

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 159844
    invoke-super {p0}, LX/0wN;->onDetachedFromWindow()V

    .line 159845
    iput-boolean v2, p0, Lcom/facebook/resources/ui/FbTextView;->a:Z

    .line 159846
    invoke-direct {p0, v2}, Lcom/facebook/resources/ui/FbTextView;->a(Z)V

    .line 159847
    const/16 v1, 0x2d

    const v2, -0x17ddaf4b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 159832
    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->a(Z)V

    .line 159833
    invoke-super {p0, p1, p2, p3, p4}, LX/0wN;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 159834
    iget-boolean v1, p0, Lcom/facebook/resources/ui/FbTextView;->a:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->a(Z)V

    .line 159835
    return-void
.end method

.method public final setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 159840
    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->a(Z)V

    .line 159841
    invoke-super {p0, p1, p2, p3, p4}, LX/0wN;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 159842
    iget-boolean v1, p0, Lcom/facebook/resources/ui/FbTextView;->a:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->a(Z)V

    .line 159843
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 159836
    invoke-super {p0, p1}, LX/0wN;->setVisibility(I)V

    .line 159837
    iget-boolean v0, p0, Lcom/facebook/resources/ui/FbTextView;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->a(Z)V

    .line 159838
    return-void

    .line 159839
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
