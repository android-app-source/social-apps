.class public final Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/0ep;


# direct methods
.method public constructor <init>(LX/0ep;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 104293
    iput-object p1, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iput-object p2, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 104272
    :try_start_0
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iget-object v0, v0, LX/0ep;->d:LX/0el;

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iget-object v1, v1, LX/0ep;->e:LX/0ed;

    invoke-interface {v0, v1}, LX/0el;->a(LX/0ed;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 104273
    :try_start_1
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iget-object v0, v0, LX/0ep;->d:LX/0el;

    invoke-interface {v0}, LX/0el;->a()V

    .line 104274
    sget-object v0, LX/0hT;->a:[I

    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iget-object v2, v2, LX/0ep;->e:LX/0ed;

    .line 104275
    iget-object v3, v2, LX/0ed;->e:LX/0eh;

    move-object v2, v3

    .line 104276
    invoke-virtual {v2}, LX/0eh;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 104277
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized language pack type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iget-object v3, v3, LX/0ep;->e:LX/0ed;

    .line 104278
    iget-object v4, v3, LX/0ed;->e:LX/0eh;

    move-object v3, v4

    .line 104279
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104280
    :catch_0
    move-exception v0

    .line 104281
    :try_start_2
    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iget-object v2, v2, LX/0ep;->d:LX/0el;

    invoke-interface {v2}, LX/0el;->c()V

    .line 104282
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104283
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 104284
    :catch_1
    move-exception v0

    .line 104285
    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 104286
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iget-object v0, v0, LX/0ep;->d:LX/0el;

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iget-object v1, v1, LX/0ep;->e:LX/0ed;

    invoke-interface {v0, v1}, LX/0el;->b(LX/0ed;)V

    .line 104287
    :goto_0
    return-void

    .line 104288
    :pswitch_0
    :try_start_4
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iget-object v0, v0, LX/0ep;->b:LX/0eq;

    invoke-virtual {v0, v1}, LX/0eq;->a(Ljava/io/InputStream;)LX/0lA;

    move-result-object v0

    .line 104289
    :goto_1
    iget-object v2, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->b:LX/0ep;

    iget-object v2, v2, LX/0ep;->d:LX/0el;

    invoke-interface {v2}, LX/0el;->b()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 104290
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 104291
    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackLoader$1;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, -0x76286421

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 104292
    :pswitch_1
    :try_start_6
    invoke-static {v1}, LX/0er;->a(Ljava/io/InputStream;)LX/0lA;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
