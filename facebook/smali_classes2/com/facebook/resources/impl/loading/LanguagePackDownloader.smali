.class public Lcom/facebook/resources/impl/loading/LanguagePackDownloader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;


# instance fields
.field private final a:Ljava/lang/String;

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AP;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2z1;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/31p;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0em;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Wn;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/316;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 115051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115052
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "i18n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->a:Ljava/lang/String;

    .line 115053
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 115054
    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->b:LX/0Ot;

    .line 115055
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 115056
    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->c:LX/0Ot;

    .line 115057
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 115058
    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->d:LX/0Ot;

    .line 115059
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 115060
    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->f:LX/0Ot;

    .line 115061
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 115062
    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    .line 115063
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 115064
    iput-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->i:LX/0Ot;

    .line 115065
    return-void
.end method

.method public static a(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;Landroid/content/Context;LX/0ff;Lcom/facebook/resources/impl/loading/LanguagePackInfo;)LX/2zH;
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 115066
    iget-object v6, p3, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    .line 115067
    iget-object v7, p3, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadChecksum:Ljava/lang/String;

    .line 115068
    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->g:LX/0em;

    iget-object v3, p3, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    iget v0, p3, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    int-to-long v4, v0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, LX/0em;->c(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 115069
    const/4 v0, 0x0

    .line 115070
    if-eqz p2, :cond_3

    .line 115071
    invoke-virtual {p2}, LX/0ff;->f()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 115072
    :goto_0
    if-nez v1, :cond_0

    .line 115073
    iget-object v0, p3, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    new-instance v1, LX/34W;

    invoke-direct {v1, p0, v7, v2}, LX/34W;-><init>(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;Ljava/lang/String;Ljava/io/File;)V

    invoke-static {v0, v1}, LX/2zH;->a(Ljava/lang/String;LX/1uy;)LX/2zH;

    move-result-object v0

    .line 115074
    :goto_1
    return-object v0

    .line 115075
    :cond_0
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115076
    new-instance v0, LX/2zH;

    invoke-direct {v0}, LX/2zH;-><init>()V

    .line 115077
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/2zH;->a:Z

    .line 115078
    move-object v0, v0

    .line 115079
    goto :goto_1

    .line 115080
    :cond_1
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/316;

    .line 115081
    iget-object v3, v0, LX/316;->b:LX/315;

    invoke-interface {v3, p1, p3, v1}, LX/315;->a(Landroid/content/Context;Lcom/facebook/resources/impl/loading/LanguagePackInfo;Ljava/lang/String;)Z

    move-result v3

    move v0, v3

    .line 115082
    if-eqz v0, :cond_2

    .line 115083
    invoke-static {v1, v6}, LX/0ff;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 115084
    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->g:LX/0em;

    iget-object v3, p3, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    iget v0, p3, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    int-to-long v4, v0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, LX/0em;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 115085
    iget-object v1, p3, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->delta:Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;

    iget-object v1, v1, Lcom/facebook/resources/impl/loading/LanguagePackDeltaInfo;->downloadUrl:Ljava/lang/String;

    new-instance v2, LX/31r;

    invoke-direct {v2, p0, p1, v0}, LX/31r;-><init>(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;Landroid/content/Context;Ljava/io/File;)V

    const/4 v3, 0x1

    .line 115086
    new-instance v0, LX/2zH;

    invoke-direct {v0}, LX/2zH;-><init>()V

    .line 115087
    iput-boolean v3, v0, LX/2zH;->a:Z

    .line 115088
    iput-boolean v3, v0, LX/2zH;->b:Z

    .line 115089
    iput-object v1, v0, LX/2zH;->c:Ljava/lang/String;

    .line 115090
    iput-object v2, v0, LX/2zH;->d:LX/1uy;

    .line 115091
    move-object v0, v0

    .line 115092
    goto :goto_1

    .line 115093
    :cond_2
    iget-object v0, p3, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    new-instance v1, LX/34W;

    invoke-direct {v1, p0, v7, v2}, LX/34W;-><init>(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;Ljava/lang/String;Ljava/io/File;)V

    invoke-static {v0, v1}, LX/2zH;->a(Ljava/lang/String;LX/1uy;)LX/2zH;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/resources/impl/loading/LanguagePackDownloader;
    .locals 11

    .prologue
    .line 115094
    sget-object v0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->j:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    if-nez v0, :cond_1

    .line 115095
    const-class v1, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    monitor-enter v1

    .line 115096
    :try_start_0
    sget-object v0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->j:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 115097
    if-eqz v2, :cond_0

    .line 115098
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 115099
    new-instance v3, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    invoke-direct {v3}, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;-><init>()V

    .line 115100
    const/16 v4, 0x12bc

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x10ce

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xb83

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    const/16 v8, 0x10cc

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0em;->a(LX/0QB;)LX/0em;

    move-result-object v9

    check-cast v9, LX/0em;

    const/16 v10, 0x10c6

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 p0, 0x10d1

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 115101
    iput-object v4, v3, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->b:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->c:LX/0Ot;

    iput-object v6, v3, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->d:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->e:LX/0Sh;

    iput-object v8, v3, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->f:LX/0Ot;

    iput-object v9, v3, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->g:LX/0em;

    iput-object v10, v3, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    iput-object p0, v3, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->i:LX/0Ot;

    .line 115102
    move-object v0, v3

    .line 115103
    sput-object v0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->j:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115104
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 115105
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 115106
    :cond_1
    sget-object v0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->j:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    return-object v0

    .line 115107
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 115108
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0ed;Ljava/io/File;)V
    .locals 5

    .prologue
    .line 115109
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 115110
    :try_start_0
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wn;

    invoke-virtual {v0}, LX/0Wn;->j()V

    .line 115111
    invoke-static {p0, p1}, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->d(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;LX/0ed;)Lcom/facebook/resources/impl/loading/LanguagePackInfo;

    move-result-object v0

    .line 115112
    new-instance v1, LX/34X;

    iget-object v2, v0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, LX/34W;

    iget-object v0, v0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadChecksum:Ljava/lang/String;

    invoke-direct {v3, p0, v0, p2}, LX/34W;-><init>(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {p1}, LX/0ed;->i()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v4

    invoke-direct {v1, v2, v3, v0, v4}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 115113
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AP;

    invoke-virtual {v0, v1}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    .line 115114
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wn;

    invoke-virtual {v0}, LX/0Wn;->k()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115115
    return-void

    .line 115116
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 115117
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->a:Ljava/lang/String;

    const-string v2, "downloadFbstr() failed with exception."

    invoke-static {v0, v2, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 115118
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wn;

    invoke-virtual {v0}, LX/0Wn;->l()V

    .line 115119
    throw v1
.end method

.method private c(LX/0ed;)Ljava/io/File;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 115120
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 115121
    :try_start_0
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->g:LX/0em;

    invoke-virtual {v0, p1}, LX/0em;->c(LX/0ed;)Ljava/io/File;

    move-result-object v0

    .line 115122
    if-eqz v0, :cond_0

    .line 115123
    :goto_0
    return-object v0

    .line 115124
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->d(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;LX/0ed;)Lcom/facebook/resources/impl/loading/LanguagePackInfo;

    move-result-object v0

    .line 115125
    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->g:LX/0em;

    .line 115126
    iget-object v2, p1, LX/0ed;->a:Landroid/content/Context;

    move-object v2, v2

    .line 115127
    iget-object v3, v0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->locale:Ljava/lang/String;

    iget v4, v0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->releaseNumber:I

    int-to-long v4, v4

    iget-object v6, v0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->contentChecksum:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, LX/0em;->c(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 115128
    new-instance v2, LX/34X;

    iget-object v3, v0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    new-instance v4, LX/34W;

    iget-object v0, v0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;->downloadChecksum:Ljava/lang/String;

    invoke-direct {v4, p0, v0, v1}, LX/34W;-><init>(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {p1}, LX/0ed;->i()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v5

    invoke-direct {v2, v3, v4, v0, v5}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 115129
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wn;

    invoke-virtual {v0}, LX/0Wn;->j()V

    .line 115130
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AP;

    invoke-virtual {v0, v2}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    .line 115131
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 115132
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wn;

    invoke-virtual {v0}, LX/0Wn;->k()V

    .line 115133
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->g:LX/0em;

    invoke-virtual {v0, p1}, LX/0em;->c(LX/0ed;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 115134
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 115135
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wn;

    invoke-virtual {v0}, LX/0Wn;->l()V

    .line 115136
    throw v1
.end method

.method public static d(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;LX/0ed;)Lcom/facebook/resources/impl/loading/LanguagePackInfo;
    .locals 4

    .prologue
    .line 115137
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    new-instance v2, LX/2z4;

    invoke-direct {v2, p1}, LX/2z4;-><init>(LX/0ed;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/impl/loading/LanguagePackInfo;

    .line 115138
    return-object v0
.end method


# virtual methods
.method public final a(LX/0ed;)Ljava/io/File;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 115139
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 115140
    iget-object v0, p1, LX/0ed;->e:LX/0eh;

    move-object v0, v0

    .line 115141
    sget-object v1, LX/0h6;->a:[I

    invoke-virtual {v0}, LX/0eh;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 115142
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Request is for unrecognized language file format : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 115143
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->g:LX/0em;

    invoke-virtual {v0, p1}, LX/0em;->a(LX/0ed;)Ljava/io/File;

    move-result-object v0

    .line 115144
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 115145
    invoke-direct {p0, p1, v0}, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->a(LX/0ed;Ljava/io/File;)V

    .line 115146
    :cond_0
    :goto_0
    return-object v0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->c(LX/0ed;)Ljava/io/File;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
