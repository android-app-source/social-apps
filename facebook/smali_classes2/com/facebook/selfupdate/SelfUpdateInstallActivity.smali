.class public Lcom/facebook/selfupdate/SelfUpdateInstallActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private A:Landroid/view/View;

.field public B:Ljava/lang/String;

.field public p:LX/Fjj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Fjr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1sf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0V8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0WV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0en;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0lC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 177308
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 177300
    invoke-virtual {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "no_cancel"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->z:Z

    .line 177301
    iget-boolean v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->z:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->r:LX/1sf;

    invoke-virtual {v0}, LX/1sf;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177302
    :cond_0
    iput-boolean v2, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->z:Z

    .line 177303
    :cond_1
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->A:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 177304
    iget-boolean v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->z:Z

    if-eqz v0, :cond_3

    .line 177305
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->A:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 177306
    :cond_2
    :goto_0
    return-void

    .line 177307
    :cond_3
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 177283
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Fjg;->t:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 177284
    if-eqz v0, :cond_0

    .line 177285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 177286
    const-string v1, "FOR DEBUG ONLY:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177287
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->r:LX/0Tn;

    const-string v3, "fql"

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 177288
    const-string v2, "Source: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177289
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->q:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 177290
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v4, 0x100000

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 177291
    const-string v2, "File Size: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MB \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177292
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->e:LX/0Tn;

    invoke-interface {v1, v2, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 177293
    const-string v2, "URL: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177294
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->h:LX/0Tn;

    invoke-interface {v1, v2, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 177295
    const-string v2, "Local File: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177296
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->p:LX/0Tn;

    invoke-interface {v1, v2, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 177297
    const-string v2, "Mime Type: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177298
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177299
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/selfupdate/SelfUpdateInstallActivity;LX/Fjj;LX/Fjr;LX/1sf;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0V8;Ljava/util/concurrent/ExecutorService;LX/0WV;LX/0en;Lcom/facebook/content/SecureContextHelper;LX/0lC;)V
    .locals 0

    .prologue
    .line 177282
    iput-object p1, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->p:LX/Fjj;

    iput-object p2, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->q:LX/Fjr;

    iput-object p3, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->r:LX/1sf;

    iput-object p4, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p5, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->t:LX/0V8;

    iput-object p6, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->u:Ljava/util/concurrent/ExecutorService;

    iput-object p7, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->v:LX/0WV;

    iput-object p8, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->w:LX/0en;

    iput-object p9, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->x:Lcom/facebook/content/SecureContextHelper;

    iput-object p10, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->y:LX/0lC;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    invoke-static {v10}, LX/Fjj;->b(LX/0QB;)LX/Fjj;

    move-result-object v1

    check-cast v1, LX/Fjj;

    invoke-static {v10}, LX/Fjr;->a(LX/0QB;)LX/Fjr;

    move-result-object v2

    check-cast v2, LX/Fjr;

    invoke-static {v10}, LX/1sf;->b(LX/0QB;)LX/1sf;

    move-result-object v3

    check-cast v3, LX/1sf;

    invoke-static {v10}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v10}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v5

    check-cast v5, LX/0V8;

    invoke-static {v10}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v10}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v7

    check-cast v7, LX/0WV;

    invoke-static {v10}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v8

    check-cast v8, LX/0en;

    invoke-static {v10}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v10}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v10

    check-cast v10, LX/0lC;

    invoke-static/range {v0 .. v10}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->a(Lcom/facebook/selfupdate/SelfUpdateInstallActivity;LX/Fjj;LX/Fjr;LX/1sf;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0V8;Ljava/util/concurrent/ExecutorService;LX/0WV;LX/0en;Lcom/facebook/content/SecureContextHelper;LX/0lC;)V

    return-void
.end method

.method private a(Ljava/lang/String;LX/0m9;)V
    .locals 3
    .param p2    # LX/0m9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 177279
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->q:LX/Fjr;

    invoke-virtual {v0}, LX/Fjr;->c()V

    .line 177280
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->u:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/selfupdate/SelfUpdateInstallActivity$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity$3;-><init>(Lcom/facebook/selfupdate/SelfUpdateInstallActivity;Ljava/lang/String;LX/0m9;)V

    const v2, 0x6ba4029

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 177281
    return-void
.end method

.method private b(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 4

    .prologue
    .line 177271
    const/4 v0, 0x0

    .line 177272
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 177273
    invoke-virtual {v1}, Ljava/net/URI;->isAbsolute()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177274
    invoke-static {v1}, LX/0en;->a(Ljava/net/URI;)Ljava/io/File;

    move-result-object v1

    .line 177275
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 177276
    :goto_1
    return-object v0

    .line 177277
    :cond_0
    invoke-static {p1}, LX/0en;->a(Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 177278
    :catch_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 177199
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->v:LX/0WV;

    invoke-virtual {v0}, LX/0WV;->d()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 177216
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 177217
    invoke-static {p0, p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 177218
    invoke-virtual {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 177219
    const-string v1, "local_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 177220
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->r:LX/0Tn;

    const-string v4, ""

    invoke-interface {v1, v2, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->B:Ljava/lang/String;

    .line 177221
    invoke-static {v3}, LX/Fjr;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 177222
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->p:LX/Fjj;

    const-string v1, "File doesn\'t exist for SelfUpdateInstallActivity"

    invoke-virtual {v0, v1}, LX/Fjj;->a(Ljava/lang/String;)V

    .line 177223
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->y:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v0

    .line 177224
    const-string v1, "local_file"

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 177225
    const-string v1, "invalid_file"

    invoke-direct {p0, v1, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->a(Ljava/lang/String;LX/0m9;)V

    .line 177226
    invoke-virtual {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->finish()V

    .line 177227
    :goto_0
    return-void

    .line 177228
    :cond_0
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->t:LX/0V8;

    sget-object v2, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v1, v2}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v4

    .line 177229
    iget-object v1, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->q:LX/0Tn;

    const-wide/32 v6, 0x1e00000

    invoke-interface {v1, v2, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 177230
    const-wide/16 v8, 0x2

    mul-long/2addr v8, v6

    cmp-long v1, v4, v8

    if-gez v1, :cond_1

    .line 177231
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->p:LX/Fjj;

    const-string v1, "Not enough free space in internal storage for installation"

    invoke-virtual {v0, v1}, LX/Fjj;->a(Ljava/lang/String;)V

    .line 177232
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->y:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v0

    .line 177233
    const-string v1, "free_space"

    invoke-virtual {v0, v1, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 177234
    const-string v1, "file_size"

    invoke-virtual {v0, v1, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 177235
    const-string v1, "not_enough_space"

    invoke-direct {p0, v1, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->a(Ljava/lang/String;LX/0m9;)V

    .line 177236
    invoke-virtual {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->finish()V

    goto :goto_0

    .line 177237
    :cond_1
    const v1, 0x7f030953

    invoke-virtual {p0, v1}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->setContentView(I)V

    .line 177238
    const-string v1, "app_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177239
    if-nez v0, :cond_4

    .line 177240
    const-string v0, ""

    move-object v1, v0

    .line 177241
    :goto_1
    const-string v0, "2.3"

    .line 177242
    invoke-direct {p0, v3}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->b(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 177243
    if-eqz v2, :cond_3

    .line 177244
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object v2, v0

    .line 177245
    :goto_2
    const v0, 0x7f0832cf

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 177246
    new-array v4, v11, [Ljava/lang/Object;

    aput-object v1, v4, v10

    invoke-static {v0, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 177247
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 177248
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177249
    const v0, 0x7f0832d0

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 177250
    new-array v4, v11, [Ljava/lang/Object;

    aput-object v1, v4, v10

    invoke-static {v0, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 177251
    const v0, 0x7f0d02a7

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 177252
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177253
    const v0, 0x7f0d17e0

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 177254
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177255
    :goto_3
    const v0, 0x7f0832d1

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 177256
    new-array v1, v11, [Ljava/lang/Object;

    aput-object v2, v1, v10

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 177257
    const v0, 0x7f0d17f0

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 177258
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177259
    invoke-virtual {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "release_notes"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177260
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 177261
    const v0, 0x7f0832d3

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 177262
    :cond_2
    const v1, 0x7f0832d2

    invoke-virtual {p0, v1}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 177263
    new-array v2, v11, [Ljava/lang/Object;

    aput-object v0, v2, v10

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 177264
    const v0, 0x7f0d17f1

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 177265
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177266
    invoke-direct {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->a(Landroid/widget/TextView;)V

    .line 177267
    const v0, 0x7f0d17f4

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/Fjh;

    invoke-direct {v1, p0, v3}, LX/Fjh;-><init>(Lcom/facebook/selfupdate/SelfUpdateInstallActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177268
    const v0, 0x7f0d17f3

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->A:Landroid/view/View;

    .line 177269
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->A:Landroid/view/View;

    new-instance v1, LX/Fji;

    invoke-direct {v1, p0}, LX/Fji;-><init>(Lcom/facebook/selfupdate/SelfUpdateInstallActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177270
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->p:LX/Fjj;

    const-string v1, "selfupdate_install_activity_shows"

    const-string v2, "source"

    iget-object v3, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->B:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_0

    :catch_0
    goto/16 :goto_3

    :cond_3
    move-object v2, v0

    goto/16 :goto_2

    :cond_4
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 177208
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 177209
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 177210
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->p:LX/Fjj;

    const-string v1, "selfupdate_installation_result_failure"

    const-string v2, "result_code"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 177211
    const-string v0, "installation_failure"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->a(Ljava/lang/String;LX/0m9;)V

    .line 177212
    invoke-virtual {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0832da

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 177213
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->finish()V

    .line 177214
    :goto_0
    return-void

    .line 177215
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 177203
    iget-object v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->p:LX/Fjj;

    const-string v1, "selfupdate_back_button"

    const-string v2, "source"

    iget-object v3, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->B:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 177204
    iget-boolean v0, p0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->z:Z

    if-nez v0, :cond_0

    .line 177205
    const-string v0, "back_pressed"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->a(Ljava/lang/String;LX/0m9;)V

    .line 177206
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 177207
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x408780f6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 177200
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 177201
    invoke-direct {p0}, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;->a()V

    .line 177202
    const/16 v1, 0x23

    const v2, -0x7c3c6eef

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
