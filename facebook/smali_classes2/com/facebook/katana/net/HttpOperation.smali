.class public Lcom/facebook/katana/net/HttpOperation;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Ljava/lang/Runnable;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static e:J


# instance fields
.field public final a:Lorg/apache/http/client/methods/HttpRequestBase;

.field private c:J

.field public d:J

.field private final f:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 179118
    const-class v0, Lcom/facebook/katana/net/HttpOperation;

    sput-object v0, Lcom/facebook/katana/net/HttpOperation;->b:Ljava/lang/Class;

    .line 179119
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/facebook/katana/net/HttpOperation;->e:J

    return-void
.end method

.method public static declared-synchronized b(Landroid/content/Context;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 179120
    const-class v1, Lcom/facebook/katana/net/HttpOperation;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v0

    check-cast v0, LX/0Uo;

    .line 179121
    invoke-virtual {v0}, LX/0Uo;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179122
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sget-object v2, LX/0sH;->Background:LX/0sH;

    invoke-static {p0, v0, v2}, LX/0sG;->a(Landroid/content/Context;Ljava/lang/Boolean;LX/0sH;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 179123
    :goto_0
    monitor-exit v1

    return-object v0

    .line 179124
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 179125
    sget-wide v4, Lcom/facebook/katana/net/HttpOperation;->e:J

    sub-long v4, v2, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/32 v6, 0x1499700

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 179126
    sput-wide v2, Lcom/facebook/katana/net/HttpOperation;->e:J

    .line 179127
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sget-object v2, LX/0sH;->Foreground:LX/0sH;

    invoke-static {p0, v0, v2}, LX/0sG;->a(Landroid/content/Context;Ljava/lang/Boolean;LX/0sH;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 179128
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 179129
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 179130
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, LX/45Y;->a(Z)V

    .line 179131
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/net/HttpOperation;->c:J

    .line 179132
    :try_start_0
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/katana/net/HttpOperation;->g:Ljava/lang/String;

    .line 179133
    iput-object v1, v0, LX/15E;->c:Ljava/lang/String;

    .line 179134
    move-object v0, v0

    .line 179135
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 179136
    iput-object v1, v0, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 179137
    move-object v0, v0

    .line 179138
    iget-object v1, p0, Lcom/facebook/katana/net/HttpOperation;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 179139
    iput-object v1, v0, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 179140
    move-object v0, v0

    .line 179141
    new-instance v1, LX/FBE;

    invoke-direct {v1, p0}, LX/FBE;-><init>(Lcom/facebook/katana/net/HttpOperation;)V

    .line 179142
    iput-object v1, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 179143
    move-object v0, v0

    .line 179144
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 179145
    iget-object v1, p0, Lcom/facebook/katana/net/HttpOperation;->f:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 179146
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
