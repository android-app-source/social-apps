.class public Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;
.super LX/0Oo;
.source ""


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private d:Landroid/content/UriMatcher;

.field private e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/28Q;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54366
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, LX/007;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".provider.FirstPartyUserValuesProvider"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->b:Ljava/lang/String;

    .line 54367
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "value"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54359
    invoke-direct {p0}, LX/0Oo;-><init>()V

    .line 54360
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 54361
    iput-object v0, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->e:LX/0Ot;

    .line 54362
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 54363
    iput-object v0, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->f:LX/0Ot;

    .line 54364
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 54365
    iput-object v0, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->g:LX/0Ot;

    return-void
.end method

.method private static a(Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;",
            "LX/0Ot",
            "<",
            "LX/28Q;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54325
    iput-object p1, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->e:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->f:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->g:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;

    const/16 v1, 0xbfa

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2ba

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x259

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->a(Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 54358
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 54357
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 54336
    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_6

    const-string v0, "name=\'active_session_info\'"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 54337
    iget-object v0, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/28Q;

    invoke-virtual {v0}, LX/28Q;->a()Lcom/facebook/katana/model/FacebookSessionInfo;

    move-result-object v2

    .line 54338
    if-nez v2, :cond_1

    move-object v0, v1

    :goto_0
    move-object v1, v0

    .line 54339
    :goto_1
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-direct {v2, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 54340
    if-eqz v1, :cond_5

    .line 54341
    if-nez p2, :cond_0

    .line 54342
    sget-object p2, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->c:[Ljava/lang/String;

    .line 54343
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 54344
    array-length v4, p2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v4, :cond_4

    aget-object v5, p2, v0

    .line 54345
    const-string v6, "name"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 54346
    const-string v5, "active_session_info"

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54347
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 54348
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-virtual {v0, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 54349
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 54350
    iget-object v0, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "SessionInfoSerialization"

    const-string v4, "Couldn\'t serialize sessionInfo."

    invoke-virtual {v0, v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 54351
    :cond_2
    const-string v6, "value"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 54352
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 54353
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only name and value are supported in the projection map"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54354
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 54355
    :cond_5
    return-object v2

    .line 54356
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 54335
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 54332
    iget-object v0, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 54333
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54334
    :pswitch_0
    const-string v0, "vnd.android.cursor.item/vnd.facebook.katana.uservalues"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 54326
    invoke-super {p0}, LX/0Oo;->a()V

    .line 54327
    const-class v0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;

    invoke-static {v0, p0}, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->a(Ljava/lang/Class;LX/02k;)V

    .line 54328
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->d:Landroid/content/UriMatcher;

    .line 54329
    iget-object v0, p0, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->d:Landroid/content/UriMatcher;

    sget-object v1, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->b:Ljava/lang/String;

    const-string v2, "user_values"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 54330
    invoke-virtual {p0}, Lcom/facebook/katana/provider/FirstPartyUserValuesProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 54331
    return-void
.end method
