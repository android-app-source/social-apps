.class public Lcom/facebook/katana/provider/UserValuesProvider;
.super LX/0Ov;
.source ""


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/28Q;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0cZ;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/content/UriMatcher;

.field private e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2BQ;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 56012
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "value"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/katana/provider/UserValuesProvider;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56005
    invoke-direct {p0}, LX/0Ov;-><init>()V

    .line 56006
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 56007
    iput-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->e:LX/0Ot;

    .line 56008
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 56009
    iput-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->g:LX/0Ot;

    .line 56010
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 56011
    iput-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->h:LX/0Ot;

    return-void
.end method

.method private static a(Lcom/facebook/katana/provider/UserValuesProvider;LX/0Or;LX/0Or;LX/0Ot;LX/03V;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/provider/UserValuesProvider;",
            "LX/0Or",
            "<",
            "LX/28Q;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0cZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2BQ;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56004
    iput-object p1, p0, Lcom/facebook/katana/provider/UserValuesProvider;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/katana/provider/UserValuesProvider;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/katana/provider/UserValuesProvider;->e:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/katana/provider/UserValuesProvider;->f:LX/03V;

    iput-object p5, p0, Lcom/facebook/katana/provider/UserValuesProvider;->g:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/katana/provider/UserValuesProvider;->h:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/katana/provider/UserValuesProvider;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/katana/provider/UserValuesProvider;

    const/16 v1, 0xbfa

    invoke-static {v6, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x2a3

    invoke-static {v6, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0xc77

    invoke-static {v6, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const/16 v5, 0x2ba

    invoke-static {v6, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v7, 0xf9a

    invoke-static {v6, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/facebook/katana/provider/UserValuesProvider;->a(Lcom/facebook/katana/provider/UserValuesProvider;LX/0Or;LX/0Or;LX/0Ot;LX/03V;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private b(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 55992
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 55993
    iget-object v1, p0, Lcom/facebook/katana/provider/UserValuesProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 55994
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55995
    :pswitch_0
    const-string v1, "user_values"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 55996
    :goto_0
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55997
    const-string v7, "name DESC"

    .line 55998
    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/provider/UserValuesProvider;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2BQ;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 55999
    invoke-virtual {p0}, Lcom/facebook/katana/provider/UserValuesProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 56000
    return-object v0

    .line 56001
    :pswitch_1
    const-string v1, "user_values"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 56002
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "name=\'"

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x2

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    move-object v7, p5

    .line 56003
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55986
    if-eqz p1, :cond_0

    .line 55987
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 55988
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 55989
    :pswitch_0
    const-string v0, "name=\'active_session_info\'"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55990
    const-string v0, "active_session_info"

    goto :goto_0

    .line 55991
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 55985
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 55929
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2BQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 55930
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 55931
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55932
    :pswitch_0
    const-string v0, "user_values"

    invoke-virtual {v1, v0, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 55933
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/katana/provider/UserValuesProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v5, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 55934
    return v0

    .line 55935
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 55936
    const-string v2, "user_values"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "name="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 55958
    invoke-direct {p0, p1, p3}, Lcom/facebook/katana/provider/UserValuesProvider;->c(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 55959
    if-eqz v3, :cond_8

    .line 55960
    const-string v0, "active_session_info"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 55961
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/28Q;

    .line 55962
    invoke-virtual {v0}, LX/28Q;->a()Lcom/facebook/katana/model/FacebookSessionInfo;

    move-result-object v1

    .line 55963
    if-nez v1, :cond_2

    move-object v0, v2

    :goto_0
    move-object v2, v0

    .line 55964
    :cond_0
    :goto_1
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 55965
    if-eqz v2, :cond_4

    .line 55966
    if-nez p2, :cond_1

    .line 55967
    sget-object p2, Lcom/facebook/katana/provider/UserValuesProvider;->c:[Ljava/lang/String;

    .line 55968
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 55969
    array-length v5, p2

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v5, :cond_7

    aget-object v6, p2, v1

    .line 55970
    const-string v7, "name"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 55971
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55972
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 55973
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-virtual {v0, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 55974
    :catch_0
    move-exception v0

    .line 55975
    iget-object v1, p0, Lcom/facebook/katana/provider/UserValuesProvider;->f:LX/03V;

    const-string v4, "SessionInfoSerialization"

    const-string v5, "Couldn\'t serialize sessionInfo."

    invoke-virtual {v1, v4, v5, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 55976
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1CA;->c:LX/0Tn;

    invoke-virtual {v1, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55977
    if-nez v2, :cond_0

    .line 55978
    invoke-direct/range {p0 .. p5}, Lcom/facebook/katana/provider/UserValuesProvider;->b(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 55979
    :cond_4
    :goto_4
    return-object v0

    .line 55980
    :cond_5
    const-string v7, "value"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 55981
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 55982
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only name and value are supported in the projection map"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55983
    :cond_7
    invoke-virtual {v4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_4

    .line 55984
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 55947
    if-eqz p2, :cond_0

    .line 55948
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    move-object v1, v0

    .line 55949
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_1

    .line 55950
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55951
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    move-object v1, v0

    goto :goto_0

    .line 55952
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2BQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v3, "user_values"

    const-string v4, "name"

    const v5, 0x1383d11

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {v0, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const v3, 0x1cc940e9

    invoke-static {v3}, LX/03h;->a(I)V

    .line 55953
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_2

    .line 55954
    sget-object v3, LX/2BM;->b:Landroid/net/Uri;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 55955
    invoke-virtual {p0}, Lcom/facebook/katana/provider/UserValuesProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 55956
    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/provider/UserValuesProvider;->f:LX/03V;

    const-string v2, "UserValuesProvider"

    const-string v3, "UserValuesProvider should not create any new entry in old table"

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 55957
    return-object v0

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 55944
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 55945
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55946
    :pswitch_0
    const-string v0, "vnd.android.cursor.item/vnd.facebook.katana.uservalues"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 55937
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->d:Landroid/content/UriMatcher;

    .line 55938
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->d:Landroid/content/UriMatcher;

    sget-object v1, LX/2BM;->a:Ljava/lang/String;

    const-string v2, "user_values"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 55939
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->d:Landroid/content/UriMatcher;

    sget-object v1, LX/2BM;->a:Ljava/lang/String;

    const-string v2, "user_values/name/*"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 55940
    const-class v0, Lcom/facebook/katana/provider/UserValuesProvider;

    invoke-static {v0, p0}, Lcom/facebook/katana/provider/UserValuesProvider;->a(Ljava/lang/Class;LX/02k;)V

    .line 55941
    iget-object v0, p0, Lcom/facebook/katana/provider/UserValuesProvider;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cZ;

    .line 55942
    invoke-virtual {v0}, LX/0Uq;->b()V

    .line 55943
    return-void
.end method
