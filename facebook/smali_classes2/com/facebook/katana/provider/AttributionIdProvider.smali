.class public Lcom/facebook/katana/provider/AttributionIdProvider;
.super LX/0Om;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54212
    invoke-direct {p0}, LX/0Om;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 54210
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 54211
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final declared-synchronized a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 54195
    monitor-enter p0

    .line 54196
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/katana/provider/AttributionIdProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1ma;->a(Landroid/content/Context;)LX/1mb;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    move-object v2, v0

    .line 54197
    :goto_0
    if-eqz v2, :cond_1

    :try_start_1
    iget-boolean v0, v2, LX/1mb;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    .line 54198
    :try_start_2
    invoke-virtual {p0}, Lcom/facebook/katana/provider/AttributionIdProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1oW;->a(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_4

    .line 54199
    invoke-virtual {p0}, Lcom/facebook/katana/provider/AttributionIdProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :goto_1
    move-object v3, v0

    .line 54200
    :goto_2
    if-eqz v2, :cond_2

    :try_start_3
    iget-object v0, v2, LX/1mb;->a:Ljava/lang/String;

    move-object v2, v0

    .line 54201
    :goto_3
    if-eqz v3, :cond_3

    iget-object v0, v3, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->a:Ljava/lang/String;

    move-object v0, v0

    .line 54202
    :goto_4
    if-eqz v3, :cond_0

    iget-boolean v1, v3, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->b:Z

    move v1, v1

    .line 54203
    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    .line 54204
    :cond_0
    new-instance v3, Landroid/database/MatrixCursor;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "aid"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "androidid"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "limit_tracking"

    aput-object v6, v4, v5

    invoke-direct {v3, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 54205
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v0, v4, v2

    const/4 v0, 0x2

    aput-object v1, v4, v0

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 54206
    monitor-exit p0

    return-object v3

    :catch_0
    move-object v2, v1

    goto :goto_0

    :catch_1
    :cond_1
    move-object v3, v1

    goto :goto_2

    :cond_2
    move-object v2, v1

    .line 54207
    goto :goto_3

    :cond_3
    move-object v0, v1

    .line 54208
    goto :goto_4

    .line 54209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 54194
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54193
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
