.class public Lcom/facebook/katana/provider/GraphQLNotificationsContentProvider;
.super LX/0PF;
.source ""


# instance fields
.field private a:LX/3Ej;

.field private b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55340
    invoke-direct {p0}, LX/0PF;-><init>()V

    return-void
.end method

.method private a(LX/3Ej;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Ej;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 55337
    iput-object p1, p0, Lcom/facebook/katana/provider/GraphQLNotificationsContentProvider;->a:LX/3Ej;

    .line 55338
    iput-object p2, p0, Lcom/facebook/katana/provider/GraphQLNotificationsContentProvider;->b:LX/0Or;

    .line 55339
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/katana/provider/GraphQLNotificationsContentProvider;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/katana/provider/GraphQLNotificationsContentProvider;

    new-instance v3, LX/3Ej;

    const-class v4, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v1, v4, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x15e7

    invoke-static {v1, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v1}, LX/0XV;->b(LX/0QB;)LX/01U;

    move-result-object v6

    check-cast v6, LX/01U;

    invoke-static {v1}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v7

    check-cast v7, LX/0aU;

    invoke-static {v1}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-direct/range {v3 .. v8}, LX/3Ej;-><init>(Landroid/content/Context;LX/0Or;LX/01U;LX/0aU;Landroid/os/Handler;)V

    move-object v0, v3

    check-cast v0, LX/3Ej;

    const/16 v2, 0x15e7

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/provider/GraphQLNotificationsContentProvider;->a(LX/3Ej;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 55334
    const-class v0, Lcom/facebook/katana/provider/GraphQLNotificationsContentProvider;

    invoke-static {v0, p0}, Lcom/facebook/katana/provider/GraphQLNotificationsContentProvider;->a(Ljava/lang/Class;LX/02k;)V

    .line 55335
    invoke-super {p0}, LX/0PF;->a()V

    .line 55336
    return-void
.end method

.method public final c(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 55328
    invoke-super {p0, p1}, LX/0PF;->c(Landroid/net/Uri;)V

    .line 55329
    iget-object v1, p0, Lcom/facebook/katana/provider/GraphQLNotificationsContentProvider;->a:LX/3Ej;

    iget-object v0, p0, Lcom/facebook/katana/provider/GraphQLNotificationsContentProvider;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 55330
    new-instance v2, Landroid/content/Intent;

    iget-object p0, v1, LX/3Ej;->e:Ljava/lang/String;

    invoke-direct {v2, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 55331
    const-string p0, "userId"

    invoke-virtual {v2, p0, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55332
    iget-object p0, v1, LX/3Ej;->b:Landroid/content/Context;

    iget-object p1, v1, LX/3Ej;->d:Ljava/lang/String;

    invoke-virtual {p0, v2, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 55333
    return-void
.end method
