.class public Lcom/facebook/katana/provider/ConnectionsProvider;
.super LX/0Ov;
.source ""


# static fields
.field private static final a:Landroid/content/UriMatcher;


# instance fields
.field private b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hwa;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 55320
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 55321
    sput-object v0, Lcom/facebook/katana/provider/ConnectionsProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, LX/0PE;->a:Ljava/lang/String;

    const-string v2, "search_results"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 55322
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55317
    invoke-direct {p0}, LX/0Ov;-><init>()V

    .line 55318
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 55319
    iput-object v0, p0, Lcom/facebook/katana/provider/ConnectionsProvider;->b:LX/0Ot;

    return-void
.end method

.method private a(Ljava/lang/RuntimeException;Ljava/lang/String;)Ljava/lang/RuntimeException;
    .locals 2

    .prologue
    .line 55311
    sget-object v0, LX/03R;->YES:LX/03R;

    invoke-virtual {p0}, Lcom/facebook/katana/provider/ConnectionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    invoke-static {v1}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 55312
    :goto_0
    if-nez v0, :cond_0

    .line 55313
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 55314
    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return-object p1

    .line 55315
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 55316
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static a(Lcom/facebook/katana/provider/ConnectionsProvider;LX/0Ot;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/provider/ConnectionsProvider;",
            "LX/0Ot",
            "<",
            "LX/Hwa;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55310
    iput-object p1, p0, Lcom/facebook/katana/provider/ConnectionsProvider;->b:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/katana/provider/ConnectionsProvider;->c:LX/03V;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/katana/provider/ConnectionsProvider;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/katana/provider/ConnectionsProvider;

    const/16 v1, 0x19fd

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0, v1, v0}, Lcom/facebook/katana/provider/ConnectionsProvider;->a(Lcom/facebook/katana/provider/ConnectionsProvider;LX/0Ot;LX/03V;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 55246
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Nothing is updatable here"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 55299
    sget-object v0, LX/0PE;->b:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55300
    iget-object v0, p0, Lcom/facebook/katana/provider/ConnectionsProvider;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hwa;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 55301
    const/4 v0, 0x0

    .line 55302
    :cond_0
    :goto_0
    return v0

    .line 55303
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/provider/ConnectionsProvider;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hwa;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 55304
    sget-object v1, Lcom/facebook/katana/provider/ConnectionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 55305
    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 55306
    const-string v1, "search_results"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 55307
    if-lez v0, :cond_0

    .line 55308
    invoke-virtual {p0}, Lcom/facebook/katana/provider/ConnectionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, LX/IYR;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 55309
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 55276
    iget-object v0, p0, Lcom/facebook/katana/provider/ConnectionsProvider;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hwa;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 55277
    sget-object v0, Lcom/facebook/katana/provider/ConnectionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 55278
    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 55279
    const v0, 0x3a2523f2

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 55280
    :try_start_0
    array-length v4, p2

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, p2, v2

    .line 55281
    const-string v6, "search_results"

    sget-object v7, LX/IYQ;->b:LX/0U1;

    .line 55282
    iget-object v8, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v8

    .line 55283
    const v8, -0x779ea913

    invoke-static {v8}, LX/03h;->a(I)V

    invoke-virtual {v3, v6, v7, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    const v5, -0x274f1320

    invoke-static {v5}, LX/03h;->a(I)V

    .line 55284
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 55285
    add-int/lit8 v1, v1, 0x1

    .line 55286
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 55287
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 55288
    :cond_1
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55289
    const v2, -0x428bcd4a

    invoke-static {v3, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 55290
    if-eqz v1, :cond_2

    .line 55291
    invoke-virtual {p0}, Lcom/facebook/katana/provider/ConnectionsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, LX/IYR;->a:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 55292
    :cond_2
    if-lez v0, :cond_3

    .line 55293
    iget-object v2, p0, Lcom/facebook/katana/provider/ConnectionsProvider;->c:LX/03V;

    const-string v3, "Failed insert into SEARCH_RESULTS_TABLE"

    const-string v4, "Failed on %d rows"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 55294
    :cond_3
    return v1

    .line 55295
    :catch_0
    move-exception v0

    .line 55296
    :try_start_1
    const-string v1, "Error in bulk insert of search results"

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/provider/ConnectionsProvider;->a(Ljava/lang/RuntimeException;Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55297
    :catchall_0
    move-exception v0

    const v1, -0x11fb3226

    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 55298
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 55264
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 55265
    sget-object v1, Lcom/facebook/katana/provider/ConnectionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 55266
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 55267
    const-string v1, "search_results"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 55268
    const-string v7, "_id ASC"

    .line 55269
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55270
    :goto_0
    iget-object v1, p0, Lcom/facebook/katana/provider/ConnectionsProvider;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hwa;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v8, v5

    .line 55271
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 55272
    invoke-virtual {p0}, Lcom/facebook/katana/provider/ConnectionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 55273
    return-object v0

    .line 55274
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v7, p5

    .line 55275
    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 55254
    iget-object v0, p0, Lcom/facebook/katana/provider/ConnectionsProvider;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hwa;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 55255
    sget-object v1, Lcom/facebook/katana/provider/ConnectionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 55256
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 55257
    const-string v1, "search_results"

    sget-object v2, LX/IYQ;->c:LX/0U1;

    .line 55258
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 55259
    const v3, 0x51dc15a4

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {v0, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const v2, 0x75ef4517

    invoke-static {v2}, LX/03h;->a(I)V

    .line 55260
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 55261
    invoke-virtual {p0}, Lcom/facebook/katana/provider/ConnectionsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, LX/IYR;->a:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 55262
    :cond_0
    sget-object v2, LX/IYR;->a:Landroid/net/Uri;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 55263
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 55250
    sget-object v0, Lcom/facebook/katana/provider/ConnectionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 55251
    if-lez v0, :cond_0

    .line 55252
    const-string v0, "vnd.android.cursor.item/vnd.com.facebook.katana.provider.friends"

    return-object v0

    .line 55253
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 55247
    monitor-enter p0

    :try_start_0
    const-class v0, Lcom/facebook/katana/provider/ConnectionsProvider;

    invoke-static {v0, p0}, Lcom/facebook/katana/provider/ConnectionsProvider;->a(Ljava/lang/Class;LX/02k;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55248
    monitor-exit p0

    return-void

    .line 55249
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
