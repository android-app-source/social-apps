.class public Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0x3;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0hh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 115791
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 115792
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 115793
    iput-object v0, p0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->a:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    const/16 p0, 0xc02

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p1, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->a:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 115785
    const-class v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    invoke-static {v0, p0}, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 115786
    iget-object v0, p0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x3;

    .line 115787
    iget-object p1, v0, LX/0x3;->a:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0hh;

    .line 115788
    move-object v0, p1

    .line 115789
    iput-object v0, p0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    .line 115790
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x11cd920d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115772
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 115773
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 115774
    if-nez v1, :cond_1

    .line 115775
    const/4 v1, 0x1

    .line 115776
    :goto_0
    move v1, v1

    .line 115777
    if-eqz v1, :cond_0

    .line 115778
    const/16 v1, 0x2b

    const v2, -0x2a1d0f05

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 115779
    :goto_1
    return-void

    .line 115780
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hh;->a(Landroid/app/Activity;)V

    .line 115781
    const v1, 0x42706a0a

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_1

    .line 115782
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p1, 0x11

    if-lt v2, p1, :cond_2

    .line 115783
    invoke-virtual {v1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    goto :goto_0

    .line 115784
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 115769
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 115770
    iget-object v0, p0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    invoke-virtual {v0, p1, p2, p3}, LX/0hh;->a(IILandroid/content/Intent;)V

    .line 115771
    return-void
.end method

.method public final onDestroy()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x58056d24

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115684
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 115685
    iget-object v1, p0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 115686
    iget-object v4, v1, LX/0hh;->m:LX/0Ot;

    if-eqz v4, :cond_0

    iget-object v4, v1, LX/0hh;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, v1, LX/0hh;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0x4;

    invoke-virtual {v4}, LX/0hD;->kw_()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 115687
    iget-object v4, v1, LX/0hh;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0x4;

    .line 115688
    const/4 v5, 0x0

    iput-object v5, v4, LX/0x4;->c:LX/0f7;

    .line 115689
    :cond_0
    iget-object v4, v1, LX/0hh;->l:LX/0Ot;

    if-eqz v4, :cond_1

    iget-object v4, v1, LX/0hh;->l:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, v1, LX/0hh;->l:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0hm;

    invoke-virtual {v4}, LX/0hm;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 115690
    iget-object v4, v1, LX/0hh;->l:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0hm;

    .line 115691
    const/4 v5, 0x0

    iput-object v5, v4, LX/0hm;->h:LX/0f7;

    .line 115692
    :cond_1
    iget-object v4, v1, LX/0hh;->k:LX/0Ot;

    if-eqz v4, :cond_3

    iget-object v4, v1, LX/0hh;->k:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, v1, LX/0hh;->k:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0x5;

    invoke-virtual {v4}, LX/0hD;->kw_()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 115693
    iget-object v4, v1, LX/0hh;->k:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0x5;

    .line 115694
    iget-object v5, v4, LX/0x5;->d:LX/0Yb;

    if-eqz v5, :cond_2

    .line 115695
    iget-object v5, v4, LX/0x5;->d:LX/0Yb;

    invoke-virtual {v5}, LX/0Yb;->c()V

    .line 115696
    const/4 v5, 0x0

    iput-object v5, v4, LX/0x5;->d:LX/0Yb;

    .line 115697
    :cond_2
    iget-object v5, v4, LX/0x5;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03V;

    const-string v6, "harrison_fragment_stacks"

    invoke-virtual {v5, v6}, LX/03V;->a(Ljava/lang/String;)V

    .line 115698
    iget-object v5, v4, LX/0x5;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03V;

    const-string v6, "harrison_current_tab"

    invoke-virtual {v5, v6}, LX/03V;->a(Ljava/lang/String;)V

    .line 115699
    const/4 v5, 0x0

    iput-object v5, v4, LX/0x5;->c:LX/0f7;

    .line 115700
    :cond_3
    iget-object v4, v1, LX/0hh;->j:LX/0Ot;

    if-eqz v4, :cond_4

    iget-object v4, v1, LX/0hh;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v4, v1, LX/0hh;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0x6;

    invoke-virtual {v4}, LX/0x6;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 115701
    iget-object v4, v1, LX/0hh;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    .line 115702
    :cond_4
    iget-object v4, v1, LX/0hh;->i:LX/0Ot;

    if-eqz v4, :cond_5

    iget-object v4, v1, LX/0hh;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v4, v1, LX/0hh;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0jP;

    invoke-virtual {v4}, LX/0hD;->kw_()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 115703
    iget-object v4, v1, LX/0hh;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0jP;

    const/4 v5, 0x0

    .line 115704
    iput-object v5, v4, LX/0jP;->k:Lcom/facebook/widget/CustomViewPager;

    .line 115705
    iput-object v5, v4, LX/0jP;->j:Landroid/app/Activity;

    .line 115706
    iput-object v5, v4, LX/0jP;->i:LX/0f7;

    .line 115707
    iget-object v5, v4, LX/0jP;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0tK;

    iget-object v6, v4, LX/0jP;->n:LX/0x7;

    invoke-virtual {v5, v6}, LX/0tK;->b(LX/0x7;)V

    .line 115708
    iget-object v5, v4, LX/0jP;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0x8;

    invoke-virtual {v5}, LX/0x8;->b()V

    .line 115709
    :cond_5
    iget-object v4, v1, LX/0hh;->h:LX/0Ot;

    if-eqz v4, :cond_6

    iget-object v4, v1, LX/0hh;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_6

    iget-object v4, v1, LX/0hh;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0iG;

    invoke-virtual {v4}, LX/0hD;->kw_()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 115710
    iget-object v4, v1, LX/0hh;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0iG;

    const/4 v6, 0x0

    .line 115711
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0iG;->a(Z)V

    .line 115712
    iget-object v5, v4, LX/0iG;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0x9;

    .line 115713
    iput-object v6, v5, LX/0x9;->j:Landroid/view/View;

    .line 115714
    iput-object v6, v4, LX/0iG;->e:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 115715
    iput-object v6, v4, LX/0iG;->d:LX/0f7;

    .line 115716
    :cond_6
    iget-object v4, v1, LX/0hh;->g:LX/0Ot;

    if-eqz v4, :cond_9

    iget-object v4, v1, LX/0hh;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_9

    iget-object v4, v1, LX/0hh;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0iD;

    invoke-virtual {v4}, LX/0hD;->kw_()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 115717
    iget-object v4, v1, LX/0hh;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0iD;

    const/4 p0, 0x0

    .line 115718
    iget-object v5, v4, LX/0iD;->u:LX/0Yb;

    if-eqz v5, :cond_7

    .line 115719
    iget-object v5, v4, LX/0iD;->u:LX/0Yb;

    invoke-virtual {v5}, LX/0Yb;->c()V

    .line 115720
    iput-object p0, v4, LX/0iD;->u:LX/0Yb;

    .line 115721
    :cond_7
    iget-object v5, v4, LX/0iD;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_8

    iget-object v5, v4, LX/0iD;->t:LX/0xA;

    if-eqz v5, :cond_8

    .line 115722
    iget-object v5, v4, LX/0iD;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0xB;

    iget-object v6, v4, LX/0iD;->t:LX/0xA;

    invoke-virtual {v5, v6}, LX/0xB;->b(LX/0xA;)V

    .line 115723
    iput-object p0, v4, LX/0iD;->t:LX/0xA;

    .line 115724
    :cond_8
    iput-object p0, v4, LX/0iD;->q:Lcom/facebook/widget/CustomViewPager;

    .line 115725
    iput-object p0, v4, LX/0iD;->p:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 115726
    iput-object p0, v4, LX/0iD;->o:LX/0f7;

    .line 115727
    iget-object v5, v4, LX/0iD;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0xC;

    invoke-virtual {v5}, LX/0xC;->a()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 115728
    iget-object v5, v4, LX/0iD;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0iE;

    invoke-interface {v5}, LX/0iE;->c()V

    .line 115729
    :cond_9
    iget-object v4, v1, LX/0hh;->f:LX/0Ot;

    if-eqz v4, :cond_a

    iget-object v4, v1, LX/0hh;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_a

    iget-object v4, v1, LX/0hh;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xD;

    invoke-virtual {v4}, LX/0hD;->kw_()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 115730
    iget-object v4, v1, LX/0hh;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xD;

    .line 115731
    iget-object v5, v4, LX/0xD;->b:LX/0Uh;

    const/16 v6, 0x3bb

    const/4 p0, 0x0

    invoke-virtual {v5, v6, p0}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v5, v4, LX/0xD;->e:LX/0dN;

    if-eqz v5, :cond_a

    .line 115732
    iget-object v5, v4, LX/0xD;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/0hM;->M:LX/0Tn;

    iget-object p0, v4, LX/0xD;->e:LX/0dN;

    invoke-interface {v5, v6, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;LX/0dN;)V

    .line 115733
    :cond_a
    iget-object v4, v1, LX/0hh;->e:LX/0Ot;

    if-eqz v4, :cond_b

    iget-object v4, v1, LX/0hh;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_b

    iget-object v4, v1, LX/0hh;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xE;

    invoke-virtual {v4}, LX/0hD;->kw_()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 115734
    iget-object v4, v1, LX/0hh;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xE;

    .line 115735
    iget-object v5, v4, LX/0xE;->d:LX/0Yb;

    if-eqz v5, :cond_b

    .line 115736
    iget-object v5, v4, LX/0xE;->d:LX/0Yb;

    invoke-virtual {v5}, LX/0Yb;->c()V

    .line 115737
    const/4 v5, 0x0

    iput-object v5, v4, LX/0xE;->d:LX/0Yb;

    .line 115738
    :cond_b
    iget-object v4, v1, LX/0hh;->d:LX/0Ot;

    if-eqz v4, :cond_d

    iget-object v4, v1, LX/0hh;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_d

    iget-object v4, v1, LX/0hh;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xF;

    invoke-virtual {v4}, LX/0hD;->kw_()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 115739
    iget-object v4, v1, LX/0hh;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xF;

    const/4 v6, 0x0

    .line 115740
    iget-object v5, v4, LX/0xF;->d:LX/0Yb;

    if-eqz v5, :cond_c

    .line 115741
    iget-object v5, v4, LX/0xF;->d:LX/0Yb;

    invoke-virtual {v5}, LX/0Yb;->c()V

    .line 115742
    iput-object v6, v4, LX/0xF;->d:LX/0Yb;

    .line 115743
    :cond_c
    iget-object v5, v4, LX/0xF;->e:LX/0Yb;

    if-eqz v5, :cond_d

    .line 115744
    iget-object v5, v4, LX/0xF;->e:LX/0Yb;

    invoke-virtual {v5}, LX/0Yb;->c()V

    .line 115745
    iput-object v6, v4, LX/0xF;->e:LX/0Yb;

    .line 115746
    :cond_d
    iget-object v4, v1, LX/0hh;->c:LX/0Ot;

    if-eqz v4, :cond_e

    iget-object v4, v1, LX/0hh;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_e

    iget-object v4, v1, LX/0hh;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xG;

    invoke-virtual {v4}, LX/0hD;->kw_()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 115747
    iget-object v4, v1, LX/0hh;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    .line 115748
    :cond_e
    iget-object v4, v1, LX/0hh;->b:LX/0Ot;

    if-eqz v4, :cond_f

    iget-object v4, v1, LX/0hh;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_f

    iget-object v4, v1, LX/0hh;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xH;

    invoke-virtual {v4}, LX/0xH;->a()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 115749
    iget-object v4, v1, LX/0hh;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xH;

    .line 115750
    iget-boolean v5, v4, LX/0xH;->e:Z

    if-eqz v5, :cond_f

    .line 115751
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {v5, v2, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v5}, Landroid/app/Activity;->stopService(Landroid/content/Intent;)Z

    .line 115752
    const/4 v5, 0x0

    iput-boolean v5, v4, LX/0xH;->e:Z

    .line 115753
    :cond_f
    iget-object v4, v1, LX/0hh;->a:LX/0Ot;

    if-eqz v4, :cond_10

    iget-object v4, v1, LX/0hh;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_10

    iget-object v4, v1, LX/0hh;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xI;

    invoke-virtual {v4}, LX/0xI;->a()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 115754
    iget-object v4, v1, LX/0hh;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0xI;

    .line 115755
    const/4 v5, 0x0

    iput-object v5, v4, LX/0xI;->f:Landroid/app/Activity;

    .line 115756
    :cond_10
    const/16 v1, 0x2b

    const v2, 0x2eb57067

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6b8a4674

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115766
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 115767
    iget-object v1, p0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    invoke-virtual {v1}, LX/0hh;->d()V

    .line 115768
    const/16 v1, 0x2b

    const v2, 0x2450fd7e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x50f3f327

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115763
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 115764
    iget-object v1, p0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    invoke-virtual {v1}, LX/0hh;->c()V

    .line 115765
    const/16 v1, 0x2b

    const v2, 0x3d9dfb46

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x185db93b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115760
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 115761
    iget-object v1, p0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    invoke-virtual {v1}, LX/0hh;->e()V

    .line 115762
    const/16 v1, 0x2b

    const v2, -0x391dbd4b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7f067778

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115757
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 115758
    iget-object v1, p0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    invoke-virtual {v1}, LX/0hh;->f()V

    .line 115759
    const/16 v1, 0x2b

    const v2, -0x3c464a19

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
