.class public Lcom/facebook/katana/fragment/FbChromeFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0f3;
.implements LX/0fk;


# instance fields
.field private A:Landroid/widget/FrameLayout;

.field public a:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/18q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0Yb;

.field private d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/0Sg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/18q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Yl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:I

.field private k:LX/0Va;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Va",
            "<*>;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/apptab/state/TabTag;

.field private m:Landroid/content/Intent;

.field public n:LX/18r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0gh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0kv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/0Yv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/0jo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Z

.field public t:Z

.field private u:Z

.field private v:LX/0gp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:LX/0gy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0gz;

.field private y:LX/0go;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private z:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 110747
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 110748
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->j:I

    .line 110749
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->z:Ljava/util/ArrayList;

    return-void
.end method

.method private a(LX/0hH;)V
    .locals 3

    .prologue
    .line 110827
    invoke-static {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->o(Lcom/facebook/katana/fragment/FbChromeFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 110828
    :cond_0
    :goto_0
    return-void

    .line 110829
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v1, "chromeless:content:fragment:tag"

    .line 110830
    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 110831
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 110832
    :goto_1
    move-object v0, v2

    .line 110833
    if-eqz v0, :cond_0

    .line 110834
    invoke-virtual {p1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/katana/fragment/FbChromeFragment;LX/0Xl;LX/18q;LX/03V;LX/0Sg;LX/18q;LX/0Uh;LX/0Yl;LX/0Ot;LX/18r;LX/0gh;LX/0kv;LX/0Yv;LX/0jo;LX/0gp;LX/0gy;LX/0go;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/fragment/FbChromeFragment;",
            "LX/0Xl;",
            "LX/18q;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/18q;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Yl;",
            "LX/0Ot",
            "<",
            "LX/0oh;",
            ">;",
            "LX/18r;",
            "LX/0gh;",
            "LX/0kv;",
            "LX/0Yv;",
            "LX/0jo;",
            "LX/0gp;",
            "LX/0gy;",
            "LX/0go;",
            ")V"
        }
    .end annotation

    .prologue
    .line 110835
    iput-object p1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->a:LX/0Xl;

    iput-object p2, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->b:LX/18q;

    iput-object p3, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->d:LX/03V;

    iput-object p4, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->e:LX/0Sg;

    iput-object p5, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->f:LX/18q;

    iput-object p6, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->g:LX/0Uh;

    iput-object p7, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->h:LX/0Yl;

    iput-object p8, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->i:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->n:LX/18r;

    iput-object p10, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->o:LX/0gh;

    iput-object p11, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->p:LX/0kv;

    iput-object p12, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->q:LX/0Yv;

    iput-object p13, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->r:LX/0jo;

    iput-object p14, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->v:LX/0gp;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->w:LX/0gy;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->y:LX/0go;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 18

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/katana/fragment/FbChromeFragment;

    invoke-static/range {v17 .. v17}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v2

    check-cast v2, LX/0Xl;

    invoke-static/range {v17 .. v17}, LX/18q;->a(LX/0QB;)LX/18q;

    move-result-object v3

    check-cast v3, LX/18q;

    invoke-static/range {v17 .. v17}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static/range {v17 .. v17}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v5

    check-cast v5, LX/0Sg;

    invoke-static/range {v17 .. v17}, LX/18q;->a(LX/0QB;)LX/18q;

    move-result-object v6

    check-cast v6, LX/18q;

    invoke-static/range {v17 .. v17}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static/range {v17 .. v17}, LX/0Yl;->a(LX/0QB;)LX/0Yl;

    move-result-object v8

    check-cast v8, LX/0Yl;

    const/16 v9, 0x2a8

    move-object/from16 v0, v17

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v17 .. v17}, LX/18r;->a(LX/0QB;)LX/18r;

    move-result-object v10

    check-cast v10, LX/18r;

    invoke-static/range {v17 .. v17}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v11

    check-cast v11, LX/0gh;

    invoke-static/range {v17 .. v17}, LX/0kv;->a(LX/0QB;)LX/0kv;

    move-result-object v12

    check-cast v12, LX/0kv;

    invoke-static/range {v17 .. v17}, LX/0Yv;->a(LX/0QB;)LX/0Yv;

    move-result-object v13

    check-cast v13, LX/0Yv;

    invoke-static/range {v17 .. v17}, LX/0oR;->a(LX/0QB;)LX/0oR;

    move-result-object v14

    check-cast v14, LX/0jo;

    invoke-static/range {v17 .. v17}, LX/0gp;->a(LX/0QB;)LX/0gp;

    move-result-object v15

    check-cast v15, LX/0gp;

    invoke-static/range {v17 .. v17}, LX/0gy;->a(LX/0QB;)LX/0gy;

    move-result-object v16

    check-cast v16, LX/0gy;

    invoke-static/range {v17 .. v17}, LX/0go;->a(LX/0QB;)LX/0go;

    move-result-object v17

    check-cast v17, LX/0go;

    invoke-static/range {v1 .. v17}, Lcom/facebook/katana/fragment/FbChromeFragment;->a(Lcom/facebook/katana/fragment/FbChromeFragment;LX/0Xl;LX/18q;LX/03V;LX/0Sg;LX/18q;LX/0Uh;LX/0Yl;LX/0Ot;LX/18r;LX/0gh;LX/0kv;LX/0Yv;LX/0jo;LX/0gp;LX/0gy;LX/0go;)V

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 110836
    iget-boolean v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->u:Z

    if-nez v0, :cond_0

    .line 110837
    iput-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->x:LX/0gz;

    .line 110838
    :goto_0
    return-void

    .line 110839
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->A:Landroid/widget/FrameLayout;

    if-nez v0, :cond_1

    .line 110840
    iput-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->x:LX/0gz;

    goto :goto_0

    .line 110841
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->w:LX/0gy;

    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->l:Lcom/facebook/apptab/state/TabTag;

    iget-object v1, v1, Lcom/facebook/apptab/state/TabTag;->b:LX/0cQ;

    invoke-virtual {v0, v1}, LX/0gy;->b(LX/0cQ;)LX/0gz;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->x:LX/0gz;

    .line 110842
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->x:LX/0gz;

    invoke-virtual {v0}, LX/0gz;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110843
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 110844
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->x:LX/0gz;

    invoke-virtual {v1}, LX/0gz;->g()I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 110845
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 110846
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->A:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->x:LX/0gz;

    .line 110847
    iget p0, v1, LX/0gz;->e:I

    move v1, p0

    .line 110848
    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    goto :goto_0

    .line 110849
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->A:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public static o(Lcom/facebook/katana/fragment/FbChromeFragment;)Z
    .locals 1

    .prologue
    .line 110850
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/0f7;

    .line 110851
    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, LX/0f7;->a(Lcom/facebook/base/fragment/FbFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/katana/fragment/FbChromeFragment;)V
    .locals 2

    .prologue
    .line 110852
    iget v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 110853
    invoke-virtual {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 110854
    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->o(Lcom/facebook/katana/fragment/FbChromeFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110855
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->v:LX/0gp;

    .line 110856
    if-eqz v0, :cond_0

    iget-object p0, v1, LX/0gp;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    if-ne v0, p0, :cond_1

    .line 110857
    :cond_0
    :goto_0
    return-void

    .line 110858
    :cond_1
    new-instance p0, Ljava/lang/ref/WeakReference;

    invoke-direct {p0, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p0, v1, LX/0gp;->b:Ljava/lang/ref/WeakReference;

    .line 110859
    iget-object p0, v1, LX/0gp;->i:LX/0nv;

    invoke-interface {p0, v0}, LX/0nv;->a(Landroid/support/v4/app/Fragment;)LX/0nx;

    move-result-object p0

    .line 110860
    if-eqz p0, :cond_2

    .line 110861
    invoke-virtual {p0, v0, v1}, LX/0nx;->a(Landroid/support/v4/app/Fragment;LX/0gp;)V

    .line 110862
    :cond_2
    move-object p0, p0

    .line 110863
    iput-object p0, v1, LX/0gp;->c:LX/0nx;

    goto :goto_0
.end method

.method private q()V
    .locals 5

    .prologue
    .line 110929
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->e:LX/0Sg;

    if-nez v0, :cond_0

    .line 110930
    :goto_0
    return-void

    .line 110931
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->e:LX/0Sg;

    const-string v1, "FbChromeFragment Preload"

    new-instance v2, Lcom/facebook/katana/fragment/FbChromeFragment$3;

    invoke-direct {v2, p0}, Lcom/facebook/katana/fragment/FbChromeFragment$3;-><init>(Lcom/facebook/katana/fragment/FbChromeFragment;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    sget-object v4, LX/0Vm;->UI:LX/0Vm;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->k:LX/0Va;

    goto :goto_0
.end method

.method public static r(Lcom/facebook/katana/fragment/FbChromeFragment;)LX/0f3;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 110864
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/0f3;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 110865
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 110866
    if-eqz p1, :cond_0

    .line 110867
    const-string v0, "fbchromefragment_eventlog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 110868
    if-eqz v0, :cond_0

    .line 110869
    iget-object v2, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 110870
    iput-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->z:Ljava/util/ArrayList;

    .line 110871
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 110872
    const-string v0, "tab_root_intent"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->m:Landroid/content/Intent;

    .line 110873
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/katana/fragment/FbChromeFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 110874
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->y:LX/0go;

    const-string v3, "current_tab_name_in_focus"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0go;->a(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->l:Lcom/facebook/apptab/state/TabTag;
    :try_end_0
    .catch LX/0iF; {:try_start_0 .. :try_end_0} :catch_0

    .line 110875
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->w:LX/0gy;

    iget-object v2, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->l:Lcom/facebook/apptab/state/TabTag;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->b:LX/0cQ;

    invoke-virtual {v0, v2}, LX/0gy;->a(LX/0cQ;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->u:Z

    .line 110876
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->n:LX/18r;

    invoke-virtual {v0, p0, v1}, LX/18r;->a(Ljava/lang/Object;I)V

    .line 110877
    if-eqz p1, :cond_1

    .line 110878
    const-string v0, "has_root_fragment_inited"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->s:Z

    .line 110879
    iget-object v2, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->n:LX/18r;

    iget-boolean v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->s:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, p0, v0}, LX/18r;->a(Ljava/lang/Object;I)V

    .line 110880
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->a:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.apptab.ui.TAB_BAR_ITEM_TAP"

    new-instance v2, LX/18s;

    invoke-direct {v2, p0}, LX/18s;-><init>(Lcom/facebook/katana/fragment/FbChromeFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.apptab.ui.MAINTAB_CHROME_DRAWN"

    new-instance v2, LX/18t;

    invoke-direct {v2, p0}, LX/18t;-><init>(Lcom/facebook/katana/fragment/FbChromeFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->c:LX/0Yb;

    .line 110881
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->c:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 110882
    return-void

    .line 110883
    :catch_0
    move-exception v0

    .line 110884
    sget-object v2, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    iput-object v2, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->l:Lcom/facebook/apptab/state/TabTag;

    .line 110885
    iget-object v2, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->d:LX/03V;

    const-string v3, "tab manager"

    invoke-virtual {v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 110886
    goto :goto_1
.end method

.method public final a(Lcom/facebook/apptab/state/TabTag;LX/0hs;)V
    .locals 1

    .prologue
    .line 110887
    invoke-static {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->o(Lcom/facebook/katana/fragment/FbChromeFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110888
    invoke-static {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->r(Lcom/facebook/katana/fragment/FbChromeFragment;)LX/0f3;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0f3;->a(Lcom/facebook/apptab/state/TabTag;LX/0hs;)V

    .line 110889
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    .line 110890
    iget-boolean v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->s:Z

    if-eqz v0, :cond_1

    .line 110891
    :cond_0
    :goto_0
    return-void

    .line 110892
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->t:Z

    if-eqz v0, :cond_0

    .line 110893
    iget v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->j:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    if-eqz p1, :cond_0

    .line 110894
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->m:Landroid/content/Intent;

    const-string v1, "target_fragment"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 110895
    if-eq v1, v3, :cond_6

    move v0, v2

    :goto_1
    const-string v3, "Cannot load fragment, type not specified"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 110896
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    .line 110897
    invoke-virtual {v3}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110898
    invoke-direct {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->e()V

    .line 110899
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->q:LX/0Yv;

    iget-object v4, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->m:Landroid/content/Intent;

    .line 110900
    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 110901
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Landroid/os/Bundle;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 110902
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->r:LX/0jo;

    invoke-interface {v0, v1}, LX/0jo;->a(I)LX/0jq;

    move-result-object v1

    .line 110903
    const-class v0, LX/0jp;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 110904
    check-cast v0, LX/0jp;

    iget-object v4, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->i:LX/0Ot;

    invoke-interface {v0, v4}, LX/0jp;->a(LX/0Ot;)V

    .line 110905
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->m:Landroid/content/Intent;

    invoke-interface {v1, v0}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 110906
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 110907
    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 110908
    const v4, 0x7f0d002f

    invoke-virtual {v1, v4, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 110909
    iget-boolean v4, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->u:Z

    if-eqz v4, :cond_5

    .line 110910
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/facebook/tablet/sideshow/SideshowHost;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 110911
    const v5, 0x7f0d11ef

    invoke-virtual {v1, v5, v4}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 110912
    :cond_5
    invoke-direct {p0, v1}, Lcom/facebook/katana/fragment/FbChromeFragment;->a(LX/0hH;)V

    .line 110913
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 110914
    invoke-virtual {v3}, LX/0gc;->b()Z

    .line 110915
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->n:LX/18r;

    const/4 v3, 0x1

    invoke-virtual {v1, p0, v3}, LX/18r;->a(Ljava/lang/Object;I)V

    .line 110916
    invoke-static {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->o(Lcom/facebook/katana/fragment/FbChromeFragment;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 110917
    :goto_3
    iput-boolean v2, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->s:Z

    goto/16 :goto_0

    .line 110918
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 110919
    :cond_7
    iget-object v6, v0, LX/0Yv;->a:LX/0Yw;

    const-string p1, "IntentAction: (%s)"

    invoke-virtual {v5}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0Yw;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 110920
    :cond_8
    invoke-static {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->p(Lcom/facebook/katana/fragment/FbChromeFragment;)V

    .line 110921
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->a:LX/0Xl;

    const-string v3, "broadcast_after_fragment_pushed_in_current_tab"

    invoke-interface {v1, v3}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 110922
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->p:LX/0kv;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0kv;->a(Landroid/content/Context;)V

    .line 110923
    instance-of v1, v0, LX/0fh;

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 110924
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->o:LX/0gh;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, LX/0gh;->a(Landroid/support/v4/app/Fragment;Landroid/content/Context;)V

    .line 110925
    :cond_9
    goto :goto_3
.end method

.method public final c()Landroid/view/View;
    .locals 1

    .prologue
    .line 110825
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 110826
    return-object v0
.end method

.method public final d()Landroid/support/v4/app/Fragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 110926
    iget-boolean v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->s:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 110927
    :cond_0
    const/4 v0, 0x0

    .line 110928
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110821
    invoke-virtual {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 110822
    instance-of v1, v0, LX/0fk;

    if-nez v1, :cond_0

    .line 110823
    const/4 v0, 0x0

    .line 110824
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/0fk;

    invoke-interface {v0}, LX/0f6;->getDebugInfo()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()Lcom/facebook/apptab/state/TabTag;
    .locals 1

    .prologue
    .line 110820
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->l:Lcom/facebook/apptab/state/TabTag;

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x49f4dc0e    # 2005889.8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 110811
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 110812
    if-eqz p1, :cond_0

    .line 110813
    const/16 v1, 0x2b

    const v2, 0x6cc86334

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 110814
    :goto_0
    return-void

    .line 110815
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->l:Lcom/facebook/apptab/state/TabTag;

    sget-object v2, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    if-ne v1, v2, :cond_2

    .line 110816
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/katana/fragment/FbChromeFragment;->a(Z)V

    .line 110817
    :cond_1
    :goto_1
    const v1, 0x726f9501

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0

    .line 110818
    :cond_2
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->l:Lcom/facebook/apptab/state/TabTag;

    instance-of v1, v1, Lcom/facebook/notifications/tab/NotificationsTab;

    if-eqz v1, :cond_1

    .line 110819
    invoke-direct {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->q()V

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 110806
    invoke-virtual {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 110807
    if-eqz v0, :cond_0

    .line 110808
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 110809
    :goto_0
    return-void

    .line 110810
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->d:LX/03V;

    const-string v1, "fragment coordinator"

    const-string v2, "Current fragment is null onActivityResult() -- maybe failed to restore fragment from saved state?"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 110803
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 110804
    invoke-direct {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->e()V

    .line 110805
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x4ea0b4c0    # 1.3481001E9f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 110793
    iget-boolean v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->u:Z

    if-eqz v0, :cond_1

    const v0, 0x7f030a67

    .line 110794
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 110795
    iget-boolean v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->u:Z

    if-eqz v0, :cond_0

    .line 110796
    const v0, 0x7f0d11ef

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->A:Landroid/widget/FrameLayout;

    .line 110797
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->f:LX/18q;

    .line 110798
    iget-object p1, v0, LX/18q;->a:LX/0Xl;

    const-string p2, "com.facebook.apptab.ui.TAB_WAITING_FOR_DRAW"

    invoke-interface {p1, p2}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 110799
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/18w;->b(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setId(I)V

    .line 110800
    invoke-direct {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->e()V

    .line 110801
    const v0, 0x507c2123

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 110802
    :cond_1
    const v0, 0x7f030a68

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x50645397

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 110788
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 110789
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->c:LX/0Yb;

    if-eqz v1, :cond_0

    .line 110790
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->c:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 110791
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->c:LX/0Yb;

    .line 110792
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x64f10bbd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x19914138

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 110784
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 110785
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->A:Landroid/widget/FrameLayout;

    .line 110786
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->t:Z

    .line 110787
    const/16 v1, 0x2b

    const v2, -0x70ed927

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4177091a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 110772
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 110773
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 110774
    if-eqz v1, :cond_1

    .line 110775
    iget-object v2, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->v:LX/0gp;

    .line 110776
    iget-object p0, v2, LX/0gp;->i:LX/0nv;

    invoke-interface {p0, v1}, LX/0nv;->a(Landroid/support/v4/app/Fragment;)LX/0nx;

    move-result-object p0

    .line 110777
    if-eqz p0, :cond_0

    .line 110778
    const/4 v1, 0x0

    iput-object v1, p0, LX/0nx;->a:Landroid/view/View;

    .line 110779
    iget-object v1, p0, LX/0nx;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 110780
    iget-object v1, p0, LX/0nx;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 110781
    iget-object v1, p0, LX/0nx;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 110782
    :cond_0
    sget-object p0, LX/0gp;->a:Ljava/lang/ref/WeakReference;

    iput-object p0, v2, LX/0gp;->b:Ljava/lang/ref/WeakReference;

    .line 110783
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x2b72a531

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4f15e678

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 110767
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 110768
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->l:Lcom/facebook/apptab/state/TabTag;

    instance-of v1, v1, Lcom/facebook/notifications/tab/NotificationsTab;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->g:LX/0Uh;

    const/16 v2, 0xdf

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110769
    invoke-direct {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->q()V

    .line 110770
    :cond_0
    invoke-static {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->p(Lcom/facebook/katana/fragment/FbChromeFragment;)V

    .line 110771
    const/16 v1, 0x2b

    const v2, 0x507e7f5a

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 110763
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 110764
    const-string v0, "has_root_fragment_inited"

    iget-boolean v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->s:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 110765
    const-string v0, "fbchromefragment_eventlog"

    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->z:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 110766
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xb16d517

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 110758
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 110759
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->k:LX/0Va;

    if-eqz v1, :cond_0

    .line 110760
    iget-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->k:LX/0Va;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Va;->cancel(Z)Z

    .line 110761
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->k:LX/0Va;

    .line 110762
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x1e1a9458

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 110750
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 110751
    iget-boolean v0, p0, Lcom/facebook/katana/fragment/FbChromeFragment;->s:Z

    if-eqz v0, :cond_0

    .line 110752
    invoke-virtual {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 110753
    if-eqz v0, :cond_0

    .line 110754
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 110755
    if-eqz p1, :cond_0

    .line 110756
    invoke-static {p0}, Lcom/facebook/katana/fragment/FbChromeFragment;->p(Lcom/facebook/katana/fragment/FbChromeFragment;)V

    .line 110757
    :cond_0
    return-void
.end method
