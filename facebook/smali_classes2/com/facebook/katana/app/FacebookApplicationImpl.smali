.class public Lcom/facebook/katana/app/FacebookApplicationImpl;
.super Lcom/facebook/base/app/AbstractApplicationLike;
.source ""

# interfaces
.implements LX/02l;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field public static final i:Ljava/lang/String;


# instance fields
.field private final j:Landroid/content/Context;

.field public k:LX/0Pw;

.field private final l:J

.field private final m:LX/00q;

.field private final n:LX/005;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53954
    const-class v0, Lcom/facebook/katana/app/FacebookApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/app/FacebookApplicationImpl;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;LX/00H;JLX/00q;LX/005;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 53948
    invoke-direct {p0, p1, p2, p6}, Lcom/facebook/base/app/AbstractApplicationLike;-><init>(Landroid/app/Application;LX/00H;LX/005;)V

    .line 53949
    iput-wide p3, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->l:J

    .line 53950
    iput-object p5, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->m:LX/00q;

    .line 53951
    iput-object p1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Landroid/content/Context;

    .line 53952
    iput-object p6, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    .line 53953
    return-void
.end method

.method public static synthetic a(LX/0Yl;ILjava/lang/String;JJLjava/lang/Boolean;LX/00q;)V
    .locals 1

    .prologue
    .line 54068
    invoke-static/range {p0 .. p8}, Lcom/facebook/katana/app/FacebookApplicationImpl;->b(LX/0Yl;ILjava/lang/String;JJLjava/lang/Boolean;LX/00q;)V

    return-void
.end method

.method private a(Lcom/facebook/performancelogger/PerformanceLogger;LX/0Yi;)V
    .locals 13

    .prologue
    .line 54041
    const-string v0, "FacebookApplicationImpl.runPerfMarkers"

    const v1, 0x6eeef8d9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 54042
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->k:LX/0Pw;

    invoke-virtual {v0}, LX/0Pw;->getProcessName()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54043
    iget-wide v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->l:J

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->m:LX/00q;

    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->f()Z

    move-result v3

    .line 54044
    iput-wide v0, p2, LX/0Yi;->w:J

    .line 54045
    iput-boolean v3, p2, LX/0Yi;->x:Z

    .line 54046
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 54047
    invoke-static {p2}, LX/0Yi;->L(LX/0Yi;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 54048
    iget-object v4, p2, LX/0Yi;->I:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 54049
    invoke-static {}, LX/1gM;->loadIsNotColdStartRunMarker()V

    .line 54050
    :cond_0
    new-instance v4, LX/0Yj;

    const v5, 0xa0016

    const-string v6, "NNFColdStartTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 54051
    new-instance v11, LX/0Yj;

    const v5, 0xa004d

    const-string v6, "NNFColdStartNetwork"

    invoke-direct {v11, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 54052
    iget-object v5, p2, LX/0Yi;->k:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    iput-wide v6, p2, LX/0Yi;->D:J

    .line 54053
    iget-wide v6, p2, LX/0Yi;->D:J

    iput-wide v6, p2, LX/0Yi;->E:J

    .line 54054
    iget-object v5, p2, LX/0Yi;->j:LX/0Yl;

    .line 54055
    sget-object v6, LX/00w;->b:LX/00w;

    move-object v6, v6

    .line 54056
    invoke-virtual {v6}, LX/00w;->d()Z

    move-result v9

    const/4 v6, 0x7

    new-array v6, v6, [LX/0Yj;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const/4 v7, 0x1

    aput-object v11, v6, v7

    const/4 v7, 0x2

    new-instance v8, LX/0Yj;

    const v10, 0xa0020

    const-string v12, "NNFColdStartAndRenderTime"

    invoke-direct {v8, v10, v12}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v8, v6, v7

    const/4 v7, 0x3

    new-instance v8, LX/0Yj;

    const v10, 0xa003a

    const-string v12, "NNFFirstRunColdStart"

    invoke-direct {v8, v10, v12}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v8, v6, v7

    const/4 v7, 0x4

    new-instance v8, LX/0Yj;

    const v10, 0xa0001

    const-string v12, "NNFColdStart"

    invoke-direct {v8, v10, v12}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v8, v6, v7

    const/4 v7, 0x5

    new-instance v8, LX/0Yj;

    const v10, 0xa003c

    const-string v12, "NNFColdStartChromeLoadTime"

    invoke-direct {v8, v10, v12}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v8, v6, v7

    const/4 v7, 0x6

    new-instance v8, LX/0Yj;

    const v10, 0xa00a6

    const-string v12, "NNFInspirationCameraColdTTI"

    invoke-direct {v8, v10, v12}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v8, v6, v7

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    move-wide v6, v0

    move-object v8, v2

    invoke-virtual/range {v5 .. v10}, LX/0Yl;->a(JLX/00q;ZLjava/util/List;)LX/0Yl;

    .line 54057
    iget-object v5, p2, LX/0Yi;->p:LX/0Sk;

    if-eqz v5, :cond_1

    .line 54058
    iget-object v5, p2, LX/0Yi;->p:LX/0Sk;

    const/4 v6, 0x1

    const-string v7, "ColdStart"

    const/4 v8, 0x2

    new-array v8, v8, [LX/0Yj;

    const/4 v9, 0x0

    aput-object v4, v8, v9

    const/4 v4, 0x1

    aput-object v11, v8, v4

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v5, v6, v7, v4}, LX/0Sk;->a(ILjava/lang/String;Ljava/lang/Object;)V

    .line 54059
    :cond_1
    new-instance v0, LX/0Yj;

    const v1, 0xa0034

    const-string v2, "NNF_PermalinkFromAndroidNotificationColdLoad"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    iget-wide v2, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->l:J

    .line 54060
    iput-wide v2, v0, LX/0Yj;->g:J

    .line 54061
    move-object v0, v0

    .line 54062
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "story_view"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v3, v3, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yj;->b()LX/0Yj;

    move-result-object v0

    .line 54063
    sget-object v1, LX/00w;->b:LX/00w;

    move-object v1, v1

    .line 54064
    invoke-virtual {v1}, LX/00w;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/0Yj;->c(Z)LX/0Yj;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54065
    :cond_2
    const v0, -0x4638677f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 54066
    return-void

    .line 54067
    :catchall_0
    move-exception v0

    const v1, 0x3ca60ca3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/katana/app/FacebookApplicationImpl;LX/0Yl;)V
    .locals 2

    .prologue
    .line 54035
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    const-string v1, "ColdStart/AppInitialization"

    invoke-virtual {v0, v1}, LX/005;->b(Ljava/lang/String;)V

    .line 54036
    if-eqz p1, :cond_0

    .line 54037
    new-instance v0, LX/17H;

    invoke-direct {v0, p0, p1}, LX/17H;-><init>(Lcom/facebook/katana/app/FacebookApplicationImpl;LX/0Yl;)V

    .line 54038
    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    invoke-virtual {v1, v0}, LX/005;->a(LX/03v;)V

    .line 54039
    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    invoke-virtual {v1, v0}, LX/005;->b(LX/03v;)V

    .line 54040
    :cond_0
    return-void
.end method

.method private static b(LX/0Yl;ILjava/lang/String;JJLjava/lang/Boolean;LX/00q;)V
    .locals 11

    .prologue
    .line 54033
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-wide v6, p3

    move-object/from16 v8, p8

    move-object/from16 v9, p7

    invoke-virtual/range {v1 .. v9}, LX/0Yl;->a(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLX/00q;Ljava/lang/Boolean;)LX/0Yl;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v2, p1

    move-object v3, p2

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, LX/0Yl;->a(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)LX/0Yl;

    .line 54034
    return-void
.end method

.method public static e(Lcom/facebook/katana/app/FacebookApplicationImpl;)V
    .locals 2

    .prologue
    .line 54069
    const-string v0, "FacebookApplicationImpl.initializeUDPPriming"

    const v1, 0x6f95d5a1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 54070
    :try_start_0
    sget-object v0, LX/00c;->a:LX/00c;

    move-object v0, v0

    .line 54071
    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/00c;->a(Landroid/content/Context;)Z

    .line 54072
    sget-object v0, LX/00a;->a:LX/00a;

    move-object v0, v0

    .line 54073
    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/00a;->a(Landroid/content/Context;)Z

    .line 54074
    sget-object v0, LX/00d;->a:LX/00d;

    move-object v0, v0

    .line 54075
    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/00d;->a(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54076
    const v0, -0x416aadd2

    invoke-static {v0}, LX/02m;->a(I)V

    .line 54077
    return-void

    .line 54078
    :catchall_0
    move-exception v0

    const v1, -0x3f90dcf3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 54028
    invoke-static {}, LX/008;->getMainDexStoreLoadInformation()LX/02X;

    move-result-object v0

    .line 54029
    if-eqz v0, :cond_0

    iget v0, v0, LX/02X;->loadResult:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 54030
    :goto_0
    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->k:LX/0Pw;

    invoke-virtual {v1}, LX/0Pw;->getProcessName()LX/00G;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 54031
    return v0

    .line 54032
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 5

    .prologue
    .line 54009
    const-string v0, "FacebookApplicationImpl.startStrictMode"

    const v1, 0x4bcd61af    # 2.6919774E7f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 54010
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->a()LX/0QA;

    move-result-object v1

    .line 54011
    invoke-static {v1}, LX/0XV;->b(LX/0QB;)LX/01U;

    move-result-object v0

    check-cast v0, LX/01U;

    .line 54012
    if-eqz v0, :cond_0

    sget-object v2, LX/01U;->DEBUG:LX/01U;

    if-ne v0, v2, :cond_0

    .line 54013
    new-instance v2, LX/J8J;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {v1}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v1

    check-cast v1, Ljava/util/Random;

    invoke-direct {v2, v0, v1}, LX/J8J;-><init>(LX/03V;Ljava/util/Random;)V

    .line 54014
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 54015
    sget-boolean v3, LX/J8J;->b:Z

    if-nez v3, :cond_1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54016
    :cond_0
    :goto_0
    const v0, -0x36aa365a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 54017
    return-void

    .line 54018
    :catchall_0
    move-exception v0

    const v1, 0x5069fd38

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 54019
    :cond_1
    :try_start_1
    sget-boolean v3, LX/J8J;->c:Z

    if-eqz v3, :cond_2

    .line 54020
    goto :goto_0

    .line 54021
    :cond_2
    const/4 v3, 0x2

    new-array v3, v3, [LX/J8M;

    sget-object v4, LX/J8O;->DetectAll:LX/J8O;

    aput-object v4, v3, v0

    sget-object v4, LX/J8O;->PenaltyLog:LX/J8O;

    aput-object v4, v3, v1

    .line 54022
    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v4}, LX/J8K;->a(Ljava/util/List;)V

    .line 54023
    new-instance v3, LX/J8I;

    iget-object v4, v2, LX/J8J;->d:LX/03V;

    iget-object p0, v2, LX/J8J;->e:Ljava/util/Random;

    invoke-direct {v3, v4, p0}, LX/J8I;-><init>(LX/03V;Ljava/util/Random;)V

    .line 54024
    const/4 v4, 0x1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v4, v3}, LX/J8G;->a(ZLX/J8I;)V

    .line 54025
    const/4 v3, 0x1

    sput-boolean v3, LX/J8J;->c:Z
    :try_end_2
    .catch LX/J8L; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 54026
    goto :goto_0

    .line 54027
    :catch_0
    const-class v1, LX/J8J;

    const-string v3, "Failed to start StrictModeAggregator"

    invoke-static {v1, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/00G;)V
    .locals 1

    .prologue
    .line 54007
    invoke-static {p1}, LX/0Pw;->convertProcessNameToProcessEnum(LX/00G;)LX/0Pw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->k:LX/0Pw;

    .line 54008
    return-void
.end method

.method public final b()V
    .locals 10

    .prologue
    const v2, 0x4e0004

    const/4 v9, 0x0

    .line 53964
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    const-string v1, "ColdStart/FBAppImpl.onCreate"

    invoke-virtual {v0, v1}, LX/005;->b(Ljava/lang/String;)V

    .line 53965
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 53966
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    const-string v1, "ColdStart/FacebookApplicationImpl.onCreate"

    const v3, 0x700011

    invoke-virtual {v0, v1, v3}, LX/006;->a(Ljava/lang/String;I)LX/00X;

    .line 53967
    const-string v0, "FacebookApplicationImpl.onCreate"

    const v1, -0x193c6f0a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 53968
    :try_start_0
    const-string v0, "TraceOrchestrator.initialize"

    const v1, -0x60595387

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 53969
    :try_start_1
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Landroid/content/Context;

    invoke-static {v0}, LX/02n;->a(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 53970
    const v0, -0x27ccd49a

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 53971
    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53972
    new-instance v0, Lcom/facebook/katana/app/FacebookApplicationImpl$1;

    invoke-direct {v0, p0}, Lcom/facebook/katana/app/FacebookApplicationImpl$1;-><init>(Lcom/facebook/katana/app/FacebookApplicationImpl;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 53973
    :cond_0
    const-string v0, "ReportAProblem.setReportAProblemConfigProvider"

    const v1, -0x200af8a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 53974
    :try_start_3
    new-instance v0, LX/0Pt;

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/0Pt;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, LX/0Pu;->a(LX/0Pt;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 53975
    const v0, -0x4a6d3904

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V

    .line 53976
    const-string v0, "FacebookApplicationImpl.super.onCreate"

    const v1, 0x694b2a9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 53977
    :try_start_5
    invoke-super {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 53978
    const v0, -0x660cac23

    :try_start_6
    invoke-static {v0}, LX/02m;->a(I)V

    .line 53979
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v0

    new-instance v1, LX/0XU;

    invoke-direct {v1}, LX/0XU;-><init>()V

    invoke-virtual {v0, v1}, LX/009;->setLogBridge(LX/03d;)V

    .line 53980
    invoke-virtual {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->a()LX/0QA;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v3

    .line 53981
    :try_start_7
    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->g()V

    .line 53982
    invoke-static {v3}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v3}, LX/0Yi;->a(LX/0QB;)LX/0Yi;

    move-result-object v1

    check-cast v1, LX/0Yi;

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a(Lcom/facebook/performancelogger/PerformanceLogger;LX/0Yi;)V

    .line 53983
    invoke-static {v3}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v0

    check-cast v0, LX/0Uq;

    .line 53984
    new-instance v1, LX/0cS;

    invoke-direct {v1, p0, v3}, LX/0cS;-><init>(Lcom/facebook/katana/app/FacebookApplicationImpl;LX/0QA;)V

    invoke-virtual {v0, v1}, LX/0Uq;->a(LX/0V0;)V

    .line 53985
    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    const-string v6, "ColdStart/UIThreadBlockedOnAppInit"

    const v7, 0x700002

    invoke-virtual {v1, v6, v7}, LX/005;->c(Ljava/lang/String;I)V

    .line 53986
    invoke-virtual {v0}, LX/0Uq;->b()V

    .line 53987
    invoke-static {}, Lcom/facebook/common/dextricks/DalvikInternals;->onColdStartEnd()V

    .line 53988
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    const-string v1, "ColdStart/UIThreadBlockedOnAppInit"

    invoke-virtual {v0, v1}, LX/005;->b(Ljava/lang/String;)V

    .line 53989
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    const-string v1, "ColdStart/ApplicationCreate"

    invoke-virtual {v0, v1}, LX/005;->b(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 53990
    const v0, -0x70b3df4f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 53991
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    const-string v1, "ColdStart/FacebookApplicationImpl.onCreate"

    invoke-virtual {v0, v1}, LX/005;->a(Ljava/lang/String;)V

    .line 53992
    if-eqz v3, :cond_1

    .line 53993
    invoke-static {v3}, LX/0Yl;->b(LX/0QB;)LX/0Yl;

    move-result-object v1

    check-cast v1, LX/0Yl;

    const-string v3, "ApplicationOnCreate"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 53994
    sget-object v0, LX/00w;->b:LX/00w;

    move-object v0, v0

    .line 53995
    invoke-virtual {v0}, LX/00w;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static/range {v1 .. v9}, Lcom/facebook/katana/app/FacebookApplicationImpl;->b(LX/0Yl;ILjava/lang/String;JJLjava/lang/Boolean;LX/00q;)V

    .line 53996
    :cond_1
    return-void

    .line 53997
    :catchall_0
    move-exception v0

    const v1, -0x634a15f7

    :try_start_8
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 53998
    :catchall_1
    move-exception v0

    move-object v1, v9

    :goto_0
    const v3, -0x56958a13

    invoke-static {v3}, LX/02m;->a(I)V

    .line 53999
    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    const-string v6, "ColdStart/FacebookApplicationImpl.onCreate"

    invoke-virtual {v3, v6}, LX/005;->a(Ljava/lang/String;)V

    .line 54000
    if-eqz v1, :cond_2

    .line 54001
    invoke-static {v1}, LX/0Yl;->b(LX/0QB;)LX/0Yl;

    move-result-object v1

    check-cast v1, LX/0Yl;

    const-string v3, "ApplicationOnCreate"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 54002
    sget-object v8, LX/00w;->b:LX/00w;

    move-object v8, v8

    .line 54003
    invoke-virtual {v8}, LX/00w;->d()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static/range {v1 .. v9}, Lcom/facebook/katana/app/FacebookApplicationImpl;->b(LX/0Yl;ILjava/lang/String;JJLjava/lang/Boolean;LX/00q;)V

    :cond_2
    throw v0

    .line 54004
    :catchall_2
    move-exception v0

    const v1, -0x7a45f51f

    :try_start_9
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 54005
    :catchall_3
    move-exception v0

    const v1, -0x484c37af

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 54006
    :catchall_4
    move-exception v0

    move-object v1, v3

    goto :goto_0
.end method

.method public final b(LX/00G;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 53955
    invoke-virtual {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->a()LX/0QA;

    move-result-object v1

    .line 53956
    invoke-static {v1}, LX/0Un;->a(LX/0QB;)LX/0Un;

    move-result-object v0

    check-cast v0, LX/0Un;

    .line 53957
    invoke-virtual {p1}, LX/00G;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 53958
    const-class v1, Lcom/facebook/contacts/background/ContactsTaskTag;

    invoke-virtual {v0, v1, v3}, LX/0Un;->a(Ljava/lang/Class;Z)V

    .line 53959
    const-class v1, Lcom/facebook/messaging/background/annotations/MessagesDataTaskTag;

    invoke-virtual {v0, v1, v3}, LX/0Un;->a(Ljava/lang/Class;Z)V

    .line 53960
    const-class v1, Lcom/facebook/messaging/background/annotations/MessagesLocalTaskTag;

    invoke-virtual {v0, v1, v3}, LX/0Un;->a(Ljava/lang/Class;Z)V

    .line 53961
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->n:LX/005;

    const-string v1, "ColdStart/AppInitialization"

    const v2, 0x700008

    invoke-virtual {v0, v1, v2}, LX/005;->c(Ljava/lang/String;I)V

    .line 53962
    return-void

    .line 53963
    :cond_0
    invoke-static {v1}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v0

    check-cast v0, LX/0Uo;

    iget-wide v2, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->l:J

    invoke-virtual {v0, v2, v3}, LX/0Uo;->a(J)V

    goto :goto_0
.end method
