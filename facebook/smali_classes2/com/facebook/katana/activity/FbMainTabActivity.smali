.class public Lcom/facebook/katana/activity/FbMainTabActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f1;
.implements LX/0f3;
.implements LX/03p;
.implements LX/0f4;
.implements LX/0f5;
.implements LX/0f7;
.implements LX/0f8;
.implements LX/0f9;
.implements LX/0fA;
.implements LX/0fB;
.implements LX/0fC;
.implements LX/0fD;
.implements LX/0fE;
.implements LX/0fF;
.implements LX/0fG;
.implements LX/0fH;
.implements LX/0fI;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation

.annotation build Lcom/facebook/navigation/annotations/NavigationComponent$Root;
.end annotation


# static fields
.field private static final R:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/katana/activity/FbMainTabActivity;",
            ">;"
        }
    .end annotation
.end field

.field private static final S:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static T:Z


# instance fields
.field public volatile A:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public volatile B:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0fW;",
            ">;"
        }
    .end annotation
.end field

.field public volatile C:LX/0Or;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public volatile D:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public volatile E:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hg;",
            ">;"
        }
    .end annotation
.end field

.field public volatile F:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0he;",
            ">;"
        }
    .end annotation
.end field

.field public volatile G:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hy;",
            ">;"
        }
    .end annotation
.end field

.field public volatile H:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hI;",
            ">;"
        }
    .end annotation
.end field

.field public volatile I:LX/0Or;
    .annotation runtime Lcom/facebook/tablet/IsTablet;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public volatile J:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gw;",
            ">;"
        }
    .end annotation
.end field

.field public volatile K:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hx;",
            ">;"
        }
    .end annotation
.end field

.field public volatile L:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hw;",
            ">;"
        }
    .end annotation
.end field

.field public volatile M:LX/0Or;
    .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ">;"
        }
    .end annotation
.end field

.field public volatile N:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hv;",
            ">;"
        }
    .end annotation
.end field

.field public volatile O:LX/0gm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile P:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hq;",
            ">;"
        }
    .end annotation
.end field

.field public volatile Q:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0fJ;",
            ">;"
        }
    .end annotation
.end field

.field private final U:J

.field private final V:LX/00q;

.field private W:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private X:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hC;",
            ">;"
        }
    .end annotation
.end field

.field private Y:LX/0gn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private Z:LX/0go;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private aA:Z

.field private aB:Z

.field private aC:LX/0Yb;

.field private aD:LX/0h2;

.field public aE:LX/0gx;

.field private aF:LX/0gg;

.field public aG:LX/0fd;

.field private aH:Z

.field private aI:LX/0gf;

.field private aJ:Z

.field private aK:LX/0hr;

.field public aL:Z

.field private aM:Z

.field public aN:LX/0gs;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

.field public aP:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:LX/0gp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ab:LX/0fO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private ac:LX/0fU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private ad:Ljava/lang/Boolean;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation runtime Lcom/facebook/work/reducedinstance/bridge/IsNewsFeedDisabledForWork;
    .end annotation
.end field

.field private ae:LX/0W3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private af:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Yl;",
            ">;"
        }
    .end annotation
.end field

.field private ag:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Yi;",
            ">;"
        }
    .end annotation
.end field

.field private ah:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public ai:LX/0Ot;
    .annotation runtime Lcom/facebook/divebar/Left;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/divebar/DivebarController;",
            ">;"
        }
    .end annotation
.end field

.field private aj:LX/0Ot;
    .annotation runtime Lcom/facebook/divebar/Right;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/divebar/DivebarController;",
            ">;"
        }
    .end annotation
.end field

.field private ak:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field

.field private al:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fP;",
            ">;"
        }
    .end annotation
.end field

.field public am:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field

.field private an:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0i0;",
            ">;"
        }
    .end annotation
.end field

.field private ao:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jM;",
            ">;"
        }
    .end annotation
.end field

.field private ap:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jN;",
            ">;"
        }
    .end annotation
.end field

.field private aq:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ja;",
            ">;"
        }
    .end annotation
.end field

.field private ar:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private as:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hB;",
            ">;"
        }
    .end annotation
.end field

.field private at:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gJ;",
            ">;"
        }
    .end annotation
.end field

.field private au:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;",
            ">;"
        }
    .end annotation
.end field

.field private av:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0id;",
            ">;"
        }
    .end annotation
.end field

.field private aw:LX/03R;

.field private ax:LX/0gH;

.field private ay:Z

.field private az:Z

.field public volatile p:LX/0gj;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile q:LX/0gk;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile r:LX/0gl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile s:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hJ;",
            ">;"
        }
    .end annotation
.end field

.field public volatile t:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;"
        }
    .end annotation
.end field

.field public volatile u:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iS;",
            ">;"
        }
    .end annotation
.end field

.field public volatile v:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iX;",
            ">;"
        }
    .end annotation
.end field

.field public volatile w:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gI;",
            ">;"
        }
    .end annotation
.end field

.field public volatile x:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ii;",
            ">;"
        }
    .end annotation
.end field

.field public volatile y:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ie;",
            ">;"
        }
    .end annotation
.end field

.field public volatile z:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 105426
    const-class v0, Lcom/facebook/katana/activity/FbMainTabActivity;

    sput-object v0, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    .line 105427
    const-string v0, "chromeless:content:fragment:tag"

    const-string v1, "PhotoAnimationDialogFragment_MEDIA_GALLERY"

    const-string v2, "PhotoAnimationDialogFragment_PHOTOS_FEED"

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/activity/FbMainTabActivity;->S:LX/0Px;

    .line 105428
    const/4 v0, 0x0

    sput-boolean v0, Lcom/facebook/katana/activity/FbMainTabActivity;->T:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 105152
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 105153
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->U:J

    .line 105154
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105155
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    .line 105156
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105157
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ag:LX/0Ot;

    .line 105158
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105159
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ah:LX/0Ot;

    .line 105160
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105161
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    .line 105162
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105163
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    .line 105164
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105165
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ak:LX/0Ot;

    .line 105166
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105167
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->al:LX/0Ot;

    .line 105168
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105169
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->am:LX/0Ot;

    .line 105170
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105171
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->an:LX/0Ot;

    .line 105172
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105173
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ao:LX/0Ot;

    .line 105174
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105175
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ap:LX/0Ot;

    .line 105176
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105177
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aq:LX/0Ot;

    .line 105178
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105179
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ar:LX/0Ot;

    .line 105180
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105181
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->as:LX/0Ot;

    .line 105182
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105183
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->at:LX/0Ot;

    .line 105184
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105185
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->au:LX/0Ot;

    .line 105186
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 105187
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    .line 105188
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aw:LX/03R;

    .line 105189
    iput-boolean v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ay:Z

    .line 105190
    iput-boolean v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->az:Z

    .line 105191
    iput-boolean v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aA:Z

    .line 105192
    iput-boolean v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aB:Z

    .line 105193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aP:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    .line 105194
    invoke-static {}, LX/00q;->a()LX/00q;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->V:LX/00q;

    .line 105195
    return-void
.end method

.method private F()V
    .locals 3

    .prologue
    .line 105145
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 105146
    if-eqz v1, :cond_0

    .line 105147
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->Q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fJ;

    .line 105148
    invoke-interface {v0, v1}, LX/0fJ;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105149
    :cond_0
    :goto_0
    return-void

    .line 105150
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 105151
    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private G()Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;
    .locals 2

    .prologue
    .line 105140
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    .line 105141
    iget-object v1, v0, LX/0fK;->q:Lcom/facebook/ui/drawers/DrawerContentFragment;

    move-object v0, v1

    .line 105142
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    if-eqz v1, :cond_0

    .line 105143
    check-cast v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    .line 105144
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private H()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 105124
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105125
    invoke-static {}, Lcom/facebook/katana/activity/FbMainTabActivity;->ao()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 105126
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    .line 105127
    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, LX/0fK;->a(LX/0fK;Landroid/app/Activity;Z)V

    .line 105128
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ab:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105129
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0fK;->b_(Z)V

    .line 105130
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0, v2}, LX/0fK;->c_(Z)V

    .line 105131
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->g()V

    .line 105132
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->i()V

    .line 105133
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aB:Z

    if-nez v0, :cond_3

    .line 105134
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->al:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fP;

    invoke-virtual {v0}, LX/0fP;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105135
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->al:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fP;

    new-instance v1, LX/0fQ;

    invoke-direct {v1, p0}, LX/0fQ;-><init>(Lcom/facebook/katana/activity/FbMainTabActivity;)V

    invoke-virtual {v0, v1}, LX/0fP;->a(LX/0fR;)V

    .line 105136
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    new-instance v1, LX/0fS;

    invoke-direct {v1, p0}, LX/0fS;-><init>(Lcom/facebook/katana/activity/FbMainTabActivity;)V

    invoke-virtual {v0, v1}, LX/0fK;->a(LX/0fT;)V

    .line 105137
    iput-boolean v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aB:Z

    .line 105138
    :cond_3
    return-void

    .line 105139
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0, p0}, LX/0fK;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private I()V
    .locals 4

    .prologue
    .line 105107
    const-string v0, "FbMainTabActivity.onCreateSetupBroadcastReceiver"

    const v1, 0x246f8ad6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 105108
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ac:LX/0fU;

    .line 105109
    iget-object v1, v0, LX/0fU;->i:LX/0Yb;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/0fU;->i:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105110
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->C:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    .line 105111
    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    .line 105112
    const-string v1, "com.facebook.apptab.ui.TAB_WAITING_FOR_DRAW"

    new-instance v2, LX/0fV;

    invoke-direct {v2, p0}, LX/0fV;-><init>(Lcom/facebook/katana/activity/FbMainTabActivity;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    .line 105113
    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aC:LX/0Yb;

    .line 105114
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aC:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 105115
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->B:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fW;

    .line 105116
    iget-object v1, v0, LX/0fW;->g:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.http.protocol.AUTH_TOKEN_EXCEPTION"

    new-instance v3, LX/0fZ;

    invoke-direct {v3, v0}, LX/0fZ;-><init>(LX/0fW;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/0fW;->h:LX/0Yb;

    .line 105117
    iget-object v1, v0, LX/0fW;->h:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105118
    const v0, 0x177bb0b4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 105119
    return-void

    .line 105120
    :catchall_0
    move-exception v0

    const v1, 0x7e93d19b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 105121
    :cond_0
    iget-object v1, v0, LX/0fU;->g:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v3, LX/0fa;

    invoke-direct {v3, v0}, LX/0fa;-><init>(LX/0fU;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v3, LX/0fb;

    invoke-direct {v3, v0}, LX/0fb;-><init>(LX/0fU;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    .line 105122
    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/0fU;->i:LX/0Yb;

    .line 105123
    iget-object v1, v0, LX/0fU;->i:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    goto :goto_0
.end method

.method private J()V
    .locals 5

    .prologue
    .line 105095
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ac:LX/0fU;

    const-class v1, Lcom/facebook/katana/activity/FbMainTabActivity;

    .line 105096
    const/4 v2, 0x0

    .line 105097
    iget-object v3, v0, LX/0fU;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0fc;

    .line 105098
    invoke-virtual {v2}, LX/0fc;->b()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 105099
    add-int/lit8 v2, v3, 0x1

    :goto_1
    move v3, v2

    .line 105100
    goto :goto_0

    .line 105101
    :cond_0
    move v1, v3

    .line 105102
    sget-boolean v0, Lcom/facebook/katana/activity/FbMainTabActivity;->T:Z

    if-nez v0, :cond_1

    const/4 v0, 0x2

    if-le v1, v0, :cond_1

    .line 105103
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->A:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 105104
    sget-object v2, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "multiple FbMainTabActivities: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 105105
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/katana/activity/FbMainTabActivity;->T:Z

    .line 105106
    :cond_1
    return-void

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public static K(Lcom/facebook/katana/activity/FbMainTabActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 105088
    iget-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ay:Z

    if-eqz v0, :cond_0

    .line 105089
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->M()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105090
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0, v1}, LX/0fK;->a_(Z)V

    .line 105091
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ac:LX/0fU;

    invoke-virtual {v0, v1}, LX/0fU;->a(Z)J

    .line 105092
    :cond_0
    :goto_0
    return-void

    .line 105093
    :cond_1
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105094
    invoke-static {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->U(Lcom/facebook/katana/activity/FbMainTabActivity;)V

    goto :goto_0
.end method

.method private L()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 105064
    sget-object v0, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105065
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    .line 105066
    iget-object v1, v0, LX/0fd;->g:LX/03R;

    move-object v0, v1

    .line 105067
    invoke-virtual {v0, v4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    .line 105068
    :goto_0
    return v0

    .line 105069
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->P()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105070
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    sget-object v1, LX/03R;->YES:LX/03R;

    .line 105071
    iput-object v1, v0, LX/0fd;->g:LX/03R;

    .line 105072
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    .line 105073
    iget-object v1, v0, LX/0fd;->g:LX/03R;

    move-object v0, v1

    .line 105074
    invoke-virtual {v0, v4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0

    .line 105075
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ac:LX/0fU;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0fU;->a(Z)J

    move-result-wide v2

    .line 105076
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->N()Z

    move-result v0

    if-nez v0, :cond_3

    .line 105077
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    .line 105078
    iget-object v1, v0, LX/0fd;->g:LX/03R;

    move-object v0, v1

    .line 105079
    invoke-virtual {v0, v4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0

    .line 105080
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ak:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long/2addr v0, v2

    .line 105081
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->R()J

    move-result-wide v2

    .line 105082
    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    .line 105083
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    sget-object v1, LX/03R;->YES:LX/03R;

    .line 105084
    iput-object v1, v0, LX/0fd;->g:LX/03R;

    .line 105085
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    .line 105086
    iget-object v1, v0, LX/0fd;->g:LX/03R;

    move-object v0, v1

    .line 105087
    invoke-virtual {v0, v4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0
.end method

.method private M()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 105057
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->Y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105058
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ac:LX/0fU;

    invoke-virtual {v0, v1}, LX/0fU;->a(Z)J

    move-result-wide v2

    .line 105059
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    move v0, v1

    .line 105060
    :goto_0
    return v0

    .line 105061
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ak:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 105062
    const-wide/32 v4, 0x1d4c0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 105063
    goto :goto_0
.end method

.method private N()Z
    .locals 1

    .prologue
    .line 105056
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private O()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 105048
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget-char v2, LX/0fe;->e:C

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 105049
    if-nez v1, :cond_1

    .line 105050
    :cond_0
    :goto_0
    return v0

    .line 105051
    :cond_1
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 105052
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 105053
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->s()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 105054
    const/4 v0, 0x1

    goto :goto_0

    .line 105055
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private P()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 105044
    iget-boolean v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aA:Z

    if-nez v1, :cond_1

    .line 105045
    :cond_0
    :goto_0
    return v0

    .line 105046
    :cond_1
    sget-object v1, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-virtual {v1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105047
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Q()Z
    .locals 3

    .prologue
    .line 105043
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget-short v1, LX/0fe;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method private R()J
    .locals 4

    .prologue
    .line 104947
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget v1, LX/0fe;->c:I

    const/16 v2, 0x384

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method private S()V
    .locals 2

    .prologue
    .line 105038
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aw:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105039
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->T()V

    .line 105040
    :cond_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aw:LX/03R;

    .line 105041
    return-void
.end method

.method private T()V
    .locals 2

    .prologue
    .line 105027
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105028
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->X()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    .line 105029
    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z

    move-result v1

    if-nez v1, :cond_1

    .line 105030
    :cond_0
    :goto_0
    return-void

    .line 105031
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v1

    invoke-virtual {v1}, LX/0fz;->e()Z

    move-result v1

    if-nez v1, :cond_2

    .line 105032
    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v1

    invoke-virtual {v1}, LX/0fz;->g()V

    .line 105033
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v1

    invoke-virtual {v1}, LX/0fz;->h()V

    .line 105034
    iget-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v1}, LX/0g2;->C()V

    .line 105035
    iget-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, LX/0g7;->g(I)V

    .line 105036
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->r:Z

    .line 105037
    iget-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    new-instance p0, LX/0gB;

    invoke-direct {p0, v0}, LX/0gB;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    invoke-virtual {v1, p0}, LX/0g7;->b(LX/0fu;)V

    goto :goto_0
.end method

.method public static U(Lcom/facebook/katana/activity/FbMainTabActivity;)V
    .locals 0

    .prologue
    .line 105023
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ai()V

    .line 105024
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->r()V

    .line 105025
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->T()V

    .line 105026
    return-void
.end method

.method private V()Z
    .locals 1

    .prologue
    .line 105022
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private W()V
    .locals 1

    .prologue
    .line 105010
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105011
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->X()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    .line 105012
    iget-object p0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    if-eqz p0, :cond_0

    .line 105013
    iget-object p0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 105014
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    if-nez v0, :cond_1

    .line 105015
    :cond_0
    :goto_0
    return-void

    .line 105016
    :cond_1
    iget-object v0, p0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->m()V

    .line 105017
    invoke-virtual {p0}, LX/0g2;->a()V

    .line 105018
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    instance-of v0, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    if-eqz v0, :cond_2

    .line 105019
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    check-cast v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    .line 105020
    invoke-virtual {v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->P()V

    goto :goto_0

    .line 105021
    :cond_2
    iget-object v0, p0, LX/0g2;->b:LX/0gC;

    invoke-interface {v0}, LX/0gC;->l()Z

    goto :goto_0
.end method

.method private X()Lcom/facebook/feed/fragment/NewsFeedFragment;
    .locals 2

    .prologue
    .line 104999
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105000
    const/4 v0, 0x0

    .line 105001
    :goto_0
    return-object v0

    .line 105002
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    .line 105003
    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/feed/fragment/NewsFeedFragment;

    if-eqz v1, :cond_1

    .line 105004
    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/fragment/NewsFeedFragment;

    goto :goto_0

    .line 105005
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    .line 105006
    invoke-virtual {v0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d()Z

    move-result v1

    if-nez v1, :cond_2

    .line 105007
    const/4 v1, 0x0

    .line 105008
    :goto_1
    move-object v0, v1

    .line 105009
    goto :goto_0

    :cond_2
    iget-object v1, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, LX/0gE;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/fragment/NewsFeedFragment;

    goto :goto_1
.end method

.method private Y()Z
    .locals 2

    .prologue
    .line 104998
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ax:LX/0gH;

    sget-object v1, LX/0gH;->OPEN_TO_LEFT_DIVEBAR:LX/0gH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Z()V
    .locals 4

    .prologue
    .line 104968
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ab:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104969
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104970
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->w:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gI;

    .line 104971
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0gI;->a(Z)V

    .line 104972
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->at:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gJ;

    .line 104973
    iget-object v1, v0, LX/0gJ;->e:LX/0gK;

    invoke-virtual {v1}, LX/0gK;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 104974
    iget-object v1, v0, LX/0gJ;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gL;

    .line 104975
    iget-object v2, v1, LX/0gL;->g:LX/0gM;

    if-eqz v2, :cond_3

    .line 104976
    :cond_1
    :goto_0
    return-void

    .line 104977
    :cond_2
    iget-object v1, v0, LX/0gJ;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gN;

    .line 104978
    iget-object v2, v1, LX/0gN;->f:LX/0gM;

    if-eqz v2, :cond_4

    .line 104979
    :goto_1
    goto :goto_0

    .line 104980
    :cond_3
    new-instance v3, LX/0gO;

    invoke-direct {v3}, LX/0gO;-><init>()V

    .line 104981
    new-instance v2, LX/0gU;

    invoke-direct {v2}, LX/0gU;-><init>()V

    move-object p0, v2

    .line 104982
    iget-object v2, p0, LX/0gW;->h:Ljava/lang/String;

    move-object v2, v2

    .line 104983
    const-string v0, "client_subscription_id"

    invoke-virtual {v3, v0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104984
    iget-object v2, v1, LX/0gL;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 104985
    const-string v0, "viewer_id"

    invoke-virtual {v3, v0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104986
    const-string v2, "data"

    invoke-virtual {p0, v2, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 104987
    :try_start_0
    iget-object v2, v1, LX/0gL;->d:LX/0gX;

    new-instance v3, LX/0gY;

    invoke-direct {v3, v1}, LX/0gY;-><init>(LX/0gL;)V

    invoke-virtual {v2, p0, v3}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v2

    iput-object v2, v1, LX/0gL;->g:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 104988
    :catch_0
    const/4 v2, 0x0

    iput-object v2, v1, LX/0gL;->g:LX/0gM;

    goto :goto_0

    .line 104989
    :cond_4
    new-instance v2, LX/0gZ;

    invoke-direct {v2}, LX/0gZ;-><init>()V

    .line 104990
    new-instance v3, LX/0ga;

    invoke-direct {v3}, LX/0ga;-><init>()V

    move-object v3, v3

    .line 104991
    iget-object p0, v3, LX/0gW;->h:Ljava/lang/String;

    move-object p0, p0

    .line 104992
    const-string v0, "client_subscription_id"

    invoke-virtual {v2, v0, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104993
    iget-object p0, v1, LX/0gN;->e:Ljava/lang/String;

    .line 104994
    const-string v0, "user_id"

    invoke-virtual {v2, v0, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104995
    const-string p0, "data"

    invoke-virtual {v3, p0, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 104996
    :try_start_1
    iget-object v2, v1, LX/0gN;->d:LX/0gX;

    new-instance p0, LX/0gb;

    invoke-direct {p0, v1}, LX/0gb;-><init>(LX/0gN;)V

    invoke-virtual {v2, v3, p0}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v2

    iput-object v2, v1, LX/0gN;->f:LX/0gM;
    :try_end_1
    .catch LX/31B; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 104997
    :catch_1
    const/4 v2, 0x0

    iput-object v2, v1, LX/0gN;->f:LX/0gM;

    goto :goto_1
.end method

.method private static a(LX/0gc;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 104965
    invoke-virtual {p0, p1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 104966
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104967
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 104957
    const/4 v2, 0x0

    .line 104958
    if-eqz p2, :cond_1

    const-string v0, "publishPostParams"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104959
    const-string v0, "publishPostParams"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 104960
    if-eqz v0, :cond_0

    .line 104961
    iget-object v2, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    move v4, v3

    .line 104962
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    .line 104963
    sget-object v1, LX/0ge;->COMPOSER_MAIN_TAB_ACTIVITY_RESULT:LX/0ge;

    move v5, p1

    invoke-virtual/range {v0 .. v5}, LX/0gd;->a(LX/0ge;Ljava/lang/String;ZZI)V

    .line 104964
    return-void

    :cond_0
    move v4, v3

    move v3, v1

    goto :goto_0

    :cond_1
    move v3, v1

    move v4, v1

    goto :goto_0
.end method

.method private a(LX/0fK;)V
    .locals 3

    .prologue
    .line 104955
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ah:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "system_triggered"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v0

    invoke-virtual {p1}, LX/0fK;->h()LX/0gi;

    move-result-object v1

    invoke-virtual {v1}, LX/0gi;->getAnalyticsTag()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 104956
    return-void
.end method

.method private a(LX/0gf;)V
    .locals 3

    .prologue
    .line 104951
    iput-object p1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aI:LX/0gf;

    .line 104952
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->az:Z

    .line 104953
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->f()LX/0Px;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ad()Lcom/facebook/apptab/state/TabTag;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0gg;->a(I)V

    .line 104954
    return-void
.end method

.method private static a(Lcom/facebook/katana/activity/FbMainTabActivity;LX/0gj;LX/0gk;LX/0gl;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0gm;LX/0Or;LX/0Or;LX/0ad;LX/0Ot;LX/0gn;LX/0go;LX/0gp;LX/0fO;LX/0fU;Ljava/lang/Boolean;LX/0W3;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/activity/FbMainTabActivity;",
            "LX/0gj;",
            "LX/0gk;",
            "LX/0gl;",
            "LX/0Or",
            "<",
            "LX/0hJ;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0iS;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0iX;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0ii;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0ie;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gd;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0fW;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0hg;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0he;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0hy;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0hI;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gw;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0hx;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0hw;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0hv;",
            ">;",
            "LX/0gm;",
            "LX/0Or",
            "<",
            "LX/0hq;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0fJ;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/0hC;",
            ">;",
            "LX/0gn;",
            "LX/0go;",
            "LX/0gp;",
            "LX/0fO;",
            "LX/0fU;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/0Yl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Yi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/divebar/DivebarController;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/divebar/DivebarController;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0fP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0i0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0jM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0jN;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ja;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0hB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gJ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0id;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104950
    iput-object p1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->p:LX/0gj;

    iput-object p2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->q:LX/0gk;

    iput-object p3, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->r:LX/0gl;

    iput-object p4, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->s:LX/0Or;

    iput-object p5, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->t:LX/0Or;

    iput-object p6, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->u:LX/0Or;

    iput-object p7, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->v:LX/0Or;

    iput-object p8, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->w:LX/0Or;

    iput-object p9, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->x:LX/0Or;

    iput-object p10, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->y:LX/0Or;

    iput-object p11, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->z:LX/0Or;

    iput-object p12, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->A:LX/0Or;

    iput-object p13, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->B:LX/0Or;

    iput-object p14, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->C:LX/0Or;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->D:LX/0Or;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->E:LX/0Or;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->F:LX/0Or;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->G:LX/0Or;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->H:LX/0Or;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->I:LX/0Or;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->J:LX/0Or;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->K:LX/0Or;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->L:LX/0Or;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->M:LX/0Or;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->N:LX/0Or;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->O:LX/0gm;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->P:LX/0Or;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->Q:LX/0Or;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->Y:LX/0gn;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->Z:LX/0go;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aa:LX/0gp;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ab:LX/0fO;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ac:LX/0fU;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ad:Ljava/lang/Boolean;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ae:LX/0W3;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ag:LX/0Ot;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ah:LX/0Ot;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ak:LX/0Ot;

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->al:LX/0Ot;

    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->am:LX/0Ot;

    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->an:LX/0Ot;

    move-object/from16 v0, p47

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ao:LX/0Ot;

    move-object/from16 v0, p48

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ap:LX/0Ot;

    move-object/from16 v0, p49

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aq:LX/0Ot;

    move-object/from16 v0, p50

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ar:LX/0Ot;

    move-object/from16 v0, p51

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->as:LX/0Ot;

    move-object/from16 v0, p52

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->at:LX/0Ot;

    move-object/from16 v0, p53

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->au:LX/0Ot;

    move-object/from16 v0, p54

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 58

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v56

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/katana/activity/FbMainTabActivity;

    const-class v3, LX/0gj;

    move-object/from16 v0, v56

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/0gj;

    const-class v4, LX/0gk;

    move-object/from16 v0, v56

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/0gk;

    const-class v5, LX/0gl;

    move-object/from16 v0, v56

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/0gl;

    const/16 v6, 0x13a

    move-object/from16 v0, v56

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x2d2

    move-object/from16 v0, v56

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x5e1

    move-object/from16 v0, v56

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x792

    move-object/from16 v0, v56

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x166

    move-object/from16 v0, v56

    invoke-static {v0, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0xe49

    move-object/from16 v0, v56

    invoke-static {v0, v11}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0xe47

    move-object/from16 v0, v56

    invoke-static {v0, v12}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x3bf

    move-object/from16 v0, v56

    invoke-static {v0, v13}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0x259

    move-object/from16 v0, v56

    invoke-static {v0, v14}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const/16 v15, 0x4d5

    move-object/from16 v0, v56

    invoke-static {v0, v15}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    const/16 v16, 0x1ce

    move-object/from16 v0, v56

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x122d

    move-object/from16 v0, v56

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    const/16 v18, 0x164

    move-object/from16 v0, v56

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    const/16 v19, 0x163

    move-object/from16 v0, v56

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    const/16 v20, 0xbe1

    move-object/from16 v0, v56

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    const/16 v21, 0x12be

    move-object/from16 v0, v56

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    const/16 v22, 0x1583

    move-object/from16 v0, v56

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    const/16 v23, 0x1239

    move-object/from16 v0, v56

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v23

    const/16 v24, 0x91

    move-object/from16 v0, v56

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v24

    const/16 v25, 0xb3e

    move-object/from16 v0, v56

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v25

    const/16 v26, 0x29d

    move-object/from16 v0, v56

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v26

    const/16 v27, 0x5ee

    move-object/from16 v0, v56

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v27

    const-class v28, LX/0gm;

    move-object/from16 v0, v56

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v28

    check-cast v28, LX/0gm;

    const/16 v29, 0xbe8

    move-object/from16 v0, v56

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v29

    const/16 v30, 0xc16

    move-object/from16 v0, v56

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v30

    invoke-static/range {v56 .. v56}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v31

    check-cast v31, LX/0ad;

    const/16 v32, 0xbf2

    move-object/from16 v0, v56

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v32

    invoke-static/range {v56 .. v56}, LX/0gn;->a(LX/0QB;)LX/0gn;

    move-result-object v33

    check-cast v33, LX/0gn;

    invoke-static/range {v56 .. v56}, LX/0go;->a(LX/0QB;)LX/0go;

    move-result-object v34

    check-cast v34, LX/0go;

    invoke-static/range {v56 .. v56}, LX/0gp;->a(LX/0QB;)LX/0gp;

    move-result-object v35

    check-cast v35, LX/0gp;

    invoke-static/range {v56 .. v56}, LX/0fO;->a(LX/0QB;)LX/0fO;

    move-result-object v36

    check-cast v36, LX/0fO;

    invoke-static/range {v56 .. v56}, LX/0fU;->a(LX/0QB;)LX/0fU;

    move-result-object v37

    check-cast v37, LX/0fU;

    invoke-static/range {v56 .. v56}, LX/0gq;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v38

    check-cast v38, Ljava/lang/Boolean;

    invoke-static/range {v56 .. v56}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v39

    check-cast v39, LX/0W3;

    const/16 v40, 0xf0f

    move-object/from16 v0, v56

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v40

    const/16 v41, 0x6a1

    move-object/from16 v0, v56

    move/from16 v1, v41

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v41

    const/16 v42, 0x97

    move-object/from16 v0, v56

    move/from16 v1, v42

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v42

    const/16 v43, 0x4fa

    move-object/from16 v0, v56

    move/from16 v1, v43

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v43

    const/16 v44, 0x4fb

    move-object/from16 v0, v56

    move/from16 v1, v44

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v44

    const/16 v45, 0x2db

    move-object/from16 v0, v56

    move/from16 v1, v45

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v45

    const/16 v46, 0x12aa

    move-object/from16 v0, v56

    move/from16 v1, v46

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v46

    const/16 v47, 0xbca

    move-object/from16 v0, v56

    move/from16 v1, v47

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v47

    const/16 v48, 0x13c

    move-object/from16 v0, v56

    move/from16 v1, v48

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v48

    const/16 v49, 0xe9f

    move-object/from16 v0, v56

    move/from16 v1, v49

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v49

    const/16 v50, 0x50d

    move-object/from16 v0, v56

    move/from16 v1, v50

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v50

    const/16 v51, 0x12bd

    move-object/from16 v0, v56

    move/from16 v1, v51

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v51

    const/16 v52, 0xac0

    move-object/from16 v0, v56

    move/from16 v1, v52

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v52

    const/16 v53, 0x4be

    move-object/from16 v0, v56

    move/from16 v1, v53

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v53

    const/16 v54, 0x165

    move-object/from16 v0, v56

    move/from16 v1, v54

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v54

    const/16 v55, 0xf0d

    move-object/from16 v0, v56

    move/from16 v1, v55

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v55

    const/16 v57, 0xf0c

    invoke-static/range {v56 .. v57}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v56

    invoke-static/range {v2 .. v56}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(Lcom/facebook/katana/activity/FbMainTabActivity;LX/0gj;LX/0gk;LX/0gl;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0gm;LX/0Or;LX/0Or;LX/0ad;LX/0Ot;LX/0gn;LX/0go;LX/0gp;LX/0fO;LX/0fU;Ljava/lang/Boolean;LX/0W3;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 104948
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(Ljava/lang/String;JLX/00q;)V

    .line 104949
    return-void
.end method

.method private a(Ljava/lang/String;JLX/00q;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 105208
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-virtual {v0, v1, v2}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105209
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Yl;

    const v2, 0xa007d

    move-object v3, p1

    move-object v5, v4

    move-wide v6, p2

    move-object v8, p4

    move-object v9, v4

    invoke-virtual/range {v1 .. v9}, LX/0Yl;->a(ILjava/lang/String;Ljava/lang/String;LX/0P1;JLX/00q;Ljava/lang/Boolean;)LX/0Yl;

    .line 105210
    :cond_0
    :goto_0
    return-void

    .line 105211
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa0004

    const-string v2, "NNFWarmStart"

    invoke-virtual {v0, v1, v2}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105212
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Yl;

    const v2, 0xa007e

    move-object v3, p1

    move-object v5, v4

    move-wide v6, p2

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->a(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 105203
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-virtual {v0, v1, v2}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105204
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa007d

    invoke-virtual {v0, v1, p1, p2}, LX/0Yl;->a(ILjava/lang/String;Z)LX/0Yl;

    .line 105205
    :cond_0
    :goto_0
    return-void

    .line 105206
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa0004

    const-string v2, "NNFWarmStart"

    invoke-virtual {v0, v1, v2}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105207
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa007e

    invoke-virtual {v0, v1, p1}, LX/0Yl;->b(ILjava/lang/String;)LX/0Yl;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/friending/jewel/FriendRequestsFragment;Lcom/facebook/katana/fragment/FbChromeFragment;)Z
    .locals 1

    .prologue
    .line 105389
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 105390
    :goto_0
    if-ne p0, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 105391
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 105392
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/katana/activity/FbMainTabActivity;LX/0gs;)V
    .locals 2

    .prologue
    .line 105393
    sget-object v0, LX/0gs;->CLOSED:LX/0gs;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->h()LX/0gi;

    move-result-object v0

    sget-object v1, LX/0gi;->DEFAULT_DIVEBAR:LX/0gi;

    if-ne v0, v1, :cond_0

    .line 105394
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->D:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 105395
    const-string v1, "1685150328371343"

    .line 105396
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 105397
    move-object v0, v0

    .line 105398
    invoke-virtual {v0, p0}, LX/0gt;->a(Landroid/content/Context;)V

    .line 105399
    :cond_0
    return-void
.end method

.method private aa()Lcom/facebook/katana/fragment/FbChromeFragment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 105400
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    if-nez v0, :cond_0

    .line 105401
    const/4 v0, 0x0

    .line 105402
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->h()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    goto :goto_0
.end method

.method private ab()V
    .locals 2

    .prologue
    .line 105403
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ab:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105404
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->at:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gJ;

    .line 105405
    iget-object v1, v0, LX/0gJ;->e:LX/0gK;

    invoke-virtual {v1}, LX/0gK;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105406
    iget-object v1, v0, LX/0gJ;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gL;

    .line 105407
    iget-object p0, v1, LX/0gL;->g:LX/0gM;

    if-nez p0, :cond_2

    .line 105408
    :cond_0
    :goto_0
    return-void

    .line 105409
    :cond_1
    iget-object v1, v0, LX/0gJ;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gN;

    .line 105410
    iget-object p0, v1, LX/0gN;->f:LX/0gM;

    if-nez p0, :cond_3

    .line 105411
    :goto_1
    goto :goto_0

    .line 105412
    :cond_2
    iget-object p0, v1, LX/0gL;->d:LX/0gX;

    iget-object v0, v1, LX/0gL;->g:LX/0gM;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0gX;->a(Ljava/util/Set;)V

    .line 105413
    const/4 p0, 0x0

    iput-object p0, v1, LX/0gL;->g:LX/0gM;

    goto :goto_0

    .line 105414
    :cond_3
    iget-object p0, v1, LX/0gN;->d:LX/0gX;

    iget-object v0, v1, LX/0gN;->f:LX/0gM;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0gX;->a(Ljava/util/Set;)V

    .line 105415
    const/4 p0, 0x0

    iput-object p0, v1, LX/0gN;->f:LX/0gM;

    goto :goto_1
.end method

.method private ac()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 105416
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget-short v2, LX/0fe;->bI:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 105417
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget-short v2, LX/0fe;->bJ:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->X()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->z()Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private ad()Lcom/facebook/apptab/state/TabTag;
    .locals 1

    .prologue
    .line 105418
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ad:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105419
    sget-object v0, Lcom/facebook/work/groupstab/WorkGroupsTab;->l:Lcom/facebook/work/groupstab/WorkGroupsTab;

    .line 105420
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    goto :goto_0
.end method

.method private ae()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 105421
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    .line 105422
    if-nez v0, :cond_0

    move v0, v1

    .line 105423
    :goto_0
    return v0

    .line 105424
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 105425
    if-eqz v0, :cond_1

    instance-of v2, v0, LX/0fj;

    if-eqz v2, :cond_1

    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private af()Z
    .locals 2

    .prologue
    .line 104634
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "chromeless:content:fragment:tag"

    invoke-static {v0, v1}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(LX/0gc;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 104635
    instance-of v0, v0, LX/0gv;

    if-eqz v0, :cond_0

    .line 104636
    const/4 v0, 0x1

    .line 104637
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ag()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 105429
    iget-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aH:Z

    if-eqz v0, :cond_0

    .line 105430
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0413

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 105431
    const v0, 0x7f0d1a73

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 105432
    const v1, 0x7f0d1a6f

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 105433
    invoke-virtual {v0, v3, v3, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 105434
    invoke-virtual {v1, v3, v3, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 105435
    const v2, 0x7f0d1a73

    invoke-virtual {p0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105436
    const v0, 0x7f0d1a6f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105437
    :cond_0
    return-void
.end method

.method private ah()V
    .locals 8

    .prologue
    .line 105438
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->J:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gw;

    .line 105439
    invoke-virtual {v0}, LX/0gw;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105440
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    const v1, 0x7f0d1a74

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    .line 105441
    iget-object v2, v0, LX/0gx;->m:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 105442
    const v2, 0x7f0b0411

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 105443
    invoke-virtual {v0}, LX/0gx;->d()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    .line 105444
    iget-object v2, v0, LX/0gx;->k:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0gy;

    .line 105445
    sget-object v6, LX/0cQ;->NATIVE_NEWS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2, v6}, LX/0gy;->b(LX/0cQ;)LX/0gz;

    move-result-object v6

    .line 105446
    mul-int v2, v4, v5

    add-int/lit8 v4, v5, -0x1

    const v5, 0x7f0b103e

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    mul-int/2addr v4, v5

    add-int/2addr v4, v2

    .line 105447
    invoke-virtual {v6}, LX/0gz;->h()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    invoke-virtual {v6}, LX/0gz;->i()I

    move-result v7

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result p0

    invoke-virtual {v1, v2, v5, v7, p0}, Landroid/view/View;->setPadding(IIII)V

    .line 105448
    iget-object v2, v0, LX/0gx;->w:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 105449
    invoke-virtual {v6}, LX/0gz;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105450
    iget v2, v6, LX/0gz;->b:I

    move v2, v2

    .line 105451
    const v4, 0x7f0b1040

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 105452
    :goto_0
    iget-object v3, v0, LX/0gx;->v:LX/0fd;

    .line 105453
    iget-object v4, v3, LX/0fd;->c:LX/0h2;

    if-eqz v4, :cond_0

    .line 105454
    iget-object v4, v3, LX/0fd;->c:LX/0h2;

    .line 105455
    iget-object v3, v4, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v3}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 105456
    :cond_0
    return-void

    .line 105457
    :cond_1
    iget-object v2, v0, LX/0gx;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0hB;

    .line 105458
    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    sub-int/2addr v2, v4

    invoke-virtual {v6}, LX/0gz;->h()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual {v6}, LX/0gz;->i()I

    move-result v4

    sub-int/2addr v2, v4

    .line 105459
    const v4, 0x7f0b103d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v2, v4

    .line 105460
    const v4, 0x7f0b103c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0
.end method

.method private ai()V
    .locals 3

    .prologue
    .line 105461
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 105462
    invoke-virtual {v0}, LX/0hC;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105463
    iget-object v1, v0, LX/0hC;->a:LX/0hE;

    const/4 v2, 0x4

    invoke-interface {v1, v2}, LX/0hE;->setVisibility(I)V

    .line 105464
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 105465
    if-nez v1, :cond_2

    .line 105466
    :cond_1
    :goto_0
    return-void

    .line 105467
    :cond_2
    const-string v0, "chromeless:content:fragment:tag"

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 105468
    if-nez v0, :cond_3

    .line 105469
    const-string v0, "PhotoAnimationDialogFragment_MEDIA_GALLERY"

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 105470
    :cond_3
    instance-of v2, v0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    if-eqz v2, :cond_4

    .line 105471
    check-cast v0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/popover/PopoverFragment;->k()V

    goto :goto_0

    .line 105472
    :cond_4
    if-eqz v0, :cond_1

    .line 105473
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method private aj()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 105474
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105475
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(LX/0fK;)V

    .line 105476
    :cond_0
    :goto_0
    return-void

    .line 105477
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105478
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(LX/0fK;)V

    goto :goto_0

    .line 105479
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v4

    .line 105480
    if-eqz v4, :cond_0

    .line 105481
    sget-object v0, Lcom/facebook/katana/activity/FbMainTabActivity;->S:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    move-object v1, v2

    :goto_1
    if-ge v3, v5, :cond_3

    sget-object v0, Lcom/facebook/katana/activity/FbMainTabActivity;->S:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 105482
    invoke-virtual {v4, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 105483
    if-nez v1, :cond_3

    .line 105484
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 105485
    :cond_3
    if-eqz v1, :cond_0

    instance-of v0, v1, LX/0f2;

    if-eqz v0, :cond_0

    .line 105486
    instance-of v0, v1, LX/0f1;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 105487
    check-cast v0, LX/0f1;

    invoke-interface {v0}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v0

    move-object v2, v0

    .line 105488
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ah:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v3, "system_triggered"

    invoke-virtual {v0, v3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v0

    check-cast v1, LX/0f2;

    invoke-interface {v1}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, LX/0gh;->a(Ljava/lang/String;ZLjava/util/Map;)V

    goto :goto_0
.end method

.method private ak()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 105371
    const v0, 0x7f0d18db

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    .line 105372
    invoke-static {}, Lcom/facebook/katana/activity/FbMainTabActivity;->ao()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105373
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 105374
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v3, 0x400

    invoke-virtual {v0, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 105375
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->H:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hI;

    .line 105376
    invoke-virtual {v0, v2}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v3

    invoke-virtual {v1, v4, v3, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 105377
    iget-object v3, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aa:LX/0gp;

    invoke-virtual {v3}, LX/0gp;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->an()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 105378
    const v3, 0x7f0d1a77

    invoke-virtual {p0, v3}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v3

    .line 105379
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-virtual {v0, v2}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v0

    invoke-direct {v4, v5, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105380
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hJ;

    .line 105381
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, LX/0hJ;->a:Ljava/lang/ref/WeakReference;

    .line 105382
    return-void
.end method

.method private al()V
    .locals 3

    .prologue
    .line 105383
    new-instance v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    invoke-direct {v0}, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    .line 105384
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 105385
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 105386
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    const-string v2, "FbMainTabFragment"

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 105387
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 105388
    return-void
.end method

.method private am()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 105370
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget-short v2, LX/0hK;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget-short v2, LX/0hK;->b:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private an()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 105369
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget-short v2, LX/0hK;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget-short v2, LX/0hK;->c:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static ao()Z
    .locals 2

    .prologue
    .line 105368
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;J)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 105363
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-virtual {v0, v1, v2}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105364
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Yl;

    const v2, 0xa007d

    move-object v3, p1

    move-object v5, v4

    move-wide v6, p2

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->b(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 105365
    :cond_0
    :goto_0
    return-void

    .line 105366
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa0004

    const-string v2, "NNFWarmStart"

    invoke-virtual {v0, v1, v2}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105367
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Yl;

    const v2, 0xa007e

    move-object v3, p1

    move-object v5, v4

    move-wide v6, p2

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->c(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 105361
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(Ljava/lang/String;Z)V

    .line 105362
    return-void
.end method

.method private d(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 105332
    const-string v0, "FbMainTabActivity.handleNewIntent"

    const v1, 0x20b8bc9a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 105333
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->Y:LX/0gn;

    invoke-virtual {v0, p1}, LX/0gn;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    .line 105334
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 105335
    const-string v0, "open_right_divebar"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 105336
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    .line 105337
    iput-object v1, v0, LX/0fK;->r:Landroid/content/Intent;

    .line 105338
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->B()V

    .line 105339
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 105340
    const-string v0, "open_left_divebar"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 105341
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    .line 105342
    iput-object v1, v0, LX/0fK;->r:Landroid/content/Intent;

    .line 105343
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->b(Z)V

    .line 105344
    :cond_1
    :goto_1
    const-string v0, "refresh_feed"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105345
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->X()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    sget-object v2, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    invoke-virtual {v0, v2}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(LX/0gf;)V

    .line 105346
    :cond_2
    const-string v0, "is_from_push_notification"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 105347
    sget-object v0, LX/0gf;->FROM_PUSH_NOTIFICATION:LX/0gf;

    .line 105348
    const-string v2, "push_notification_objid"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "push_notification_href"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "push_notification_uid"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "push_notification_type"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, LX/0gf;->setExtras(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105349
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->q()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 105350
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->X()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(LX/0gf;)V

    .line 105351
    :cond_3
    :goto_2
    const-string v0, "jump_to_top"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 105352
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->T()V

    .line 105353
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->Y:LX/0gn;

    invoke-virtual {v0, v1}, LX/0gn;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 105354
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-virtual {v0, v1}, LX/0fd;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105355
    :cond_5
    const v0, -0x61b8ac96

    invoke-static {v0}, LX/02m;->a(I)V

    .line 105356
    return-void

    .line 105357
    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 105358
    :catchall_0
    move-exception v0

    const v1, -0x6022909e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 105359
    :cond_7
    :try_start_2
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->f()V

    goto/16 :goto_1

    .line 105360
    :cond_8
    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(LX/0gf;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 105283
    new-instance v0, LX/0fd;

    invoke-direct {v0, p0, p0}, LX/0fd;-><init>(LX/0fF;LX/0fG;)V

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    .line 105284
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->r:LX/0gl;

    .line 105285
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    const v2, 0x7f0d1a6e

    invoke-virtual {p0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gl;->a(LX/0fd;Landroid/view/View;)LX/0gx;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    .line 105286
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->q:LX/0gk;

    .line 105287
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    const v2, 0x7f0d00bc

    invoke-virtual {p0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gk;->a(LX/0fd;Landroid/view/View;)LX/0h2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aD:LX/0h2;

    .line 105288
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->p:LX/0gj;

    .line 105289
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    const v2, 0x7f0d1a6b

    invoke-virtual {p0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0gj;->a(LX/0fd;Landroid/view/View;LX/0gc;)LX/0gg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    .line 105290
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aD:LX/0h2;

    iget-object v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    iget-object v3, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    .line 105291
    iput-object v1, v0, LX/0fd;->c:LX/0h2;

    .line 105292
    iput-object v2, v0, LX/0fd;->d:LX/0gx;

    .line 105293
    iput-object v3, v0, LX/0fd;->e:LX/0gg;

    .line 105294
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    const/4 v2, 0x0

    .line 105295
    iget-object v1, v0, LX/0gx;->o:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0gx;->d()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0R9;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 105296
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/apptab/state/TabTag;

    .line 105297
    invoke-static {v0, v1}, LX/0gx;->b(LX/0gx;Lcom/facebook/apptab/state/TabTag;)V

    goto :goto_1

    .line 105298
    :cond_0
    invoke-virtual {v0}, LX/0gx;->d()LX/0Px;

    move-result-object v1

    goto :goto_0

    .line 105299
    :cond_1
    iput v2, v0, LX/0gx;->A:I

    .line 105300
    iget-object v1, v0, LX/0gx;->l:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 105301
    sget-object v3, LX/0hM;->P:LX/0Tn;

    invoke-interface {v1, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 105302
    :goto_2
    invoke-virtual {v0}, LX/0gx;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 105303
    invoke-virtual {v0}, LX/0gx;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/facebook/notifications/tab/NotificationsTab;

    if-eqz v3, :cond_2

    .line 105304
    iput v2, v0, LX/0gx;->A:I

    .line 105305
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 105306
    :cond_3
    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->P:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 105307
    :goto_3
    invoke-virtual {v0}, LX/0gx;->d()LX/0Px;

    move-result-object v1

    iget v2, v0, LX/0gx;->A:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/0gx;->z:Ljava/lang/String;

    .line 105308
    iget-object v1, v0, LX/0gx;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gx;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0hO;->setSelected(Z)V

    .line 105309
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ah()V

    .line 105310
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aD:LX/0h2;

    invoke-virtual {v0}, LX/0h2;->a()V

    .line 105311
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    .line 105312
    new-instance v1, LX/0hP;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/0hP;-><init>(LX/0gg;Landroid/content/Intent;)V

    iput-object v1, v0, LX/0gg;->r:LX/0hP;

    .line 105313
    iget-object v1, v0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    iget-object v2, v0, LX/0gg;->r:LX/0hP;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 105314
    iget-object v1, v0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    new-instance v2, LX/0hZ;

    invoke-direct {v2, v0, p0}, LX/0hZ;-><init>(LX/0gg;Landroid/app/Activity;)V

    .line 105315
    iput-object v2, v1, Lcom/facebook/widget/CustomViewPager;->d:LX/0ha;

    .line 105316
    if-nez p1, :cond_7

    const/4 v1, 0x1

    .line 105317
    :goto_4
    iget-object v2, v0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setOnlyCreatePagesImmediatelyOffscreen(Z)V

    .line 105318
    iget-object v2, v0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    iget-object v3, v0, LX/0gg;->r:LX/0hP;

    invoke-virtual {v3}, LX/0hP;->b()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 105319
    iget-object v2, v0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    iget-object v3, v0, LX/0gg;->o:LX/0fd;

    .line 105320
    iget-object v4, v3, LX/0fd;->d:LX/0gx;

    .line 105321
    iget v3, v4, LX/0gx;->A:I

    move v4, v3

    .line 105322
    move v3, v4

    .line 105323
    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 105324
    if-eqz v1, :cond_4

    .line 105325
    iget-object v1, v0, LX/0gg;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sg;

    const-string v2, "Viewpager CreatePagesOtherThanImmediatelyOffscreen"

    new-instance v3, Lcom/facebook/ui/mainview/ViewPagerController$2;

    invoke-direct {v3, v0}, Lcom/facebook/ui/mainview/ViewPagerController$2;-><init>(LX/0gg;)V

    sget-object v4, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    sget-object v5, LX/0Vm;->UI:LX/0Vm;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 105326
    :cond_4
    invoke-static {v0}, LX/0gg;->g(LX/0gg;)V

    .line 105327
    new-instance v1, LX/0hb;

    invoke-direct {v1, v0, p0}, LX/0hb;-><init>(LX/0gg;Landroid/app/Activity;)V

    .line 105328
    iget-object v2, v0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 105329
    return-void

    .line 105330
    :cond_5
    iget-object v1, v0, LX/0gx;->o:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, LX/0gx;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v2, v1, 0x0

    :cond_6
    iput v2, v0, LX/0gx;->A:I

    goto/16 :goto_3

    .line 105331
    :cond_7
    const/4 v1, 0x0

    goto :goto_4
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 105278
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-virtual {v0, v1, v2}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105279
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa007d

    invoke-virtual {v0, v1, p1}, LX/0Yl;->c(ILjava/lang/String;)LX/0Yl;

    .line 105280
    :cond_0
    :goto_0
    return-void

    .line 105281
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa0004

    const-string v2, "NNFWarmStart"

    invoke-virtual {v0, v1, v2}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105282
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    const v1, 0xa007e

    invoke-virtual {v0, v1, p1}, LX/0Yl;->f(ILjava/lang/String;)LX/0Yl;

    goto :goto_0
.end method

.method private d(Z)V
    .locals 6

    .prologue
    const/high16 v3, 0x8000000

    const/high16 v2, -0x80000000

    .line 105255
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->as:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v1

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->as:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-virtual {v0}, LX/0hB;->e()I

    move-result v0

    if-eq v1, v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 105256
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 105257
    if-eqz p1, :cond_2

    .line 105258
    invoke-virtual {v1, v3}, Landroid/view/Window;->addFlags(I)V

    .line 105259
    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 105260
    :goto_0
    const v0, 0x7f0d1a6c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v2

    .line 105261
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->as:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-virtual {v0}, LX/0hB;->e()I

    move-result v4

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->as:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    sub-int v0, v4, v0

    :goto_1
    iput v0, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 105262
    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 105263
    if-eqz p1, :cond_0

    .line 105264
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v0, v4, :cond_4

    invoke-virtual {v1}, Landroid/view/Window;->getNavigationBarColor()I

    move-result v0

    :goto_2
    invoke-direct {v3, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 105265
    :cond_0
    const v0, 0x7f0d1a6a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 105266
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105267
    if-eqz p1, :cond_5

    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->as:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-virtual {v1}, LX/0hB;->e()I

    move-result v1

    add-int/2addr v3, v1

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->as:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    sub-int v1, v3, v1

    .line 105268
    :goto_3
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v5, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 105269
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 105270
    const/16 v2, 0x50

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 105271
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105272
    :cond_1
    return-void

    .line 105273
    :cond_2
    invoke-virtual {v1, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 105274
    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    goto/16 :goto_0

    .line 105275
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 105276
    :cond_4
    const/high16 v0, -0x1000000

    goto :goto_2

    .line 105277
    :cond_5
    iget v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_3
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 105247
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105248
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0, p0}, LX/0fK;->a(Landroid/app/Activity;)V

    .line 105249
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0fK;->b_(Z)V

    .line 105250
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0fK;->c_(Z)V

    .line 105251
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->i()V

    .line 105252
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->g()V

    .line 105253
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    new-instance v1, LX/0hd;

    invoke-direct {v1, p0}, LX/0hd;-><init>(Lcom/facebook/katana/activity/FbMainTabActivity;)V

    invoke-virtual {v0, v1}, LX/0fK;->a(LX/0fT;)V

    .line 105254
    return-void
.end method

.method public final B()V
    .locals 2

    .prologue
    .line 105239
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ab:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105240
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->F:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0he;

    .line 105241
    sget-object v1, LX/0hf;->FEED:LX/0hf;

    iput-object v1, v0, LX/0he;->j:LX/0hf;

    .line 105242
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->E:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hg;

    .line 105243
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hg;->a(Z)V

    .line 105244
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->H()V

    .line 105245
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0fK;->a_(Z)V

    .line 105246
    return-void
.end method

.method public final C()V
    .locals 3

    .prologue
    .line 105232
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ac()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105233
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget-short v1, LX/0fe;->bE:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/0gf;->TAB_CLICK_MANUAL:LX/0gf;

    .line 105234
    :goto_0
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->X()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v1

    .line 105235
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105236
    invoke-virtual {v1, v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(LX/0gf;)V

    .line 105237
    :cond_0
    return-void

    .line 105238
    :cond_1
    sget-object v0, LX/0gf;->TAB_CLICK:LX/0gf;

    goto :goto_0
.end method

.method public final D()V
    .locals 2

    .prologue
    .line 105223
    iget-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->az:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105224
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->X()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    .line 105225
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aI:LX/0gf;

    if-eqz v1, :cond_1

    .line 105226
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aI:LX/0gf;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(LX/0gf;)V

    .line 105227
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->T()V

    .line 105228
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aI:LX/0gf;

    .line 105229
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->az:Z

    .line 105230
    :cond_0
    return-void

    .line 105231
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->mJ_()V

    goto :goto_0
.end method

.method public final E()V
    .locals 1

    .prologue
    .line 105213
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    if-eqz v0, :cond_0

    .line 105214
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    .line 105215
    iget-object p0, v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    if-nez p0, :cond_1

    .line 105216
    :cond_0
    :goto_0
    return-void

    .line 105217
    :cond_1
    iget-object p0, v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    .line 105218
    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hm;

    invoke-virtual {v0}, LX/0hm;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105219
    iget-object v0, p0, LX/0hh;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hm;

    .line 105220
    iget-object p0, v0, LX/0hm;->e:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0hn;

    .line 105221
    iget-object v0, p0, LX/0hn;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hp;

    invoke-virtual {v0}, LX/0hp;->a()V

    .line 105222
    :cond_2
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/0hO;
    .locals 1

    .prologue
    .line 105042
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-virtual {v0, p1}, LX/0fd;->c(Ljava/lang/String;)LX/0hO;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 105196
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 105197
    const-string v0, "unknown"

    .line 105198
    :goto_0
    return-object v0

    .line 105199
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 105200
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/0fh;

    if-eqz v1, :cond_1

    .line 105201
    check-cast v0, LX/0fh;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 105202
    :cond_1
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public final a(IIIZ)V
    .locals 1

    .prologue
    .line 104437
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aL:Z

    .line 104438
    if-gtz p3, :cond_0

    if-nez p4, :cond_0

    if-eq p1, p2, :cond_1

    .line 104439
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aM:Z

    .line 104440
    :cond_1
    return-void
.end method

.method public final a(LX/0hE;)V
    .locals 1

    .prologue
    .line 104618
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 104619
    iput-object p1, v0, LX/0hC;->a:LX/0hE;

    .line 104620
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 104615
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 104616
    invoke-direct {p0, p1}, Lcom/facebook/katana/activity/FbMainTabActivity;->d(Landroid/content/Intent;)V

    .line 104617
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 104608
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 104609
    :try_start_0
    const-string v0, "FbMainTabActivity.createDispatcher"

    const v1, 0xdd8ee6f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 104610
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->P:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hq;

    .line 104611
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    invoke-virtual {v0, v1}, LX/0hq;->a(LX/0Ot;)LX/0hr;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104612
    const v0, 0x41ef098f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 104613
    return-void

    .line 104614
    :catchall_0
    move-exception v0

    const v1, 0x3e453d2b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/apptab/state/TabTag;LX/0hO;)V
    .locals 1

    .prologue
    .line 104606
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    invoke-virtual {v0, p1, p2}, LX/0gx;->a(Lcom/facebook/apptab/state/TabTag;LX/0hO;)V

    .line 104607
    return-void
.end method

.method public final a(Lcom/facebook/apptab/state/TabTag;LX/0hs;)V
    .locals 2

    .prologue
    .line 104600
    invoke-virtual {p1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v0

    .line 104601
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    .line 104602
    iget-boolean p0, v1, LX/0gg;->u:Z

    move v1, p0

    .line 104603
    if-eqz v1, :cond_0

    .line 104604
    invoke-virtual {p2, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 104605
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 5

    .prologue
    .line 104595
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(Lcom/facebook/friending/jewel/FriendRequestsFragment;Lcom/facebook/katana/fragment/FbChromeFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104596
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    invoke-virtual {p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b()Z

    move-result v1

    .line 104597
    const v2, 0x7f020886

    move v2, v2

    .line 104598
    invoke-virtual {p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->d()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->e()Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0hr;->a(ZILjava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 104599
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)V
    .locals 0

    .prologue
    .line 104593
    iput-object p1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aP:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    .line 104594
    return-void
.end method

.method public final a(Lcom/facebook/base/fragment/FbFragment;)Z
    .locals 1

    .prologue
    .line 104588
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    if-nez v0, :cond_0

    .line 104589
    const/4 v0, 0x0

    .line 104590
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    .line 104591
    invoke-virtual {v0}, LX/0gg;->b()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object p0

    if-ne p0, p1, :cond_1

    const/4 p0, 0x1

    :goto_1
    move v0, p0

    .line 104592
    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final b(Landroid/content/Intent;)LX/0cQ;
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 104578
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/content/Intent;)LX/0cQ;

    move-result-object v0

    .line 104579
    if-eqz v0, :cond_0

    .line 104580
    :goto_0
    return-object v0

    .line 104581
    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "target_tab_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104582
    const-string v0, "target_tab_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104583
    if-eqz v0, :cond_1

    .line 104584
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->Z:LX/0go;

    invoke-virtual {v1, v0}, LX/0go;->b(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    .line 104585
    if-eqz v0, :cond_1

    .line 104586
    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->b:LX/0cQ;

    goto :goto_0

    .line 104587
    :cond_1
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ad()Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->b:LX/0cQ;

    goto :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 104572
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 104573
    :goto_0
    return-object v0

    .line 104574
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 104575
    if-eqz v0, :cond_1

    instance-of v2, v0, LX/0hF;

    if-eqz v2, :cond_1

    .line 104576
    check-cast v0, LX/0hF;

    invoke-interface {v0}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 104577
    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 104461
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->O:LX/0gm;

    .line 104462
    invoke-virtual {v0, p0}, LX/0gm;->a(Landroid/app/Activity;)LX/0hu;

    move-result-object v0

    .line 104463
    iget-object v1, v0, LX/0hu;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 104464
    iget-object v1, v0, LX/0hu;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0fO;

    .line 104465
    invoke-virtual {v1}, LX/0fO;->f()Z

    move-result v2

    if-nez v2, :cond_10

    .line 104466
    sget-object v1, LX/0gH;->NO_LEFT_DIVEBAR:LX/0gH;

    .line 104467
    :goto_0
    move-object v0, v1

    .line 104468
    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ax:LX/0gH;

    .line 104469
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 104470
    if-eqz p1, :cond_0

    .line 104471
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 104472
    iput-boolean v7, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aA:Z

    .line 104473
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 104474
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->F()V

    .line 104475
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LX/0gn;->c(Landroid/content/Intent;)Z

    move-result v0

    .line 104476
    if-eqz v0, :cond_2

    .line 104477
    sget-object v0, LX/00a;->a:LX/00a;

    move-object v0, v0

    .line 104478
    iget-object v1, v0, LX/00a;->o:LX/00b;

    sget-object v4, LX/00b;->NONE:LX/00b;

    if-ne v1, v4, :cond_1

    iget-boolean v1, v0, LX/00a;->l:Z

    if-eqz v1, :cond_1

    .line 104479
    invoke-static {v0}, LX/00a;->j(LX/00a;)V

    .line 104480
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yi;

    iget-wide v4, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->U:J

    .line 104481
    invoke-static {v0, v4, v5}, LX/0Yi;->c(LX/0Yi;J)V

    .line 104482
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->N:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hv;

    .line 104483
    invoke-virtual {v0}, LX/0hv;->a()V

    .line 104484
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->M:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wd;

    .line 104485
    new-instance v1, Lcom/facebook/katana/activity/FbMainTabActivity$1;

    invoke-direct {v1, p0}, Lcom/facebook/katana/activity/FbMainTabActivity$1;-><init>(Lcom/facebook/katana/activity/FbMainTabActivity;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 104486
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->L:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hw;

    .line 104487
    invoke-virtual {v0}, LX/0hw;->a()V

    .line 104488
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->K:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hx;

    .line 104489
    invoke-virtual {v0}, LX/0hx;->a()V

    .line 104490
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ah:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "cold_start"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 104491
    :cond_2
    const-string v0, "MainTabActivityEndToEnd"

    iget-wide v4, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->U:J

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->V:LX/00q;

    invoke-direct {p0, v0, v4, v5, v1}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(Ljava/lang/String;JLX/00q;)V

    .line 104492
    const-string v0, "MainTabActivityPreOnCreate"

    iget-wide v4, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->U:J

    invoke-direct {p0, v0, v4, v5}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(Ljava/lang/String;J)V

    .line 104493
    const-string v0, "MainTabActivityPreOnCreate"

    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/katana/activity/FbMainTabActivity;->b(Ljava/lang/String;J)V

    .line 104494
    const-string v0, "MainTabActivityOnCreate"

    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(Ljava/lang/String;J)V

    .line 104495
    const-string v0, "MainTabActivityViewInflation"

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->c(Ljava/lang/String;)V

    .line 104496
    const-string v0, "FbMainTabActivity.setContentView"

    const v1, 0x24537a2b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 104497
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->J:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gw;

    .line 104498
    :try_start_0
    invoke-virtual {v0}, LX/0gw;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 104499
    const v0, 0x7f030a65

    .line 104500
    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->setContentView(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104501
    const v0, 0xb98e089

    invoke-static {v0}, LX/02m;->a(I)V

    .line 104502
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ak()V

    .line 104503
    if-nez p1, :cond_a

    .line 104504
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->al()V

    .line 104505
    :cond_3
    :goto_2
    const-string v0, "MainTabActivityViewInflation"

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->d(Ljava/lang/String;)V

    .line 104506
    const-string v0, "MainTabActivityOnCreatePostInflation"

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->c(Ljava/lang/String;)V

    .line 104507
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->I:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 104508
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aa:LX/0gp;

    invoke-virtual {v0}, LX/0gp;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 104509
    const v0, 0x7f0d18db

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    .line 104510
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->H:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hI;

    .line 104511
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v0

    .line 104512
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->am()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 104513
    iget-object v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aa:LX/0gp;

    const v3, 0x7f0d00bc

    invoke-virtual {p0, v3}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v3

    new-array v4, v7, [Landroid/view/View;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v6, v0, v4}, LX/0gp;->a(Landroid/view/View;II[Landroid/view/View;)V

    .line 104514
    :goto_3
    iget-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aH:Z

    if-eqz v0, :cond_4

    .line 104515
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aa:LX/0gp;

    new-array v1, v8, [Landroid/view/View;

    const v2, 0x7f0d1a6e

    invoke-virtual {p0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v6

    const v2, 0x7f0d1a6f

    invoke-virtual {p0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, LX/0gp;->a([Landroid/view/View;)V

    .line 104516
    :cond_4
    invoke-direct {p0, p1}, Lcom/facebook/katana/activity/FbMainTabActivity;->d(Landroid/os/Bundle;)V

    .line 104517
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->I()V

    .line 104518
    if-nez p1, :cond_e

    .line 104519
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 104520
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->Y:LX/0gn;

    invoke-virtual {v0, v1}, LX/0gn;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    .line 104521
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->G:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hy;

    .line 104522
    invoke-interface {v0, v1}, LX/0hy;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_5

    if-ne v2, v1, :cond_5

    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "faceweb_modal"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "extra_launch_uri"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 104523
    :cond_5
    invoke-direct {p0, v1}, Lcom/facebook/katana/activity/FbMainTabActivity;->d(Landroid/content/Intent;)V

    .line 104524
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->an:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0i0;

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    .line 104525
    iget-object v2, v1, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    move-object v1, v2

    .line 104526
    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-virtual {v2}, LX/0fd;->f()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0i0;->a(Landroid/view/ViewGroup;Ljava/util/List;)V

    .line 104527
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    invoke-virtual {v0}, LX/0gx;->g()V

    .line 104528
    iput-boolean v7, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aJ:Z

    .line 104529
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ac:LX/0fU;

    new-instance v1, LX/0i2;

    invoke-direct {v1, p0}, LX/0i2;-><init>(Lcom/facebook/katana/activity/FbMainTabActivity;)V

    .line 104530
    iput-object v1, v0, LX/0fU;->l:LX/0i3;

    .line 104531
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    invoke-virtual {v0, p0, p1}, LX/0hr;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 104532
    const-string v0, "MainTabActivityOnCreatePostInflation"

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->d(Ljava/lang/String;)V

    .line 104533
    const-string v0, "MainTabActivityOnCreate"

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->d(Ljava/lang/String;)V

    .line 104534
    return-void

    .line 104535
    :cond_7
    :try_start_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ae:LX/0W3;

    sget-wide v2, LX/0X5;->cd:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 104536
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aH:Z

    .line 104537
    const v0, 0x7f030a64

    goto/16 :goto_1

    .line 104538
    :cond_8
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aa:LX/0gp;

    invoke-virtual {v0}, LX/0gp;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->an()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 104539
    const v0, 0x7f030a66

    goto/16 :goto_1

    .line 104540
    :cond_9
    const v0, 0x7f030a63
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 104541
    :catchall_0
    move-exception v0

    const v1, -0x43a13a49

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 104542
    :cond_a
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "FbMainTabFragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 104543
    instance-of v1, v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    if-eqz v1, :cond_3

    .line 104544
    check-cast v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    goto/16 :goto_2

    .line 104545
    :cond_b
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->an()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 104546
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aa:LX/0gp;

    const v2, 0x7f0d1d0b

    invoke-virtual {p0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v2

    new-array v3, v8, [Landroid/view/View;

    const v4, 0x7f0d00bc

    invoke-virtual {p0, v4}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v6

    const v4, 0x7f0d1d0b

    invoke-virtual {p0, v4}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v0, v0, v3}, LX/0gp;->a(Landroid/view/View;II[Landroid/view/View;)V

    goto/16 :goto_3

    .line 104547
    :cond_c
    iget-object v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aa:LX/0gp;

    const/4 v3, 0x0

    new-array v4, v7, [Landroid/view/View;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v6, v0, v4}, LX/0gp;->a(Landroid/view/View;II[Landroid/view/View;)V

    goto/16 :goto_3

    .line 104548
    :cond_d
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->Y()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 104549
    invoke-virtual {p0, v6}, Lcom/facebook/katana/activity/FbMainTabActivity;->b(Z)V

    goto/16 :goto_4

    .line 104550
    :cond_e
    const-string v0, "open_right_divebar"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 104551
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->B()V

    goto/16 :goto_4

    .line 104552
    :cond_f
    const-string v0, "open_left_divebar"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 104553
    invoke-virtual {p0, v6}, Lcom/facebook/katana/activity/FbMainTabActivity;->b(Z)V

    goto/16 :goto_4

    .line 104554
    :cond_10
    iget-object v2, v0, LX/0hu;->k:LX/0i4;

    iget-object v3, v0, LX/0hu;->l:Landroid/app/Activity;

    invoke-virtual {v2, v3}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v2

    sget-object v3, LX/0hu;->b:[Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v3

    .line 104555
    iget-object v2, v0, LX/0hu;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    .line 104556
    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->r(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)Z

    .line 104557
    iget-object v4, v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    move-object v2, v4

    .line 104558
    invoke-static {v1}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v4

    invoke-virtual {v4}, LX/0i8;->h()Z

    move-result v4

    move v1, v4

    .line 104559
    invoke-static {}, LX/0i9;->a()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 104560
    if-eqz v1, :cond_11

    sget-object v1, LX/0gH;->OPEN_TO_LEFT_DIVEBAR:LX/0gH;

    goto/16 :goto_0

    :cond_11
    sget-object v1, LX/0gH;->HAS_LEFT_DIVEBAR:LX/0gH;

    goto/16 :goto_0

    .line 104561
    :cond_12
    if-nez v3, :cond_13

    sget-object v3, LX/0i7;->FETCHED:LX/0i7;

    if-eq v2, v3, :cond_13

    .line 104562
    invoke-static {v0, v1}, LX/0hu;->a(LX/0hu;Z)LX/0gH;

    move-result-object v1

    goto/16 :goto_0

    .line 104563
    :cond_13
    iget-object v3, v0, LX/0hu;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0iA;

    new-instance v4, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v5, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAMERA:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v4, v5}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v5, LX/0iB;

    invoke-virtual {v3, v4, v5}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v3

    check-cast v3, LX/0iB;

    .line 104564
    if-eqz v3, :cond_17

    const/4 v3, 0x1

    :goto_5
    move v3, v3

    .line 104565
    if-nez v3, :cond_15

    .line 104566
    if-eqz v1, :cond_14

    sget-object v1, LX/0gH;->OPEN_TO_LEFT_DIVEBAR:LX/0gH;

    goto/16 :goto_0

    :cond_14
    sget-object v1, LX/0gH;->HAS_LEFT_DIVEBAR:LX/0gH;

    goto/16 :goto_0

    .line 104567
    :cond_15
    sget-object v3, LX/0iC;->a:[I

    invoke-virtual {v2}, LX/0i7;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 104568
    sget-object v1, LX/0gH;->NO_LEFT_DIVEBAR:LX/0gH;

    goto/16 :goto_0

    .line 104569
    :pswitch_0
    invoke-static {v0, v1}, LX/0hu;->a(LX/0hu;Z)LX/0gH;

    move-result-object v1

    goto/16 :goto_0

    .line 104570
    :pswitch_1
    sget-object v1, LX/0gH;->NO_LEFT_DIVEBAR:LX/0gH;

    goto/16 :goto_0

    .line 104571
    :pswitch_2
    if-eqz v1, :cond_16

    sget-object v1, LX/0gH;->OPEN_TO_LEFT_DIVEBAR:LX/0gH;

    goto/16 :goto_0

    :cond_16
    sget-object v1, LX/0gH;->HAS_LEFT_DIVEBAR:LX/0gH;

    goto/16 :goto_0

    :cond_17
    const/4 v3, 0x0

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 1

    .prologue
    .line 104458
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(Lcom/facebook/friending/jewel/FriendRequestsFragment;Lcom/facebook/katana/fragment/FbChromeFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104459
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    invoke-virtual {v0}, LX/0hr;->b()V

    .line 104460
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 104446
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    if-eqz v0, :cond_0

    .line 104447
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    .line 104448
    iget-object v1, v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    .line 104449
    iget-object v2, v1, LX/0hh;->g:LX/0Ot;

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/0hh;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/0hh;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0iD;

    invoke-virtual {v2}, LX/0hD;->kw_()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104450
    iget-object v2, v1, LX/0hh;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0iD;

    .line 104451
    :try_start_0
    iget-object p0, v2, LX/0iD;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0go;

    invoke-virtual {p0, p1}, LX/0go;->a(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object p0

    .line 104452
    const/4 v0, 0x0

    invoke-static {v2, p0, v0}, LX/0iD;->a(LX/0iD;Lcom/facebook/apptab/state/TabTag;I)V

    .line 104453
    sget-object v0, Lcom/facebook/marketplace/tab/MarketplaceTab;->m:Lcom/facebook/marketplace/tab/MarketplaceTab;

    if-ne p0, v0, :cond_0

    .line 104454
    iget-object p0, v2, LX/0iD;->g:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0iE;

    invoke-interface {p0}, LX/0iE;->b()V
    :try_end_0
    .catch LX/0iF; {:try_start_0 .. :try_end_0} :catch_0

    .line 104455
    :cond_0
    :goto_0
    return-void

    .line 104456
    :catch_0
    move-exception p0

    move-object v0, p0

    .line 104457
    iget-object p0, v2, LX/0iD;->l:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/03V;

    const-string v1, "tab manager"

    invoke-virtual {p0, v1, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 104442
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->A()V

    .line 104443
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->setRequestedOrientation(I)V

    .line 104444
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0, p1}, LX/0fK;->a_(Z)V

    .line 104445
    return-void
.end method

.method public final c()Lcom/facebook/base/fragment/FbFragment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 104441
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 104377
    const-string v0, "reset_feed_view"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 104378
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104379
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->r()V

    .line 104380
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->W()V

    .line 104381
    :cond_1
    :goto_0
    return-void

    .line 104382
    :cond_2
    const-string v0, "jump_to_top"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104383
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->T()V

    goto :goto_0
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 104389
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 104390
    const-string v0, "FbMainTabActivity.injectMe"

    const v1, -0x24d9ff41

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 104391
    :try_start_0
    invoke-static {p0, p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104392
    const v0, 0x204b5409

    invoke-static {v0}, LX/02m;->a(I)V

    .line 104393
    return-void

    .line 104394
    :catchall_0
    move-exception v0

    const v1, 0x4d3f7232    # 2.0074576E8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 104395
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    if-eqz v0, :cond_0

    .line 104396
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    .line 104397
    iget-object p0, v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    if-nez p0, :cond_1

    .line 104398
    :cond_0
    :goto_0
    return-void

    .line 104399
    :cond_1
    iget-object p0, v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    .line 104400
    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iG;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 104401
    iget-object v0, p0, LX/0hh;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iG;

    invoke-virtual {v0, p1}, LX/0iG;->a(Z)V

    .line 104402
    :cond_2
    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 104403
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aL:Z

    .line 104404
    return-void
.end method

.method public final e()LX/0fK;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 104405
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()LX/0fK;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 104406
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 104407
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    invoke-virtual {v0}, LX/0hC;->b()Z

    move-result v0

    return v0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104408
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 104409
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v1

    .line 104410
    if-eqz v1, :cond_0

    .line 104411
    invoke-virtual {v1}, Lcom/facebook/katana/fragment/FbChromeFragment;->getDebugInfo()Ljava/util/Map;

    move-result-object v1

    .line 104412
    if-eqz v1, :cond_0

    .line 104413
    invoke-virtual {v0, v1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 104414
    :cond_0
    const-string v1, "harrison_fragment_stacks"

    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 104415
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/0iH;
    .locals 1

    .prologue
    .line 104416
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->mo_()LX/0iH;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/0hE;
    .locals 2

    .prologue
    .line 104417
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 104418
    iget-object v1, v0, LX/0hC;->f:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 104419
    iget-object v1, v0, LX/0hC;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iI;

    iget-object p0, v0, LX/0hC;->f:Landroid/app/Activity;

    invoke-virtual {v1, p0}, LX/0iI;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v1

    iput-object v1, v0, LX/0hC;->a:LX/0hE;

    .line 104420
    :cond_0
    iget-object v1, v0, LX/0hC;->a:LX/0hE;

    move-object v0, v1

    .line 104421
    return-object v0
.end method

.method public final i_(Z)V
    .locals 1

    .prologue
    .line 104422
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ab:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104423
    invoke-direct {p0, p1}, Lcom/facebook/katana/activity/FbMainTabActivity;->d(Z)V

    .line 104424
    :cond_0
    return-void
.end method

.method public final j()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 104425
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->G()Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    move-result-object v0

    .line 104426
    if-eqz v0, :cond_0

    .line 104427
    iget-object p0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    if-eqz p0, :cond_1

    iget-object p0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    .line 104428
    iget-object v0, p0, LX/0iJ;->w:Landroid/view/View$OnClickListener;

    move-object p0, v0

    .line 104429
    :goto_0
    move-object v0, p0

    .line 104430
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final k()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 104431
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->G()Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    move-result-object v0

    .line 104432
    if-eqz v0, :cond_0

    .line 104433
    iget-object p0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    if-eqz p0, :cond_1

    iget-object p0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    .line 104434
    iget-object v0, p0, LX/0iJ;->x:Landroid/view/View$OnClickListener;

    move-object p0, v0

    .line 104435
    :goto_0
    move-object v0, p0

    .line 104436
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final l()LX/0hE;
    .locals 2

    .prologue
    .line 104384
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 104385
    iget-object v1, v0, LX/0hC;->f:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 104386
    iget-object v1, v0, LX/0hC;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iL;

    iget-object p0, v0, LX/0hC;->f:Landroid/app/Activity;

    invoke-virtual {v1, p0}, LX/0iL;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v1

    iput-object v1, v0, LX/0hC;->a:LX/0hE;

    .line 104387
    :cond_0
    iget-object v1, v0, LX/0hC;->a:LX/0hE;

    move-object v0, v1

    .line 104388
    return-object v0
.end method

.method public final m()LX/0hE;
    .locals 2

    .prologue
    .line 104772
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 104773
    iget-object v1, v0, LX/0hC;->f:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 104774
    iget-object v1, v0, LX/0hC;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iM;

    iget-object p0, v0, LX/0hC;->f:Landroid/app/Activity;

    invoke-virtual {v1, p0}, LX/0iM;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v1

    iput-object v1, v0, LX/0hC;->a:LX/0hE;

    .line 104775
    :cond_0
    iget-object v1, v0, LX/0hC;->a:LX/0hE;

    move-object v0, v1

    .line 104776
    return-object v0
.end method

.method public final mn_()LX/0iO;
    .locals 1

    .prologue
    .line 104946
    new-instance v0, LX/0iN;

    invoke-direct {v0, p0}, LX/0iN;-><init>(Lcom/facebook/katana/activity/FbMainTabActivity;)V

    return-object v0
.end method

.method public final mo_()LX/0iH;
    .locals 4

    .prologue
    .line 104945
    new-instance v1, LX/0iH;

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, LX/0iP;->a(Landroid/view/ViewConfiguration;)I

    move-result v2

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->as:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    int-to-float v3, v0

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-direct {v1, v2, v3, v0}, LX/0iH;-><init>(IFLX/0fK;)V

    return-object v1
.end method

.method public final n()Lcom/facebook/apptab/state/TabTag;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 104942
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    if-nez v0, :cond_0

    .line 104943
    const/4 v0, 0x0

    .line 104944
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    invoke-virtual {v0}, LX/0gx;->e()Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()LX/0hE;
    .locals 2

    .prologue
    .line 104937
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 104938
    iget-object v1, v0, LX/0hC;->f:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 104939
    iget-object v1, v0, LX/0hC;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iQ;

    iget-object p0, v0, LX/0hC;->f:Landroid/app/Activity;

    invoke-virtual {v1, p0}, LX/0iQ;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v1

    iput-object v1, v0, LX/0hC;->a:LX/0hE;

    .line 104940
    :cond_0
    iget-object v1, v0, LX/0hC;->a:LX/0hE;

    move-object v0, v1

    .line 104941
    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 104922
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 104923
    const/16 v0, 0x22b2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ar:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x4c1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104924
    const v0, 0x7f0d0037

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 104925
    new-instance v1, LX/0hs;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 104926
    const/4 v2, -0x1

    .line 104927
    iput v2, v1, LX/0hs;->t:I

    .line 104928
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082344

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 104929
    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 104930
    :cond_0
    const v0, 0xffff

    and-int/2addr v0, p1

    .line 104931
    const/16 v1, 0x6dc

    if-ne v0, v1, :cond_1

    .line 104932
    invoke-direct {p0, p2, p3}, Lcom/facebook/katana/activity/FbMainTabActivity;->a(ILandroid/content/Intent;)V

    .line 104933
    :cond_1
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v1

    .line 104934
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_3

    .line 104935
    :cond_2
    :goto_0
    return-void

    .line 104936
    :cond_3
    invoke-virtual {v1, v0, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 3

    .prologue
    .line 104918
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onAttachedToWindow()V

    .line 104919
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->W:LX/0ad;

    sget-short v1, LX/0iR;->c:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104920
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setFormat(I)V

    .line 104921
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 104898
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ah:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_back_button"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 104899
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->fU_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104900
    :cond_0
    :goto_0
    return-void

    .line 104901
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->fU_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104902
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 104903
    iget-object v1, v0, LX/0hC;->a:LX/0hE;

    if-eqz v1, :cond_4

    .line 104904
    iget-object v1, v0, LX/0hC;->a:LX/0hE;

    invoke-interface {v1}, LX/0hE;->b()Z

    move-result v1

    .line 104905
    :goto_1
    move v0, v1

    .line 104906
    if-nez v0, :cond_0

    .line 104907
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->af()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104908
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104909
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ad()Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 104910
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ad()Lcom/facebook/apptab/state/TabTag;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0fd;->d(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0gg;->a(I)V

    .line 104911
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iS;

    .line 104912
    invoke-virtual {v0}, LX/0iS;->a()V

    goto :goto_0

    .line 104913
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 104914
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->c:Z

    move v0, v0

    .line 104915
    if-nez v0, :cond_3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->moveTaskToBack(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104916
    sget-object v0, LX/03R;->YES:LX/03R;

    iput-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aw:LX/03R;

    goto :goto_0

    .line 104917
    :cond_3
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 104888
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 104889
    iget-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aJ:Z

    if-eqz v0, :cond_0

    .line 104890
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 104891
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    .line 104892
    iget-object v1, v0, LX/0gx;->m:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 104893
    const p1, 0x7f0b0413

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 104894
    iget-object p1, v0, LX/0gx;->w:Lcom/facebook/widget/CustomLinearLayout;

    invoke-static {p1, v1}, LX/0iT;->a(Landroid/view/View;I)V

    .line 104895
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ah()V

    .line 104896
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ag()V

    .line 104897
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, -0x251ad68f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 104871
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    if-eqz v0, :cond_0

    .line 104872
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    invoke-virtual {v0, p0}, LX/0hr;->a(Landroid/app/Activity;)V

    .line 104873
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aC:LX/0Yb;

    if-eqz v0, :cond_1

    .line 104874
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aC:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 104875
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ac:LX/0fU;

    if-eqz v0, :cond_2

    .line 104876
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ac:LX/0fU;

    invoke-virtual {v0}, LX/0fU;->b()V

    .line 104877
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ar:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ar:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v2, 0x4b2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104878
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->an:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0i0;

    .line 104879
    iget-object v2, v0, LX/0i0;->b:LX/0iU;

    .line 104880
    invoke-virtual {v2}, LX/0iV;->h()V

    .line 104881
    const/4 v0, 0x0

    iput-object v0, v2, LX/0iV;->c:Landroid/view/ViewGroup;

    .line 104882
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aa:LX/0gp;

    const/4 v2, 0x0

    .line 104883
    iput-object v2, v0, LX/0gp;->d:LX/0iW;

    .line 104884
    iput-object v2, v0, LX/0gp;->e:LX/0iW;

    .line 104885
    sget-object v2, LX/0gp;->a:Ljava/lang/ref/WeakReference;

    iput-object v2, v0, LX/0gp;->b:Ljava/lang/ref/WeakReference;

    .line 104886
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 104887
    const/16 v0, 0x23

    const v2, -0x7ffd4e7e

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    .line 104854
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    invoke-virtual {v0, p1, p2}, LX/0hr;->a(ILandroid/view/KeyEvent;)V

    .line 104855
    const/16 v0, 0x19

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_5

    .line 104856
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iX;

    .line 104857
    iget v1, v0, LX/0iX;->o:I

    iget-object v2, v0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v2}, LX/0iY;->m()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 104858
    iget-object v2, v0, LX/0iX;->d:LX/0iZ;

    iget-object v1, v0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v1}, LX/0iY;->m()I

    move-result v1

    iget v3, v0, LX/0iX;->o:I

    if-le v1, v3, :cond_4

    sget-object v1, LX/0ia;->VOLUME_INCREASE:LX/0ia;

    :goto_0
    invoke-virtual {v2, v1}, LX/0iZ;->a(LX/0ia;)V

    .line 104859
    iget-object v1, v0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v1}, LX/0iY;->m()I

    move-result v1

    iput v1, v0, LX/0iX;->o:I

    .line 104860
    :cond_1
    iget-boolean v1, v0, LX/0iX;->e:Z

    if-eqz v1, :cond_2

    const/16 v1, 0x19

    if-eq p1, v1, :cond_3

    :cond_2
    iget-boolean v1, v0, LX/0iX;->f:Z

    if-eqz v1, :cond_5

    const/16 v1, 0x18

    if-ne p1, v1, :cond_5

    .line 104861
    :cond_3
    iget-object v1, v0, LX/0iX;->k:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ib;

    .line 104862
    invoke-interface {v1}, LX/0ib;->g()V

    goto :goto_1

    .line 104863
    :cond_4
    sget-object v1, LX/0ia;->VOLUME_DECREASE:LX/0ia;

    goto :goto_0

    .line 104864
    :cond_5
    const/16 v0, 0x52

    if-ne p1, v0, :cond_6

    .line 104865
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    .line 104866
    if-eqz v0, :cond_6

    .line 104867
    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 104868
    instance-of v1, v0, LX/0ic;

    if-eqz v1, :cond_6

    check-cast v0, LX/0ic;

    invoke-interface {v0}, LX/0ic;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 104869
    const/4 v0, 0x1

    .line 104870
    :goto_2
    return v0

    :cond_6
    invoke-super {p0, p1, p2}, Lcom/facebook/base/activity/FbFragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_2
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x22

    const v1, -0x52719bf4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 104841
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    sget-object v2, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0id;->g(Ljava/lang/String;)V

    .line 104842
    iget-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aL:Z

    if-nez v0, :cond_0

    .line 104843
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yi;

    invoke-virtual {v0}, LX/0Yi;->a()V

    .line 104844
    :cond_0
    iput-boolean v3, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ay:Z

    .line 104845
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 104846
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    .line 104847
    iput-boolean v3, v0, LX/0gx;->y:Z

    .line 104848
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    invoke-virtual {v0, p0}, LX/0hr;->c(Landroid/app/Activity;)V

    .line 104849
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    sget-object v2, LX/03R;->UNSET:LX/03R;

    .line 104850
    iput-object v2, v0, LX/0fd;->g:LX/03R;

    .line 104851
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    sget-object v2, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0id;->h(Ljava/lang/String;)V

    .line 104852
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ab()V

    .line 104853
    const/16 v0, 0x23

    const v2, 0x69cef790

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 13

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x4ddf836a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 104789
    const-string v0, "MainTabActivityOnResume"

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->c(Ljava/lang/String;)V

    .line 104790
    const-string v0, "FbMainTabActivity.onResume"

    const v2, -0x182f930e

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 104791
    :try_start_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 104792
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aj()V

    .line 104793
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->S()V

    .line 104794
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    invoke-virtual {v0}, LX/0gx;->g()V

    .line 104795
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104796
    invoke-static {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->U(Lcom/facebook/katana/activity/FbMainTabActivity;)V

    .line 104797
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aA:Z

    .line 104798
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ay:Z

    .line 104799
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    invoke-virtual {v0, p0}, LX/0hr;->b(Landroid/app/Activity;)V

    .line 104800
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->J()V

    .line 104801
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aD:LX/0h2;

    if-eqz v0, :cond_1

    .line 104802
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aD:LX/0h2;

    const/4 v3, 0x0

    .line 104803
    iget-object v2, v0, LX/0h2;->r:LX/0fd;

    invoke-virtual {v2}, LX/0fd;->h()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v2

    .line 104804
    if-nez v2, :cond_6

    .line 104805
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aa:LX/0gp;

    invoke-virtual {v0}, LX/0gp;->c()V

    .line 104806
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->Z()V

    .line 104807
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aE:LX/0gx;

    iget-object v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-virtual {v2}, LX/0fd;->i()I

    move-result v2

    invoke-virtual {v0, v2}, LX/0gx;->a(I)V

    .line 104808
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->y:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ie;

    .line 104809
    iget-object v2, v0, LX/0ie;->a:LX/0if;

    sget-object v3, LX/0ig;->ar:LX/0ih;

    invoke-virtual {v2, v3}, LX/0if;->c(LX/0ih;)V

    .line 104810
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->x:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ii;

    .line 104811
    invoke-virtual {v0}, LX/0ii;->a()V

    .line 104812
    iget-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aL:Z

    if-nez v0, :cond_4

    .line 104813
    iget-boolean v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aM:Z

    if-eqz v0, :cond_2

    .line 104814
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yi;

    invoke-virtual {v0}, LX/0Yi;->a()V

    .line 104815
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->z()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 104816
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yi;

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 104817
    iget-object v2, v0, LX/0Yi;->j:LX/0Yl;

    const/4 v3, 0x7

    new-array v3, v3, [LX/0Yj;

    new-instance v4, LX/0Yj;

    const v5, 0xa0001

    const-string v6, "NNFColdStart"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v8

    new-instance v4, LX/0Yj;

    const v5, 0xa003c

    const-string v6, "NNFColdStartChromeLoadTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v9

    new-instance v4, LX/0Yj;

    const v5, 0xa0016

    const-string v6, "NNFColdStartTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v10

    new-instance v4, LX/0Yj;

    const v5, 0xa004d

    const-string v6, "NNFColdStartNetwork"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v11

    new-instance v4, LX/0Yj;

    const v5, 0xa0020

    const-string v6, "NNFColdStartAndRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v12

    const/4 v4, 0x5

    new-instance v5, LX/0Yj;

    const v6, 0xa0034

    const-string v7, "NNF_PermalinkFromAndroidNotificationColdLoad"

    invoke-direct {v5, v6, v7}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x6

    new-instance v5, LX/0Yj;

    const v6, 0xa003a

    const-string v7, "NNFFirstRunColdStart"

    invoke-direct {v5, v6, v7}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v5, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 104818
    invoke-static {v2, v3}, LX/0Yl;->d(LX/0Yl;Ljava/util/List;)V

    .line 104819
    iget-object v2, v0, LX/0Yi;->j:LX/0Yl;

    const/16 v3, 0x8

    new-array v3, v3, [LX/0Yj;

    new-instance v4, LX/0Yj;

    const v5, 0xa0013

    const-string v6, "NNFWarmStartTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v8

    new-instance v4, LX/0Yj;

    const v5, 0xa001e

    const-string v6, "NNFWarmStart"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v9

    new-instance v4, LX/0Yj;

    const v5, 0xa0023

    const-string v6, "NNFWarmStartAndRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v10

    new-instance v4, LX/0Yj;

    const v5, 0xa0024

    const-string v6, "NNFWarmStartAndFreshRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v11

    new-instance v4, LX/0Yj;

    const v5, 0xa0025

    const-string v6, "NNFWarmStartAndCachedRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v3, v12

    const/4 v4, 0x5

    new-instance v5, LX/0Yj;

    const v6, 0xa0004

    const-string v7, "NNFWarmStart"

    invoke-direct {v5, v6, v7}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x6

    new-instance v5, LX/0Yj;

    const v6, 0xa0031

    const-string v7, "NNF_PermalinkFromAndroidNotificationWarmLoad"

    invoke-direct {v5, v6, v7}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x7

    new-instance v5, LX/0Yj;

    const v6, 0xa0014

    const-string v7, "NNFFreshContentStart"

    invoke-direct {v5, v6, v7}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v5, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 104820
    invoke-static {v2, v3}, LX/0Yl;->d(LX/0Yl;Ljava/util/List;)V

    .line 104821
    :goto_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yi;

    const/4 v7, 0x0

    const v5, 0xa003c

    .line 104822
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    const v4, 0xa0001

    const-string v6, "NNFColdStart"

    invoke-virtual {v3, v4, v6}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 104823
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    const v4, 0xa005a

    const-string v6, "NNFCold_MaintabCreateToFeedCreate"

    const/4 v8, 0x1

    invoke-virtual {v3, v4, v6, v8}, LX/0Yl;->a(ILjava/lang/String;Z)LX/0Yl;

    .line 104824
    :cond_3
    :goto_2
    invoke-static {v0}, LX/0Yi;->L(LX/0Yi;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 104825
    iget-object v3, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v4, "NNFColdStartChromeLoadTime"

    invoke-interface {v3, v5, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 104826
    :cond_4
    :goto_3
    const-string v0, "MainTabActivityOnResume"

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->d(Ljava/lang/String;)V

    .line 104827
    const-string v0, "MainTabActivityEndToEnd"

    invoke-direct {p0, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104828
    const v0, -0x3fb140d8

    invoke-static {v0}, LX/02m;->a(I)V

    .line 104829
    const v0, -0x21d83dde

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 104830
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yi;

    invoke-virtual {v0}, LX/0Yi;->q()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 104831
    :catchall_0
    move-exception v0

    const v2, 0x40b61259

    invoke-static {v2}, LX/02m;->a(I)V

    const v2, 0x5f6e43ab

    invoke-static {v2, v1}, LX/02F;->c(II)V

    throw v0

    .line 104832
    :cond_6
    :try_start_2
    iget-object v2, v0, LX/0h2;->r:LX/0fd;

    .line 104833
    iget-boolean v4, v2, LX/0fd;->f:Z

    move v2, v4

    .line 104834
    if-eqz v2, :cond_1

    .line 104835
    iget-object v2, v0, LX/0h2;->r:LX/0fd;

    .line 104836
    iput-boolean v3, v2, LX/0fd;->f:Z

    .line 104837
    iget-object v2, v0, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v2, v3}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->b(Z)V

    goto/16 :goto_0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104838
    :cond_7
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    const v4, 0xa0004

    const-string v6, "NNFWarmStart"

    invoke-virtual {v3, v4, v6}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 104839
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    const v4, 0xa005b

    const-string v6, "NNFWarm_MaintabCreateToFeedCreate"

    invoke-virtual {v3, v4, v6}, LX/0Yl;->b(ILjava/lang/String;)LX/0Yl;

    goto :goto_2

    .line 104840
    :cond_8
    iget-object v4, v0, LX/0Yi;->j:LX/0Yl;

    const-string v6, "NNFColdStartChromeLoadTime"

    iget-object v3, v0, LX/0Yi;->k:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v9

    move-object v8, v7

    invoke-virtual/range {v4 .. v10}, LX/0Yl;->b(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    goto :goto_3
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 104780
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ar:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x252

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104781
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    if-eqz v0, :cond_0

    .line 104782
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->s()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->ad()Lcom/facebook/apptab/state/TabTag;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    .line 104783
    iget-object v3, v0, LX/0gg;->r:LX/0hP;

    if-eqz v3, :cond_0

    .line 104784
    iget-object v3, v0, LX/0gg;->r:LX/0hP;

    invoke-virtual {v3, v1, v2}, LX/0hP;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104785
    :cond_0
    const-string v1, "open_right_divebar"

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->b()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 104786
    const-string v1, "open_left_divebar"

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fK;

    invoke-virtual {v0}, LX/0fK;->b()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 104787
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 104788
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x593a9b17

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 104777
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 104778
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    invoke-virtual {v1, p0}, LX/0hr;->d(Landroid/app/Activity;)V

    .line 104779
    const/16 v1, 0x23

    const v2, 0x661caf69

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/16 v2, 0x22

    const v3, 0x5919d7d

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 104621
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    .line 104622
    iput-boolean v0, v1, LX/0fd;->h:Z

    .line 104623
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aN:LX/0gs;

    .line 104624
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    move v1, v0

    .line 104625
    :goto_0
    if-eqz v1, :cond_0

    .line 104626
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    sget-object v3, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0id;->i(Ljava/lang/String;)V

    .line 104627
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 104628
    if-eqz v1, :cond_1

    .line 104629
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    sget-object v1, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->j(Ljava/lang/String;)V

    .line 104630
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    if-eqz v0, :cond_2

    .line 104631
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    invoke-virtual {v0, p0}, LX/0hr;->e(Landroid/app/Activity;)V

    .line 104632
    :cond_2
    const v0, 0x33df6c22

    invoke-static {v0, v2}, LX/02F;->c(II)V

    return-void

    :cond_3
    move v1, v0

    .line 104633
    goto :goto_0
.end method

.method public final onUserInteraction()V
    .locals 3

    .prologue
    .line 104764
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onUserInteraction()V

    .line 104765
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->G()Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    move-result-object v0

    .line 104766
    if-eqz v0, :cond_0

    .line 104767
    const/4 p0, 0x1

    .line 104768
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    invoke-interface {v1}, LX/0iO;->d()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0it;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104769
    :cond_0
    :goto_0
    return-void

    .line 104770
    :cond_1
    iget v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ax:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ax:I

    if-le v1, p0, :cond_0

    .line 104771
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->c()LX/0jJ;

    move-result-object v1

    sget-object v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1, p0}, LX/0jL;->i(Z)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 104759
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->X:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hC;

    .line 104760
    invoke-virtual {v0}, LX/0hC;->b()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 104761
    iget-object p0, v0, LX/0hC;->a:LX/0hE;

    invoke-interface {p0}, LX/0hE;->b()Z

    move-result p0

    .line 104762
    :goto_0
    move v0, p0

    .line 104763
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 104753
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    .line 104754
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    move v0, v2

    .line 104755
    :goto_0
    return v0

    .line 104756
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v3

    instance-of v3, v3, Lcom/facebook/feed/fragment/NewsFeedFragment;

    if-eqz v3, :cond_2

    move v0, v1

    .line 104757
    goto :goto_0

    .line 104758
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v3

    instance-of v3, v3, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 104745
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->f()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v1, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0gg;->a(I)V

    .line 104746
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    .line 104747
    iput-object v1, v0, LX/0fd;->g:LX/03R;

    .line 104748
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104749
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    .line 104750
    invoke-virtual {v0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104751
    :cond_0
    :goto_0
    return-void

    .line 104752
    :cond_1
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a$redex0(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;I)V

    goto :goto_0
.end method

.method public final s()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 104742
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    if-nez v0, :cond_0

    .line 104743
    const/4 v0, 0x0

    .line 104744
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/high16 v3, 0x10000

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 104695
    iget-object v2, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    move v2, v0

    .line 104696
    :goto_0
    if-eqz v2, :cond_0

    .line 104697
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    sget-object v1, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->q(Ljava/lang/String;)V

    .line 104698
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ao:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ao:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jM;

    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, p0, p1, v1}, LX/0jM;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    .line 104699
    if-eqz v2, :cond_1

    .line 104700
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    sget-object v1, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->r(Ljava/lang/String;)V

    .line 104701
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v2, v1

    .line 104702
    goto :goto_0

    .line 104703
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aF:LX/0gg;

    .line 104704
    iget-object v4, v1, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    move-object v1, v4

    .line 104705
    goto :goto_1

    .line 104706
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->au:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 104707
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->au:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    invoke-virtual {v0, p1}, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->a(Landroid/content/Intent;)V

    .line 104708
    :cond_5
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ap:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    if-eqz p1, :cond_6

    .line 104709
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ap:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jN;

    invoke-virtual {v0, p1}, LX/0jN;->a(Landroid/content/Intent;)V

    .line 104710
    :cond_6
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    if-eqz v0, :cond_7

    .line 104711
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aO:Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;

    .line 104712
    iget-object v1, v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    if-nez v1, :cond_c

    .line 104713
    :cond_7
    :goto_3
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    if-eqz v0, :cond_9

    const-string v0, "target_fragment"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sget-object v1, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_9

    .line 104714
    const-string v0, "search_titles_app_diable_animation"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 104715
    const-string v0, "inflate_fragment_before_animation"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 104716
    const/high16 v0, 0x10000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 104717
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    const/4 v1, 0x1

    .line 104718
    iput-boolean v1, v0, LX/0fd;->f:Z

    .line 104719
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 104720
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/katana/activity/FbMainTabActivity;->overridePendingTransition(II)V

    .line 104721
    if-eqz v2, :cond_8

    .line 104722
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0id;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104723
    :cond_8
    :goto_4
    if-eqz v2, :cond_1

    .line 104724
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    sget-object v1, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->r(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 104725
    :cond_9
    :try_start_2
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    and-int/2addr v0, v3

    if-eqz v0, :cond_b

    .line 104726
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 104727
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/katana/activity/FbMainTabActivity;->overridePendingTransition(II)V

    .line 104728
    if-eqz v2, :cond_8

    .line 104729
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0id;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 104730
    :catchall_0
    move-exception v0

    move-object v1, v0

    if-eqz v2, :cond_a

    .line 104731
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    sget-object v2, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0id;->r(Ljava/lang/String;)V

    :cond_a
    throw v1

    .line 104732
    :cond_b
    :try_start_3
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 104733
    sget-object v0, LX/0jO;->SLIDE_LEFT_IN:LX/0jO;

    iget v1, v0, LX/0jO;->resource:I

    .line 104734
    sget-object v0, LX/0jO;->DROP_OUT:LX/0jO;

    iget v0, v0, LX/0jO;->resource:I

    .line 104735
    invoke-virtual {p0, v1, v0}, Lcom/facebook/katana/activity/FbMainTabActivity;->overridePendingTransition(II)V

    .line 104736
    if-eqz v2, :cond_8

    .line 104737
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    invoke-virtual {v0, v1}, LX/0id;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 104738
    :cond_c
    iget-object v1, v0, Lcom/facebook/katana/fragment/maintab/FbMainTabFragment;->b:LX/0hh;

    .line 104739
    iget-object v0, v1, LX/0hh;->i:LX/0Ot;

    if-eqz v0, :cond_d

    iget-object v0, v1, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, v1, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 104740
    iget-object v0, v1, LX/0hh;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jP;

    invoke-virtual {v0, p1}, LX/0jP;->a(Landroid/content/Intent;)V

    .line 104741
    :cond_d
    goto/16 :goto_3
.end method

.method public final startActivityForResult(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    .line 104687
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 104688
    :goto_0
    if-eqz v1, :cond_0

    .line 104689
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    sget-object v2, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0id;->s(Ljava/lang/String;)V

    .line 104690
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/base/activity/FbFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 104691
    if-eqz v1, :cond_1

    .line 104692
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->av:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0id;

    sget-object v1, Lcom/facebook/katana/activity/FbMainTabActivity;->R:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/0id;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 104693
    :cond_1
    return-void

    .line 104694
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method

.method public final t()V
    .locals 3

    .prologue
    .line 104673
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104674
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->X()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    .line 104675
    iget-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    if-eqz v1, :cond_0

    .line 104676
    iget-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 104677
    iget-object v2, v1, LX/0g2;->b:LX/0gC;

    if-nez v2, :cond_1

    .line 104678
    :cond_0
    :goto_0
    return-void

    .line 104679
    :cond_1
    iget-object v2, v1, LX/0g2;->c:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 104680
    iget-object v0, v1, LX/0g2;->p:LX/0jU;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0jU;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_1

    .line 104681
    :cond_2
    iget-object v2, v1, LX/0g2;->b:LX/0gC;

    instance-of v2, v2, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    if-eqz v2, :cond_0

    .line 104682
    iget-object v2, v1, LX/0g2;->b:LX/0gC;

    check-cast v2, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    .line 104683
    iget-object p0, v2, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    if-eqz p0, :cond_3

    .line 104684
    iget-object p0, v2, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    .line 104685
    invoke-static {p0}, LX/0jY;->m(LX/0jY;)Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b()V

    .line 104686
    :cond_3
    goto :goto_0
.end method

.method public final u()Ljava/lang/String;
    .locals 5

    .prologue
    .line 104653
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 104654
    const/4 v0, 0x1

    .line 104655
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->f()LX/0Px;

    move-result-object v1

    .line 104656
    if-eqz v1, :cond_4

    .line 104657
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    .line 104658
    if-nez v1, :cond_1

    .line 104659
    const/16 v1, 0xa

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 104660
    :cond_1
    const/4 v1, 0x0

    .line 104661
    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    .line 104662
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104663
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->s()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 104664
    const-string v4, " (current)"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104665
    :cond_2
    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104666
    iget-object v4, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aG:LX/0fd;

    invoke-virtual {v4, v0}, LX/0fd;->e(Ljava/lang/String;)Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    .line 104667
    if-eqz v0, :cond_0

    .line 104668
    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 104669
    if-eqz v0, :cond_3

    .line 104670
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 104671
    :cond_3
    const-string v0, "Chrome fragment loaded, content fragment not loaded."

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 104672
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 104650
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aq:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104651
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aq:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ja;

    invoke-virtual {v0}, LX/0ja;->a()V

    .line 104652
    :cond_0
    return-void
.end method

.method public final w()LX/0cQ;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 104645
    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->s()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 104646
    :cond_0
    :goto_0
    return-object v0

    .line 104647
    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->Z:LX/0go;

    invoke-virtual {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0go;->b(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v1

    .line 104648
    if-eqz v1, :cond_0

    .line 104649
    iget-object v0, v1, Lcom/facebook/apptab/state/TabTag;->b:LX/0cQ;

    goto :goto_0
.end method

.method public final x()LX/0gz;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 104641
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104642
    invoke-direct {p0}, Lcom/facebook/katana/activity/FbMainTabActivity;->aa()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    .line 104643
    iget-object p0, v0, Lcom/facebook/katana/fragment/FbChromeFragment;->x:LX/0gz;

    move-object v0, p0

    .line 104644
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()V
    .locals 1

    .prologue
    .line 104639
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->aK:LX/0hr;

    invoke-virtual {v0}, LX/0hr;->c()V

    .line 104640
    return-void
.end method

.method public final z()Z
    .locals 2

    .prologue
    .line 104638
    iget-object v0, p0, Lcom/facebook/katana/activity/FbMainTabActivity;->ax:LX/0gH;

    sget-object v1, LX/0gH;->NO_LEFT_DIVEBAR:LX/0gH;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
