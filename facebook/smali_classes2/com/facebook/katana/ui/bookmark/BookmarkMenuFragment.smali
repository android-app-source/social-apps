.class public Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;
.super Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;
.source ""

# interfaces
.implements LX/0fw;


# annotations
.annotation runtime Lcom/facebook/navigation/annotations/NavigationComponent;
    scrollableContent = .subannotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
        id = "R.id.bookmarks_list"
        type = .enum LX/8Cz;->LIST_VIEW:LX/8Cz;
    .end subannotation
.end annotation


# static fields
.field private static final Y:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;",
            ">;"
        }
    .end annotation
.end field

.field private static final Z:I


# instance fields
.field public A:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/EhL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0hx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/2lP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/17X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0tK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/ReactFragmentActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:Lcom/facebook/bookmark/client/BookmarkClient;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/2lS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/fbreact/interfaces/FbReactInstanceManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/work/bookmarks/bridge/IsWorkBookmarksEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/bookmarks/bridge/WorkGroupedBookmarksQuery;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/1E2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/19w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:LX/0tQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public V:LX/15W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public W:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public X:LX/2lX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final aa:LX/1EI;

.field private ab:Landroid/view/ViewGroup;

.field public ac:Z

.field public ad:Z

.field public ae:LX/2lI;

.field private af:LX/0xA;

.field public ag:Lcom/facebook/bookmark/model/Bookmark;

.field public ah:LX/0g8;

.field public ai:Landroid/widget/ProgressBar;

.field private aj:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public ak:Z

.field public al:LX/3Af;

.field public am:LX/34b;

.field public an:LX/3Af;

.field public ao:LX/34b;

.field public final ap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/EhL;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public aq:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;"
        }
    .end annotation
.end field

.field public final ar:Ljava/lang/Object;

.field public final as:Ljava/lang/Object;

.field public final at:Ljava/lang/Object;

.field public q:LX/2lG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/2lJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/10M;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6G2;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0xB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/2lM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Lcom/facebook/katana/urimap/IntentHandlerUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0gh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0jh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 136875
    const-class v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;

    sput-object v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->Y:Ljava/lang/Class;

    .line 136876
    const v0, 0x7f0831fc

    sput v0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->Z:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 136877
    const v0, 0x7f0309d9

    const v1, 0x7f0d0746

    invoke-direct {p0, v0, v1}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;-><init>(II)V

    .line 136878
    new-instance v0, LX/2l2;

    invoke-direct {v0, p0}, LX/2l2;-><init>(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)V

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aa:LX/1EI;

    .line 136879
    iput-boolean v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ac:Z

    .line 136880
    iput-boolean v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ak:Z

    .line 136881
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ap:Ljava/util/Map;

    .line 136882
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 136883
    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aq:Ljava/util/List;

    .line 136884
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ar:Ljava/lang/Object;

    .line 136885
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->as:Ljava/lang/Object;

    .line 136886
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->at:Ljava/lang/Object;

    .line 136887
    return-void
.end method

.method private static a(JLjava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136888
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(JLjava/util/List;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static a(JLjava/util/List;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136889
    if-ltz p3, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lt p3, v0, :cond_1

    .line 136890
    :cond_0
    :goto_0
    return-object p2

    .line 136891
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->b(JLjava/util/List;)I

    move-result v0

    .line 136892
    if-ltz v0, :cond_0

    .line 136893
    invoke-static {p2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p2

    .line 136894
    invoke-interface {p2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    .line 136895
    invoke-interface {p2, p3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/2lG;LX/2lJ;LX/0Or;LX/10M;LX/0Ot;LX/0xB;LX/2lM;Lcom/facebook/katana/urimap/IntentHandlerUtil;LX/0gh;LX/0jh;Ljava/lang/Boolean;Ljava/util/Set;LX/0hx;Lcom/facebook/content/SecureContextHelper;LX/2lP;LX/17X;LX/0tK;LX/0Or;LX/0ad;Lcom/facebook/bookmark/client/BookmarkClient;LX/0Or;LX/2lS;LX/0Ot;Ljava/lang/Boolean;LX/0Ot;LX/1E2;LX/0Uh;LX/0Ot;LX/0Ot;LX/19w;LX/0tQ;LX/15W;LX/0iA;LX/2lX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;",
            "LX/2lG;",
            "LX/2lJ;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/10M;",
            "LX/0Ot",
            "<",
            "LX/6G2;",
            ">;",
            "LX/0xB;",
            "LX/2lM;",
            "Lcom/facebook/katana/urimap/IntentHandlerUtil;",
            "LX/0gh;",
            "LX/0jh;",
            "Ljava/lang/Boolean;",
            "Ljava/util/Set",
            "<",
            "LX/EhL;",
            ">;",
            "LX/0hx;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/2lP;",
            "LX/17X;",
            "LX/0tK;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/bookmark/client/BookmarkClient;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/2lS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/fbreact/interfaces/FbReactInstanceManager;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/bookmarks/bridge/WorkGroupedBookmarksQuery;",
            ">;",
            "LX/1E2;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            "LX/0tQ;",
            "LX/15W;",
            "LX/0iA;",
            "LX/2lX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 136896
    iput-object p1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    iput-object p2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iput-object p3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->s:LX/0Or;

    iput-object p4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->t:LX/10M;

    iput-object p5, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->v:LX/0xB;

    iput-object p7, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->w:LX/2lM;

    iput-object p8, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->x:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    iput-object p9, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->y:LX/0gh;

    iput-object p10, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->z:LX/0jh;

    iput-object p11, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->A:Ljava/lang/Boolean;

    iput-object p12, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->B:Ljava/util/Set;

    iput-object p13, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->C:LX/0hx;

    iput-object p14, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->D:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->E:LX/2lP;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->F:LX/17X;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->G:LX/0tK;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->H:LX/0Or;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->I:LX/0ad;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->J:Lcom/facebook/bookmark/client/BookmarkClient;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->K:LX/0Or;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->L:LX/2lS;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->M:LX/0Ot;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->N:Ljava/lang/Boolean;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->O:LX/0Ot;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->P:LX/1E2;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->Q:LX/0Uh;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->R:LX/0Ot;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->S:LX/0Ot;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->T:LX/19w;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->U:LX/0tQ;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->V:LX/15W;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->W:LX/0iA;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->X:LX/2lX;

    return-void
.end method

.method public static a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V
    .locals 1
    .param p3    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 136897
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 136898
    invoke-static {p0, p1, v0, p3, p4}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;Ljava/lang/String;ILjava/lang/Object;)V

    .line 136899
    return-void
.end method

.method public static a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 2
    .param p3    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 136900
    if-nez p4, :cond_0

    .line 136901
    :goto_0
    return-void

    .line 136902
    :cond_0
    invoke-virtual {p1, p2}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, LX/FBj;

    invoke-direct {v1, p0, p4}, LX/FBj;-><init>(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/util/List;LX/2lI;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2lI;",
            ">;",
            "LX/2lI;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 137089
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 137090
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lI;

    .line 137091
    instance-of v2, v0, LX/EhP;

    if-eqz v2, :cond_2

    .line 137092
    check-cast v0, LX/EhP;

    .line 137093
    iget-object v2, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v0, v2

    .line 137094
    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 137095
    if-eqz v0, :cond_2

    const-string v2, "pinned"

    iget-object v0, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137096
    add-int/lit8 v0, v1, 0x1

    invoke-interface {p1, v0, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 137097
    :cond_0
    if-eqz p3, :cond_1

    .line 137098
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v1, LX/2la;->Divider:LX/2la;

    new-instance v2, Lcom/facebook/bookmark/model/BookmarksGroup;

    const-string v3, "pinned"

    const v4, 0x7f0831f0

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/bookmark/model/BookmarksGroup;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Lcom/facebook/bookmark/model/BookmarksGroup;Z)LX/2lI;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137099
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137100
    :cond_1
    return-void

    .line 137101
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 37

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v36

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;

    invoke-static/range {v36 .. v36}, LX/2lG;->a(LX/0QB;)LX/2lG;

    move-result-object v3

    check-cast v3, LX/2lG;

    invoke-static/range {v36 .. v36}, LX/2lJ;->a(LX/0QB;)LX/2lJ;

    move-result-object v4

    check-cast v4, LX/2lJ;

    const/16 v5, 0x15e7

    move-object/from16 v0, v36

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static/range {v36 .. v36}, LX/10M;->a(LX/0QB;)LX/10M;

    move-result-object v6

    check-cast v6, LX/10M;

    const/16 v7, 0x186a

    move-object/from16 v0, v36

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {v36 .. v36}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v8

    check-cast v8, LX/0xB;

    invoke-static/range {v36 .. v36}, LX/2lM;->a(LX/0QB;)LX/2lM;

    move-result-object v9

    check-cast v9, LX/2lM;

    invoke-static/range {v36 .. v36}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(LX/0QB;)Lcom/facebook/katana/urimap/IntentHandlerUtil;

    move-result-object v10

    check-cast v10, Lcom/facebook/katana/urimap/IntentHandlerUtil;

    invoke-static/range {v36 .. v36}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v11

    check-cast v11, LX/0gh;

    invoke-static/range {v36 .. v36}, LX/0jg;->a(LX/0QB;)LX/0jh;

    move-result-object v12

    check-cast v12, LX/0jh;

    invoke-static/range {v36 .. v36}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-static/range {v36 .. v36}, LX/2lO;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v14

    invoke-static/range {v36 .. v36}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v15

    check-cast v15, LX/0hx;

    invoke-static/range {v36 .. v36}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v16

    check-cast v16, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v36 .. v36}, LX/2lP;->a(LX/0QB;)LX/2lP;

    move-result-object v17

    check-cast v17, LX/2lP;

    invoke-static/range {v36 .. v36}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v18

    check-cast v18, LX/17X;

    invoke-static/range {v36 .. v36}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v19

    check-cast v19, LX/0tK;

    const/16 v20, 0xd

    move-object/from16 v0, v36

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-static/range {v36 .. v36}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v21

    check-cast v21, LX/0ad;

    invoke-static/range {v36 .. v36}, Lcom/facebook/bookmark/client/BookmarkClient;->a(LX/0QB;)Lcom/facebook/bookmark/client/BookmarkClient;

    move-result-object v22

    check-cast v22, Lcom/facebook/bookmark/client/BookmarkClient;

    const/16 v23, 0x1399

    move-object/from16 v0, v36

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v23

    invoke-static/range {v36 .. v36}, LX/2lS;->a(LX/0QB;)LX/2lS;

    move-result-object v24

    check-cast v24, LX/2lS;

    const/16 v25, 0x1c41

    move-object/from16 v0, v36

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    invoke-static/range {v36 .. v36}, LX/2lV;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v26

    check-cast v26, Ljava/lang/Boolean;

    const/16 v27, 0x38b4

    move-object/from16 v0, v36

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v27

    invoke-static/range {v36 .. v36}, LX/1E2;->a(LX/0QB;)LX/1E2;

    move-result-object v28

    check-cast v28, LX/1E2;

    invoke-static/range {v36 .. v36}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v29

    check-cast v29, LX/0Uh;

    const/16 v30, 0xbc

    move-object/from16 v0, v36

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v30

    const/16 v31, 0x24e

    move-object/from16 v0, v36

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v31

    invoke-static/range {v36 .. v36}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v32

    check-cast v32, LX/19w;

    invoke-static/range {v36 .. v36}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v33

    check-cast v33, LX/0tQ;

    invoke-static/range {v36 .. v36}, LX/15W;->a(LX/0QB;)LX/15W;

    move-result-object v34

    check-cast v34, LX/15W;

    invoke-static/range {v36 .. v36}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v35

    check-cast v35, LX/0iA;

    invoke-static/range {v36 .. v36}, LX/2lX;->a(LX/0QB;)LX/2lX;

    move-result-object v36

    check-cast v36, LX/2lX;

    invoke-static/range {v2 .. v36}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/2lG;LX/2lJ;LX/0Or;LX/10M;LX/0Ot;LX/0xB;LX/2lM;Lcom/facebook/katana/urimap/IntentHandlerUtil;LX/0gh;LX/0jh;Ljava/lang/Boolean;Ljava/util/Set;LX/0hx;Lcom/facebook/content/SecureContextHelper;LX/2lP;LX/17X;LX/0tK;LX/0Or;LX/0ad;Lcom/facebook/bookmark/client/BookmarkClient;LX/0Or;LX/2lS;LX/0Ot;Ljava/lang/Boolean;LX/0Ot;LX/1E2;LX/0Uh;LX/0Ot;LX/0Ot;LX/19w;LX/0tQ;LX/15W;LX/0iA;LX/2lX;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 136903
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->as:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136904
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->C:LX/0hx;

    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->z:LX/0jh;

    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6G2;

    invoke-static {v1, v2, v3, v0}, LX/FBm;->a(Landroid/content/Context;LX/0hx;LX/0jh;LX/6G2;)V

    .line 136905
    :cond_0
    :goto_0
    return-void

    .line 136906
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->at:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136907
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->w:LX/2lM;

    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->x:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->y:LX/0gh;

    invoke-static {v0, v1, v2, v3}, LX/FBm;->a(Landroid/content/Context;LX/2lM;Lcom/facebook/katana/urimap/IntentHandlerUtil;LX/0gh;)V

    goto :goto_0
.end method

.method private static b(JLjava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 136908
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 136909
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v2, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v2, p0

    if-nez v0, :cond_0

    .line 136910
    :goto_1
    return v1

    .line 136911
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 136912
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private b(Lcom/facebook/bookmark/model/BookmarksGroup;)V
    .locals 3

    .prologue
    .line 136913
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->F:LX/17X;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, LX/0ax;->ck:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 136914
    sget-object v1, LX/0ax;->ck:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 136915
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->D:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 136916
    return-void
.end method

.method public static b(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 136917
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 136918
    :cond_0
    :goto_0
    return-void

    .line 136919
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ai:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 136920
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aq:Ljava/util/List;

    .line 136921
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    .line 136922
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 136923
    const/4 v0, 0x0

    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 136924
    const-string v1, "settings"

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 136925
    invoke-interface {v5, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 136926
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136927
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EhL;

    .line 136928
    invoke-interface {v0}, LX/EhL;->a()Z

    move-result v2

    .line 136929
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ap:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136930
    if-eqz v2, :cond_3

    .line 136931
    const/4 v4, 0x0

    .line 136932
    invoke-interface {v0}, LX/EhL;->c()Ljava/lang/String;

    move-result-object v7

    .line 136933
    if-eqz v7, :cond_19

    move v3, v4

    .line 136934
    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_19

    .line 136935
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v2, v2, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-static {v2, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 136936
    add-int/lit8 v4, v3, 0x1

    .line 136937
    :goto_4
    move v2, v4

    .line 136938
    invoke-interface {v0}, LX/EhL;->d()Lcom/facebook/bookmark/model/BookmarksGroup;

    move-result-object v0

    invoke-interface {v5, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_2

    .line 136939
    :cond_4
    const/4 v1, 0x0

    .line 136940
    const/4 v0, 0x0

    move v2, v0

    move v3, v1

    :goto_5
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_13

    .line 136941
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 136942
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->b()I

    move-result v1

    if-eqz v1, :cond_12

    .line 136943
    const-string v1, "group"

    iget-object v4, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-direct {p0, v5}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->g(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 136944
    const/4 v3, 0x1

    move v1, v3

    .line 136945
    :goto_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto :goto_5

    .line 136946
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 136947
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->c()Z

    move-result v1

    move v1, v1

    .line 136948
    if-nez v1, :cond_12

    .line 136949
    if-lez v2, :cond_7

    .line 136950
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v7, LX/2la;->Divider:LX/2la;

    const/4 v1, 0x1

    if-le v2, v1, :cond_c

    const/4 v1, 0x1

    :goto_7
    invoke-virtual {v4, v7, v0, v1}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Lcom/facebook/bookmark/model/BookmarksGroup;Z)LX/2lI;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136951
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->e()Ljava/util/List;

    move-result-object v1

    .line 136952
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->I:LX/0ad;

    sget-short v7, LX/34q;->d:S

    const/4 v8, 0x0

    invoke-interface {v4, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->Q:LX/0Uh;

    const/16 v7, 0x611

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 136953
    const-wide v8, 0x70ccb8b19e36L

    invoke-static {v8, v9, v1}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->b(JLjava/util/List;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const-wide v8, 0x8bb78869L

    invoke-static {v8, v9, v1}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->b(JLjava/util/List;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 136954
    const-wide v8, 0x21531ffed86f8L

    invoke-static {v8, v9, v1}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->b(JLjava/util/List;)I

    move-result v7

    .line 136955
    const/4 v8, -0x1

    if-eq v7, v8, :cond_8

    if-ge v4, v7, :cond_8

    .line 136956
    const-wide v8, 0x21531ffed86f8L

    invoke-static {v8, v9, v1, v4}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(JLjava/util/List;I)Ljava/util/List;

    move-result-object v1

    .line 136957
    :cond_8
    const-wide v8, 0xc63f291b142bL

    invoke-static {v8, v9, v1}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(JLjava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 136958
    const-wide v8, 0x268a3ef6875cfL

    invoke-static {v8, v9, v1}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(JLjava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 136959
    const-wide v8, 0x229cf4f51d6aaL

    invoke-static {v8, v9, v1}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(JLjava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 136960
    const-wide v8, 0x3290f61563f45L

    invoke-static {v8, v9, v1}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(JLjava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 136961
    const/4 v1, 0x0

    move v4, v1

    :goto_8
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_11

    .line 136962
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/model/Bookmark;

    .line 136963
    const-string v8, "profile"

    iget-object v9, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    invoke-virtual {v1, v8}, Lcom/facebook/bookmark/model/Bookmark;->a(Z)V

    .line 136964
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/CharSequence;

    const/4 v9, 0x0

    iget-object v10, v1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-static {v8}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_9

    iget-object v8, v1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    sget-object v9, LX/0ax;->dl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 136965
    :cond_9
    invoke-virtual {p0, v1}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->b(Lcom/facebook/bookmark/model/Bookmark;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 136966
    iget-wide v8, v1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v10, 0x21531ffed86f8L

    cmp-long v8, v8, v10

    if-nez v8, :cond_a

    .line 136967
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ak:Z

    .line 136968
    :cond_a
    const-string v8, "two_rows"

    iget-object v9, v1, Lcom/facebook/bookmark/model/Bookmark;->layout:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 136969
    iget-object v8, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v9, LX/2la;->FamilyBridgesProfile:LX/2la;

    invoke-virtual {v8, v9, v1}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b(LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)LX/2lI;

    move-result-object v1

    .line 136970
    :goto_9
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136971
    :cond_b
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_8

    .line 136972
    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_7

    .line 136973
    :cond_d
    const-string v8, "profile"

    iget-object v9, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    const-string v8, "profile"

    iget-object v9, v1, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 136974
    iget-object v8, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v9, LX/2la;->Profile:LX/2la;

    .line 136975
    new-instance v10, LX/EhR;

    invoke-direct {v10, v8, v9, v1}, LX/EhR;-><init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)V

    move-object v1, v10

    .line 136976
    goto :goto_9

    .line 136977
    :cond_e
    const-wide v8, 0x11b0dc443L

    iget-wide v10, v1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_f

    const-wide v8, 0x229cf4f51d6aaL

    iget-wide v10, v1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v8, v8, v10

    if-nez v8, :cond_10

    .line 136978
    :cond_f
    iget-object v8, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v9, LX/2la;->NewsFeed:LX/2la;

    .line 136979
    new-instance v10, LX/EhO;

    invoke-direct {v10, v8, v9, v1}, LX/EhO;-><init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)V

    move-object v1, v10

    .line 136980
    goto :goto_9

    .line 136981
    :cond_10
    iget-object v8, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v9, LX/2la;->Bookmark:LX/2la;

    invoke-virtual {v8, v9, v1}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->c(LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)LX/2lI;

    move-result-object v1

    goto :goto_9

    .line 136982
    :cond_11
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->g()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 136983
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v4, LX/2la;->SeeAll:LX/2la;

    const/4 v7, -0x1

    const v8, 0x7f0831f7

    invoke-virtual {v1, v4, v0, v7, v8}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Ljava/lang/Object;II)LX/2lI;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_12
    move v1, v3

    goto/16 :goto_6

    .line 136984
    :cond_13
    if-eqz v3, :cond_15

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->s(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 136985
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->O:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G6u;

    invoke-virtual {v0}, LX/G6u;->c()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 136986
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->O:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G6u;

    invoke-virtual {v0}, LX/G6u;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_a
    if-ge v1, v3, :cond_15

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 136987
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v5, LX/2la;->Divider:LX/2la;

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v0, v7}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Lcom/facebook/bookmark/model/BookmarksGroup;Z)LX/2lI;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136988
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    .line 136989
    iget-object v5, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v7, LX/2la;->Bookmark:LX/2la;

    invoke-virtual {v5, v7, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->c(LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)LX/2lI;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 136990
    :cond_14
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 136991
    :cond_15
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 136992
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 136993
    :cond_16
    :goto_c
    invoke-virtual {p0, v6}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->a(Ljava/util/List;)V

    .line 136994
    invoke-direct {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->n()V

    .line 136995
    invoke-direct {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q()V

    goto/16 :goto_0

    .line 136996
    :cond_17
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_3

    .line 136997
    :cond_18
    add-int/lit8 v4, v4, 0x1

    :cond_19
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_1a

    .line 136998
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v2, v2, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    const-string v3, "settings"

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    goto/16 :goto_4

    .line 136999
    :cond_1a
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    goto/16 :goto_4

    .line 137000
    :cond_1b
    const/4 v0, 0x0

    .line 137001
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->U:LX/0tQ;

    .line 137002
    iget-object v4, v3, LX/0tQ;->a:LX/0ad;

    sget-short v5, LX/0wh;->M:S

    const/4 v7, 0x0

    invoke-interface {v4, v5, v7}, LX/0ad;->a(SZ)Z

    move-result v4

    move v3, v4

    .line 137003
    if-eqz v3, :cond_1c

    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->T:LX/19w;

    invoke-virtual {v3, v0, v0}, LX/19w;->a(ZZ)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1c

    const/4 v0, 0x1

    :cond_1c
    move v0, v0

    .line 137004
    if-eqz v0, :cond_1d

    iget-boolean v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ak:Z

    if-nez v0, :cond_1d

    .line 137005
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v3, LX/2la;->IconLabel:LX/2la;

    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v4, v4, LX/2lJ;->e:Lcom/facebook/bookmark/model/Bookmark;

    const v5, 0x7f0217a7

    const v7, 0x7f083201

    invoke-virtual {v0, v3, v4, v5, v7}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Ljava/lang/Object;II)LX/2lI;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {p0, v6, v0, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/util/List;LX/2lI;Z)V

    .line 137006
    :cond_1d
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->G:LX/0tK;

    invoke-virtual {v0}, LX/0tK;->f()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->G:LX/0tK;

    invoke-virtual {v0}, LX/0tK;->g()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 137007
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v3, LX/2la;->IconLabel:LX/2la;

    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v4, v4, LX/2lJ;->d:Lcom/facebook/bookmark/model/Bookmark;

    const v5, 0x7f02040f

    const v7, 0x7f0809fd

    invoke-virtual {v0, v3, v4, v5, v7}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Ljava/lang/Object;II)LX/2lI;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {p0, v6, v0, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/util/List;LX/2lI;Z)V

    .line 137008
    :cond_1e
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_d
    if-ltz v3, :cond_32

    .line 137009
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lI;

    .line 137010
    instance-of v4, v0, LX/EhP;

    if-eqz v4, :cond_20

    .line 137011
    check-cast v0, LX/EhP;

    .line 137012
    iget-object v3, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v0, v3

    .line 137013
    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 137014
    if-eqz v0, :cond_32

    const-string v3, "settings"

    iget-object v0, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    move v0, v1

    .line 137015
    :goto_e
    if-nez v0, :cond_1f

    .line 137016
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v3, LX/2la;->Divider:LX/2la;

    new-instance v4, Lcom/facebook/bookmark/model/BookmarksGroup;

    const-string v5, "settings"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->I:LX/0ad;

    invoke-static {v7, v8}, LX/FBm;->a(Landroid/content/Context;LX/0ad;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v4, v5, v7, v2, v8}, Lcom/facebook/bookmark/model/BookmarksGroup;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V

    invoke-virtual {v0, v3, v4, v1}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Lcom/facebook/bookmark/model/BookmarksGroup;Z)LX/2lI;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137017
    :cond_1f
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->X:LX/2lX;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->I:LX/0ad;

    invoke-static {v4}, LX/FBm;->a(LX/0ad;)Z

    move-result v4

    iget-object v5, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->G:LX/0tK;

    invoke-static {v5}, LX/FBm;->a(LX/0tK;)Z

    move-result v5

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->z:LX/0jh;

    invoke-static {v7, v8}, LX/FBm;->a(Landroid/content/Context;LX/0jh;)Z

    move-result v7

    invoke-virtual {v0, v3, v4, v5, v7}, LX/2lX;->a(Landroid/content/Context;ZZZ)Ljava/util/List;

    move-result-object v0

    .line 137018
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FBn;

    .line 137019
    iget-object v4, v0, LX/FBn;->a:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 137020
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v5, LX/2la;->IconLabel:LX/2la;

    iget-object v7, v0, LX/FBn;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget v8, v0, LX/FBn;->c:I

    iget-object v0, v0, LX/FBn;->d:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5, v7, v8, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Ljava/lang/Object;ILjava/lang/CharSequence;)LX/2lI;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 137021
    :cond_20
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto/16 :goto_d

    .line 137022
    :pswitch_0
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v5, LX/2la;->SettingsBottomSheetLauncher:LX/2la;

    iget-object v7, v0, LX/FBn;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget v8, v0, LX/FBn;->c:I

    iget-object v0, v0, LX/FBn;->d:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5, v7, v8, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Ljava/lang/Object;ILjava/lang/CharSequence;)LX/2lI;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 137023
    :pswitch_1
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v5, LX/2la;->HelpAndSupportBottomSheetLauncher:LX/2la;

    iget-object v7, v0, LX/FBn;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget v8, v0, LX/FBn;->c:I

    iget-object v0, v0, LX/FBn;->d:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5, v7, v8, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Ljava/lang/Object;ILjava/lang/CharSequence;)LX/2lI;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 137024
    :pswitch_2
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v5, LX/2la;->IconLabel:LX/2la;

    iget-object v7, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->as:Ljava/lang/Object;

    iget v8, v0, LX/FBn;->c:I

    iget-object v0, v0, LX/FBn;->d:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5, v7, v8, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Ljava/lang/Object;ILjava/lang/CharSequence;)LX/2lI;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 137025
    :pswitch_3
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v5, LX/2la;->IconLabel:LX/2la;

    iget-object v7, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->at:Ljava/lang/Object;

    iget v8, v0, LX/FBn;->c:I

    iget-object v0, v0, LX/FBn;->d:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5, v7, v8, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Ljava/lang/Object;ILjava/lang/CharSequence;)LX/2lI;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 137026
    :pswitch_4
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v5, LX/2la;->IconLabel:LX/2la;

    iget-object v7, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ar:Ljava/lang/Object;

    iget v8, v0, LX/FBn;->c:I

    iget-object v0, v0, LX/FBn;->d:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5, v7, v8, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(LX/2lb;Ljava/lang/Object;ILjava/lang/CharSequence;)LX/2lI;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_f

    .line 137027
    :cond_21
    iget-boolean v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ac:Z

    if-eqz v0, :cond_22

    .line 137028
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ae:LX/2lI;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137029
    :cond_22
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->I:LX/0ad;

    invoke-static {v0}, LX/FBm;->a(LX/0ad;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 137030
    new-instance v0, LX/3Af;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/3Af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->al:LX/3Af;

    .line 137031
    new-instance v0, LX/34b;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/34b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->am:LX/34b;

    .line 137032
    new-instance v0, LX/3Af;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/3Af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->an:LX/3Af;

    .line 137033
    new-instance v0, LX/34b;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/34b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ao:LX/34b;

    .line 137034
    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 137035
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->am:LX/34b;

    .line 137036
    iput-boolean v1, v0, LX/34b;->i:Z

    .line 137037
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ao:LX/34b;

    .line 137038
    iput-boolean v1, v0, LX/34b;->i:Z

    .line 137039
    :cond_23
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->am:LX/34b;

    const v3, 0x7f0831f4

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_26

    const v0, 0x7f0208ac

    :goto_10
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v4, v4, LX/2lJ;->a:Lcom/facebook/bookmark/model/Bookmark;

    invoke-static {p0, v1, v3, v0, v4}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V

    .line 137040
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->am:LX/34b;

    const v3, 0x7f0831f2

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_27

    const v0, 0x7f02089a

    :goto_11
    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v4, v4, LX/2lJ;->g:Lcom/facebook/bookmark/model/Bookmark;

    invoke-static {p0, v1, v3, v0, v4}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V

    .line 137041
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->Q:LX/0Uh;

    const/16 v1, 0x72

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 137042
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->am:LX/34b;

    const v2, 0x7f081e0a

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_28

    const v0, 0x7f0208ac

    :goto_12
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v3, v3, LX/2lJ;->h:Lcom/facebook/bookmark/model/Bookmark;

    invoke-static {p0, v1, v2, v0, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V

    .line 137043
    :cond_24
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->am:LX/34b;

    const v2, 0x7f0831ec

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_29

    const v0, 0x7f020943

    :goto_13
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v3, v3, LX/2lJ;->c:Lcom/facebook/bookmark/model/Bookmark;

    invoke-static {p0, v1, v2, v0, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V

    .line 137044
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->am:LX/34b;

    const v2, 0x7f0831f6

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_2a

    const v0, 0x7f020902

    :goto_14
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    invoke-virtual {v3}, LX/2lJ;->a()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v3

    invoke-static {p0, v1, v2, v0, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V

    .line 137045
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->am:LX/34b;

    const v2, 0x7f0831dc

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_2b

    const v0, 0x7f0208f4

    :goto_15
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v3, v3, LX/2lJ;->j:Lcom/facebook/bookmark/model/Bookmark;

    invoke-static {p0, v1, v2, v0, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V

    .line 137046
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ao:LX/34b;

    const v2, 0x7f0831fd

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_2c

    const v0, 0x7f02099d

    :goto_16
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v3, v3, LX/2lJ;->i:Lcom/facebook/bookmark/model/Bookmark;

    invoke-static {p0, v1, v2, v0, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V

    .line 137047
    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ao:LX/34b;

    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->A:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2d

    const v0, 0x7f083204

    :goto_17
    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v1

    if-eqz v1, :cond_2e

    const v1, 0x7f0209a7

    :goto_18
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v3, v3, LX/2lJ;->l:Lcom/facebook/bookmark/model/Bookmark;

    invoke-static {p0, v2, v0, v1, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V

    .line 137048
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->z:LX/0jh;

    invoke-static {v0, v1}, LX/FBm;->a(Landroid/content/Context;LX/0jh;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 137049
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ao:LX/34b;

    const v2, 0x7f0818ee

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_2f

    const v0, 0x7f0207c4

    :goto_19
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->as:Ljava/lang/Object;

    invoke-static {p0, v1, v2, v0, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V

    .line 137050
    :cond_25
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ao:LX/34b;

    const v2, 0x7f0800cb

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_30

    const v0, 0x7f0208ed

    :goto_1a
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v3, v3, LX/2lJ;->m:Lcom/facebook/bookmark/model/Bookmark;

    invoke-static {p0, v1, v2, v0, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;IILjava/lang/Object;)V

    .line 137051
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->A:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->w:LX/2lM;

    invoke-virtual {v0}, LX/2lM;->d()Z

    move-result v0

    if-nez v0, :cond_16

    .line 137052
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ao:LX/34b;

    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->w:LX/2lM;

    invoke-virtual {v0}, LX/2lM;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-eqz v0, :cond_31

    const v0, 0x7f020772

    :goto_1b
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->at:Ljava/lang/Object;

    invoke-static {p0, v1, v2, v0, v3}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;LX/34b;Ljava/lang/String;ILjava/lang/Object;)V

    goto/16 :goto_c

    .line 137053
    :cond_26
    const v0, 0x7f0200a1

    goto/16 :goto_10

    .line 137054
    :cond_27
    const v0, 0x7f020042

    goto/16 :goto_11

    .line 137055
    :cond_28
    const v0, 0x7f0200a1

    goto/16 :goto_12

    .line 137056
    :cond_29
    const v0, 0x7f020ae0

    goto/16 :goto_13

    .line 137057
    :cond_2a
    const v0, 0x7f020057

    goto/16 :goto_14

    .line 137058
    :cond_2b
    const v0, 0x7f02026f

    goto/16 :goto_15

    .line 137059
    :cond_2c
    const v0, 0x7f020cbf

    goto/16 :goto_16

    .line 137060
    :cond_2d
    const v0, 0x7f083203

    goto/16 :goto_17

    :cond_2e
    const v1, 0x7f021911

    goto/16 :goto_18

    .line 137061
    :cond_2f
    const v0, 0x7f021624

    goto :goto_19

    .line 137062
    :cond_30
    const v0, 0x7f02003e

    goto :goto_1a

    .line 137063
    :cond_31
    const v0, 0x7f020f18

    goto :goto_1b

    :cond_32
    move v0, v2

    goto/16 :goto_e

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private g(Ljava/util/List;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 136866
    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->s(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 136867
    :goto_0
    return v0

    .line 136868
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 136869
    invoke-virtual {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/model/Bookmark;

    .line 136870
    const-string v5, "pinned"

    iget-object v6, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-wide v6, 0xd16b5fe18c70L

    iget-wide v8, v1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v1, v6, v8

    if-nez v1, :cond_2

    .line 136871
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 136872
    goto :goto_0
.end method

.method private n()V
    .locals 10

    .prologue
    const-wide v8, 0x21531ffed86f8L

    .line 137064
    invoke-virtual {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 137065
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->V:LX/15W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BOOKMARK_TAB_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v4, LX/Cv5;

    new-instance v5, LX/Cv4;

    invoke-virtual {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->k()LX/0g8;

    move-result-object v6

    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->p:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->p:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    invoke-direct {v5, v6, v0}, LX/Cv4;-><init>(LX/0g8;I)V

    invoke-virtual {v1, v2, v3, v4, v5}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 137066
    :cond_0
    return-void

    .line 137067
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private q()V
    .locals 10

    .prologue
    .line 137068
    invoke-virtual {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 137069
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->W:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->OFFLINE_FEED_BOOKMARK_TRIGGER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v2, LX/Erw;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/Erw;

    .line 137070
    if-eqz v0, :cond_0

    .line 137071
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->W:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/Erw;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 137072
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->p:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->f:Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v2, v2, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->p:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->r:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->f:Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v2, v2, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 137073
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->k()LX/0g8;

    move-result-object v3

    .line 137074
    if-ltz v1, :cond_0

    invoke-interface {v3}, LX/0g8;->s()I

    move-result v4

    if-lt v1, v4, :cond_2

    .line 137075
    :cond_0
    :goto_1
    return-void

    .line 137076
    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    .line 137077
    :cond_2
    new-instance v5, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v5, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 137078
    new-instance v4, Lcom/facebook/feed/offlinefeed/nux/OfflineFeedBookmarkNuxInterstitialController$1;

    invoke-direct {v4, v0, v3, v1}, Lcom/facebook/feed/offlinefeed/nux/OfflineFeedBookmarkNuxInterstitialController$1;-><init>(LX/Erw;LX/0g8;I)V

    const v6, 0x20174fb0

    invoke-static {v5, v4, v6}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 137079
    const-class v4, LX/0f0;

    invoke-static {v2, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0f0;

    .line 137080
    if-eqz v4, :cond_0

    .line 137081
    const-class v6, LX/0f3;

    invoke-interface {v4, v6}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0f3;

    .line 137082
    if-eqz v4, :cond_0

    invoke-interface {v4}, LX/0f3;->n()Lcom/facebook/apptab/state/TabTag;

    move-result-object v4

    sget-object v6, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    if-ne v4, v6, :cond_0

    .line 137083
    new-instance v4, LX/0hs;

    const/4 v6, 0x2

    invoke-direct {v4, v2, v6}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 137084
    const/4 v6, -0x1

    .line 137085
    iput v6, v4, LX/0hs;->t:I

    .line 137086
    const v6, 0x7f082ef5

    invoke-virtual {v4, v6}, LX/0hs;->a(I)V

    .line 137087
    const v6, 0x7f082ef6

    invoke-virtual {v2, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 137088
    new-instance v6, Lcom/facebook/feed/offlinefeed/nux/OfflineFeedBookmarkNuxInterstitialController$2;

    invoke-direct {v6, v0, v3, v1, v4}, Lcom/facebook/feed/offlinefeed/nux/OfflineFeedBookmarkNuxInterstitialController$2;-><init>(LX/Erw;LX/0g8;ILX/0hs;)V

    const-wide/16 v8, 0x7d0

    const v4, -0x3e1b5e06

    invoke-static {v5, v6, v8, v9, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1
.end method

.method public static s(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z
    .locals 1

    .prologue
    .line 136873
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->A:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->N:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z
    .locals 3

    .prologue
    .line 136874
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->I:LX/0ad;

    sget-short v1, LX/EgI;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 136742
    invoke-super {p0, p1}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->a(Landroid/os/Bundle;)V

    .line 136743
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 136744
    invoke-static {p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->s(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 136745
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->B:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EhL;

    .line 136746
    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ap:Ljava/util/Map;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 136747
    :cond_0
    const v0, 0x7f0301c3

    iput v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->n:I

    .line 136748
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->P:LX/1E2;

    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aa:LX/1EI;

    invoke-virtual {v0, v1}, LX/1E2;->a(LX/1EI;)V

    .line 136749
    new-instance v0, LX/2lY;

    invoke-direct {v0, p0}, LX/2lY;-><init>(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)V

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->af:LX/0xA;

    .line 136750
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->v:LX/0xB;

    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->af:LX/0xA;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/0xA;)V

    .line 136751
    return-void

    .line 136752
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->O:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G6u;

    new-instance v1, LX/FBk;

    invoke-direct {v1, p0}, LX/FBk;-><init>(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)V

    invoke-virtual {v0}, LX/G6u;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136753
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 136754
    :cond_0
    :goto_0
    return-void

    .line 136755
    :cond_1
    if-nez p2, :cond_2

    iget-boolean v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ac:Z

    if-eqz v0, :cond_3

    .line 136756
    :cond_2
    iput-boolean v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ac:Z

    .line 136757
    iget-object v0, p1, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    move-object v0, v0

    .line 136758
    invoke-static {p0, v0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->b(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/util/List;)V

    .line 136759
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ad:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aj:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 136760
    iput-boolean v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ad:Z

    .line 136761
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aj:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/bookmark/model/BookmarksGroup;)V
    .locals 1

    .prologue
    .line 136762
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->g:LX/2l5;

    if-eqz v0, :cond_0

    .line 136763
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->g:LX/2l5;

    invoke-interface {v0}, LX/2l5;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->b(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/util/List;)V

    .line 136764
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 136765
    invoke-static {}, LX/2la;->values()[LX/2la;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final c()V
    .locals 7

    .prologue
    .line 136766
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->g:LX/2l5;

    invoke-interface {v0}, LX/2l5;->e()Lcom/facebook/bookmark/FetchBookmarksResult;

    move-result-object v0

    .line 136767
    iget-object v1, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 136768
    sget-object v2, LX/0ta;->NO_DATA:LX/0ta;

    if-eq v1, v2, :cond_0

    .line 136769
    iget-object v1, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 136770
    sget-object v2, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    if-ne v1, v2, :cond_1

    .line 136771
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ac:Z

    .line 136772
    :cond_1
    iget-object v1, v0, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    move-object v1, v1

    .line 136773
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136774
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ai:Landroid/widget/ProgressBar;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 136775
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ai:Landroid/widget/ProgressBar;

    new-instance v4, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment$4;

    invoke-direct {v4, p0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment$4;-><init>(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)V

    const-wide/16 v5, 0x1388

    invoke-virtual {v3, v4, v5, v6}, Landroid/widget/ProgressBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 136776
    :goto_0
    return-void

    .line 136777
    :cond_2
    iget-object v1, v0, Lcom/facebook/bookmark/FetchBookmarksResult;->a:LX/0Px;

    move-object v0, v1

    .line 136778
    invoke-static {p0, v0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->b(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/util/List;)V

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 136779
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1b122fa4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 136780
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->q:LX/2lG;

    sget-object v2, LX/2la;->Loader:LX/2la;

    .line 136781
    new-instance v4, LX/2lc;

    invoke-direct {v4, v1, v2}, LX/2lc;-><init>(LX/2lG;LX/2lb;)V

    move-object v1, v4

    .line 136782
    iput-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ae:LX/2lI;

    .line 136783
    invoke-super {p0, p1}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 136784
    const/16 v1, 0x2b

    const v2, -0x3ac6111a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 136785
    invoke-super {p0, p1}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 136786
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    invoke-static {v0}, LX/62G;->a(LX/0g8;)V

    .line 136787
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, 0x5bf5c52a

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 136788
    iput-object p2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ab:Landroid/view/ViewGroup;

    .line 136789
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 136790
    new-instance v3, LX/2iI;

    const v0, 0x7f0d0746

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v3, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    .line 136791
    const v0, 0x7f0d0747

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ai:Landroid/widget/ProgressBar;

    .line 136792
    const v0, 0x7f0d0745

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aj:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 136793
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aj:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v5}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 136794
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aj:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v3, 0x1

    new-array v3, v3, [I

    const v4, 0x7f0a00d1

    aput v4, v3, v5

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 136795
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aj:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v3, LX/2lZ;

    invoke-direct {v3, p0}, LX/2lZ;-><init>(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 136796
    const/16 v0, 0x2b

    const v3, -0x645a3362

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x3f3e501e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 136797
    invoke-super {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onDestroy()V

    .line 136798
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->v:LX/0xB;

    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->af:LX/0xA;

    invoke-virtual {v1, v2}, LX/0xB;->b(LX/0xA;)V

    .line 136799
    iput-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    .line 136800
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aj:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v1, :cond_0

    .line 136801
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aj:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 136802
    iput-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aj:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 136803
    :cond_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 136804
    iput-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aq:Ljava/util/List;

    .line 136805
    const/16 v1, 0x2b

    const v2, 0x4d4d41c6    # 2.15227488E8f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 136806
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ah:LX/0g8;

    invoke-interface {v0, p3}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v0

    .line 136807
    instance-of v1, v0, LX/2lH;

    if-nez v1, :cond_0

    .line 136808
    :goto_0
    return-void

    .line 136809
    :cond_0
    check-cast v0, LX/2lH;

    .line 136810
    invoke-virtual {v0}, LX/2lH;->a()LX/2lb;

    move-result-object v1

    check-cast v1, LX/2la;

    .line 136811
    sget-object v2, LX/2la;->SeeAll:LX/2la;

    if-ne v1, v2, :cond_1

    .line 136812
    iget-object v2, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 136813
    check-cast v2, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 136814
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->C:LX/0hx;

    iget-object v4, v2, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    sget-object v5, LX/CIq;->c:Ljava/lang/String;

    sget-object v6, LX/CIp;->u:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136815
    const-string v3, "group"

    iget-object v4, v2, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 136816
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->H:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 136817
    const-string v3, "target_fragment"

    sget-object v4, LX/0cQ;->GROUPS_GRID_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 136818
    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->D:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 136819
    :cond_1
    iget-object v2, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 136820
    instance-of v2, v2, Lcom/facebook/bookmark/model/Bookmark;

    if-eqz v2, :cond_3

    .line 136821
    invoke-virtual {v0}, LX/2lH;->c()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v2

    .line 136822
    if-eqz v2, :cond_3

    iget-wide v4, v2, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v6, 0x268a3ef6875cfL

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, v2, Lcom/facebook/bookmark/model/Bookmark;->a:Lcom/facebook/bookmark/model/BookmarksGroup;

    if-eqz v3, :cond_3

    .line 136823
    iget-object v0, v2, Lcom/facebook/bookmark/model/Bookmark;->a:Lcom/facebook/bookmark/model/BookmarksGroup;

    invoke-direct {p0, v0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->b(Lcom/facebook/bookmark/model/BookmarksGroup;)V

    goto :goto_0

    .line 136824
    :cond_2
    invoke-direct {p0, v2}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->b(Lcom/facebook/bookmark/model/BookmarksGroup;)V

    goto :goto_0

    .line 136825
    :cond_3
    sget-object v2, LX/2la;->IconLabel:LX/2la;

    if-ne v1, v2, :cond_4

    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ar:Ljava/lang/Object;

    .line 136826
    iget-object v3, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 136827
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 136828
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iget-object v2, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->C:LX/0hx;

    iget-object v3, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->t:LX/10M;

    iget-object v4, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->E:LX/2lP;

    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->s:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v2, v3, v4, v1}, LX/FBm;->a(Landroid/app/Activity;LX/0hx;LX/10M;LX/2lP;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 136829
    :cond_4
    sget-object v2, LX/2la;->IconLabel:LX/2la;

    if-ne v1, v2, :cond_5

    .line 136830
    iget-object v2, v0, LX/2lH;->d:Ljava/lang/Object;

    move-object v0, v2

    .line 136831
    invoke-static {p0, v0}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a$redex0(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/lang/Object;)V

    .line 136832
    :cond_5
    sget-object v0, LX/2la;->SettingsBottomSheetLauncher:LX/2la;

    if-ne v1, v0, :cond_6

    .line 136833
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->al:LX/3Af;

    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->am:LX/34b;

    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 136834
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->al:LX/3Af;

    invoke-virtual {v0, v8}, LX/3Af;->a(I)V

    .line 136835
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->al:LX/3Af;

    invoke-virtual {v0, p2}, LX/3Af;->a(Landroid/view/View;)V

    goto/16 :goto_0

    .line 136836
    :cond_6
    sget-object v0, LX/2la;->HelpAndSupportBottomSheetLauncher:LX/2la;

    if-ne v1, v0, :cond_7

    .line 136837
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->an:LX/3Af;

    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ao:LX/34b;

    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 136838
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->an:LX/3Af;

    invoke-virtual {v0, v8}, LX/3Af;->a(I)V

    .line 136839
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->an:LX/3Af;

    invoke-virtual {v0, p2}, LX/3Af;->a(Landroid/view/View;)V

    goto/16 :goto_0

    .line 136840
    :cond_7
    invoke-super/range {p0 .. p5}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto/16 :goto_0
.end method

.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 136841
    invoke-super/range {p0 .. p5}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v0

    return v0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x68bc6bd4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 136842
    invoke-super {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onPause()V

    .line 136843
    const/16 v1, 0x2b

    const v2, -0x69baaa80

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x41d6886f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 136844
    invoke-super {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onResume()V

    .line 136845
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EhL;

    .line 136846
    invoke-interface {v1}, LX/EhL;->a()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->ap:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, LX/EhL;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136847
    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->aq:Ljava/util/List;

    invoke-static {p0, v1}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->b(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/util/List;)V

    .line 136848
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->r:Z

    .line 136849
    const/16 v1, 0x2b

    const v2, 0x74361540

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 136850
    invoke-super {p0, p1}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 136851
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4b8a5be2    # 1.813498E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 136852
    invoke-super {p0}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onStart()V

    .line 136853
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 136854
    if-eqz v0, :cond_0

    .line 136855
    sget v2, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->Z:I

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 136856
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 136857
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x73f79f58

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 136858
    invoke-super {p0, p1, p2}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 136859
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-nez v0, :cond_1

    .line 136860
    :cond_0
    :goto_0
    return-void

    .line 136861
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->I:LX/0ad;

    sget-short v1, LX/347;->I:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136862
    iget-object v0, p0, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JKf;

    .line 136863
    iget-object v1, v0, LX/JKf;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/JKf;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 136864
    iget-object v1, v0, LX/JKf;->e:LX/0Sg;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p1, LX/JKf;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string p1, " - React Native Preload"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance p1, Lcom/facebook/fbreact/fb4a/Fb4aReactInstanceManager$1;

    invoke-direct {p1, v0}, Lcom/facebook/fbreact/fb4a/Fb4aReactInstanceManager$1;-><init>(LX/JKf;)V

    sget-object p2, LX/0VZ;->APPLICATION_LOADED_LOW_PRIORITY:LX/0VZ;

    sget-object p0, LX/0Vm;->UI:LX/0Vm;

    invoke-virtual {v1, v2, p1, p2, p0}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 136865
    :cond_2
    goto :goto_0
.end method
