.class public final Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1nz;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:F

.field public b:Landroid/net/Uri;

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:Ljava/lang/String;

.field public e:LX/1aZ;

.field public f:LX/1Up;

.field public g:Landroid/graphics/PointF;

.field public h:Landroid/graphics/PointF;

.field public i:LX/1Up;

.field public j:LX/4Ab;

.field public k:Landroid/graphics/ColorFilter;

.field public l:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/1Up;

.field public o:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/1Up;

.field public q:I

.field public r:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/1Up;

.field public t:I

.field public u:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic v:LX/1nz;


# direct methods
.method public constructor <init>(LX/1nz;)V
    .locals 1

    .prologue
    .line 318010
    iput-object p1, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->v:LX/1nz;

    .line 318011
    move-object v0, p1

    .line 318012
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 318013
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->a:F

    .line 318014
    sget-object v0, LX/1o0;->c:LX/1Up;

    iput-object v0, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->f:LX/1Up;

    .line 318015
    sget-object v0, LX/1o0;->a:LX/1Up;

    iput-object v0, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->i:LX/1Up;

    .line 318016
    sget-object v0, LX/1o0;->b:LX/1Up;

    iput-object v0, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->n:LX/1Up;

    .line 318017
    sget-object v0, LX/1o0;->d:LX/1Up;

    iput-object v0, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->p:LX/1Up;

    .line 318018
    sget-object v0, LX/1o0;->e:LX/1Up;

    iput-object v0, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->s:LX/1Up;

    .line 318019
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318020
    const-string v0, "FbSizeAwareFrescoComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/1nz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318021
    check-cast p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 318022
    iget-object v0, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->e:LX/1aZ;

    iput-object v0, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->e:LX/1aZ;

    .line 318023
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 318024
    if-ne p0, p1, :cond_1

    .line 318025
    :cond_0
    :goto_0
    return v0

    .line 318026
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 318027
    goto :goto_0

    .line 318028
    :cond_3
    check-cast p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 318029
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 318030
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 318031
    if-eq v2, v3, :cond_0

    .line 318032
    iget v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->a:F

    iget v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->a:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 318033
    goto :goto_0

    .line 318034
    :cond_4
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->b:Landroid/net/Uri;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 318035
    goto :goto_0

    .line 318036
    :cond_6
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->b:Landroid/net/Uri;

    if-nez v2, :cond_5

    .line 318037
    :cond_7
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 318038
    goto :goto_0

    .line 318039
    :cond_9
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_8

    .line 318040
    :cond_a
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->d:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 318041
    goto :goto_0

    .line 318042
    :cond_c
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->d:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 318043
    :cond_d
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->f:LX/1Up;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->f:LX/1Up;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->f:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 318044
    goto :goto_0

    .line 318045
    :cond_f
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->f:LX/1Up;

    if-nez v2, :cond_e

    .line 318046
    :cond_10
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->g:Landroid/graphics/PointF;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->g:Landroid/graphics/PointF;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->g:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 318047
    goto/16 :goto_0

    .line 318048
    :cond_12
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->g:Landroid/graphics/PointF;

    if-nez v2, :cond_11

    .line 318049
    :cond_13
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->h:Landroid/graphics/PointF;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->h:Landroid/graphics/PointF;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->h:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 318050
    goto/16 :goto_0

    .line 318051
    :cond_15
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->h:Landroid/graphics/PointF;

    if-nez v2, :cond_14

    .line 318052
    :cond_16
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->i:LX/1Up;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->i:LX/1Up;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->i:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 318053
    goto/16 :goto_0

    .line 318054
    :cond_18
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->i:LX/1Up;

    if-nez v2, :cond_17

    .line 318055
    :cond_19
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->j:LX/4Ab;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->j:LX/4Ab;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->j:LX/4Ab;

    invoke-virtual {v2, v3}, LX/4Ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_1a
    move v0, v1

    .line 318056
    goto/16 :goto_0

    .line 318057
    :cond_1b
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->j:LX/4Ab;

    if-nez v2, :cond_1a

    .line 318058
    :cond_1c
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->k:Landroid/graphics/ColorFilter;

    if-eqz v2, :cond_1e

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->k:Landroid/graphics/ColorFilter;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->k:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    :cond_1d
    move v0, v1

    .line 318059
    goto/16 :goto_0

    .line 318060
    :cond_1e
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->k:Landroid/graphics/ColorFilter;

    if-nez v2, :cond_1d

    .line 318061
    :cond_1f
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->l:LX/1dc;

    if-eqz v2, :cond_21

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->l:LX/1dc;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->l:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    :cond_20
    move v0, v1

    .line 318062
    goto/16 :goto_0

    .line 318063
    :cond_21
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->l:LX/1dc;

    if-nez v2, :cond_20

    .line 318064
    :cond_22
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->m:LX/1dc;

    if-eqz v2, :cond_24

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->m:LX/1dc;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->m:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_25

    :cond_23
    move v0, v1

    .line 318065
    goto/16 :goto_0

    .line 318066
    :cond_24
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->m:LX/1dc;

    if-nez v2, :cond_23

    .line 318067
    :cond_25
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->n:LX/1Up;

    if-eqz v2, :cond_27

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->n:LX/1Up;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->n:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    :cond_26
    move v0, v1

    .line 318068
    goto/16 :goto_0

    .line 318069
    :cond_27
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->n:LX/1Up;

    if-nez v2, :cond_26

    .line 318070
    :cond_28
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->o:LX/1dc;

    if-eqz v2, :cond_2a

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->o:LX/1dc;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->o:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2b

    :cond_29
    move v0, v1

    .line 318071
    goto/16 :goto_0

    .line 318072
    :cond_2a
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->o:LX/1dc;

    if-nez v2, :cond_29

    .line 318073
    :cond_2b
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->p:LX/1Up;

    if-eqz v2, :cond_2d

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->p:LX/1Up;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->p:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2e

    :cond_2c
    move v0, v1

    .line 318074
    goto/16 :goto_0

    .line 318075
    :cond_2d
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->p:LX/1Up;

    if-nez v2, :cond_2c

    .line 318076
    :cond_2e
    iget v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->q:I

    iget v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->q:I

    if-eq v2, v3, :cond_2f

    move v0, v1

    .line 318077
    goto/16 :goto_0

    .line 318078
    :cond_2f
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->r:LX/1dc;

    if-eqz v2, :cond_31

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->r:LX/1dc;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->r:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_32

    :cond_30
    move v0, v1

    .line 318079
    goto/16 :goto_0

    .line 318080
    :cond_31
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->r:LX/1dc;

    if-nez v2, :cond_30

    .line 318081
    :cond_32
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->s:LX/1Up;

    if-eqz v2, :cond_34

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->s:LX/1Up;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->s:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_35

    :cond_33
    move v0, v1

    .line 318082
    goto/16 :goto_0

    .line 318083
    :cond_34
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->s:LX/1Up;

    if-nez v2, :cond_33

    .line 318084
    :cond_35
    iget v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->t:I

    iget v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->t:I

    if-eq v2, v3, :cond_36

    move v0, v1

    .line 318085
    goto/16 :goto_0

    .line 318086
    :cond_36
    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->u:LX/1dc;

    if-eqz v2, :cond_37

    iget-object v2, p0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->u:LX/1dc;

    iget-object v3, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->u:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 318087
    goto/16 :goto_0

    .line 318088
    :cond_37
    iget-object v2, p1, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->u:LX/1dc;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 318089
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;

    .line 318090
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/components/fb/fresco/sizeaware/FbSizeAwareFrescoComponent$FbSizeAwareFrescoComponentImpl;->e:LX/1aZ;

    .line 318091
    return-object v0
.end method
