.class public Lcom/facebook/components/feed/StackComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/242;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/241;

.field private final e:LX/0ad;

.field private f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/241;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 270535
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 270536
    iput-object p2, p0, Lcom/facebook/components/feed/StackComponentPartDefinition;->d:LX/241;

    .line 270537
    iput-object p3, p0, Lcom/facebook/components/feed/StackComponentPartDefinition;->e:LX/0ad;

    .line 270538
    return-void
.end method

.method private a(LX/1De;LX/242;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/242;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 270539
    iget-object v0, p0, Lcom/facebook/components/feed/StackComponentPartDefinition;->d:LX/241;

    const/4 v1, 0x0

    .line 270540
    new-instance v2, LX/34P;

    invoke-direct {v2, v0}, LX/34P;-><init>(LX/241;)V

    .line 270541
    sget-object p0, LX/241;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/2uR;

    .line 270542
    if-nez p0, :cond_0

    .line 270543
    new-instance p0, LX/2uR;

    invoke-direct {p0}, LX/2uR;-><init>()V

    .line 270544
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/2uR;->a$redex0(LX/2uR;LX/1De;IILX/34P;)V

    .line 270545
    move-object v2, p0

    .line 270546
    move-object v1, v2

    .line 270547
    move-object v0, v1

    .line 270548
    iget-object v1, p2, LX/242;->a:LX/0Px;

    .line 270549
    iget-object v2, v0, LX/2uR;->a:LX/34P;

    iput-object v1, v2, LX/34P;->a:LX/0Px;

    .line 270550
    iget-object v2, v0, LX/2uR;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 270551
    move-object v0, v0

    .line 270552
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/components/feed/StackComponentPartDefinition;
    .locals 6

    .prologue
    .line 270553
    const-class v1, Lcom/facebook/components/feed/StackComponentPartDefinition;

    monitor-enter v1

    .line 270554
    :try_start_0
    sget-object v0, Lcom/facebook/components/feed/StackComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 270555
    sput-object v2, Lcom/facebook/components/feed/StackComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 270556
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270557
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 270558
    new-instance p0, Lcom/facebook/components/feed/StackComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/241;->a(LX/0QB;)LX/241;

    move-result-object v4

    check-cast v4, LX/241;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/components/feed/StackComponentPartDefinition;-><init>(Landroid/content/Context;LX/241;LX/0ad;)V

    .line 270559
    move-object v0, p0

    .line 270560
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 270561
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/components/feed/StackComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270562
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 270563
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 270564
    check-cast p2, LX/242;

    invoke-direct {p0, p1, p2}, Lcom/facebook/components/feed/StackComponentPartDefinition;->a(LX/1De;LX/242;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 270565
    check-cast p2, LX/242;

    invoke-direct {p0, p1, p2}, Lcom/facebook/components/feed/StackComponentPartDefinition;->a(LX/1De;LX/242;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 270566
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 270567
    check-cast p1, LX/242;

    .line 270568
    iget-object v0, p1, LX/242;->b:LX/0jW;

    return-object v0
.end method

.method public final iV_()Z
    .locals 3

    .prologue
    .line 270569
    iget-object v0, p0, Lcom/facebook/components/feed/StackComponentPartDefinition;->f:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 270570
    iget-object v0, p0, Lcom/facebook/components/feed/StackComponentPartDefinition;->e:LX/0ad;

    sget-short v1, LX/1Dd;->s:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/components/feed/StackComponentPartDefinition;->f:Ljava/lang/Boolean;

    .line 270571
    :cond_0
    iget-object v0, p0, Lcom/facebook/components/feed/StackComponentPartDefinition;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
