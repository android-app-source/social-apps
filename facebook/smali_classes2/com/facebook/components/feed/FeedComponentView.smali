.class public Lcom/facebook/components/feed/FeedComponentView;
.super Lcom/facebook/components/ComponentView;
.source ""

# interfaces
.implements LX/1dh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 313971
    invoke-direct {p0, p1}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    .line 313972
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 313973
    invoke-direct {p0, p1, p2}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 313974
    return-void
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 313975
    const v0, 0x7f0d00f3

    invoke-static {p0, v0}, LX/5JR;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0ht;->a(Landroid/view/View;)V

    .line 313976
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 313977
    const v0, 0x7f0d00f3

    invoke-static {p0, v0}, LX/5JR;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 313978
    if-eqz v0, :cond_0

    const v1, 0x7f0d00f3

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
