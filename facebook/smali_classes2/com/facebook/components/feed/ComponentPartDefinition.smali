.class public abstract Lcom/facebook/components/feed/ComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Rj;
.implements LX/0Ya;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DeprecatedSuperclass"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<TP;",
        "LX/1dV;",
        "TE;",
        "Lcom/facebook/components/feed/FeedComponentView;",
        ">;",
        "LX/1Rj",
        "<TP;TE;>;",
        "LX/0Ya;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:LX/1V3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1V3",
            "<TP;TE;>;"
        }
    .end annotation
.end field

.field public c:LX/0ad;

.field private final d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 256944
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 256950
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 256951
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->d:Ljava/lang/String;

    .line 256952
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/components/feed/ComponentPartDefinition;

    invoke-static {v0}, LX/1V3;->a(LX/0QB;)LX/1V3;

    move-result-object v1

    check-cast v1, LX/1V3;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v1, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->b:LX/1V3;

    iput-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->c:LX/0ad;

    .line 256953
    return-void
.end method

.method public static e()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256949
    new-instance v0, LX/1Uz;

    invoke-direct {v0}, LX/1Uz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 256948
    const/4 v0, -0x1

    return v0
.end method

.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 256945
    invoke-virtual {p0}, Lcom/facebook/components/feed/ComponentPartDefinition;->iV_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256946
    sget-object v0, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    .line 256947
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/components/feed/ComponentPartDefinition;->d()LX/1Cz;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 256943
    check-cast p3, LX/1Pn;

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TP;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end method

.method public a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;TP;TE;)",
            "LX/1dV;"
        }
    .end annotation

    .prologue
    .line 256942
    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->b:LX/1V3;

    iget-object v4, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/components/feed/ComponentPartDefinition;->f()Z

    move-result v6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, LX/1V3;->a(LX/1aD;Ljava/lang/Object;LX/1Pn;Ljava/lang/String;LX/1Rj;Z)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 256941
    check-cast p3, LX/1Pn;

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public a(LX/1aD;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;TP;)V"
        }
    .end annotation

    .prologue
    .line 256940
    return-void
.end method

.method public a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 256954
    invoke-virtual {p4, p2}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 256955
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;)V
    .locals 4

    .prologue
    .line 256927
    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pn;

    .line 256928
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;)V

    .line 256929
    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->b:LX/1V3;

    .line 256930
    invoke-virtual {p2}, LX/1dV;->getComponentView()Lcom/facebook/components/ComponentView;

    move-result-object v1

    .line 256931
    if-eqz v1, :cond_0

    .line 256932
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 256933
    :cond_0
    invoke-virtual {p2}, LX/1dV;->i()LX/1mg;

    move-result-object v1

    .line 256934
    if-eqz v1, :cond_1

    invoke-static {v0}, LX/1V3;->b(LX/1V3;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 256935
    invoke-interface {p0, p1}, LX/1Rj;->b(Ljava/lang/Object;)LX/0jW;

    move-result-object v2

    .line 256936
    if-eqz v2, :cond_1

    .line 256937
    check-cast p3, LX/1Pr;

    new-instance v3, LX/1z8;

    invoke-static {p1, v2, p0}, LX/1V3;->a(Ljava/lang/Object;LX/0jW;LX/1Rj;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, LX/1z8;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v3, v1}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 256938
    :cond_1
    invoke-virtual {p2}, LX/1dV;->j()V

    .line 256939
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x122ee810

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 256926
    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    const/16 v1, 0x1f

    const v2, -0x6d68427e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 256925
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public a(LX/1Pn;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 256924
    const/4 v0, 0x0

    return v0
.end method

.method public b(Ljava/lang/Object;)LX/0jW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)",
            "LX/0jW;"
        }
    .end annotation

    .prologue
    .line 256923
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 256921
    invoke-virtual {p4}, Lcom/facebook/components/ComponentView;->h()V

    .line 256922
    return-void
.end method

.method public bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 256908
    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 256920
    const/4 v0, 0x1

    return v0
.end method

.method public d()LX/1Cz;
    .locals 1

    .prologue
    .line 256919
    sget-object v0, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public f()Z
    .locals 3

    .prologue
    .line 256912
    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->b:LX/1V3;

    .line 256913
    iget-object v1, v0, LX/1V3;->d:LX/1V4;

    .line 256914
    iget-object v2, v1, LX/1V4;->f:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    .line 256915
    iget-object v2, v1, LX/1V4;->a:LX/0ad;

    sget-short p0, LX/1Dd;->e:S

    const/4 v0, 0x0

    invoke-interface {v2, p0, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, LX/1V4;->f:Ljava/lang/Boolean;

    .line 256916
    :cond_0
    iget-object v2, v1, LX/1V4;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v1, v2

    .line 256917
    move v0, v1

    .line 256918
    return v0
.end method

.method public iV_()Z
    .locals 3

    .prologue
    .line 256909
    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 256910
    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->c:LX/0ad;

    sget-short v1, LX/1Dd;->n:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->e:Ljava/lang/Boolean;

    .line 256911
    :cond_0
    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
