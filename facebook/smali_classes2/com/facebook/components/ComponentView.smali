.class public Lcom/facebook/components/ComponentView;
.super LX/1cn;
.source ""


# static fields
.field private static final g:[I


# instance fields
.field public a:LX/1dV;

.field private final b:LX/1dF;

.field private c:Z

.field public final d:Landroid/graphics/Rect;

.field private final e:Landroid/view/accessibility/AccessibilityManager;

.field private final f:LX/1d7;

.field private h:LX/1dV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 282629
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/components/ComponentView;->g:[I

    return-void
.end method

.method public constructor <init>(LX/1De;)V
    .locals 1

    .prologue
    .line 282630
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/components/ComponentView;-><init>(LX/1De;Landroid/util/AttributeSet;)V

    .line 282631
    return-void
.end method

.method public constructor <init>(LX/1De;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 282632
    invoke-direct {p0, p1, p2}, LX/1cn;-><init>(LX/1De;Landroid/util/AttributeSet;)V

    .line 282633
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/components/ComponentView;->d:Landroid/graphics/Rect;

    .line 282634
    new-instance v0, LX/1d6;

    invoke-direct {v0, p0}, LX/1d6;-><init>(Lcom/facebook/components/ComponentView;)V

    iput-object v0, p0, Lcom/facebook/components/ComponentView;->f:LX/1d7;

    .line 282635
    new-instance v0, LX/1dF;

    invoke-direct {v0, p0}, LX/1dF;-><init>(Lcom/facebook/components/ComponentView;)V

    iput-object v0, p0, Lcom/facebook/components/ComponentView;->b:LX/1dF;

    .line 282636
    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, LX/1De;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/facebook/components/ComponentView;->e:Landroid/view/accessibility/AccessibilityManager;

    .line 282637
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 282638
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 282639
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 282640
    new-instance v0, LX/1De;

    invoke-direct {v0, p1}, LX/1De;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, p2}, Lcom/facebook/components/ComponentView;-><init>(LX/1De;Landroid/util/AttributeSet;)V

    .line 282641
    return-void
.end method

.method private static b(LX/1cn;)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 282642
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/1cn;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 282643
    invoke-virtual {p0, v1}, LX/1cn;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 282644
    invoke-virtual {v0}, Landroid/view/View;->isLayoutRequested()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 282645
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/view/View;->measure(II)V

    .line 282646
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 282647
    :cond_0
    instance-of v3, v0, LX/1cn;

    if-eqz v3, :cond_1

    .line 282648
    check-cast v0, LX/1cn;

    invoke-static {v0}, Lcom/facebook/components/ComponentView;->b(LX/1cn;)V

    .line 282649
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 282650
    :cond_2
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 282539
    iget-boolean v0, p0, Lcom/facebook/components/ComponentView;->c:Z

    if-nez v0, :cond_1

    .line 282540
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/components/ComponentView;->c:Z

    .line 282541
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-eqz v0, :cond_0

    .line 282542
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    invoke-virtual {v0}, LX/1dV;->a()V

    .line 282543
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/components/ComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1cs;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p0, v0}, LX/1cn;->b(Z)V

    .line 282544
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->e:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lcom/facebook/components/ComponentView;->f:LX/1d7;

    .line 282545
    sget-object p0, LX/1d8;->a:LX/1dB;

    invoke-interface {p0, v0, v1}, LX/1dB;->a(Landroid/view/accessibility/AccessibilityManager;LX/1d7;)Z

    .line 282546
    :cond_1
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    .line 282651
    iget-boolean v0, p0, Lcom/facebook/components/ComponentView;->c:Z

    if-eqz v0, :cond_2

    .line 282652
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/components/ComponentView;->c:Z

    .line 282653
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-eqz v0, :cond_1

    .line 282654
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->b:LX/1dF;

    .line 282655
    invoke-virtual {v0}, LX/1dF;->c()V

    .line 282656
    iget-object v1, v0, LX/1dF;->n:LX/3Ep;

    if-eqz v1, :cond_0

    .line 282657
    iget-object v1, v0, LX/1dF;->n:LX/3Ep;

    invoke-static {v1}, LX/1cy;->a(LX/3Ep;)V

    .line 282658
    const/4 v1, 0x0

    iput-object v1, v0, LX/1dF;->n:LX/3Ep;

    .line 282659
    :cond_0
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    invoke-virtual {v0}, LX/1dV;->d()V

    .line 282660
    :cond_1
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->e:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lcom/facebook/components/ComponentView;->f:LX/1d7;

    .line 282661
    sget-object p0, LX/1d8;->a:LX/1dB;

    invoke-interface {p0, v0, v1}, LX/1dB;->b(Landroid/view/accessibility/AccessibilityManager;LX/1d7;)Z

    .line 282662
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(LX/1dN;Landroid/graphics/Rect;)V
    .locals 13

    .prologue
    .line 282663
    if-nez p2, :cond_e

    .line 282664
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 282665
    :goto_0
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->b:LX/1dF;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 282666
    invoke-static {}, LX/1dS;->b()V

    .line 282667
    const-string v1, "mount"

    invoke-static {v1}, LX/1mm;->a(Ljava/lang/String;)V

    .line 282668
    iget-object v1, v0, LX/1dF;->j:Lcom/facebook/components/ComponentView;

    .line 282669
    iget-object v4, v1, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    move-object v7, v4

    .line 282670
    iget-object v1, v7, LX/1dV;->k:LX/1De;

    move-object v1, v1

    .line 282671
    iget-object v4, v1, LX/1De;->d:LX/1cp;

    move-object v8, v4

    .line 282672
    if-eqz v8, :cond_0

    .line 282673
    const/4 v1, 0x6

    invoke-virtual {v8, v1, v7}, LX/1cp;->a(ILjava/lang/Object;)V

    .line 282674
    :cond_0
    iget-object v1, p1, LX/1dN;->d:LX/1my;

    if-eqz v1, :cond_1c

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 282675
    if-eqz v1, :cond_1d

    iget-object v4, v0, LX/1dF;->n:LX/3Ep;

    if-nez v4, :cond_1d

    .line 282676
    sget-object v4, LX/1cy;->A:LX/0Zj;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3Ep;

    .line 282677
    if-nez v4, :cond_1

    .line 282678
    new-instance v4, LX/3Ep;

    invoke-direct {v4}, LX/3Ep;-><init>()V

    .line 282679
    :cond_1
    move-object v4, v4

    .line 282680
    iput-object v4, v0, LX/1dF;->n:LX/3Ep;

    .line 282681
    :cond_2
    :goto_2
    iget-object v4, v0, LX/1dF;->n:LX/3Ep;

    if-eqz v4, :cond_3

    .line 282682
    iget-object v4, v0, LX/1dF;->n:LX/3Ep;

    invoke-virtual {v4}, LX/3Ep;->a()V

    .line 282683
    :cond_3
    iget-boolean v1, v0, LX/1dF;->e:Z

    if-eqz v1, :cond_4

    .line 282684
    invoke-static {v0, v2}, LX/1dF;->a(LX/1dF;Z)V

    .line 282685
    invoke-static {v0, p1}, LX/1dF;->b(LX/1dF;LX/1dN;)V

    .line 282686
    :cond_4
    iget-object v1, v0, LX/1dF;->m:LX/1dH;

    const/4 v4, 0x0

    .line 282687
    iput v4, v1, LX/1dH;->a:I

    .line 282688
    iput v4, v1, LX/1dH;->b:I

    .line 282689
    iput v4, v1, LX/1dH;->c:I

    .line 282690
    iput v4, v1, LX/1dH;->d:I

    .line 282691
    iget v1, p1, LX/1dN;->A:I

    move v9, v1

    .line 282692
    if-eqz p2, :cond_f

    move v6, v2

    .line 282693
    :goto_3
    if-eqz v6, :cond_10

    sget-boolean v1, LX/1V5;->f:Z

    if-nez v1, :cond_10

    invoke-static {v0, p1, p2}, LX/1dF;->d(LX/1dF;LX/1dN;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 282694
    iget-object v1, v0, LX/1dF;->k:Landroid/graphics/Rect;

    invoke-virtual {v1, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 282695
    :cond_5
    :goto_4
    iput-boolean v3, v0, LX/1dF;->e:Z

    .line 282696
    invoke-static {v0, p1, p2}, LX/1dF;->b(LX/1dF;LX/1dN;Landroid/graphics/Rect;)V

    .line 282697
    iget-object v1, p1, LX/1dN;->d:LX/1my;

    move-object v1, v1

    .line 282698
    iget-object v2, v0, LX/1dF;->n:LX/3Ep;

    if-eqz v2, :cond_c

    .line 282699
    const/4 v2, 0x0

    iget-object v4, v0, LX/1dF;->a:LX/0tf;

    invoke-virtual {v4}, LX/0tf;->a()I

    move-result v6

    move v5, v2

    :goto_5
    if-ge v5, v6, :cond_7

    .line 282700
    iget-object v2, v0, LX/1dF;->a:LX/0tf;

    invoke-virtual {v2, v5}, LX/0tf;->c(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1cv;

    .line 282701
    instance-of v4, v2, LX/1dJ;

    if-eqz v4, :cond_6

    move-object v4, v2

    .line 282702
    check-cast v4, LX/1dJ;

    .line 282703
    iget-object v10, v4, LX/1dJ;->f:Ljava/lang/String;

    move-object v4, v10

    .line 282704
    if-eqz v4, :cond_6

    .line 282705
    iget-object v10, v0, LX/1dF;->n:LX/3Ep;

    .line 282706
    iget-object v11, v2, LX/1cv;->b:Ljava/lang/Object;

    move-object v2, v11

    .line 282707
    check-cast v2, Landroid/view/View;

    invoke-virtual {v10, v4, v2}, LX/3Ep;->a(Ljava/lang/String;Landroid/view/View;)V

    .line 282708
    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_5

    .line 282709
    :cond_7
    iget-object v2, v0, LX/1dF;->n:LX/3Ep;

    .line 282710
    iget-object v4, v2, LX/3Ep;->b:LX/01J;

    iget-object v5, v2, LX/3Ep;->a:LX/01J;

    invoke-virtual {v4, v5}, LX/01J;->a(LX/01J;)V

    .line 282711
    iget-object v4, v2, LX/3Ep;->a:LX/01J;

    invoke-virtual {v4}, LX/01J;->clear()V

    .line 282712
    const/4 v4, 0x0

    move v5, v4

    :goto_6
    iget-object v4, v2, LX/3Ep;->b:LX/01J;

    invoke-virtual {v4}, LX/01J;->size()I

    move-result v4

    if-ge v5, v4, :cond_9

    .line 282713
    iget-object v4, v2, LX/3Ep;->b:LX/01J;

    invoke-virtual {v4, v5}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 282714
    iget-object v6, v2, LX/3Ep;->c:LX/01J;

    invoke-virtual {v6, v4}, LX/01J;->a(Ljava/lang/Object;)I

    move-result v4

    .line 282715
    if-ltz v4, :cond_8

    .line 282716
    iget-object v6, v2, LX/3Ep;->c:LX/01J;

    invoke-virtual {v6, v4}, LX/01J;->d(I)Ljava/lang/Object;

    .line 282717
    :cond_8
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_6

    .line 282718
    :cond_9
    iget-object v4, v2, LX/3Ep;->a:LX/01J;

    .line 282719
    iget-object v5, v2, LX/3Ep;->c:LX/01J;

    iput-object v5, v2, LX/3Ep;->a:LX/01J;

    .line 282720
    iput-object v4, v2, LX/3Ep;->c:LX/01J;

    .line 282721
    iget-object v4, v2, LX/3Ep;->d:LX/01J;

    invoke-virtual {v4}, LX/01J;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move v5, v4

    :goto_7
    if-ltz v5, :cond_b

    .line 282722
    iget-object v4, v2, LX/3Ep;->d:LX/01J;

    invoke-virtual {v4, v5}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 282723
    iget-object v6, v2, LX/3Ep;->b:LX/01J;

    invoke-virtual {v6, v4}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 282724
    iget-object v6, v2, LX/3Ep;->b:LX/01J;

    invoke-virtual {v6, v4}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 282725
    :goto_8
    add-int/lit8 v4, v5, -0x1

    move v5, v4

    goto :goto_7

    .line 282726
    :cond_a
    iget-object v4, v2, LX/3Ep;->d:LX/01J;

    invoke-virtual {v4, v5}, LX/01J;->d(I)Ljava/lang/Object;

    goto :goto_8

    .line 282727
    :cond_b
    invoke-static {v2, v1}, LX/3Ep;->b(LX/3Ep;LX/1my;)V

    .line 282728
    :cond_c
    invoke-static {v0, p1}, LX/1dF;->a(LX/1dF;LX/1dN;)V

    .line 282729
    invoke-static {v0, v3}, LX/1dF;->a(LX/1dF;Z)V

    .line 282730
    iput v9, v0, LX/1dF;->q:I

    .line 282731
    if-eqz v8, :cond_d

    .line 282732
    iget-object v1, v7, LX/1dV;->k:LX/1De;

    move-object v1, v1

    .line 282733
    iget-object v2, v1, LX/1De;->c:Ljava/lang/String;

    move-object v1, v2

    .line 282734
    iget-object v2, v0, LX/1dF;->m:LX/1dH;

    const/4 v5, 0x6

    .line 282735
    const-string v3, "log_tag"

    invoke-virtual {v8, v5, v7, v3, v1}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 282736
    const-string v3, "mounted_count"

    iget v4, v2, LX/1dH;->a:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v5, v7, v3, v4}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 282737
    const-string v3, "unmounted_count"

    iget v4, v2, LX/1dH;->b:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v5, v7, v3, v4}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 282738
    const-string v3, "updated_count"

    iget v4, v2, LX/1dH;->c:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v5, v7, v3, v4}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 282739
    const-string v3, "no_op_count"

    iget v4, v2, LX/1dH;->d:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v5, v7, v3, v4}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 282740
    const-string v3, "is_dirty"

    iget-boolean v4, v0, LX/1dF;->e:Z

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v5, v7, v3, v4}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 282741
    const/16 v3, 0x10

    invoke-virtual {v8, v5, v7, v3}, LX/1cp;->a(ILjava/lang/Object;I)V

    .line 282742
    :cond_d
    invoke-static {}, LX/1mm;->a()V

    .line 282743
    return-void

    .line 282744
    :cond_e
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_0

    :cond_f
    move v6, v3

    .line 282745
    goto/16 :goto_3

    .line 282746
    :cond_10
    if-eqz v6, :cond_11

    sget-boolean v1, LX/1V5;->f:Z

    if-eqz v1, :cond_11

    invoke-static {v0, p1, p2}, LX/1dF;->e(LX/1dF;LX/1dN;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 282747
    :cond_11
    invoke-virtual {p1}, LX/1dN;->c()I

    move-result v10

    move v5, v3

    :goto_9
    if-ge v5, v10, :cond_1b

    .line 282748
    invoke-virtual {p1, v5}, LX/1dN;->b(I)LX/1dK;

    move-result-object v11

    .line 282749
    iget-object v1, v11, LX/1dK;->b:LX/1X1;

    move-object v12, v1

    .line 282750
    invoke-virtual {v12}, LX/1X1;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1mm;->a(Ljava/lang/String;)V

    .line 282751
    invoke-static {v0, v5}, LX/1dF;->a(LX/1dF;I)LX/1cv;

    move-result-object p0

    .line 282752
    if-eqz p0, :cond_14

    move v4, v2

    .line 282753
    :goto_a
    if-eqz v6, :cond_12

    invoke-static {p0}, LX/1dF;->a(LX/1cv;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 282754
    iget-object v1, v11, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v1, v1

    .line 282755
    invoke-static {p2, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_15

    :cond_12
    move v1, v2

    .line 282756
    :goto_b
    if-eqz v1, :cond_16

    if-nez v4, :cond_16

    .line 282757
    invoke-static {v0, v5, v11, p1}, LX/1dF;->a(LX/1dF;ILX/1dK;LX/1dN;)V

    .line 282758
    :cond_13
    :goto_c
    invoke-static {}, LX/1mm;->a()V

    .line 282759
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_9

    :cond_14
    move v4, v3

    .line 282760
    goto :goto_a

    :cond_15
    move v1, v3

    .line 282761
    goto :goto_b

    .line 282762
    :cond_16
    if-nez v1, :cond_17

    if-eqz v4, :cond_17

    .line 282763
    iget-object v1, v0, LX/1dF;->i:LX/1De;

    iget-object v4, v0, LX/1dF;->f:LX/0tf;

    invoke-static {v0, v1, v5, v4}, LX/1dF;->a(LX/1dF;LX/1De;ILX/0tf;)V

    goto :goto_c

    .line 282764
    :cond_17
    if-eqz v4, :cond_13

    .line 282765
    if-eqz v6, :cond_18

    .line 282766
    iget-object v1, v12, LX/1X1;->e:LX/1S3;

    move-object v1, v1

    .line 282767
    invoke-virtual {v1}, LX/1S3;->d()Z

    move-result v1

    move v1, v1

    .line 282768
    if-eqz v1, :cond_18

    .line 282769
    iget-object v1, p0, LX/1cv;->a:LX/1X1;

    move-object v1, v1

    .line 282770
    invoke-static {v1}, LX/1X1;->d(LX/1X1;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 282771
    :cond_18
    :goto_d
    iget-boolean v1, v0, LX/1dF;->e:Z

    if-eqz v1, :cond_13

    .line 282772
    if-ltz v9, :cond_19

    iget v1, v0, LX/1dF;->q:I

    if-ne v9, v1, :cond_19

    move v1, v2

    .line 282773
    :goto_e
    invoke-static {v0, v11, p0, v1, v8}, LX/1dF;->a(LX/1dF;LX/1dK;LX/1cv;ZLX/1cp;)Z

    move-result v1

    .line 282774
    if-eqz v1, :cond_1a

    .line 282775
    iget-object v1, v0, LX/1dF;->m:LX/1dH;

    .line 282776
    iget v4, v1, LX/1dH;->c:I

    add-int/lit8 v11, v4, 0x1

    iput v11, v1, LX/1dH;->c:I

    .line 282777
    goto :goto_c

    :cond_19
    move v1, v3

    .line 282778
    goto :goto_e

    .line 282779
    :cond_1a
    iget-object v1, v0, LX/1dF;->m:LX/1dH;

    .line 282780
    iget v4, v1, LX/1dH;->d:I

    add-int/lit8 v11, v4, 0x1

    iput v11, v1, LX/1dH;->d:I

    .line 282781
    goto :goto_c

    .line 282782
    :cond_1b
    if-eqz v6, :cond_5

    .line 282783
    const/4 v2, 0x0

    .line 282784
    invoke-virtual {p2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 282785
    iget-object v1, v0, LX/1dF;->k:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    .line 282786
    :goto_f
    goto/16 :goto_4

    :cond_1c
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 282787
    :cond_1d
    if-nez v1, :cond_2

    iget-object v4, v0, LX/1dF;->n:LX/3Ep;

    if-eqz v4, :cond_2

    .line 282788
    iget-object v4, v0, LX/1dF;->n:LX/3Ep;

    invoke-static {v4}, LX/1cy;->a(LX/3Ep;)V

    .line 282789
    const/4 v4, 0x0

    iput-object v4, v0, LX/1dF;->n:LX/3Ep;

    goto/16 :goto_2

    .line 282790
    :cond_1e
    iget-object v1, p0, LX/1cv;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 282791
    check-cast v1, Landroid/view/View;

    invoke-static {v1, p2}, LX/1dF;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_d

    .line 282792
    :cond_1f
    iget-object v1, p1, LX/1dN;->l:Ljava/util/ArrayList;

    move-object v5, v1

    .line 282793
    iget-object v1, p1, LX/1dN;->m:Ljava/util/ArrayList;

    move-object v6, v1

    .line 282794
    invoke-virtual {p1}, LX/1dN;->c()I

    move-result v10

    .line 282795
    invoke-virtual {p1}, LX/1dN;->c()I

    move-result v1

    iput v1, v0, LX/1dF;->o:I

    move v4, v2

    .line 282796
    :goto_10
    if-ge v4, v10, :cond_20

    .line 282797
    iget v11, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1dK;

    .line 282798
    iget-object v12, v1, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v1, v12

    .line 282799
    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-gt v11, v1, :cond_22

    .line 282800
    iput v4, v0, LX/1dF;->o:I

    .line 282801
    :cond_20
    invoke-virtual {p1}, LX/1dN;->c()I

    move-result v1

    iput v1, v0, LX/1dF;->p:I

    .line 282802
    :goto_11
    if-ge v2, v10, :cond_21

    .line 282803
    iget v4, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1dK;

    .line 282804
    iget-object v5, v1, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v1, v5

    .line 282805
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-ge v4, v1, :cond_23

    .line 282806
    iput v2, v0, LX/1dF;->p:I

    .line 282807
    :cond_21
    iget-object v1, v0, LX/1dF;->k:Landroid/graphics/Rect;

    invoke-virtual {v1, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_f

    .line 282808
    :cond_22
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_10

    .line 282809
    :cond_23
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_11
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 282810
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    .line 282811
    iget-boolean v1, v0, LX/1dV;->m:Z

    move v0, v1

    .line 282812
    if-eqz v0, :cond_0

    .line 282813
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    invoke-virtual {v0, p1}, LX/1dV;->a(Landroid/graphics/Rect;)V

    return-void

    .line 282814
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "To perform incremental mounting, you need first to enable it when creating the ComponentTree."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final cq_()V
    .locals 2

    .prologue
    .line 282815
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-eqz v0, :cond_1

    .line 282816
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    .line 282817
    invoke-static {}, LX/1dS;->b()V

    .line 282818
    invoke-static {v0}, LX/1dV;->n(LX/1dV;)Z

    move-result v1

    move v0, v1

    .line 282819
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/components/ComponentView;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282820
    :cond_0
    invoke-static {p0}, Lcom/facebook/components/ComponentView;->b(LX/1cn;)V

    .line 282821
    :cond_1
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 282822
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    .line 282823
    iget-boolean v1, v0, LX/1dV;->l:Z

    move v0, v1

    .line 282824
    if-eqz v0, :cond_0

    .line 282825
    const/4 v0, 0x0

    .line 282826
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, LX/1cn;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 282827
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    iput-object v0, p0, Lcom/facebook/components/ComponentView;->h:LX/1dV;

    .line 282828
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 282829
    const/4 v0, 0x0

    return v0
.end method

.method public final g()V
    .locals 7

    .prologue
    .line 282613
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->b:LX/1dF;

    .line 282614
    iget-object v1, v0, LX/1dF;->d:[J

    if-nez v1, :cond_1

    .line 282615
    :cond_0
    return-void

    .line 282616
    :cond_1
    const/4 v1, 0x0

    iget-object v2, v0, LX/1dF;->d:[J

    array-length v2, v2

    :goto_0
    if-ge v1, v2, :cond_0

    .line 282617
    invoke-static {v0, v1}, LX/1dF;->a(LX/1dF;I)LX/1cv;

    move-result-object v3

    .line 282618
    if-eqz v3, :cond_2

    .line 282619
    iget-boolean v4, v3, LX/1cv;->r:Z

    move v4, v4

    .line 282620
    if-nez v4, :cond_2

    .line 282621
    iget-object v4, v3, LX/1cv;->a:LX/1X1;

    move-object v4, v4

    .line 282622
    iget-object v5, v4, LX/1X1;->e:LX/1S3;

    move-object v5, v5

    .line 282623
    iget-object v6, v0, LX/1dF;->i:LX/1De;

    .line 282624
    iget-object p0, v3, LX/1cv;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 282625
    invoke-virtual {v5, v6, p0, v4}, LX/1S3;->b(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 282626
    const/4 v4, 0x1

    .line 282627
    iput-boolean v4, v3, LX/1cv;->r:Z

    .line 282628
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getComponent()LX/1dV;
    .locals 1

    .prologue
    .line 282830
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    return-object v0
.end method

.method public getPreviousMountBounds()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 282538
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->d:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 282547
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->b:LX/1dF;

    invoke-virtual {v0}, LX/1dF;->c()V

    .line 282548
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 282549
    iget-boolean v0, p0, Lcom/facebook/components/ComponentView;->c:Z

    if-eqz v0, :cond_0

    .line 282550
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to clear the ComponentTree while attached."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282551
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    .line 282552
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 282553
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    .line 282554
    iget-boolean v1, v0, LX/1dV;->m:Z

    move v0, v1

    .line 282555
    if-eqz v0, :cond_0

    .line 282556
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    invoke-virtual {v0}, LX/1dV;->c()V

    return-void

    .line 282557
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "To perform incremental mounting, you need first to enable it when creating the ComponentTree."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 282558
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    .line 282559
    iget-boolean p0, v0, LX/1dV;->m:Z

    move v0, p0

    .line 282560
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 282561
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-eqz v0, :cond_0

    .line 282562
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    invoke-virtual {v0}, LX/1dV;->j()V

    .line 282563
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    .line 282564
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 282565
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->b:LX/1dF;

    .line 282566
    invoke-static {}, LX/1dS;->b()V

    .line 282567
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1dF;->e:Z

    .line 282568
    iget-object v1, v0, LX/1dF;->k:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    .line 282569
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 282570
    return-void
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 282571
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->b:LX/1dF;

    .line 282572
    invoke-static {}, LX/1dS;->b()V

    .line 282573
    iget-boolean p0, v0, LX/1dF;->e:Z

    move v0, p0

    .line 282574
    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x245959d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 282575
    invoke-super {p0}, LX/1cn;->onAttachedToWindow()V

    .line 282576
    invoke-direct {p0}, Lcom/facebook/components/ComponentView;->o()V

    .line 282577
    const/16 v1, 0x2d

    const v2, 0x65cfe02d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x13b8550c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 282578
    invoke-super {p0}, LX/1cn;->onDetachedFromWindow()V

    .line 282579
    invoke-direct {p0}, Lcom/facebook/components/ComponentView;->p()V

    .line 282580
    const/16 v1, 0x2d

    const v2, -0x37e6999d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 0

    .prologue
    .line 282581
    invoke-super {p0}, LX/1cn;->onFinishTemporaryDetach()V

    .line 282582
    invoke-direct {p0}, Lcom/facebook/components/ComponentView;->o()V

    .line 282583
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 282584
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 282585
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 282586
    iget-object v2, p0, Lcom/facebook/components/ComponentView;->h:LX/1dV;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-nez v2, :cond_0

    .line 282587
    iget-object v2, p0, Lcom/facebook/components/ComponentView;->h:LX/1dV;

    invoke-virtual {p0, v2}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 282588
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/components/ComponentView;->h:LX/1dV;

    .line 282589
    :cond_0
    iget-object v2, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-eqz v2, :cond_1

    .line 282590
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    sget-object v1, Lcom/facebook/components/ComponentView;->g:[I

    invoke-virtual {v0, p1, p2, v1}, LX/1dV;->a(II[I)V

    .line 282591
    sget-object v0, Lcom/facebook/components/ComponentView;->g:[I

    const/4 v1, 0x0

    aget v1, v0, v1

    .line 282592
    sget-object v0, Lcom/facebook/components/ComponentView;->g:[I

    const/4 v2, 0x1

    aget v0, v0, v2

    .line 282593
    :cond_1
    invoke-virtual {p0, v1, v0}, Lcom/facebook/components/ComponentView;->setMeasuredDimension(II)V

    .line 282594
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 282595
    invoke-super {p0}, LX/1cn;->onStartTemporaryDetach()V

    .line 282596
    invoke-direct {p0}, Lcom/facebook/components/ComponentView;->p()V

    .line 282597
    return-void
.end method

.method public setComponent(LX/1dV;)V
    .locals 1

    .prologue
    .line 282598
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/components/ComponentView;->h:LX/1dV;

    .line 282599
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-ne v0, p1, :cond_1

    .line 282600
    iget-boolean v0, p0, Lcom/facebook/components/ComponentView;->c:Z

    if-eqz v0, :cond_0

    .line 282601
    invoke-virtual {p0}, Lcom/facebook/components/ComponentView;->g()V

    .line 282602
    :cond_0
    :goto_0
    return-void

    .line 282603
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/components/ComponentView;->m()V

    .line 282604
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-eqz v0, :cond_3

    .line 282605
    iget-boolean v0, p0, Lcom/facebook/components/ComponentView;->c:Z

    if-eqz v0, :cond_2

    .line 282606
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    invoke-virtual {v0}, LX/1dV;->d()V

    .line 282607
    :cond_2
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    invoke-virtual {v0}, LX/1dV;->e()V

    .line 282608
    :cond_3
    iput-object p1, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    .line 282609
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    if-eqz v0, :cond_0

    .line 282610
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    invoke-virtual {v0, p0}, LX/1dV;->a(Lcom/facebook/components/ComponentView;)V

    .line 282611
    iget-boolean v0, p0, Lcom/facebook/components/ComponentView;->c:Z

    if-eqz v0, :cond_0

    .line 282612
    iget-object v0, p0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    invoke-virtual {v0}, LX/1dV;->a()V

    goto :goto_0
.end method
