.class public Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfigurationSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 106106
    const-class v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 106150
    const-class v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfigurationSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106149
    new-instance v0, LX/Av4;

    invoke-direct {v0}, LX/Av4;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 106136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106137
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 106138
    iput-object v3, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    .line 106139
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 106140
    iput-object v3, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->b:Ljava/lang/String;

    .line 106141
    :goto_1
    return-void

    .line 106142
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v2, v0, [Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 106143
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    array-length v0, v2

    if-ge v1, v0, :cond_1

    .line 106144
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 106145
    aput-object v0, v2, v1

    .line 106146
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 106147
    :cond_1
    invoke-static {v2}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    goto :goto_0

    .line 106148
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;)V
    .locals 1

    .prologue
    .line 106132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106133
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;->a:LX/0Px;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    .line 106134
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->b:Ljava/lang/String;

    .line 106135
    return-void
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;
    .locals 2

    .prologue
    .line 106131
    new-instance v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 106151
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 106122
    if-ne p0, p1, :cond_1

    .line 106123
    :cond_0
    :goto_0
    return v0

    .line 106124
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    if-nez v2, :cond_2

    move v0, v1

    .line 106125
    goto :goto_0

    .line 106126
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    .line 106127
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 106128
    goto :goto_0

    .line 106129
    :cond_3
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 106130
    goto :goto_0
.end method

.method public getAppliedInspirations()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "applied_inspirations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 106121
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    return-object v0
.end method

.method public getStoryId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "story_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 106120
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 106119
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 106107
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    if-nez v0, :cond_1

    .line 106108
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 106109
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 106110
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 106111
    :goto_0
    return-void

    .line 106112
    :cond_1
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 106113
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 106114
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 106115
    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->writeToParcel(Landroid/os/Parcel;I)V

    .line 106116
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 106117
    :cond_2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 106118
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
