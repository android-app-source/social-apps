.class public Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field

.field private static final k:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile z:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;


# instance fields
.field public final l:LX/AwR;

.field public final m:LX/0SG;

.field public final n:Landroid/content/Context;

.field public final o:LX/03V;

.field public final p:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final q:LX/0tX;

.field public final r:LX/0tS;

.field public final s:LX/ArM;

.field public final t:LX/0fO;

.field public final u:LX/0TD;

.field public final v:Ljava/util/concurrent/Executor;

.field private w:LX/3AP;

.field public x:LX/0i7;

.field public final y:LX/0Vj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vj",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;",
            ">;",
            "LX/Aw4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 119634
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "inspiration_nux/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 119635
    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a:LX/0Tn;

    const-string v1, "time_fetch"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->b:LX/0Tn;

    .line 119636
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a:LX/0Tn;

    const-string v1, "videos_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->c:LX/0Tn;

    .line 119637
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a:LX/0Tn;

    const-string v1, "video"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->d:LX/0Tn;

    .line 119638
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a:LX/0Tn;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->e:LX/0Tn;

    .line 119639
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a:LX/0Tn;

    const-string v1, "image"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->f:LX/0Tn;

    .line 119640
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a:LX/0Tn;

    const-string v1, "sound_effect"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->g:LX/0Tn;

    .line 119641
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a:LX/0Tn;

    const-string v1, "nux_cache_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->h:LX/0Tn;

    .line 119642
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a:LX/0Tn;

    const-string v1, "failed_attempt_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->i:LX/0Tn;

    .line 119643
    const/4 v0, 0x7

    new-array v0, v0, [LX/0Tn;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->b:LX/0Tn;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->c:LX/0Tn;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->d:LX/0Tn;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->e:LX/0Tn;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->f:LX/0Tn;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->g:LX/0Tn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->h:LX/0Tn;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->j:Ljava/util/Set;

    .line 119644
    const-class v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    const-string v1, "nux_asset_download"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->k:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method private constructor <init>(LX/0Zb;LX/AwR;LX/1Go;LX/0SG;LX/13t;Landroid/content/Context;Ljava/util/concurrent/Executor;LX/03V;Lcom/facebook/http/common/FbHttpRequestProcessor;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tX;LX/0tS;LX/ArM;LX/0fO;LX/0TD;LX/1Gl;LX/1Gk;LX/1Gm;)V
    .locals 12
    .param p2    # LX/AwR;
        .annotation runtime Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxCache;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p15    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 119611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119612
    new-instance v2, LX/Aw0;

    invoke-direct {v2, p0}, LX/Aw0;-><init>(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)V

    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->y:LX/0Vj;

    .line 119613
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->q:LX/0tX;

    .line 119614
    new-instance v2, LX/3AP;

    const-string v5, "inspiration_nux_videos"

    move-object/from16 v3, p6

    move-object/from16 v4, p9

    move-object/from16 v6, p18

    move-object v7, p1

    move-object/from16 v8, p17

    move-object/from16 v9, p16

    move-object v10, p3

    move-object/from16 v11, p5

    invoke-direct/range {v2 .. v11}, LX/3AP;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->w:LX/3AP;

    .line 119615
    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->l:LX/AwR;

    .line 119616
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->m:LX/0SG;

    .line 119617
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->n:Landroid/content/Context;

    .line 119618
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->u:LX/0TD;

    .line 119619
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->o:LX/03V;

    .line 119620
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 119621
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->r:LX/0tS;

    .line 119622
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->t:LX/0fO;

    .line 119623
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->s:LX/ArM;

    .line 119624
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->v:Ljava/util/concurrent/Executor;

    .line 119625
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->b:LX/0Tn;

    const-wide/16 v4, -0x1f4

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 119626
    const-wide/16 v4, -0x1f4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 119627
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->r(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 119628
    :goto_0
    return-void

    .line 119629
    :cond_0
    invoke-virtual/range {p14 .. p14}, LX/0fO;->h()Ljava/lang/String;

    move-result-object v2

    .line 119630
    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->h:LX/0Tn;

    const-string v5, "none"

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 119631
    sget-object v2, LX/0i7;->FETCHED_BUT_STALE:LX/0i7;

    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    goto :goto_0

    .line 119632
    :cond_1
    sget-object v2, LX/0i7;->FETCHED:LX/0i7;

    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    goto :goto_0

    .line 119633
    :cond_2
    sget-object v2, LX/0i7;->NO_DATA:LX/0i7;

    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;
    .locals 3

    .prologue
    .line 119601
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->z:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    if-nez v0, :cond_1

    .line 119602
    const-class v1, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    monitor-enter v1

    .line 119603
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->z:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 119604
    if-eqz v2, :cond_0

    .line 119605
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->b(LX/0QB;)Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->z:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119606
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 119607
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 119608
    :cond_1
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->z:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    return-object v0

    .line 119609
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 119610
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119596
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 119597
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 119598
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->w:LX/3AP;

    new-instance v3, LX/34X;

    new-instance v4, LX/AwT;

    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->l:LX/AwR;

    invoke-direct {v4, v5, v1}, LX/AwT;-><init>(LX/AwR;Ljava/lang/String;)V

    sget-object v1, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v3, v0, v4, v1}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v2, v3}, LX/3AP;->b(LX/34X;)LX/1j2;

    move-result-object v0

    .line 119599
    iget-object v1, v0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1

    .line 119600
    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;Ljava/util/List;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p2    # Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;",
            ">;",
            "Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;",
            "Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;",
            "Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Aw4;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 119583
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 119584
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;

    .line 119585
    invoke-static {p0, v0}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119586
    :cond_0
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 119587
    invoke-static {p0, p2}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 119588
    if-nez p3, :cond_1

    invoke-static {v4}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v1, v0

    .line 119589
    :goto_1
    if-nez p4, :cond_2

    invoke-static {v4}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 119590
    :goto_2
    const/4 v4, 0x4

    new-array v4, v4, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v3, v4, v2

    const/4 v2, 0x2

    aput-object v1, v4, v2

    const/4 v1, 0x3

    aput-object v0, v4, v1

    invoke-static {v4}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 119591
    new-instance v1, LX/Aw2;

    invoke-direct {v1, p0}, LX/Aw2;-><init>(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)V

    .line 119592
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 119593
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 119594
    :cond_1
    invoke-static {p0, p3}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 119595
    :cond_2
    invoke-static {p0, p4}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119581
    new-instance v0, LX/Aw1;

    invoke-direct {v0, p0}, LX/Aw1;-><init>(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 119582
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;
    .locals 20

    .prologue
    .line 119645
    new-instance v1, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/Avw;->a(LX/0QB;)LX/AwR;

    move-result-object v3

    check-cast v3, LX/AwR;

    invoke-static/range {p0 .. p0}, LX/1Gn;->a(LX/0QB;)LX/1Gn;

    move-result-object v4

    check-cast v4, LX/1Go;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v6

    check-cast v6, LX/13t;

    const-class v7, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v10

    check-cast v10, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v12

    check-cast v12, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/0tS;->a(LX/0QB;)LX/0tS;

    move-result-object v13

    check-cast v13, LX/0tS;

    invoke-static/range {p0 .. p0}, LX/ArM;->a(LX/0QB;)LX/ArM;

    move-result-object v14

    check-cast v14, LX/ArM;

    invoke-static/range {p0 .. p0}, LX/0fO;->a(LX/0QB;)LX/0fO;

    move-result-object v15

    check-cast v15, LX/0fO;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v16

    check-cast v16, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v17

    check-cast v17, LX/1Gl;

    invoke-static/range {p0 .. p0}, LX/1Gk;->a(LX/0QB;)LX/1Gk;

    move-result-object v18

    check-cast v18, LX/1Gk;

    invoke-static/range {p0 .. p0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v19

    check-cast v19, LX/1Gm;

    invoke-direct/range {v1 .. v19}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;-><init>(LX/0Zb;LX/AwR;LX/1Go;LX/0SG;LX/13t;Landroid/content/Context;Ljava/util/concurrent/Executor;LX/03V;Lcom/facebook/http/common/FbHttpRequestProcessor;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tX;LX/0tS;LX/ArM;LX/0fO;LX/0TD;LX/1Gl;LX/1Gk;LX/1Gm;)V

    .line 119646
    return-object v1
.end method

.method public static p(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)V
    .locals 5

    .prologue
    .line 119579
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->i:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->i:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 119580
    return-void
.end method

.method public static r(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 119574
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    sget-object v2, LX/0i7;->NO_DATA:LX/0i7;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    sget-object v2, LX/0i7;->FETCHING:LX/0i7;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    sget-object v2, LX/0i7;->FETCH_FAILURE:LX/0i7;

    if-ne v1, v2, :cond_1

    .line 119575
    :cond_0
    :goto_0
    return v0

    .line 119576
    :cond_1
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->m:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->b:LX/0Tn;

    const-wide/16 v6, -0x1f4

    invoke-interface {v1, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {}, LX/Avx;->a()Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getStaleDays()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x5265c00

    mul-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    const/4 v0, 0x1

    .line 119577
    :cond_2
    if-nez v0, :cond_0

    .line 119578
    sget-object v1, LX/0i7;->NO_DATA:LX/0i7;

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v6, -0x1f4

    const/4 v1, 0x0

    .line 119559
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->r(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)Z

    .line 119560
    sget-object v0, LX/Aw3;->a:[I

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    invoke-virtual {v2}, LX/0i7;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 119561
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move-object v0, v1

    .line 119562
    :goto_0
    return-object v0

    .line 119563
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->c:LX/0Tn;

    const/16 v3, -0x1f4

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    .line 119564
    int-to-long v4, v3

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 119565
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->o:LX/03V;

    const-string v2, "InspirationNuxStore#getNuxVideoAssets()"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Somehow we\'re in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " state but don\'t have videos count.\nTime fetched: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->b:LX/0Tn;

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\nAudio file path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->e:LX/0Tn;

    const-string v6, "none"

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\nImage file path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->f:LX/0Tn;

    const-string v6, "none"

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 119566
    goto :goto_0

    .line 119567
    :cond_0
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 119568
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 119569
    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->d:LX/0Tn;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v5, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 119570
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->l:LX/AwR;

    invoke-virtual {v6, v0}, LX/AwR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 119571
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 119572
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 119573
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Landroid/net/Uri;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 119553
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->r(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)Z

    .line 119554
    sget-object v1, LX/Aw3;->a:[I

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    invoke-virtual {v2}, LX/0i7;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 119555
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119556
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->e:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 119557
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->l:LX/AwR;

    invoke-virtual {v2, v0}, LX/AwR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 119558
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    :pswitch_1
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final c()Landroid/net/Uri;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 119543
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->r(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)Z

    .line 119544
    sget-object v1, LX/Aw3;->a:[I

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    invoke-virtual {v2}, LX/0i7;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 119545
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119546
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->g:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 119547
    if-nez v1, :cond_0

    .line 119548
    :goto_0
    :pswitch_1
    return-object v0

    .line 119549
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->l:LX/AwR;

    invoke-virtual {v2, v1}, LX/AwR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 119550
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final clearUserData()V
    .locals 4

    .prologue
    .line 119551
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v1, 0x1

    new-array v1, v1, [LX/0Tn;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a:LX/0Tn;

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    .line 119552
    return-void
.end method
