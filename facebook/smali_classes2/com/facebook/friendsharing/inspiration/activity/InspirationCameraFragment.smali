.class public Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;
.super Lcom/facebook/ui/drawers/DrawerContentFragment;
.source ""


# static fields
.field public static final F:Ljava/lang/String;

.field public static final G:LX/0jK;


# instance fields
.field public volatile A:LX/Atb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile B:LX/0i4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile C:LX/ArU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile D:LX/ArN;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public E:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:LX/0kL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private I:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Av8;",
            ">;"
        }
    .end annotation
.end field

.field public J:LX/0fO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public K:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public L:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public M:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private N:LX/AFQ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private O:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Yi;",
            ">;"
        }
    .end annotation
.end field

.field private P:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private Q:LX/ArV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private R:LX/192;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public S:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public T:LX/ArS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private U:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/87T;",
            ">;"
        }
    .end annotation
.end field

.field public V:LX/0iJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0iJ",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            "LX/0ij;",
            ">;"
        }
    .end annotation
.end field

.field public W:LX/AtM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AtM",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            "LX/0ij;",
            ">;"
        }
    .end annotation
.end field

.field public X:LX/AsB;

.field public Y:LX/Ass;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Ass",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            "LX/0ij;",
            ">;"
        }
    .end annotation
.end field

.field public Z:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            "LX/0ij;",
            ">;"
        }
    .end annotation
.end field

.field public volatile a:LX/Aux;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final aA:LX/Ar3;

.field private final aB:LX/Ar4;

.field public final aC:LX/Ar5;

.field private aD:I

.field public aa:LX/Auw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Auw",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            "LX/0ij;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:LX/Aud;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Aud",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            "LX/0ij;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:LX/ArT;

.field public ad:Landroid/widget/FrameLayout;

.field public ae:Landroid/view/View;

.field public af:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

.field public ag:LX/Ata;

.field public ah:LX/HvN;

.field public ai:LX/0ij;

.field public final aj:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0iK",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            ">;>;"
        }
    .end annotation
.end field

.field public ak:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field private al:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public am:LX/0iO;

.field public an:LX/Arh;

.field private ao:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

.field public ap:Z

.field public aq:Z

.field public ar:Z

.field public as:LX/ArL;

.field public at:J

.field public au:Z

.field public av:Z

.field public aw:Z

.field public ax:I

.field public final ay:LX/Ar1;

.field public final az:LX/Ar2;

.field public volatile b:LX/Aue;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile c:LX/Atz;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile d:LX/AuM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile e:LX/Ate;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/AJX;",
            ">;"
        }
    .end annotation
.end field

.field public volatile g:LX/0Or;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public volatile h:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/AtS;",
            ">;"
        }
    .end annotation
.end field

.field public volatile i:LX/AtN;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile j:LX/AsJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile k:LX/AsC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile l:LX/AsS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile m:LX/Ari;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile n:LX/Ati;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile o:LX/Ast;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile p:LX/AuG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile q:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/87V;",
            ">;"
        }
    .end annotation
.end field

.field public volatile r:LX/AuU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile s:LX/Avt;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile t:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public volatile u:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field public volatile v:LX/AR2;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile w:LX/ARD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile x:LX/AR4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile y:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ARX;",
            ">;"
        }
    .end annotation
.end field

.field public volatile z:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ki;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107604
    const-class v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->F:Ljava/lang/String;

    .line 107605
    const-class v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107490
    invoke-direct {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;-><init>()V

    .line 107491
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 107492
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->O:LX/0Ot;

    .line 107493
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 107494
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->U:LX/0Ot;

    .line 107495
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aj:Ljava/util/List;

    .line 107496
    iput-boolean v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ar:Z

    .line 107497
    iput-boolean v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->av:Z

    .line 107498
    iput-boolean v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aw:Z

    .line 107499
    new-instance v0, LX/Ar1;

    invoke-direct {v0, p0}, LX/Ar1;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ay:LX/Ar1;

    .line 107500
    new-instance v0, LX/Ar2;

    invoke-direct {v0, p0}, LX/Ar2;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->az:LX/Ar2;

    .line 107501
    new-instance v0, LX/Ar3;

    invoke-direct {v0, p0}, LX/Ar3;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aA:LX/Ar3;

    .line 107502
    new-instance v0, LX/Ar4;

    invoke-direct {v0, p0}, LX/Ar4;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aB:LX/Ar4;

    .line 107503
    new-instance v0, LX/Ar5;

    invoke-direct {v0, p0}, LX/Ar5;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aC:LX/Ar5;

    .line 107504
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aD:I

    return-void
.end method

.method public static B(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 107505
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->D(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    .line 107506
    sget-object v1, LX/875;->UNKNOWN:LX/875;

    invoke-static {p0, v2, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)V

    .line 107507
    if-eqz v0, :cond_0

    .line 107508
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v1, LX/ArJ;->TAP_BACK_TO_CAMERA_BUTTON:LX/ArJ;

    invoke-virtual {v0, v1}, LX/ArT;->q(LX/ArJ;)V

    .line 107509
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v1, LX/ArJ;->TAP_BACK_TO_CAMERA_BUTTON:LX/ArJ;

    invoke-virtual {v0, v1}, LX/ArT;->e(LX/ArJ;)V

    .line 107510
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v1, LX/ArJ;->TAP_BACK_TO_CAMERA_BUTTON:LX/ArJ;

    invoke-virtual {v0, v1}, LX/ArT;->b(LX/ArJ;)V

    .line 107511
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v1, LX/ArJ;->TAP_BACK_TO_CAMERA_BUTTON:LX/ArJ;

    invoke-virtual {v0, v1}, LX/ArT;->p(LX/ArJ;)V

    .line 107512
    const/4 v2, 0x0

    .line 107513
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->c()LX/0jJ;

    move-result-object v0

    const-class v1, LX/AuF;

    invoke-static {v1}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsMirrorOn(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsMuted(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 107514
    const/4 v0, 0x1

    .line 107515
    :goto_0
    return v0

    .line 107516
    :cond_0
    invoke-direct {p0, v2}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->b(LX/ArJ;)V

    .line 107517
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static D(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z
    .locals 1

    .prologue
    .line 107518
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static K(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V
    .locals 14

    .prologue
    .line 107519
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->i:LX/AtN;

    .line 107520
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/AtN;->a(Ljava/lang/Object;LX/ArL;LX/ArT;LX/Ata;)LX/AtM;

    move-result-object v0

    .line 107521
    invoke-static {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107522
    move-object v0, v0

    .line 107523
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    .line 107524
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v1, 0x7f0d1787

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 107525
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aA:LX/Ar3;

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aB:LX/Ar4;

    const/4 v7, 0x0

    .line 107526
    const v4, 0x7f030933

    invoke-virtual {v0, v4}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 107527
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v4

    iput-object v4, v1, LX/AtM;->o:Landroid/view/View;

    .line 107528
    iget-object v4, v1, LX/AtM;->G:LX/0fO;

    invoke-virtual {v4}, LX/0fO;->k()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v1, LX/AtM;->J:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->d()I

    move-result v4

    iget-object v5, v1, LX/AtM;->J:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->e()I

    move-result v5

    if-eq v4, v5, :cond_0

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v4, v5, :cond_0

    .line 107529
    iget-object v4, v1, LX/AtM;->o:Landroid/view/View;

    iget-object v5, v1, LX/AtM;->J:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->e()I

    move-result v5

    iget-object v6, v1, LX/AtM;->J:LX/0hB;

    invoke-virtual {v6}, LX/0hB;->d()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4, v7, v7, v7, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 107530
    :cond_0
    iget-object v4, v1, LX/AtM;->o:Landroid/view/View;

    const v5, 0x7f0d1787

    invoke-static {v4, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v4, v1, LX/AtM;->p:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 107531
    iget-object v4, v1, LX/AtM;->o:Landroid/view/View;

    const v5, 0x7f0d1785

    invoke-static {v4, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v1, LX/AtM;->u:Landroid/view/View;

    .line 107532
    iget-object v4, v1, LX/AtM;->o:Landroid/view/View;

    const v5, 0x7f0d1788

    invoke-static {v4, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v4, v1, LX/AtM;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 107533
    iget-object v4, v1, LX/AtM;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v5, 0x7f021553

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 107534
    new-instance v4, LX/AtE;

    invoke-direct {v4, v1}, LX/AtE;-><init>(LX/AtM;)V

    iput-object v4, v1, LX/AtM;->N:Landroid/view/View$OnClickListener;

    .line 107535
    const-wide/16 v12, 0x0

    .line 107536
    iget-object v8, v1, LX/AtM;->g:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b1aa4

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 107537
    iget-object v9, v1, LX/AtM;->L:LX/0wW;

    invoke-virtual {v9}, LX/0wW;->a()LX/0wd;

    move-result-object v9

    invoke-static {}, LX/87U;->a()LX/0wT;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v9

    invoke-virtual {v9, v12, v13}, LX/0wd;->a(D)LX/0wd;

    move-result-object v9

    invoke-virtual {v9, v12, v13}, LX/0wd;->b(D)LX/0wd;

    move-result-object v9

    invoke-virtual {v9}, LX/0wd;->j()LX/0wd;

    move-result-object v9

    new-instance v10, LX/AtB;

    invoke-direct {v10, v1, v8}, LX/AtB;-><init>(LX/AtM;I)V

    invoke-virtual {v9, v10}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v8

    iput-object v8, v1, LX/AtM;->K:LX/0wd;

    .line 107538
    iput-object v2, v1, LX/AtM;->y:LX/Ar3;

    .line 107539
    iput-object v3, v1, LX/AtM;->z:LX/Ar4;

    .line 107540
    const/16 v7, 0x8

    .line 107541
    iget-object v4, v1, LX/AtM;->o:Landroid/view/View;

    const v5, 0x7f0d178a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 107542
    const v5, 0x7f030935

    invoke-virtual {v4, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 107543
    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v5

    .line 107544
    const v4, 0x7f0d1790

    invoke-static {v5, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 107545
    const v4, 0x7f0d1791

    invoke-static {v5, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v4, v1, LX/AtM;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 107546
    const v4, 0x7f0d1793

    invoke-static {v5, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v4, v1, LX/AtM;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 107547
    iget-object v4, v1, LX/AtM;->o:Landroid/view/View;

    const v6, 0x7f0d178b

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 107548
    const v6, 0x7f030934

    invoke-virtual {v4, v6}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 107549
    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v6

    .line 107550
    const v4, 0x7f0d1795

    invoke-static {v6, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 107551
    const v4, 0x7f0d1791

    invoke-static {v6, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v4, v1, LX/AtM;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 107552
    const v4, 0x7f0d1793

    invoke-static {v6, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v4, v1, LX/AtM;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 107553
    const v4, 0x7f0d1792

    invoke-static {v6, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/CustomFrameLayout;

    iput-object v4, v1, LX/AtM;->x:Lcom/facebook/widget/CustomFrameLayout;

    .line 107554
    new-instance v7, Landroid/animation/LayoutTransition;

    invoke-direct {v7}, Landroid/animation/LayoutTransition;-><init>()V

    .line 107555
    const/4 v4, 0x0

    invoke-virtual {v7, v4}, Landroid/animation/LayoutTransition;->setAnimateParentHierarchy(Z)V

    move-object v4, v5

    .line 107556
    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    move-object v4, v6

    .line 107557
    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 107558
    invoke-static {v1}, LX/AtM;->g(LX/AtM;)V

    .line 107559
    iget-object v4, v1, LX/AtM;->g:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x10e0000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    iput v4, v1, LX/AtM;->M:I

    .line 107560
    invoke-virtual {v1}, LX/AtM;->b()V

    .line 107561
    iget-object v4, v1, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    invoke-static {v4}, LX/87N;->a(LX/0il;)LX/86o;

    move-result-object v4

    sget-object v5, LX/86o;->NONE:LX/86o;

    if-ne v4, v5, :cond_1

    .line 107562
    invoke-static {v1}, LX/AtM;->k(LX/AtM;)V

    .line 107563
    :goto_0
    iget-object v4, v1, LX/AtM;->o:Landroid/view/View;

    move-object v0, v4

    .line 107564
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v2, 0x7f0d0864

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 107565
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->o:LX/Ast;

    .line 107566
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->az:LX/Ar2;

    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    invoke-virtual {v2, v3, v4, v1, v5}, LX/Ast;->a(Ljava/lang/Object;LX/Ar2;Landroid/view/ViewStub;LX/Ata;)LX/Ass;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Y:LX/Ass;

    .line 107567
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Y:LX/Ass;

    invoke-static {p0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107568
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v2, 0x7f0d179b

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 107569
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 107570
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->e:LX/Ate;

    .line 107571
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v3, 0x7f0d17c5

    invoke-static {v1, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 107572
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    .line 107573
    new-instance v6, LX/Atd;

    check-cast v3, LX/0il;

    invoke-static {v2}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->a(LX/0QB;)Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    move-result-object v5

    check-cast v5, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    invoke-direct {v6, v3, v1, v4, v5}, LX/Atd;-><init>(LX/0il;Landroid/view/ViewStub;LX/0hB;Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;)V

    .line 107574
    invoke-static {v2}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    .line 107575
    iput-object v4, v6, LX/Atd;->b:LX/0Sh;

    .line 107576
    move-object v1, v6

    .line 107577
    invoke-static {p0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107578
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->d:LX/AuM;

    .line 107579
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v3, 0x7f0d17c6

    invoke-static {v1, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 107580
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    .line 107581
    new-instance v5, LX/AuL;

    check-cast v3, LX/0il;

    const-class v4, Landroid/content/Context;

    invoke-interface {v2, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {v5, v3, v1, v4}, LX/AuL;-><init>(LX/0il;Landroid/view/ViewStub;Landroid/content/Context;)V

    .line 107582
    move-object v1, v5

    .line 107583
    invoke-static {p0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107584
    const v4, 0x7f0d178d

    invoke-static {v0, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Landroid/view/ViewStub;

    .line 107585
    const v4, 0x7f0d178e

    invoke-static {v0, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewStub;

    .line 107586
    const v4, 0x7f0d178f

    invoke-static {v0, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Landroid/view/ViewStub;

    .line 107587
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->l:LX/AsS;

    .line 107588
    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->af:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v6, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    iget-object v8, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    invoke-virtual/range {v4 .. v9}, LX/AsS;->a(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;LX/ArT;LX/ArL;Landroid/view/ViewStub;)LX/AsR;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107589
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->k:LX/AsC;

    .line 107590
    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->af:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v6, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    iget-object v8, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    move-object v9, v10

    invoke-virtual/range {v4 .. v9}, LX/AsC;->a(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;LX/ArT;LX/ArL;Landroid/view/ViewStub;)LX/AsB;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->X:LX/AsB;

    invoke-static {p0, v4}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107591
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->X:LX/AsB;

    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    .line 107592
    iput-object v5, v4, LX/AsB;->q:LX/As8;

    .line 107593
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->j:LX/AsJ;

    .line 107594
    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->af:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v6, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v4, v5, v6, v11}, LX/AsJ;->a(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;Landroid/view/ViewStub;)LX/AsI;

    move-result-object v4

    .line 107595
    invoke-static {p0, v4}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107596
    return-void

    .line 107597
    :cond_1
    invoke-static {v1}, LX/AtM;->j(LX/AtM;)V

    goto/16 :goto_0
.end method

.method private L()V
    .locals 4

    .prologue
    .line 107598
    new-instance v1, LX/Asx;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/0f9;

    invoke-interface {v0}, LX/0f9;->mo_()LX/0iH;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->af:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-direct {v1, v0, v2, v3}, LX/Asx;-><init>(LX/0iH;Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0il;)V

    invoke-static {p0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107599
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Z:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    new-instance v1, LX/Ar0;

    invoke-direct {v1, p0}, LX/Ar0;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    .line 107600
    iput-object v1, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->C:LX/Ar0;

    .line 107601
    iget-object p0, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    if-eqz p0, :cond_0

    .line 107602
    iget-object p0, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    invoke-virtual {p0, v1}, LX/9d5;->a(LX/Ar0;)V

    .line 107603
    :cond_0
    return-void
.end method

.method public static P(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V
    .locals 13

    .prologue
    const/4 v4, 0x0

    .line 107606
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    .line 107607
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 107608
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v2}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v2

    .line 107609
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->T:LX/ArS;

    .line 107610
    invoke-static {v0}, LX/7kq;->j(LX/0Px;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 107611
    const v6, 0xb60007

    .line 107612
    invoke-static {v3, v6}, LX/ArS;->d(LX/ArS;I)V

    .line 107613
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Px;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    .line 107614
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v5

    .line 107615
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 107616
    sget-object v8, LX/ArO;->SESSION_ID:LX/ArO;

    invoke-virtual {v8}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107617
    invoke-static {v7, v2}, LX/ArS;->a(Ljava/util/Map;Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)V

    .line 107618
    sget-object v8, LX/ArO;->WIDTH:LX/ArO;

    invoke-virtual {v8}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v8

    .line 107619
    iget-object v9, v5, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v9, v9

    .line 107620
    invoke-static {v9}, LX/1kH;->c(Landroid/net/Uri;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107621
    sget-object v8, LX/ArO;->HEIGHT:LX/ArO;

    invoke-virtual {v8}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v8

    .line 107622
    iget-object v9, v5, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v9, v9

    .line 107623
    invoke-static {v9}, LX/1kH;->b(Landroid/net/Uri;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107624
    iget-object v8, v5, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v5, v8

    .line 107625
    invoke-static {v5}, LX/1kH;->a(Landroid/net/Uri;)I

    move-result v5

    .line 107626
    sget-object v8, LX/ArO;->DURATION:LX/ArO;

    invoke-virtual {v8}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v8

    int-to-float v5, v5

    const/high16 v9, 0x447a0000    # 1000.0f

    div-float/2addr v5, v9

    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v7, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107627
    move-object v5, v7

    .line 107628
    invoke-static {v3, v6, v5}, LX/ArS;->a(LX/ArS;ILjava/util/Map;)V

    .line 107629
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f082789

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f08278a

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v4, v4}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v2

    .line 107630
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/87T;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/87T;->a(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getMediaSource()LX/875;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)V

    .line 107631
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AtS;

    .line 107632
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    .line 107633
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    .line 107634
    invoke-static {v3}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 107635
    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v6

    .line 107636
    iget-object v7, v0, LX/AtS;->b:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/Context;

    .line 107637
    iget-object v7, v0, LX/AtS;->g:LX/BTh;

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v8

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v7 .. v12}, LX/BTh;->a(Landroid/net/Uri;JILandroid/content/Context;)Landroid/net/Uri;

    move-result-object v7

    .line 107638
    if-nez v7, :cond_2

    .line 107639
    const/4 v7, 0x0

    invoke-static {v7}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 107640
    :goto_1
    move-object v5, v7

    .line 107641
    :goto_2
    move-object v0, v5

    .line 107642
    new-instance v3, LX/Aqy;

    invoke-direct {v3, p0, v2}, LX/Aqy;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/4BY;)V

    invoke-static {v0, v3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 107643
    return-void

    .line 107644
    :cond_0
    const v6, 0xb60008

    .line 107645
    invoke-static {v3, v6}, LX/ArS;->d(LX/ArS;I)V

    .line 107646
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Px;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    .line 107647
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v5

    .line 107648
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 107649
    sget-object v8, LX/ArO;->SESSION_ID:LX/ArO;

    invoke-virtual {v8}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107650
    invoke-static {v7, v2}, LX/ArS;->a(Ljava/util/Map;Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)V

    .line 107651
    sget-object v8, LX/ArO;->WIDTH:LX/ArO;

    invoke-virtual {v8}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v8

    .line 107652
    iget v9, v5, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v9, v9

    .line 107653
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107654
    sget-object v8, LX/ArO;->HEIGHT:LX/ArO;

    invoke-virtual {v8}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v8

    .line 107655
    iget v9, v5, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v5, v9

    .line 107656
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v7, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107657
    move-object v5, v7

    .line 107658
    invoke-static {v3, v6, v5}, LX/ArS;->a(LX/ArS;ILjava/util/Map;)V

    .line 107659
    goto/16 :goto_0

    :cond_1
    const/4 v5, 0x1

    invoke-static {v0, v3, v5}, LX/AtS;->a(LX/AtS;Lcom/facebook/composer/attachments/ComposerAttachment;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto :goto_2

    :cond_2
    const/4 v8, 0x1

    invoke-static {v0, v7, v6, v8}, LX/AtS;->a(LX/AtS;Landroid/net/Uri;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    goto :goto_1
.end method

.method private T()Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 6

    .prologue
    .line 107660
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-static {v0}, LX/87Q;->a(LX/0is;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v4

    .line 107661
    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getPromptType()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v3}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getTrackingString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/32e;->c:LX/32e;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v0

    return-object v0
.end method

.method public static W(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V
    .locals 4

    .prologue
    .line 107662
    const/4 v0, 0x0

    sget-object v1, LX/875;->UNKNOWN:LX/875;

    invoke-static {p0, v0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)V

    .line 107663
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ae:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 107664
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab:LX/Aud;

    if-eqz v0, :cond_0

    .line 107665
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab:LX/Aud;

    .line 107666
    iget-object v1, v0, LX/Aud;->g:LX/Auh;

    const/4 v3, 0x0

    .line 107667
    iget-object v2, v1, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v2}, Lcom/facebook/drawingview/DrawingView;->a()V

    .line 107668
    sget-object v2, LX/86w;->HIDDEN:LX/86w;

    invoke-virtual {v1, v2}, LX/Auh;->setDrawingMode(LX/86w;)V

    .line 107669
    const/4 v2, 0x0

    iput v2, v1, LX/Auh;->f:F

    .line 107670
    iput v3, v1, LX/Auh;->g:I

    .line 107671
    iput v3, v1, LX/Auh;->h:I

    .line 107672
    iput v3, v1, LX/Auh;->i:I

    .line 107673
    iput v3, v1, LX/Auh;->j:I

    .line 107674
    const/4 v1, 0x0

    iput-object v1, v0, LX/Aud;->h:Lcom/facebook/photos/creativeediting/model/DoodleParams;

    .line 107675
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa:LX/Auw;

    if-eqz v0, :cond_1

    .line 107676
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa:LX/Auw;

    .line 107677
    sget-object v1, LX/870;->NO_FORMAT_IN_PROCESS:LX/870;

    invoke-static {v0, v1}, LX/Auw;->b(LX/Auw;LX/870;)V

    .line 107678
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Auw;->i:Z

    .line 107679
    iget-object v1, v0, LX/Auw;->f:LX/Av1;

    invoke-virtual {v1}, LX/Av1;->a()V

    .line 107680
    iget-object v1, v0, LX/Auw;->g:LX/Aur;

    invoke-virtual {v1}, LX/Aur;->e()V

    .line 107681
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v1

    invoke-static {v0, v1}, LX/Auw;->a(LX/Auw;Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V

    .line 107682
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Y:LX/Ass;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    if-eqz v0, :cond_2

    .line 107683
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    .line 107684
    iget-object v1, v0, LX/AtM;->u:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 107685
    sget-object v1, LX/87O;->CAPTURE:LX/87O;

    invoke-static {v0, v1}, LX/AtM;->a(LX/AtM;LX/87O;)V

    .line 107686
    sget-object v1, LX/87O;->CAPTURE:LX/87O;

    iput-object v1, v0, LX/AtM;->H:LX/87O;

    .line 107687
    invoke-virtual {v0}, LX/AtM;->b()V

    .line 107688
    :cond_2
    return-void
.end method

.method public static X(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z
    .locals 1

    .prologue
    .line 107689
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    invoke-interface {v0}, LX/0iO;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static Z(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V
    .locals 2

    .prologue
    .line 107863
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->X(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107864
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->O:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yi;

    invoke-virtual {v0}, LX/0Yi;->q()V

    .line 107865
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xa10007

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 107866
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xa10005

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 107867
    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 107690
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 107691
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "camera_lib_type"

    sget-object v2, LX/6KU;->CAMERA_CORE:LX/6KU;

    invoke-virtual {v2}, LX/6KU;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 107692
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "product_name"

    sget-object v2, LX/6KV;->PURPLE_RAIN:LX/6KV;

    invoke-virtual {v2}, LX/6KV;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 107693
    return-void
.end method

.method private a(LX/ArJ;)V
    .locals 14
    .param p1    # LX/ArJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v4, 0xa10004

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 107788
    invoke-direct {p0, v4}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(I)V

    .line 107789
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    sget-object v1, LX/5L2;->ON_RESUME:LX/5L2;

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/5L2;)V

    .line 107790
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->L:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->at:J

    .line 107791
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->u()V

    .line 107792
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    .line 107793
    iget-boolean v1, v0, LX/Ata;->d:Z

    move v0, v1

    .line 107794
    if-nez v0, :cond_0

    .line 107795
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->E:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    .line 107796
    iget-object v5, v0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0il;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v5}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v5

    iget-object v6, v0, LX/ArT;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setInspirationSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v6

    iget-object v5, v0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0il;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v5}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setHasChangedInspiration(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v5

    .line 107797
    iget-object v7, v0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0il;

    check-cast v7, LX/0im;

    invoke-interface {v7}, LX/0im;->c()LX/0jJ;

    move-result-object v7

    const-class v8, LX/ArT;

    invoke-static {v8}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v7

    check-cast v7, LX/0jL;

    check-cast v7, LX/0jL;

    invoke-virtual {v7, v6}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0jL;

    check-cast v7, LX/0jL;

    invoke-virtual {v7, v5}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0jL;

    check-cast v7, LX/0jL;

    invoke-virtual {v7}, LX/0jL;->a()V

    .line 107798
    iget-object v5, v0, LX/ArT;->b:LX/ArL;

    .line 107799
    sget-object v6, LX/ArH;->START_INSPIRATION_SESSION:LX/ArH;

    invoke-static {v5, v6, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 107800
    invoke-static {v6, v1}, LX/ArL;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 107801
    invoke-static {v5, v6}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 107802
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    .line 107803
    iget-boolean v5, v1, LX/Ata;->d:Z

    move v1, v5

    .line 107804
    const/4 v6, -0x1

    const/4 v8, 0x1

    .line 107805
    iget-boolean v5, v0, LX/0iJ;->r:Z

    if-eqz v5, :cond_10

    .line 107806
    iget-object v5, v0, LX/0iJ;->q:LX/0iB;

    if-eqz v5, :cond_1

    .line 107807
    iget-object v5, v0, LX/0iJ;->q:LX/0iB;

    .line 107808
    iget-object v7, v5, LX/0iB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    sget-object v9, LX/AwE;->c:LX/0Tn;

    const/4 v10, 0x1

    invoke-interface {v7, v9, v10}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v7

    invoke-interface {v7}, LX/0hN;->commit()V

    .line 107809
    iget-object v5, v0, LX/0iJ;->l:LX/0iA;

    invoke-virtual {v5}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v5

    const-string v7, "4442"

    invoke-virtual {v5, v7}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 107810
    :cond_1
    if-nez v1, :cond_3

    .line 107811
    iget-object v5, v0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    .line 107812
    iget v7, v5, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move v7, v7

    .line 107813
    if-eqz p1, :cond_2

    .line 107814
    invoke-static {v0}, LX/0iJ;->o(LX/0iJ;)V

    .line 107815
    iget-object v9, v0, LX/0iJ;->h:LX/ArT;

    iget v5, v0, LX/0iJ;->v:I

    if-ne v5, v8, :cond_e

    move v5, v6

    .line 107816
    :goto_0
    iget-object v10, v9, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v10}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0il;

    invoke-interface {v10}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v10}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v10

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setNuxSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v10

    iget-object v11, v9, LX/ArT;->a:LX/0SG;

    invoke-interface {v11}, LX/0SG;->a()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setNuxSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v10

    invoke-static {v9, v10}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 107817
    iget-object v10, v9, LX/ArT;->b:LX/ArL;

    .line 107818
    sget-object v11, LX/ArH;->START_NUX_SESSION:LX/ArH;

    invoke-static {v10, v11, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v11

    invoke-static {v10}, LX/ArL;->h(LX/ArL;)Ljava/util/Map;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v11

    sget-object v12, LX/ArI;->INDEX:LX/ArI;

    invoke-virtual {v12}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v11

    invoke-static {v10, v11}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 107819
    :cond_2
    iget-object v5, v0, LX/0iJ;->g:LX/ArL;

    iget v9, v0, LX/0iJ;->v:I

    if-ne v9, v8, :cond_f

    :goto_1
    invoke-virtual {v5, v6}, LX/ArL;->a(I)V

    .line 107820
    :cond_3
    invoke-static {v0}, LX/0iJ;->m(LX/0iJ;)V

    .line 107821
    iget-object v5, v0, LX/0iJ;->k:LX/Avn;

    invoke-virtual {v5}, LX/Avn;->c()V

    .line 107822
    iget-object v5, v0, LX/0iJ;->k:LX/Avn;

    .line 107823
    iget-object v6, v5, LX/Avn;->h:LX/0Yd;

    if-nez v6, :cond_4

    .line 107824
    new-instance v6, LX/0Yd;

    const-string v7, "android.media.RINGER_MODE_CHANGED"

    new-instance v9, LX/Avm;

    invoke-direct {v9, v5}, LX/Avm;-><init>(LX/Avn;)V

    invoke-direct {v6, v7, v9}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    iput-object v6, v5, LX/Avn;->h:LX/0Yd;

    .line 107825
    :cond_4
    iget-object v6, v5, LX/Avn;->a:Landroid/content/Context;

    iget-object v7, v5, LX/Avn;->h:LX/0Yd;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.media.RINGER_MODE_CHANGED"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 107826
    const/4 v6, 0x1

    iput-boolean v6, v5, LX/Avn;->i:Z

    .line 107827
    iget-boolean v5, v0, LX/0iJ;->r:Z

    invoke-static {v0, v5}, LX/0iJ;->a(LX/0iJ;Z)V

    move v5, v8

    .line 107828
    :goto_2
    move v0, v5

    .line 107829
    if-eqz v0, :cond_5

    .line 107830
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    .line 107831
    iput-boolean v2, v0, LX/Ata;->d:Z

    .line 107832
    :goto_3
    return-void

    .line 107833
    :cond_5
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->D(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 107834
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 107835
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 107836
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_a

    .line 107837
    const/4 v0, 0x0

    sget-object v1, LX/875;->UNKNOWN:LX/875;

    invoke-static {p0, v0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)V

    .line 107838
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->X:LX/AsB;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    sget-object v1, LX/AtZ;->STORAGE_PERMISSION:LX/AtZ;

    invoke-virtual {v0, v1}, LX/Ata;->a(LX/AtZ;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 107839
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->X:LX/AsB;

    invoke-virtual {v0}, LX/AsB;->a()V

    .line 107840
    :cond_7
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0it;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 107841
    :cond_8
    invoke-static {p0, p1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->c$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/ArJ;)V

    .line 107842
    :cond_9
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 107843
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xa10005

    invoke-interface {v0, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 107844
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    .line 107845
    iput-boolean v2, v0, LX/Ata;->d:Z

    .line 107846
    goto :goto_3

    .line 107847
    :cond_a
    invoke-static {p0, p1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->d(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/ArJ;)V

    goto :goto_4

    .line 107848
    :cond_b
    if-eqz p1, :cond_6

    .line 107849
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    .line 107850
    iget-boolean v1, v0, LX/Ata;->d:Z

    move v0, v1

    .line 107851
    if-nez v0, :cond_c

    .line 107852
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->E:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    invoke-virtual {v0, p1, v1}, LX/ArT;->b(LX/ArJ;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)V

    .line 107853
    :cond_c
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    .line 107854
    const-string v1, "1752514608329267"

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 107855
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aw:Z

    goto :goto_4

    .line 107856
    :cond_d
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    .line 107857
    iget-boolean v1, v0, LX/Ata;->d:Z

    move v0, v1

    .line 107858
    if-nez v0, :cond_6

    .line 107859
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->E:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    invoke-virtual {v0, p1, v1}, LX/ArT;->c(LX/ArJ;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)V

    goto :goto_4

    :cond_e
    move v5, v7

    .line 107860
    goto/16 :goto_0

    :cond_f
    move v6, v7

    .line 107861
    goto/16 :goto_1

    .line 107862
    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_2
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0iK",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107785
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aj:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107786
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-virtual {v0, p1}, LX/HvN;->a(LX/0iK;)V

    .line 107787
    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/Aux;LX/Aue;LX/Atz;LX/AuM;LX/Ate;LX/0Or;LX/0Or;LX/0Or;LX/AtN;LX/AsJ;LX/AsC;LX/AsS;LX/Ari;LX/Ati;LX/Ast;LX/AuG;LX/0Or;LX/AuU;LX/Avt;LX/0Or;LX/0Or;LX/AR2;LX/ARD;LX/AR4;LX/0Or;LX/0Or;LX/Atb;LX/0i4;LX/ArU;LX/ArN;LX/0kL;LX/0Or;LX/0fO;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/03V;LX/AFQ;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/ArV;LX/192;LX/0Sh;LX/ArS;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;",
            "LX/Aux;",
            "LX/Aue;",
            "LX/Atz;",
            "LX/AuM;",
            "LX/Ate;",
            "LX/0Or",
            "<",
            "LX/AJX;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Or",
            "<",
            "LX/AtS;",
            ">;",
            "LX/AtN;",
            "LX/AsJ;",
            "LX/AsC;",
            "LX/AsS;",
            "LX/Ari;",
            "LX/Ati;",
            "LX/Ast;",
            "LX/AuG;",
            "LX/0Or",
            "<",
            "LX/87V;",
            ">;",
            "LX/AuU;",
            "LX/Avt;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/AR2;",
            "LX/ARD;",
            "LX/AR4;",
            "LX/0Or",
            "<",
            "LX/ARX;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1Ki;",
            ">;",
            "LX/Atb;",
            "LX/0i4;",
            "LX/ArU;",
            "LX/ArN;",
            "LX/0kL;",
            "LX/0Or",
            "<",
            "LX/Av8;",
            ">;",
            "LX/0fO;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/AFQ;",
            "LX/0Ot",
            "<",
            "LX/0Yi;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/ArV;",
            "LX/192;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/ArS;",
            "LX/0Ot",
            "<",
            "LX/87T;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107784
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a:LX/Aux;

    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->b:LX/Aue;

    iput-object p3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->c:LX/Atz;

    iput-object p4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->d:LX/AuM;

    iput-object p5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->e:LX/Ate;

    iput-object p6, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->f:LX/0Or;

    iput-object p7, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->g:LX/0Or;

    iput-object p8, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->h:LX/0Or;

    iput-object p9, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->i:LX/AtN;

    iput-object p10, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->j:LX/AsJ;

    iput-object p11, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->k:LX/AsC;

    iput-object p12, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->l:LX/AsS;

    iput-object p13, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->m:LX/Ari;

    iput-object p14, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->n:LX/Ati;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->o:LX/Ast;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->p:LX/AuG;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->q:LX/0Or;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->r:LX/AuU;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->s:LX/Avt;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->t:LX/0Or;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->u:LX/0Or;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->v:LX/AR2;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->w:LX/ARD;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->x:LX/AR4;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->y:LX/0Or;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->z:LX/0Or;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->A:LX/Atb;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->B:LX/0i4;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->C:LX/ArU;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->D:LX/ArN;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->H:LX/0kL;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->I:LX/0Or;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->J:LX/0fO;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->K:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->L:LX/0SG;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->M:LX/03V;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->N:LX/AFQ;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->O:LX/0Ot;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Q:LX/ArV;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->R:LX/192;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->S:LX/0Sh;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->T:LX/ArS;

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->U:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)V
    .locals 2
    .param p0    # Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 107780
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 107781
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-static {v0, v1, p1, p2}, LX/2rf;->a(LX/0jL;LX/0iq;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)LX/0jL;

    .line 107782
    invoke-virtual {v0}, LX/0jL;->a()V

    .line 107783
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 48

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v46

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    const-class v3, LX/Aux;

    move-object/from16 v0, v46

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Aux;

    const-class v4, LX/Aue;

    move-object/from16 v0, v46

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Aue;

    const-class v5, LX/Atz;

    move-object/from16 v0, v46

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Atz;

    const-class v6, LX/AuM;

    move-object/from16 v0, v46

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/AuM;

    const-class v7, LX/Ate;

    move-object/from16 v0, v46

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Ate;

    const/16 v8, 0x17bd

    move-object/from16 v0, v46

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x1430

    move-object/from16 v0, v46

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x228e

    move-object/from16 v0, v46

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const-class v11, LX/AtN;

    move-object/from16 v0, v46

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/AtN;

    const-class v12, LX/AsJ;

    move-object/from16 v0, v46

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/AsJ;

    const-class v13, LX/AsC;

    move-object/from16 v0, v46

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/AsC;

    const-class v14, LX/AsS;

    move-object/from16 v0, v46

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/AsS;

    const-class v15, LX/Ari;

    move-object/from16 v0, v46

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/Ari;

    const-class v16, LX/Ati;

    move-object/from16 v0, v46

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/Ati;

    const-class v17, LX/Ast;

    move-object/from16 v0, v46

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/Ast;

    const-class v18, LX/AuG;

    move-object/from16 v0, v46

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/AuG;

    const/16 v19, 0x22ae

    move-object/from16 v0, v46

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    const-class v20, LX/AuU;

    move-object/from16 v0, v46

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/AuU;

    const-class v21, LX/Avt;

    move-object/from16 v0, v46

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/Avt;

    const/16 v22, 0x122d

    move-object/from16 v0, v46

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    const/16 v23, 0x19c6

    move-object/from16 v0, v46

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v23

    const-class v24, LX/AR2;

    move-object/from16 v0, v46

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/AR2;

    const-class v25, LX/ARD;

    move-object/from16 v0, v46

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/ARD;

    const-class v26, LX/AR4;

    move-object/from16 v0, v46

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/AR4;

    const/16 v27, 0x19d8

    move-object/from16 v0, v46

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v27

    const/16 v28, 0x3d9

    move-object/from16 v0, v46

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v28

    const-class v29, LX/Atb;

    move-object/from16 v0, v46

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v29

    check-cast v29, LX/Atb;

    const-class v30, LX/0i4;

    move-object/from16 v0, v46

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v30

    check-cast v30, LX/0i4;

    const-class v31, LX/ArU;

    move-object/from16 v0, v46

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v31

    check-cast v31, LX/ArU;

    const-class v32, LX/ArN;

    move-object/from16 v0, v46

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v32

    check-cast v32, LX/ArN;

    invoke-static/range {v46 .. v46}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v33

    check-cast v33, LX/0kL;

    const/16 v34, 0x2294

    move-object/from16 v0, v46

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v34

    invoke-static/range {v46 .. v46}, LX/0fO;->a(LX/0QB;)LX/0fO;

    move-result-object v35

    check-cast v35, LX/0fO;

    invoke-static/range {v46 .. v46}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v36

    check-cast v36, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v46 .. v46}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v37

    check-cast v37, LX/0SG;

    invoke-static/range {v46 .. v46}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v38

    check-cast v38, LX/03V;

    invoke-static/range {v46 .. v46}, LX/AFQ;->a(LX/0QB;)LX/AFQ;

    move-result-object v39

    check-cast v39, LX/AFQ;

    const/16 v40, 0x6a1

    move-object/from16 v0, v46

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v40

    invoke-static/range {v46 .. v46}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v41

    check-cast v41, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v46 .. v46}, LX/ArV;->a(LX/0QB;)LX/ArV;

    move-result-object v42

    check-cast v42, LX/ArV;

    invoke-static/range {v46 .. v46}, LX/192;->a(LX/0QB;)LX/192;

    move-result-object v43

    check-cast v43, LX/192;

    invoke-static/range {v46 .. v46}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v44

    check-cast v44, LX/0Sh;

    invoke-static/range {v46 .. v46}, LX/ArS;->a(LX/0QB;)LX/ArS;

    move-result-object v45

    check-cast v45, LX/ArS;

    const/16 v47, 0x22ac

    invoke-static/range {v46 .. v47}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v46

    invoke-static/range {v2 .. v46}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/Aux;LX/Aue;LX/Atz;LX/AuM;LX/Ate;LX/0Or;LX/0Or;LX/0Or;LX/AtN;LX/AsJ;LX/AsC;LX/AsS;LX/Ari;LX/Ati;LX/Ast;LX/AuG;LX/0Or;LX/AuU;LX/Avt;LX/0Or;LX/0Or;LX/AR2;LX/ARD;LX/AR4;LX/0Or;LX/0Or;LX/Atb;LX/0i4;LX/ArU;LX/ArN;LX/0kL;LX/0Or;LX/0fO;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/03V;LX/AFQ;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/ArV;LX/192;LX/0Sh;LX/ArS;LX/0Ot;)V

    return-void
.end method

.method public static a(LX/0Px;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 107779
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    const-string v3, "1752514608329267"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0jL;Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 107748
    invoke-static {p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v0

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    move-result-object v4

    const-string v5, "1752514608329267"

    invoke-virtual {v4, v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->setId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->setInspirationModels(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    move-result-object v4

    .line 107749
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v5

    .line 107750
    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v1

    .line 107751
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/87V;

    .line 107752
    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v6

    .line 107753
    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(LX/0Px;)Z

    move-result p2

    if-eqz p2, :cond_8

    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_8

    invoke-static {v6}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(LX/0Px;)Z

    move-result p2

    if-nez p2, :cond_8

    const/4 p2, 0x1

    :goto_0
    move v1, p2

    .line 107754
    if-eqz v1, :cond_3

    .line 107755
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ao:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ao:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v6

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 107756
    if-nez v1, :cond_9

    move v8, v10

    :goto_1
    if-nez v6, :cond_a

    move v7, v10

    :goto_2
    xor-int/2addr v7, v8

    if-eqz v7, :cond_b

    .line 107757
    :cond_0
    :goto_3
    move v1, v9

    .line 107758
    if-eqz v1, :cond_2

    .line 107759
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ao:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v1

    .line 107760
    :goto_4
    if-eqz v1, :cond_1

    const-string v6, "1752514608329267"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 107761
    :cond_1
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, LX/87V;->a(LX/0io;Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)Ljava/lang/String;

    move-result-object v0

    .line 107762
    :goto_5
    invoke-static {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setInspirationModels(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setSelectedId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setStarId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_7

    move v0, v2

    :goto_6
    invoke-virtual {v1, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setAreEffectsLoading(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Ljava/lang/Object;

    .line 107763
    return-void

    :cond_2
    move-object v1, v3

    .line 107764
    goto :goto_4

    .line 107765
    :cond_3
    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 107766
    :cond_4
    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v6

    invoke-static {v6, v1}, LX/87Q;->c(LX/0Px;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    move-object v0, v1

    .line 107767
    goto :goto_5

    .line 107768
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v6

    invoke-static {v6, v1}, LX/87Q;->b(LX/0Px;Ljava/lang/String;)I

    move-result v1

    .line 107769
    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v1, v6, :cond_6

    .line 107770
    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 107771
    :cond_6
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, LX/87V;->a(LX/0io;Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 107772
    :cond_7
    const/4 v0, 0x0

    goto :goto_6

    :cond_8
    const/4 p2, 0x0

    goto/16 :goto_0

    :cond_9
    move v8, v9

    .line 107773
    goto/16 :goto_1

    :cond_a
    move v7, v9

    goto/16 :goto_2

    .line 107774
    :cond_b
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    if-ne v7, v8, :cond_0

    move v8, v9

    .line 107775
    :goto_7
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    if-ge v8, v7, :cond_c

    .line 107776
    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-static {p2, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 107777
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_7

    :cond_c
    move v9, v10

    .line 107778
    goto/16 :goto_3
.end method

.method public static a$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/photos/base/media/PhotoItem;)V
    .locals 13

    .prologue
    .line 107695
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJX;

    .line 107696
    sget-object v1, LX/0zS;->b:LX/0zS;

    sget-object v2, LX/AJX;->b:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v0, v1, v2}, LX/AJX;->a(LX/AJX;LX/0zS;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/AJV;

    invoke-direct {v2, v0}, LX/AJV;-><init>(LX/AJX;)V

    iget-object v3, v0, LX/AJX;->d:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 107697
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/87T;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    .line 107698
    invoke-static {v1}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    .line 107699
    invoke-static {v2}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 107700
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 107701
    :goto_0
    if-nez v3, :cond_3

    .line 107702
    :goto_1
    move-object v0, v2

    .line 107703
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->al:LX/0Px;

    .line 107704
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 107705
    invoke-static {}, Lcom/facebook/audience/model/SharesheetSelectedAudience;->newBuilder()LX/7h4;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 107706
    iput-object v2, v1, LX/7h4;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 107707
    move-object v1, v1

    .line 107708
    iget-object v2, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->g:LX/0Px;

    .line 107709
    iput-object v2, v1, LX/7h4;->a:LX/0Px;

    .line 107710
    move-object v1, v1

    .line 107711
    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->h:Z

    .line 107712
    iput-boolean v0, v1, LX/7h4;->c:Z

    .line 107713
    move-object v0, v1

    .line 107714
    invoke-virtual {v0}, LX/7h4;->a()Lcom/facebook/audience/model/SharesheetSelectedAudience;

    move-result-object v1

    .line 107715
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v2, LX/ArJ;->PREVIEW_CONFIRM:LX/ArJ;

    .line 107716
    iget-object v7, v0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0il;

    invoke-interface {v7}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v7}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v7

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setShareSheetSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v7

    iget-object v8, v0, LX/ArT;->a:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v9

    invoke-virtual {v7, v9, v10}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setShareSheetSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setLoggingSurface(LX/874;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v7

    invoke-static {v0, v7}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 107717
    iget-object v7, v0, LX/ArT;->b:LX/ArL;

    .line 107718
    sget-object v8, LX/ArH;->START_SHARE_SHEET_SESSION:LX/ArH;

    invoke-static {v7, v8, v2}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v7, v9}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    sget-object v9, LX/ArI;->EXTRA_ANNOTATIONS_DATA:LX/ArI;

    invoke-virtual {v9}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7}, LX/ArL;->i(LX/ArL;)LX/0lF;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    invoke-static {v7, v8}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 107719
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->av:Z

    .line 107720
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->N:LX/AFQ;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->al:LX/0Px;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->al:LX/0Px;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 107721
    :goto_2
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v5

    if-nez v5, :cond_5

    .line 107722
    :cond_0
    const/4 v5, 0x0

    .line 107723
    :goto_3
    move-object v4, v5

    .line 107724
    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->al:LX/0Px;

    invoke-static {v5}, LX/7kq;->j(LX/0Px;)Z

    move-result v5

    move-object v6, p0

    .line 107725
    iget-object p1, v0, LX/AFQ;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v6}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    move-object v7, v0

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    move v12, v5

    .line 107726
    iget-object v0, v7, LX/AFQ;->a:LX/17Y;

    sget-object v1, LX/0ax;->gq:Ljava/lang/String;

    invoke-interface {v0, p0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 107727
    const-string v1, "extra_selected_audience"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 107728
    const-string v1, "extra_prompt_id"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107729
    const-string v1, "extra_inspiration_group_session_id"

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107730
    const-string v1, "extra_media_content_id"

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107731
    const-string v1, "extra_is_video"

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 107732
    move-object v7, v0

    .line 107733
    const/16 v8, 0x3c

    invoke-interface {p1, v7, v8, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 107734
    invoke-virtual {v6}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, LX/AFQ;->a(Landroid/app/Activity;)V

    .line 107735
    return-void

    .line 107736
    :cond_1
    const/4 v4, 0x0

    goto :goto_2

    .line 107737
    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 107738
    :cond_3
    invoke-static {v2}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setDisplayUri(Ljava/lang/String;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v2

    .line 107739
    iput-object v2, v4, LX/7kv;->f:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 107740
    move-object v2, v4

    .line 107741
    invoke-virtual {v2}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    goto/16 :goto_1

    .line 107742
    :cond_4
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107743
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107744
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v3

    iget-object v2, v0, LX/87T;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/74n;

    invoke-static {v3, v2}, LX/7kv;->a(Landroid/net/Uri;LX/74n;)LX/7kv;

    move-result-object v2

    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setDisplayUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setOriginalUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->k()F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setAspectRatio(F)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    .line 107745
    iput-object v3, v2, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 107746
    move-object v2, v2

    .line 107747
    invoke-virtual {v2}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    goto/16 :goto_1

    :cond_5
    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_3
.end method

.method public static aa(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 107694
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-static {v0}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    return-object v0
.end method

.method public static ab(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 107462
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    .line 107463
    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v1

    .line 107464
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/ArJ;)V
    .locals 3
    .param p1    # LX/ArJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 107465
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    .line 107466
    iget-boolean v2, v1, LX/Ata;->c:Z

    move v1, v2

    .line 107467
    iget-boolean v2, v0, LX/0iJ;->r:Z

    if-eqz v2, :cond_1

    .line 107468
    if-nez v1, :cond_0

    .line 107469
    invoke-static {v0, p1}, LX/0iJ;->a(LX/0iJ;LX/ArJ;)V

    .line 107470
    :cond_0
    invoke-static {v0}, LX/0iJ;->n(LX/0iJ;)V

    .line 107471
    iget-object v2, v0, LX/0iJ;->k:LX/Avn;

    invoke-virtual {v2}, LX/Avn;->e()V

    .line 107472
    iget-object v2, v0, LX/0iJ;->k:LX/Avn;

    invoke-virtual {v2}, LX/Avn;->b()V

    .line 107473
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    .line 107474
    iget-boolean v1, v0, LX/0iJ;->r:Z

    move v0, v1

    .line 107475
    if-nez v0, :cond_2

    .line 107476
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    invoke-virtual {v0, p1}, LX/ArT;->q(LX/ArJ;)V

    .line 107477
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->D(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 107478
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    invoke-virtual {v0, p1}, LX/ArT;->e(LX/ArJ;)V

    .line 107479
    :cond_2
    :goto_0
    if-eqz p1, :cond_3

    .line 107480
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    invoke-virtual {v0, p1}, LX/ArT;->a(LX/ArJ;)V

    .line 107481
    :cond_3
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    .line 107482
    iget-boolean v1, v0, LX/Ata;->c:Z

    move v0, v1

    .line 107483
    if-nez v0, :cond_4

    .line 107484
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Z(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    .line 107485
    :cond_4
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    const/4 v1, 0x0

    .line 107486
    iput-boolean v1, v0, LX/Ata;->d:Z

    .line 107487
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    sget-object v1, LX/5L2;->ON_PAUSE:LX/5L2;

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/5L2;)V

    .line 107488
    return-void

    .line 107489
    :cond_5
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    invoke-virtual {v0, p1}, LX/ArT;->c(LX/ArJ;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;)V
    .locals 4
    .param p0    # Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 107185
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ax:I

    .line 107186
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ki;

    .line 107187
    if-nez p1, :cond_2

    const/4 v3, 0x1

    .line 107188
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ak:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-nez v1, :cond_0

    .line 107189
    sget-object v1, LX/21D;->NEWSFEED:LX/21D;

    const-string v2, "inspiration"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ak:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 107190
    :cond_0
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ak:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v1, v1

    .line 107191
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1Ki;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Ljava/lang/String;)Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    move-result-object v0

    move-object v1, v0

    .line 107192
    :goto_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->y:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARX;

    .line 107193
    new-instance v2, LX/Ar7;

    invoke-direct {v2, p0}, LX/Ar7;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    invoke-interface {v0, v1, v2}, LX/ARX;->a(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;LX/Ar6;)LX/HvN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    .line 107194
    if-nez p1, :cond_1

    .line 107195
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 107196
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->J:LX/0fO;

    .line 107197
    invoke-static {v1}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v2

    invoke-virtual {v2}, LX/0i8;->d()Z

    move-result v2

    move v1, v2

    .line 107198
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->K:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/AwE;->b:LX/0Tn;

    invoke-interface {v2, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    .line 107199
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v2}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->setIsCameraFrontFacing(Z)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Ljava/lang/Object;

    .line 107200
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->setDefaultInspirationLandingIndex(I)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v1

    .line 107201
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 107202
    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->setInspirationModels(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0jL;Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)V

    .line 107203
    invoke-virtual {v0}, LX/0jL;->a()V

    .line 107204
    :cond_1
    return-void

    :cond_2
    move-object v1, p1

    .line 107205
    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/ArJ;)V
    .locals 7
    .param p0    # Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 107171
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->I:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Av8;

    new-instance v3, LX/ArB;

    invoke-direct {v3, p0, p1}, LX/ArB;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/ArJ;)V

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->E:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->E:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->getAppliedInspirations()LX/0Px;

    move-result-object v1

    move-object v2, v1

    :goto_0
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isLocationFetchAndRequeryingEffectsInProgress()Z

    move-result v1

    .line 107172
    iget-boolean v5, v0, LX/Av8;->f:Z

    if-nez v5, :cond_1

    .line 107173
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/ArB;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)V

    .line 107174
    :goto_1
    return-void

    .line 107175
    :cond_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 107176
    move-object v2, v1

    goto :goto_0

    .line 107177
    :cond_1
    iget-object v6, v0, LX/Av8;->a:LX/AvB;

    if-eqz v1, :cond_2

    sget-object v5, LX/AvA;->NETWORK:LX/AvA;

    :goto_2
    invoke-virtual {v6, v5}, LX/AvB;->a(LX/AvA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 107178
    iget-object v5, v0, LX/Av8;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/AvE;

    .line 107179
    invoke-virtual {v5, v6}, LX/AvE;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 107180
    new-instance p0, LX/Av7;

    invoke-direct {p0, v0, v2, v4}, LX/Av7;-><init>(LX/Av8;LX/0Px;LX/0is;)V

    iget-object p1, v0, LX/Av8;->c:Ljava/util/concurrent/Executor;

    invoke-static {v6, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 107181
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107182
    new-instance p0, LX/AvC;

    invoke-direct {p0, v5, v3}, LX/AvC;-><init>(LX/AvE;LX/ArB;)V

    iget-object p1, v5, LX/AvE;->f:Ljava/util/concurrent/Executor;

    invoke-static {v6, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 107183
    goto :goto_1

    .line 107184
    :cond_2
    sget-object v5, LX/AvA;->CACHE:LX/AvA;

    goto :goto_2
.end method

.method public static d(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/ArJ;)V
    .locals 3
    .param p0    # Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 107153
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    .line 107154
    iget-object v1, v0, LX/AtM;->u:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 107155
    sget-object v1, LX/87O;->PREVIEW:LX/87O;

    invoke-static {v0, v1}, LX/AtM;->a(LX/AtM;LX/87O;)V

    .line 107156
    sget-object v1, LX/87O;->PREVIEW:LX/87O;

    iput-object v1, v0, LX/AtM;->H:LX/87O;

    .line 107157
    invoke-virtual {v0}, LX/AtM;->b()V

    .line 107158
    if-eqz p1, :cond_0

    .line 107159
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    invoke-virtual {v0, p1}, LX/ArT;->d(LX/ArJ;)V

    .line 107160
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    invoke-virtual {v0, p1}, LX/ArT;->p(LX/ArJ;)V

    .line 107161
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab:LX/Aud;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aud;

    .line 107162
    iget-object v1, v0, LX/Aud;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 107163
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 107164
    iget-object v2, v0, LX/Aud;->g:LX/Auh;

    iget-object p0, v0, LX/Aud;->e:LX/87P;

    invoke-virtual {p0, v1}, LX/87P;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Landroid/graphics/Rect;

    move-result-object v1

    .line 107165
    iput-object v1, v2, LX/Auh;->e:Landroid/graphics/Rect;

    .line 107166
    new-instance p0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result p1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 107167
    iget p1, v1, Landroid/graphics/Rect;->left:I

    iput p1, p0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 107168
    iget p1, v1, Landroid/graphics/Rect;->top:I

    iput p1, p0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 107169
    iget-object p1, v2, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {p1, p0}, Lcom/facebook/drawingview/DrawingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 107170
    return-void
.end method

.method private r()V
    .locals 8

    .prologue
    .line 107139
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v1, 0x7f0d17ac

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/view/ViewStub;

    .line 107140
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v1, 0x7f0d0ba3

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->af:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    .line 107141
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->s:LX/Avt;

    .line 107142
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    .line 107143
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v4, v4

    .line 107144
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    new-instance v6, LX/0zw;

    invoke-direct {v6, v7}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    invoke-virtual/range {v0 .. v6}, LX/Avt;->a(Ljava/lang/Object;LX/ArL;LX/ArT;LX/0gc;Landroid/app/Activity;LX/0zw;)LX/0iJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    .line 107145
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    invoke-static {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107146
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->K(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    .line 107147
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->s(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    .line 107148
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->p:LX/AuG;

    .line 107149
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    new-instance v3, LX/Aqw;

    invoke-direct {v3, p0}, LX/Aqw;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v4, 0x7f0d17ae

    invoke-static {v0, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v1, v2, v3, v0}, LX/AuG;->a(Ljava/lang/Object;LX/Aqw;Landroid/view/ViewStub;)LX/AuF;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107150
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->t$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107151
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->m()V

    .line 107152
    :cond_0
    return-void
.end method

.method private static s(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V
    .locals 12

    .prologue
    .line 107133
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->r:LX/AuU;

    .line 107134
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v4, 0x7f0d17b0

    invoke-static {v0, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 107135
    new-instance v5, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;

    move-object v6, v2

    check-cast v6, LX/0il;

    const/16 v7, 0xc81

    invoke-static {v1, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v11

    check-cast v11, Landroid/content/res/Resources;

    move-object v7, v3

    move-object v8, v0

    invoke-direct/range {v5 .. v11}, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;-><init>(LX/0il;LX/Ata;Landroid/view/ViewStub;LX/0Or;LX/1Ck;Landroid/content/res/Resources;)V

    .line 107136
    move-object v0, v5

    .line 107137
    invoke-static {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107138
    return-void
.end method

.method private static t$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z
    .locals 3

    .prologue
    .line 107128
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isInNuxMode()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    sget-object v1, LX/AtZ;->CAMERA_PERMISSION:LX/AtZ;

    invoke-virtual {v0, v1}, LX/Ata;->a(LX/AtZ;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    .line 107129
    iget-object v1, v0, LX/0iJ;->q:LX/0iB;

    if-nez v1, :cond_0

    .line 107130
    iget-object v1, v0, LX/0iJ;->l:LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAMERA:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, p0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class p0, LX/0iB;

    invoke-virtual {v1, v2, p0}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/0iB;

    iput-object v1, v0, LX/0iJ;->q:LX/0iB;

    .line 107131
    :cond_0
    iget-object v1, v0, LX/0iJ;->q:LX/0iB;

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 107132
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 11

    .prologue
    .line 107068
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->t$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107069
    :goto_0
    return-void

    .line 107070
    :cond_0
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Z(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    .line 107071
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    new-instance v1, LX/ArA;

    invoke-direct {v1, p0}, LX/ArA;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    const/4 v4, 0x1

    .line 107072
    iput-object v1, v0, LX/0iJ;->t:LX/ArA;

    .line 107073
    iget-boolean v2, v0, LX/0iJ;->r:Z

    move v2, v2

    .line 107074
    if-eqz v2, :cond_1

    .line 107075
    :goto_1
    goto :goto_0

    .line 107076
    :cond_1
    invoke-static {v0, v4}, LX/0iJ;->a(LX/0iJ;Z)V

    .line 107077
    iget-object v2, v0, LX/0iJ;->i:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    .line 107078
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 107079
    const v2, 0x7f0d17c9

    invoke-static {v3, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iput-object v2, v0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    .line 107080
    const v2, 0x7f0d17ca

    invoke-static {v3, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    iput-object v2, v0, LX/0iJ;->p:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    .line 107081
    iget-object v2, v0, LX/0iJ;->p:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    invoke-virtual {v2, v4}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->setFillFirstCircle(Z)V

    .line 107082
    iget-object v2, v0, LX/0iJ;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    .line 107083
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a()LX/0Px;

    move-result-object v3

    iput-object v3, v0, LX/0iJ;->s:LX/0Px;

    .line 107084
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->b()Landroid/net/Uri;

    move-result-object v3

    .line 107085
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->c()Landroid/net/Uri;

    move-result-object v2

    .line 107086
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v0, LX/0iJ;->u:Ljava/util/List;

    .line 107087
    iget-object v4, v0, LX/0iJ;->s:LX/0Px;

    if-eqz v4, :cond_3

    .line 107088
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 107089
    iget-object v4, v0, LX/0iJ;->s:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v9

    move v8, v7

    .line 107090
    :goto_2
    add-int/lit8 v4, v9, -0x1

    if-ge v8, v4, :cond_2

    .line 107091
    iget-object v4, v0, LX/0iJ;->s:LX/0Px;

    invoke-virtual {v4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    iget-object v5, v0, LX/0iJ;->e:[I

    rem-int v10, v8, v9

    aget v5, v5, v10

    .line 107092
    new-instance v10, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    invoke-direct {v10}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;-><init>()V

    .line 107093
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 107094
    const-string v1, "inspiration_nux_video_uri"

    invoke-virtual {p0, v1, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 107095
    const-string v1, "video_foreground_color_id"

    invoke-virtual {p0, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 107096
    invoke-virtual {v10, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 107097
    move-object v5, v10

    .line 107098
    move-object v4, v5

    .line 107099
    check-cast v4, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    new-instance v10, LX/Avq;

    invoke-direct {v10, v0, v8}, LX/Avq;-><init>(LX/0iJ;I)V

    .line 107100
    iput-object v10, v4, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->f:LX/Avq;

    .line 107101
    iget-object v4, v0, LX/0iJ;->u:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107102
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_2

    .line 107103
    :cond_2
    if-ne v9, v6, :cond_4

    move v5, v6

    .line 107104
    :goto_3
    iget-object v4, v0, LX/0iJ;->s:LX/0Px;

    invoke-virtual {v4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    if-eqz v5, :cond_5

    iget-object v6, v0, LX/0iJ;->e:[I

    iget-object v7, v0, LX/0iJ;->e:[I

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    aget v6, v6, v7

    :goto_4
    invoke-static {v4, v6, v5}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->a(Landroid/net/Uri;IZ)Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;

    move-result-object v4

    .line 107105
    invoke-static {v0, v4}, LX/0iJ;->a(LX/0iJ;Lcom/facebook/base/fragment/FbFragment;)V

    .line 107106
    :goto_5
    iget-object v4, v0, LX/0iJ;->u:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v0, LX/0iJ;->v:I

    .line 107107
    new-instance v4, LX/Avl;

    iget-object v5, v0, LX/0iJ;->j:LX/0gc;

    iget-object v6, v0, LX/0iJ;->u:Ljava/util/List;

    invoke-direct {v4, v5, v6}, LX/Avl;-><init>(LX/0gc;Ljava/util/List;)V

    .line 107108
    iget-object v5, v0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    invoke-virtual {v5, v4}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setAdapter(LX/0gG;)V

    .line 107109
    iget-object v4, v0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget v5, v0, LX/0iJ;->v:I

    invoke-virtual {v4, v5}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setOffscreenPageLimit(I)V

    .line 107110
    iget-object v4, v0, LX/0iJ;->k:LX/Avn;

    .line 107111
    iput-object v3, v4, LX/Avn;->f:Landroid/net/Uri;

    .line 107112
    iget-object v3, v0, LX/0iJ;->k:LX/Avn;

    .line 107113
    iput-object v2, v3, LX/Avn;->g:Landroid/net/Uri;

    .line 107114
    const/4 v4, 0x0

    .line 107115
    iget-object v2, v0, LX/0iJ;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/AwE;->d:LX/0Tn;

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 107116
    iget v2, v0, LX/0iJ;->v:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, LX/0iJ;->a$redex0(LX/0iJ;I)V

    .line 107117
    :goto_6
    iget-object v2, v0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    new-instance v3, LX/Avs;

    invoke-direct {v3, v0}, LX/Avs;-><init>(LX/0iJ;)V

    .line 107118
    iput-object v3, v2, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ab:LX/Avr;

    .line 107119
    goto/16 :goto_1

    .line 107120
    :cond_3
    const/4 v4, 0x1

    .line 107121
    const/4 v5, 0x0

    iget-object v6, v0, LX/0iJ;->e:[I

    iget-object v7, v0, LX/0iJ;->e:[I

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    aget v6, v6, v7

    iget v7, v0, LX/0iJ;->v:I

    if-ne v7, v4, :cond_7

    :goto_7
    invoke-static {v5, v6, v4}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->a(Landroid/net/Uri;IZ)Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;

    move-result-object v4

    .line 107122
    invoke-static {v0, v4}, LX/0iJ;->a(LX/0iJ;Lcom/facebook/base/fragment/FbFragment;)V

    .line 107123
    goto :goto_5

    :cond_4
    move v5, v7

    .line 107124
    goto :goto_3

    .line 107125
    :cond_5
    iget-object v6, v0, LX/0iJ;->e:[I

    rem-int v7, v8, v9

    aget v6, v6, v7

    goto :goto_4

    .line 107126
    :cond_6
    invoke-static {v0, v4}, LX/0iJ;->a$redex0(LX/0iJ;I)V

    goto :goto_6

    .line 107127
    :cond_7
    const/4 v4, 0x0

    goto :goto_7
.end method

.method public static w(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V
    .locals 2

    .prologue
    .line 107065
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Z:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ae:Landroid/view/View;

    if-nez v0, :cond_1

    .line 107066
    :cond_0
    :goto_0
    return-void

    .line 107067
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ae:Landroid/view/View;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment$8;

    invoke-direct {v1, p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment$8;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private y()Z
    .locals 1

    .prologue
    .line 107062
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->X(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->av:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    .line 107063
    iget-boolean p0, v0, LX/Ata;->c:Z

    move v0, p0

    .line 107064
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const v3, 0xa10002

    .line 107027
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->a(Landroid/os/Bundle;)V

    .line 107028
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 107029
    invoke-direct {p0, v3}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(I)V

    .line 107030
    const v0, 0xa10005

    invoke-direct {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(I)V

    .line 107031
    const v0, 0xa10007

    invoke-direct {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(I)V

    .line 107032
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 107033
    const-string v1, "inspiration_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;

    .line 107034
    if-eqz v0, :cond_0

    .line 107035
    invoke-virtual {v0}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->getComposerConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ak:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 107036
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 107037
    :goto_0
    new-instance v1, LX/0ij;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ay:LX/Ar1;

    invoke-direct {v1, v2}, LX/0ij;-><init>(LX/Ar1;)V

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    .line 107038
    invoke-static {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->b(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;)V

    .line 107039
    new-instance v1, LX/ArC;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-direct {v1, p0, v2}, LX/ArC;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0il;)V

    invoke-static {p0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107040
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->D:LX/ArN;

    .line 107041
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    .line 107042
    new-instance p1, LX/ArL;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {p1, v1, v2}, LX/ArL;-><init>(LX/0il;LX/0Zb;)V

    .line 107043
    invoke-static {v0}, LX/87V;->a(LX/0QB;)LX/87V;

    move-result-object v2

    check-cast v2, LX/87V;

    .line 107044
    iput-object v2, p1, LX/ArL;->c:LX/87V;

    .line 107045
    move-object v0, p1

    .line 107046
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    .line 107047
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->C:LX/ArU;

    .line 107048
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    .line 107049
    new-instance p1, LX/ArT;

    check-cast v2, LX/0il;

    invoke-direct {p1, v1, v2}, LX/ArT;-><init>(LX/ArL;LX/0il;)V

    .line 107050
    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    .line 107051
    iput-object v4, p1, LX/ArT;->a:LX/0SG;

    .line 107052
    move-object v0, p1

    .line 107053
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    .line 107054
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->B:LX/0i4;

    .line 107055
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    .line 107056
    new-instance v2, LX/Ata;

    invoke-direct {v2, v0, v1}, LX/Ata;-><init>(LX/0i5;LX/ArL;)V

    .line 107057
    move-object v0, v2

    .line 107058
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ag:LX/Ata;

    .line 107059
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 107060
    return-void

    .line 107061
    :cond_1
    const-string v0, "state_key_system_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 107020
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->au:Z

    .line 107021
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-static {v1}, LX/Arx;->c(Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107022
    :goto_0
    return v0

    .line 107023
    :cond_0
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getInspirationBackStack()LX/0Px;

    move-result-object v1

    .line 107024
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107025
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->B(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    goto :goto_0

    .line 107026
    :cond_1
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-static {v1, v0, v2}, LX/87R;->a(LX/0il;ZLX/0jK;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 107206
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->b(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;)V

    .line 107207
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iK;

    .line 107208
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-virtual {v2, v0}, LX/HvN;->a(LX/0iK;)V

    goto :goto_0

    .line 107209
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 107210
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    invoke-interface {v0}, LX/0iO;->e()Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;

    move-result-object v1

    .line 107211
    const/4 v0, 0x0

    .line 107212
    if-eqz v1, :cond_2

    .line 107213
    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->getCTAConfig()Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 107214
    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->getCTAConfig()Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->E:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    .line 107215
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ar:Z

    .line 107216
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->getStartReason()LX/ArJ;

    move-result-object v0

    .line 107217
    :cond_2
    invoke-direct {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(LX/ArJ;)V

    .line 107218
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->w(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    .line 107219
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 107220
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0it;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107221
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Q:LX/ArV;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 107222
    iget-object v2, v0, LX/ArV;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0gt;

    const-string v3, "555132941353983"

    .line 107223
    iput-object v3, v2, LX/0gt;->a:Ljava/lang/String;

    .line 107224
    move-object v2, v2

    .line 107225
    invoke-virtual {v2, v1}, LX/0gt;->a(Landroid/content/Context;)V

    .line 107226
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ao:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    .line 107227
    const/4 v0, 0x0

    .line 107228
    iget-boolean v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->av:Z

    if-nez v1, :cond_1

    .line 107229
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->au:Z

    if-eqz v0, :cond_2

    sget-object v0, LX/ArJ;->TAP_TO_FEED_BUTTON:LX/ArJ;

    .line 107230
    :cond_1
    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->b(LX/ArJ;)V

    .line 107231
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->au:Z

    .line 107232
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 107233
    return-void

    .line 107234
    :cond_2
    sget-object v0, LX/ArJ;->SWIPE_TO_FEED:LX/ArJ;

    goto :goto_0
.end method

.method public final m()V
    .locals 15

    .prologue
    .line 107235
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    if-nez v0, :cond_0

    .line 107236
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, LX/0f9;

    if-eqz v0, :cond_3

    .line 107237
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/0f9;

    invoke-interface {v0}, LX/0f9;->mn_()LX/0iO;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    .line 107238
    :cond_0
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->n:LX/Ati;

    .line 107239
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    const/16 v5, 0x4e20

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v6, 0x7f0d07c5

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 107240
    new-instance v7, LX/Ath;

    move-object v8, v4

    check-cast v8, LX/0il;

    const-class v9, Landroid/content/Context;

    invoke-interface {v3, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-static {v3}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v12

    check-cast v12, LX/0Sh;

    invoke-static {v3}, LX/0wX;->a(LX/0QB;)LX/0wX;

    move-result-object v13

    check-cast v13, LX/0wY;

    invoke-static {v3}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v14

    check-cast v14, LX/0wW;

    move v9, v5

    move-object v10, v2

    invoke-direct/range {v7 .. v14}, LX/Ath;-><init>(LX/0il;ILandroid/view/ViewStub;Landroid/content/Context;LX/0Sh;LX/0wY;LX/0wW;)V

    .line 107241
    move-object v2, v7

    .line 107242
    invoke-static {p0, v2}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107243
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v3, 0x7f0d179a

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 107244
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ae:Landroid/view/View;

    .line 107245
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->m:LX/Ari;

    .line 107246
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->af:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ae:Landroid/view/View;

    check-cast v5, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    new-instance v6, LX/Aqx;

    invoke-direct {v6, p0}, LX/Aqx;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    iget-object v8, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    invoke-virtual/range {v2 .. v8}, LX/Ari;->a(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;LX/Aqx;LX/ArL;LX/ArT;)LX/Arh;

    move-result-object v2

    .line 107247
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->an:LX/Arh;

    .line 107248
    invoke-static {p0, v2}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107249
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Z:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    if-eqz v2, :cond_4

    .line 107250
    :goto_0
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->L()V

    .line 107251
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa:LX/Auw;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab:LX/Aud;

    if-nez v2, :cond_1

    .line 107252
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v3, 0x7f0d0ba3

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 107253
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->b:LX/Aue;

    .line 107254
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    .line 107255
    new-instance v6, LX/Aud;

    move-object v7, v4

    check-cast v7, LX/0il;

    const-class v8, Landroid/content/Context;

    invoke-interface {v3, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {v3}, LX/8GT;->a(LX/0QB;)LX/8GT;

    move-result-object v10

    check-cast v10, LX/8GT;

    invoke-static {v3}, LX/87P;->b(LX/0QB;)LX/87P;

    move-result-object v11

    check-cast v11, LX/87P;

    move-object v8, v2

    invoke-direct/range {v6 .. v11}, LX/Aud;-><init>(LX/0il;Landroid/widget/FrameLayout;Landroid/content/Context;LX/8GT;LX/87P;)V

    .line 107256
    move-object v3, v6

    .line 107257
    iput-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab:LX/Aud;

    .line 107258
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a:LX/Aux;

    .line 107259
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aC:LX/Ar5;

    .line 107260
    new-instance v6, LX/Auw;

    move-object v7, v4

    check-cast v7, LX/0il;

    const-class v8, Landroid/content/Context;

    invoke-interface {v3, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {v3}, LX/87P;->b(LX/0QB;)LX/87P;

    move-result-object v11

    check-cast v11, LX/87P;

    .line 107261
    new-instance v12, LX/Aur;

    const-class v8, Landroid/content/Context;

    invoke-interface {v3, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    .line 107262
    new-instance v13, LX/Av3;

    invoke-static {v3}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v9

    check-cast v9, LX/0wW;

    invoke-direct {v13, v9}, LX/Av3;-><init>(LX/0wW;)V

    .line 107263
    move-object v9, v13

    .line 107264
    check-cast v9, LX/Av3;

    invoke-direct {v12, v8, v9}, LX/Aur;-><init>(Landroid/content/Context;LX/Av3;)V

    .line 107265
    move-object v12, v12

    .line 107266
    check-cast v12, LX/Aur;

    invoke-static {v3}, LX/86n;->b(LX/0QB;)LX/86n;

    move-result-object v13

    check-cast v13, LX/86n;

    invoke-static {v3}, LX/0tS;->b(LX/0QB;)LX/0tS;

    move-result-object v14

    check-cast v14, LX/0tS;

    move-object v8, v2

    move-object v9, v5

    invoke-direct/range {v6 .. v14}, LX/Auw;-><init>(LX/0il;Landroid/widget/FrameLayout;LX/Ar5;Landroid/content/Context;LX/87P;LX/Aur;LX/86n;LX/0tS;)V

    .line 107267
    move-object v2, v6

    .line 107268
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa:LX/Auw;

    .line 107269
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa:LX/Auw;

    invoke-static {p0, v2}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107270
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab:LX/Aud;

    invoke-static {p0, v2}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107271
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    .line 107272
    iget-boolean v1, v0, LX/0iJ;->r:Z

    if-nez v1, :cond_6

    .line 107273
    :goto_1
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    if-eqz v0, :cond_2

    .line 107274
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    invoke-virtual {v0}, LX/AtM;->b()V

    .line 107275
    :cond_2
    return-void

    .line 107276
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The hosting activity didn\'t not implement InspirationCameraFragmentHost interface"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107277
    :cond_4
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v3, 0x7f0d179c

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 107278
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 107279
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v5, 0x7f0d1252

    invoke-static {v4, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 107280
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v5, 0x7f0d17ad

    invoke-static {v4, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 107281
    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/facebook/friendsharing/inspiration/view/InspirationHScrollCirclePageIndicator;

    .line 107282
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v5, 0x7f0d17af

    invoke-static {v4, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 107283
    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v8

    .line 107284
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v5, 0x7f0d1786

    invoke-static {v4, v5}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewStub;

    .line 107285
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->c:LX/Atz;

    .line 107286
    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    iget-object v6, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->an:LX/Arh;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    new-instance v12, LX/Aqz;

    invoke-direct {v12, p0}, LX/Aqz;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    iget-object v13, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    iget-object v14, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->T:LX/ArS;

    invoke-virtual/range {v4 .. v14}, LX/Atz;->a(Ljava/lang/Object;LX/Arh;Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;LX/Aqz;LX/ArT;LX/ArS;)Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Z:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    .line 107287
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Z:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    invoke-static {p0, v4}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0iK;)V

    .line 107288
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v2}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v2

    .line 107289
    if-eqz v2, :cond_5

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_5

    .line 107290
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->w(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    .line 107291
    :cond_5
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ae:Landroid/view/View;

    new-instance v3, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment$12;

    invoke-direct {v3, p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment$12;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    invoke-static {v2, v3}, LX/Awl;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 107292
    :cond_6
    iget-object v1, v0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setVisibility(I)V

    .line 107293
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0iJ;->a(LX/0iJ;Z)V

    .line 107294
    iget-object v1, v0, LX/0iJ;->k:LX/Avn;

    invoke-virtual {v1}, LX/Avn;->e()V

    .line 107295
    iget-object v1, v0, LX/0iJ;->k:LX/Avn;

    invoke-virtual {v1}, LX/Avn;->b()V

    .line 107296
    iget-object v1, v0, LX/0iJ;->h:LX/ArT;

    sget-object v2, LX/ArJ;->NUX_COMPLETE:LX/ArJ;

    invoke-virtual {v1, v2}, LX/ArT;->b(LX/ArJ;)V

    .line 107297
    iget-object v1, v0, LX/0iJ;->h:LX/ArT;

    sget-object v2, LX/ArJ;->NUX_COMPLETE:LX/ArJ;

    invoke-virtual {v1, v2}, LX/ArT;->p(LX/ArJ;)V

    .line 107298
    sget-object v1, LX/ArJ;->NUX_COMPLETE:LX/ArJ;

    invoke-static {v0, v1}, LX/0iJ;->a(LX/0iJ;LX/ArJ;)V

    .line 107299
    iget-object v1, v0, LX/0iJ;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    move-object v2, v1

    .line 107300
    check-cast v2, LX/0im;

    invoke-interface {v2}, LX/0im;->c()LX/0jJ;

    move-result-object v2

    sget-object v3, LX/0iJ;->d:LX/0jK;

    invoke-virtual {v2, v3}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v2

    .line 107301
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    sget-object v3, LX/86t;->NUX:LX/86t;

    invoke-static {v2, v1, v3}, LX/87R;->a(LX/0jL;Lcom/facebook/composer/system/model/ComposerModelImpl;LX/86t;)LX/0jL;

    move-result-object v1

    .line 107302
    invoke-virtual {v1}, LX/0jL;->a()V

    goto/16 :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 107303
    const/16 v0, 0x3c

    if-ne p1, v0, :cond_10

    .line 107304
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const-string v1, "extra_selected_audience"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/SharesheetSelectedAudience;

    .line 107305
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->T()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v5

    .line 107306
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    new-instance v6, LX/7lP;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v2

    invoke-direct {v6, v2}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 107307
    iget-object v2, v0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v2, v2

    .line 107308
    if-eqz v2, :cond_9

    move v2, v3

    .line 107309
    :goto_0
    iput-boolean v2, v6, LX/7lP;->a:Z

    .line 107310
    move-object v2, v6

    .line 107311
    iget-object v6, v0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v6, v6

    .line 107312
    invoke-virtual {v2, v6}, LX/7lP;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)LX/7lP;

    move-result-object v2

    .line 107313
    iget-object v6, v0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    move-object v6, v6

    .line 107314
    iput-object v6, v2, LX/7lP;->g:LX/0Px;

    .line 107315
    move-object v2, v2

    .line 107316
    iget-boolean v6, v0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->c:Z

    move v6, v6

    .line 107317
    iput-boolean v6, v2, LX/7lP;->h:Z

    .line 107318
    move-object v2, v2

    .line 107319
    invoke-virtual {v2}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jL;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    .line 107320
    iget-object v2, v1, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 107321
    iget-object v2, v1, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPromptAnalytics()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v2

    invoke-static {v2, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 107322
    iget-object v2, v1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v2, :cond_0

    .line 107323
    iget-object v2, v1, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v2

    iput-object v2, v1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 107324
    :cond_0
    iget-object v2, v1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v2, v5}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setPromptAnalytics(Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 107325
    iget-object v2, v1, LX/0jL;->a:LX/0cA;

    sget-object v6, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v2, v6}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 107326
    :cond_1
    move-object v1, v1

    .line 107327
    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 107328
    const/4 v1, -0x1

    if-ne p2, v1, :cond_e

    .line 107329
    const-string v2, "Inspirations content must have an attached prompt id"

    .line 107330
    invoke-static {v5, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107331
    iget-object v1, v5, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_11

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 107332
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->al:LX/0Px;

    invoke-virtual {v1, v2}, LX/0jL;->c(LX/0Px;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 107333
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->x:LX/AR4;

    .line 107334
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->w:LX/ARD;

    .line 107335
    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->v:LX/AR2;

    .line 107336
    iget-object v6, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1, v7}, LX/AR4;->a(Ljava/lang/Object;)LX/AR3;

    move-result-object v1

    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, LX/AR2;->a(Ljava/lang/Object;LX/ARN;)LX/AR1;

    move-result-object v5

    invoke-virtual {v2, v6, v1, v5}, LX/ARD;->a(Ljava/lang/Object;LX/AR3;LX/AR1;)LX/ARC;

    move-result-object v1

    invoke-virtual {v1}, LX/ARC;->b()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_composer_has_published"

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 107337
    iget-object v1, v0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, v1

    .line 107338
    if-eqz v1, :cond_2

    .line 107339
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->u:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 107340
    invoke-virtual {v1, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 107341
    :cond_2
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-static {v1}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    .line 107342
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 107343
    invoke-static {v2}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v2

    if-eqz v2, :cond_a

    check-cast v1, Lcom/facebook/photos/base/media/VideoItem;

    .line 107344
    iget-wide v10, v1, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v6, v10

    .line 107345
    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    .line 107346
    :goto_2
    iget-object v1, v0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, v1

    .line 107347
    if-eqz v1, :cond_b

    move v1, v3

    .line 107348
    :goto_3
    iget-object v2, v0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    move-object v2, v2

    .line 107349
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    move v2, v3

    .line 107350
    :goto_4
    iget-boolean v5, v0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->c:Z

    move v0, v5

    .line 107351
    if-nez v1, :cond_3

    if-nez v2, :cond_3

    if-eqz v0, :cond_4

    :cond_3
    move v4, v3

    :cond_4
    const-string v3, "We must have posted somewhere"

    invoke-static {v4, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 107352
    if-nez v1, :cond_6

    if-nez v0, :cond_6

    .line 107353
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->N:LX/AFQ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "extra_sharesheet_survey_data"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "extra_sharesheet_integration_point_id"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v10, 0x1

    .line 107354
    iget-object v8, v0, LX/AFQ;->d:LX/0gI;

    invoke-virtual {v8, v10}, LX/0gI;->a(Z)V

    .line 107355
    iget-object v8, v0, LX/AFQ;->a:LX/17Y;

    sget-object v9, LX/0ax;->gp:Ljava/lang/String;

    invoke-interface {v8, v3, v9}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    .line 107356
    if-eqz v4, :cond_5

    if-eqz v5, :cond_5

    .line 107357
    const-string v9, "extra_sharesheet_survey_data"

    invoke-virtual {v8, v9, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 107358
    const-string v9, "extra_sharesheet_integration_point_id"

    invoke-virtual {v8, v9, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107359
    :cond_5
    iget-object v9, v0, LX/AFQ;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v9, v8, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 107360
    iget-object v8, v0, LX/AFQ;->c:LX/0hg;

    invoke-virtual {v8, v10}, LX/0hg;->a(Z)V

    .line 107361
    :cond_6
    sget-object v0, LX/ArJ;->DIRECT:LX/ArJ;

    .line 107362
    if-eqz v1, :cond_d

    if-eqz v2, :cond_d

    .line 107363
    sget-object v0, LX/ArJ;->NEWSFEED_DIRECT:LX/ArJ;

    .line 107364
    :cond_7
    :goto_5
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    long-to-float v2, v6

    invoke-virtual {v1, v0, v2}, LX/ArL;->b(LX/ArJ;F)V

    .line 107365
    const/4 v0, 0x0

    sget-object v1, LX/875;->UNKNOWN:LX/875;

    invoke-static {p0, v0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)V

    .line 107366
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    invoke-interface {v0}, LX/0iO;->b()Z

    .line 107367
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    invoke-interface {v0}, LX/0iO;->c()V

    .line 107368
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v1, LX/ArJ;->POST_PROMPT:LX/ArJ;

    invoke-virtual {v0, v1}, LX/ArT;->g(LX/ArJ;)V

    .line 107369
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v1, LX/ArJ;->POST_PROMPT:LX/ArJ;

    invoke-virtual {v0, v1}, LX/ArT;->a(LX/ArJ;)V

    .line 107370
    :cond_8
    :goto_6
    return-void

    :cond_9
    move v2, v4

    .line 107371
    goto/16 :goto_0

    .line 107372
    :cond_a
    const-wide/16 v6, 0x0

    goto/16 :goto_2

    :cond_b
    move v1, v4

    .line 107373
    goto/16 :goto_3

    :cond_c
    move v2, v4

    .line 107374
    goto/16 :goto_4

    .line 107375
    :cond_d
    if-eqz v1, :cond_7

    .line 107376
    sget-object v0, LX/ArJ;->NEWSFEED:LX/ArJ;

    goto :goto_5

    .line 107377
    :cond_e
    if-nez p2, :cond_8

    .line 107378
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    .line 107379
    sget-object v1, LX/ArH;->CANCEL_COMPOSER:LX/ArH;

    invoke-static {v0, v1}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v2}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/ArI;->EXTRA_ANNOTATIONS_DATA:LX/ArI;

    invoke-virtual {v2}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, LX/ArL;->i(LX/ArL;)LX/0lF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 107380
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v1, LX/ArJ;->CANCEL_COMPOSER:LX/ArJ;

    invoke-virtual {v0, v1}, LX/ArT;->g(LX/ArJ;)V

    .line 107381
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v1, LX/ArJ;->CANCEL_COMPOSER:LX/ArJ;

    invoke-virtual {v0, v1}, LX/ArT;->d(LX/ArJ;)V

    .line 107382
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v1, LX/ArJ;->CANCEL_COMPOSER:LX/ArJ;

    invoke-virtual {v0, v1}, LX/ArT;->p(LX/ArJ;)V

    .line 107383
    const-string v0, "extra_sharesheet_survey_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "extra_sharesheet_integration_point_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 107384
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 107385
    const-string v1, "extra_sharesheet_integration_point_id"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107386
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 107387
    move-object v0, v0

    .line 107388
    const-string v1, "extra_sharesheet_survey_data"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/os/Bundle;)LX/0gt;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    .line 107389
    :cond_f
    goto :goto_6

    .line 107390
    :cond_10
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_6

    .line 107391
    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x6bf44652

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 107392
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onAttach(Landroid/app/Activity;)V

    .line 107393
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 107394
    instance-of v0, p1, LX/0f9;

    if-eqz v0, :cond_1

    .line 107395
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/0f9;

    invoke-interface {v0}, LX/0f9;->mn_()LX/0iO;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    .line 107396
    :cond_0
    const v0, -0x6f217f22

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 107397
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "The hosting activity didn\'t not implement InspirationCameraFragmentHost interface"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const v2, -0x7c6505f2

    invoke-static {v2, v1}, LX/02F;->f(II)V

    throw v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 107398
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 107399
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_0

    .line 107400
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Z(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    .line 107401
    :cond_0
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aD:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_2

    .line 107402
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aD:I

    if-ne v0, v2, :cond_1

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 107403
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->w(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    .line 107404
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aD:I

    .line 107405
    :cond_2
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v3, 0xa10003

    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x46a89449

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 107449
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 107450
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const/16 v2, 0x2b

    const v3, -0x16e9ae19

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 107451
    :goto_0
    return-object v0

    .line 107452
    :cond_0
    invoke-direct {p0, v3}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(I)V

    .line 107453
    const v0, 0x7f03093a

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    .line 107454
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->r()V

    .line 107455
    if-eqz p3, :cond_1

    .line 107456
    const-string v0, "did_launch_to_sharesheet"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->av:Z

    .line 107457
    const-string v0, "processed_composer_attachment"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 107458
    if-eqz v0, :cond_1

    .line 107459
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->al:LX/0Px;

    .line 107460
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 107461
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ad:Landroid/widget/FrameLayout;

    const v2, 0x3dafcd3a

    invoke-static {v2, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x31f05b52

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 107406
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-virtual {v1}, LX/HvN;->g()V

    .line 107407
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onDestroy()V

    .line 107408
    const/16 v1, 0x2b

    const v2, 0x12e145b6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6e964b29

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 107409
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    sget-object v2, LX/5L2;->ON_DESTROY_VIEW:LX/5L2;

    invoke-virtual {v1, v2}, LX/HvN;->a(LX/5L2;)V

    .line 107410
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onDestroyView()V

    .line 107411
    const/16 v1, 0x2b

    const v2, 0x60148132

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x49c7c88a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 107412
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->b(LX/ArJ;)V

    .line 107413
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->R:LX/192;

    .line 107414
    invoke-static {v0}, LX/192;->d(LX/192;)V

    .line 107415
    const v3, 0xa00ac

    invoke-static {v0, v3}, LX/192;->c(LX/192;I)V

    .line 107416
    const v3, 0xa00ab

    invoke-static {v0, v3}, LX/192;->c(LX/192;I)V

    .line 107417
    const v3, 0xa00a9

    invoke-static {v0, v3}, LX/192;->c(LX/192;I)V

    .line 107418
    const-wide/16 v3, -0x1

    iput-wide v3, v0, LX/192;->g:J

    .line 107419
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onPause()V

    .line 107420
    const v0, -0x2d26e6e4

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 107421
    :cond_0
    sget-object v0, LX/ArJ;->BACKGROUND_APP:LX/ArJ;

    goto :goto_0
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7e0dfa00

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 107422
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onResume()V

    .line 107423
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->X(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107424
    const/4 v2, 0x0

    .line 107425
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    invoke-interface {v0}, LX/0iO;->e()Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;

    move-result-object v0

    .line 107426
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->getStartReason()LX/ArJ;

    move-result-object v0

    .line 107427
    :goto_0
    iget-boolean v4, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->av:Z

    if-eqz v4, :cond_2

    .line 107428
    :goto_1
    move-object v0, v2

    .line 107429
    invoke-direct {p0, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(LX/ArJ;)V

    .line 107430
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->av:Z

    .line 107431
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->I:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Av8;

    .line 107432
    iget-object v2, v0, LX/Av8;->a:LX/AvB;

    sget-object v4, LX/AvA;->REFRESH:LX/AvA;

    invoke-virtual {v2, v4}, LX/AvB;->a(LX/AvA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 107433
    iget-object v2, v0, LX/Av8;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AvE;

    invoke-virtual {v2, v4}, LX/AvE;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v4, LX/Av6;

    invoke-direct {v4, v0}, LX/Av6;-><init>(LX/Av8;)V

    iget-object p0, v0, LX/Av8;->c:Ljava/util/concurrent/Executor;

    invoke-static {v2, v4, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 107434
    const/16 v0, 0x2b

    const v2, 0x6f7643f5

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move-object v0, v2

    .line 107435
    goto :goto_0

    .line 107436
    :cond_2
    if-nez v0, :cond_3

    .line 107437
    sget-object v2, LX/ArJ;->FOREGROUND_APP:LX/ArJ;

    goto :goto_1

    :cond_3
    move-object v2, v0

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 107438
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 107439
    const-string v0, "did_launch_to_sharesheet"

    iget-boolean v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->av:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107440
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->al:LX/0Px;

    if-eqz v0, :cond_0

    .line 107441
    const-string v0, "processed_composer_attachment"

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->al:LX/0Px;

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 107442
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 107443
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab:LX/Aud;

    if-eqz v1, :cond_1

    .line 107444
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab:LX/Aud;

    invoke-virtual {v1, v0}, LX/Aud;->a(LX/0jL;)LX/0jL;

    .line 107445
    :cond_1
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa:LX/Auw;

    if-eqz v1, :cond_2

    .line 107446
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa:LX/Auw;

    invoke-virtual {v1, v0}, LX/Auw;->a(LX/0jL;)V

    .line 107447
    :cond_2
    invoke-virtual {v0}, LX/0jL;->a()V

    .line 107448
    return-void
.end method
