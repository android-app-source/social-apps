.class public Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/1Vu;

.field private final e:LX/1Vv;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D0e;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D0r;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Vu;LX/1Vv;LX/0Ot;LX/0Ot;LX/1V0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1Vu;",
            "LX/1Vv;",
            "LX/0Ot",
            "<",
            "LX/D0e;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D0r;",
            ">;",
            "LX/1V0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266741
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 266742
    iput-object p2, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->d:LX/1Vu;

    .line 266743
    iput-object p3, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->e:LX/1Vv;

    .line 266744
    iput-object p4, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->f:LX/0Ot;

    .line 266745
    iput-object p5, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->g:LX/0Ot;

    .line 266746
    iput-object p6, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->h:LX/1V0;

    .line 266747
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 266767
    iget-object v0, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->d:LX/1Vu;

    invoke-virtual {v0, p2}, LX/1Vu;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    move-result-object v0

    .line 266768
    sget-object v1, LX/D0t;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 266769
    const/4 v0, 0x0

    .line 266770
    :goto_0
    return-object v0

    .line 266771
    :pswitch_0
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->b(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    .line 266772
    :goto_1
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 266773
    iget-object v2, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->h:LX/1V0;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 266774
    :pswitch_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED_AND_ALREADY_APPEALED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_2
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;Z)LX/1X1;

    move-result-object v0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;Z)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Pf;",
            "Z)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 266748
    iget-object v0, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D0e;

    const/4 v1, 0x0

    .line 266749
    new-instance v2, LX/D0d;

    invoke-direct {v2, v0}, LX/D0d;-><init>(LX/D0e;)V

    .line 266750
    iget-object p0, v0, LX/D0e;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/D0c;

    .line 266751
    if-nez p0, :cond_0

    .line 266752
    new-instance p0, LX/D0c;

    invoke-direct {p0, v0}, LX/D0c;-><init>(LX/D0e;)V

    .line 266753
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/D0c;->a$redex0(LX/D0c;LX/1De;IILX/D0d;)V

    .line 266754
    move-object v2, p0

    .line 266755
    move-object v1, v2

    .line 266756
    move-object v0, v1

    .line 266757
    iget-object v1, v0, LX/D0c;->a:LX/D0d;

    iput-object p3, v1, LX/D0d;->b:LX/1Pn;

    .line 266758
    iget-object v1, v0, LX/D0c;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266759
    move-object v0, v0

    .line 266760
    iget-object v1, v0, LX/D0c;->a:LX/D0d;

    iput-object p2, v1, LX/D0d;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266761
    iget-object v1, v0, LX/D0c;->e:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266762
    move-object v0, v0

    .line 266763
    iget-object v1, v0, LX/D0c;->a:LX/D0d;

    iput-boolean p4, v1, LX/D0d;->a:Z

    .line 266764
    iget-object v1, v0, LX/D0c;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266765
    move-object v0, v0

    .line 266766
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;
    .locals 10

    .prologue
    .line 266730
    const-class v1, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

    monitor-enter v1

    .line 266731
    :try_start_0
    sget-object v0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266732
    sput-object v2, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266733
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266734
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266735
    new-instance v3, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1Vu;->a(LX/0QB;)LX/1Vu;

    move-result-object v5

    check-cast v5, LX/1Vu;

    invoke-static {v0}, LX/1Vv;->b(LX/0QB;)LX/1Vv;

    move-result-object v6

    check-cast v6, LX/1Vv;

    const/16 v7, 0x3568

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x356e

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v9

    check-cast v9, LX/1V0;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1Vu;LX/1Vv;LX/0Ot;LX/0Ot;LX/1V0;)V

    .line 266736
    move-object v0, v3

    .line 266737
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266738
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266739
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266740
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pf;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1dV;",
            "LX/1Pf;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 266725
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 266726
    iget-object v1, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->e:LX/1Vv;

    const-string v2, "dti_header_shown"

    .line 266727
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266728
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/1Vv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 266729
    return-void
.end method

.method private b(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 266775
    iget-object v0, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D0r;

    const/4 v1, 0x0

    .line 266776
    new-instance v2, LX/D0q;

    invoke-direct {v2, v0}, LX/D0q;-><init>(LX/D0r;)V

    .line 266777
    iget-object p0, v0, LX/D0r;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/D0p;

    .line 266778
    if-nez p0, :cond_0

    .line 266779
    new-instance p0, LX/D0p;

    invoke-direct {p0, v0}, LX/D0p;-><init>(LX/D0r;)V

    .line 266780
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/D0p;->a$redex0(LX/D0p;LX/1De;IILX/D0q;)V

    .line 266781
    move-object v2, p0

    .line 266782
    move-object v1, v2

    .line 266783
    move-object v0, v1

    .line 266784
    iget-object v1, v0, LX/D0p;->a:LX/D0q;

    iput-object p3, v1, LX/D0q;->b:LX/1Pq;

    .line 266785
    iget-object v1, v0, LX/D0p;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266786
    move-object v0, v0

    .line 266787
    iget-object v1, v0, LX/D0p;->a:LX/D0q;

    iput-object p2, v1, LX/D0q;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266788
    iget-object v1, v0, LX/D0p;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266789
    move-object v0, v0

    .line 266790
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 266724
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 266723
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 266713
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pf;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7fd89767

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 266722
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pf;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pf;Lcom/facebook/components/feed/FeedComponentView;)V

    const/16 v1, 0x1f

    const v2, 0x5d95fcdf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 266714
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266715
    iget-object v0, p0, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->d:LX/1Vu;

    const/4 v1, 0x0

    .line 266716
    iget-object v2, v0, LX/1Vu;->b:LX/0Uh;

    const/16 v3, 0x60

    const/4 p0, 0x0

    invoke-virtual {v2, v3, p0}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v2, v2

    .line 266717
    if-nez v2, :cond_0

    .line 266718
    :goto_0
    move v0, v1

    .line 266719
    return v0

    .line 266720
    :cond_0
    sget-object v2, LX/1Vz;->a:[I

    invoke-virtual {v0, p1}, LX/1Vu;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 266721
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
