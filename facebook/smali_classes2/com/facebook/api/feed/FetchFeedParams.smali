.class public Lcom/facebook/api/feed/FetchFeedParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/FetchFeedParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0rS;

.field public final b:Lcom/facebook/api/feedtype/FeedType;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public f:LX/0gf;

.field public final g:Ljava/lang/String;

.field public final h:Lcom/facebook/api/feed/FeedFetchContext;

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/feed/Vpv;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Lcom/facebook/common/callercontext/CallerContext;

.field public final l:Z

.field public final m:Z

.field public n:J

.field public o:J

.field public p:Z

.field public q:Lcom/facebook/http/interfaces/RequestPriority;

.field public final r:Z

.field public final s:LX/0rU;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150205
    new-instance v0, LX/0ri;

    invoke-direct {v0}, LX/0ri;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/FetchFeedParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0rT;)V
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 150092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150093
    iput-wide v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->n:J

    .line 150094
    iput-wide v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    .line 150095
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    .line 150096
    iget-object v0, p1, LX/0rT;->a:LX/0rS;

    move-object v0, v0

    .line 150097
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    .line 150098
    iget-object v0, p1, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v0

    .line 150099
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 150100
    iget v0, p1, LX/0rT;->c:I

    move v0, v0

    .line 150101
    iput v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    .line 150102
    iget-object v0, p1, LX/0rT;->g:Ljava/lang/String;

    move-object v0, v0

    .line 150103
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    .line 150104
    iget-object v0, p1, LX/0rT;->f:Ljava/lang/String;

    move-object v0, v0

    .line 150105
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    .line 150106
    iget-object v0, p1, LX/0rT;->n:LX/0Px;

    move-object v0, v0

    .line 150107
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    .line 150108
    iget-object v0, p1, LX/0rT;->o:LX/0Px;

    move-object v0, v0

    .line 150109
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    .line 150110
    iget-object v0, p1, LX/0rT;->i:LX/0gf;

    move-object v0, v0

    .line 150111
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    .line 150112
    iget-boolean v0, p1, LX/0rT;->j:Z

    move v0, v0

    .line 150113
    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    .line 150114
    iget-object v0, p1, LX/0rT;->h:Ljava/lang/String;

    move-object v0, v0

    .line 150115
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    .line 150116
    iget-object v0, p1, LX/0rT;->k:LX/0rU;

    move-object v0, v0

    .line 150117
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    .line 150118
    iget-object v0, p1, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    move-object v0, v0

    .line 150119
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->h:Lcom/facebook/api/feed/FeedFetchContext;

    .line 150120
    iget-object v0, p1, LX/0rT;->m:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 150121
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 150122
    iget-boolean v0, p1, LX/0rT;->p:Z

    move v0, v0

    .line 150123
    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->l:Z

    .line 150124
    iget-boolean v0, p1, LX/0rT;->q:Z

    move v0, v0

    .line 150125
    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->m:Z

    .line 150126
    iget-wide v2, p1, LX/0rT;->d:J

    move-wide v0, v2

    .line 150127
    iput-wide v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->n:J

    .line 150128
    iget-wide v2, p1, LX/0rT;->e:J

    move-wide v0, v2

    .line 150129
    iput-wide v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    .line 150130
    iget-boolean v0, p1, LX/0rT;->r:Z

    move v0, v0

    .line 150131
    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    .line 150132
    iget-object v0, p1, LX/0rT;->s:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, v0

    .line 150133
    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    .line 150134
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 150176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150177
    iput-wide v4, p0, Lcom/facebook/api/feed/FetchFeedParams;->n:J

    .line 150178
    iput-wide v4, p0, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    .line 150179
    iput-boolean v1, p0, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    .line 150180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    .line 150181
    const-class v0, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedtype/FeedType;

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 150182
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    .line 150183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    .line 150184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    .line 150185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0gf;->valueOf(Ljava/lang/String;)LX/0gf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    .line 150186
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    .line 150187
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    .line 150188
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rU;->valueOf(Ljava/lang/String;)LX/0rU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    .line 150189
    const-class v0, Lcom/facebook/api/feed/FeedFetchContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FeedFetchContext;

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->h:Lcom/facebook/api/feed/FeedFetchContext;

    .line 150190
    const-class v0, Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 150191
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    .line 150192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 150193
    const-class v3, Lcom/facebook/api/feed/Vpv;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 150194
    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    .line 150195
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->l:Z

    .line 150196
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->m:Z

    .line 150197
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    .line 150198
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    .line 150199
    const-class v0, Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/interfaces/RequestPriority;

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    .line 150200
    return-void

    :cond_0
    move v0, v2

    .line 150201
    goto :goto_0

    :cond_1
    move v0, v2

    .line 150202
    goto :goto_1

    :cond_2
    move v0, v2

    .line 150203
    goto :goto_2

    :cond_3
    move v1, v2

    .line 150204
    goto :goto_3
.end method

.method public static newBuilder()LX/0rT;
    .locals 1

    .prologue
    .line 150175
    new-instance v0, LX/0rT;

    invoke-direct {v0}, LX/0rT;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150174
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 150173
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150172
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 150206
    instance-of v1, p1, Lcom/facebook/api/feed/FetchFeedParams;

    if-eqz v1, :cond_0

    .line 150207
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 150208
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v1, v1

    .line 150209
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v2, v2

    .line 150210
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150211
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 150212
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 150213
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150214
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 150215
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 150216
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150217
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v1, v1

    .line 150218
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 150219
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150220
    iget v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v1, v1

    .line 150221
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 150222
    iget v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v2, v2

    .line 150223
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150224
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v1, v1

    .line 150225
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v2, v2

    .line 150226
    if-ne v1, v2, :cond_0

    .line 150227
    iget-boolean v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    move v1, v1

    .line 150228
    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    move v2, v2

    .line 150229
    if-ne v1, v2, :cond_0

    .line 150230
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v1, v1

    .line 150231
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v2, v2

    .line 150232
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150233
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v1, v1

    .line 150234
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v2, v2

    .line 150235
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150236
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->h:Lcom/facebook/api/feed/FeedFetchContext;

    move-object v1, v1

    .line 150237
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->h:Lcom/facebook/api/feed/FeedFetchContext;

    move-object v2, v2

    .line 150238
    invoke-virtual {v1, v2}, Lcom/facebook/api/feed/FeedFetchContext;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150239
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->k:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v1

    .line 150240
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->k:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 150241
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150242
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    move-object v1, v1

    .line 150243
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    move-object v2, v2

    .line 150244
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150245
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    move-object v1, v1

    .line 150246
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    move-object v2, v2

    .line 150247
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150248
    iget-boolean v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->l:Z

    move v1, v1

    .line 150249
    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->l:Z

    move v2, v2

    .line 150250
    if-ne v1, v2, :cond_0

    .line 150251
    iget-boolean v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->m:Z

    move v1, v1

    .line 150252
    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->m:Z

    move v2, v2

    .line 150253
    if-ne v1, v2, :cond_0

    .line 150254
    iget-wide v4, p1, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    move-wide v2, v4

    .line 150255
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 150256
    iget-wide v4, p0, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    move-wide v2, v4

    .line 150257
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150258
    iget-boolean v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    move v1, v1

    .line 150259
    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    move v2, v2

    .line 150260
    if-ne v1, v2, :cond_0

    .line 150261
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v1, v1

    .line 150262
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v2, v2

    .line 150263
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 150264
    :cond_0
    return v0
.end method

.method public final g()Lcom/facebook/api/feedtype/FeedType;
    .locals 1

    .prologue
    .line 150171
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    return-object v0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 150138
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 150139
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v2, v2

    .line 150140
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 150141
    iget v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v2, v2

    .line 150142
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 150143
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 150144
    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 150145
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 150146
    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 150147
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 150148
    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 150149
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v2, v2

    .line 150150
    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 150151
    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    move v2, v2

    .line 150152
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 150153
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v2, v2

    .line 150154
    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 150155
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v2, v2

    .line 150156
    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 150157
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    move-object v2, v2

    .line 150158
    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 150159
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    move-object v2, v2

    .line 150160
    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 150161
    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->l:Z

    move v2, v2

    .line 150162
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 150163
    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->m:Z

    move v2, v2

    .line 150164
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 150165
    iget-wide v4, p0, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    move-wide v2, v4

    .line 150166
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 150167
    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    move v2, v2

    .line 150168
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 150169
    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v2, v2

    .line 150170
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()LX/0gf;
    .locals 1

    .prologue
    .line 150137
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    return-object v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 150136
    iget-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->m:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 150135
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "Freshness"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    invoke-virtual {v2}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "Type"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "FirstItems"

    iget v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "Before"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "After"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "FetchFeedCause"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    invoke-virtual {v2}, LX/0gf;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "PreferChunked"

    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "FetchTypeForLogging"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    invoke-virtual {v2}, LX/0rU;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "ClientQueryID"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "ViewContext"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->h:Lcom/facebook/api/feed/FeedFetchContext;

    invoke-virtual {v2}, Lcom/facebook/api/feed/FeedFetchContext;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "CallerContext"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "RecentVpvs"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "RecentVpvsV2"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "NoSkipping"

    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->l:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "AllowGaps"

    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->m:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "MaxStoryStalenessTime"

    iget-wide v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "AllowPinnedDummyStories"

    iget-boolean v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "RequestPriority"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 150069
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150070
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 150071
    iget v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 150072
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150073
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150074
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    invoke-virtual {v0}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150075
    iget-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 150076
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150077
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    invoke-virtual {v0}, LX/0rU;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150078
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->h:Lcom/facebook/api/feed/FeedFetchContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 150079
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 150080
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 150081
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 150082
    iget-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->l:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 150083
    iget-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->m:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 150084
    iget-wide v4, p0, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 150085
    iget-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 150086
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 150087
    return-void

    :cond_0
    move v0, v2

    .line 150088
    goto :goto_0

    :cond_1
    move v0, v2

    .line 150089
    goto :goto_1

    :cond_2
    move v0, v2

    .line 150090
    goto :goto_2

    :cond_3
    move v1, v2

    .line 150091
    goto :goto_3
.end method
