.class public Lcom/facebook/api/feed/FeedFetchContext;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/FeedFetchContext;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Lcom/facebook/api/feed/FeedFetchContext;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149690
    new-instance v0, Lcom/facebook/api/feed/FeedFetchContext;

    invoke-direct {v0, v1, v1}, Lcom/facebook/api/feed/FeedFetchContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/api/feed/FeedFetchContext;->a:Lcom/facebook/api/feed/FeedFetchContext;

    .line 149691
    new-instance v0, LX/0rV;

    invoke-direct {v0}, LX/0rV;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/FeedFetchContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 149686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149687
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FeedFetchContext;->b:Ljava/lang/String;

    .line 149688
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/FeedFetchContext;->c:Ljava/lang/String;

    .line 149689
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149683
    iput-object p1, p0, Lcom/facebook/api/feed/FeedFetchContext;->b:Ljava/lang/String;

    .line 149684
    iput-object p2, p0, Lcom/facebook/api/feed/FeedFetchContext;->c:Ljava/lang/String;

    .line 149685
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 149659
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 149673
    instance-of v1, p1, Lcom/facebook/api/feed/FeedFetchContext;

    if-eqz v1, :cond_0

    .line 149674
    check-cast p1, Lcom/facebook/api/feed/FeedFetchContext;

    .line 149675
    iget-object v1, p1, Lcom/facebook/api/feed/FeedFetchContext;->b:Ljava/lang/String;

    move-object v1, v1

    .line 149676
    iget-object v2, p0, Lcom/facebook/api/feed/FeedFetchContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 149677
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149678
    iget-object v1, p1, Lcom/facebook/api/feed/FeedFetchContext;->c:Ljava/lang/String;

    move-object v1, v1

    .line 149679
    iget-object v2, p0, Lcom/facebook/api/feed/FeedFetchContext;->c:Ljava/lang/String;

    move-object v2, v2

    .line 149680
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 149681
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 149668
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 149669
    iget-object v2, p0, Lcom/facebook/api/feed/FeedFetchContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 149670
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 149671
    iget-object v2, p0, Lcom/facebook/api/feed/FeedFetchContext;->c:Ljava/lang/String;

    move-object v2, v2

    .line 149672
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 149663
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "toId"

    .line 149664
    iget-object v2, p0, Lcom/facebook/api/feed/FeedFetchContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 149665
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "viaId"

    .line 149666
    iget-object v2, p0, Lcom/facebook/api/feed/FeedFetchContext;->c:Ljava/lang/String;

    move-object v2, v2

    .line 149667
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 149660
    iget-object v0, p0, Lcom/facebook/api/feed/FeedFetchContext;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 149661
    iget-object v0, p0, Lcom/facebook/api/feed/FeedFetchContext;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 149662
    return-void
.end method
