.class public Lcom/facebook/api/feed/FetchFeedResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/FetchFeedResult;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;


# instance fields
.field public final a:Lcom/facebook/api/feed/FetchFeedParams;

.field public final b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

.field public final c:Z

.field public final d:LX/0qw;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 197372
    new-instance v0, LX/0uq;

    invoke-direct {v0}, LX/0uq;-><init>()V

    invoke-static {v2, v2, v1, v1}, LX/16z;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    .line 197373
    iput-object v1, v0, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 197374
    move-object v0, v0

    .line 197375
    invoke-virtual {v0}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    sput-object v0, Lcom/facebook/api/feed/FetchFeedResult;->e:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 197376
    new-instance v0, LX/17M;

    invoke-direct {v0}, LX/17M;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/FetchFeedResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 197365
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 197366
    const-class v0, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedParams;

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 197367
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 197368
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    .line 197369
    sget-object v0, LX/0qw;->FULL:LX/0qw;

    iput-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->d:LX/0qw;

    .line 197370
    return-void

    .line 197371
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/api/feed/FetchFeedParams;LX/0Px;Lcom/facebook/graphql/model/GraphQLPageInfo;Ljava/lang/String;LX/0ta;JZ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedParams;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            "Ljava/lang/String;",
            "LX/0ta;",
            "JZ)V"
        }
    .end annotation

    .prologue
    .line 197288
    new-instance v0, LX/0uq;

    invoke-direct {v0}, LX/0uq;-><init>()V

    invoke-virtual {v0, p2}, LX/0uq;->a(LX/0Px;)LX/0uq;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0uq;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/0uq;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/0uq;->a(Ljava/lang/String;)LX/0uq;

    move-result-object v0

    invoke-virtual {v0}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p5

    move-wide v4, p6

    move/from16 v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    .line 197289
    return-void
.end method

.method public constructor <init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V
    .locals 8

    .prologue
    .line 197363
    sget-object v7, LX/0qw;->FULL:LX/0qw;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZLX/0qw;)V

    .line 197364
    return-void
.end method

.method public constructor <init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZLX/0qw;)V
    .locals 0

    .prologue
    .line 197357
    invoke-direct {p0, p3, p4, p5}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 197358
    iput-object p1, p0, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 197359
    iput-object p2, p0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 197360
    iput-boolean p6, p0, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    .line 197361
    iput-object p7, p0, Lcom/facebook/api/feed/FetchFeedResult;->d:LX/0qw;

    .line 197362
    return-void
.end method

.method public static a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 7

    .prologue
    .line 197356
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    sget-object v2, Lcom/facebook/api/feed/FetchFeedResult;->e:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    sget-object v3, LX/0ta;->NO_DATA:LX/0ta;

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 9

    .prologue
    .line 197307
    const-string v0, "FetchFeedResult.mergeContinuous received null chunk"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197308
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    iget-object v1, p0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    iget-object v2, p2, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 197309
    const-string v3, "FetchFeedResult.mergeContiguousHomeFeedStories received null chunk"

    invoke-static {v2, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197310
    new-instance v3, LX/0uq;

    invoke-direct {v3}, LX/0uq;-><init>()V

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 197311
    iput-object v4, v3, LX/0uq;->d:LX/0Px;

    .line 197312
    move-object v3, v3

    .line 197313
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v5

    .line 197314
    const-string v6, "GraphQLPageInfo.mergeContinuous received null PageInfo"

    invoke-static {v5, v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197315
    invoke-static {v4}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 197316
    :goto_0
    move-object v4, v5

    .line 197317
    iput-object v4, v3, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 197318
    move-object v3, v3

    .line 197319
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j()Ljava/lang/String;

    move-result-object v4

    .line 197320
    iput-object v4, v3, LX/0uq;->c:Ljava/lang/String;

    .line 197321
    move-object v3, v3

    .line 197322
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->a()I

    move-result v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->a()I

    move-result v5

    add-int/2addr v4, v5

    .line 197323
    iput v4, v3, LX/0uq;->b:I

    .line 197324
    move-object v3, v3

    .line 197325
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->p()Ljava/lang/String;

    move-result-object v4

    .line 197326
    iput-object v4, v3, LX/0uq;->i:Ljava/lang/String;

    .line 197327
    move-object v3, v3

    .line 197328
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->q()Ljava/lang/String;

    move-result-object v4

    .line 197329
    iput-object v4, v3, LX/0uq;->j:Ljava/lang/String;

    .line 197330
    move-object v3, v3

    .line 197331
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->l()LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->l()LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 197332
    iput-object v4, v3, LX/0uq;->e:LX/0Px;

    .line 197333
    move-object v3, v3

    .line 197334
    invoke-virtual {v3}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v3

    move-object v2, v3

    .line 197335
    iget-object v1, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v1

    .line 197336
    iget-wide v7, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v7

    .line 197337
    iget-boolean v1, p0, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v1, v1

    .line 197338
    if-eqz v1, :cond_0

    .line 197339
    iget-boolean v1, p2, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v1, v1

    .line 197340
    if-eqz v1, :cond_0

    const/4 v6, 0x1

    :goto_1
    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    return-object v0

    :cond_0
    const/4 v6, 0x0

    goto :goto_1

    .line 197341
    :cond_1
    invoke-static {v5}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z

    move-result v6

    if-nez v6, :cond_2

    move-object v5, v4

    .line 197342
    goto :goto_0

    .line 197343
    :cond_2
    new-instance v6, LX/17L;

    invoke-direct {v6}, LX/17L;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPageInfo;->p_()Ljava/lang/String;

    move-result-object v7

    .line 197344
    iput-object v7, v6, LX/17L;->f:Ljava/lang/String;

    .line 197345
    move-object v6, v6

    .line 197346
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v7

    .line 197347
    iput-boolean v7, v6, LX/17L;->e:Z

    .line 197348
    move-object v6, v6

    .line 197349
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v7

    .line 197350
    iput-object v7, v6, LX/17L;->c:Ljava/lang/String;

    .line 197351
    move-object v6, v6

    .line 197352
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v7

    .line 197353
    iput-boolean v7, v6, LX/17L;->d:Z

    .line 197354
    move-object v6, v6

    .line 197355
    invoke-virtual {v6}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v5

    goto/16 :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 1

    .prologue
    .line 197306
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197305
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1

    .prologue
    .line 197304
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197303
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 197302
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/api/feed/FetchFeedParams;
    .locals 1

    .prologue
    .line 197301
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 197296
    const-class v0, Lcom/facebook/api/feed/FetchFeedResult;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "params"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "result"

    iget-object v2, p0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "clientTimeMs"

    .line 197297
    iget-wide v4, p0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v4

    .line 197298
    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "freshness"

    .line 197299
    iget-object v2, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 197300
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 197290
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 197291
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 197292
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 197293
    iget-boolean v0, p0, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 197294
    return-void

    .line 197295
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
