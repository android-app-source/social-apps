.class public final Lcom/facebook/api/prefetch/GraphQLPrefetchController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static r:LX/0Xm;


# instance fields
.field public final c:LX/0tX;

.field public final d:Ljava/util/concurrent/ScheduledExecutorService;

.field public final e:LX/0si;

.field public final f:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "LX/0uk;",
            "Ljava/util/concurrent/ConcurrentSkipListSet",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "LX/1w7;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0uo;

.field public final i:LX/0uo;

.field private final j:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "LX/0uk;",
            "LX/1Ep;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0un;

.field private final l:LX/0oz;

.field private final m:Ljava/lang/Runnable;

.field private n:Ljava/util/concurrent/ScheduledFuture;

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Uh;

.field private final q:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 156936
    const-class v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a:Ljava/lang/String;

    .line 156937
    const-class v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ScheduledExecutorService;LX/0un;LX/0si;LX/0oz;LX/0Ot;LX/0Uh;LX/0ad;)V
    .locals 3
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0un;",
            "LX/0si;",
            "LX/0oz;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x2

    .line 156994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156995
    iput-object p1, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c:LX/0tX;

    .line 156996
    iput-object p2, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 156997
    iput-object p3, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->k:LX/0un;

    .line 156998
    iput-object p4, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->e:LX/0si;

    .line 156999
    iput-object p5, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->l:LX/0oz;

    .line 157000
    iput-object p6, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->o:LX/0Ot;

    .line 157001
    iput-object p7, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->p:LX/0Uh;

    .line 157002
    iput-object p8, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->q:LX/0ad;

    .line 157003
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->n:Ljava/util/concurrent/ScheduledFuture;

    .line 157004
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0, v1}, LX/0S8;->c(I)LX/0S8;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0S8;->a(I)LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->f:Ljava/util/concurrent/ConcurrentMap;

    .line 157005
    new-instance v0, LX/0uo;

    invoke-direct {v0}, LX/0uo;-><init>()V

    iput-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->h:LX/0uo;

    .line 157006
    new-instance v0, LX/0uo;

    invoke-direct {v0}, LX/0uo;-><init>()V

    iput-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->i:LX/0uo;

    .line 157007
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0, v1}, LX/0S8;->c(I)LX/0S8;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0S8;->a(I)LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->j:Ljava/util/concurrent/ConcurrentMap;

    .line 157008
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 157009
    new-instance v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController$1;

    invoke-direct {v0, p0}, Lcom/facebook/api/prefetch/GraphQLPrefetchController$1;-><init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;)V

    iput-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->m:Ljava/lang/Runnable;

    .line 157010
    return-void
.end method

.method private static a(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;ZLX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Z)LX/0TF;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0uk",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;Z",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Z)",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 157011
    new-instance v0, LX/6BC;

    move-object v1, p0

    move-object v2, p1

    move/from16 v3, p9

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p7

    invoke-direct/range {v0 .. v10}, LX/6BC;-><init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;ZLjava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;ZLX/0TF;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method private static a(LX/0uk;Ljava/lang/String;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0uk",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/0zS;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 157012
    invoke-virtual {p0, p1}, LX/0uk;->b(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    .line 157013
    if-nez v0, :cond_1

    .line 157014
    :cond_0
    :goto_0
    return-object v0

    .line 157015
    :cond_1
    invoke-virtual {p0}, LX/0uk;->c()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    .line 157016
    if-eqz p2, :cond_2

    .line 157017
    invoke-virtual {v0, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 157018
    :cond_2
    if-eqz p3, :cond_0

    .line 157019
    iput-object p3, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 157020
    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/api/prefetch/GraphQLPrefetchController;
    .locals 12

    .prologue
    .line 157021
    const-class v1, Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    monitor-enter v1

    .line 157022
    :try_start_0
    sget-object v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->r:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 157023
    sput-object v2, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->r:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 157024
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157025
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 157026
    new-instance v3, Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ScheduledExecutorService;

    const-class v6, LX/0un;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/0un;

    invoke-static {v0}, LX/0sh;->a(LX/0QB;)LX/0sh;

    move-result-object v7

    check-cast v7, LX/0si;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v8

    check-cast v8, LX/0oz;

    const/16 v9, 0x2d2

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;-><init>(LX/0tX;Ljava/util/concurrent/ScheduledExecutorService;LX/0un;LX/0si;LX/0oz;LX/0Ot;LX/0Uh;LX/0ad;)V

    .line 157027
    move-object v0, v3

    .line 157028
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 157029
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157030
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 157031
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/api/prefetch/GraphQLPrefetchController;Ljava/util/concurrent/Executor;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 157032
    invoke-interface {p2}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 157033
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->q:Z

    move v0, v0

    .line 157034
    if-nez v0, :cond_1

    .line 157035
    invoke-interface {p2}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157036
    invoke-static {p2}, LX/0Vg;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p3, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 157037
    :cond_0
    :goto_0
    return-void

    .line 157038
    :cond_1
    invoke-static {p2, p3, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;LX/0zS;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0uk",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/concurrent/Executor;",
            "LX/0zS;",
            ")V"
        }
    .end annotation

    .prologue
    .line 157039
    invoke-static {p1, p2, p6, p4}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c(LX/0uk;Ljava/lang/String;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    .line 157040
    iget-object v1, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 157041
    iget-object v1, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->h:LX/0uo;

    invoke-virtual {v1, p2, p1, v0}, LX/0uo;->a(Ljava/lang/String;LX/0uk;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 157042
    const/4 v1, 0x1

    .line 157043
    new-instance p0, LX/6BD;

    invoke-direct {p0, v1}, LX/6BD;-><init>(Z)V

    invoke-static {v0, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v0, p0

    .line 157044
    invoke-static {v0, p3, p5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 157045
    return-void
.end method

.method public static b(LX/0uk;Ljava/lang/String;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0uk",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/0zS;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 157046
    invoke-virtual {p0, p1}, LX/0uk;->a(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    .line 157047
    invoke-virtual {p0}, LX/0uk;->c()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    .line 157048
    if-eqz p2, :cond_0

    .line 157049
    invoke-virtual {v0, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 157050
    :cond_0
    if-eqz p3, :cond_1

    .line 157051
    iput-object p3, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 157052
    :cond_1
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/executor/GraphQLResult;Z)LX/6BE;
    .locals 2
    .param p0    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;Z)",
            "LX/6BE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 156988
    if-nez p0, :cond_0

    .line 156989
    new-instance v0, LX/6BE;

    const/4 v1, 0x0

    const/4 p0, 0x1

    invoke-direct {v0, v1, p1, p0}, LX/6BE;-><init>(Ljava/lang/Object;ZZ)V

    move-object v0, v0

    .line 156990
    :goto_0
    return-object v0

    .line 156991
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 156992
    new-instance v1, LX/6BE;

    const/4 p0, 0x0

    invoke-direct {v1, v0, p1, p0}, LX/6BE;-><init>(Ljava/lang/Object;ZZ)V

    move-object v0, v1

    .line 156993
    goto :goto_0
.end method

.method public static b(Lcom/facebook/api/prefetch/GraphQLPrefetchController;)V
    .locals 5

    .prologue
    .line 157053
    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->n:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->n:Ljava/util/concurrent/ScheduledFuture;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 157054
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->d:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->n:Ljava/util/concurrent/ScheduledFuture;

    .line 157055
    :cond_1
    return-void
.end method

.method public static b(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 156980
    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->j:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ep;

    .line 156981
    if-nez v0, :cond_0

    .line 156982
    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->k:LX/0un;

    invoke-virtual {p1}, LX/0uk;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p2}, LX/0uk;->a(Ljava/lang/String;)LX/0zO;

    move-result-object v2

    .line 156983
    iget-object p2, v2, LX/0zO;->m:LX/0gW;

    move-object v2, p2

    .line 156984
    iget-object p2, v2, LX/0gW;->f:Ljava/lang/String;

    move-object v2, p2

    .line 156985
    invoke-virtual {v0, v1, v2}, LX/0un;->a(Ljava/lang/String;Ljava/lang/String;)LX/1Ep;

    move-result-object v0

    .line 156986
    iget-object v1, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->j:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156987
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0uk",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/concurrent/Executor;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 156972
    sget-object v1, LX/0zS;->b:LX/0zS;

    move-object/from16 v0, p4

    invoke-static {p1, p2, v1, v0}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0uk;Ljava/lang/String;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v1

    .line 156973
    if-nez v1, :cond_0

    .line 156974
    const/4 v1, 0x0

    .line 156975
    :goto_0
    return v1

    .line 156976
    :cond_0
    iget-object v2, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v11

    .line 156977
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v7, p4

    move-object/from16 v9, p5

    invoke-static/range {v1 .. v10}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;ZLX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Z)LX/0TF;

    move-result-object v1

    .line 156978
    move-object/from16 v0, p5

    invoke-static {p0, v0, v11, v1}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(Lcom/facebook/api/prefetch/GraphQLPrefetchController;Ljava/util/concurrent/Executor;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 156979
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static c(LX/0uk;Ljava/lang/String;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0uk",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/0zS;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 156969
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(LX/0uk;Ljava/lang/String;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    .line 156970
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    .line 156971
    return-object v0
.end method

.method public static c(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0uk",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 156964
    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->i:LX/0uo;

    invoke-virtual {v0, p2, p1}, LX/0uo;->a(Ljava/lang/String;LX/0uk;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 156965
    if-eqz v7, :cond_0

    .line 156966
    new-instance v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController$4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/prefetch/GraphQLPrefetchController$4;-><init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V

    invoke-interface {v7, v0, p5}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 156967
    :goto_0
    return-void

    .line 156968
    :cond_0
    sget-object v6, LX/0zS;->d:LX/0zS;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a$redex0(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;LX/0zS;)V

    goto :goto_0
.end method

.method public static e(LX/0uk;)I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 156963
    invoke-virtual {p0}, LX/0uk;->f()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/0uk;)LX/1Ep;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 156962
    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->j:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ep;

    return-object v0
.end method

.method public final a(LX/0uk;Ljava/lang/String;LX/0TF;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0uk",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 156945
    if-nez p6, :cond_2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 156946
    :goto_0
    invoke-static/range {p0 .. p2}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;)V

    .line 156947
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->q:LX/0ad;

    sget-short v5, LX/0wn;->ao:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->q:LX/0ad;

    sget-short v5, LX/0wn;->ap:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    move v10, v4

    .line 156948
    :goto_1
    if-eqz v10, :cond_0

    .line 156949
    invoke-virtual/range {p1 .. p1}, LX/0uk;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    .line 156950
    invoke-static/range {v4 .. v9}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V

    .line 156951
    :cond_0
    sget-object v4, LX/0zS;->b:LX/0zS;

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p5

    invoke-static {v1, v2, v4, v3}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c(LX/0uk;Ljava/lang/String;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v4

    .line 156952
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v20

    .line 156953
    if-nez v10, :cond_1

    .line 156954
    invoke-virtual/range {p1 .. p1}, LX/0uk;->e()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    .line 156955
    invoke-static/range {v4 .. v9}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V

    .line 156956
    :cond_1
    invoke-virtual/range {p1 .. p1}, LX/0uk;->e()Z

    move-result v4

    if-nez v4, :cond_4

    const/4 v14, 0x1

    :goto_2
    const/16 v19, 0x0

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    move-object/from16 v17, v9

    move-object/from16 v18, p6

    invoke-static/range {v10 .. v19}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;ZLX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Z)LX/0TF;

    move-result-object v4

    .line 156957
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2, v4}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(Lcom/facebook/api/prefetch/GraphQLPrefetchController;Ljava/util/concurrent/Executor;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 156958
    return-void

    :cond_2
    move-object/from16 v9, p6

    .line 156959
    goto/16 :goto_0

    .line 156960
    :cond_3
    const/4 v4, 0x0

    move v10, v4

    goto :goto_1

    .line 156961
    :cond_4
    const/4 v14, 0x0

    goto :goto_2
.end method

.method public final a(LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0uk",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 156939
    invoke-static {p0, p1, p2}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;)V

    .line 156940
    sget-object v1, LX/0zS;->b:LX/0zS;

    move-object/from16 v0, p4

    invoke-static {p1, p2, v1, v0}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c(LX/0uk;Ljava/lang/String;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v1

    .line 156941
    iget-object v2, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v11

    .line 156942
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v7, p4

    move-object/from16 v9, p5

    invoke-static/range {v1 .. v10}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;Ljava/lang/String;LX/0TF;ZLX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Z)LX/0TF;

    move-result-object v1

    .line 156943
    move-object/from16 v0, p5

    invoke-static {p0, v0, v11, v1}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(Lcom/facebook/api/prefetch/GraphQLPrefetchController;Ljava/util/concurrent/Executor;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 156944
    return-void
.end method

.method public final a(LX/0uk;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 156938
    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->e:LX/0si;

    sget-object v1, LX/0zS;->b:LX/0zS;

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(LX/0uk;Ljava/lang/String;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0sj;->d(LX/0zO;)Z

    move-result v0

    return v0
.end method
