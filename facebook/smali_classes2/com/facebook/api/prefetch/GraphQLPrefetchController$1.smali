.class public final Lcom/facebook/api/prefetch/GraphQLPrefetchController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;


# direct methods
.method public constructor <init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;)V
    .locals 0

    .prologue
    .line 157100
    iput-object p1, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController$1;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 157101
    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController$1;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v0, v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 157102
    :goto_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ConcurrentSkipListSet;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentSkipListSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 157103
    iget-object v3, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController$1;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0uk;

    .line 157104
    invoke-static {v1}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->e(LX/0uk;)I

    move-result v6

    .line 157105
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 157106
    iget-object v4, v3, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ConcurrentSkipListSet;

    .line 157107
    :cond_1
    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentSkipListSet;->pollFirst()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 157108
    invoke-virtual {v1}, LX/0uk;->g()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v3, v1, v5}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0uk;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 157109
    :cond_2
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157110
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v5, v6, :cond_1

    .line 157111
    :cond_3
    move-object v4, v7

    .line 157112
    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 157113
    :goto_1
    goto :goto_0

    .line 157114
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController$1;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v0, v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1w7;

    if-eqz v0, :cond_6

    .line 157115
    iget-object v1, p0, Lcom/facebook/api/prefetch/GraphQLPrefetchController$1;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v2, v0, LX/1w7;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, v0, LX/1w7;->b:LX/0uk;

    iget-object v0, v0, LX/1w7;->c:Ljava/lang/Integer;

    .line 157116
    :try_start_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    .line 157117
    invoke-virtual {v3, v4}, LX/0uk;->a(Ljava/lang/String;)LX/0zO;

    move-result-object v5

    .line 157118
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 157119
    :cond_5
    :goto_3
    goto :goto_2

    .line 157120
    :cond_6
    return-void

    .line 157121
    :cond_7
    invoke-virtual {v3, v1}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0uk;)LX/1Ep;

    move-result-object v5

    .line 157122
    if-nez v4, :cond_a

    .line 157123
    :cond_8
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    .line 157124
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v6

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 157125
    new-instance v8, LX/0v6;

    invoke-virtual {v1}, LX/0uk;->d()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v8, v6}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 157126
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 157127
    sget-object v10, LX/0zS;->e:LX/0zS;

    sget-object v11, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v1, v6, v10, v11}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->b(LX/0uk;Ljava/lang/String;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v10

    .line 157128
    invoke-virtual {v8, v10}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 157129
    iget-object v11, v3, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->i:LX/0uo;

    invoke-virtual {v11, v6, v1, v10}, LX/0uo;->a(Ljava/lang/String;LX/0uk;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 157130
    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 157131
    :cond_9
    iget-object v6, v3, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->c:LX/0tX;

    invoke-virtual {v6, v8}, LX/0tX;->a(LX/0v6;)V

    .line 157132
    invoke-static {v7}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v6, v6

    .line 157133
    new-instance v7, LX/3U0;

    invoke-direct {v7, v3, v5, v4, v1}, LX/3U0;-><init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/1Ep;Ljava/util/Collection;LX/0uk;)V

    iget-object v4, v3, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->d:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v6, v7, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 157134
    :cond_a
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 157135
    if-nez v8, :cond_b

    .line 157136
    :goto_6
    goto :goto_5

    .line 157137
    :cond_b
    iget-object v10, v5, LX/1Ep;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v10, v8, v11}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 157138
    :pswitch_0
    :try_start_1
    new-instance v4, LX/1lO;

    invoke-direct {v4}, LX/1lO;-><init>()V

    .line 157139
    iput-object v2, v4, LX/1lO;->k:Ljava/lang/Object;

    .line 157140
    move-object v4, v4

    .line 157141
    invoke-virtual {v4}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v4

    .line 157142
    iget-object v6, v1, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->e:LX/0si;

    invoke-interface {v6, v5, v4}, LX/0sj;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto/16 :goto_3

    :catch_0
    goto/16 :goto_3

    .line 157143
    :pswitch_1
    iget-object v4, v1, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->e:LX/0si;

    invoke-interface {v4, v5}, LX/0sj;->b(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v4

    .line 157144
    if-eqz v4, :cond_5

    .line 157145
    iget-object v6, v4, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v6, v6

    .line 157146
    if-eqz v6, :cond_5

    .line 157147
    iget-object v6, v4, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v6

    .line 157148
    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v4}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    if-nez v4, :cond_5

    .line 157149
    iget-object v4, v1, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->e:LX/0si;

    invoke-interface {v4, v5}, LX/0si;->a(LX/0zO;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
