.class public Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/162;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:LX/21B;

.field public final f:LX/21C;

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public final i:LX/21D;

.field public final j:J

.field private k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 339562
    new-instance v0, LX/219;

    invoke-direct {v0}, LX/219;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 339560
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 339561
    return-void
.end method

.method private constructor <init>(LX/162;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 339538
    invoke-static {}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->newBuilder()LX/21A;

    move-result-object v0

    .line 339539
    iput-object p1, v0, LX/21A;->a:LX/162;

    .line 339540
    move-object v0, v0

    .line 339541
    iput-object p2, v0, LX/21A;->b:Ljava/lang/String;

    .line 339542
    move-object v0, v0

    .line 339543
    iput-object p3, v0, LX/21A;->c:Ljava/lang/String;

    .line 339544
    move-object v0, v0

    .line 339545
    iput-object p4, v0, LX/21A;->d:Ljava/lang/String;

    .line 339546
    move-object v0, v0

    .line 339547
    sget-object v1, LX/21B;->UNKNOWN:LX/21B;

    .line 339548
    iput-object v1, v0, LX/21A;->e:LX/21B;

    .line 339549
    move-object v0, v0

    .line 339550
    sget-object v1, LX/21C;->UNKNOWN:LX/21C;

    .line 339551
    iput-object v1, v0, LX/21A;->f:LX/21C;

    .line 339552
    move-object v0, v0

    .line 339553
    iput-boolean p5, v0, LX/21A;->h:Z

    .line 339554
    move-object v0, v0

    .line 339555
    sget-object v1, LX/21D;->INVALID:LX/21D;

    .line 339556
    iput-object v1, v0, LX/21A;->i:LX/21D;

    .line 339557
    move-object v0, v0

    .line 339558
    invoke-direct {p0, v0}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/21A;)V

    .line 339559
    return-void
.end method

.method public constructor <init>(LX/21A;)V
    .locals 2

    .prologue
    .line 339525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339526
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->k:LX/0Px;

    .line 339527
    iget-object v0, p1, LX/21A;->a:LX/162;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    .line 339528
    iget-object v0, p1, LX/21A;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    .line 339529
    iget-object v0, p1, LX/21A;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    .line 339530
    iget-object v0, p1, LX/21A;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->d:Ljava/lang/String;

    .line 339531
    iget-object v0, p1, LX/21A;->e:LX/21B;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->e:LX/21B;

    .line 339532
    iget-object v0, p1, LX/21A;->f:LX/21C;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f:LX/21C;

    .line 339533
    iget-object v0, p1, LX/21A;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->g:Ljava/lang/String;

    .line 339534
    iget-boolean v0, p1, LX/21A;->h:Z

    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->h:Z

    .line 339535
    iget-object v0, p1, LX/21A;->i:LX/21D;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->i:LX/21D;

    .line 339536
    iget-wide v0, p1, LX/21A;->j:J

    iput-wide v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    .line 339537
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 339451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339452
    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->k:LX/0Px;

    .line 339453
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 339454
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 339455
    :goto_0
    check-cast v0, LX/162;

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    .line 339456
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    .line 339457
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    .line 339458
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->d:Ljava/lang/String;

    .line 339459
    invoke-static {}, LX/21B;->values()[LX/21B;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->e:LX/21B;

    .line 339460
    invoke-static {}, LX/21C;->values()[LX/21C;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f:LX/21C;

    .line 339461
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->g:Ljava/lang/String;

    .line 339462
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->h:Z

    .line 339463
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 339464
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {}, LX/21D;->values()[LX/21D;

    move-result-object v3

    array-length v3, v3

    if-lt v1, v3, :cond_1

    .line 339465
    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 339466
    :cond_1
    invoke-static {}, LX/21D;->values()[LX/21D;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v0, v1, v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->i:LX/21D;

    .line 339467
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    .line 339468
    return-void

    .line 339469
    :catch_0
    move-exception v0

    .line 339470
    new-instance v1, Landroid/os/ParcelFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not parse parcel "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/28F;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 339471
    :catch_1
    move-exception v0

    .line 339472
    new-instance v1, Landroid/os/ParcelFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not parse parcel "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move v0, v2

    .line 339473
    goto :goto_1

    .line 339474
    :catch_2
    goto/16 :goto_0
.end method

.method public static newBuilder()LX/21A;
    .locals 1

    .prologue
    .line 339524
    new-instance v0, LX/21A;

    invoke-direct {v0}, LX/21A;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 339523
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 339515
    if-ne p0, p1, :cond_1

    .line 339516
    :cond_0
    :goto_0
    return v0

    .line 339517
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 339518
    :cond_3
    check-cast p1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 339519
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    iget-object v3, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 339520
    goto :goto_0

    .line 339521
    :cond_4
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 339522
    goto :goto_0
.end method

.method public final f()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 339506
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->k:LX/0Px;

    if-nez v0, :cond_2

    .line 339507
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 339508
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    if-eqz v0, :cond_0

    .line 339509
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 339510
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 339511
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 339512
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 339513
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->k:LX/0Px;

    .line 339514
    :cond_2
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->k:LX/0Px;

    return-object v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 339505
    iget-wide v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    return-wide v0
.end method

.method public final l()Ljava/lang/String;
    .locals 4

    .prologue
    .line 339491
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 339492
    const-string v0, "Nectar Module: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 339493
    iget-object v2, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 339494
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339495
    const-string v0, "Feedback Source: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 339496
    iget-object v2, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 339497
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339498
    const-string v0, "Feedback Referrer: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 339499
    iget-object v2, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 339500
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339501
    const-string v0, "Tracking Codes: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339502
    const-string v0, "Comments Funnel Logger Instance Id: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339503
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 339504
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 339475
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 339476
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 339477
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 339478
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 339479
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->e:LX/21B;

    if-nez v0, :cond_1

    sget-object v0, LX/21B;->UNKNOWN:LX/21B;

    invoke-virtual {v0}, LX/21B;->ordinal()I

    move-result v0

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 339480
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f:LX/21C;

    if-nez v0, :cond_2

    sget-object v0, LX/21C;->UNKNOWN:LX/21C;

    invoke-virtual {v0}, LX/21C;->ordinal()I

    move-result v0

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 339481
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 339482
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->h:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 339483
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->i:LX/21D;

    if-nez v0, :cond_4

    sget-object v0, LX/21D;->INVALID:LX/21D;

    invoke-virtual {v0}, LX/21D;->ordinal()I

    move-result v0

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 339484
    iget-wide v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 339485
    return-void

    .line 339486
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 339487
    :cond_1
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->e:LX/21B;

    invoke-virtual {v0}, LX/21B;->ordinal()I

    move-result v0

    goto :goto_1

    .line 339488
    :cond_2
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f:LX/21C;

    invoke-virtual {v0}, LX/21C;->ordinal()I

    move-result v0

    goto :goto_2

    .line 339489
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 339490
    :cond_4
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->i:LX/21D;

    invoke-virtual {v0}, LX/21D;->ordinal()I

    move-result v0

    goto :goto_4
.end method
