.class public Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160659
    new-instance v0, LX/55q;

    invoke-direct {v0}, LX/55q;-><init>()V

    sput-object v0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 160655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160656
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->a:Ljava/lang/String;

    .line 160657
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->b:Ljava/lang/String;

    .line 160658
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 160651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160652
    iput-object p1, p0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->a:Ljava/lang/String;

    .line 160653
    iput-object p2, p0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->b:Ljava/lang/String;

    .line 160654
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 160644
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 160650
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "feedStyleParam"

    iget-object v2, p0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "orderByParam"

    iget-object v2, p0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 160645
    iget-object v0, p0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->a:Ljava/lang/String;

    move-object v0, v0

    .line 160646
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 160647
    iget-object v0, p0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->b:Ljava/lang/String;

    move-object v0, v0

    .line 160648
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 160649
    return-void
.end method
