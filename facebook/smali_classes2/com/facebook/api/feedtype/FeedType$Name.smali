.class public final Lcom/facebook/api/feedtype/FeedType$Name;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final A:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feedtype/FeedType$Name;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final b:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final c:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final d:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final e:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final f:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final g:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final h:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final i:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final j:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final k:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final l:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final m:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final n:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final o:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final p:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final q:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final r:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final s:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final t:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final u:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final v:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final w:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final x:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final y:Lcom/facebook/api/feedtype/FeedType$Name;

.field public static final z:Lcom/facebook/api/feedtype/FeedType$Name;


# instance fields
.field public final B:Ljava/lang/String;

.field public final C:LX/0pL;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 144271
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "news_feed"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144272
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "friendlist_feed"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->b:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144273
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "good_friends_feed"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144274
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "hashtag_feed"

    sget-object v2, LX/0pL;->NO_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->d:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144275
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "page_feed"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->e:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144276
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "page_news_feed"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144277
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "stories_about_page_feed"

    sget-object v2, LX/0pL;->NO_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->g:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144278
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "stories_about_topic_feed"

    sget-object v2, LX/0pL;->NO_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->h:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144279
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "reaction_feed_type"

    sget-object v2, LX/0pL;->NO_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->i:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144280
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "event_feed_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->j:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144281
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "group_feed_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->k:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144282
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "cross_group_for_sale_posts_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->l:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144283
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "group_feed_owner_authored_available_for_sale_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->m:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144284
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "group_feed_owner_authored_sold_for_sale_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->n:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144285
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "group_feed_owner_authored_expired_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->o:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144286
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "group_feed_available_for_sale_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->p:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144287
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "group_feed_member_bio_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->q:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144288
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "group_feed_pending_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->r:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144289
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "group_feed_pinned_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->s:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144290
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "group_feed_learning_unit_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->t:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144291
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "group_feed_reported_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->u:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144292
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "groups_feed_discussion_topics_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->v:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144293
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "groups_feed_divein_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->w:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144294
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "community_for_sale_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->x:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144295
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "community_trending_stories_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->y:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144296
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "goodwill_throwback_feed_type"

    sget-object v2, LX/0pL;->NO_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->z:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144297
    new-instance v0, Lcom/facebook/api/feedtype/FeedType$Name;

    const-string v1, "gametime_plays_feed_type"

    sget-object v2, LX/0pL;->DISK_AND_MEMORY_CACHE:LX/0pL;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;-><init>(Ljava/lang/String;LX/0pL;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->A:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144298
    new-instance v0, LX/0pM;

    invoke-direct {v0}, LX/0pM;-><init>()V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 144299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144300
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    .line 144301
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0pL;->valueOf(Ljava/lang/String;)LX/0pL;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedtype/FeedType$Name;->C:LX/0pL;

    .line 144302
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LX/0pL;)V
    .locals 0

    .prologue
    .line 144303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144304
    iput-object p1, p0, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    .line 144305
    iput-object p2, p0, Lcom/facebook/api/feedtype/FeedType$Name;->C:LX/0pL;

    .line 144306
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 144307
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 144308
    check-cast p1, Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144309
    if-eqz p1, :cond_0

    .line 144310
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v0, v0

    .line 144311
    iget-object v1, p1, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v1, v1

    .line 144312
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 144313
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v0, v0

    .line 144314
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144315
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType$Name;->C:LX/0pL;

    move-object v0, v0

    .line 144316
    invoke-virtual {v0}, LX/0pL;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144317
    return-void
.end method
