.class public Lcom/facebook/api/feedtype/FeedType;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feedtype/FeedType;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/api/feedtype/FeedType;

.field public static final b:Lcom/facebook/api/feedtype/FeedType;

.field public static final c:Lcom/facebook/api/feedtype/FeedType;

.field public static final d:Lcom/facebook/api/feedtype/FeedType;


# instance fields
.field public final e:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/api/feedtype/FeedType$Name;

.field public g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 144266
    new-instance v0, Lcom/facebook/api/feedtype/FeedType;

    const-string v1, "most_recent"

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 144267
    new-instance v0, Lcom/facebook/api/feedtype/FeedType;

    const-string v1, "top_stories"

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 144268
    new-instance v0, Lcom/facebook/api/feedtype/FeedType;

    const-string v1, "good_friends"

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType;->c:Lcom/facebook/api/feedtype/FeedType;

    .line 144269
    new-instance v0, Lcom/facebook/api/feedtype/FeedType;

    const-string v1, "onboarding"

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType;->d:Lcom/facebook/api/feedtype/FeedType;

    .line 144270
    new-instance v0, LX/0pN;

    invoke-direct {v0}, LX/0pN;-><init>()V

    sput-object v0, Lcom/facebook/api/feedtype/FeedType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 144257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144258
    const-string v1, "native_newsfeed"

    iput-object v1, p0, Lcom/facebook/api/feedtype/FeedType;->g:Ljava/lang/String;

    .line 144259
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 144260
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    .line 144261
    const-class v0, Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedtype/FeedType$Name;

    iput-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144262
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->g:Ljava/lang/String;

    .line 144263
    return-void

    .line 144264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 144265
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/facebook/api/feedtype/FeedType$Name;",
            ")V"
        }
    .end annotation

    .prologue
    .line 144252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144253
    const-string v0, "native_newsfeed"

    iput-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->g:Ljava/lang/String;

    .line 144254
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    .line 144255
    iput-object p2, p0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144256
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/facebook/api/feedtype/FeedType$Name;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 144246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144247
    const-string v0, "native_newsfeed"

    iput-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->g:Ljava/lang/String;

    .line 144248
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    .line 144249
    iput-object p2, p0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144250
    iput-object p3, p0, Lcom/facebook/api/feedtype/FeedType;->g:Ljava/lang/String;

    .line 144251
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144245
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 144244
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/0pL;
    .locals 1

    .prologue
    .line 144241
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 144242
    iget-object p0, v0, Lcom/facebook/api/feedtype/FeedType$Name;->C:LX/0pL;

    move-object v0, p0

    .line 144243
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 144235
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/api/feedtype/FeedType;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 144236
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v2

    move-object v0, p1

    check-cast v0, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144237
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v0

    .line 144238
    check-cast p1, Lcom/facebook/api/feedtype/FeedType;

    .line 144239
    iget-object v2, p1, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v2

    .line 144240
    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 144220
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144234
    invoke-virtual {p0}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 144221
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v0

    .line 144222
    instance-of v0, v0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v0, :cond_0

    .line 144223
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 144224
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v0

    .line 144225
    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 144226
    :goto_0
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v0

    .line 144227
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 144228
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->g:Ljava/lang/String;

    move-object v0, v0

    .line 144229
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144230
    return-void

    .line 144231
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 144232
    iget-object v0, p0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v0

    .line 144233
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    goto :goto_0
.end method
