.class public interface abstract Lcom/facebook/performancelogger/PerformanceLogger;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 79015
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "perfmarker_to_logcat"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/performancelogger/PerformanceLogger;->a:LX/0Tn;

    return-void
.end method


# virtual methods
.method public abstract a(ILjava/lang/String;)V
.end method

.method public abstract a(ILjava/lang/String;D)V
.end method

.method public abstract a(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(ILjava/lang/String;Ljava/lang/String;J)V
.end method

.method public abstract a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract a(LX/0Yj;)V
.end method

.method public abstract a(LX/0Yj;D)V
.end method

.method public abstract a(LX/0Yj;Z)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(ILjava/lang/String;)V
.end method

.method public abstract b(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract b(LX/0Yj;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c(ILjava/lang/String;)V
.end method

.method public abstract c(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract c(LX/0Yj;)V
.end method

.method public abstract d(ILjava/lang/String;)V
.end method

.method public abstract d(LX/0Yj;)V
.end method

.method public abstract d(ILjava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract e(ILjava/lang/String;)V
.end method

.method public abstract e(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract e(LX/0Yj;)Z
.end method

.method public abstract f(ILjava/lang/String;)V
.end method

.method public abstract f(LX/0Yj;)V
.end method

.method public abstract g(ILjava/lang/String;)Z
.end method

.method public abstract h(ILjava/lang/String;)Z
.end method
