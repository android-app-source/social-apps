.class public Lcom/facebook/photos/prefetch/PrefetchParams;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1bf;

.field public final b:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public constructor <init>(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 291024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291025
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bf;

    iput-object v0, p0, Lcom/facebook/photos/prefetch/PrefetchParams;->a:LX/1bf;

    .line 291026
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/photos/prefetch/PrefetchParams;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 291027
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 291029
    instance-of v1, p1, Lcom/facebook/photos/prefetch/PrefetchParams;

    if-nez v1, :cond_1

    .line 291030
    :cond_0
    :goto_0
    return v0

    .line 291031
    :cond_1
    check-cast p1, Lcom/facebook/photos/prefetch/PrefetchParams;

    .line 291032
    iget-object v1, p0, Lcom/facebook/photos/prefetch/PrefetchParams;->a:LX/1bf;

    iget-object v2, p1, Lcom/facebook/photos/prefetch/PrefetchParams;->a:LX/1bf;

    invoke-virtual {v1, v2}, LX/1bf;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/prefetch/PrefetchParams;->b:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v2, p1, Lcom/facebook/photos/prefetch/PrefetchParams;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 291028
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/prefetch/PrefetchParams;->a:LX/1bf;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/photos/prefetch/PrefetchParams;->b:Lcom/facebook/common/callercontext/CallerContext;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
