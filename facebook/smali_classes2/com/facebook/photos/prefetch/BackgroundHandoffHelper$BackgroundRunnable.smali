.class public final Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1Jc;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Jc;)V
    .locals 2

    .prologue
    const/16 v1, 0x28

    .line 230196
    iput-object p1, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->a:LX/1Jc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->b:Ljava/util/List;

    .line 230198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->c:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 230199
    iget-object v0, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->a:LX/1Jc;

    iget-object v1, v0, LX/1Jc;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 230200
    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->a:LX/1Jc;

    iget-object v0, v0, LX/1Jc;->e:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->b:Ljava/util/List;

    invoke-static {v0, v2}, LX/1Jc;->c(Ljava/util/List;Ljava/util/List;)V

    .line 230201
    iget-object v0, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->a:LX/1Jc;

    iget-object v0, v0, LX/1Jc;->f:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->c:Ljava/util/List;

    invoke-static {v0, v2}, LX/1Jc;->c(Ljava/util/List;Ljava/util/List;)V

    .line 230202
    iget-object v0, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->a:LX/1Jc;

    const/4 v2, 0x0

    .line 230203
    iput-boolean v2, v0, LX/1Jc;->g:Z

    .line 230204
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230205
    iget-object v0, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->a:LX/1Jc;

    iget-object v0, v0, LX/1Jc;->b:LX/1JZ;

    iget-object v1, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;->c:Ljava/util/List;

    .line 230206
    iget-object v3, v0, LX/1JZ;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->clear()V

    .line 230207
    iget-object v3, v0, LX/1JZ;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v3, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 230208
    iget-object v3, v0, LX/1JZ;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v3, v2}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 230209
    iget-object v3, v0, LX/1JZ;->b:Ljava/util/LinkedHashSet;

    iget-object p0, v0, LX/1JZ;->d:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/util/LinkedHashSet;->removeAll(Ljava/util/Collection;)Z

    .line 230210
    iget-object v3, v0, LX/1JZ;->b:Ljava/util/LinkedHashSet;

    iget-object p0, v0, LX/1JZ;->e:LX/0aq;

    invoke-virtual {p0}, LX/0aq;->d()Ljava/util/Map;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/util/LinkedHashSet;->removeAll(Ljava/util/Collection;)Z

    .line 230211
    iget-object v3, v0, LX/1JZ;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 230212
    iget-object v3, v0, LX/1JZ;->c:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 230213
    iget-object v3, v0, LX/1JZ;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/prefetch/PrefetchParams;

    .line 230214
    invoke-static {v0, v3}, LX/1JZ;->b(LX/1JZ;Lcom/facebook/photos/prefetch/PrefetchParams;)V

    goto :goto_0

    .line 230215
    :cond_0
    iget-object v3, v0, LX/1JZ;->d:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    iget p0, v0, LX/1JZ;->j:I

    if-ge v3, p0, :cond_1

    .line 230216
    invoke-static {v0}, LX/1JZ;->a(LX/1JZ;)V

    .line 230217
    :cond_1
    return-void

    .line 230218
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
