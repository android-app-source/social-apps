.class public Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;
.super LX/0b5;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Z

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86684
    new-instance v0, LX/8Kg;

    invoke-direct {v0}, LX/8Kg;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 86677
    const-class v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/8KZ;->valueOf(Ljava/lang/String;)LX/8KZ;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    invoke-direct {p0, v0, v1, v2}, LX/0b5;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    .line 86678
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    .line 86679
    if-eqz v0, :cond_0

    const-class v0, Landroid/content/Intent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    :goto_0
    iput-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->a:Landroid/content/Intent;

    .line 86680
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->b:Z

    .line 86681
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->c:Z

    .line 86682
    return-void

    .line 86683
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/content/Intent;ZZ)V
    .locals 2
    .param p2    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86685
    sget-object v0, LX/8KZ;->UPLOAD_FAILED:LX/8KZ;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-direct {p0, p1, v0, v1}, LX/0b5;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    .line 86686
    iput-object p2, p0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->a:Landroid/content/Intent;

    .line 86687
    iput-boolean p3, p0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->b:Z

    .line 86688
    iput-boolean p4, p0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->c:Z

    .line 86689
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 86676
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 86661
    iget-object v0, p0, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86662
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 86663
    iget-object v0, p0, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 86664
    invoke-virtual {v0}, LX/8KZ;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86665
    iget v0, p0, LX/0b5;->c:F

    move v0, v0

    .line 86666
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 86667
    iget-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->a:Landroid/content/Intent;

    move-object v0, v0

    .line 86668
    if-eqz v0, :cond_0

    .line 86669
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 86670
    iget-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->a:Landroid/content/Intent;

    move-object v0, v0

    .line 86671
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 86672
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 86673
    iget-boolean v0, p0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 86674
    return-void

    .line 86675
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    goto :goto_0
.end method
