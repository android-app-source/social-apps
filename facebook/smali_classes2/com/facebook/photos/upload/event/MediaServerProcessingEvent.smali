.class public Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;
.super LX/0b5;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86933
    new-instance v0, LX/8Ke;

    invoke-direct {v0}, LX/8Ke;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 86934
    const-class v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/8KZ;->valueOf(Ljava/lang/String;)LX/8KZ;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    invoke-direct {p0, v0, v1, v2}, LX/0b5;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    .line 86935
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->a:Ljava/lang/String;

    .line 86936
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 86937
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2
    .param p4    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86938
    if-eqz p3, :cond_0

    sget-object v0, LX/8KZ;->MEDIA_PROCESSING_SUCCESS:LX/8KZ;

    :goto_0
    const/high16 v1, -0x40800000    # -1.0f

    invoke-direct {p0, p1, v0, v1}, LX/0b5;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    .line 86939
    iput-object p2, p0, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->a:Ljava/lang/String;

    .line 86940
    iput-object p4, p0, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 86941
    return-void

    .line 86942
    :cond_0
    sget-object v0, LX/8KZ;->MEDIA_PROCESSING_FAILED:LX/8KZ;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 86943
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 86944
    iget-object v0, p0, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86945
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 86946
    iget-object v0, p0, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 86947
    invoke-virtual {v0}, LX/8KZ;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86948
    iget v0, p0, LX/0b5;->c:F

    move v0, v0

    .line 86949
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 86950
    iget-object v0, p0, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->a:Ljava/lang/String;

    move-object v0, v0

    .line 86951
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86952
    iget-object v0, p0, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 86953
    return-void
.end method
