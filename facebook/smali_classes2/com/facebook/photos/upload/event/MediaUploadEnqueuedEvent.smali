.class public Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;
.super LX/0b5;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86633
    new-instance v0, LX/8Kf;

    invoke-direct {v0}, LX/8Kf;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/event/MediaUploadEnqueuedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 86631
    const-class v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/8KZ;->valueOf(Ljava/lang/String;)LX/8KZ;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    invoke-direct {p0, v0, v1, v2}, LX/0b5;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    .line 86632
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 2

    .prologue
    .line 86629
    sget-object v0, LX/8KZ;->UPLOADING:LX/8KZ;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-direct {p0, p1, v0, v1}, LX/0b5;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    .line 86630
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 86621
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 86622
    iget-object v0, p0, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86623
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 86624
    iget-object v0, p0, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 86625
    invoke-virtual {v0}, LX/8KZ;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86626
    iget v0, p0, LX/0b5;->c:F

    move v0, v0

    .line 86627
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 86628
    return-void
.end method
