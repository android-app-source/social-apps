.class public Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;
.super LX/0b5;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86709
    new-instance v0, LX/8Kh;

    invoke-direct {v0}, LX/8Kh;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 86710
    const-class v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/8KZ;->valueOf(Ljava/lang/String;)LX/8KZ;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    invoke-direct {p0, v0, v1, v2}, LX/0b5;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    .line 86711
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->a:Ljava/lang/String;

    .line 86712
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->b:Landroid/os/Bundle;

    .line 86713
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 86714
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->d:Ljava/lang/String;

    .line 86715
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 2
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86716
    sget-object v0, LX/8KZ;->UPLOAD_SUCCESS:LX/8KZ;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-direct {p0, p1, v0, v1}, LX/0b5;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    .line 86717
    iput-object p2, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->a:Ljava/lang/String;

    .line 86718
    iput-object p3, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->b:Landroid/os/Bundle;

    .line 86719
    iput-object p4, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 86720
    iput-object p5, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->d:Ljava/lang/String;

    .line 86721
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 86722
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 86723
    iget-object v0, p0, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86724
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 86725
    iget-object v0, p0, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 86726
    invoke-virtual {v0}, LX/8KZ;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86727
    iget v0, p0, LX/0b5;->c:F

    move v0, v0

    .line 86728
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 86729
    iget-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->a:Ljava/lang/String;

    move-object v0, v0

    .line 86730
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86731
    iget-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->b:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 86732
    iget-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 86733
    iget-object v0, p0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->d:Ljava/lang/String;

    move-object v0, v0

    .line 86734
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86735
    return-void
.end method
