.class public Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1WO;

.field private final e:LX/1V0;

.field public final f:LX/1WM;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1WO;LX/1V0;LX/1WM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268140
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 268141
    iput-object p3, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->e:LX/1V0;

    .line 268142
    iput-object p2, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->d:LX/1WO;

    .line 268143
    iput-object p4, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->f:LX/1WM;

    .line 268144
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 268121
    iget-object v0, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->d:LX/1WO;

    const/4 v1, 0x0

    .line 268122
    new-instance v2, LX/Ccb;

    invoke-direct {v2, v0}, LX/Ccb;-><init>(LX/1WO;)V

    .line 268123
    sget-object v3, LX/1WO;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Cca;

    .line 268124
    if-nez v3, :cond_0

    .line 268125
    new-instance v3, LX/Cca;

    invoke-direct {v3}, LX/Cca;-><init>()V

    .line 268126
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Cca;->a$redex0(LX/Cca;LX/1De;IILX/Ccb;)V

    .line 268127
    move-object v2, v3

    .line 268128
    move-object v1, v2

    .line 268129
    move-object v0, v1

    .line 268130
    iget-object v1, v0, LX/Cca;->a:LX/Ccb;

    iput-object p2, v1, LX/Ccb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268131
    iget-object v1, v0, LX/Cca;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 268132
    move-object v0, v0

    .line 268133
    new-instance v1, LX/Ccc;

    invoke-direct {v1, p0, p2, p3}, LX/Ccc;-><init>(Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)V

    move-object v1, v1

    .line 268134
    iget-object v2, v0, LX/Cca;->a:LX/Ccb;

    iput-object v1, v2, LX/Ccb;->b:Landroid/text/style/ClickableSpan;

    .line 268135
    iget-object v2, v0, LX/Cca;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 268136
    move-object v0, v0

    .line 268137
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 268138
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->g:LX/1Ua;

    const v3, 0x7f020a43

    const/4 v4, -0x1

    invoke-direct {v1, p2, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 268139
    iget-object v2, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;
    .locals 7

    .prologue
    .line 268084
    const-class v1, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;

    monitor-enter v1

    .line 268085
    :try_start_0
    sget-object v0, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268086
    sput-object v2, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268087
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268088
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268089
    new-instance p0, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1WO;->a(LX/0QB;)LX/1WO;

    move-result-object v4

    check-cast v4, LX/1WO;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v6

    check-cast v6, LX/1WM;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;-><init>(Landroid/content/Context;LX/1WO;LX/1V0;LX/1WM;)V

    .line 268090
    move-object v0, p0

    .line 268091
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268092
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268093
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268094
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 268102
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268103
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268104
    invoke-static {v0}, LX/17E;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 268105
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    const p1, 0x39d284c8

    invoke-static {v2, p1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v2

    .line 268106
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->D()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 268107
    if-eqz v2, :cond_3

    .line 268108
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 268109
    invoke-static {v0}, LX/1WM;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 268110
    :goto_1
    iget-object v2, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->f:LX/1WM;

    invoke-virtual {v2, v0}, LX/1WM;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_2
    return-object v0

    .line 268111
    :cond_0
    invoke-static {v0}, LX/17E;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 268112
    invoke-static {v0}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 268113
    invoke-static {v0}, LX/1WM;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 268114
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 268115
    :cond_1
    invoke-static {v0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v0

    .line 268116
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 268117
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 268118
    invoke-static {v0}, LX/1WM;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 268119
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 268120
    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 268101
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 268100
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 268098
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268099
    invoke-static {p0, p1}, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->c(Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 268095
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268096
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268097
    check-cast v0, LX/0jW;

    return-object v0
.end method
