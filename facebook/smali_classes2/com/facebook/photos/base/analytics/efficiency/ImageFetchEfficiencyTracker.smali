.class public Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;
.super LX/1BT;
.source ""

# interfaces
.implements LX/1Iq;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/1In;

.field private final b:LX/1Io;

.field private final c:LX/0SG;

.field private d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/1fm;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1In;LX/1Io;LX/0SG;)V
    .locals 1
    .param p1    # LX/1In;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1Io;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 229336
    invoke-direct {p0}, LX/1BT;-><init>()V

    .line 229337
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    .line 229338
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->e:Z

    .line 229339
    iput-object p1, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->a:LX/1In;

    .line 229340
    iput-object p2, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->b:LX/1Io;

    .line 229341
    iput-object p3, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->c:LX/0SG;

    .line 229342
    return-void
.end method

.method private declared-synchronized a()LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "LX/1fm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229289
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->b(Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-wide v0, v0, LX/1fm;->c:J

    sub-long v0, v2, v0

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 229290
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 229291
    :goto_0
    monitor-exit p0

    return-object v0

    .line 229292
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    .line 229293
    iget-object v1, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->b:LX/1Io;

    invoke-virtual {v1}, LX/1Io;->c()V

    .line 229294
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    .line 229295
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 229296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 229326
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    move v0, v1

    .line 229327
    :goto_0
    monitor-exit p0

    return v0

    .line 229328
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->b:LX/1Io;

    .line 229329
    iget-object v2, v0, LX/1Io;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v2

    move v0, v2

    .line 229330
    if-nez v0, :cond_1

    .line 229331
    const/4 v0, 0x0

    goto :goto_0

    .line 229332
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->b:LX/1Io;

    invoke-virtual {v0}, LX/1Io;->b()LX/0am;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    iput-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    .line 229333
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 229334
    goto :goto_0

    .line 229335
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;IZZ)V
    .locals 11

    .prologue
    .line 229321
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->b(Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 229322
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 229323
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->a:LX/1In;

    invoke-interface {v0, p1, p2}, LX/1In;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229324
    iget-object v1, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->b:LX/1Io;

    invoke-virtual {p1}, LX/1bf;->b()Landroid/net/Uri;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v10

    move v3, p3

    move v6, p4

    move/from16 v7, p5

    invoke-virtual/range {v1 .. v10}, LX/1Io;->a(Landroid/net/Uri;IJZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/1fm;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 229325
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/1bf;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 229314
    monitor-enter p0

    if-nez p3, :cond_0

    :try_start_0
    invoke-static {p0}, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->b(Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 229315
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 229316
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-object v0, v0, LX/1fm;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-object v0, v0, LX/1fm;->a:Landroid/net/Uri;

    .line 229317
    iget-object v1, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 229318
    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229319
    iget-object v1, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->b:LX/1Io;

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-object v2, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/1Io;->a(LX/1fm;J)LX/1fm;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->d:LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 229320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(Ljava/lang/String;)LX/0am;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229297
    invoke-direct {p0}, Lcom/facebook/photos/base/analytics/efficiency/ImageFetchEfficiencyTracker;->a()LX/0am;

    move-result-object v1

    .line 229298
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229299
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 229300
    :goto_0
    return-object v0

    .line 229301
    :cond_0
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 229302
    const-string v0, "photo"

    .line 229303
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 229304
    const-string v3, "calling_class"

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-object v0, v0, LX/1fm;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 229305
    const-string v3, "analytics_tag"

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-object v0, v0, LX/1fm;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 229306
    const-string v3, "feature_tag"

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-object v0, v0, LX/1fm;->i:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 229307
    const-string v3, "content_length"

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget v0, v0, LX/1fm;->b:I

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 229308
    const-string v3, "is_prefetch"

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-boolean v0, v0, LX/1fm;->e:Z

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 229309
    const-string v3, "is_cancellation_requested"

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-boolean v0, v0, LX/1fm;->f:Z

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 229310
    const-string v3, "ui_requested"

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-object v0, v0, LX/1fm;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 229311
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-object v0, v0, LX/1fm;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229312
    const-string v3, "prefetch_to_ui_time"

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-object v0, v0, LX/1fm;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fm;

    iget-wide v0, v0, LX/1fm;->c:J

    sub-long v0, v4, v0

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 229313
    :cond_1
    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto/16 :goto_0
.end method
