.class public Lcom/facebook/quicklog/PerformanceLoggingEvent;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Po;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Po",
        "<",
        "Lcom/facebook/quicklog/PerformanceLoggingEvent;",
        ">;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# static fields
.field public static a:I

.field public static b:I

.field public static final c:LX/0aw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aw",
            "<",
            "Lcom/facebook/quicklog/PerformanceLoggingEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public d:Z

.field public e:I

.field public f:I

.field public g:J

.field public h:J

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:Z

.field public n:Z

.field public o:S

.field public p:S

.field public q:I

.field public r:LX/17K;

.field public s:LX/0Y0;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/00q;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:LX/03R;

.field public v:LX/03R;

.field public w:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public x:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/lang/String;

.field private z:Lcom/facebook/quicklog/PerformanceLoggingEvent;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 196983
    const/16 v0, 0xff

    sput v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a:I

    .line 196984
    const/16 v0, 0x18

    sput v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->b:I

    .line 196985
    new-instance v0, LX/17I;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, LX/17I;-><init>(I)V

    sput-object v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->c:LX/0aw;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 196986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196987
    sget v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a:I

    and-int/lit8 v0, v0, 0x1

    sget v1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->b:I

    shl-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->e:I

    .line 196988
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->w:Ljava/util/ArrayList;

    .line 196989
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->x:Ljava/util/ArrayList;

    .line 196990
    invoke-virtual {p0}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->b()V

    .line 196991
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 196992
    iget-object v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->z:Lcom/facebook/quicklog/PerformanceLoggingEvent;

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 196993
    iget v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->e:I

    const v1, -0xff0001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->e:I

    .line 196994
    iget v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->e:I

    and-int/lit16 v1, p1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->e:I

    .line 196995
    return-void
.end method

.method public final a(JJ)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 196996
    cmp-long v0, p1, v2

    if-ltz v0, :cond_0

    cmp-long v0, p3, v2

    if-ltz v0, :cond_0

    cmp-long v0, p3, p1

    if-ltz v0, :cond_0

    .line 196997
    sub-long v0, p3, p1

    long-to-int v0, v0

    iput v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->j:I

    .line 196998
    :goto_0
    return-void

    .line 196999
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->j:I

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 197000
    check-cast p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;

    .line 197001
    iput-object p1, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->z:Lcom/facebook/quicklog/PerformanceLoggingEvent;

    .line 197002
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 197003
    iget-object v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197004
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 197005
    if-nez p1, :cond_0

    .line 197006
    :goto_0
    return-void

    .line 197007
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->w:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 197008
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->k:I

    .line 197009
    iput-object v1, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->y:Ljava/lang/String;

    .line 197010
    iget-object v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 197011
    iget-object v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 197012
    iput-object v1, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->z:Lcom/facebook/quicklog/PerformanceLoggingEvent;

    .line 197013
    iput-object v1, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->t:LX/00q;

    .line 197014
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 197015
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197016
    iget-object v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 197017
    :cond_0
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197018
    iget v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->k:I

    invoke-static {v0}, LX/2BW;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197019
    iget-short v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->o:S

    invoke-static {v0}, LX/2BY;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final run()V
    .locals 12

    .prologue
    .line 197020
    iget-object v0, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->r:LX/17K;

    .line 197021
    iget-object v1, v0, LX/17K;->a:LX/0mh;

    .line 197022
    iget-object v2, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->y:Ljava/lang/String;

    move-object v2, v2

    .line 197023
    const-string v3, "perf"

    sget-object v4, LX/0mq;->CLIENT_EVENT:LX/0mq;

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0mh;->a(Ljava/lang/String;Ljava/lang/String;LX/0mq;Z)LX/0mj;

    move-result-object v1

    .line 197024
    iget v2, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->f:I

    move v2, v2

    .line 197025
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0mj;->b(Ljava/lang/String;)LX/0mj;

    .line 197026
    iget-wide v6, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->g:J

    move-wide v3, v6

    .line 197027
    invoke-virtual {v1, v3, v4}, LX/0mj;->a(J)LX/0mj;

    .line 197028
    const-string v6, "marker_id"

    .line 197029
    iget v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->k:I

    move v7, v7

    .line 197030
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197031
    const-string v6, "instance_id"

    .line 197032
    iget v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->f:I

    move v7, v7

    .line 197033
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197034
    const-string v6, "sample_rate"

    .line 197035
    iget v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->l:I

    move v7, v7

    .line 197036
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197037
    const-string v6, "time_since_boot_ms"

    .line 197038
    iget-wide v10, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->h:J

    move-wide v8, v10

    .line 197039
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197040
    const-string v6, "duration_ms"

    .line 197041
    iget v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->i:I

    move v7, v7

    .line 197042
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197043
    const-string v6, "action_id"

    .line 197044
    iget-short v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->o:S

    move v7, v7

    .line 197045
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197046
    const-string v6, "duration_since_prev_action_ms"

    .line 197047
    iget v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->q:I

    move v7, v7

    .line 197048
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197049
    const-string v6, "prev_action_id"

    .line 197050
    iget-short v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->p:S

    move v7, v7

    .line 197051
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197052
    iget-object v6, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->t:LX/00q;

    move-object v6, v6

    .line 197053
    if-eqz v6, :cond_1

    .line 197054
    iget-boolean v7, v6, LX/00q;->c:Z

    move v7, v7

    .line 197055
    if-eqz v7, :cond_1

    .line 197056
    const-string v7, "start_pri"

    .line 197057
    iget v8, v6, LX/00q;->f:I

    move v8, v8

    .line 197058
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197059
    const-string v7, "stop_pri"

    .line 197060
    iget v8, v6, LX/00q;->g:I

    move v8, v8

    .line 197061
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197062
    const-string v7, "ps_cpu_ms"

    .line 197063
    iget-wide v10, v6, LX/00q;->h:J

    move-wide v8, v10

    .line 197064
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197065
    const-string v7, "ps_flt"

    .line 197066
    iget-wide v10, v6, LX/00q;->j:J

    move-wide v8, v10

    .line 197067
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197068
    invoke-virtual {v6}, LX/00q;->m()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 197069
    const-string v7, "th_cpu_ms"

    .line 197070
    iget-wide v10, v6, LX/00q;->i:J

    move-wide v8, v10

    .line 197071
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197072
    const-string v7, "th_flt"

    .line 197073
    iget-wide v10, v6, LX/00q;->k:J

    move-wide v8, v10

    .line 197074
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197075
    :cond_0
    const-string v7, "class_load_attempts"

    invoke-virtual {v6}, LX/00q;->j()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197076
    const-string v7, "dex_queries"

    invoke-virtual {v6}, LX/00q;->k()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197077
    const-string v7, "low_power_state"

    .line 197078
    iget-object v8, v6, LX/00q;->m:Ljava/lang/String;

    move-object v6, v8

    .line 197079
    invoke-virtual {v1, v7, v6}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 197080
    :cond_1
    iget-object v6, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->v:LX/03R;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->v:LX/03R;

    invoke-virtual {v6}, LX/03R;->isSet()Z

    move-result v6

    if-eqz v6, :cond_8

    const/4 v6, 0x1

    :goto_0
    move v6, v6

    .line 197081
    if-eqz v6, :cond_2

    .line 197082
    const-string v6, "was_backgrounded"

    .line 197083
    iget-object v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->v:LX/03R;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, LX/03R;->asBoolean(Z)Z

    move-result v7

    move v7, v7

    .line 197084
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0mj;

    .line 197085
    :cond_2
    iget-boolean v6, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->d:Z

    move v6, v6

    .line 197086
    if-eqz v6, :cond_3

    .line 197087
    iget-object v6, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->u:LX/03R;

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->u:LX/03R;

    invoke-virtual {v6}, LX/03R;->isSet()Z

    move-result v6

    if-eqz v6, :cond_9

    const/4 v6, 0x1

    :goto_1
    move v6, v6

    .line 197088
    if-eqz v6, :cond_3

    .line 197089
    const-string v6, "app_started_in_bg"

    .line 197090
    iget-object v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->u:LX/03R;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, LX/03R;->asBoolean(Z)Z

    move-result v7

    move v7, v7

    .line 197091
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0mj;

    .line 197092
    :cond_3
    const-string v6, "method"

    .line 197093
    iget-boolean v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->n:Z

    move v7, v7

    .line 197094
    iget-boolean v8, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->m:Z

    move v8, v8

    .line 197095
    invoke-static {v7, v8}, LX/17y;->a(ZZ)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 197096
    iget v6, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->j:I

    move v6, v6

    .line 197097
    const/4 v7, -0x1

    if-eq v6, v7, :cond_4

    .line 197098
    const-string v6, "gc_ms"

    .line 197099
    iget v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->j:I

    move v7, v7

    .line 197100
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197101
    :cond_4
    iget-object v6, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->w:Ljava/util/ArrayList;

    move-object v6, v6

    .line 197102
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 197103
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 197104
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 197105
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 197106
    invoke-virtual {v1, v6, v7}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    goto :goto_2

    .line 197107
    :cond_5
    const-string v6, "marker"

    .line 197108
    iget-short v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->o:S

    move v7, v7

    .line 197109
    const/4 v8, 0x3

    if-ne v7, v8, :cond_a

    .line 197110
    const-string v8, "client_fail"

    .line 197111
    :goto_3
    move-object v7, v8

    .line 197112
    invoke-virtual {v1, v6, v7}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 197113
    const-string v6, "value"

    .line 197114
    iget v7, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->i:I

    move v7, v7

    .line 197115
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, LX/0mj;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;

    .line 197116
    iget-object v6, p0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->s:LX/0Y0;

    move-object v6, v6

    .line 197117
    if-eqz v6, :cond_6

    .line 197118
    const-string v7, "connqual"

    invoke-virtual {v6}, LX/0Y0;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 197119
    const-string v7, "network_type"

    .line 197120
    iget-object v8, v6, LX/0Y0;->b:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-virtual {v8}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v8

    move-object v8, v8

    .line 197121
    invoke-virtual {v1, v7, v8}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 197122
    const-string v7, "network_subtype"

    .line 197123
    iget-object v8, v6, LX/0Y0;->b:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-virtual {v8}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v8

    move-object v6, v8

    .line 197124
    invoke-virtual {v1, v7, v6}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 197125
    :cond_6
    sget-object v6, LX/17K;->b:Ljava/lang/String;

    if-eqz v6, :cond_7

    .line 197126
    const-string v6, "scenario"

    sget-object v7, LX/17K;->b:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, LX/0mj;->d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;

    .line 197127
    :cond_7
    invoke-virtual {v1}, LX/0mj;->e()V

    .line 197128
    sget-object v0, Lcom/facebook/quicklog/PerformanceLoggingEvent;->c:LX/0aw;

    invoke-virtual {v0, p0}, LX/0aw;->a(LX/0Po;)V

    .line 197129
    return-void

    :cond_8
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 197130
    :cond_a
    const/4 v8, 0x4

    if-ne v7, v8, :cond_b

    .line 197131
    const-string v8, "client_cancel"

    goto :goto_3

    .line 197132
    :cond_b
    const-string v8, "client_tti"

    goto :goto_3
.end method
