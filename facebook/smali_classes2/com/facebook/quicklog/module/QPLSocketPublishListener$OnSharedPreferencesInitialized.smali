.class public final Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0YL;


# direct methods
.method public constructor <init>(LX/0YL;)V
    .locals 0

    .prologue
    .line 81137
    iput-object p1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 81138
    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0YL;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0YL;->b:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0YL;->c:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 81139
    :cond_0
    iget-object v2, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    if-eqz v0, :cond_2

    sget-object v1, LX/03R;->YES:LX/03R;

    .line 81140
    :goto_0
    iput-object v1, v2, LX/0YL;->j:LX/03R;

    .line 81141
    if-nez v0, :cond_3

    .line 81142
    :cond_1
    :goto_1
    return-void

    .line 81143
    :cond_2
    sget-object v1, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 81144
    :cond_3
    iget-object v0, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0YL;->c:LX/0Tn;

    const/16 v3, 0x2328

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 81145
    iput v1, v0, LX/0YL;->k:I

    .line 81146
    const/4 v0, 0x0

    .line 81147
    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0YL;->b:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 81148
    iget-object v2, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v2, v2, LX/0YL;->g:Ljava/util/List;

    monitor-enter v2

    .line 81149
    :try_start_0
    iget-object v3, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v3, v3, LX/0YL;->i:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 81150
    iget-object v3, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v3, v3, LX/0YL;->h:Ljava/util/Set;

    invoke-static {v3, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 81151
    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->i:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 81152
    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->i:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 81153
    :try_start_1
    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4l5;

    .line 81154
    iget-object v4, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v4, v4, LX/0YL;->h:Ljava/util/Set;

    .line 81155
    iget-object v5, v1, LX/4l5;->a:Ljava/lang/String;

    move-object v1, v5

    .line 81156
    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 81157
    const/4 v1, 0x1

    .line 81158
    :goto_2
    move v1, v1

    .line 81159
    if-eqz v1, :cond_5

    .line 81160
    new-instance v0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;

    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    invoke-direct {v0, v1}, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;-><init>(LX/0YL;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81161
    :cond_5
    :try_start_2
    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->i:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 81162
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 81163
    if-eqz v0, :cond_1

    .line 81164
    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->f:Ljava/util/concurrent/ExecutorService;

    const v2, 0x3917e105

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_1

    .line 81165
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;->a:LX/0YL;

    iget-object v1, v1, LX/0YL;->i:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 81166
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method
