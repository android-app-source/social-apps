.class public Lcom/facebook/quicklog/QuickPerformanceLoggerProvider;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private static b:LX/4il;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0

    .prologue
    .line 81724
    sput-object p0, Lcom/facebook/quicklog/QuickPerformanceLoggerProvider;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 81725
    return-void
.end method

.method public static getQPLInstance()Lcom/facebook/quicklog/QuickPerformanceLogger;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 81717
    sget-object v0, Lcom/facebook/quicklog/QuickPerformanceLoggerProvider;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v0, :cond_0

    .line 81718
    sget-object v0, Lcom/facebook/quicklog/QuickPerformanceLoggerProvider;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 81719
    :goto_0
    return-object v0

    .line 81720
    :cond_0
    sget-object v0, Lcom/facebook/quicklog/QuickPerformanceLoggerProvider;->b:LX/4il;

    if-eqz v0, :cond_1

    .line 81721
    sget-object v0, Lcom/facebook/quicklog/QuickPerformanceLoggerProvider;->b:LX/4il;

    invoke-interface {v0}, LX/4il;->a()Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    .line 81722
    sput-object v0, Lcom/facebook/quicklog/QuickPerformanceLoggerProvider;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    goto :goto_0

    .line 81723
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
