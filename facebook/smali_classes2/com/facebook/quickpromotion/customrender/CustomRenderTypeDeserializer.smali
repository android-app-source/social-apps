.class public Lcom/facebook/quickpromotion/customrender/CustomRenderTypeDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 273226
    const-class v0, Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    new-instance v1, Lcom/facebook/quickpromotion/customrender/CustomRenderTypeDeserializer;

    invoke-direct {v1}, Lcom/facebook/quickpromotion/customrender/CustomRenderTypeDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 273227
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 273228
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 273229
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/quickpromotion/customrender/CustomRenderType;->fromString(Ljava/lang/String;)Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    move-result-object v0

    return-object v0
.end method
