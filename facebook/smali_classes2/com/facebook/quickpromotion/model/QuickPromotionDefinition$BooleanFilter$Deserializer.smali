.class public final Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 272565
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    return-void
.end method

.method private static a(LX/15w;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;
    .locals 2

    .prologue
    .line 272566
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v0, v1, :cond_0

    .line 272567
    const/4 v0, 0x0

    .line 272568
    :goto_0
    return-object v0

    .line 272569
    :cond_0
    invoke-virtual {p0}, LX/15w;->a()LX/0lD;

    move-result-object v0

    check-cast v0, LX/0lB;

    .line 272570
    invoke-virtual {v0}, LX/0lD;->b()LX/0lp;

    move-result-object v0

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 272571
    const-class v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    .line 272572
    new-instance v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    invoke-direct {v1, v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;-><init>(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 272573
    invoke-static {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter$Deserializer;->a(LX/15w;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    move-result-object v0

    return-object v0
.end method
