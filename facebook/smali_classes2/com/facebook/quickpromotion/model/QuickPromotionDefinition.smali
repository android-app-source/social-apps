.class public Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quickpromotion/model/QuickPromotionDefinitionDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;",
            ">;"
        }
    .end annotation
.end field

.field public final animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "animated_image"
    .end annotation
.end field

.field private b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Attribute;",
            ">;"
        }
    .end annotation
.end field

.field public final booleanFilter:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "boolean_filter_root"
    .end annotation
.end field

.field public final brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "branding_image"
    .end annotation
.end field

.field public final clientTtlSeconds:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "client_ttl_seconds"
    .end annotation
.end field

.field public final content:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "content"
    .end annotation
.end field

.field public final creatives:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creatives"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;",
            ">;"
        }
    .end annotation
.end field

.field public final customRenderParams:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "custom_renderer_params"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final customRenderType:Lcom/facebook/quickpromotion/customrender/CustomRenderType;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "custom_renderer_type"
    .end annotation
.end field

.field public final dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dismiss_action"
    .end annotation
.end field

.field public final endTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "end_time"
    .end annotation
.end field

.field private final filters:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contextual_filters"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;",
            ">;"
        }
    .end annotation
.end field

.field public final footer:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "footer"
    .end annotation
.end field

.field public final imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image"
    .end annotation
.end field

.field public final instanceLogData:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "instance_log_data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final isExposureHoldout:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_exposure_holdout"
    .end annotation
.end field

.field public final logEligibilityWaterfall:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "log_eligibility_waterfall"
    .end annotation
.end field

.field public final maxImpressions:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "max_impressions"
    .end annotation
.end field

.field public final primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "primary_action"
    .end annotation
.end field

.field public final priority:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "priority"
    .end annotation
.end field

.field public final promotionId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "promotion_id"
    .end annotation
.end field

.field public final secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "secondary_action"
    .end annotation
.end field

.field public final socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "social_context"
    .end annotation
.end field

.field public final startTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_time"
    .end annotation
.end field

.field private final template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "template"
    .end annotation
.end field

.field public final templateParameters:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "template_parameters"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field

.field private final triggers:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "triggers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation
.end field

.field public final viewerImpressions:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "viewer_impressions"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 271794
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinitionDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 271793
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinitionSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 271792
    new-instance v0, LX/1Xj;

    invoke-direct {v0}, LX/1Xj;-><init>()V

    sput-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 271754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271755
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    .line 271756
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 271757
    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->triggers:Ljava/util/List;

    .line 271758
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 271759
    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->creatives:LX/0Px;

    .line 271760
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 271761
    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->filters:Ljava/util/List;

    .line 271762
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->booleanFilter:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    .line 271763
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    .line 271764
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    .line 271765
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    .line 271766
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    .line 271767
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 271768
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 271769
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 271770
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    .line 271771
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->footer:Ljava/lang/String;

    .line 271772
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->UNKNOWN:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    .line 271773
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 271774
    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->templateParameters:LX/0P1;

    .line 271775
    iput-wide v2, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->priority:J

    .line 271776
    iput v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->maxImpressions:I

    .line 271777
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->viewerImpressions:I

    .line 271778
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 271779
    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->b:LX/0Rf;

    .line 271780
    iput-wide v2, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->startTime:J

    .line 271781
    iput-wide v2, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->endTime:J

    .line 271782
    iput-wide v2, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->clientTtlSeconds:J

    .line 271783
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 271784
    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    .line 271785
    iput-boolean v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->isExposureHoldout:Z

    .line 271786
    iput-boolean v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->logEligibilityWaterfall:Z

    .line 271787
    iput-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    .line 271788
    sget-object v0, Lcom/facebook/quickpromotion/customrender/CustomRenderType;->UNKNOWN:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderType:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    .line 271789
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 271790
    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderParams:LX/0P1;

    .line 271791
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 271714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271715
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    .line 271716
    const-class v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->triggers:Ljava/util/List;

    .line 271717
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 271718
    sget-object v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 271719
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->creatives:LX/0Px;

    .line 271720
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 271721
    sget-object v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 271722
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->filters:Ljava/util/List;

    .line 271723
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->booleanFilter:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    .line 271724
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    .line 271725
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    .line 271726
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    .line 271727
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    .line 271728
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 271729
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 271730
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 271731
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    .line 271732
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->footer:Ljava/lang/String;

    .line 271733
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->fromString(Ljava/lang/String;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    .line 271734
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    .line 271735
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->templateParameters:LX/0P1;

    .line 271736
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->priority:J

    .line 271737
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->maxImpressions:I

    .line 271738
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->viewerImpressions:I

    .line 271739
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Attribute;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0RA;->a(Ljava/lang/Iterable;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->b:LX/0Rf;

    .line 271740
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->startTime:J

    .line 271741
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->endTime:J

    .line 271742
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->clientTtlSeconds:J

    .line 271743
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    .line 271744
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    .line 271745
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->isExposureHoldout:Z

    .line 271746
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->logEligibilityWaterfall:Z

    .line 271747
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    .line 271748
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/quickpromotion/customrender/CustomRenderType;->fromString(Ljava/lang/String;)Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderType:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    .line 271749
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    .line 271750
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderParams:LX/0P1;

    .line 271751
    return-void

    :cond_0
    move v0, v2

    .line 271752
    goto :goto_0

    :cond_1
    move v1, v2

    .line 271753
    goto :goto_1
.end method

.method public static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z
    .locals 2

    .prologue
    .line 271713
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->ANDROID_MESSENGER_THREAD_LIST_HEADER_BUTTONLESS_BANNER:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->ANDROID_MESSENGER_THREAD_VIEW_HEADER_BUTTONLESS_BANNER:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->ANDROID_MESSENGER_THREAD_VIEW_COMPOSER_BUTTONLESS_BANNER:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->SEARCH_BAR_TOOLTIP:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->CUSTOM_RENDERED:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z
    .locals 2

    .prologue
    .line 271712
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->SEARCH_BAR_TOOLTIP:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->CUSTOM_RENDERED:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z
    .locals 2

    .prologue
    .line 271711
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->SEARCH_BAR_TOOLTIP:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271708
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->triggers:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->triggers:Ljava/util/List;

    :goto_0
    return-object v0

    .line 271709
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 271710
    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 13
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271795
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->creatives:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 271796
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->creatives:LX/0Px;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 271797
    :goto_0
    return-object v0

    .line 271798
    :cond_0
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a:LX/0Px;

    if-nez v0, :cond_1

    .line 271799
    new-instance v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v5, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v6, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v7, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v8, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v9, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->footer:Ljava/lang/String;

    iget-object v10, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    iget-object v11, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->templateParameters:LX/0P1;

    iget-object v12, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;LX/0P1;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;)V

    .line 271800
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a:LX/0Px;

    .line 271801
    :cond_1
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a:LX/0Px;

    goto :goto_0
.end method

.method public final c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;
    .locals 2

    .prologue
    .line 271707
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271704
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->filters:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->filters:Ljava/util/List;

    :goto_0
    return-object v0

    .line 271705
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 271706
    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 271703
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;
    .locals 1

    .prologue
    .line 271702
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->UNKNOWN:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    goto :goto_0
.end method

.method public final f()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Attribute;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271699
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->b:LX/0Rf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->b:LX/0Rf;

    :goto_0
    return-object v0

    .line 271700
    :cond_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 271701
    goto :goto_0
.end method

.method public final g()Lcom/facebook/quickpromotion/customrender/CustomRenderType;
    .locals 1

    .prologue
    .line 271661
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderType:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderType:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/quickpromotion/customrender/CustomRenderType;->UNKNOWN:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    goto :goto_0
.end method

.method public getAttributesList()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attributes"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Attribute;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271698
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public readAttributes(Ljava/util/List;)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attributes"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Attribute;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 271696
    invoke-static {p1}, LX/0RA;->a(Ljava/lang/Iterable;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->b:LX/0Rf;

    .line 271697
    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 271662
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 271663
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 271664
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->creatives:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->creatives:LX/0Px;

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 271665
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->d()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 271666
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->booleanFilter:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 271667
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 271668
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 271669
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 271670
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 271671
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 271672
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 271673
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 271674
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 271675
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->footer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 271676
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 271677
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->templateParameters:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 271678
    iget-wide v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->priority:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 271679
    iget v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->maxImpressions:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 271680
    iget v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->viewerImpressions:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 271681
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 271682
    iget-wide v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->startTime:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 271683
    iget-wide v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->endTime:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 271684
    iget-wide v4, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->clientTtlSeconds:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 271685
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 271686
    iget-boolean v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->isExposureHoldout:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 271687
    iget-boolean v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->logEligibilityWaterfall:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 271688
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 271689
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->g()Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/customrender/CustomRenderType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 271690
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderParams:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 271691
    return-void

    .line 271692
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 271693
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 271694
    goto :goto_1

    :cond_2
    move v1, v2

    .line 271695
    goto :goto_2
.end method
