.class public final Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 272580
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;LX/0nX;)V
    .locals 2

    .prologue
    .line 272582
    if-nez p0, :cond_0

    .line 272583
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 272584
    :goto_0
    return-void

    .line 272585
    :cond_0
    invoke-virtual {p1}, LX/0nX;->a()LX/0lD;

    move-result-object v0

    check-cast v0, LX/0lB;

    .line 272586
    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    invoke-virtual {v0, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 272587
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 272581
    check-cast p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    invoke-static {p1, p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter$Serializer;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;LX/0nX;)V

    return-void
.end method
