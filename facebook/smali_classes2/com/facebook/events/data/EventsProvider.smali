.class public Lcom/facebook/events/data/EventsProvider;
.super LX/0Ov;
.source ""


# instance fields
.field public a:LX/Bl3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Bky;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56395
    invoke-direct {p0}, LX/0Ov;-><init>()V

    return-void
.end method

.method private static a(J)J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 56396
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 56397
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 56398
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 56399
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 56400
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 56401
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 56402
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(JJ)LX/0ux;
    .locals 9

    .prologue
    const-wide/32 v6, 0xa4cb80

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 56403
    sub-long v0, p0, p2

    cmp-long v0, v0, v6

    if-gez v0, :cond_0

    .line 56404
    sub-long p2, p0, v6

    .line 56405
    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 56406
    sget-object v1, LX/Bkw;->I:LX/0U1;

    invoke-virtual {v1, v0}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 56407
    sget-object v2, LX/Bkw;->J:LX/0U1;

    invoke-virtual {v2, v0}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 56408
    new-array v2, v5, [LX/0ux;

    aput-object v1, v2, v3

    aput-object v0, v2, v4

    invoke-static {v2}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v0

    .line 56409
    new-array v1, v5, [LX/0ux;

    aput-object v0, v1, v3

    invoke-static {}, Lcom/facebook/events/data/EventsProvider;->e()LX/0ux;

    move-result-object v0

    aput-object v0, v1, v4

    invoke-static {v1}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    .line 56410
    return-object v0
.end method

.method private static a(Landroid/content/ContentResolver;LX/Bky;JJ)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 56411
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 56412
    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 56413
    sget-object v2, LX/Bkw;->I:LX/0U1;

    invoke-virtual {v2, v0}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 56414
    sget-object v2, LX/Bkw;->I:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 56415
    new-array v2, v7, [LX/0ux;

    aput-object v0, v2, v5

    aput-object v1, v2, v6

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    .line 56416
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    .line 56417
    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    .line 56418
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    sget-object v0, LX/Bkw;->b:LX/0U1;

    .line 56419
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 56420
    aput-object v0, v2, v5

    sget-object v0, LX/Bkw;->I:LX/0U1;

    .line 56421
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 56422
    aput-object v0, v2, v6

    sget-object v0, LX/Bkw;->J:LX/0U1;

    .line 56423
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 56424
    aput-object v0, v2, v7

    .line 56425
    iget-object v1, p1, LX/Bky;->h:Landroid/net/Uri;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56426
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 56427
    sget-object v0, LX/Bkw;->b:LX/0U1;

    .line 56428
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 56429
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 56430
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    .line 56431
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 56432
    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 56433
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56434
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 56435
    :catch_0
    move-exception v0

    .line 56436
    :try_start_1
    const-class v2, Lcom/facebook/events/data/EventsProvider;

    const-string v3, "Unexpected error when deleting events for sync: "

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56437
    if-eqz p0, :cond_1

    .line 56438
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 56439
    :cond_1
    :goto_1
    return-object v1

    .line 56440
    :cond_2
    if-eqz p0, :cond_1

    .line 56441
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 56442
    :catchall_0
    move-exception v0

    if-eqz p0, :cond_3

    .line 56443
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static a(Landroid/content/ContentResolver;LX/Bky;ILX/0TD;)V
    .locals 2

    .prologue
    .line 56444
    new-instance v0, Lcom/facebook/events/data/EventsProvider$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/events/data/EventsProvider$2;-><init>(Landroid/content/ContentResolver;LX/Bky;I)V

    const v1, 0x64475155

    invoke-static {p3, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 56445
    return-void
.end method

.method public static a(Landroid/content/ContentResolver;LX/Bky;JJLjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/Bky;",
            "JJ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56446
    invoke-static/range {p0 .. p5}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;JJ)Landroid/database/Cursor;

    move-result-object v0

    .line 56447
    invoke-static {v0, p6}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/database/Cursor;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 56448
    invoke-static {p0, p1, v0}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Ljava/util/List;)V

    .line 56449
    return-void
.end method

.method public static a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 56450
    iget-object v0, p1, LX/Bky;->c:Landroid/net/Uri;

    .line 56451
    iget-object v1, p2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 56452
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 56453
    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 56454
    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 56455
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 56456
    invoke-virtual {p1, v1}, LX/Bky;->a(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p2}, LX/Bl4;->a(Lcom/facebook/events/model/Event;)Landroid/content/ContentValues;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56457
    :goto_0
    if-eqz v1, :cond_0

    .line 56458
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 56459
    :cond_0
    :goto_1
    return-void

    .line 56460
    :cond_1
    :try_start_1
    iget-object v0, p1, LX/Bky;->b:Landroid/net/Uri;

    invoke-static {p2}, LX/Bl4;->a(Lcom/facebook/events/model/Event;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 56461
    :catch_0
    move-exception v0

    .line 56462
    :try_start_2
    const-class v2, Lcom/facebook/events/data/EventsProvider;

    const-string v3, "Unexpected error when updating/inserting events: "

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 56463
    if-eqz v1, :cond_0

    .line 56464
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 56465
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 56466
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;LX/0TD;)V
    .locals 2

    .prologue
    .line 56467
    new-instance v0, Lcom/facebook/events/data/EventsProvider$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/events/data/EventsProvider$1;-><init>(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;)V

    const v1, 0x6215691e

    invoke-static {p3, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 56468
    return-void
.end method

.method public static a(Landroid/content/ContentResolver;LX/Bky;Ljava/lang/String;LX/0TD;)V
    .locals 1

    .prologue
    .line 56469
    new-instance v0, Lcom/facebook/events/data/EventsProvider$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/events/data/EventsProvider$3;-><init>(Landroid/content/ContentResolver;LX/Bky;Ljava/lang/String;)V

    invoke-interface {p3, v0}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 56470
    return-void
.end method

.method private static a(Landroid/content/ContentResolver;LX/Bky;Ljava/util/List;)V
    .locals 3
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/Bky;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56389
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56390
    sget-object v0, LX/Bkw;->b:LX/0U1;

    .line 56391
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 56392
    invoke-static {v0, p2}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    .line 56393
    iget-object v1, p1, LX/Bky;->h:Landroid/net/Uri;

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 56394
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/events/data/EventsProvider;LX/Bl3;LX/0SG;LX/Bky;)V
    .locals 0

    .prologue
    .line 56471
    iput-object p1, p0, Lcom/facebook/events/data/EventsProvider;->a:LX/Bl3;

    iput-object p2, p0, Lcom/facebook/events/data/EventsProvider;->b:LX/0SG;

    iput-object p3, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/data/EventsProvider;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/data/EventsProvider;

    invoke-static {v2}, LX/Bl3;->a(LX/0QB;)LX/Bl3;

    move-result-object v0

    check-cast v0, LX/Bl3;

    invoke-static {v2}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {v2}, LX/Bky;->b(LX/0QB;)LX/Bky;

    move-result-object v2

    check-cast v2, LX/Bky;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/data/EventsProvider;->a(Lcom/facebook/events/data/EventsProvider;LX/Bl3;LX/0SG;LX/Bky;)V

    return-void
.end method

.method public static b(Landroid/content/ContentResolver;LX/Bky;I)V
    .locals 1

    .prologue
    .line 56245
    invoke-static {p0, p1, p2}, Lcom/facebook/events/data/EventsProvider;->c(Landroid/content/ContentResolver;LX/Bky;I)Ljava/util/List;

    move-result-object v0

    .line 56246
    invoke-static {p0, p1, v0}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Ljava/util/List;)V

    .line 56247
    return-void
.end method

.method private static c(Landroid/content/ContentResolver;LX/Bky;I)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/Bky;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 56248
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, LX/Bkw;->b:LX/0U1;

    .line 56249
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 56250
    aput-object v1, v2, v0

    .line 56251
    sget-object v0, LX/Bkw;->I:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v5

    .line 56252
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 56253
    iget-object v1, p1, LX/Bky;->e:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 56254
    :try_start_0
    sget-object v0, LX/Bkw;->b:LX/0U1;

    .line 56255
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 56256
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 56257
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 56258
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-ge v3, p2, :cond_1

    .line 56259
    if-eqz v2, :cond_0

    .line 56260
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 56261
    :cond_0
    :goto_0
    return-object v0

    .line 56262
    :cond_1
    :try_start_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 56263
    :try_start_2
    invoke-interface {v2, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 56264
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_2

    .line 56265
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56266
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 56267
    :catch_0
    move-exception v1

    .line 56268
    :goto_2
    :try_start_3
    const-class v3, Lcom/facebook/events/data/EventsProvider;

    const-string v4, "Unexpected error when deleting past events: "

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 56269
    if-eqz v2, :cond_0

    .line 56270
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 56271
    :cond_2
    if-eqz v2, :cond_0

    .line 56272
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 56273
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_3

    .line 56274
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 56275
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    goto :goto_2
.end method

.method private d()Landroid/content/UriMatcher;
    .locals 4

    .prologue
    .line 56276
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 56277
    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->a:Ljava/lang/String;

    const-string v2, "events"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 56278
    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->a:Ljava/lang/String;

    const-string v2, "events/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 56279
    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->a:Ljava/lang/String;

    const-string v2, "events/facebook_id/#"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 56280
    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->a:Ljava/lang/String;

    const-string v2, "upcoming"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 56281
    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->a:Ljava/lang/String;

    const-string v2, "past"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 56282
    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->a:Ljava/lang/String;

    const-string v2, "invited"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 56283
    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->a:Ljava/lang/String;

    const-string v2, "hosting"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 56284
    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->a:Ljava/lang/String;

    const-string v2, "custom"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 56285
    return-object v0
.end method

.method private static e()LX/0ux;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 56286
    sget-object v0, LX/Bkw;->D:LX/0U1;

    const-string v1, "0"

    invoke-virtual {v0, v1}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 56287
    sget-object v1, LX/Bkw;->B:LX/0U1;

    .line 56288
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 56289
    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->HOST:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    const/4 v3, 0x4

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->LIKED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->SAVED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->SUBSCRIBED_ADMIN_CALENDAR:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 56290
    sget-object v2, LX/Bkw;->C:LX/0U1;

    .line 56291
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 56292
    new-array v3, v8, [Ljava/lang/String;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 56293
    new-array v3, v8, [LX/0ux;

    aput-object v0, v3, v5

    aput-object v1, v3, v6

    aput-object v2, v3, v7

    invoke-static {v3}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56294
    invoke-virtual {p0}, Lcom/facebook/events/data/EventsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 56295
    invoke-virtual {p0}, Lcom/facebook/events/data/EventsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 56296
    invoke-virtual {p0}, Lcom/facebook/events/data/EventsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->f:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 56297
    invoke-virtual {p0}, Lcom/facebook/events/data/EventsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v1, v1, LX/Bky;->g:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 56298
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 56299
    iget-object v0, p0, Lcom/facebook/events/data/EventsProvider;->a:LX/Bl3;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 56300
    iget-object v0, p0, Lcom/facebook/events/data/EventsProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 56301
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported URI for update: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56302
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Bky;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 56303
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v1

    .line 56304
    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    .line 56305
    :goto_0
    const-string v3, "events"

    invoke-virtual {v2, v3, p2, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 56306
    if-lez v0, :cond_0

    .line 56307
    invoke-virtual {p0}, Lcom/facebook/events/data/EventsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 56308
    invoke-direct {p0}, Lcom/facebook/events/data/EventsProvider;->f()V

    .line 56309
    :cond_0
    return v0

    .line 56310
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Bky;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 56311
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v1

    .line 56312
    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 56313
    iget-object v0, p0, Lcom/facebook/events/data/EventsProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 56314
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported URI for delete: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56315
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/events/data/EventsProvider;->a:LX/Bl3;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 56316
    const/4 v0, 0x0

    .line 56317
    :cond_0
    :goto_0
    return v0

    .line 56318
    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Bky;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 56319
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p2

    .line 56320
    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object p3

    .line 56321
    :goto_1
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/events/data/EventsProvider;->a:LX/Bl3;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 56322
    const-string v1, "events"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 56323
    if-lez v0, :cond_0

    .line 56324
    invoke-direct {p0}, Lcom/facebook/events/data/EventsProvider;->f()V

    goto :goto_0

    .line 56325
    :pswitch_4
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Bky;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 56326
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p2

    .line 56327
    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 56328
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 56329
    const-string v1, "events"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 56330
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 56331
    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 56332
    invoke-static {v2, v3}, Lcom/facebook/events/data/EventsProvider;->a(J)J

    move-result-wide v4

    .line 56333
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 56334
    invoke-static {v2, v3, v4, v5}, Lcom/facebook/events/data/EventsProvider;->a(JJ)LX/0ux;

    move-result-object v2

    .line 56335
    iget-object v3, p0, Lcom/facebook/events/data/EventsProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 56336
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Uri for query not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    move-object v4, p4

    move-object v3, p3

    .line 56337
    :goto_0
    invoke-static {p5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v7, LX/Bkx;->a:Ljava/lang/String;

    .line 56338
    :goto_1
    iget-object v1, p0, Lcom/facebook/events/data/EventsProvider;->a:LX/Bl3;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 56339
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 56340
    invoke-virtual {p0}, Lcom/facebook/events/data/EventsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 56341
    return-object v0

    .line 56342
    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/Bky;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 56343
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p3

    .line 56344
    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object p4

    move-object v4, p4

    move-object v3, p3

    .line 56345
    goto :goto_0

    .line 56346
    :pswitch_3
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/Bky;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 56347
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p3

    .line 56348
    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object p4

    move-object v4, p4

    move-object v3, p3

    .line 56349
    goto :goto_0

    .line 56350
    :pswitch_4
    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p3

    .line 56351
    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object p4

    move-object v4, p4

    move-object v3, p3

    .line 56352
    goto :goto_0

    .line 56353
    :pswitch_5
    sget-object v2, LX/Bkw;->I:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 56354
    const/4 v3, 0x2

    new-array v3, v3, [LX/0ux;

    const/4 v4, 0x0

    sget-object v5, LX/Bkw;->J:LX/0U1;

    invoke-virtual {v5, v1}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    sget-object v4, LX/Bkw;->J:LX/0U1;

    .line 56355
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 56356
    invoke-static {v4}, LX/0uu;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    invoke-static {v4}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v3}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v1

    .line 56357
    const/4 v3, 0x3

    new-array v3, v3, [LX/0ux;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v2, 0x1

    aput-object v1, v3, v2

    const/4 v1, 0x2

    invoke-static {}, Lcom/facebook/events/data/EventsProvider;->e()LX/0ux;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v3}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v1

    .line 56358
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p3

    .line 56359
    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object p4

    move-object v4, p4

    move-object v3, p3

    .line 56360
    goto/16 :goto_0

    .line 56361
    :pswitch_6
    const/4 v1, 0x2

    new-array v1, v1, [LX/0ux;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    sget-object v3, LX/Bkw;->B:LX/0U1;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v1

    .line 56362
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p3

    .line 56363
    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object p4

    move-object v4, p4

    move-object v3, p3

    .line 56364
    goto/16 :goto_0

    .line 56365
    :pswitch_7
    const/4 v1, 0x2

    new-array v1, v1, [LX/0ux;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    sget-object v3, LX/Bkw;->H:LX/0U1;

    const-string v4, "0"

    invoke-virtual {v3, v4}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v1

    .line 56366
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p3

    .line 56367
    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object p4

    move-object v4, p4

    move-object v3, p3

    .line 56368
    goto/16 :goto_0

    :pswitch_8
    move-object v4, p4

    move-object v3, p3

    .line 56369
    goto/16 :goto_0

    :cond_0
    move-object v7, p5

    .line 56370
    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 56371
    iget-object v0, p0, Lcom/facebook/events/data/EventsProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 56372
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Uri for insert not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56373
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/data/EventsProvider;->a:LX/Bl3;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 56374
    const-string v1, "events"

    const v2, 0x3cb36118

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v0, v1, v4, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const v2, 0x3e03e723

    invoke-static {v2}, LX/03h;->a(I)V

    .line 56375
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 56376
    iget-object v2, p0, Lcom/facebook/events/data/EventsProvider;->c:LX/Bky;

    iget-object v2, v2, LX/Bky;->b:Landroid/net/Uri;

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 56377
    invoke-virtual {p0}, Lcom/facebook/events/data/EventsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 56378
    invoke-direct {p0}, Lcom/facebook/events/data/EventsProvider;->f()V

    .line 56379
    return-object v0

    .line 56380
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Writing to DB failed for values: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 56381
    iget-object v0, p0, Lcom/facebook/events/data/EventsProvider;->d:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 56382
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported Uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56383
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.facebook.events.event"

    .line 56384
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "vnd.android.cursor.item/vnd.facebook.events.event"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 56385
    invoke-super {p0}, LX/0Ov;->a()V

    .line 56386
    const-class v0, Lcom/facebook/events/data/EventsProvider;

    invoke-static {v0, p0}, Lcom/facebook/events/data/EventsProvider;->a(Ljava/lang/Class;LX/02k;)V

    .line 56387
    invoke-direct {p0}, Lcom/facebook/events/data/EventsProvider;->d()Landroid/content/UriMatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/data/EventsProvider;->d:Landroid/content/UriMatcher;

    .line 56388
    return-void
.end method
