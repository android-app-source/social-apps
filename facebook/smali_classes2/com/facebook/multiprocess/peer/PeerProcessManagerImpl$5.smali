.class public final Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/os/Message;

.field public final synthetic b:LX/0cI;


# direct methods
.method public constructor <init>(LX/0cI;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 296133
    iput-object p1, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;->b:LX/0cI;

    iput-object p2, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;->a:Landroid/os/Message;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 296134
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 296135
    iget-object v0, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;->b:LX/0cI;

    iget-object v0, v0, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cK;

    .line 296136
    :try_start_0
    iget-object v1, v0, LX/0cK;->a:Landroid/os/Messenger;

    iget-object v4, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;->a:Landroid/os/Message;

    invoke-virtual {v1, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 296137
    :catch_0
    move-exception v1

    .line 296138
    instance-of v4, v1, Landroid/os/DeadObjectException;

    if-eqz v4, :cond_0

    .line 296139
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 296140
    :cond_0
    iget-object v4, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;->b:LX/0cI;

    iget-object v4, v4, LX/0cI;->e:LX/03V;

    const-class v5, LX/0cJ;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "RemoteException occurred when sending the message to peer "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/0cK;->c:LX/00G;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "; message: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;->a:Landroid/os/Message;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "; data keys: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", "

    invoke-static {v6}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;->a:Landroid/os/Message;

    invoke-virtual {v7}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "; peer info: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;->b:LX/0cI;

    iget-object v6, v6, LX/0cI;->f:LX/0cK;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 296141
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cK;

    .line 296142
    iget-object v2, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$5;->b:LX/0cI;

    invoke-static {v2, v0}, LX/0cI;->a$redex0(LX/0cI;LX/0cK;)V

    goto :goto_1

    .line 296143
    :cond_2
    return-void
.end method
