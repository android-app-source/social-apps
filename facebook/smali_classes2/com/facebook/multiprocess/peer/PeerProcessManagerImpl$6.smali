.class public final Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0cK;

.field public final synthetic b:Landroid/os/Message;

.field public final synthetic c:LX/0cI;


# direct methods
.method public constructor <init>(LX/0cI;LX/0cK;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 294217
    iput-object p1, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;->c:LX/0cI;

    iput-object p2, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;->a:LX/0cK;

    iput-object p3, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;->b:Landroid/os/Message;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 294218
    :try_start_0
    iget-object v0, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;->a:LX/0cK;

    iget-object v0, v0, LX/0cK;->a:Landroid/os/Messenger;

    iget-object v1, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;->b:Landroid/os/Message;

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 294219
    :goto_0
    return-void

    .line 294220
    :catch_0
    move-exception v0

    .line 294221
    instance-of v1, v0, Landroid/os/DeadObjectException;

    if-eqz v1, :cond_0

    .line 294222
    iget-object v0, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;->c:LX/0cI;

    iget-object v1, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;->a:LX/0cK;

    invoke-static {v0, v1}, LX/0cI;->a$redex0(LX/0cI;LX/0cK;)V

    goto :goto_0

    .line 294223
    :cond_0
    iget-object v1, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;->c:LX/0cI;

    iget-object v1, v1, LX/0cI;->e:LX/03V;

    const-class v2, LX/0cJ;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RemoteException occurred when sending the message to peer "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$6;->a:LX/0cK;

    iget-object v4, v4, LX/0cK;->c:LX/00G;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
