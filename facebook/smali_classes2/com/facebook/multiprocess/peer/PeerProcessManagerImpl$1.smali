.class public final Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0cI;

.field private b:I


# direct methods
.method public constructor <init>(LX/0cI;)V
    .locals 1

    .prologue
    .line 88172
    iput-object p1, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->a:LX/0cI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88173
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->b:I

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const-wide/32 v0, 0xea60

    .line 88162
    iget-object v2, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->a:LX/0cI;

    iget-object v2, v2, LX/0cI;->r:Landroid/content/Intent;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88163
    iget-object v2, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->a:LX/0cI;

    iget-object v2, v2, LX/0cI;->h:Landroid/os/Handler;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88164
    iget v2, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->b:I

    .line 88165
    iget v2, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->b:I

    const/4 v3, 0x5

    if-ge v2, v3, :cond_0

    .line 88166
    iget-object v2, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->a:LX/0cI;

    invoke-static {v2}, LX/0cI;->g$redex0(LX/0cI;)V

    .line 88167
    const/4 v2, 0x1

    iget v3, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->b:I

    shl-int/2addr v2, v3

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 88168
    cmp-long v4, v2, v0

    if-lez v4, :cond_1

    .line 88169
    :goto_0
    iget-object v2, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->a:LX/0cI;

    iget-object v2, v2, LX/0cI;->h:Landroid/os/Handler;

    iget-object v3, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->a:LX/0cI;

    iget-object v3, v3, LX/0cI;->s:Ljava/lang/Runnable;

    const v4, 0x2a7b4ef4

    invoke-static {v2, v3, v0, v1, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 88170
    :goto_1
    return-void

    .line 88171
    :cond_0
    iget-object v0, p0, Lcom/facebook/multiprocess/peer/PeerProcessManagerImpl$1;->a:LX/0cI;

    invoke-static {v0}, LX/0cI;->h(LX/0cI;)V

    goto :goto_1

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method
