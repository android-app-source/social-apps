.class public Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/flexiblesampling/SamplingPolicyConfig;
.implements LX/0Ya;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0XZ;

.field public c:LX/0lC;

.field public d:LX/2Y1;

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0mR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132490
    const-class v0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 132524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132525
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;

    invoke-static {v0}, LX/0XZ;->a(LX/0QB;)LX/0XZ;

    move-result-object v2

    check-cast v2, LX/0XZ;

    invoke-static {v0}, LX/2Y1;->a(LX/0QB;)LX/2Y1;

    move-result-object v3

    check-cast v3, LX/2Y1;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0mR;->b(LX/0QB;)LX/0mR;

    move-result-object v0

    check-cast v0, LX/0mR;

    iput-object v2, v1, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->b:LX/0XZ;

    iput-object v4, v1, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->c:LX/0lC;

    iput-object v3, v1, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->d:LX/2Y1;

    iput-object v5, v1, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->e:LX/0Or;

    iput-object v0, v1, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->f:LX/0mR;

    .line 132526
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132521
    iget-object v0, p0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->b:LX/0XZ;

    .line 132522
    iget-object p0, v0, LX/0XZ;->b:LX/0Xb;

    invoke-virtual {p0}, LX/0Xc;->a()Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 132523
    return-object v0
.end method

.method public final a(Ljava/io/InputStream;)V
    .locals 6

    .prologue
    .line 132494
    iget-object v0, p0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->c:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/io/InputStream;)LX/0lF;

    move-result-object v1

    .line 132495
    if-nez v1, :cond_1

    .line 132496
    sget-object v0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->a:Ljava/lang/String;

    const-string v1, "No content from Http response"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 132497
    :cond_0
    :goto_0
    return-void

    .line 132498
    :cond_1
    const-string v0, "checksum"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 132499
    const-string v2, "config"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 132500
    if-eqz v0, :cond_2

    if-nez v2, :cond_3

    .line 132501
    :cond_2
    sget-object v0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->a:Ljava/lang/String;

    const-string v2, "Incomplete response: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 132502
    :cond_3
    iget-object v3, p0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->b:LX/0XZ;

    new-instance v4, LX/2Yk;

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    const-string v0, "app_data"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_1
    invoke-direct {v4, v5, v2, v0}, LX/2Yk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132503
    if-eqz v4, :cond_4

    .line 132504
    iget-object v0, v4, LX/2Yk;->a:Ljava/lang/String;

    move-object v0, v0

    .line 132505
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 132506
    :cond_4
    :goto_2
    const-string v0, "app_data"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 132507
    if-eqz v0, :cond_0

    .line 132508
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 132509
    iget-object v1, p0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->c:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, "pigeon_internal"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 132510
    if-eqz v0, :cond_0

    const-string v1, "regenerate_deviceid"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 132511
    iget-object v0, p0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->d:LX/2Y1;

    invoke-virtual {v0}, LX/2Y1;->a()V

    goto :goto_0

    .line 132512
    :cond_5
    const-string v0, "app_data"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 132513
    :cond_6
    iget-object v0, v3, LX/0XZ;->b:LX/0Xb;

    invoke-virtual {v0}, LX/0Xc;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 132514
    :goto_3
    iget-object v0, v4, LX/2Yk;->a:Ljava/lang/String;

    move-object v0, v0

    .line 132515
    iget-object v2, v4, LX/2Yk;->b:Ljava/lang/String;

    move-object v2, v2

    .line 132516
    invoke-virtual {v3, v0, v2}, LX/0XZ;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 132517
    :cond_7
    new-instance v0, LX/0ap;

    iget-object v2, v3, LX/0XZ;->b:LX/0Xb;

    invoke-direct {v0, v2}, LX/0ap;-><init>(LX/0Xb;)V

    .line 132518
    iget-object v2, v3, LX/0XZ;->f:LX/0ap;

    if-eqz v2, :cond_8

    .line 132519
    iget-object v2, v3, LX/0XZ;->f:LX/0ap;

    new-instance v5, LX/3zj;

    iget-object p1, v3, LX/0XZ;->a:LX/0WS;

    invoke-virtual {p1}, LX/0WS;->a()Ljava/util/Map;

    move-result-object p1

    invoke-direct {v5, p1}, LX/3zj;-><init>(Ljava/util/Map;)V

    invoke-static {v2, v5}, LX/0ap;->a$redex0(LX/0ap;LX/3zj;)V

    .line 132520
    :cond_8
    iput-object v0, v3, LX/0XZ;->f:LX/0ap;

    goto :goto_3
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132493
    const-string v0, "v2"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132492
    iget-object v0, p0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132491
    iget-object v0, p0, Lcom/facebook/analytics/NewAnalyticsSamplingPolicyConfig;->f:LX/0mR;

    invoke-virtual {v0}, LX/0mR;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
