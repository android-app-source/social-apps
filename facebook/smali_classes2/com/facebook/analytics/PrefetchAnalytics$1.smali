.class public final Lcom/facebook/analytics/PrefetchAnalytics$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1Ep;


# direct methods
.method public constructor <init>(LX/1Ep;)V
    .locals 0

    .prologue
    .line 221271
    iput-object p1, p0, Lcom/facebook/analytics/PrefetchAnalytics$1;->a:LX/1Ep;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 221272
    iget-object v0, p0, Lcom/facebook/analytics/PrefetchAnalytics$1;->a:LX/1Ep;

    iget-object v0, v0, LX/1Ep;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 221273
    iget-object v0, p0, Lcom/facebook/analytics/PrefetchAnalytics$1;->a:LX/1Ep;

    const/4 v4, 0x0

    .line 221274
    iget-object v1, v0, LX/1Ep;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 221275
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 221276
    new-instance p0, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {p0, v2}, LX/162;-><init>(LX/0mC;)V

    move v3, v4

    .line 221277
    :goto_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0m9;

    if-eqz v2, :cond_1

    .line 221278
    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p0, v3, v2}, LX/162;->a(ILX/0lF;)LX/162;

    move v3, v5

    goto :goto_1

    .line 221279
    :cond_1
    iget-object v2, v0, LX/1Ep;->b:LX/0Zb;

    const-string v3, "prefetch_cache_efficiency"

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 221280
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 221281
    const-string v3, "cache_name"

    iget-object v5, v0, LX/1Ep;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 221282
    const-string v3, "query_name"

    iget-object v5, v0, LX/1Ep;->f:Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 221283
    const-string v3, "action_type"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 221284
    const-string v1, "cache_entries"

    invoke-virtual {v2, v1, p0}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 221285
    invoke-virtual {v2}, LX/0oG;->d()V

    goto :goto_0

    .line 221286
    :cond_2
    return-void
.end method
