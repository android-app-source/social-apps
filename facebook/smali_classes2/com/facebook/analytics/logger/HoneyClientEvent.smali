.class public Lcom/facebook/analytics/logger/HoneyClientEvent;
.super Lcom/facebook/analytics/HoneyAnalyticsEvent;
.source ""


# instance fields
.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field private i:LX/0m9;

.field public j:Z

.field public k:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 131790
    const-string v0, "client_event"

    invoke-direct {p0, v0, p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 131791
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->k:Z

    .line 131792
    return-void
.end method

.method private m(Ljava/lang/String;)LX/0lF;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 131793
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Invalid Key"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 131794
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    if-nez v0, :cond_2

    move-object v0, v1

    .line 131795
    :cond_0
    :goto_1
    return-object v0

    .line 131796
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 131797
    :cond_2
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-virtual {v0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 131798
    if-nez v0, :cond_0

    move-object v0, v1

    .line 131799
    goto :goto_1
.end method

.method private t()V
    .locals 2

    .prologue
    .line 131800
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    if-nez v0, :cond_0

    .line 131801
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    .line 131802
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 131803
    invoke-direct {p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->t()V

    .line 131804
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-virtual {v0, p1, p2, p3}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 131805
    return-object p0
.end method

.method public final a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1

    .prologue
    .line 131806
    invoke-direct {p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->t()V

    .line 131807
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-virtual {v0, p1, p2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 131808
    return-object p0
.end method

.method public final a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 131809
    invoke-direct {p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->t()V

    .line 131810
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-virtual {v0, p1, p2, p3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 131811
    return-object p0
.end method

.method public final a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1
    .param p2    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 131812
    invoke-direct {p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->t()V

    .line 131813
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-virtual {v0, p1, p2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 131814
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 131764
    if-nez p2, :cond_0

    .line 131765
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1

    .prologue
    .line 131815
    invoke-direct {p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->t()V

    .line 131816
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-virtual {v0, p1, p2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 131817
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;"
        }
    .end annotation

    .prologue
    .line 131818
    if-nez p1, :cond_1

    .line 131819
    :cond_0
    return-object p0

    .line 131820
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 131821
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 131822
    instance-of v3, v1, LX/0lF;

    if-eqz v3, :cond_2

    .line 131823
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v1, LX/0lF;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 131824
    :cond_2
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 131825
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 131826
    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1

    .prologue
    .line 131827
    iput-boolean p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->j:Z

    .line 131828
    const-string v0, "sponsored"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 131829
    return-object p0
.end method

.method public final a(LX/0n9;)V
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 131830
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    if-eqz v0, :cond_0

    .line 131831
    :try_start_0
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-static {v0, p1}, LX/0nG;->a(LX/0m9;LX/0n9;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131832
    :cond_0
    return-void

    .line 131833
    :catch_0
    move-exception v0

    .line 131834
    new-instance v1, Ljava/lang/IllegalArgumentException;

    iget-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 131784
    invoke-direct {p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->t()V

    .line 131785
    if-eqz p2, :cond_0

    .line 131786
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-virtual {v0, p1, p2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131787
    :cond_0
    return-object p0
.end method

.method public final f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 0

    .prologue
    .line 131788
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 131789
    return-object p0
.end method

.method public final g(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 131721
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 131722
    return-object p0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131723
    invoke-virtual {p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 131724
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 131725
    return-object p0
.end method

.method public final h()Ljava/lang/String;
    .locals 6

    .prologue
    .line 131726
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 131727
    const-string v1, "time"

    .line 131728
    iget-wide v4, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    move-wide v2, v4

    .line 131729
    invoke-static {v2, v3}, LX/3za;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131730
    const-string v1, "log_type"

    .line 131731
    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    move-object v2, v2

    .line 131732
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131733
    const-string v1, "name"

    .line 131734
    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 131735
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131736
    iget-object v1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 131737
    const-string v1, "module"

    iget-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131738
    :cond_0
    iget-object v1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 131739
    const-string v1, "obj_type"

    iget-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131740
    :cond_1
    iget-object v1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 131741
    const-string v1, "obj_id"

    iget-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131742
    :cond_2
    iget-object v1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 131743
    const-string v1, "uuid"

    iget-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131744
    :cond_3
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i:Ljava/lang/String;

    move-object v1, v1

    .line 131745
    if-eqz v1, :cond_4

    const-string v2, "AUTO_SET"

    if-eq v1, v2, :cond_4

    .line 131746
    const-string v2, "process"

    invoke-virtual {p0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 131747
    :cond_4
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->k:LX/162;

    move-object v1, v1

    .line 131748
    if-eqz v1, :cond_5

    .line 131749
    const-string v2, "enabled_features"

    invoke-virtual {p0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 131750
    :cond_5
    iget-object v1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    if-eqz v1, :cond_6

    .line 131751
    const-string v1, "extra"

    iget-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 131752
    :cond_6
    iget-object v1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->g:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 131753
    const-string v1, "interface"

    iget-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131754
    const-string v1, "src_interface"

    iget-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131755
    :cond_7
    iget-object v1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->h:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 131756
    const-string v1, "dst_interface"

    iget-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131757
    :cond_8
    iget-boolean v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g:Z

    move v1, v1

    .line 131758
    if-eqz v1, :cond_9

    .line 131759
    const-string v1, "bg"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 131760
    :cond_9
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 131761
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 131762
    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 131763
    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1

    .prologue
    .line 131766
    const-string v0, "fbobj"

    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 131767
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 131768
    return-object p0
.end method

.method public final j(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 131769
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 131770
    return-object p0
.end method

.method public final k(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 0

    .prologue
    .line 131771
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->g:Ljava/lang/String;

    .line 131772
    return-object p0
.end method

.method public final l(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 131773
    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->m(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 131774
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final r()LX/0lF;
    .locals 1

    .prologue
    .line 131775
    const-string v0, "tracking"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->m(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 131776
    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->i:LX/0m9;

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 131777
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 131778
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    move-object v1, v1

    .line 131779
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 131780
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v1, v1

    .line 131781
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 131782
    iget-object v1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    move-object v1, v1

    .line 131783
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
