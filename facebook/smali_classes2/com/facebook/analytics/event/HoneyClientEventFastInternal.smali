.class public Lcom/facebook/analytics/event/HoneyClientEventFastInternal;
.super Lcom/facebook/analytics/HoneyAnalyticsEvent;
.source ""


# static fields
.field private static final c:LX/0Zh;


# instance fields
.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:LX/0n9;

.field public k:Z

.field private l:Z

.field private m:Z

.field private final n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132327
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    sput-object v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->c:LX/0Zh;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 132324
    const-string v0, "client_event"

    invoke-direct {p0, v0, p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 132325
    iput-boolean p2, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->n:Z

    .line 132326
    return-void
.end method

.method private r()V
    .locals 2

    .prologue
    .line 132215
    iget-boolean v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->l:Z

    if-nez v0, :cond_0

    .line 132216
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "isSampled was not invoked, how can you have known?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132217
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->n:Z

    if-nez v0, :cond_1

    .line 132218
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Event is not sampled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132219
    :cond_1
    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 132321
    iget-boolean v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->m:Z

    if-eqz v0, :cond_0

    .line 132322
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You have already ejected params."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132323
    :cond_0
    return-void
.end method

.method public static t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V
    .locals 0

    .prologue
    .line 132318
    invoke-direct {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->r()V

    .line 132319
    invoke-direct {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->s()V

    .line 132320
    return-void
.end method

.method public static u(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V
    .locals 2

    .prologue
    .line 132314
    iget-object v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    if-nez v0, :cond_0

    .line 132315
    sget-object v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->c:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    .line 132316
    iget-object v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0nA;->a(LX/0nD;)V

    .line 132317
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;
    .locals 2

    .prologue
    .line 132309
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132310
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->u(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132311
    iget-object v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 132312
    invoke-static {v0, p1, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132313
    return-object p0
.end method

.method public final a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;
    .locals 3
    .param p2    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 132303
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132304
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->u(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132305
    :try_start_0
    iget-object v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    invoke-static {p1, p2, v0}, LX/0nG;->a(Ljava/lang/String;LX/0lF;LX/0n9;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132306
    return-object p0

    .line 132307
    :catch_0
    move-exception v0

    .line 132308
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 132300
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132301
    if-nez p2, :cond_0

    .line 132302
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;
    .locals 2

    .prologue
    .line 132295
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132296
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->u(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132297
    iget-object v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 132298
    invoke-static {v0, p1, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132299
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 132289
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132290
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->u(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132291
    if-eqz p2, :cond_0

    .line 132292
    iget-object v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    .line 132293
    invoke-static {v0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132294
    :cond_0
    return-object p0
.end method

.method public final h()Ljava/lang/String;
    .locals 8

    .prologue
    .line 132237
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132238
    sget-object v0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->c:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v0

    .line 132239
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0nA;->a(LX/0nD;)V

    .line 132240
    const-string v2, "time"

    .line 132241
    iget-wide v6, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    move-wide v4, v6

    .line 132242
    invoke-static {v4, v5}, LX/3za;->a(J)Ljava/lang/String;

    move-result-object v3

    .line 132243
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132244
    const-string v2, "log_type"

    .line 132245
    iget-object v3, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    move-object v3, v3

    .line 132246
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132247
    const-string v2, "name"

    .line 132248
    iget-object v3, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v3, v3

    .line 132249
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132250
    iget-object v2, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 132251
    const-string v2, "module"

    iget-object v3, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->d:Ljava/lang/String;

    .line 132252
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132253
    :cond_0
    iget-object v2, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 132254
    const-string v2, "obj_type"

    iget-object v3, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->e:Ljava/lang/String;

    .line 132255
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132256
    :cond_1
    iget-object v2, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 132257
    const-string v2, "obj_id"

    iget-object v3, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->f:Ljava/lang/String;

    .line 132258
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132259
    :cond_2
    iget-object v2, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->g:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 132260
    const-string v2, "uuid"

    iget-object v3, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->g:Ljava/lang/String;

    .line 132261
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132262
    :cond_3
    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i:Ljava/lang/String;

    move-object v2, v2

    .line 132263
    if-eqz v2, :cond_4

    const-string v3, "AUTO_SET"

    if-eq v2, v3, :cond_4

    .line 132264
    const-string v3, "process"

    invoke-virtual {p0, v3, v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 132265
    :cond_4
    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->k:LX/162;

    move-object v2, v2

    .line 132266
    if-eqz v2, :cond_5

    .line 132267
    const-string v3, "enabled_features"

    invoke-virtual {p0, v3, v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 132268
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->p()LX/0n9;

    move-result-object v2

    .line 132269
    if-eqz v2, :cond_6

    .line 132270
    const-string v3, "extra"

    invoke-virtual {v0, v3, v2}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 132271
    :cond_6
    iget-object v2, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->h:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 132272
    const-string v2, "interface"

    iget-object v3, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->h:Ljava/lang/String;

    .line 132273
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132274
    const-string v2, "src_interface"

    iget-object v3, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->h:Ljava/lang/String;

    .line 132275
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132276
    :cond_7
    iget-object v2, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->i:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 132277
    const-string v2, "dst_interface"

    iget-object v3, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->i:Ljava/lang/String;

    .line 132278
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132279
    :cond_8
    iget-boolean v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g:Z

    move v2, v2

    .line 132280
    if-eqz v2, :cond_9

    .line 132281
    const-string v2, "bg"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 132282
    invoke-static {v0, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132283
    :cond_9
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 132284
    :try_start_0
    invoke-virtual {v0, v1}, LX/0nA;->a(Ljava/io/Writer;)V

    .line 132285
    invoke-virtual {v0}, LX/0nA;->a()V

    .line 132286
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 132287
    :catch_0
    move-exception v0

    .line 132288
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 132234
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 132235
    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 132236
    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 132232
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->l:Z

    .line 132233
    iget-boolean v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->n:Z

    return v0
.end method

.method public final p()LX/0n9;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 132227
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132228
    iget-object v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    .line 132229
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->j:LX/0n9;

    .line 132230
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->m:Z

    .line 132231
    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132225
    invoke-static {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->t(Lcom/facebook/analytics/event/HoneyClientEventFastInternal;)V

    .line 132226
    iget-object v0, p0, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 132220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 132221
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    move-object v1, v1

    .line 132222
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 132223
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v1, v1

    .line 132224
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
