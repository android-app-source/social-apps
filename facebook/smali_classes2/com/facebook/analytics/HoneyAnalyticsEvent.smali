.class public Lcom/facebook/analytics/HoneyAnalyticsEvent;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/analytics/HoneyAnalyticsEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:[Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Ljava/util/Map;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:J

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:LX/162;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 131925
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "log_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "time"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "uid"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "bg"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->c:[Ljava/lang/String;

    .line 131926
    new-instance v0, LX/0m8;

    invoke-direct {v0}, LX/0m8;-><init>()V

    sput-object v0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    .line 131898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131899
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 131900
    const-string v0, "AUTO_SET"

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->f:Ljava/lang/String;

    .line 131901
    const-string v0, "AUTO_SET"

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i:Ljava/lang/String;

    .line 131902
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 131903
    sget-object v2, Lcom/facebook/analytics/HoneyAnalyticsEvent;->c:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 131904
    invoke-virtual {v1, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 131905
    new-instance v0, Landroid/os/ParcelFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Missing required field "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 131906
    :catch_0
    move-exception v0

    .line 131907
    new-instance v1, Landroid/os/ParcelFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to process event "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/28F;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131908
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131909
    :cond_1
    :try_start_1
    const-string v0, "name"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    .line 131910
    const-string v0, "log_type"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    .line 131911
    const-string v0, "session_id"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->h:Ljava/lang/String;

    .line 131912
    const-string v0, "time"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->x()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 131913
    const-string v0, "uid"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->f:Ljava/lang/String;

    .line 131914
    const-string v0, "bg"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g:Z

    .line 131915
    const-string v0, "data"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->j:Ljava/lang/String;

    .line 131916
    const-string v0, "tags"

    invoke-virtual {v1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 131917
    const-string v0, "tags"

    invoke-virtual {v1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 131918
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;Z)Ljava/util/Map;

    move-result-object v1

    .line 131919
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 131920
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 131921
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch LX/28F; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 131922
    :catch_1
    move-exception v0

    .line 131923
    new-instance v1, Landroid/os/ParcelFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to process event "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131924
    :cond_2
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 131890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131891
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 131892
    const-string v0, "AUTO_SET"

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->f:Ljava/lang/String;

    .line 131893
    const-string v0, "AUTO_SET"

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->i:Ljava/lang/String;

    .line 131894
    iput-object p1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    .line 131895
    const-string v0, "AUTO_SET"

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->f:Ljava/lang/String;

    .line 131896
    iput-object p2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    .line 131897
    return-void
.end method

.method public static declared-synchronized a(Lcom/facebook/analytics/HoneyAnalyticsEvent;Z)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131885
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 131886
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->b:Ljava/util/Map;

    .line 131887
    :cond_0
    iget-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->b:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 131888
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(J)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 1

    .prologue
    .line 131883
    iput-wide p1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 131884
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/facebook/analytics/HoneyAnalyticsEvent;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 131881
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;Z)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131882
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 131889
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 131877
    invoke-static {p0, v0}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;Z)Ljava/util/Map;

    move-result-object v1

    .line 131878
    if-eqz v1, :cond_0

    .line 131879
    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 131880
    :cond_0
    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131876
    invoke-virtual {p0}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131875
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 131869
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 131870
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;Z)Ljava/util/Map;

    move-result-object v2

    .line 131871
    if-eqz v2, :cond_0

    .line 131872
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 131873
    :goto_0
    move-object v2, v2

    .line 131874
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131835
    iget-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131867
    iget-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    move-object v0, v0

    .line 131868
    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 8

    .prologue
    .line 131836
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 131837
    const-string v0, "name"

    .line 131838
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v1, v1

    .line 131839
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131840
    const-string v0, "log_type"

    .line 131841
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    move-object v1, v1

    .line 131842
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131843
    const-string v0, "session_id"

    .line 131844
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->h:Ljava/lang/String;

    move-object v1, v1

    .line 131845
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131846
    const-string v0, "time"

    .line 131847
    iget-wide v6, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    move-wide v4, v6

    .line 131848
    invoke-virtual {v2, v0, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 131849
    const-string v0, "uid"

    .line 131850
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->f:Ljava/lang/String;

    move-object v1, v1

    .line 131851
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131852
    const-string v0, "bg"

    .line 131853
    iget-boolean v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g:Z

    move v1, v1

    .line 131854
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 131855
    const-string v0, "data"

    .line 131856
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->j:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 131857
    invoke-virtual {p0}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->j:Ljava/lang/String;

    .line 131858
    :cond_0
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->j:Ljava/lang/String;

    move-object v1, v1

    .line 131859
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 131860
    iget-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->b:Ljava/util/Map;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 131861
    new-instance v3, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 131862
    iget-object v0, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 131863
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 131864
    :cond_1
    const-string v0, "tags"

    invoke-virtual {v2, v0, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 131865
    :cond_2
    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 131866
    return-void
.end method
