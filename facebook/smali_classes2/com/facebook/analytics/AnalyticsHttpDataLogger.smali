.class public Lcom/facebook/analytics/AnalyticsHttpDataLogger;
.super LX/1iY;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/analytics/NetworkDataLogger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 298541
    const-class v0, Lcom/facebook/analytics/AnalyticsHttpDataLogger;

    sput-object v0, Lcom/facebook/analytics/AnalyticsHttpDataLogger;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/analytics/NetworkDataLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 298497
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 298498
    iput-object p1, p0, Lcom/facebook/analytics/AnalyticsHttpDataLogger;->b:Lcom/facebook/analytics/NetworkDataLogger;

    .line 298499
    return-void
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 298500
    iget-object v0, p0, Lcom/facebook/analytics/AnalyticsHttpDataLogger;->b:Lcom/facebook/analytics/NetworkDataLogger;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 15

    .prologue
    .line 298501
    iget-object v0, p0, Lcom/facebook/analytics/AnalyticsHttpDataLogger;->b:Lcom/facebook/analytics/NetworkDataLogger;

    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    .line 298502
    iget-object v2, p0, LX/1iY;->d:LX/1iW;

    move-object v2, v2

    .line 298503
    invoke-virtual {p0}, LX/1iY;->a()Lorg/apache/http/protocol/HttpContext;

    move-result-object v3

    .line 298504
    iget-object v4, p0, LX/1iY;->c:Lorg/apache/http/HttpResponse;

    move-object v4, v4

    .line 298505
    invoke-static {v3}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v5

    .line 298506
    iget-object v6, v5, LX/1iV;->f:Lcom/facebook/common/callercontext/CallerContext;

    move-object v5, v6

    .line 298507
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v5}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v5

    const-string v6, "audio_upload"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 298508
    const-string v5, "audio_upload_sent"

    .line 298509
    :goto_0
    iget-object v6, v0, Lcom/facebook/analytics/NetworkDataLogger;->e:LX/0Uo;

    invoke-virtual {v6}, LX/0Uo;->j()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 298510
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_bg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 298511
    :cond_0
    move-object v5, v5

    .line 298512
    invoke-static {v0, v3, v1}, Lcom/facebook/analytics/NetworkDataLogger;->b(Lcom/facebook/analytics/NetworkDataLogger;Lorg/apache/http/protocol/HttpContext;Ljava/net/URI;)Ljava/lang/String;

    move-result-object v6

    .line 298513
    invoke-virtual {v2}, LX/1iW;->i()J

    move-result-wide v7

    .line 298514
    iget-object v9, v0, Lcom/facebook/analytics/NetworkDataLogger;->c:LX/0Vw;

    invoke-virtual {v9, v5, v7, v8}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 298515
    iget-object v5, v0, Lcom/facebook/analytics/NetworkDataLogger;->d:LX/1ia;

    invoke-virtual {v5, v6, v7, v8}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 298516
    if-eqz v4, :cond_3

    .line 298517
    invoke-static {v4}, LX/1Gl;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v5

    .line 298518
    invoke-static {v5}, Lcom/facebook/analytics/NetworkDataLogger;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 298519
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, v0, Lcom/facebook/analytics/NetworkDataLogger;->f:LX/1ib;

    invoke-virtual {v8, v1}, LX/1ib;->a(Ljava/net/URI;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_received"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 298520
    iget-object v7, v0, Lcom/facebook/analytics/NetworkDataLogger;->e:LX/0Uo;

    invoke-virtual {v7}, LX/0Uo;->j()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 298521
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_bg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 298522
    :cond_1
    move-object v7, v6

    .line 298523
    invoke-static {v0, v3, v1, v5}, Lcom/facebook/analytics/NetworkDataLogger;->a(Lcom/facebook/analytics/NetworkDataLogger;Lorg/apache/http/protocol/HttpContext;Ljava/net/URI;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 298524
    iget-object v5, v2, LX/1iW;->responseHeaderBytes:LX/1iX;

    .line 298525
    iget-wide v13, v5, LX/1iX;->a:J

    move-wide v5, v13

    .line 298526
    iget-object v9, v2, LX/1iW;->responseBodyBytes:LX/1iX;

    .line 298527
    iget-wide v13, v9, LX/1iX;->a:J

    move-wide v9, v13

    .line 298528
    const-wide/16 v11, 0x0

    cmp-long v11, v9, v11

    if-ltz v11, :cond_2

    add-long/2addr v5, v9

    .line 298529
    :cond_2
    iget-object v9, v0, Lcom/facebook/analytics/NetworkDataLogger;->c:LX/0Vw;

    invoke-virtual {v9, v7, v5, v6}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 298530
    iget-object v7, v0, Lcom/facebook/analytics/NetworkDataLogger;->d:LX/1ia;

    invoke-virtual {v7, v8, v5, v6}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 298531
    :cond_3
    return-void

    .line 298532
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v0, Lcom/facebook/analytics/NetworkDataLogger;->f:LX/1ib;

    invoke-virtual {v6, v1}, LX/1ib;->a(Ljava/net/URI;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_sent"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 1
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 298533
    invoke-super/range {p0 .. p5}, LX/1iY;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    .line 298534
    invoke-direct {p0}, Lcom/facebook/analytics/AnalyticsHttpDataLogger;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298535
    invoke-direct {p0}, Lcom/facebook/analytics/AnalyticsHttpDataLogger;->f()V

    .line 298536
    :cond_0
    return-void
.end method

.method public final b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 1

    .prologue
    .line 298537
    invoke-super {p0, p1, p2}, LX/1iY;->b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 298538
    invoke-direct {p0}, Lcom/facebook/analytics/AnalyticsHttpDataLogger;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298539
    invoke-direct {p0}, Lcom/facebook/analytics/AnalyticsHttpDataLogger;->f()V

    .line 298540
    :cond_0
    return-void
.end method
