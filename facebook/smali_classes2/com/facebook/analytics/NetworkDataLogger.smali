.class public Lcom/facebook/analytics/NetworkDataLogger;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/01T;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile j:Lcom/facebook/analytics/NetworkDataLogger;


# instance fields
.field public final c:LX/0Vw;

.field public final d:LX/1ia;

.field public final e:LX/0Uo;

.field public final f:LX/1ib;

.field private final g:LX/00H;

.field private final h:LX/13x;

.field private final i:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 298633
    const-class v0, Lcom/facebook/analytics/NetworkDataLogger;

    sput-object v0, Lcom/facebook/analytics/NetworkDataLogger;->a:Ljava/lang/Class;

    .line 298634
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/analytics/NetworkDataLogger;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/0Vw;LX/1ia;LX/0Uo;LX/1ib;LX/00H;LX/13x;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 298624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298625
    iput-object p1, p0, Lcom/facebook/analytics/NetworkDataLogger;->c:LX/0Vw;

    .line 298626
    iput-object p2, p0, Lcom/facebook/analytics/NetworkDataLogger;->d:LX/1ia;

    .line 298627
    iput-object p3, p0, Lcom/facebook/analytics/NetworkDataLogger;->e:LX/0Uo;

    .line 298628
    iput-object p4, p0, Lcom/facebook/analytics/NetworkDataLogger;->f:LX/1ib;

    .line 298629
    iput-object p5, p0, Lcom/facebook/analytics/NetworkDataLogger;->g:LX/00H;

    .line 298630
    iput-object p6, p0, Lcom/facebook/analytics/NetworkDataLogger;->h:LX/13x;

    .line 298631
    iput-object p7, p0, Lcom/facebook/analytics/NetworkDataLogger;->i:LX/0Uh;

    .line 298632
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/analytics/NetworkDataLogger;
    .locals 11

    .prologue
    .line 298611
    sget-object v0, Lcom/facebook/analytics/NetworkDataLogger;->j:Lcom/facebook/analytics/NetworkDataLogger;

    if-nez v0, :cond_1

    .line 298612
    const-class v1, Lcom/facebook/analytics/NetworkDataLogger;

    monitor-enter v1

    .line 298613
    :try_start_0
    sget-object v0, Lcom/facebook/analytics/NetworkDataLogger;->j:Lcom/facebook/analytics/NetworkDataLogger;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 298614
    if-eqz v2, :cond_0

    .line 298615
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 298616
    new-instance v3, Lcom/facebook/analytics/NetworkDataLogger;

    invoke-static {v0}, LX/0Vw;->a(LX/0QB;)LX/0Vw;

    move-result-object v4

    check-cast v4, LX/0Vw;

    invoke-static {v0}, LX/1ia;->a(LX/0QB;)LX/1ia;

    move-result-object v5

    check-cast v5, LX/1ia;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v6

    check-cast v6, LX/0Uo;

    invoke-static {v0}, LX/1ib;->b(LX/0QB;)LX/1ib;

    move-result-object v7

    check-cast v7, LX/1ib;

    const-class v8, LX/00H;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/00H;

    invoke-static {v0}, LX/13x;->a(LX/0QB;)LX/13x;

    move-result-object v9

    check-cast v9, LX/13x;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/analytics/NetworkDataLogger;-><init>(LX/0Vw;LX/1ia;LX/0Uo;LX/1ib;LX/00H;LX/13x;LX/0Uh;)V

    .line 298617
    move-object v0, v3

    .line 298618
    sput-object v0, Lcom/facebook/analytics/NetworkDataLogger;->j:Lcom/facebook/analytics/NetworkDataLogger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298619
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 298620
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 298621
    :cond_1
    sget-object v0, Lcom/facebook/analytics/NetworkDataLogger;->j:Lcom/facebook/analytics/NetworkDataLogger;

    return-object v0

    .line 298622
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 298623
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/analytics/NetworkDataLogger;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
    .locals 3
    .param p0    # Lcom/facebook/analytics/NetworkDataLogger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 298635
    if-nez p1, :cond_1

    .line 298636
    :cond_0
    :goto_0
    return-object v0

    .line 298637
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/facebook/analytics/NetworkDataLogger;->b:LX/0Px;

    iget-object v2, p0, Lcom/facebook/analytics/NetworkDataLogger;->g:LX/00H;

    .line 298638
    iget-object p0, v2, LX/00H;->j:LX/01T;

    move-object v2, p0

    .line 298639
    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 298640
    :cond_2
    iget-object v0, p1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v0, v0

    .line 298641
    goto :goto_0
.end method

.method public static a(Lcom/facebook/analytics/NetworkDataLogger;Lorg/apache/http/protocol/HttpContext;Ljava/net/URI;Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 298599
    invoke-static {p1}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v0

    .line 298600
    iget-object v1, v0, LX/1iV;->f:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v1

    .line 298601
    invoke-static {p3}, Lcom/facebook/analytics/NetworkDataLogger;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 298602
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/facebook/analytics/NetworkDataLogger;->f:LX/1ib;

    invoke-virtual {v3, p2}, LX/1ib;->a(Ljava/net/URI;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 298603
    const-string v0, "ACTIVE"

    .line 298604
    iget-object v3, p0, Lcom/facebook/analytics/NetworkDataLogger;->e:LX/0Uo;

    invoke-virtual {v3}, LX/0Uo;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 298605
    const-string v0, "BACKGROUND"

    .line 298606
    :cond_0
    iget-object v3, p0, Lcom/facebook/analytics/NetworkDataLogger;->i:LX/0Uh;

    const/16 v4, 0x48d

    invoke-virtual {v3, v4, v7}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 298607
    const-string v3, ":"

    invoke-static {v3}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v3

    const-string v4, "unknown"

    invoke-virtual {v3, v4}, LX/0PO;->useForNull(Ljava/lang/String;)LX/0PO;

    move-result-object v3

    invoke-static {p0, v1}, Lcom/facebook/analytics/NetworkDataLogger;->a(Lcom/facebook/analytics/NetworkDataLogger;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "RECEIVED"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v7

    aput-object v0, v6, v8

    if-nez v1, :cond_1

    const-string v0, "unknown"

    :goto_0
    aput-object v0, v6, v9

    iget-object v0, p0, Lcom/facebook/analytics/NetworkDataLogger;->h:LX/13x;

    invoke-virtual {v0}, LX/13x;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v10

    invoke-virtual {v3, v4, v5, v6}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 298608
    :goto_1
    return-object v0

    .line 298609
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 298610
    :cond_2
    const-string v3, ":"

    invoke-static {v3}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v3

    const-string v4, "unknown"

    invoke-virtual {v3, v4}, LX/0PO;->useForNull(Ljava/lang/String;)LX/0PO;

    move-result-object v3

    invoke-static {p0, v1}, Lcom/facebook/analytics/NetworkDataLogger;->a(Lcom/facebook/analytics/NetworkDataLogger;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "RECEIVED"

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v2, v6, v7

    aput-object v0, v6, v8

    if-nez v1, :cond_3

    const-string v0, "unknown"

    :goto_2
    aput-object v0, v6, v9

    invoke-virtual {v3, v4, v5, v6}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 298582
    const-string v0, ""

    .line 298583
    if-eqz p0, :cond_0

    .line 298584
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 298585
    if-lez v1, :cond_0

    .line 298586
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 298587
    :cond_0
    return-object v0
.end method

.method public static b(Lcom/facebook/analytics/NetworkDataLogger;Lorg/apache/http/protocol/HttpContext;Ljava/net/URI;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 298588
    invoke-static {p1}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v0

    .line 298589
    iget-object v2, v0, LX/1iV;->f:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 298590
    iget-object v0, p0, Lcom/facebook/analytics/NetworkDataLogger;->f:LX/1ib;

    invoke-virtual {v0, p2}, LX/1ib;->a(Ljava/net/URI;)Ljava/lang/String;

    move-result-object v3

    .line 298591
    const-string v0, "ACTIVE"

    .line 298592
    iget-object v4, p0, Lcom/facebook/analytics/NetworkDataLogger;->e:LX/0Uo;

    invoke-virtual {v4}, LX/0Uo;->j()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 298593
    const-string v0, "BACKGROUND"

    .line 298594
    :cond_0
    iget-object v4, p0, Lcom/facebook/analytics/NetworkDataLogger;->i:LX/0Uh;

    const/16 v5, 0x48d

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 298595
    const-string v4, ":"

    invoke-static {v4}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v4

    const-string v5, "unknown"

    invoke-virtual {v4, v5}, LX/0PO;->useForNull(Ljava/lang/String;)LX/0PO;

    move-result-object v4

    invoke-static {p0, v2}, Lcom/facebook/analytics/NetworkDataLogger;->a(Lcom/facebook/analytics/NetworkDataLogger;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "SENT"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v3, v7, v8

    aput-object v0, v7, v9

    if-nez v2, :cond_1

    move-object v0, v1

    :goto_0
    aput-object v0, v7, v10

    iget-object v0, p0, Lcom/facebook/analytics/NetworkDataLogger;->h:LX/13x;

    invoke-virtual {v0}, LX/13x;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v11

    invoke-virtual {v4, v5, v6, v7}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 298596
    :goto_1
    return-object v0

    .line 298597
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 298598
    :cond_2
    const-string v4, ":"

    invoke-static {v4}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v4

    const-string v5, "unknown"

    invoke-virtual {v4, v5}, LX/0PO;->useForNull(Ljava/lang/String;)LX/0PO;

    move-result-object v4

    invoke-static {p0, v2}, Lcom/facebook/analytics/NetworkDataLogger;->a(Lcom/facebook/analytics/NetworkDataLogger;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "SENT"

    new-array v7, v11, [Ljava/lang/Object;

    aput-object v3, v7, v8

    aput-object v0, v7, v9

    if-nez v2, :cond_3

    :goto_2
    aput-object v1, v7, v10

    invoke-virtual {v4, v5, v6, v7}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method
