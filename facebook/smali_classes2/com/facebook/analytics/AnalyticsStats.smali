.class public Lcom/facebook/analytics/AnalyticsStats;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:Lcom/facebook/analytics/AnalyticsStats;


# instance fields
.field private final a:LX/0So;

.field private b:J

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0nF;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 84386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84387
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/analytics/AnalyticsStats;->c:Ljava/util/Map;

    .line 84388
    iput-object p1, p0, Lcom/facebook/analytics/AnalyticsStats;->a:LX/0So;

    .line 84389
    invoke-interface {p1}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/analytics/AnalyticsStats;->b:J

    .line 84390
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/analytics/AnalyticsStats;
    .locals 4

    .prologue
    .line 84391
    sget-object v0, Lcom/facebook/analytics/AnalyticsStats;->d:Lcom/facebook/analytics/AnalyticsStats;

    if-nez v0, :cond_1

    .line 84392
    const-class v1, Lcom/facebook/analytics/AnalyticsStats;

    monitor-enter v1

    .line 84393
    :try_start_0
    sget-object v0, Lcom/facebook/analytics/AnalyticsStats;->d:Lcom/facebook/analytics/AnalyticsStats;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 84394
    if-eqz v2, :cond_0

    .line 84395
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 84396
    new-instance p0, Lcom/facebook/analytics/AnalyticsStats;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {p0, v3}, Lcom/facebook/analytics/AnalyticsStats;-><init>(LX/0So;)V

    .line 84397
    move-object v0, p0

    .line 84398
    sput-object v0, Lcom/facebook/analytics/AnalyticsStats;->d:Lcom/facebook/analytics/AnalyticsStats;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84399
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 84400
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 84401
    :cond_1
    sget-object v0, Lcom/facebook/analytics/AnalyticsStats;->d:Lcom/facebook/analytics/AnalyticsStats;

    return-object v0

    .line 84402
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 84403
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized b(Lcom/facebook/analytics/AnalyticsStats;Ljava/lang/String;)LX/0nF;
    .locals 2

    .prologue
    .line 84404
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/analytics/AnalyticsStats;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0nF;

    .line 84405
    if-nez v0, :cond_0

    .line 84406
    new-instance v0, LX/0nF;

    invoke-direct {v0, p1}, LX/0nF;-><init>(Ljava/lang/String;)V

    .line 84407
    iget-object v1, p0, Lcom/facebook/analytics/AnalyticsStats;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84408
    :cond_0
    monitor-exit p0

    return-object v0

    .line 84409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 84410
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, Lcom/facebook/analytics/AnalyticsStats;->b(Lcom/facebook/analytics/AnalyticsStats;Ljava/lang/String;)LX/0nF;

    move-result-object v0

    .line 84411
    iget v1, v0, LX/0nF;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0nF;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84412
    monitor-exit p0

    return-void

    .line 84413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
