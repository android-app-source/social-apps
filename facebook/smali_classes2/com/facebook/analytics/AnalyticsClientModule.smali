.class public Lcom/facebook/analytics/AnalyticsClientModule;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation

.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static a:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83456
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/analytics/AnalyticsClientModule;->a:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83444
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 83445
    return-void
.end method

.method public static a(Ljava/util/concurrent/ExecutorService;LX/0Wb;)LX/0Wd;
    .locals 1
    .param p0    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/AnalyticsThreadExecutor;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/analytics/AnalyticsThreadExecutorOnIdle;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 83455
    invoke-virtual {p1, p0}, LX/0Wb;->a(Ljava/util/concurrent/ExecutorService;)LX/0Wd;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Ot;)LX/0Zb;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0ZZ;",
            ">;)",
            "LX/0Zb;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 83454
    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    return-object v0
.end method

.method public static a(LX/0Xl;LX/0Zb;LX/0So;LX/0So;Landroid/os/Handler;)LX/13S;
    .locals 6
    .param p0    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p2    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedAwakeTimeSinceBoot;
        .end annotation
    .end param
    .param p3    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 83453
    new-instance v0, LX/13S;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/13S;-><init>(LX/0Xl;LX/0Zb;LX/0So;LX/0So;Landroid/os/Handler;)V

    return-object v0
.end method

.method public static a(LX/0gh;)Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/analytics/session/AnalyticsBackgroundState;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 83451
    iget-boolean v0, p0, LX/0gh;->B:Z

    move v0, v0

    .line 83452
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;)Ljava/lang/Long;
    .locals 4
    .annotation runtime Lcom/facebook/analytics/annotations/DeviceStatusReporterInterval;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 83457
    sget-object v0, LX/0uQ;->f:LX/0Tn;

    const-wide/32 v2, 0x36ee80

    invoke-interface {p0, v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0Ot;)LX/0mI;
    .locals 1
    .annotation runtime Lcom/facebook/analytics2/loggermodule/NormalPriEventListener;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2DK;",
            ">;)",
            "LX/0mI;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 83450
    new-instance v0, LX/0mH;

    invoke-direct {v0, p0}, LX/0mH;-><init>(LX/0Ot;)V

    return-object v0
.end method

.method public static c(LX/0Ot;)LX/0mI;
    .locals 1
    .annotation runtime Lcom/facebook/analytics2/loggermodule/HighPriEventListener;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2DK;",
            ">;)",
            "LX/0mI;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 83449
    new-instance v0, LX/0mK;

    invoke-direct {v0, p0}, LX/0mK;-><init>(LX/0Ot;)V

    return-object v0
.end method

.method public static getInstanceForTest_AnalyticsLogger(LX/0QA;)LX/0Zb;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 83448
    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    return-object v0
.end method

.method public static getInstanceForTest_LoggingTestConfig(LX/0QA;)LX/0aD;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 83447
    invoke-static {p0}, LX/0aD;->a(LX/0QB;)LX/0aD;

    move-result-object v0

    check-cast v0, LX/0aD;

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 83446
    return-void
.end method
