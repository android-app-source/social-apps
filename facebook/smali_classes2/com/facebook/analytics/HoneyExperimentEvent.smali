.class public Lcom/facebook/analytics/HoneyExperimentEvent;
.super Lcom/facebook/analytics/HoneyAnalyticsEvent;
.source ""


# instance fields
.field public c:Ljava/lang/String;

.field private d:LX/0m9;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 169574
    const-string v0, "experiment"

    invoke-direct {p0, v0, p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 169575
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/HoneyExperimentEvent;
    .locals 2

    .prologue
    .line 169538
    iget-object v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    if-nez v0, :cond_0

    .line 169539
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    iput-object v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    .line 169540
    :cond_0
    iget-object v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    invoke-virtual {v0, p1, p2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 169541
    return-object p0
.end method

.method public final a(LX/0n9;)V
    .locals 3

    .prologue
    .line 169564
    iget-object v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    if-eqz v0, :cond_0

    .line 169565
    :try_start_0
    iget-object v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    invoke-static {v0, p1}, LX/0nG;->a(LX/0m9;LX/0n9;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169566
    :cond_0
    return-void

    .line 169567
    :catch_0
    move-exception v0

    .line 169568
    new-instance v1, Ljava/lang/IllegalArgumentException;

    iget-object v2, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyExperimentEvent;
    .locals 2

    .prologue
    .line 169569
    iget-object v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    if-nez v0, :cond_0

    .line 169570
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    iput-object v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    .line 169571
    :cond_0
    if-eqz p2, :cond_1

    .line 169572
    iget-object v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    invoke-virtual {v0, p1, p2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 169573
    :cond_1
    return-object p0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169563
    invoke-virtual {p0}, Lcom/facebook/analytics/HoneyExperimentEvent;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 6

    .prologue
    .line 169542
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 169543
    const-string v1, "time"

    .line 169544
    iget-wide v4, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    move-wide v2, v4

    .line 169545
    invoke-static {v2, v3}, LX/3za;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 169546
    const-string v1, "log_type"

    .line 169547
    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a:Ljava/lang/String;

    move-object v2, v2

    .line 169548
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 169549
    const-string v1, "name"

    .line 169550
    iget-object v2, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 169551
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 169552
    iget-object v1, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 169553
    const-string v1, "exprID"

    iget-object v2, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 169554
    :cond_0
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->k:LX/162;

    move-object v1, v1

    .line 169555
    if-eqz v1, :cond_1

    .line 169556
    const-string v2, "enabled_features"

    invoke-virtual {p0, v2, v1}, Lcom/facebook/analytics/HoneyExperimentEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/HoneyExperimentEvent;

    .line 169557
    :cond_1
    iget-object v1, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    if-eqz v1, :cond_2

    .line 169558
    const-string v1, "result"

    iget-object v2, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->d:LX/0m9;

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 169559
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g:Z

    move v1, v1

    .line 169560
    if-eqz v1, :cond_3

    .line 169561
    const-string v1, "bg"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 169562
    :cond_3
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
