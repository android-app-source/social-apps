.class public final enum Lcom/facebook/csslayout/YogaDirection;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/csslayout/YogaDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/csslayout/YogaDirection;

.field public static final enum INHERIT:Lcom/facebook/csslayout/YogaDirection;

.field public static final enum LTR:Lcom/facebook/csslayout/YogaDirection;

.field public static final enum RTL:Lcom/facebook/csslayout/YogaDirection;


# instance fields
.field private mIntValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 218844
    new-instance v0, Lcom/facebook/csslayout/YogaDirection;

    const-string v1, "INHERIT"

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/csslayout/YogaDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaDirection;->INHERIT:Lcom/facebook/csslayout/YogaDirection;

    .line 218845
    new-instance v0, Lcom/facebook/csslayout/YogaDirection;

    const-string v1, "LTR"

    invoke-direct {v0, v1, v3, v3}, Lcom/facebook/csslayout/YogaDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaDirection;->LTR:Lcom/facebook/csslayout/YogaDirection;

    .line 218846
    new-instance v0, Lcom/facebook/csslayout/YogaDirection;

    const-string v1, "RTL"

    invoke-direct {v0, v1, v4, v4}, Lcom/facebook/csslayout/YogaDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaDirection;->RTL:Lcom/facebook/csslayout/YogaDirection;

    .line 218847
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/csslayout/YogaDirection;

    sget-object v1, Lcom/facebook/csslayout/YogaDirection;->INHERIT:Lcom/facebook/csslayout/YogaDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/csslayout/YogaDirection;->LTR:Lcom/facebook/csslayout/YogaDirection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/csslayout/YogaDirection;->RTL:Lcom/facebook/csslayout/YogaDirection;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/csslayout/YogaDirection;->$VALUES:[Lcom/facebook/csslayout/YogaDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 218856
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 218857
    iput p3, p0, Lcom/facebook/csslayout/YogaDirection;->mIntValue:I

    .line 218858
    return-void
.end method

.method public static fromInt(I)Lcom/facebook/csslayout/YogaDirection;
    .locals 3

    .prologue
    .line 218850
    packed-switch p0, :pswitch_data_0

    .line 218851
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unkown enum value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218852
    :pswitch_0
    sget-object v0, Lcom/facebook/csslayout/YogaDirection;->INHERIT:Lcom/facebook/csslayout/YogaDirection;

    .line 218853
    :goto_0
    return-object v0

    .line 218854
    :pswitch_1
    sget-object v0, Lcom/facebook/csslayout/YogaDirection;->LTR:Lcom/facebook/csslayout/YogaDirection;

    goto :goto_0

    .line 218855
    :pswitch_2
    sget-object v0, Lcom/facebook/csslayout/YogaDirection;->RTL:Lcom/facebook/csslayout/YogaDirection;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaDirection;
    .locals 1

    .prologue
    .line 218859
    const-class v0, Lcom/facebook/csslayout/YogaDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaDirection;

    return-object v0
.end method

.method public static values()[Lcom/facebook/csslayout/YogaDirection;
    .locals 1

    .prologue
    .line 218849
    sget-object v0, Lcom/facebook/csslayout/YogaDirection;->$VALUES:[Lcom/facebook/csslayout/YogaDirection;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/csslayout/YogaDirection;

    return-object v0
.end method


# virtual methods
.method public final intValue()I
    .locals 1

    .prologue
    .line 218848
    iget v0, p0, Lcom/facebook/csslayout/YogaDirection;->mIntValue:I

    return v0
.end method
