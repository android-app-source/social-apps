.class public final enum Lcom/facebook/csslayout/YogaEdge;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/csslayout/YogaEdge;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/csslayout/YogaEdge;

.field public static final enum ALL:Lcom/facebook/csslayout/YogaEdge;

.field public static final enum BOTTOM:Lcom/facebook/csslayout/YogaEdge;

.field public static final enum END:Lcom/facebook/csslayout/YogaEdge;

.field public static final enum HORIZONTAL:Lcom/facebook/csslayout/YogaEdge;

.field public static final enum LEFT:Lcom/facebook/csslayout/YogaEdge;

.field public static final enum RIGHT:Lcom/facebook/csslayout/YogaEdge;

.field public static final enum START:Lcom/facebook/csslayout/YogaEdge;

.field public static final enum TOP:Lcom/facebook/csslayout/YogaEdge;

.field public static final enum VERTICAL:Lcom/facebook/csslayout/YogaEdge;


# instance fields
.field private mIntValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 317229
    new-instance v0, Lcom/facebook/csslayout/YogaEdge;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v4, v4}, Lcom/facebook/csslayout/YogaEdge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaEdge;->LEFT:Lcom/facebook/csslayout/YogaEdge;

    .line 317230
    new-instance v0, Lcom/facebook/csslayout/YogaEdge;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v5, v5}, Lcom/facebook/csslayout/YogaEdge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaEdge;->TOP:Lcom/facebook/csslayout/YogaEdge;

    .line 317231
    new-instance v0, Lcom/facebook/csslayout/YogaEdge;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v6, v6}, Lcom/facebook/csslayout/YogaEdge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaEdge;->RIGHT:Lcom/facebook/csslayout/YogaEdge;

    .line 317232
    new-instance v0, Lcom/facebook/csslayout/YogaEdge;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v7, v7}, Lcom/facebook/csslayout/YogaEdge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaEdge;->BOTTOM:Lcom/facebook/csslayout/YogaEdge;

    .line 317233
    new-instance v0, Lcom/facebook/csslayout/YogaEdge;

    const-string v1, "START"

    invoke-direct {v0, v1, v8, v8}, Lcom/facebook/csslayout/YogaEdge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaEdge;->START:Lcom/facebook/csslayout/YogaEdge;

    .line 317234
    new-instance v0, Lcom/facebook/csslayout/YogaEdge;

    const-string v1, "END"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/csslayout/YogaEdge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaEdge;->END:Lcom/facebook/csslayout/YogaEdge;

    .line 317235
    new-instance v0, Lcom/facebook/csslayout/YogaEdge;

    const-string v1, "HORIZONTAL"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/csslayout/YogaEdge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaEdge;->HORIZONTAL:Lcom/facebook/csslayout/YogaEdge;

    .line 317236
    new-instance v0, Lcom/facebook/csslayout/YogaEdge;

    const-string v1, "VERTICAL"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/csslayout/YogaEdge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaEdge;->VERTICAL:Lcom/facebook/csslayout/YogaEdge;

    .line 317237
    new-instance v0, Lcom/facebook/csslayout/YogaEdge;

    const-string v1, "ALL"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/csslayout/YogaEdge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaEdge;->ALL:Lcom/facebook/csslayout/YogaEdge;

    .line 317238
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/csslayout/YogaEdge;

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->LEFT:Lcom/facebook/csslayout/YogaEdge;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->TOP:Lcom/facebook/csslayout/YogaEdge;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->RIGHT:Lcom/facebook/csslayout/YogaEdge;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->BOTTOM:Lcom/facebook/csslayout/YogaEdge;

    aput-object v1, v0, v7

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->START:Lcom/facebook/csslayout/YogaEdge;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/csslayout/YogaEdge;->END:Lcom/facebook/csslayout/YogaEdge;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/csslayout/YogaEdge;->HORIZONTAL:Lcom/facebook/csslayout/YogaEdge;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/csslayout/YogaEdge;->VERTICAL:Lcom/facebook/csslayout/YogaEdge;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/csslayout/YogaEdge;->ALL:Lcom/facebook/csslayout/YogaEdge;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/csslayout/YogaEdge;->$VALUES:[Lcom/facebook/csslayout/YogaEdge;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 317226
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 317227
    iput p3, p0, Lcom/facebook/csslayout/YogaEdge;->mIntValue:I

    .line 317228
    return-void
.end method

.method public static fromInt(I)Lcom/facebook/csslayout/YogaEdge;
    .locals 3

    .prologue
    .line 317214
    packed-switch p0, :pswitch_data_0

    .line 317215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unkown enum value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317216
    :pswitch_0
    sget-object v0, Lcom/facebook/csslayout/YogaEdge;->LEFT:Lcom/facebook/csslayout/YogaEdge;

    .line 317217
    :goto_0
    return-object v0

    .line 317218
    :pswitch_1
    sget-object v0, Lcom/facebook/csslayout/YogaEdge;->TOP:Lcom/facebook/csslayout/YogaEdge;

    goto :goto_0

    .line 317219
    :pswitch_2
    sget-object v0, Lcom/facebook/csslayout/YogaEdge;->RIGHT:Lcom/facebook/csslayout/YogaEdge;

    goto :goto_0

    .line 317220
    :pswitch_3
    sget-object v0, Lcom/facebook/csslayout/YogaEdge;->BOTTOM:Lcom/facebook/csslayout/YogaEdge;

    goto :goto_0

    .line 317221
    :pswitch_4
    sget-object v0, Lcom/facebook/csslayout/YogaEdge;->START:Lcom/facebook/csslayout/YogaEdge;

    goto :goto_0

    .line 317222
    :pswitch_5
    sget-object v0, Lcom/facebook/csslayout/YogaEdge;->END:Lcom/facebook/csslayout/YogaEdge;

    goto :goto_0

    .line 317223
    :pswitch_6
    sget-object v0, Lcom/facebook/csslayout/YogaEdge;->HORIZONTAL:Lcom/facebook/csslayout/YogaEdge;

    goto :goto_0

    .line 317224
    :pswitch_7
    sget-object v0, Lcom/facebook/csslayout/YogaEdge;->VERTICAL:Lcom/facebook/csslayout/YogaEdge;

    goto :goto_0

    .line 317225
    :pswitch_8
    sget-object v0, Lcom/facebook/csslayout/YogaEdge;->ALL:Lcom/facebook/csslayout/YogaEdge;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaEdge;
    .locals 1

    .prologue
    .line 317211
    const-class v0, Lcom/facebook/csslayout/YogaEdge;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaEdge;

    return-object v0
.end method

.method public static values()[Lcom/facebook/csslayout/YogaEdge;
    .locals 1

    .prologue
    .line 317213
    sget-object v0, Lcom/facebook/csslayout/YogaEdge;->$VALUES:[Lcom/facebook/csslayout/YogaEdge;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/csslayout/YogaEdge;

    return-object v0
.end method


# virtual methods
.method public final intValue()I
    .locals 1

    .prologue
    .line 317212
    iget v0, p0, Lcom/facebook/csslayout/YogaEdge;->mIntValue:I

    return v0
.end method
