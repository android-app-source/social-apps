.class public final enum Lcom/facebook/csslayout/YogaJustify;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/csslayout/YogaJustify;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/csslayout/YogaJustify;

.field public static final enum CENTER:Lcom/facebook/csslayout/YogaJustify;

.field public static final enum FLEX_END:Lcom/facebook/csslayout/YogaJustify;

.field public static final enum FLEX_START:Lcom/facebook/csslayout/YogaJustify;

.field public static final enum SPACE_AROUND:Lcom/facebook/csslayout/YogaJustify;

.field public static final enum SPACE_BETWEEN:Lcom/facebook/csslayout/YogaJustify;


# instance fields
.field private mIntValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 218789
    new-instance v0, Lcom/facebook/csslayout/YogaJustify;

    const-string v1, "FLEX_START"

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/csslayout/YogaJustify;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaJustify;->FLEX_START:Lcom/facebook/csslayout/YogaJustify;

    .line 218790
    new-instance v0, Lcom/facebook/csslayout/YogaJustify;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v3, v3}, Lcom/facebook/csslayout/YogaJustify;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaJustify;->CENTER:Lcom/facebook/csslayout/YogaJustify;

    .line 218791
    new-instance v0, Lcom/facebook/csslayout/YogaJustify;

    const-string v1, "FLEX_END"

    invoke-direct {v0, v1, v4, v4}, Lcom/facebook/csslayout/YogaJustify;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaJustify;->FLEX_END:Lcom/facebook/csslayout/YogaJustify;

    .line 218792
    new-instance v0, Lcom/facebook/csslayout/YogaJustify;

    const-string v1, "SPACE_BETWEEN"

    invoke-direct {v0, v1, v5, v5}, Lcom/facebook/csslayout/YogaJustify;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaJustify;->SPACE_BETWEEN:Lcom/facebook/csslayout/YogaJustify;

    .line 218793
    new-instance v0, Lcom/facebook/csslayout/YogaJustify;

    const-string v1, "SPACE_AROUND"

    invoke-direct {v0, v1, v6, v6}, Lcom/facebook/csslayout/YogaJustify;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaJustify;->SPACE_AROUND:Lcom/facebook/csslayout/YogaJustify;

    .line 218794
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/csslayout/YogaJustify;

    sget-object v1, Lcom/facebook/csslayout/YogaJustify;->FLEX_START:Lcom/facebook/csslayout/YogaJustify;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/csslayout/YogaJustify;->CENTER:Lcom/facebook/csslayout/YogaJustify;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/csslayout/YogaJustify;->FLEX_END:Lcom/facebook/csslayout/YogaJustify;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/csslayout/YogaJustify;->SPACE_BETWEEN:Lcom/facebook/csslayout/YogaJustify;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/csslayout/YogaJustify;->SPACE_AROUND:Lcom/facebook/csslayout/YogaJustify;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/csslayout/YogaJustify;->$VALUES:[Lcom/facebook/csslayout/YogaJustify;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 218786
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 218787
    iput p3, p0, Lcom/facebook/csslayout/YogaJustify;->mIntValue:I

    .line 218788
    return-void
.end method

.method public static fromInt(I)Lcom/facebook/csslayout/YogaJustify;
    .locals 3

    .prologue
    .line 218795
    packed-switch p0, :pswitch_data_0

    .line 218796
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unkown enum value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218797
    :pswitch_0
    sget-object v0, Lcom/facebook/csslayout/YogaJustify;->FLEX_START:Lcom/facebook/csslayout/YogaJustify;

    .line 218798
    :goto_0
    return-object v0

    .line 218799
    :pswitch_1
    sget-object v0, Lcom/facebook/csslayout/YogaJustify;->CENTER:Lcom/facebook/csslayout/YogaJustify;

    goto :goto_0

    .line 218800
    :pswitch_2
    sget-object v0, Lcom/facebook/csslayout/YogaJustify;->FLEX_END:Lcom/facebook/csslayout/YogaJustify;

    goto :goto_0

    .line 218801
    :pswitch_3
    sget-object v0, Lcom/facebook/csslayout/YogaJustify;->SPACE_BETWEEN:Lcom/facebook/csslayout/YogaJustify;

    goto :goto_0

    .line 218802
    :pswitch_4
    sget-object v0, Lcom/facebook/csslayout/YogaJustify;->SPACE_AROUND:Lcom/facebook/csslayout/YogaJustify;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaJustify;
    .locals 1

    .prologue
    .line 218785
    const-class v0, Lcom/facebook/csslayout/YogaJustify;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaJustify;

    return-object v0
.end method

.method public static values()[Lcom/facebook/csslayout/YogaJustify;
    .locals 1

    .prologue
    .line 218784
    sget-object v0, Lcom/facebook/csslayout/YogaJustify;->$VALUES:[Lcom/facebook/csslayout/YogaJustify;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/csslayout/YogaJustify;

    return-object v0
.end method


# virtual methods
.method public final intValue()I
    .locals 1

    .prologue
    .line 218783
    iget v0, p0, Lcom/facebook/csslayout/YogaJustify;->mIntValue:I

    return v0
.end method
