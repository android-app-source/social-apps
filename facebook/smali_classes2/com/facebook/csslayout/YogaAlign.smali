.class public final enum Lcom/facebook/csslayout/YogaAlign;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/csslayout/YogaAlign;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/csslayout/YogaAlign;

.field public static final enum AUTO:Lcom/facebook/csslayout/YogaAlign;

.field public static final enum CENTER:Lcom/facebook/csslayout/YogaAlign;

.field public static final enum FLEX_END:Lcom/facebook/csslayout/YogaAlign;

.field public static final enum FLEX_START:Lcom/facebook/csslayout/YogaAlign;

.field public static final enum STRETCH:Lcom/facebook/csslayout/YogaAlign;


# instance fields
.field private mIntValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 218760
    new-instance v0, Lcom/facebook/csslayout/YogaAlign;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/csslayout/YogaAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaAlign;->AUTO:Lcom/facebook/csslayout/YogaAlign;

    .line 218761
    new-instance v0, Lcom/facebook/csslayout/YogaAlign;

    const-string v1, "FLEX_START"

    invoke-direct {v0, v1, v3, v3}, Lcom/facebook/csslayout/YogaAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaAlign;->FLEX_START:Lcom/facebook/csslayout/YogaAlign;

    .line 218762
    new-instance v0, Lcom/facebook/csslayout/YogaAlign;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v4, v4}, Lcom/facebook/csslayout/YogaAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaAlign;->CENTER:Lcom/facebook/csslayout/YogaAlign;

    .line 218763
    new-instance v0, Lcom/facebook/csslayout/YogaAlign;

    const-string v1, "FLEX_END"

    invoke-direct {v0, v1, v5, v5}, Lcom/facebook/csslayout/YogaAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaAlign;->FLEX_END:Lcom/facebook/csslayout/YogaAlign;

    .line 218764
    new-instance v0, Lcom/facebook/csslayout/YogaAlign;

    const-string v1, "STRETCH"

    invoke-direct {v0, v1, v6, v6}, Lcom/facebook/csslayout/YogaAlign;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    .line 218765
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/csslayout/YogaAlign;

    sget-object v1, Lcom/facebook/csslayout/YogaAlign;->AUTO:Lcom/facebook/csslayout/YogaAlign;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/csslayout/YogaAlign;->FLEX_START:Lcom/facebook/csslayout/YogaAlign;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/csslayout/YogaAlign;->CENTER:Lcom/facebook/csslayout/YogaAlign;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/csslayout/YogaAlign;->FLEX_END:Lcom/facebook/csslayout/YogaAlign;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/csslayout/YogaAlign;->$VALUES:[Lcom/facebook/csslayout/YogaAlign;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 218766
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 218767
    iput p3, p0, Lcom/facebook/csslayout/YogaAlign;->mIntValue:I

    .line 218768
    return-void
.end method

.method public static fromInt(I)Lcom/facebook/csslayout/YogaAlign;
    .locals 3

    .prologue
    .line 218769
    packed-switch p0, :pswitch_data_0

    .line 218770
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unkown enum value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218771
    :pswitch_0
    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->AUTO:Lcom/facebook/csslayout/YogaAlign;

    .line 218772
    :goto_0
    return-object v0

    .line 218773
    :pswitch_1
    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->FLEX_START:Lcom/facebook/csslayout/YogaAlign;

    goto :goto_0

    .line 218774
    :pswitch_2
    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->CENTER:Lcom/facebook/csslayout/YogaAlign;

    goto :goto_0

    .line 218775
    :pswitch_3
    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->FLEX_END:Lcom/facebook/csslayout/YogaAlign;

    goto :goto_0

    .line 218776
    :pswitch_4
    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaAlign;
    .locals 1

    .prologue
    .line 218777
    const-class v0, Lcom/facebook/csslayout/YogaAlign;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaAlign;

    return-object v0
.end method

.method public static values()[Lcom/facebook/csslayout/YogaAlign;
    .locals 1

    .prologue
    .line 218778
    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->$VALUES:[Lcom/facebook/csslayout/YogaAlign;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/csslayout/YogaAlign;

    return-object v0
.end method


# virtual methods
.method public final intValue()I
    .locals 1

    .prologue
    .line 218779
    iget v0, p0, Lcom/facebook/csslayout/YogaAlign;->mIntValue:I

    return v0
.end method
