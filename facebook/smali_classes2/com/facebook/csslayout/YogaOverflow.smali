.class public final enum Lcom/facebook/csslayout/YogaOverflow;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/csslayout/YogaOverflow;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/csslayout/YogaOverflow;

.field public static final enum HIDDEN:Lcom/facebook/csslayout/YogaOverflow;

.field public static final enum SCROLL:Lcom/facebook/csslayout/YogaOverflow;

.field public static final enum VISIBLE:Lcom/facebook/csslayout/YogaOverflow;


# instance fields
.field private mIntValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 314578
    new-instance v0, Lcom/facebook/csslayout/YogaOverflow;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/csslayout/YogaOverflow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaOverflow;->VISIBLE:Lcom/facebook/csslayout/YogaOverflow;

    .line 314579
    new-instance v0, Lcom/facebook/csslayout/YogaOverflow;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v3, v3}, Lcom/facebook/csslayout/YogaOverflow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaOverflow;->HIDDEN:Lcom/facebook/csslayout/YogaOverflow;

    .line 314580
    new-instance v0, Lcom/facebook/csslayout/YogaOverflow;

    const-string v1, "SCROLL"

    invoke-direct {v0, v1, v4, v4}, Lcom/facebook/csslayout/YogaOverflow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaOverflow;->SCROLL:Lcom/facebook/csslayout/YogaOverflow;

    .line 314581
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/csslayout/YogaOverflow;

    sget-object v1, Lcom/facebook/csslayout/YogaOverflow;->VISIBLE:Lcom/facebook/csslayout/YogaOverflow;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/csslayout/YogaOverflow;->HIDDEN:Lcom/facebook/csslayout/YogaOverflow;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/csslayout/YogaOverflow;->SCROLL:Lcom/facebook/csslayout/YogaOverflow;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/csslayout/YogaOverflow;->$VALUES:[Lcom/facebook/csslayout/YogaOverflow;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 314575
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 314576
    iput p3, p0, Lcom/facebook/csslayout/YogaOverflow;->mIntValue:I

    .line 314577
    return-void
.end method

.method public static fromInt(I)Lcom/facebook/csslayout/YogaOverflow;
    .locals 3

    .prologue
    .line 314582
    packed-switch p0, :pswitch_data_0

    .line 314583
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unkown enum value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314584
    :pswitch_0
    sget-object v0, Lcom/facebook/csslayout/YogaOverflow;->VISIBLE:Lcom/facebook/csslayout/YogaOverflow;

    .line 314585
    :goto_0
    return-object v0

    .line 314586
    :pswitch_1
    sget-object v0, Lcom/facebook/csslayout/YogaOverflow;->HIDDEN:Lcom/facebook/csslayout/YogaOverflow;

    goto :goto_0

    .line 314587
    :pswitch_2
    sget-object v0, Lcom/facebook/csslayout/YogaOverflow;->SCROLL:Lcom/facebook/csslayout/YogaOverflow;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaOverflow;
    .locals 1

    .prologue
    .line 314574
    const-class v0, Lcom/facebook/csslayout/YogaOverflow;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaOverflow;

    return-object v0
.end method

.method public static values()[Lcom/facebook/csslayout/YogaOverflow;
    .locals 1

    .prologue
    .line 314573
    sget-object v0, Lcom/facebook/csslayout/YogaOverflow;->$VALUES:[Lcom/facebook/csslayout/YogaOverflow;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/csslayout/YogaOverflow;

    return-object v0
.end method


# virtual methods
.method public final intValue()I
    .locals 1

    .prologue
    .line 314572
    iget v0, p0, Lcom/facebook/csslayout/YogaOverflow;->mIntValue:I

    return v0
.end method
