.class public Lcom/facebook/csslayout/YogaNode;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1mn;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1mn",
        "<",
        "Lcom/facebook/csslayout/YogaNode;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/csslayout/YogaNode;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/csslayout/YogaNode;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/csslayout/YogaMeasureFunction;

.field private d:J

.field private e:Ljava/lang/Object;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private mHeight:F
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mLayoutDirection:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mLeft:F
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mTop:F
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mWidth:F
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 314570
    const-string v0, "yoga"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 314571
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/high16 v1, 0x7fc00000    # NaNf

    const/4 v0, 0x0

    .line 314556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314557
    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->f:Z

    .line 314558
    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->g:Z

    .line 314559
    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->h:Z

    .line 314560
    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->i:Z

    .line 314561
    iput v1, p0, Lcom/facebook/csslayout/YogaNode;->mWidth:F

    .line 314562
    iput v1, p0, Lcom/facebook/csslayout/YogaNode;->mHeight:F

    .line 314563
    iput v1, p0, Lcom/facebook/csslayout/YogaNode;->mTop:F

    .line 314564
    iput v1, p0, Lcom/facebook/csslayout/YogaNode;->mLeft:F

    .line 314565
    iput v0, p0, Lcom/facebook/csslayout/YogaNode;->mLayoutDirection:I

    .line 314566
    invoke-direct {p0}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeNew()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    .line 314567
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 314568
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to allocate native memory"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314569
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/csslayout/YogaExperimentalFeature;Z)V
    .locals 1

    .prologue
    .line 314554
    invoke-virtual {p0}, Lcom/facebook/csslayout/YogaExperimentalFeature;->intValue()I

    move-result v0

    invoke-static {v0, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGSetExperimentalFeatureEnabled(IZ)V

    .line 314555
    return-void
.end method

.method private d(I)Lcom/facebook/csslayout/YogaNode;
    .locals 1

    .prologue
    .line 314553
    iget-object v0, p0, Lcom/facebook/csslayout/YogaNode;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaNode;

    return-object v0
.end method

.method private static native jni_YGIsExperimentalFeatureEnabled(I)Z
.end method

.method public static native jni_YGLog(ILjava/lang/String;)V
.end method

.method private native jni_YGNodeCalculateLayout(J)V
.end method

.method private native jni_YGNodeCopyStyle(JJ)V
.end method

.method private native jni_YGNodeFree(J)V
.end method

.method public static native jni_YGNodeGetInstanceCount()I
.end method

.method private native jni_YGNodeHasNewLayout(J)Z
.end method

.method private native jni_YGNodeInsertChild(JJI)V
.end method

.method private native jni_YGNodeIsDirty(J)Z
.end method

.method private native jni_YGNodeMarkDirty(J)V
.end method

.method private native jni_YGNodeMarkLayoutSeen(J)V
.end method

.method private native jni_YGNodeNew()J
.end method

.method private native jni_YGNodeRemoveChild(JJ)V
.end method

.method private native jni_YGNodeReset(J)V
.end method

.method private native jni_YGNodeSetHasMeasureFunc(JZ)V
.end method

.method private native jni_YGNodeStyleGetAlignContent(J)I
.end method

.method private native jni_YGNodeStyleGetAlignItems(J)I
.end method

.method private native jni_YGNodeStyleGetAlignSelf(J)I
.end method

.method private native jni_YGNodeStyleGetAspectRatio(J)F
.end method

.method private native jni_YGNodeStyleGetBorder(JI)F
.end method

.method private native jni_YGNodeStyleGetDirection(J)I
.end method

.method private native jni_YGNodeStyleGetFlexBasis(J)F
.end method

.method private native jni_YGNodeStyleGetFlexDirection(J)I
.end method

.method private native jni_YGNodeStyleGetFlexGrow(J)F
.end method

.method private native jni_YGNodeStyleGetFlexShrink(J)F
.end method

.method private native jni_YGNodeStyleGetHeight(J)F
.end method

.method private native jni_YGNodeStyleGetJustifyContent(J)I
.end method

.method private native jni_YGNodeStyleGetMargin(JI)F
.end method

.method private native jni_YGNodeStyleGetMaxHeight(J)F
.end method

.method private native jni_YGNodeStyleGetMaxWidth(J)F
.end method

.method private native jni_YGNodeStyleGetMinHeight(J)F
.end method

.method private native jni_YGNodeStyleGetMinWidth(J)F
.end method

.method private native jni_YGNodeStyleGetOverflow(J)I
.end method

.method private native jni_YGNodeStyleGetPadding(JI)F
.end method

.method private native jni_YGNodeStyleGetPosition(JI)F
.end method

.method private native jni_YGNodeStyleGetPositionType(J)I
.end method

.method private native jni_YGNodeStyleGetWidth(J)F
.end method

.method private native jni_YGNodeStyleSetAlignContent(JI)V
.end method

.method private native jni_YGNodeStyleSetAlignItems(JI)V
.end method

.method private native jni_YGNodeStyleSetAlignSelf(JI)V
.end method

.method private native jni_YGNodeStyleSetAspectRatio(JF)V
.end method

.method private native jni_YGNodeStyleSetBorder(JIF)V
.end method

.method private native jni_YGNodeStyleSetDirection(JI)V
.end method

.method private native jni_YGNodeStyleSetFlex(JF)V
.end method

.method private native jni_YGNodeStyleSetFlexBasis(JF)V
.end method

.method private native jni_YGNodeStyleSetFlexDirection(JI)V
.end method

.method private native jni_YGNodeStyleSetFlexGrow(JF)V
.end method

.method private native jni_YGNodeStyleSetFlexShrink(JF)V
.end method

.method private native jni_YGNodeStyleSetFlexWrap(JI)V
.end method

.method private native jni_YGNodeStyleSetHeight(JF)V
.end method

.method private native jni_YGNodeStyleSetJustifyContent(JI)V
.end method

.method private native jni_YGNodeStyleSetMargin(JIF)V
.end method

.method private native jni_YGNodeStyleSetMaxHeight(JF)V
.end method

.method private native jni_YGNodeStyleSetMaxWidth(JF)V
.end method

.method private native jni_YGNodeStyleSetMinHeight(JF)V
.end method

.method private native jni_YGNodeStyleSetMinWidth(JF)V
.end method

.method private native jni_YGNodeStyleSetOverflow(JI)V
.end method

.method private native jni_YGNodeStyleSetPadding(JIF)V
.end method

.method private native jni_YGNodeStyleSetPosition(JIF)V
.end method

.method private native jni_YGNodeStyleSetPositionType(JI)V
.end method

.method private native jni_YGNodeStyleSetWidth(JF)V
.end method

.method private static native jni_YGSetExperimentalFeatureEnabled(IZ)V
.end method

.method private static native jni_YGSetLogger(Ljava/lang/Object;)V
.end method


# virtual methods
.method public final a(Lcom/facebook/csslayout/YogaEdge;)F
    .locals 3

    .prologue
    .line 314497
    iget-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->f:Z

    if-nez v0, :cond_1

    .line 314498
    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v0

    sget-object v1, Lcom/facebook/csslayout/YogaEdge;->START:Lcom/facebook/csslayout/YogaEdge;

    invoke-virtual {v1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    .line 314499
    :goto_0
    return v0

    .line 314500
    :cond_0
    const/high16 v0, 0x7fc00000    # NaNf

    goto :goto_0

    .line 314501
    :cond_1
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleGetPadding(JI)F

    move-result v0

    goto :goto_0
.end method

.method public final a(I)Lcom/facebook/csslayout/YogaNode;
    .locals 6

    .prologue
    .line 314493
    iget-object v0, p0, Lcom/facebook/csslayout/YogaNode;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaNode;

    .line 314494
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/csslayout/YogaNode;->a:Lcom/facebook/csslayout/YogaNode;

    .line 314495
    iget-wide v2, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    iget-wide v4, v0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeRemoveChild(JJ)V

    .line 314496
    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x7fc00000    # NaNf

    const/4 v0, 0x0

    .line 314480
    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->f:Z

    .line 314481
    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->g:Z

    .line 314482
    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->h:Z

    .line 314483
    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->i:Z

    .line 314484
    iput v1, p0, Lcom/facebook/csslayout/YogaNode;->mWidth:F

    .line 314485
    iput v1, p0, Lcom/facebook/csslayout/YogaNode;->mHeight:F

    .line 314486
    iput v1, p0, Lcom/facebook/csslayout/YogaNode;->mTop:F

    .line 314487
    iput v1, p0, Lcom/facebook/csslayout/YogaNode;->mLeft:F

    .line 314488
    iput v0, p0, Lcom/facebook/csslayout/YogaNode;->mLayoutDirection:I

    .line 314489
    iput-object v2, p0, Lcom/facebook/csslayout/YogaNode;->c:Lcom/facebook/csslayout/YogaMeasureFunction;

    .line 314490
    iput-object v2, p0, Lcom/facebook/csslayout/YogaNode;->e:Ljava/lang/Object;

    .line 314491
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeReset(J)V

    .line 314492
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 314478
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetFlex(JF)V

    .line 314479
    return-void
.end method

.method public final bridge synthetic a(LX/1mn;I)V
    .locals 0

    .prologue
    .line 314477
    check-cast p1, Lcom/facebook/csslayout/YogaNode;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaNode;I)V

    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaAlign;)V
    .locals 3

    .prologue
    .line 314475
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaAlign;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetAlignItems(JI)V

    .line 314476
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaDirection;)V
    .locals 3

    .prologue
    .line 314473
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaDirection;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetDirection(JI)V

    .line 314474
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaEdge;F)V
    .locals 3

    .prologue
    .line 314470
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->g:Z

    .line 314471
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2, p2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetMargin(JIF)V

    .line 314472
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaFlexDirection;)V
    .locals 3

    .prologue
    .line 314468
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaFlexDirection;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetFlexDirection(JI)V

    .line 314469
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaJustify;)V
    .locals 3

    .prologue
    .line 314437
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaJustify;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetJustifyContent(JI)V

    .line 314438
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaMeasureFunction;)V
    .locals 4

    .prologue
    .line 314439
    iput-object p1, p0, Lcom/facebook/csslayout/YogaNode;->c:Lcom/facebook/csslayout/YogaMeasureFunction;

    .line 314440
    iget-wide v2, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v2, v3, v0}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeSetHasMeasureFunc(JZ)V

    .line 314441
    return-void

    .line 314442
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/csslayout/YogaNode;I)V
    .locals 7

    .prologue
    .line 314443
    iget-object v0, p1, Lcom/facebook/csslayout/YogaNode;->a:Lcom/facebook/csslayout/YogaNode;

    if-eqz v0, :cond_0

    .line 314444
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Child already has a parent, it must be removed first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314445
    :cond_0
    iget-object v0, p0, Lcom/facebook/csslayout/YogaNode;->b:Ljava/util/List;

    if-nez v0, :cond_1

    .line 314446
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/csslayout/YogaNode;->b:Ljava/util/List;

    .line 314447
    :cond_1
    iget-object v0, p0, Lcom/facebook/csslayout/YogaNode;->b:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 314448
    iput-object p0, p1, Lcom/facebook/csslayout/YogaNode;->a:Lcom/facebook/csslayout/YogaNode;

    .line 314449
    iget-wide v2, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    iget-wide v4, p1, Lcom/facebook/csslayout/YogaNode;->d:J

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeInsertChild(JJI)V

    .line 314450
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaOverflow;)V
    .locals 3

    .prologue
    .line 314451
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaOverflow;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetOverflow(JI)V

    .line 314452
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaPositionType;)V
    .locals 3

    .prologue
    .line 314453
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaPositionType;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetPositionType(JI)V

    .line 314454
    return-void
.end method

.method public final a(Lcom/facebook/csslayout/YogaWrap;)V
    .locals 3

    .prologue
    .line 314455
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaWrap;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetFlexWrap(JI)V

    .line 314456
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 314457
    iput-object p1, p0, Lcom/facebook/csslayout/YogaNode;->e:Ljava/lang/Object;

    .line 314458
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 314459
    iget-object v0, p0, Lcom/facebook/csslayout/YogaNode;->b:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/csslayout/YogaNode;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic b(I)LX/1mn;
    .locals 1

    .prologue
    .line 314460
    invoke-virtual {p0, p1}, Lcom/facebook/csslayout/YogaNode;->a(I)Lcom/facebook/csslayout/YogaNode;

    move-result-object v0

    return-object v0
.end method

.method public final b(F)V
    .locals 2

    .prologue
    .line 314461
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetFlexGrow(JF)V

    .line 314462
    return-void
.end method

.method public final b(Lcom/facebook/csslayout/YogaAlign;)V
    .locals 3

    .prologue
    .line 314463
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaAlign;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetAlignSelf(JI)V

    .line 314464
    return-void
.end method

.method public final b(Lcom/facebook/csslayout/YogaEdge;F)V
    .locals 3

    .prologue
    .line 314434
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->f:Z

    .line 314435
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2, p2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetPadding(JIF)V

    .line 314436
    return-void
.end method

.method public final synthetic c(I)LX/1mn;
    .locals 1

    .prologue
    .line 314465
    invoke-direct {p0, p1}, Lcom/facebook/csslayout/YogaNode;->d(I)Lcom/facebook/csslayout/YogaNode;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 314466
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeCalculateLayout(J)V

    .line 314467
    return-void
.end method

.method public final c(F)V
    .locals 2

    .prologue
    .line 314524
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetFlexShrink(JF)V

    .line 314525
    return-void
.end method

.method public final c(Lcom/facebook/csslayout/YogaAlign;)V
    .locals 3

    .prologue
    .line 314551
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaAlign;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetAlignContent(JI)V

    .line 314552
    return-void
.end method

.method public final c(Lcom/facebook/csslayout/YogaEdge;F)V
    .locals 3

    .prologue
    .line 314548
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->h:Z

    .line 314549
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2, p2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetBorder(JIF)V

    .line 314550
    return-void
.end method

.method public final d(F)V
    .locals 2

    .prologue
    .line 314546
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetFlexBasis(JF)V

    .line 314547
    return-void
.end method

.method public final d(Lcom/facebook/csslayout/YogaEdge;F)V
    .locals 3

    .prologue
    .line 314543
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/csslayout/YogaNode;->i:Z

    .line 314544
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaEdge;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2, p2}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetPosition(JIF)V

    .line 314545
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 314542
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeHasNewLayout(J)Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 314540
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeMarkDirty(J)V

    .line 314541
    return-void
.end method

.method public final e(F)V
    .locals 2

    .prologue
    .line 314538
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetWidth(JF)V

    .line 314539
    return-void
.end method

.method public final f(F)V
    .locals 2

    .prologue
    .line 314536
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetHeight(JF)V

    .line 314537
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 314535
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeIsDirty(J)Z

    move-result v0

    return v0
.end method

.method public final finalize()V
    .locals 2

    .prologue
    .line 314531
    :try_start_0
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeFree(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314532
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 314533
    return-void

    .line 314534
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 314529
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeMarkLayoutSeen(J)V

    .line 314530
    return-void
.end method

.method public final g(F)V
    .locals 2

    .prologue
    .line 314527
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetMinWidth(JF)V

    .line 314528
    return-void
.end method

.method public final h()Lcom/facebook/csslayout/YogaDirection;
    .locals 4

    .prologue
    .line 314526
    invoke-static {}, Lcom/facebook/csslayout/YogaDirection;->values()[Lcom/facebook/csslayout/YogaDirection;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v2, v3}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleGetDirection(J)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final h(F)V
    .locals 2

    .prologue
    .line 314502
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetMinHeight(JF)V

    .line 314503
    return-void
.end method

.method public final i()F
    .locals 2

    .prologue
    .line 314523
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleGetWidth(J)F

    move-result v0

    return v0
.end method

.method public final i(F)V
    .locals 2

    .prologue
    .line 314521
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetMaxWidth(JF)V

    .line 314522
    return-void
.end method

.method public final j()F
    .locals 2

    .prologue
    .line 314520
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleGetHeight(J)F

    move-result v0

    return v0
.end method

.method public final j(F)V
    .locals 2

    .prologue
    .line 314518
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetMaxHeight(JF)V

    .line 314519
    return-void
.end method

.method public final k()F
    .locals 1

    .prologue
    .line 314517
    iget v0, p0, Lcom/facebook/csslayout/YogaNode;->mLeft:F

    return v0
.end method

.method public final k(F)V
    .locals 2

    .prologue
    .line 314515
    iget-wide v0, p0, Lcom/facebook/csslayout/YogaNode;->d:J

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/csslayout/YogaNode;->jni_YGNodeStyleSetAspectRatio(JF)V

    .line 314516
    return-void
.end method

.method public final l()F
    .locals 1

    .prologue
    .line 314514
    iget v0, p0, Lcom/facebook/csslayout/YogaNode;->mTop:F

    return v0
.end method

.method public final m()F
    .locals 1

    .prologue
    .line 314513
    iget v0, p0, Lcom/facebook/csslayout/YogaNode;->mWidth:F

    return v0
.end method

.method public final measure(FIFI)J
    .locals 6
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 314510
    invoke-virtual {p0}, Lcom/facebook/csslayout/YogaNode;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 314511
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Measure function isn\'t defined!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314512
    :cond_0
    iget-object v0, p0, Lcom/facebook/csslayout/YogaNode;->c:Lcom/facebook/csslayout/YogaMeasureFunction;

    invoke-static {}, Lcom/facebook/csslayout/YogaMeasureMode;->values()[Lcom/facebook/csslayout/YogaMeasureMode;

    move-result-object v1

    aget-object v3, v1, p2

    invoke-static {}, Lcom/facebook/csslayout/YogaMeasureMode;->values()[Lcom/facebook/csslayout/YogaMeasureMode;

    move-result-object v1

    aget-object v5, v1, p4

    move-object v1, p0

    move v2, p1

    move v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/facebook/csslayout/YogaMeasureFunction;->measure(LX/1mn;FLcom/facebook/csslayout/YogaMeasureMode;FLcom/facebook/csslayout/YogaMeasureMode;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final n()F
    .locals 1

    .prologue
    .line 314509
    iget v0, p0, Lcom/facebook/csslayout/YogaNode;->mHeight:F

    return v0
.end method

.method public final o()Lcom/facebook/csslayout/YogaDirection;
    .locals 2

    .prologue
    .line 314508
    invoke-static {}, Lcom/facebook/csslayout/YogaDirection;->values()[Lcom/facebook/csslayout/YogaDirection;

    move-result-object v0

    iget v1, p0, Lcom/facebook/csslayout/YogaNode;->mLayoutDirection:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 314507
    iget-object v0, p0, Lcom/facebook/csslayout/YogaNode;->c:Lcom/facebook/csslayout/YogaMeasureFunction;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 314506
    iget-object v0, p0, Lcom/facebook/csslayout/YogaNode;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public final synthetic r()LX/1mn;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 314504
    iget-object v0, p0, Lcom/facebook/csslayout/YogaNode;->a:Lcom/facebook/csslayout/YogaNode;

    move-object v0, v0

    .line 314505
    return-object v0
.end method
