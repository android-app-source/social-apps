.class public final enum Lcom/facebook/csslayout/YogaFlexDirection;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/csslayout/YogaFlexDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/csslayout/YogaFlexDirection;

.field public static final enum COLUMN:Lcom/facebook/csslayout/YogaFlexDirection;

.field public static final enum COLUMN_REVERSE:Lcom/facebook/csslayout/YogaFlexDirection;

.field public static final enum ROW:Lcom/facebook/csslayout/YogaFlexDirection;

.field public static final enum ROW_REVERSE:Lcom/facebook/csslayout/YogaFlexDirection;


# instance fields
.field private mIntValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 218806
    new-instance v0, Lcom/facebook/csslayout/YogaFlexDirection;

    const-string v1, "COLUMN"

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/csslayout/YogaFlexDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->COLUMN:Lcom/facebook/csslayout/YogaFlexDirection;

    .line 218807
    new-instance v0, Lcom/facebook/csslayout/YogaFlexDirection;

    const-string v1, "COLUMN_REVERSE"

    invoke-direct {v0, v1, v3, v3}, Lcom/facebook/csslayout/YogaFlexDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->COLUMN_REVERSE:Lcom/facebook/csslayout/YogaFlexDirection;

    .line 218808
    new-instance v0, Lcom/facebook/csslayout/YogaFlexDirection;

    const-string v1, "ROW"

    invoke-direct {v0, v1, v4, v4}, Lcom/facebook/csslayout/YogaFlexDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->ROW:Lcom/facebook/csslayout/YogaFlexDirection;

    .line 218809
    new-instance v0, Lcom/facebook/csslayout/YogaFlexDirection;

    const-string v1, "ROW_REVERSE"

    invoke-direct {v0, v1, v5, v5}, Lcom/facebook/csslayout/YogaFlexDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->ROW_REVERSE:Lcom/facebook/csslayout/YogaFlexDirection;

    .line 218810
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/csslayout/YogaFlexDirection;

    sget-object v1, Lcom/facebook/csslayout/YogaFlexDirection;->COLUMN:Lcom/facebook/csslayout/YogaFlexDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/csslayout/YogaFlexDirection;->COLUMN_REVERSE:Lcom/facebook/csslayout/YogaFlexDirection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/csslayout/YogaFlexDirection;->ROW:Lcom/facebook/csslayout/YogaFlexDirection;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/csslayout/YogaFlexDirection;->ROW_REVERSE:Lcom/facebook/csslayout/YogaFlexDirection;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->$VALUES:[Lcom/facebook/csslayout/YogaFlexDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 218811
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 218812
    iput p3, p0, Lcom/facebook/csslayout/YogaFlexDirection;->mIntValue:I

    .line 218813
    return-void
.end method

.method public static fromInt(I)Lcom/facebook/csslayout/YogaFlexDirection;
    .locals 3

    .prologue
    .line 218814
    packed-switch p0, :pswitch_data_0

    .line 218815
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unkown enum value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218816
    :pswitch_0
    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->COLUMN:Lcom/facebook/csslayout/YogaFlexDirection;

    .line 218817
    :goto_0
    return-object v0

    .line 218818
    :pswitch_1
    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->COLUMN_REVERSE:Lcom/facebook/csslayout/YogaFlexDirection;

    goto :goto_0

    .line 218819
    :pswitch_2
    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->ROW:Lcom/facebook/csslayout/YogaFlexDirection;

    goto :goto_0

    .line 218820
    :pswitch_3
    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->ROW_REVERSE:Lcom/facebook/csslayout/YogaFlexDirection;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaFlexDirection;
    .locals 1

    .prologue
    .line 218821
    const-class v0, Lcom/facebook/csslayout/YogaFlexDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaFlexDirection;

    return-object v0
.end method

.method public static values()[Lcom/facebook/csslayout/YogaFlexDirection;
    .locals 1

    .prologue
    .line 218822
    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->$VALUES:[Lcom/facebook/csslayout/YogaFlexDirection;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/csslayout/YogaFlexDirection;

    return-object v0
.end method


# virtual methods
.method public final intValue()I
    .locals 1

    .prologue
    .line 218823
    iget v0, p0, Lcom/facebook/csslayout/YogaFlexDirection;->mIntValue:I

    return v0
.end method
