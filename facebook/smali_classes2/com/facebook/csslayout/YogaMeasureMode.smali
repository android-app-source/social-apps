.class public final enum Lcom/facebook/csslayout/YogaMeasureMode;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/csslayout/YogaMeasureMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/csslayout/YogaMeasureMode;

.field public static final enum AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

.field public static final enum EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

.field public static final enum UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;


# instance fields
.field private mIntValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 317373
    new-instance v0, Lcom/facebook/csslayout/YogaMeasureMode;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/csslayout/YogaMeasureMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 317374
    new-instance v0, Lcom/facebook/csslayout/YogaMeasureMode;

    const-string v1, "EXACTLY"

    invoke-direct {v0, v1, v3, v3}, Lcom/facebook/csslayout/YogaMeasureMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 317375
    new-instance v0, Lcom/facebook/csslayout/YogaMeasureMode;

    const-string v1, "AT_MOST"

    invoke-direct {v0, v1, v4, v4}, Lcom/facebook/csslayout/YogaMeasureMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 317376
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/csslayout/YogaMeasureMode;

    sget-object v1, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->$VALUES:[Lcom/facebook/csslayout/YogaMeasureMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 317370
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 317371
    iput p3, p0, Lcom/facebook/csslayout/YogaMeasureMode;->mIntValue:I

    .line 317372
    return-void
.end method

.method public static fromInt(I)Lcom/facebook/csslayout/YogaMeasureMode;
    .locals 3

    .prologue
    .line 317377
    packed-switch p0, :pswitch_data_0

    .line 317378
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unkown enum value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317379
    :pswitch_0
    sget-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->UNDEFINED:Lcom/facebook/csslayout/YogaMeasureMode;

    .line 317380
    :goto_0
    return-object v0

    .line 317381
    :pswitch_1
    sget-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    goto :goto_0

    .line 317382
    :pswitch_2
    sget-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaMeasureMode;
    .locals 1

    .prologue
    .line 317369
    const-class v0, Lcom/facebook/csslayout/YogaMeasureMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaMeasureMode;

    return-object v0
.end method

.method public static values()[Lcom/facebook/csslayout/YogaMeasureMode;
    .locals 1

    .prologue
    .line 317367
    sget-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->$VALUES:[Lcom/facebook/csslayout/YogaMeasureMode;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/csslayout/YogaMeasureMode;

    return-object v0
.end method


# virtual methods
.method public final intValue()I
    .locals 1

    .prologue
    .line 317368
    iget v0, p0, Lcom/facebook/csslayout/YogaMeasureMode;->mIntValue:I

    return v0
.end method
