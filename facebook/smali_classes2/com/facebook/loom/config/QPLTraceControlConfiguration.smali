.class public Lcom/facebook/loom/config/QPLTraceControlConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pm;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/loom/config/QPLTraceControlConfigurationDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/loom/config/QPLTraceControlConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private final mCoinflipSampleRate:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "coinflip_sample_rate"
    .end annotation
.end field

.field private mCpuSamplingRateMs:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cpu_sampling_rate_ms"
    .end annotation
.end field

.field private final mEnabledEventProviders:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "enabled_event_providers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 57237
    const-class v0, Lcom/facebook/loom/config/QPLTraceControlConfigurationDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57235
    const/4 v0, -0x1

    invoke-direct {p0, v1, v0, v1}, Lcom/facebook/loom/config/QPLTraceControlConfiguration;-><init>(III)V

    .line 57236
    return-void
.end method

.method public constructor <init>(III)V
    .locals 1

    .prologue
    .line 57229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57230
    iput p1, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->mCoinflipSampleRate:I

    .line 57231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->mEnabledEventProviders:LX/0Px;

    .line 57232
    iput p2, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->a:I

    .line 57233
    iput p3, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->mCpuSamplingRateMs:I

    .line 57234
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57226
    iget-object v0, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->mEnabledEventProviders:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->mEnabledEventProviders:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57227
    iget-object v0, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->mEnabledEventProviders:LX/0Px;

    invoke-static {v0}, Lcom/facebook/loom/config/LoomConfiguration;->a(Ljava/util/List;)I

    move-result v0

    iput v0, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->a:I

    .line 57228
    :cond_0
    return-object p0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 57225
    iget v0, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->mCoinflipSampleRate:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 57223
    iget v0, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->a:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 57224
    iget v0, p0, Lcom/facebook/loom/config/QPLTraceControlConfiguration;->mCpuSamplingRateMs:I

    return v0
.end method
