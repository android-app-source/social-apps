.class public Lcom/facebook/loom/provider/SystemCounterThread;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private a:I

.field private b:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private c:Landroid/os/HandlerThread;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private d:Landroid/os/Handler;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final e:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56956
    const-string v0, "loom"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 56957
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56915
    const/16 v0, 0x32

    iput v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->a:I

    .line 56916
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->b:Z

    .line 56917
    iput-object v1, p0, Lcom/facebook/loom/provider/SystemCounterThread;->c:Landroid/os/HandlerThread;

    .line 56918
    iput-object v1, p0, Lcom/facebook/loom/provider/SystemCounterThread;->d:Landroid/os/Handler;

    .line 56919
    new-instance v0, Lcom/facebook/loom/provider/SystemCounterThread$1;

    invoke-direct {v0, p0}, Lcom/facebook/loom/provider/SystemCounterThread$1;-><init>(Lcom/facebook/loom/provider/SystemCounterThread;)V

    iput-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->e:Ljava/lang/Runnable;

    .line 56920
    return-void
.end method

.method private declared-synchronized c()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.os.HandlerThread._Constructor",
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    .line 56921
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->d:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 56922
    :goto_0
    monitor-exit p0

    return-void

    .line 56923
    :cond_0
    :try_start_1
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Loom System Counters"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->c:Landroid/os/HandlerThread;

    .line 56924
    iget-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 56925
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/loom/provider/SystemCounterThread;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->d:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 56926
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 56927
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/facebook/loom/provider/SystemCounterThread;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized e(Lcom/facebook/loom/provider/SystemCounterThread;)V
    .locals 5

    .prologue
    .line 56928
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/facebook/loom/provider/SystemCounterThread;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56929
    invoke-static {}, LX/GzP;->a()V

    .line 56930
    invoke-virtual {p0}, Lcom/facebook/loom/provider/SystemCounterThread;->logThreadCounters()V

    .line 56931
    iget-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/loom/provider/SystemCounterThread;->e:Ljava/lang/Runnable;

    iget v2, p0, Lcom/facebook/loom/provider/SystemCounterThread;->a:I

    int-to-long v2, v2

    const v4, 0x7bb04282

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56932
    :cond_0
    monitor-exit p0

    return-void

    .line 56933
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static native initHybrid()Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 56934
    monitor-enter p0

    const/16 v0, 0x40

    :try_start_0
    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 56935
    :goto_0
    monitor-exit p0

    return-void

    .line 56936
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/facebook/loom/provider/SystemCounterThread;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 56937
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->b:Z

    .line 56938
    invoke-direct {p0}, Lcom/facebook/loom/provider/SystemCounterThread;->c()V

    .line 56939
    invoke-static {}, Landroid/os/Debug;->startAllocCounting()V

    .line 56940
    invoke-static {p0}, Lcom/facebook/loom/provider/SystemCounterThread;->e(Lcom/facebook/loom/provider/SystemCounterThread;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 56941
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 56942
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->b:Z

    if-eqz v0, :cond_0

    .line 56943
    invoke-static {}, LX/GzP;->a()V

    .line 56944
    invoke-virtual {p0}, Lcom/facebook/loom/provider/SystemCounterThread;->logThreadCounters()V

    .line 56945
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->b:Z

    .line 56946
    iget-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->mHybridData:Lcom/facebook/jni/HybridData;

    if-eqz v0, :cond_1

    .line 56947
    iget-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->mHybridData:Lcom/facebook/jni/HybridData;

    invoke-virtual {v0}, Lcom/facebook/jni/HybridData;->resetNative()V

    .line 56948
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 56949
    :cond_1
    iget-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->c:Landroid/os/HandlerThread;

    if-eqz v0, :cond_2

    .line 56950
    iget-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 56951
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->c:Landroid/os/HandlerThread;

    .line 56952
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/loom/provider/SystemCounterThread;->d:Landroid/os/Handler;

    .line 56953
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56954
    monitor-exit p0

    return-void

    .line 56955
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public native logThreadCounters()V
.end method
