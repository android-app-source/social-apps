.class public Lcom/facebook/loom/provider/NetworkProvider;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57018
    const-string v0, "loom"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 57019
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57016
    invoke-static {}, Lcom/facebook/loom/provider/NetworkProvider;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/loom/provider/NetworkProvider;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 57017
    return-void
.end method

.method private static native initHybrid()Lcom/facebook/jni/HybridData;
.end method

.method private native nativeDisable()V
.end method

.method private native nativeEnable()V
.end method

.method private native setTigonService(Lcom/facebook/tigon/tigonapi/TigonXplatService;Ljava/util/concurrent/Executor;)V
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 57012
    const/16 v0, 0x100

    invoke-static {v0}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 57013
    :goto_0
    return-void

    .line 57014
    :cond_0
    invoke-direct {p0}, Lcom/facebook/loom/provider/NetworkProvider;->nativeEnable()V

    goto :goto_0
.end method

.method public final a(LX/03A;)V
    .locals 2

    .prologue
    .line 57007
    iget-object v0, p1, LX/03A;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 57008
    check-cast v0, Lcom/facebook/tigon/tigonapi/TigonXplatService;

    .line 57009
    iget-object v1, p1, LX/03A;->b:Ljava/util/concurrent/ExecutorService;

    move-object v1, v1

    .line 57010
    invoke-direct {p0, v0, v1}, Lcom/facebook/loom/provider/NetworkProvider;->setTigonService(Lcom/facebook/tigon/tigonapi/TigonXplatService;Ljava/util/concurrent/Executor;)V

    .line 57011
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 57005
    invoke-direct {p0}, Lcom/facebook/loom/provider/NetworkProvider;->nativeDisable()V

    .line 57006
    return-void
.end method
