.class public final Lcom/facebook/loom/provider/NativeEventProvider;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 57192
    sget-boolean v0, Lcom/facebook/loom/core/TraceEvents;->a:Z

    if-eqz v0, :cond_0

    .line 57193
    invoke-static {}, Lcom/facebook/loom/provider/NativeEventProvider;->insertNativeAnnotations()V

    .line 57194
    :cond_0
    return-void
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 57189
    sget-boolean v0, Lcom/facebook/loom/core/TraceEvents;->a:Z

    if-eqz v0, :cond_0

    .line 57190
    invoke-static {}, Lcom/facebook/loom/provider/NativeEventProvider;->insertNativeCounters()V

    .line 57191
    :cond_0
    return-void
.end method

.method public static native insertNativeAnnotations()V
.end method

.method public static native insertNativeCounters()V
.end method
