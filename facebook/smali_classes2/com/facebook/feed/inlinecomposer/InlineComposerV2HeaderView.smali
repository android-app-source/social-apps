.class public Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements LX/1aQ;
.implements LX/1aT;


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1E1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private d:Ljava/lang/Integer;

.field private e:Landroid/widget/TextView;

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 276904
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 276905
    invoke-direct {p0, p1}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a(Landroid/content/Context;)V

    .line 276906
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 276907
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 276908
    invoke-direct {p0, p1}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a(Landroid/content/Context;)V

    .line 276909
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 276910
    const-class v0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    invoke-static {v0, p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 276911
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->c:Landroid/content/Context;

    .line 276912
    const v0, 0x7f030636

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 276913
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->setOrientation(I)V

    .line 276914
    const v0, 0x7f0d111e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->e:Landroid/widget/TextView;

    .line 276915
    const v0, 0x7f0d111c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 276916
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->b:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->b:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 276917
    const v0, 0x7f0d112c

    invoke-virtual {p0, v0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 276918
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 276919
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;LX/0hB;LX/1E1;)V
    .locals 0

    .prologue
    .line 276920
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a:LX/0hB;

    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->b:LX/1E1;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    invoke-static {v1}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-static {v1}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v1

    check-cast v1, LX/1E1;

    invoke-static {p0, v0, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a(Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;LX/0hB;LX/1E1;)V

    return-void
.end method

.method private b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 276921
    new-instance v0, LX/1bW;

    invoke-direct {v0, p0, p1}, LX/1bW;-><init>(Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 276922
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 276923
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 276924
    const v1, 0x7f0a015a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 276925
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 276926
    const v1, 0x7f0b0033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 276927
    return-void
.end method

.method private getFlyoutMaximumWidth()I
    .locals 3

    .prologue
    .line 276928
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 276929
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 276930
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v0, v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 276931
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 276932
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 276873
    invoke-direct {p0, p1}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->b(Landroid/view/View$OnClickListener;)V

    .line 276874
    invoke-direct {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->d()V

    .line 276875
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 276933
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276934
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 276935
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    .line 276936
    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    .line 276937
    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    .line 276938
    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 276939
    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    .line 276940
    if-eqz p1, :cond_0

    const v0, 0x7f02152f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 276941
    invoke-virtual {v1, v3, v2, v5, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 276942
    return-void

    .line 276943
    :cond_0
    const v0, 0x7f02152e

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 276903
    const v0, 0x7f0d2794

    invoke-virtual {p0, v0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewStub;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 276944
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 276945
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 276868
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 276869
    if-eqz v0, :cond_0

    .line 276870
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getCollapsedHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 276871
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->requestLayout()V

    .line 276872
    :cond_0
    return-void
.end method

.method public getAttachmentInsertPoint()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 276876
    const v0, 0x7f0d2797

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getCollapsedHeight()I
    .locals 2

    .prologue
    .line 276877
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b11c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getExpandedFlyoutHeight()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 276878
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getFlyoutView()Landroid/view/View;

    move-result-object v0

    .line 276879
    invoke-direct {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getFlyoutMaximumWidth()I

    move-result v1

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 276880
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getFlyoutView()Landroid/view/View;
    .locals 1

    .prologue
    .line 276881
    const v0, 0x7f0d2793

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getFlyoutXoutButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 276882
    const v0, 0x7f0d2795

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getIconView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1

    .prologue
    .line 276883
    const v0, 0x7f0d279a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->b(I)LX/0am;

    move-result-object v0

    .line 276884
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 276885
    const v0, 0x7f0d112a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 276886
    :cond_0
    const v0, 0x7f0d279a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public getPromptDisplayReasonView()Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;
    .locals 1

    .prologue
    .line 276887
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 276888
    const v0, 0x7f0d2794

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 276889
    :cond_0
    const v0, 0x7f0d2798

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    return-object v0
.end method

.method public getPromptSubtitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 276890
    const v0, 0x7f0d0b8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getPromptTitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 276891
    const v0, 0x7f0d0b8d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getShimmerContainer()Lcom/facebook/widget/ShimmerFrameLayout;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 276892
    const v0, 0x7f0d2799

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ShimmerFrameLayout;

    .line 276893
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ShimmerFrameLayout;->setVisibility(I)V

    .line 276894
    return-object v0
.end method

.method public getV2AttachmentView()Landroid/view/View;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 276895
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 276896
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onMeasure(II)V

    .line 276897
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 276898
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->getMeasuredHeight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->d:Ljava/lang/Integer;

    .line 276899
    :cond_0
    return-void
.end method

.method public setHintColor(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 276900
    if-nez p1, :cond_0

    .line 276901
    :goto_0
    return-void

    .line 276902
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->e:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
