.class public Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "Lcom/facebook/feed/inlinecomposer/multirow/common/HasComposerLauncherContext;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1EE;",
        "LX/1aH;",
        "TE;",
        "Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1RV;

.field private static r:LX/0Xm;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0ad;

.field private final f:Landroid/content/Context;

.field private final g:LX/0W9;

.field public final h:LX/0Yi;

.field public final i:LX/1Ns;

.field public final j:LX/1Np;

.field public final k:LX/1Nq;

.field private final l:LX/0oJ;

.field public final m:Lcom/facebook/content/SecureContextHelper;

.field public final n:LX/1RW;

.field public final o:LX/1RX;

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1E8;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0fO;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 246304
    new-instance v0, LX/1RU;

    invoke-direct {v0}, LX/1RU;-><init>()V

    sput-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->a:LX/1Cz;

    .line 246305
    new-instance v0, LX/1RV;

    const v1, 0x7f0a0166

    invoke-direct {v0, v1, v2, v2}, LX/1RV;-><init>(IZZ)V

    sput-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->b:LX/1RV;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0ad;Landroid/content/Context;LX/0W9;LX/0Yi;LX/1Ns;LX/1Np;LX/1Nq;LX/0oJ;Lcom/facebook/content/SecureContextHelper;LX/1RW;LX/1RX;LX/0Or;LX/0fO;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;",
            "LX/0ad;",
            "Landroid/content/Context;",
            "LX/0W9;",
            "LX/0Yi;",
            "LX/1Ns;",
            "LX/1Np;",
            "LX/1Nq;",
            "LX/0oJ;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1RW;",
            "LX/1RX;",
            "LX/0Or",
            "<",
            "LX/1E8;",
            ">;",
            "LX/0fO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 246210
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 246211
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->c:LX/0Ot;

    .line 246212
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->d:LX/0Ot;

    .line 246213
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->e:LX/0ad;

    .line 246214
    iput-object p4, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->f:Landroid/content/Context;

    .line 246215
    iput-object p5, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->g:LX/0W9;

    .line 246216
    iput-object p6, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->h:LX/0Yi;

    .line 246217
    iput-object p7, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->i:LX/1Ns;

    .line 246218
    iput-object p8, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->j:LX/1Np;

    .line 246219
    iput-object p9, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->k:LX/1Nq;

    .line 246220
    iput-object p10, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->l:LX/0oJ;

    .line 246221
    iput-object p11, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->m:Lcom/facebook/content/SecureContextHelper;

    .line 246222
    iput-object p12, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->n:LX/1RW;

    .line 246223
    iput-object p13, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->o:LX/1RX;

    .line 246224
    iput-object p14, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->p:LX/0Or;

    .line 246225
    iput-object p15, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->q:LX/0fO;

    .line 246226
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;
    .locals 3

    .prologue
    .line 246296
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;

    monitor-enter v1

    .line 246297
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->r:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 246298
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->r:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 246299
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246300
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 246301
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246302
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 246303
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;
    .locals 17

    .prologue
    .line 246294
    new-instance v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;

    const/16 v2, 0x64f

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xe0f

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const-class v5, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v6

    check-cast v6, LX/0W9;

    invoke-static/range {p0 .. p0}, LX/0Yi;->a(LX/0QB;)LX/0Yi;

    move-result-object v7

    check-cast v7, LX/0Yi;

    const-class v8, LX/1Ns;

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1Ns;

    invoke-static/range {p0 .. p0}, LX/1Np;->a(LX/0QB;)LX/1Np;

    move-result-object v9

    check-cast v9, LX/1Np;

    invoke-static/range {p0 .. p0}, LX/1Nq;->a(LX/0QB;)LX/1Nq;

    move-result-object v10

    check-cast v10, LX/1Nq;

    invoke-static/range {p0 .. p0}, LX/0oJ;->a(LX/0QB;)LX/0oJ;

    move-result-object v11

    check-cast v11, LX/0oJ;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v12

    check-cast v12, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/1RW;->a(LX/0QB;)LX/1RW;

    move-result-object v13

    check-cast v13, LX/1RW;

    invoke-static/range {p0 .. p0}, LX/1RX;->a(LX/0QB;)LX/1RX;

    move-result-object v14

    check-cast v14, LX/1RX;

    const/16 v15, 0x64b

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0fO;->a(LX/0QB;)LX/0fO;

    move-result-object v16

    check-cast v16, LX/0fO;

    invoke-direct/range {v1 .. v16}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0ad;Landroid/content/Context;LX/0W9;LX/0Yi;LX/1Ns;LX/1Np;LX/1Nq;LX/0oJ;Lcom/facebook/content/SecureContextHelper;LX/1RW;LX/1RX;LX/0Or;LX/0fO;)V

    .line 246295
    return-object v1
.end method

.method public static c(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 246293
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->e:LX/0ad;

    sget-short v1, LX/1EB;->y:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->k:LX/1Nq;

    invoke-static {}, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;->c()Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 246306
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 246276
    check-cast p2, LX/1EE;

    check-cast p3, LX/1Pn;

    .line 246277
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 246278
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    sget-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->b:LX/1RV;

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 246279
    const v3, 0x7f0d1123

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    .line 246280
    iget-boolean v2, p2, LX/1EE;->t:Z

    move v2, v2

    .line 246281
    if-eqz v2, :cond_0

    .line 246282
    new-instance v2, LX/1vA;

    invoke-direct {v2, p0, p3, v0}, LX/1vA;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;LX/1Pn;Landroid/app/Activity;)V

    move-object v2, v2

    .line 246283
    :goto_0
    invoke-interface {p1, v3, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 246284
    const v2, 0x7f0d1124

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    .line 246285
    new-instance v3, LX/1aF;

    invoke-direct {v3, p0, p3, v0}, LX/1aF;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;LX/1Pn;Landroid/app/Activity;)V

    move-object v3, v3

    .line 246286
    invoke-interface {p1, v2, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 246287
    const v2, 0x7f0d1125

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    .line 246288
    new-instance v3, LX/1aG;

    invoke-direct {v3, p0, p3, v0}, LX/1aG;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;LX/1Pn;Landroid/app/Activity;)V

    move-object v0, v3

    .line 246289
    invoke-interface {p1, v2, v1, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 246290
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->g:LX/0W9;

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E8;

    invoke-virtual {v0}, LX/1E8;->j()Z

    move-result v0

    invoke-static {v1, p2, v2, v0}, LX/1aH;->a(Landroid/content/res/Resources;LX/1EE;LX/0W9;Z)LX/1aH;

    move-result-object v0

    return-object v0

    .line 246291
    :cond_0
    new-instance v2, LX/1aE;

    invoke-direct {v2, p0, p3, p2, v0}, LX/1aE;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;LX/1Pn;LX/1EE;Landroid/app/Activity;)V

    move-object v2, v2

    .line 246292
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5061035c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 246241
    check-cast p1, LX/1EE;

    check-cast p2, LX/1aH;

    check-cast p4, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 246242
    iget-boolean v1, p1, LX/1EE;->c:Z

    move v1, v1

    .line 246243
    if-eqz v1, :cond_7

    move v1, v2

    :goto_0
    invoke-virtual {p4, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonVisibility(I)V

    .line 246244
    iget-object v1, p2, LX/1aH;->b:Ljava/lang/String;

    iget-object v5, p2, LX/1aH;->a:Ljava/lang/String;

    iget-object p3, p2, LX/1aH;->c:Ljava/lang/String;

    invoke-virtual {p4, v1, v5, p3}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246245
    iget v1, p2, LX/1aH;->d:I

    if-eqz v1, :cond_0

    .line 246246
    iget v1, p2, LX/1aH;->d:I

    invoke-virtual {p4, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setStatusButtonDrawable(I)V

    .line 246247
    :cond_0
    iget v1, p2, LX/1aH;->e:I

    const/4 v5, -0x1

    if-eq v1, v5, :cond_1

    .line 246248
    iget v1, p2, LX/1aH;->e:I

    invoke-virtual {p4, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setStatusButtonDrawablePadding(I)V

    .line 246249
    :cond_1
    iget v1, p1, LX/1EE;->k:I

    move v1, v1

    .line 246250
    if-eqz v1, :cond_2

    .line 246251
    iget v1, p1, LX/1EE;->k:I

    move v1, v1

    .line 246252
    invoke-virtual {p4, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setPhotoButtonDrawable(I)V

    .line 246253
    :cond_2
    iget-object v1, p2, LX/1aH;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 246254
    iget-object v1, p2, LX/1aH;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 246255
    :cond_3
    iget-boolean v1, p1, LX/1EE;->u:Z

    move v1, v1

    .line 246256
    if-eqz v1, :cond_4

    .line 246257
    iget v1, p1, LX/1EE;->m:I

    move v1, v1

    .line 246258
    iget v5, p1, LX/1EE;->n:I

    move v5, v5

    .line 246259
    iget p3, p1, LX/1EE;->o:I

    move p3, p3

    .line 246260
    invoke-virtual {p4, v1, v5, p3}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a(III)V

    .line 246261
    :cond_4
    iget-object v1, p1, LX/1EE;->r:Ljava/lang/Integer;

    move-object v1, v1

    .line 246262
    if-eqz v1, :cond_5

    .line 246263
    iget-object v1, p1, LX/1EE;->r:Ljava/lang/Integer;

    move-object v1, v1

    .line 246264
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setButtonTextColor(I)V

    .line 246265
    :cond_5
    const/4 v1, 0x0

    .line 246266
    iget-object v5, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->o:LX/1RX;

    invoke-virtual {v5}, LX/1RX;->a()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->o:LX/1RX;

    invoke-virtual {v5}, LX/1RX;->b()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->e:LX/0ad;

    sget-short p3, LX/1aO;->R:S

    invoke-interface {v5, p3, v1}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v1, 0x1

    :cond_6
    move v1, v1

    .line 246267
    if-eqz v1, :cond_8

    .line 246268
    :goto_1
    new-instance v1, LX/1aN;

    invoke-direct {v1, p0}, LX/1aN;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;)V

    move-object v1, v1

    .line 246269
    iget-object v4, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->e:LX/0ad;

    sget-char v5, LX/1aO;->P:C

    const/4 p3, 0x0

    invoke-interface {v4, v5, p3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p4, v2, v1, v4}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a(ILandroid/view/View$OnClickListener;Ljava/lang/String;)V

    .line 246270
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->h:LX/0Yi;

    .line 246271
    iget-object v2, v1, LX/0Yi;->j:LX/0Yl;

    const v4, 0xa009b

    const-string v5, "NNFColdLoadInlineComposerAfterLoggedIn"

    invoke-virtual {v2, v4, v5}, LX/0Yl;->c(ILjava/lang/String;)LX/0Yl;

    .line 246272
    iget-object v2, v1, LX/0Yi;->j:LX/0Yl;

    const v4, 0xa009c

    const-string v5, "NNFWarmLoadInlineComposerAfterLoggedIn"

    invoke-virtual {v2, v4, v5}, LX/0Yl;->f(ILjava/lang/String;)LX/0Yl;

    .line 246273
    const/16 v1, 0x1f

    const v2, -0x6d6822af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_7
    move v1, v4

    .line 246274
    goto/16 :goto_0

    :cond_8
    move v2, v4

    .line 246275
    goto :goto_1
.end method

.method public final a(LX/1EE;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 246231
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->e:LX/0ad;

    sget-short v2, LX/1Nu;->o:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->e:LX/0ad;

    sget-short v2, LX/1RY;->v:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->e:LX/0ad;

    sget-short v2, LX/0fe;->aG:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 246232
    :cond_0
    :goto_0
    return v0

    .line 246233
    :cond_1
    invoke-virtual {p1}, LX/1EE;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->l:LX/0oJ;

    .line 246234
    sget-short v2, LX/0ob;->F:S

    sget-short v3, LX/0ob;->t:S

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, LX/0oJ;->a(LX/0oJ;SSZ)Z

    move-result v2

    move v1, v2

    .line 246235
    if-eqz v1, :cond_0

    .line 246236
    :cond_2
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->q:LX/0fO;

    .line 246237
    invoke-static {v1}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v2

    invoke-virtual {v2}, LX/0i8;->k()Z

    move-result v2

    move v1, v2

    .line 246238
    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->q:LX/0fO;

    invoke-virtual {v1}, LX/0fO;->o()Z

    move-result v1

    if-nez v1, :cond_0

    .line 246239
    iget-boolean v1, p1, LX/1EE;->d:Z

    move v1, v1

    .line 246240
    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->e:LX/0ad;

    sget-short v2, LX/1RY;->e:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 246230
    check-cast p1, LX/1EE;

    invoke-virtual {p0, p1}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->a(LX/1EE;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 246227
    check-cast p4, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    .line 246228
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonVisibility(I)V

    .line 246229
    return-void
.end method
