.class public Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/feed/inlinecomposer/multirow/common/HasNextDefinitionInfo;",
        ":",
        "Lcom/facebook/feed/inlinecomposer/multirow/common/HasPreviousDefinitionInfo;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1bP;",
        "LX/1bV;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/1E1;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1E1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 280481
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 280482
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    .line 280483
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;->b:LX/1E1;

    .line 280484
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;
    .locals 5

    .prologue
    .line 280485
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;

    monitor-enter v1

    .line 280486
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 280487
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 280488
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280489
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 280490
    new-instance p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v4

    check-cast v4, LX/1E1;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;-><init>(Landroid/content/res/Resources;LX/1E1;)V

    .line 280491
    move-object v0, p0

    .line 280492
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 280493
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280494
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 280495
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 280496
    check-cast p2, LX/1bP;

    check-cast p3, LX/1Qi;

    const/4 v1, 0x0

    .line 280497
    move-object v0, p3

    check-cast v0, LX/1Qi;

    .line 280498
    iget-boolean v2, v0, LX/1Qi;->t:Z

    move v4, v2

    .line 280499
    iget-boolean v0, p3, LX/1Qi;->s:Z

    move v5, v0

    .line 280500
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;->b:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    iget-boolean v2, p2, LX/1bP;->d:Z

    .line 280501
    if-eqz v0, :cond_6

    if-nez v2, :cond_6

    .line 280502
    const v3, 0x7f020aa9

    .line 280503
    :goto_0
    move v6, v3

    .line 280504
    iget-boolean v0, p2, LX/1bP;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b0112

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 280505
    :goto_1
    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 280506
    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v7, 0x7f0b00d8

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 280507
    iget-boolean v2, p2, LX/1bP;->a:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_2
    add-int/2addr v2, v7

    .line 280508
    iget-boolean v8, p2, LX/1bP;->c:Z

    if-eqz v8, :cond_2

    :goto_3
    add-int/2addr v3, v7

    .line 280509
    if-eqz v4, :cond_5

    .line 280510
    sub-int v1, v2, v7

    .line 280511
    :goto_4
    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;->b:LX/1E1;

    invoke-virtual {v2}, LX/1E1;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 280512
    iget-boolean v2, p2, LX/1bP;->d:Z

    if-eqz v2, :cond_4

    .line 280513
    sub-int v2, v3, v7

    .line 280514
    :goto_5
    new-instance v3, LX/1bV;

    invoke-direct {v3, v6, v0, v1, v2}, LX/1bV;-><init>(IIII)V

    return-object v3

    :cond_0
    move v0, v1

    .line 280515
    goto :goto_1

    :cond_1
    move v2, v1

    .line 280516
    goto :goto_2

    :cond_2
    move v3, v1

    .line 280517
    goto :goto_3

    .line 280518
    :cond_3
    if-eqz v5, :cond_4

    .line 280519
    sub-int v2, v3, v7

    goto :goto_5

    :cond_4
    move v2, v3

    goto :goto_5

    :cond_5
    move v1, v2

    goto :goto_4

    .line 280520
    :cond_6
    if-eqz v4, :cond_8

    .line 280521
    if-eqz v5, :cond_7

    const v3, 0x7f020aaa

    goto :goto_0

    :cond_7
    const v3, 0x7f020aa8

    goto :goto_0

    .line 280522
    :cond_8
    if-eqz v5, :cond_9

    const v3, 0x7f020aab

    goto :goto_0

    :cond_9
    const v3, 0x7f020aa9

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4e8562a3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 280523
    check-cast p2, LX/1bV;

    .line 280524
    iget v1, p2, LX/1bV;->a:I

    invoke-virtual {p4, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 280525
    iget v1, p2, LX/1bV;->b:I

    iget v2, p2, LX/1bV;->c:I

    iget p0, p2, LX/1bV;->b:I

    iget p1, p2, LX/1bV;->d:I

    invoke-virtual {p4, v1, v2, p0, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 280526
    const/16 v1, 0x1f

    const v2, -0x7d4e1238

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
