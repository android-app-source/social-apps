.class public Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Boolean;",
        "LX/1bU;",
        "LX/1Pn;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0WJ;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Np;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 237839
    const-class v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;

    const-string v1, "inline_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0WJ;LX/0Ot;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Ot",
            "<",
            "LX/1Np;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 237869
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 237870
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->a:LX/0WJ;

    .line 237871
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->b:LX/0Ot;

    .line 237872
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->c:LX/0Ot;

    .line 237873
    iput-object p4, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 237874
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 237866
    if-eqz p0, :cond_0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0203b2

    invoke-static {v0}, LX/1bX;->a(I)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 237867
    iget-object p0, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, p0

    .line 237868
    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;
    .locals 7

    .prologue
    .line 237855
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;

    monitor-enter v1

    .line 237856
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 237857
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 237858
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237859
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 237860
    new-instance v5, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    const/16 v4, 0x64d

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v4, 0xbc6

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {v5, v3, v6, p0, v4}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;-><init>(LX/0WJ;LX/0Ot;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 237861
    move-object v0, v5

    .line 237862
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 237863
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237864
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 237865
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 237853
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 237854
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 237849
    check-cast p2, Ljava/lang/Boolean;

    check-cast p3, LX/1Pn;

    .line 237850
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 237851
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/1bT;

    invoke-direct {v1, p0, p3}, LX/1bT;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;LX/1Pn;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 237852
    :cond_0
    new-instance v0, LX/1bU;

    invoke-static {p0}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->a(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1bU;-><init>(Landroid/net/Uri;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x40aa7bdf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 237840
    check-cast p2, LX/1bU;

    check-cast p4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 237841
    invoke-static {p0}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->a(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;)Ljava/lang/String;

    move-result-object v2

    .line 237842
    iget-object v1, p2, LX/1bU;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 237843
    :goto_0
    move v1, v1

    .line 237844
    if-eqz v1, :cond_0

    .line 237845
    invoke-static {v2}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p2, LX/1bU;->a:Landroid/net/Uri;

    .line 237846
    :cond_0
    iget-object v1, p2, LX/1bU;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 237847
    const/16 v1, 0x1f

    const v2, -0x25c605f4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 237848
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
