.class public Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/1EE;",
        "Ljava/lang/Void;",
        "LX/1Qi;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerSelectorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 245711
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 245712
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;->a:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerSelectorPartDefinition;

    .line 245713
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;
    .locals 4

    .prologue
    .line 245714
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;

    monitor-enter v1

    .line 245715
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 245716
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 245717
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245718
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 245719
    new-instance p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerSelectorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerSelectorPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerSelectorPartDefinition;)V

    .line 245720
    move-object v0, p0

    .line 245721
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 245722
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245723
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 245724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 245725
    check-cast p2, LX/1EE;

    .line 245726
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;->a:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 245727
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 245728
    const/4 v0, 0x1

    return v0
.end method
