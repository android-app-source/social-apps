.class public Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/1EE;",
        "Ljava/lang/Void;",
        "LX/1Qi;",
        ">;"
    }
.end annotation


# static fields
.field private static q:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition",
            "<",
            "LX/1Qi;",
            "Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition",
            "<",
            "LX/1Qi;",
            "Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition",
            "<",
            "LX/1Qi;",
            ">;>;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition",
            "<",
            "LX/1Qi;",
            ">;>;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/meme/prompt/MemePromptPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1RH;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/1E0;

.field private final l:LX/1E1;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0ad;

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/1E0;LX/1E1;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/photoreminder/MediaReminderPromptsPromptPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/meme/prompt/MemePromptPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1RH;",
            ">;",
            "LX/1E0;",
            "LX/1E1;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 245886
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 245887
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->a:LX/0Ot;

    .line 245888
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->b:LX/0Ot;

    .line 245889
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->c:LX/0Ot;

    .line 245890
    iput-object p4, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->d:LX/0Ot;

    .line 245891
    iput-object p5, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->e:LX/0Ot;

    .line 245892
    iput-object p6, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->f:LX/0Ot;

    .line 245893
    iput-object p7, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->g:LX/0Ot;

    .line 245894
    iput-object p8, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->h:LX/0Ot;

    .line 245895
    iput-object p9, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->i:LX/0Ot;

    .line 245896
    iput-object p10, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->j:LX/0Or;

    .line 245897
    iput-object p11, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->k:LX/1E0;

    .line 245898
    iput-object p12, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->l:LX/1E1;

    .line 245899
    iput-object p13, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->m:LX/0Ot;

    .line 245900
    iput-object p14, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->o:LX/0Ot;

    .line 245901
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->p:LX/0Ot;

    .line 245902
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->n:LX/0ad;

    .line 245903
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;
    .locals 3

    .prologue
    .line 245904
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;

    monitor-enter v1

    .line 245905
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 245906
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 245907
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245908
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 245909
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245910
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 245911
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;
    .locals 19

    .prologue
    .line 245912
    new-instance v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;

    const/16 v3, 0x651

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x65a

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xfcb

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xfc9

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x6a3

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xf25

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xa92

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xa98

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xa9a

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xfd1

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/1E0;->a(LX/0QB;)LX/1E0;

    move-result-object v13

    check-cast v13, LX/1E0;

    invoke-static/range {p0 .. p0}, LX/1E1;->a(LX/0QB;)LX/1E1;

    move-result-object v14

    check-cast v14, LX/1E1;

    const/16 v15, 0x1c9f

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0xad1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0xa84

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v18

    check-cast v18, LX/0ad;

    invoke-direct/range {v2 .. v18}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/1E0;LX/1E1;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V

    .line 245913
    return-object v2
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 245914
    check-cast p2, LX/1EE;

    check-cast p3, LX/1Qi;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 245915
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->n:LX/0ad;

    sget-short v1, LX/1Nu;->n:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v5

    .line 245916
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RH;

    invoke-virtual {p3}, LX/1Qj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/1RH;->a(LX/1EE;Landroid/content/Context;)LX/AkL;

    move-result-object v6

    .line 245917
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->l:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;

    invoke-virtual {v0, p2}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerFooterPartDefinition;->a(LX/1EE;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v4, v2

    .line 245918
    :goto_0
    if-eqz v5, :cond_0

    .line 245919
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 245920
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->o:LX/0Ot;

    new-instance v1, LX/1Rd;

    invoke-virtual {p2}, LX/1EE;->f()Z

    move-result v7

    invoke-direct {v1, v7}, LX/1Rd;-><init>(Z)V

    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v7

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->l:LX/1E1;

    invoke-virtual {v1}, LX/1E1;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->l:LX/1E1;

    invoke-virtual {v1}, LX/1E1;->c()Z

    move-result v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_1
    invoke-static {p2, v6, v1, v4}, LX/1Rh;->a(LX/1EE;LX/AkL;ZZ)LX/1Rh;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 245921
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->l:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz v6, :cond_7

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->l:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 245922
    :goto_2
    new-instance v1, LX/1Ri;

    invoke-virtual {p2}, LX/1EE;->a()LX/1RN;

    move-result-object v0

    invoke-direct {v1, v0, p2, v6}, LX/1Ri;-><init>(LX/1RN;LX/0jW;LX/AkL;)V

    .line 245923
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 245924
    invoke-virtual {p2}, LX/1EE;->a()LX/1RN;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p2}, LX/1EE;->a()LX/1RN;

    move-result-object v0

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->d()LX/5ob;

    move-result-object v0

    sget-object v7, LX/5ob;->INLINE_COMPOSER:LX/5ob;

    invoke-virtual {v0, v7}, LX/5ob;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v3

    :goto_3
    and-int/2addr v0, v2

    .line 245925
    iget-object v7, v1, LX/1Ri;->a:LX/1RN;

    if-eqz v7, :cond_1

    .line 245926
    iget-object v7, v1, LX/1Ri;->a:LX/1RN;

    iget-object v7, v7, LX/1RN;->c:LX/32e;

    iget-object v7, v7, LX/32e;->a:LX/24P;

    sget-object p3, LX/24P;->MAXIMIZED:LX/24P;

    if-ne v7, p3, :cond_9

    :goto_4
    and-int v4, v0, v3

    .line 245927
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->e:LX/0Ot;

    invoke-static {p1, v4, v0, v1}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->f:LX/0Ot;

    invoke-virtual {v0, v4, v3, v1}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->g:LX/0Ot;

    invoke-virtual {v0, v4, v3, v1}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->p:LX/0Ot;

    invoke-virtual {v0, v4, v3, v1}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->i:LX/0Ot;

    invoke-virtual {v0, v4, v3, v1}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->d:LX/0Ot;

    invoke-virtual {v0, v4, v3, v1}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->h:LX/0Ot;

    invoke-virtual {v0, v4, v3, v1}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->c:LX/0Ot;

    invoke-virtual {v0, v4, v3, v1}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 245928
    if-nez v5, :cond_2

    .line 245929
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 245930
    :cond_2
    invoke-virtual {p2}, LX/1EE;->a()LX/1RN;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, LX/1EE;->a()LX/1RN;

    move-result-object v0

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->d()LX/5ob;

    .line 245931
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->l:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz v6, :cond_4

    .line 245932
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 245933
    :cond_4
    const/4 v0, 0x0

    return-object v0

    :cond_5
    move v4, v3

    .line 245934
    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 245935
    goto/16 :goto_1

    :cond_7
    move v2, v3

    .line 245936
    goto/16 :goto_2

    :cond_8
    move v0, v4

    .line 245937
    goto :goto_3

    :cond_9
    move v3, v4

    .line 245938
    goto :goto_4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 245939
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGroupPartDefinition;->k:LX/1E0;

    invoke-virtual {v0}, LX/1E0;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
