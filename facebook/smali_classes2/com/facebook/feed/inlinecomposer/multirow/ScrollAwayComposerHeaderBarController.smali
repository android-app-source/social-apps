.class public Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fm;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/03V;

.field public final c:LX/1Ns;

.field private final d:Landroid/content/Context;

.field public final e:Landroid/support/v4/app/FragmentActivity;

.field public final f:LX/1Nq;

.field public final g:LX/0ad;

.field public final h:LX/0WJ;

.field public final i:LX/1EE;

.field private final j:LX/1JA;

.field public k:Landroid/support/design/widget/AppBarLayout;

.field public l:Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 237798
    const-class v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;

    const-string v1, "inline_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/1Ns;Landroid/content/Context;Landroid/support/v4/app/FragmentActivity;LX/0ad;LX/1Nq;LX/0WJ;LX/1EE;LX/1JA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 237799
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 237800
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->b:LX/03V;

    .line 237801
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->c:LX/1Ns;

    .line 237802
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->d:Landroid/content/Context;

    .line 237803
    iput-object p4, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->e:Landroid/support/v4/app/FragmentActivity;

    .line 237804
    iput-object p5, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->g:LX/0ad;

    .line 237805
    iput-object p6, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->f:LX/1Nq;

    .line 237806
    iput-object p7, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->h:LX/0WJ;

    .line 237807
    iput-object p8, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->i:LX/1EE;

    .line 237808
    iput-object p9, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->j:LX/1JA;

    .line 237809
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 237810
    const v0, 0x7f0d120b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 237811
    const v0, 0x7f0d120f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->l:Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    .line 237812
    const v0, 0x7f0d120d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->k:Landroid/support/design/widget/AppBarLayout;

    .line 237813
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->l:Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    if-nez v0, :cond_1

    .line 237814
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->b:LX/03V;

    const-string v1, "scroll_away_composer_header"

    const-string v2, "layout is not valid for scroll away composer"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 237815
    :goto_0
    return-void

    .line 237816
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 237817
    instance-of v1, v0, LX/1uK;

    if-nez v1, :cond_2

    .line 237818
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->b:LX/03V;

    const-string v1, "scroll_away_composer_header"

    const-string v2, "newsfeed container is not in a CoordinatorLayout"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 237819
    :cond_2
    check-cast v0, LX/1uK;

    .line 237820
    new-instance v1, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;

    invoke-direct {v1}, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;-><init>()V

    invoke-virtual {v0, v1}, LX/1uK;->a(Landroid/support/design/widget/CoordinatorLayout$Behavior;)V

    .line 237821
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->l:Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    .line 237822
    new-instance v1, LX/Ajy;

    invoke-direct {v1, p0}, LX/Ajy;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;)V

    move-object v1, v1

    .line 237823
    invoke-virtual {v0, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a(Landroid/view/View$OnClickListener;)V

    .line 237824
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->l:Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    .line 237825
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->h:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 237826
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v1, v1

    .line 237827
    if-eqz v1, :cond_4

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    :goto_2
    move-object v1, v2

    .line 237828
    sget-object v2, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 237829
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->l:Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->i:LX/1EE;

    .line 237830
    iget-object v2, v1, LX/1EE;->i:Ljava/lang/String;

    move-object v1, v2

    .line 237831
    invoke-virtual {v0, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a(Ljava/lang/String;)V

    .line 237832
    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    const v2, 0x7f0203b2

    invoke-static {v2}, LX/1bX;->a(I)LX/1bX;

    move-result-object v2

    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 237833
    iget-object v1, v2, LX/1bf;->b:Landroid/net/Uri;

    move-object v2, v1

    .line 237834
    goto :goto_2
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 237835
    return-void
.end method

.method public final kw_()Z
    .locals 1

    .prologue
    .line 237836
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->j:LX/1JA;

    .line 237837
    iget-boolean p0, v0, LX/1JA;->a:Z

    move v0, p0

    .line 237838
    return v0
.end method
