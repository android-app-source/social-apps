.class public Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/feed/inlinecomposer/multirow/common/HasFeedUnitTopGetter;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1bO;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 280476
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 280477
    new-instance v0, LX/1bO;

    invoke-direct {v0}, LX/1bO;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;->a:LX/1bO;

    .line 280478
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;
    .locals 3

    .prologue
    .line 280465
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;

    monitor-enter v1

    .line 280466
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 280467
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 280468
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280469
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 280470
    new-instance v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;-><init>()V

    .line 280471
    move-object v0, v0

    .line 280472
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 280473
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280474
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 280475
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x63b8f07a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 280454
    check-cast p3, LX/1Qi;

    .line 280455
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;->a:LX/1bO;

    .line 280456
    iput-object p4, v1, LX/1bO;->b:Landroid/view/View;

    .line 280457
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;->a:LX/1bO;

    .line 280458
    iput-object v1, p3, LX/1Qi;->r:LX/1bO;

    .line 280459
    const/16 v1, 0x1f

    const v2, -0x1314c2b6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 280460
    check-cast p3, LX/1Qi;

    const/4 v1, 0x0

    .line 280461
    iput-object v1, p3, LX/1Qi;->r:LX/1bO;

    .line 280462
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;->a:LX/1bO;

    .line 280463
    iput-object v1, v0, LX/1bO;->b:Landroid/view/View;

    .line 280464
    return-void
.end method
