.class public Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/feed/inlinecomposer/multirow/common/HasComposerLauncherContext;",
        ":",
        "Lcom/facebook/feed/inlinecomposer/multirow/common/HasFeedUnitTopGetter;",
        ":",
        "Lcom/facebook/feed/inlinecomposer/multirow/common/HasNextDefinitionInfo;",
        ":",
        "Lcom/facebook/feed/inlinecomposer/multirow/common/HasPreviousDefinitionInfo;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Qa;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Rh;",
        "LX/1bS;",
        "TE;",
        "Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static w:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/prompt/InlineComposerGoodFriendsPromptIconPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/1Np;

.field private final j:LX/1Ns;

.field public final k:LX/0ad;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1RH;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Landroid/support/v4/app/FragmentActivity;

.field private final o:LX/1Rg;

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/24B;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/1E1;

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Nq;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0oJ;

.field private final u:LX/0fO;

.field private final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 246680
    new-instance v0, LX/1Rf;

    invoke-direct {v0}, LX/1Rf;-><init>()V

    sput-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1Np;LX/0ad;LX/1Ns;LX/0Or;LX/0Ot;LX/1Rg;LX/0Ot;LX/1E1;LX/0Ot;LX/0Ot;LX/0Ot;LX/0oJ;LX/0fO;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerExposedTopGetterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerProfilePhotoPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;",
            ">;",
            "LX/1Np;",
            "LX/0ad;",
            "LX/1Ns;",
            "LX/0Or",
            "<",
            "LX/1RH;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;",
            ">;",
            "Lcom/facebook/ui/animations/persistent/parts/AnimationPartFactory;",
            "LX/0Ot",
            "<",
            "LX/24B;",
            ">;",
            "LX/1E1;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Nq;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/prompt/InlineComposerGoodFriendsPromptIconPartDefinition;",
            ">;",
            "LX/0oJ;",
            "LX/0fO;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 246657
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 246658
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->b:Landroid/content/Context;

    .line 246659
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->c:LX/0Ot;

    .line 246660
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->d:LX/0Ot;

    .line 246661
    iput-object p4, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->e:LX/0Ot;

    .line 246662
    iput-object p5, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->f:LX/0Ot;

    .line 246663
    iput-object p6, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->g:LX/0Ot;

    .line 246664
    iput-object p9, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->j:LX/1Ns;

    .line 246665
    iput-object p7, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->i:LX/1Np;

    .line 246666
    iput-object p8, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->k:LX/0ad;

    .line 246667
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->b:Landroid/content/Context;

    const-class v2, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    iput-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->n:Landroid/support/v4/app/FragmentActivity;

    .line 246668
    iput-object p10, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->l:LX/0Or;

    .line 246669
    iput-object p11, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->m:LX/0Ot;

    .line 246670
    iput-object p12, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->o:LX/1Rg;

    .line 246671
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->p:LX/0Ot;

    .line 246672
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->q:LX/1E1;

    .line 246673
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->r:LX/0Ot;

    .line 246674
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->s:LX/0Ot;

    .line 246675
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->h:LX/0Ot;

    .line 246676
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->v:LX/0Ot;

    .line 246677
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->t:LX/0oJ;

    .line 246678
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->u:LX/0fO;

    .line 246679
    return-void
.end method

.method private static a(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;ZLX/1Rh;)LX/1bR;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 246636
    iget-object v0, p2, LX/1Rh;->b:LX/1EE;

    invoke-virtual {v0}, LX/1EE;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246637
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->t:LX/0oJ;

    .line 246638
    sget-char v1, LX/0ob;->D:C

    sget-char v2, LX/0ob;->r:C

    const-string v3, "no_prompt_icon"

    const-string v4, "glyph_pile"

    invoke-static {v0, v1, v2, v3, v4}, LX/0oJ;->a(LX/0oJ;CCLjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    move v0, v1

    .line 246639
    if-eqz v0, :cond_1

    sget-object v0, LX/1bR;->DEFAULT:LX/1bR;

    .line 246640
    :cond_0
    :goto_0
    return-object v0

    .line 246641
    :cond_1
    sget-object v0, LX/1bR;->NONE:LX/1bR;

    goto :goto_0

    .line 246642
    :cond_2
    iget-object v0, p2, LX/1Rh;->b:LX/1EE;

    .line 246643
    iget-boolean v2, v0, LX/1EE;->d:Z

    move v0, v2

    .line 246644
    if-nez v0, :cond_3

    .line 246645
    sget-object v0, LX/1bR;->NONE:LX/1bR;

    goto :goto_0

    .line 246646
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->k:LX/0ad;

    sget-short v2, LX/1RY;->e:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 246647
    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->k:LX/0ad;

    sget-short v3, LX/1RY;->v:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 246648
    if-nez v0, :cond_4

    if-nez v2, :cond_4

    .line 246649
    sget-object v0, LX/1bR;->NONE:LX/1bR;

    goto :goto_0

    .line 246650
    :cond_4
    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/24B;

    iget-object v2, p2, LX/1Rh;->b:LX/1EE;

    invoke-virtual {v2}, LX/1EE;->a()LX/1RN;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/24B;->b(LX/1RN;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    .line 246651
    :goto_1
    if-nez v0, :cond_7

    .line 246652
    sget-object v0, LX/1bR;->NONE:LX/1bR;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 246653
    goto :goto_1

    .line 246654
    :cond_7
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->k:LX/0ad;

    sget-wide v2, LX/1RY;->u:D

    const-class v1, LX/1bR;

    sget-object v4, LX/1bR;->DEFAULT:LX/1bR;

    invoke-interface {v0, v2, v3, v1, v4}, LX/0ad;->a(DLjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1bR;

    .line 246655
    if-nez v0, :cond_0

    .line 246656
    sget-object v0, LX/1bR;->NONE:LX/1bR;

    goto :goto_0
.end method

.method private a(LX/1aD;LX/1Rh;LX/1Qi;)LX/1bS;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/1Rh;",
            "TE;)",
            "LX/1bS;"
        }
    .end annotation

    .prologue
    .line 246602
    move-object/from16 v0, p2

    iget-object v6, v0, LX/1Rh;->b:LX/1EE;

    .line 246603
    invoke-virtual {v6}, LX/1EE;->i()Z

    move-result v2

    .line 246604
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->j:LX/1Ns;

    invoke-virtual/range {p3 .. p3}, LX/1Qi;->b()LX/1Qh;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, LX/1Qi;->d()LX/1DQ;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v4

    .line 246605
    move-object/from16 v0, p2

    iget-object v1, v0, LX/1Rh;->c:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_5

    move-object/from16 v0, p2

    iget-object v1, v0, LX/1Rh;->c:Landroid/view/View$OnClickListener;

    move-object v13, v1

    .line 246606
    :goto_0
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 246607
    invoke-virtual {v6}, LX/1EE;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v6}, LX/1EE;->h()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->t:LX/0oJ;

    invoke-virtual {v1}, LX/0oJ;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 246608
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->v:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 246609
    :cond_0
    if-eqz v2, :cond_6

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->k:LX/0ad;

    sget-short v2, LX/1RY;->e:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    move v2, v1

    .line 246610
    :goto_1
    const v3, 0x7f0d111c

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 246611
    const v2, 0x7f0d111e

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    invoke-virtual {v6}, LX/1EE;->x()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 246612
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->l:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1RH;

    check-cast p3, LX/1Pn;

    invoke-interface/range {p3 .. p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, LX/1RH;->a(LX/1EE;Landroid/content/Context;)LX/AkL;

    move-result-object v2

    .line 246613
    move-object/from16 v0, p2

    iget-object v1, v0, LX/1Rh;->b:LX/1EE;

    invoke-virtual {v1}, LX/1EE;->a()LX/1RN;

    move-result-object v1

    .line 246614
    move-object/from16 v0, p2

    iget-boolean v3, v0, LX/1Rh;->a:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->q:LX/1E1;

    invoke-virtual {v3}, LX/1E1;->d()Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v2, :cond_1

    iget-object v1, v1, LX/1RN;->c:LX/32e;

    iget-object v1, v1, LX/32e;->a:LX/24P;

    sget-object v3, LX/24P;->MINIMIZED:LX/24P;

    invoke-virtual {v1, v3}, LX/24P;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_1
    const/4 v1, 0x1

    move v3, v1

    .line 246615
    :goto_2
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    if-eqz v3, :cond_8

    move-object/from16 v0, p2

    iget-boolean v3, v0, LX/1Rh;->f:Z

    invoke-static {v3}, LX/1bP;->b(Z)LX/1bP;

    move-result-object v3

    :goto_3
    move-object/from16 v0, p1

    invoke-interface {v0, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 246616
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->t:LX/0oJ;

    invoke-virtual {v1}, LX/0oJ;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v6}, LX/1EE;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 246617
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v3, LX/Ajv;

    move-object/from16 v0, p2

    invoke-direct {v3, p0, v6, v4, v0}, LX/Ajv;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;LX/1EE;LX/1aw;LX/1Rh;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 246618
    :cond_2
    const/4 v1, 0x0

    .line 246619
    move-object/from16 v0, p2

    iget-boolean v3, v0, LX/1Rh;->a:Z

    if-eqz v3, :cond_b

    invoke-virtual {v6}, LX/1EE;->i()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->u:LX/0fO;

    invoke-virtual {v3}, LX/0fO;->b()Z

    move-result v3

    if-nez v3, :cond_b

    .line 246620
    if-eqz v2, :cond_a

    .line 246621
    const/4 v14, 0x1

    .line 246622
    :goto_4
    if-nez v2, :cond_9

    const/4 v3, 0x0

    .line 246623
    :goto_5
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, LX/1Nt;

    new-instance v1, LX/24D;

    invoke-virtual {v6}, LX/1EE;->a()LX/1RN;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->p:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/24B;

    move-object/from16 v0, p2

    iget-object v8, v0, LX/1Rh;->b:LX/1EE;

    invoke-virtual {v8}, LX/1EE;->a()LX/1RN;

    move-result-object v8

    invoke-virtual {v5, v8}, LX/24B;->b(LX/1RN;)Z

    move-result v5

    invoke-direct/range {v1 .. v6}, LX/24D;-><init>(LX/AkL;LX/AkM;LX/1RN;ZLX/0jW;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v7, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 246624
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->q:LX/1E1;

    invoke-virtual {v1}, LX/1E1;->d()Z

    move-result v1

    if-nez v1, :cond_3

    .line 246625
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->r:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v7, LX/24H;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/1Rh;->b:LX/1EE;

    invoke-virtual {v4}, LX/1EE;->a()LX/1RN;

    move-result-object v10

    iget-object v4, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->p:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/24B;

    move-object/from16 v0, p2

    iget-object v5, v0, LX/1Rh;->b:LX/1EE;

    invoke-virtual {v5}, LX/1EE;->a()LX/1RN;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/24B;->b(LX/1RN;)Z

    move-result v12

    move-object v8, v2

    move-object v9, v3

    move-object v11, v6

    invoke-direct/range {v7 .. v12}, LX/24H;-><init>(LX/AkL;LX/AkM;LX/1RN;LX/0jW;Z)V

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v7}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 246626
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/24B;

    move-object/from16 v0, p2

    iget-object v2, v0, LX/1Rh;->b:LX/1EE;

    invoke-virtual {v2}, LX/1EE;->a()LX/1RN;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v4, v0, LX/1Rh;->d:LX/0jW;

    const-class v5, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    new-instance v6, LX/24I;

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->o:LX/1Rg;

    invoke-direct {v6, v2}, LX/24I;-><init>(LX/1Rg;)V

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, LX/24B;->a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V

    :cond_3
    move v2, v14

    .line 246627
    :goto_6
    move-object/from16 v0, p2

    invoke-static {p0, v2, v0}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->a(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;ZLX/1Rh;)LX/1bR;

    move-result-object v3

    .line 246628
    sget-object v1, LX/1bR;->NONE:LX/1bR;

    if-eq v3, v1, :cond_4

    .line 246629
    const v4, 0x7f0d112b

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 246630
    :cond_4
    new-instance v1, LX/1bS;

    invoke-direct {v1, v13, v2}, LX/1bS;-><init>(Landroid/view/View$OnClickListener;Z)V

    return-object v1

    .line 246631
    :cond_5
    new-instance v1, LX/1bN;

    invoke-direct {v1, p0, v6, v4}, LX/1bN;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;LX/1EE;LX/1aw;)V

    move-object v13, v1

    goto/16 :goto_0

    .line 246632
    :cond_6
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_1

    .line 246633
    :cond_7
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_2

    .line 246634
    :cond_8
    move-object/from16 v0, p2

    iget-boolean v3, v0, LX/1Rh;->f:Z

    invoke-static {v3}, LX/1bP;->a(Z)LX/1bP;

    move-result-object v3

    goto/16 :goto_3

    .line 246635
    :cond_9
    invoke-interface {v2}, LX/AkL;->g()LX/AkM;

    move-result-object v3

    goto/16 :goto_5

    :cond_a
    move v14, v1

    goto/16 :goto_4

    :cond_b
    move v2, v1

    goto :goto_6
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;
    .locals 3

    .prologue
    .line 246594
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    monitor-enter v1

    .line 246595
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->w:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 246596
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->w:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 246597
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246598
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 246599
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246600
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 246601
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;
    .locals 23

    .prologue
    .line 246592
    new-instance v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x660

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x650

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x655

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xe17

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x656

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/1Np;->a(LX/0QB;)LX/1Np;

    move-result-object v9

    check-cast v9, LX/1Np;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    const-class v11, LX/1Ns;

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/1Ns;

    const/16 v12, 0xfd1

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x1c9a

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/1Rg;->a(LX/0QB;)LX/1Rg;

    move-result-object v14

    check-cast v14, LX/1Rg;

    const/16 v15, 0x661

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/1E1;->a(LX/0QB;)LX/1E1;

    move-result-object v16

    check-cast v16, LX/1E1;

    const/16 v17, 0x663

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0xbdd

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x232a

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/0oJ;->a(LX/0QB;)LX/0oJ;

    move-result-object v20

    check-cast v20, LX/0oJ;

    invoke-static/range {p0 .. p0}, LX/0fO;->a(LX/0QB;)LX/0fO;

    move-result-object v21

    check-cast v21, LX/0fO;

    const/16 v22, 0x2328

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-direct/range {v2 .. v22}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1Np;LX/0ad;LX/1Ns;LX/0Or;LX/0Ot;LX/1Rg;LX/0Ot;LX/1E1;LX/0Ot;LX/0Ot;LX/0Ot;LX/0oJ;LX/0fO;LX/0Ot;)V

    .line 246593
    return-object v2
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 246580
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 246591
    check-cast p2, LX/1Rh;

    check-cast p3, LX/1Qi;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->a(LX/1aD;LX/1Rh;LX/1Qi;)LX/1bS;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3e50d890

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 246585
    check-cast p1, LX/1Rh;

    check-cast p2, LX/1bS;

    check-cast p4, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    .line 246586
    iget-object v1, p2, LX/1bS;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->a(Landroid/view/View$OnClickListener;)V

    .line 246587
    iget-object v1, p1, LX/1Rh;->b:LX/1EE;

    .line 246588
    iget-object p1, v1, LX/1EE;->q:Ljava/lang/Integer;

    move-object v1, p1

    .line 246589
    invoke-virtual {p4, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->setHintColor(Ljava/lang/Integer;)V

    .line 246590
    const/16 v1, 0x1f

    const v2, 0x1bed917a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 246584
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 246581
    check-cast p4, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    .line 246582
    invoke-virtual {p4}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->b()V

    .line 246583
    return-void
.end method
