.class public Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1RV;",
        "LX/1aI;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0fO;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 276593
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 276594
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    .line 276595
    invoke-virtual {p2}, LX/0fO;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->b:Z

    .line 276596
    return-void
.end method

.method private a(IIZ)Landroid/graphics/drawable/Drawable;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 276597
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 276598
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    if-eqz p3, :cond_0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 276599
    iget-object v6, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v9, 0x7f020839

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 276600
    new-instance v6, Landroid/graphics/drawable/LayerDrawable;

    const/4 v10, 0x2

    new-array v10, v10, [Landroid/graphics/drawable/Drawable;

    aput-object v9, v10, v8

    aput-object v1, v10, v7

    invoke-direct {v6, v10}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 276601
    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    move v10, v8

    move v11, v8

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    .line 276602
    move-object v1, v6

    .line 276603
    :cond_0
    move v3, v2

    move v4, v2

    move v5, p2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 276604
    iget-boolean v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->b:Z

    if-nez v1, :cond_1

    .line 276605
    :goto_0
    return-object v0

    .line 276606
    :cond_1
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v4, 0x7f0a0168

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v3, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 276607
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    aput-object v3, v4, v2

    const/4 v2, 0x1

    aput-object v0, v4, v2

    invoke-direct {v1, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move-object v0, v1

    .line 276608
    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;
    .locals 5

    .prologue
    .line 276609
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;

    monitor-enter v1

    .line 276610
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 276611
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 276612
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276613
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 276614
    new-instance p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v4

    check-cast v4, LX/0fO;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;-><init>(Landroid/content/res/Resources;LX/0fO;)V

    .line 276615
    move-object v0, p0

    .line 276616
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 276617
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 276618
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 276619
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 276620
    check-cast p2, LX/1RV;

    const/4 v1, 0x0

    .line 276621
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b00d8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 276622
    iget v0, p2, LX/1RV;->a:I

    iget-boolean v3, p2, LX/1RV;->b:Z

    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 276623
    iget-boolean v0, p2, LX/1RV;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 276624
    :goto_0
    iget-boolean v4, p2, LX/1RV;->c:Z

    if-eqz v4, :cond_1

    .line 276625
    :goto_1
    new-instance v4, LX/1aI;

    add-int/2addr v2, v1

    invoke-direct {v4, v3, v0, v1, v2}, LX/1aI;-><init>(Landroid/graphics/drawable/Drawable;III)V

    return-object v4

    .line 276626
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v4, 0x7f0b0112

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    .line 276627
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerBottomBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v4, 0x7f0b0061

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1bb98300

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 276628
    check-cast p2, LX/1aI;

    .line 276629
    iget-object v1, p2, LX/1aI;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 276630
    iget v1, p2, LX/1aI;->b:I

    iget v2, p2, LX/1aI;->c:I

    iget p0, p2, LX/1aI;->b:I

    iget p1, p2, LX/1aI;->d:I

    invoke-virtual {p4, v1, v2, p0, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 276631
    const/16 v1, 0x1f

    const v2, 0x25b57de7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
