.class public Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/1EE;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1E0;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1E0;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1E0;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkHeaderPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 245863
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 245864
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;->a:LX/1E0;

    .line 245865
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;->b:LX/0Ot;

    .line 245866
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;->c:LX/0Ot;

    .line 245867
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;
    .locals 6

    .prologue
    .line 245875
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;

    monitor-enter v1

    .line 245876
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 245877
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 245878
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245879
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 245880
    new-instance v4, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;

    invoke-static {v0}, LX/1E0;->b(LX/0QB;)LX/1E0;

    move-result-object v3

    check-cast v3, LX/1E0;

    const/16 v5, 0x669

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x668

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;-><init>(LX/1E0;LX/0Ot;LX/0Ot;)V

    .line 245881
    move-object v0, v4

    .line 245882
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 245883
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245884
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 245885
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 245869
    check-cast p2, LX/1EE;

    .line 245870
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 245871
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 245872
    iget-object v1, p2, LX/1EE;->e:LX/1EF;

    move-object v1, v1

    .line 245873
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 245874
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 245868
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerWorkPartDefinition;->a:LX/1E0;

    invoke-virtual {v0}, LX/1E0;->b()Z

    move-result v0

    return v0
.end method
