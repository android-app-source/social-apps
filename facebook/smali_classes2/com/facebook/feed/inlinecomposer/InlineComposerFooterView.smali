.class public Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field private b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field private c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field private d:[Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field private e:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 275944
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 275945
    invoke-direct {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a()V

    .line 275946
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 275941
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 275942
    invoke-direct {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a()V

    .line 275943
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 275932
    const v0, 0x7f030634

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 275933
    invoke-virtual {p0, v3}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setOrientation(I)V

    .line 275934
    const v0, 0x7f0d1123

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 275935
    const v0, 0x7f0d1124

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 275936
    const v0, 0x7f0d1125

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 275937
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/fbui/widget/text/ImageWithTextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->d:[Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 275938
    invoke-direct {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->b()V

    .line 275939
    new-instance v1, LX/0zw;

    const v0, 0x7f0d1129

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->e:LX/0zw;

    .line 275940
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 275928
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->d:[Lcom/facebook/fbui/widget/text/ImageWithTextView;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 275929
    const v4, 0x3f4ccccd    # 0.8f

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageScale(F)V

    .line 275930
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275931
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 275924
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 275925
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 275926
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 275927
    return-void
.end method

.method public final a(ILandroid/view/View$OnClickListener;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 275915
    if-nez p1, :cond_1

    .line 275916
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    .line 275917
    const v0, 0x7f0d1127

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 275918
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->f:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p3, :cond_0

    :goto_0
    invoke-virtual {v0, p3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 275919
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 275920
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->invalidate()V

    .line 275921
    return-void

    .line 275922
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081b47

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 275923
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 275911
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 275912
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 275913
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 275914
    return-void
.end method

.method public setButtonTextColor(I)V
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 275947
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->d:[Lcom/facebook/fbui/widget/text/ImageWithTextView;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 275948
    invoke-virtual {v3, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setTextColor(I)V

    .line 275949
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275950
    :cond_0
    return-void
.end method

.method public setCheckinButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 275909
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 275910
    return-void
.end method

.method public setCheckinButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 275907
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 275908
    return-void
.end method

.method public setCheckinButtonVisibility(I)V
    .locals 1

    .prologue
    .line 275905
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 275906
    return-void
.end method

.method public setPhotoButtonDrawable(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 275894
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 275895
    return-void
.end method

.method public setPhotoButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 275903
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 275904
    return-void
.end method

.method public setStatusButtonDrawable(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 275901
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 275902
    return-void
.end method

.method public setStatusButtonDrawablePadding(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 275898
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->getCompoundDrawablePadding()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 275899
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 275900
    :cond_0
    return-void
.end method

.method public setStatusButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 275896
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 275897
    return-void
.end method
