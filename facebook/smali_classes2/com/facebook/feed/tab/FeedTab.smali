.class public final Lcom/facebook/feed/tab/FeedTab;
.super Lcom/facebook/apptab/state/TabTag;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/feed/tab/FeedTab;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/facebook/feed/tab/FeedTab;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108310
    new-instance v0, Lcom/facebook/feed/tab/FeedTab;

    invoke-direct {v0}, Lcom/facebook/feed/tab/FeedTab;-><init>()V

    sput-object v0, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    .line 108311
    new-instance v0, LX/0oQ;

    invoke-direct {v0}, LX/0oQ;-><init>()V

    sput-object v0, Lcom/facebook/feed/tab/FeedTab;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const v6, 0x63000e

    .line 108313
    sget-object v1, LX/0ax;->cL:Ljava/lang/String;

    sget-object v2, LX/0cQ;->NATIVE_NEWS_FEED_SWITCHER_FRAGMENT:LX/0cQ;

    const v3, 0x7f021079

    const/4 v4, 0x1

    const-string v5, "native_newsfeed"

    const v10, 0x7f080a27

    const v11, 0x7f0d0030

    move-object v0, p0

    move v7, v6

    move-object v9, v8

    invoke-direct/range {v0 .. v11}, Lcom/facebook/apptab/state/TabTag;-><init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V

    .line 108314
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108312
    const-string v0, "Feed"

    return-object v0
.end method
