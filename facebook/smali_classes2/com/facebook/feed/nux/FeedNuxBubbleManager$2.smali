.class public final Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1PJ;


# direct methods
.method public constructor <init>(LX/1PJ;)V
    .locals 0

    .prologue
    .line 243940
    iput-object p1, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 243941
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    iget-object v0, v0, LX/1PJ;->q:Ljava/lang/Runnable;

    if-eq v0, p0, :cond_1

    .line 243942
    :cond_0
    :goto_0
    return-void

    .line 243943
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    iget-object v0, v0, LX/1PJ;->p:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243944
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    const/4 v3, 0x0

    .line 243945
    iget-object v1, v0, LX/1PJ;->m:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_5

    .line 243946
    :cond_2
    :goto_1
    move-object v1, v3

    .line 243947
    if-eqz v1, :cond_0

    .line 243948
    iget-object v0, v1, LX/8D5;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 243949
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243950
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    iget-object v0, v0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    if-nez v0, :cond_3

    .line 243951
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    iget-object v2, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    invoke-static {v2, v1}, LX/1PJ;->c(LX/1PJ;LX/8D5;)Lcom/facebook/nux/ui/NuxBubbleView;

    move-result-object v2

    .line 243952
    iput-object v2, v0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    .line 243953
    :goto_2
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    invoke-static {v0, v1}, LX/1PJ;->d(LX/1PJ;LX/8D5;)V

    .line 243954
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    .line 243955
    iput-object v1, v0, LX/1PJ;->o:LX/8D5;

    .line 243956
    goto :goto_0

    .line 243957
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    iget-object v0, v0, LX/1PJ;->o:LX/8D5;

    if-ne v0, v1, :cond_4

    .line 243958
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    iget-object v0, v0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/nux/ui/NuxBubbleView;->setVisibility(I)V

    .line 243959
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    iget-object v0, v0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0}, Lcom/facebook/nux/ui/NuxBubbleView;->b()V

    goto :goto_2

    .line 243960
    :cond_4
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    invoke-static {v0}, LX/1PJ;->c(LX/1PJ;)V

    .line 243961
    iget-object v0, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    iget-object v2, p0, Lcom/facebook/feed/nux/FeedNuxBubbleManager$2;->a:LX/1PJ;

    invoke-static {v2, v1}, LX/1PJ;->c(LX/1PJ;LX/8D5;)Lcom/facebook/nux/ui/NuxBubbleView;

    move-result-object v2

    .line 243962
    iput-object v2, v0, LX/1PJ;->c:Lcom/facebook/nux/ui/NuxBubbleView;

    .line 243963
    goto :goto_2

    .line 243964
    :cond_5
    iget-object v1, v0, LX/1PJ;->n:Ljava/util/WeakHashMap;

    iget-object v2, v0, LX/1PJ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 243965
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 243966
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v4, v3

    :cond_6
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 243967
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 243968
    if-eqz v1, :cond_6

    .line 243969
    iget-object v2, v0, LX/1PJ;->m:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8D5;

    .line 243970
    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-static {v0, v2}, LX/1PJ;->b(LX/1PJ;LX/8D5;)Z

    move-result v6

    if-eqz v6, :cond_9

    iget-object v6, v2, LX/8D5;->d:LX/8D6;

    iget-object v7, v0, LX/1PJ;->l:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v6, v7}, LX/8D6;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v6

    if-eqz v6, :cond_9

    if-eqz v4, :cond_7

    invoke-static {v1}, LX/1PJ;->b(Landroid/view/View;)I

    move-result v6

    invoke-static {v3}, LX/1PJ;->b(Landroid/view/View;)I

    move-result v7

    if-ge v6, v7, :cond_9

    :cond_7
    :goto_4
    move-object v3, v1

    move-object v4, v2

    .line 243971
    goto :goto_3

    :cond_8
    move-object v3, v4

    .line 243972
    goto/16 :goto_1

    :cond_9
    move-object v1, v3

    move-object v2, v4

    goto :goto_4
.end method
