.class public Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field public static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile o:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;


# instance fields
.field public a:LX/1Fn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Eru;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Be;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Eri;",
            ">;"
        }
    .end annotation
.end field

.field public i:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CH7;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;"
        }
    .end annotation
.end field

.field public l:Z

.field public m:LX/DC2;

.field public final n:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 292005
    const-class v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->b:Ljava/lang/String;

    .line 292006
    const-class v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 292025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292026
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 292027
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    .line 292028
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 292029
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->e:LX/0Ot;

    .line 292030
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 292031
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->f:LX/0Ot;

    .line 292032
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 292033
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->g:LX/0Ot;

    .line 292034
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 292035
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->h:LX/0Ot;

    .line 292036
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 292037
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->j:LX/0Ot;

    .line 292038
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 292039
    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->k:LX/0Ot;

    .line 292040
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->l:Z

    .line 292041
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->n:Ljava/util/ArrayDeque;

    .line 292042
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;
    .locals 12

    .prologue
    .line 292010
    sget-object v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->o:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    if-nez v0, :cond_1

    .line 292011
    const-class v1, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    monitor-enter v1

    .line 292012
    :try_start_0
    sget-object v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->o:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 292013
    if-eqz v2, :cond_0

    .line 292014
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 292015
    new-instance v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    invoke-direct {v3}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;-><init>()V

    .line 292016
    const/16 v4, 0x1ca9

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1f4

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x140f

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1644

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1ca7

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const-class v9, Landroid/content/Context;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    const/16 v10, 0x2524

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/1Fn;->b(LX/0QB;)LX/1Fn;

    move-result-object v11

    check-cast v11, LX/1Fn;

    const/16 p0, 0x695

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 292017
    iput-object v4, v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->e:LX/0Ot;

    iput-object v6, v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->f:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->g:LX/0Ot;

    iput-object v8, v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->h:LX/0Ot;

    iput-object v9, v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->i:Landroid/content/Context;

    iput-object v10, v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->j:LX/0Ot;

    iput-object v11, v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a:LX/1Fn;

    iput-object p0, v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->k:LX/0Ot;

    .line 292018
    move-object v0, v3

    .line 292019
    sput-object v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->o:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292020
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 292021
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 292022
    :cond_1
    sget-object v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->o:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    return-object v0

    .line 292023
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 292024
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 2

    .prologue
    .line 292007
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 292008
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const p0, 0x4ed245b

    if-ne v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 292009
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 291850
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    .line 291851
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 291852
    const/4 v1, 0x0

    :goto_0
    move-object v0, v1

    .line 291853
    if-nez v0, :cond_1

    .line 291854
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    invoke-virtual {v0}, LX/Eru;->g()LX/Erj;

    move-result-object v0

    move-object v1, v0

    .line 291855
    :goto_1
    if-eqz v1, :cond_0

    .line 291856
    invoke-virtual {v1}, LX/Erj;->a()V

    .line 291857
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    iget-object v1, v1, LX/Erj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Eru;->c(Ljava/lang/String;)V

    .line 291858
    :cond_0
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_1

    .line 291859
    :pswitch_0
    iget-object v1, v0, LX/Eru;->c:Ljava/util/ArrayDeque;

    invoke-static {v1}, LX/Eru;->a(Ljava/util/ArrayDeque;)LX/Erj;

    move-result-object v1

    goto :goto_0

    .line 291860
    :pswitch_1
    iget-object v1, v0, LX/Eru;->d:Ljava/util/ArrayDeque;

    invoke-static {v1}, LX/Eru;->a(Ljava/util/ArrayDeque;)LX/Erj;

    move-result-object v1

    goto :goto_0

    .line 291861
    :pswitch_2
    iget-object v1, v0, LX/Eru;->e:Ljava/util/ArrayDeque;

    invoke-static {v1}, LX/Eru;->a(Ljava/util/ArrayDeque;)LX/Erj;

    move-result-object v1

    goto :goto_0

    .line 291862
    :pswitch_3
    iget-object v1, v0, LX/Eru;->f:Ljava/util/ArrayDeque;

    invoke-static {v1}, LX/Eru;->a(Ljava/util/ArrayDeque;)LX/Erj;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static declared-synchronized b(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 291995
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/38I;->a(Ljava/lang/Integer;)Ljava/lang/String;

    .line 291996
    iget-boolean v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 291997
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 291998
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    invoke-virtual {v0, p2}, LX/Eru;->a(Ljava/lang/String;)Z

    move-result v0

    move v0, v0

    .line 291999
    if-nez v0, :cond_2

    .line 292000
    invoke-static {p1}, LX/38I;->a(Ljava/lang/Integer;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 292001
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 292002
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->m:LX/DC2;

    invoke-static {p0}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->e(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;)I

    move-result v1

    invoke-interface {v0, v1}, LX/DC2;->a(I)V

    .line 292003
    invoke-static {p0}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->h(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 292004
    invoke-static {p0, p1}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a$redex0(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 291994
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;)I
    .locals 1

    .prologue
    .line 292043
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    invoke-virtual {v0}, LX/Eru;->b()I

    move-result v0

    return v0
.end method

.method public static h(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;)Z
    .locals 3

    .prologue
    const/16 v2, 0x64

    const/4 v1, 0x0

    .line 291983
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    invoke-virtual {v0}, LX/Eru;->c()Z

    move-result v0

    move v0, v0

    .line 291984
    if-nez v0, :cond_1

    .line 291985
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    invoke-virtual {v0}, LX/Eru;->e()Z

    move-result v0

    move v0, v0

    .line 291986
    if-nez v0, :cond_1

    .line 291987
    iput-boolean v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->l:Z

    .line 291988
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    invoke-virtual {v0}, LX/Eru;->b()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 291989
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->m:LX/DC2;

    invoke-interface {v0, v2}, LX/DC2;->a(I)V

    .line 291990
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->m:LX/DC2;

    invoke-interface {v0, v1}, LX/DC2;->b(I)V

    .line 291991
    invoke-static {p0}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->i(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;)V

    .line 291992
    const/4 v0, 0x1

    .line 291993
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static i(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;)V
    .locals 3

    .prologue
    .line 291966
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    const/4 v2, 0x0

    .line 291967
    iget-object v1, v0, LX/Eru;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->clear()V

    .line 291968
    iget-object v1, v0, LX/Eru;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->clear()V

    .line 291969
    iget-object v1, v0, LX/Eru;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->clear()V

    .line 291970
    iget-object v1, v0, LX/Eru;->f:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->clear()V

    .line 291971
    iget-object v1, v0, LX/Eru;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 291972
    iget-object v1, v0, LX/Eru;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 291973
    iput v2, v0, LX/Eru;->i:I

    .line 291974
    iput v2, v0, LX/Eru;->l:I

    .line 291975
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eri;

    .line 291976
    iget-object v1, v0, LX/Eri;->d:LX/FSR;

    .line 291977
    iget-object v0, v1, LX/FSR;->e:LX/FSN;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->c()V

    .line 291978
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->n:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledFuture;

    .line 291979
    invoke-interface {v0}, Ljava/util/concurrent/ScheduledFuture;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledFuture;->isDone()Z

    move-result v2

    if-nez v2, :cond_0

    .line 291980
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    goto :goto_0

    .line 291981
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->n:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    .line 291982
    return-void
.end method


# virtual methods
.method public final a(LX/0oG;)V
    .locals 7

    .prologue
    .line 291958
    invoke-static {}, LX/3T5;->a()[Ljava/lang/Integer;

    move-result-object v2

    .line 291959
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    .line 291960
    aget-object v3, v2, v1

    .line 291961
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "purge_count_"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, LX/38I;->a(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    .line 291962
    iget-object v5, v0, LX/Eru;->j:[I

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, LX/3CW;->a(I)I

    move-result v6

    aget v5, v5, v6

    move v0, v5

    .line 291963
    invoke-virtual {p1, v4, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 291964
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 291965
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "onMediaStarted"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    .line 291953
    iget-boolean v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    invoke-virtual {v0, p2, p3, p1}, LX/Eru;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 291954
    :cond_0
    :goto_0
    return-void

    .line 291955
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;

    invoke-direct {v2, p0, p3}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$1;-><init>(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a:LX/1Fn;

    invoke-virtual {v3}, LX/1Fn;->d()I

    move-result v3

    int-to-long v3, v3

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    .line 291956
    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->n:Ljava/util/ArrayDeque;

    invoke-virtual {v2, v1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 291957
    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/util/List;LX/DC2;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;",
            "LX/DC2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 291870
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->l:Z

    if-eqz v0, :cond_0

    .line 291871
    new-instance v0, LX/DC6;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DC6;-><init>(Ljava/lang/Integer;)V

    move-object v0, v0

    .line 291872
    invoke-interface {p2, v0}, LX/DC2;->a(LX/DC6;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291873
    :goto_0
    monitor-exit p0

    return-void

    .line 291874
    :cond_0
    if-eqz p1, :cond_1

    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 291875
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p2, v0}, LX/DC2;->b(I)V

    .line 291876
    invoke-static {p0}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->i(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 291877
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 291878
    :cond_2
    :try_start_2
    iput-object p2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->m:LX/DC2;

    .line 291879
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->l:Z

    .line 291880
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_4

    .line 291881
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 291882
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 291883
    iget-object v2, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object v2, v2

    .line 291884
    if-nez v2, :cond_8

    .line 291885
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 291886
    :cond_4
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eru;

    const/4 v2, 0x0

    .line 291887
    iget-object v1, v0, LX/Eru;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I

    move-result v1

    iget-object v3, v0, LX/Eru;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->size()I

    move-result v3

    add-int/2addr v1, v3

    iget-object v3, v0, LX/Eru;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->size()I

    move-result v3

    add-int/2addr v1, v3

    iget-object v3, v0, LX/Eru;->f:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->size()I

    move-result v3

    add-int/2addr v1, v3

    move v1, v1

    .line 291888
    iput v1, v0, LX/Eru;->i:I

    move v1, v2

    .line 291889
    :goto_2
    iget-object v3, v0, LX/Eru;->j:[I

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 291890
    iget-object v3, v0, LX/Eru;->j:[I

    aput v2, v3, v1

    .line 291891
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 291892
    :cond_5
    iput v2, v0, LX/Eru;->l:I

    .line 291893
    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a:LX/1Fn;

    .line 291894
    iget-object v5, v2, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0W3;

    sget-wide v7, LX/0X5;->ed:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    move v2, v5

    .line 291895
    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a:LX/1Fn;

    .line 291896
    iget-object v5, v2, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0W3;

    sget-wide v7, LX/0X5;->eb:J

    const/16 v6, 0xc8

    invoke-interface {v5, v7, v8, v6}, LX/0W4;->a(JI)I

    move-result v5

    move v2, v5

    .line 291897
    move v3, v2

    .line 291898
    :goto_3
    const/4 v2, 0x0

    move v4, v2

    :goto_4
    if-ge v4, v3, :cond_7

    .line 291899
    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Eru;

    invoke-virtual {v2}, LX/Eru;->c()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 291900
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 291901
    rem-int/lit8 v2, v4, 0x4

    if-nez v2, :cond_11

    .line 291902
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 291903
    :goto_5
    move-object v2, v2

    .line 291904
    invoke-static {p0, v2}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a$redex0(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;)V

    .line 291905
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_4

    .line 291906
    :cond_6
    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Eru;

    .line 291907
    iget-object v3, v2, LX/Eru;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->size()I

    move-result v3

    iget-object v4, v2, LX/Eru;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->size()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, v2, LX/Eru;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->size()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, v2, LX/Eru;->f:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->size()I

    move-result v4

    add-int/2addr v3, v4

    move v2, v3

    .line 291908
    move v3, v2

    goto :goto_3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 291909
    :cond_7
    goto/16 :goto_0

    .line 291910
    :cond_8
    :try_start_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 291911
    instance-of v3, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_3

    .line 291912
    new-instance v4, Ljava/util/ArrayDeque;

    invoke-direct {v4}, Ljava/util/ArrayDeque;-><init>()V

    .line 291913
    new-instance v5, Ljava/util/ArrayDeque;

    invoke-direct {v5}, Ljava/util/ArrayDeque;-><init>()V

    .line 291914
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4, v2}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 291915
    :cond_9
    :goto_6
    invoke-virtual {v4}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 291916
    invoke-virtual {v4}, Ljava/util/ArrayDeque;->removeLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 291917
    invoke-static {v2}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayDeque;->addAll(Ljava/util/Collection;)Z

    .line 291918
    const/4 v3, 0x0

    .line 291919
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayDeque;->addAll(Ljava/util/Collection;)Z

    .line 291920
    :goto_7
    invoke-virtual {v5}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    .line 291921
    invoke-virtual {v5}, Ljava/util/ArrayDeque;->removeLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 291922
    if-eqz v2, :cond_e

    .line 291923
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayDeque;->addAll(Ljava/util/Collection;)Z

    .line 291924
    invoke-static {v2}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 291925
    iget-object v6, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Eru;

    new-instance v7, LX/Ern;

    .line 291926
    iget-object p2, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object p2, p2

    .line 291927
    invoke-direct {v7, p0, v2, p2}, LX/Ern;-><init>(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v6, v7, p2}, LX/Eru;->a(LX/Erj;Ljava/lang/Integer;)V

    .line 291928
    :cond_a
    if-eqz v2, :cond_f

    invoke-static {v2}, LX/1VO;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v6

    if-eqz v6, :cond_f

    invoke-static {v2}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v6

    if-nez v6, :cond_f

    const/4 v6, 0x1

    :goto_8
    move v6, v6

    .line 291929
    if-eqz v6, :cond_b

    .line 291930
    iget-object v6, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Eru;

    new-instance v7, LX/Erm;

    .line 291931
    iget-object p2, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object p2, p2

    .line 291932
    invoke-direct {v7, p0, v2, p2, v0}, LX/Erm;-><init>(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;Lcom/facebook/feed/model/ClientFeedUnitEdge;)V

    const/4 p2, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v6, v7, p2}, LX/Eru;->a(LX/Erj;Ljava/lang/Integer;)V

    .line 291933
    :cond_b
    invoke-static {v2}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 291934
    iget-object v6, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Eru;

    new-instance v7, LX/Erl;

    invoke-direct {v7, p0, v2, v0}, LX/Erl;-><init>(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/model/ClientFeedUnitEdge;)V

    const/4 p2, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v6, v7, p2}, LX/Eru;->a(LX/Erj;Ljava/lang/Integer;)V

    .line 291935
    const/4 v6, 0x1

    .line 291936
    :goto_9
    move v2, v6

    .line 291937
    or-int/2addr v2, v3

    :goto_a
    move v3, v2

    .line 291938
    goto :goto_7

    .line 291939
    :cond_c
    if-nez v3, :cond_9

    .line 291940
    const/4 v2, 0x1

    move v2, v2

    .line 291941
    if-eqz v2, :cond_d

    .line 291942
    iget-object v2, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Eru;

    new-instance v6, LX/Erk;

    .line 291943
    iget-object v3, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object v7, v3

    .line 291944
    iget-object v3, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Eri;

    invoke-direct {v6, p0, v0, v7, v3}, LX/Erk;-><init>(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Lcom/facebook/feed/model/ClientFeedUnitEdge;Ljava/lang/String;LX/Eri;)V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LX/Eru;->a(LX/Erj;Ljava/lang/Integer;)V

    .line 291945
    :cond_d
    goto/16 :goto_6

    :cond_e
    move v2, v3

    goto :goto_a

    :cond_f
    const/4 v6, 0x0

    goto :goto_8

    :cond_10
    const/4 v6, 0x0

    goto :goto_9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 291946
    :cond_11
    rem-int/lit8 v2, v4, 0x4

    if-ne v2, v6, :cond_12

    .line 291947
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_5

    .line 291948
    :cond_12
    rem-int/lit8 v2, v4, 0x4

    if-ne v2, v7, :cond_13

    .line 291949
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_5

    .line 291950
    :cond_13
    rem-int/lit8 v2, v4, 0x4

    if-ne v2, v8, :cond_14

    .line 291951
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_5

    .line 291952
    :cond_14
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_5
.end method

.method public final declared-synchronized a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 291863
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->l:Z

    .line 291864
    invoke-static {p0}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->i(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;)V

    .line 291865
    iget-object v1, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->m:LX/DC2;

    if-eqz v1, :cond_0

    .line 291866
    iget-object v0, p0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->m:LX/DC2;

    invoke-interface {v0}, LX/DC2;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291867
    const/4 v0, 0x1

    .line 291868
    :cond_0
    monitor-exit p0

    return v0

    .line 291869
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
