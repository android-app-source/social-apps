.class public Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/1Vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Vs",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1V0;

.field private final g:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 266571
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Vs;LX/1V0;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266566
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 266567
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->e:LX/1Vs;

    .line 266568
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->f:LX/1V0;

    .line 266569
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->g:LX/0ad;

    .line 266570
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 266547
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->e:LX/1Vs;

    const/4 v1, 0x0

    .line 266548
    new-instance v2, LX/Bse;

    invoke-direct {v2, v0}, LX/Bse;-><init>(LX/1Vs;)V

    .line 266549
    iget-object v3, v0, LX/1Vs;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bsd;

    .line 266550
    if-nez v3, :cond_0

    .line 266551
    new-instance v3, LX/Bsd;

    invoke-direct {v3, v0}, LX/Bsd;-><init>(LX/1Vs;)V

    .line 266552
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Bsd;->a$redex0(LX/Bsd;LX/1De;IILX/Bse;)V

    .line 266553
    move-object v2, v3

    .line 266554
    move-object v1, v2

    .line 266555
    move-object v0, v1

    .line 266556
    iget-object v1, v0, LX/Bsd;->a:LX/Bse;

    iput-object p2, v1, LX/Bse;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266557
    iget-object v1, v0, LX/Bsd;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266558
    move-object v1, v0

    .line 266559
    move-object v0, p3

    check-cast v0, LX/1Pr;

    .line 266560
    iget-object v2, v1, LX/Bsd;->a:LX/Bse;

    iput-object v0, v2, LX/Bse;->b:LX/1Pr;

    .line 266561
    iget-object v2, v1, LX/Bsd;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 266562
    move-object v0, v1

    .line 266563
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 266564
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 266565
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;
    .locals 7

    .prologue
    .line 266536
    const-class v1, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    monitor-enter v1

    .line 266537
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266538
    sput-object v2, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266539
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266540
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266541
    new-instance p0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1Vs;->a(LX/0QB;)LX/1Vs;

    move-result-object v4

    check-cast v4, LX/1Vs;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;-><init>(Landroid/content/Context;LX/1Vs;LX/1V0;LX/0ad;)V

    .line 266542
    move-object v0, p0

    .line 266543
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266544
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266545
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266546
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 266572
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 266535
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 266532
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 266533
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266534
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/14w;->r(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->g:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-short v3, LX/0wi;->h:S

    invoke-interface {v0, v2, v3, v1}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 266530
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266531
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 266529
    sget-object v0, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
