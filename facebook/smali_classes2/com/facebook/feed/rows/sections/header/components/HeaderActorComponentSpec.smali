.class public Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field public final c:LX/1xa;

.field public final d:LX/1nu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1nu",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/1xZ;

.field public final f:LX/0ad;

.field public final g:LX/1Bv;

.field public final h:LX/1BM;

.field public final i:LX/0pJ;

.field public j:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 337915
    const-class v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 337916
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 337917
    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->b:Landroid/util/SparseArray;

    const v1, 0x7f0d008f

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 337918
    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1xZ;LX/1xa;LX/0ad;LX/1Bv;LX/1BM;LX/0pJ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 337905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 337906
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->j:LX/03R;

    .line 337907
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->d:LX/1nu;

    .line 337908
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->e:LX/1xZ;

    .line 337909
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->c:LX/1xa;

    .line 337910
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->f:LX/0ad;

    .line 337911
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->g:LX/1Bv;

    .line 337912
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->h:LX/1BM;

    .line 337913
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->i:LX/0pJ;

    .line 337914
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;
    .locals 11

    .prologue
    .line 337894
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;

    monitor-enter v1

    .line 337895
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 337896
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 337897
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337898
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 337899
    new-instance v3, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1xZ;->a(LX/0QB;)LX/1xZ;

    move-result-object v5

    check-cast v5, LX/1xZ;

    invoke-static {v0}, LX/1xa;->a(LX/0QB;)LX/1xa;

    move-result-object v6

    check-cast v6, LX/1xa;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v8

    check-cast v8, LX/1Bv;

    invoke-static {v0}, LX/1BM;->a(LX/0QB;)LX/1BM;

    move-result-object v9

    check-cast v9, LX/1BM;

    invoke-static {v0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v10

    check-cast v10, LX/0pJ;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;-><init>(LX/1nu;LX/1xZ;LX/1xa;LX/0ad;LX/1Bv;LX/1BM;LX/0pJ;)V

    .line 337900
    move-object v0, v3

    .line 337901
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 337902
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 337903
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 337904
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
