.class public Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/1xQ;",
            ">;"
        }
    .end annotation
.end field

.field private static n:LX/0Xm;


# instance fields
.field private final e:LX/1VD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1VD",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1VE;

.field private final g:LX/1V0;

.field private final h:LX/1VH;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition",
            "<",
            "LX/1xQ;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:LX/0ad;

.field private final k:LX/1VI;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 257512
    new-instance v0, LX/1VC;

    invoke-direct {v0}, LX/1VC;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1VD;LX/1VE;LX/1V0;LX/1VH;LX/0ad;LX/0Ot;LX/1VI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1VD;",
            "LX/1VE;",
            "LX/1V0;",
            "LX/1VH;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;",
            ">;",
            "LX/1VI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 257503
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 257504
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->e:LX/1VD;

    .line 257505
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->f:LX/1VE;

    .line 257506
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->g:LX/1V0;

    .line 257507
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->h:LX/1VH;

    .line 257508
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->j:LX/0ad;

    .line 257509
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->i:LX/0Ot;

    .line 257510
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->k:LX/1VI;

    .line 257511
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;
    .locals 12

    .prologue
    .line 257492
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    monitor-enter v1

    .line 257493
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 257494
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 257495
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257496
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 257497
    new-instance v3, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1VD;->a(LX/0QB;)LX/1VD;

    move-result-object v5

    check-cast v5, LX/1VD;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v6

    check-cast v6, LX/1VE;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/1VH;->a(LX/0QB;)LX/1VH;

    move-result-object v8

    check-cast v8, LX/1VH;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    const/16 v10, 0x904

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v11

    check-cast v11, LX/1VI;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1VD;LX/1VE;LX/1V0;LX/1VH;LX/0ad;LX/0Ot;LX/1VI;)V

    .line 257498
    move-object v0, v3

    .line 257499
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 257500
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257501
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 257502
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 257487
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257488
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 257489
    invoke-static {p0}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 257490
    invoke-static {v0}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, LX/1WP;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1WQ;

    move-result-object v1

    invoke-static {v1}, LX/14w;->a(LX/1WQ;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, LX/1VF;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 257491
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static e(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 257486
    invoke-static {p0}, LX/1VF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 257483
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->l:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 257484
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->j:LX/0ad;

    sget-short v1, LX/1Dd;->l:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->l:Ljava/lang/Boolean;

    .line 257485
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 257472
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->e:LX/1VD;

    invoke-virtual {v0, p1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object v3

    .line 257473
    invoke-static {p2}, LX/1VF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 257474
    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, LX/1X4;->c(Z)LX/1X4;

    move-result-object v3

    invoke-static {p2}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, LX/1X4;->f(Z)LX/1X4;

    move-result-object v3

    invoke-static {p2}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, LX/1X4;->g(Z)LX/1X4;

    move-result-object v0

    invoke-static {p2}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-nez v3, :cond_5

    :goto_4
    invoke-virtual {v0, v1}, LX/1X4;->d(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object v0

    .line 257475
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->f:LX/1VE;

    invoke-virtual {v1, p2}, LX/1VE;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257476
    const v1, 0x7f0b1076

    invoke-virtual {v0, v1}, LX/1X4;->h(I)LX/1X4;

    move-result-object v1

    const v2, 0x7f0b00f2

    invoke-virtual {v1, v2}, LX/1X4;->m(I)LX/1X4;

    move-result-object v1

    const v2, 0x7f0b00f3

    invoke-virtual {v1, v2}, LX/1X4;->j(I)LX/1X4;

    .line 257477
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->k:LX/1VI;

    invoke-virtual {v1}, LX/1VI;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 257478
    const v1, 0x7f0e0742

    invoke-virtual {v0, v1}, LX/1X4;->n(I)LX/1X4;

    .line 257479
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->f:LX/1VE;

    invoke-virtual {v1, p2}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v1

    .line 257480
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 257481
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->h:LX/1VH;

    invoke-virtual {v1, p1}, LX/1VH;->c(LX/1De;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1XD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XD;

    move-result-object v1

    sget-object v2, LX/1EO;->NEWSFEED:LX/1EO;

    invoke-virtual {v1, v2}, LX/1XD;->a(LX/1EO;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XD;->a(LX/1X1;)LX/1XD;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    .line 257482
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 257451
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 257471
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 257465
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 257466
    invoke-super {p0, p1, p2}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;)V

    .line 257467
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    .line 257468
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 257469
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 257470
    return-void
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 257464
    invoke-direct {p0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->g()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 257463
    invoke-direct {p0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->g()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 257461
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 257462
    invoke-static {p1}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 257459
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 257460
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 257458
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method

.method public final iV_()Z
    .locals 3

    .prologue
    .line 257452
    invoke-super {p0}, Lcom/facebook/components/feed/ComponentPartDefinition;->iV_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257453
    const/4 v0, 0x1

    .line 257454
    :goto_0
    return v0

    .line 257455
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->m:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 257456
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->j:LX/0ad;

    sget-short v1, LX/1Dd;->o:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->m:Ljava/lang/Boolean;

    .line 257457
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method
