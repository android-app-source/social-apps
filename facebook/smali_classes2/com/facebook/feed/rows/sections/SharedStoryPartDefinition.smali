.class public Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static p:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

.field private final c:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

.field private final g:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

.field private final h:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final i:Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

.field private final j:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

.field private final k:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

.field private final l:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

.field private final n:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

.field private final o:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;LX/0Ot;LX/0Ot;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;",
            "Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;",
            "Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;",
            "Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;",
            ">;",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;",
            "Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;",
            "Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265062
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 265063
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    .line 265064
    iput-object p12, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    .line 265065
    iput-object p11, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    .line 265066
    iput-object p9, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->d:LX/0Ot;

    .line 265067
    iput-object p10, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->e:LX/0Ot;

    .line 265068
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 265069
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->g:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    .line 265070
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->h:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 265071
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->i:Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

    .line 265072
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->j:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 265073
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->k:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 265074
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->l:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    .line 265075
    iput-object p13, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->m:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    .line 265076
    iput-object p14, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->n:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    .line 265077
    iput-object p15, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->o:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    .line 265078
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;
    .locals 3

    .prologue
    .line 265079
    const-class v1, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;

    monitor-enter v1

    .line 265080
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265081
    sput-object v2, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265082
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265083
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265084
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265085
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265086
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 265087
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265088
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265089
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;
    .locals 17

    .prologue
    .line 265090
    new-instance v1, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    const/16 v10, 0x8f5

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x8f7

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    move-result-object v15

    check-cast v15, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    move-result-object v16

    check-cast v16, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    invoke-direct/range {v1 .. v16}, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;-><init>(Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;LX/0Ot;LX/0Ot;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;)V

    .line 265091
    return-object v1
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 265092
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 265093
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265094
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265095
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->l:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    new-instance v2, LX/1W0;

    invoke-direct {v2, v0, p3}, LX/1W0;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/1Pr;)V

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265096
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->k:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265097
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->j:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265098
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->o:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265099
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->i:Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265100
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->h:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265101
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->g:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265102
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265103
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    sget-object v2, LX/1Qt;->FEED:LX/1Qt;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->e:LX/0Ot;

    invoke-static {p1, v1, v2, p2}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->d:LX/0Ot;

    invoke-virtual {v1, v2, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 265104
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->m:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265105
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265106
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265107
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->n:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265108
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265109
    const/4 v0, 0x0

    return-object v0

    .line 265110
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 265111
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265112
    invoke-static {p1}, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
