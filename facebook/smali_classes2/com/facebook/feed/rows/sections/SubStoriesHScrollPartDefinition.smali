.class public Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:LX/2dq;

.field private final f:LX/99j;

.field private final g:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final h:LX/1VF;

.field public final i:LX/1Qx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;LX/2dq;LX/99j;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1VF;LX/1Qx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268256
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 268257
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->a:Landroid/content/Context;

    .line 268258
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->b:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;

    .line 268259
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->c:Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;

    .line 268260
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 268261
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->e:LX/2dq;

    .line 268262
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->f:LX/99j;

    .line 268263
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->g:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 268264
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->h:LX/1VF;

    .line 268265
    iput-object p9, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->i:LX/1Qx;

    .line 268266
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;
    .locals 13

    .prologue
    .line 268267
    const-class v1, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    monitor-enter v1

    .line 268268
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268269
    sput-object v2, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268270
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268271
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268272
    new-instance v3, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v8

    check-cast v8, LX/2dq;

    invoke-static {v0}, LX/99j;->a(LX/0QB;)LX/99j;

    move-result-object v9

    check-cast v9, LX/99j;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v11

    check-cast v11, LX/1VF;

    invoke-static {v0}, LX/1Qu;->a(LX/0QB;)LX/1Qx;

    move-result-object v12

    check-cast v12, LX/1Qx;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;LX/2dq;LX/99j;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1VF;LX/1Qx;)V

    .line 268273
    move-object v0, v3

    .line 268274
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268275
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268276
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268277
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 268278
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268279
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1WP;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1WQ;

    move-result-object v0

    invoke-static {v0}, LX/14w;->a(LX/1WQ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268280
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268281
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1VF;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 268282
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 268283
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268284
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 268285
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268286
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/2eF;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 268287
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->e:LX/2dq;

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->f:LX/99j;

    invoke-virtual {v1}, LX/99j;->b()F

    move-result v1

    const/high16 v2, 0x41000000    # 8.0f

    add-float/2addr v1, v2

    sget-object v2, LX/2eF;->a:LX/1Ua;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    .line 268288
    iget-object v6, p0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->g:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->I_()I

    move-result v2

    .line 268289
    iget-object v3, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 268290
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268291
    invoke-static {v3}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v4

    .line 268292
    new-instance p3, LX/Br4;

    invoke-direct {p3, p0, p2, v4, v3}, LX/Br4;-><init>(Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v3, p3

    .line 268293
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 268294
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 268295
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268296
    invoke-static {p1}, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
