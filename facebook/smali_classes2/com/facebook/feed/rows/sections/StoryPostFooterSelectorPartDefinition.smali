.class public Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/1VF;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/1VF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;",
            ">;",
            "LX/1VF;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265771
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 265772
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->a:LX/1VF;

    .line 265773
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->c:LX/0Ot;

    .line 265774
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->b:LX/0Ot;

    .line 265775
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->d:LX/0Ot;

    .line 265776
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;
    .locals 7

    .prologue
    .line 265777
    const-class v1, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    monitor-enter v1

    .line 265778
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265779
    sput-object v2, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265780
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265781
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 265782
    new-instance v4, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    const/16 v3, 0x6eb

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x6ed

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x1d07

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v3

    check-cast v3, LX/1VF;

    invoke-direct {v4, v5, v6, p0, v3}, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/1VF;)V

    .line 265783
    move-object v0, v4

    .line 265784
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265785
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265786
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265787
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 265788
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265789
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->d:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->c:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 265790
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265791
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 265792
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265793
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->a:LX/1VF;

    .line 265794
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265795
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/1VF;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
