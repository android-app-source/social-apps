.class public Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static p:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265796
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 265797
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->a:LX/0Ot;

    .line 265798
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->b:LX/0Ot;

    .line 265799
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->c:LX/0Ot;

    .line 265800
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->d:LX/0Ot;

    .line 265801
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->e:LX/0Ot;

    .line 265802
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->f:LX/0Ot;

    .line 265803
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->g:LX/0Ot;

    .line 265804
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->h:LX/0Ot;

    .line 265805
    iput-object p9, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->i:LX/0Ot;

    .line 265806
    iput-object p10, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->j:LX/0Ot;

    .line 265807
    iput-object p11, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->k:LX/0Ot;

    .line 265808
    iput-object p12, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->l:LX/0Ot;

    .line 265809
    iput-object p13, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->m:LX/0Ot;

    .line 265810
    iput-object p14, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->n:LX/0Ot;

    .line 265811
    iput-object p15, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->o:LX/0Ot;

    .line 265812
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;
    .locals 3

    .prologue
    .line 265813
    const-class v1, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    monitor-enter v1

    .line 265814
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265815
    sput-object v2, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265816
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265817
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265818
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265819
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265820
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;
    .locals 18

    .prologue
    .line 265821
    new-instance v2, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    const/16 v3, 0x9bb

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x9b8

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x9b4

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x9b7

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x8ab

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x831

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x977

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x967

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x9b9

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x889

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x9bf

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x8c4

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x7e9

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x8e1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0xa20

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 265822
    return-object v2
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 265823
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 265824
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265825
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265826
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-static {p1, v1, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    .line 265827
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    sget-object v3, LX/1Qt;->PERMALINK:LX/1Qt;

    if-ne v2, v3, :cond_0

    .line 265828
    invoke-static {v0}, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 265829
    const/4 v2, 0x0

    .line 265830
    :goto_0
    move v2, v2

    .line 265831
    if-nez v2, :cond_1

    .line 265832
    :cond_0
    invoke-static {v0}, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 265833
    const/4 v2, 0x0

    .line 265834
    :goto_1
    move v0, v2

    .line 265835
    if-eqz v0, :cond_2

    .line 265836
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 265837
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 265838
    const/4 v0, 0x0

    return-object v0

    :cond_3
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-static {v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aP()Z

    move-result v2

    goto :goto_0

    :cond_4
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-static {v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aO()Z

    move-result v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 265839
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x1

    .line 265840
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265841
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265842
    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0, v1}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
