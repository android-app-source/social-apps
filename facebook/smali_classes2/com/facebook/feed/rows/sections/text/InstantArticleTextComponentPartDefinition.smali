.class public Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;

.field private final e:LX/1W4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1W4",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1V0;

.field private final g:LX/1VF;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;LX/1W4;LX/1V0;LX/1VF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267270
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 267271
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;->d:Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;

    .line 267272
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;->e:LX/1W4;

    .line 267273
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;->f:LX/1V0;

    .line 267274
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;->g:LX/1VF;

    .line 267275
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 267232
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;->e:LX/1W4;

    const/4 v1, 0x0

    .line 267233
    new-instance v2, LX/BtA;

    invoke-direct {v2, v0}, LX/BtA;-><init>(LX/1W4;)V

    .line 267234
    iget-object v3, v0, LX/1W4;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bt9;

    .line 267235
    if-nez v3, :cond_0

    .line 267236
    new-instance v3, LX/Bt9;

    invoke-direct {v3, v0}, LX/Bt9;-><init>(LX/1W4;)V

    .line 267237
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Bt9;->a$redex0(LX/Bt9;LX/1De;IILX/BtA;)V

    .line 267238
    move-object v2, v3

    .line 267239
    move-object v1, v2

    .line 267240
    move-object v0, v1

    .line 267241
    iget-object v1, v0, LX/Bt9;->a:LX/BtA;

    iput-object p2, v1, LX/BtA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267242
    iget-object v1, v0, LX/Bt9;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 267243
    move-object v0, v0

    .line 267244
    iget-object v1, v0, LX/Bt9;->a:LX/BtA;

    iput-object p3, v1, LX/BtA;->b:LX/1Pn;

    .line 267245
    iget-object v1, v0, LX/Bt9;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 267246
    move-object v0, v0

    .line 267247
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 267248
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;
    .locals 9

    .prologue
    .line 267259
    const-class v1, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;

    monitor-enter v1

    .line 267260
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267261
    sput-object v2, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267262
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267263
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267264
    new-instance v3, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;

    invoke-static {v0}, LX/1W4;->a(LX/0QB;)LX/1W4;

    move-result-object v6

    check-cast v6, LX/1W4;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v8

    check-cast v8, LX/1VF;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;LX/1W4;LX/1V0;LX/1VF;)V

    .line 267265
    move-object v0, v3

    .line 267266
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267267
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267268
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267269
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 267258
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 267276
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 267253
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267254
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267255
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 267256
    invoke-static {v0}, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 267257
    if-eqz v1, :cond_0

    invoke-static {v0}, LX/1VF;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 267250
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267251
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267252
    check-cast v0, LX/0jW;

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 267249
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
