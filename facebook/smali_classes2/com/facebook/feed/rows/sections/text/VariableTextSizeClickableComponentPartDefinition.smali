.class public Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final d:LX/1VF;

.field private final e:LX/1V0;

.field private final f:LX/1VH;

.field private final g:LX/1W8;

.field private final h:LX/1WB;

.field private final i:LX/1WC;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1VF;LX/1V0;LX/1VH;LX/1W8;LX/1WB;LX/1WC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267499
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 267500
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->d:LX/1VF;

    .line 267501
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->e:LX/1V0;

    .line 267502
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->f:LX/1VH;

    .line 267503
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->g:LX/1W8;

    .line 267504
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->h:LX/1WB;

    .line 267505
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->i:LX/1WC;

    .line 267506
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 267507
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->h:LX/1WB;

    .line 267508
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267509
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/1WB;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1z6;

    move-result-object v0

    .line 267510
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->g:LX/1W8;

    invoke-virtual {v1, p1, p3, p2}, LX/1W8;->a(LX/1De;LX/1Pn;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v1

    .line 267511
    new-instance v2, LX/1X6;

    iget v3, v0, LX/1z6;->d:I

    iget v0, v0, LX/1z6;->c:I

    const/4 v6, -0x1

    .line 267512
    if-ne v3, v6, :cond_0

    if-ne v0, v6, :cond_0

    .line 267513
    sget-object v4, LX/1Ua;->d:LX/1Ua;

    .line 267514
    :goto_0
    move-object v0, v4

    .line 267515
    invoke-direct {v2, p2, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 267516
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v0, p1, p3, v2, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 267517
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->f:LX/1VH;

    invoke-virtual {v1, p1}, LX/1VH;->c(LX/1De;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1XD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XD;

    move-result-object v1

    sget-object v2, LX/1EO;->NEWSFEED:LX/1EO;

    invoke-virtual {v1, v2}, LX/1XD;->a(LX/1EO;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XD;->a(LX/1X1;)LX/1XD;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 267518
    :cond_0
    if-ne v3, v6, :cond_1

    sget-object v4, LX/1Ua;->d:LX/1Ua;

    .line 267519
    iget-object v5, v4, LX/1Ua;->t:LX/1Ub;

    move-object v4, v5

    .line 267520
    iget v5, v4, LX/1Ub;->b:F

    move v4, v5

    .line 267521
    move v5, v4

    .line 267522
    :goto_1
    if-ne v0, v6, :cond_2

    sget-object v4, LX/1Ua;->d:LX/1Ua;

    .line 267523
    iget-object v6, v4, LX/1Ua;->t:LX/1Ub;

    move-object v4, v6

    .line 267524
    iget v6, v4, LX/1Ub;->a:F

    move v4, v6

    .line 267525
    :goto_2
    invoke-static {}, LX/1UY;->b()LX/1UY;

    move-result-object v6

    .line 267526
    iput v5, v6, LX/1UY;->c:F

    .line 267527
    move-object v5, v6

    .line 267528
    iput v4, v5, LX/1UY;->b:F

    .line 267529
    move-object v4, v5

    .line 267530
    invoke-virtual {v4}, LX/1UY;->i()LX/1Ua;

    move-result-object v4

    goto :goto_0

    .line 267531
    :cond_1
    int-to-float v4, v3

    move v5, v4

    goto :goto_1

    .line 267532
    :cond_2
    int-to-float v4, v0

    goto :goto_2
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;
    .locals 11

    .prologue
    .line 267533
    const-class v1, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;

    monitor-enter v1

    .line 267534
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267535
    sput-object v2, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267536
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267537
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267538
    new-instance v3, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v5

    check-cast v5, LX/1VF;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/1VH;->a(LX/0QB;)LX/1VH;

    move-result-object v7

    check-cast v7, LX/1VH;

    invoke-static {v0}, LX/1W8;->a(LX/0QB;)LX/1W8;

    move-result-object v8

    check-cast v8, LX/1W8;

    invoke-static {v0}, LX/1WB;->a(LX/0QB;)LX/1WB;

    move-result-object v9

    check-cast v9, LX/1WB;

    invoke-static {v0}, LX/1WC;->a(LX/0QB;)LX/1WC;

    move-result-object v10

    check-cast v10, LX/1WC;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;-><init>(Landroid/content/Context;LX/1VF;LX/1V0;LX/1VH;LX/1W8;LX/1WB;LX/1WC;)V

    .line 267539
    move-object v0, v3

    .line 267540
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267541
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267542
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267543
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 267544
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 267545
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 267546
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267547
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267548
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 267549
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;->i:LX/1WC;

    invoke-virtual {v1, v0}, LX/1WC;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/1VF;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 267550
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267551
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 267552
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
