.class public Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/350;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Bt8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/Bt8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267277
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 267278
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;->a:LX/0Or;

    .line 267279
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;
    .locals 4

    .prologue
    .line 267280
    const-class v1, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;

    monitor-enter v1

    .line 267281
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267282
    sput-object v2, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267283
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267284
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267285
    new-instance v3, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;

    const/16 p0, 0x1d44

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;-><init>(LX/0Or;)V

    .line 267286
    move-object v0, v3

    .line 267287
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267288
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267289
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267290
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 267291
    invoke-static {p0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 267292
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    move-object v3, v0

    .line 267293
    :goto_0
    if-nez v3, :cond_1

    move-object v0, v1

    .line 267294
    :goto_1
    return-object v0

    :cond_0
    move-object v3, v1

    .line 267295
    goto :goto_0

    .line 267296
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_3

    .line 267297
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 267298
    if-eqz v0, :cond_2

    .line 267299
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 267300
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, 0x5fcedbf5

    if-ne v5, v6, :cond_2

    .line 267301
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 267302
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 267303
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 267304
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    .line 267305
    invoke-static {p2}, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 267306
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 267307
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bt8;

    .line 267308
    iput-object v2, v0, LX/Bt8;->b:Ljava/lang/String;

    .line 267309
    instance-of v2, p3, LX/1Po;

    if-eqz v2, :cond_0

    .line 267310
    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    .line 267311
    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-virtual {v1}, LX/1Qt;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 267312
    iput-object v1, v0, LX/Bt8;->c:Ljava/lang/String;

    .line 267313
    :goto_0
    new-instance v1, LX/350;

    invoke-direct {v1, v0}, LX/350;-><init>(LX/31j;)V

    move-object v0, v1

    .line 267314
    :goto_1
    return-object v0

    .line 267315
    :cond_0
    iput-object v1, v0, LX/Bt8;->c:Ljava/lang/String;

    .line 267316
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 267317
    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x49bb3afc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 267318
    check-cast p2, LX/350;

    .line 267319
    if-eqz p2, :cond_0

    .line 267320
    invoke-virtual {p2, p4}, LX/350;->a(Landroid/view/View;)V

    .line 267321
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x7d5662cc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 267322
    check-cast p2, LX/350;

    .line 267323
    if-eqz p2, :cond_0

    .line 267324
    invoke-static {p4}, LX/350;->b(Landroid/view/View;)V

    .line 267325
    :cond_0
    return-void
.end method
