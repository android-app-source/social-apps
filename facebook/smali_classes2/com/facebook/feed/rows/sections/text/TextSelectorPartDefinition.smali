.class public Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/InstantArticleTextComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/VariableTextSizeClickableComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267200
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 267201
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->a:LX/0Ot;

    .line 267202
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->b:LX/0Ot;

    .line 267203
    move-object v0, p3

    .line 267204
    iput-object v0, p0, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->c:LX/0Ot;

    .line 267205
    move-object v0, p4

    .line 267206
    iput-object v0, p0, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->d:LX/0Ot;

    .line 267207
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;
    .locals 7

    .prologue
    .line 267208
    const-class v1, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

    monitor-enter v1

    .line 267209
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267210
    sput-object v2, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267211
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267212
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267213
    new-instance v3, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

    const/16 v4, 0x746

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x74c

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x752

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x74e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 267214
    move-object v0, v3

    .line 267215
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267216
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267217
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267218
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 267219
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 267220
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->b:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->d:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    .line 267221
    const/4 v1, 0x0

    .line 267222
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    .line 267223
    if-nez v2, :cond_2

    .line 267224
    :cond_0
    :goto_0
    move v1, v1

    .line 267225
    if-eqz v1, :cond_1

    .line 267226
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->c:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 267227
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->a:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 267228
    const/4 v0, 0x0

    return-object v0

    .line 267229
    :cond_2
    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    .line 267230
    sget-object p1, LX/1Qt;->FEED:LX/1Qt;

    if-eq v2, p1, :cond_3

    sget-object p1, LX/1Qt;->GOOD_FRIENDS_FEED:LX/1Qt;

    if-eq v2, p1, :cond_3

    sget-object p1, LX/1Qt;->MY_TIMELINE:LX/1Qt;

    if-eq v2, p1, :cond_3

    sget-object p1, LX/1Qt;->OTHER_PERSON_TIMELINE:LX/1Qt;

    if-ne v2, p1, :cond_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 267231
    const/4 v0, 0x1

    return v0
.end method
