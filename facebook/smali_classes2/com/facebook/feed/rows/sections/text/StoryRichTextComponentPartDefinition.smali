.class public Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final d:LX/1VH;

.field private final e:LX/1W6;

.field private final f:LX/1W7;

.field private final g:LX/0tO;

.field private final h:LX/1V0;

.field public final i:LX/14w;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1VH;LX/1W6;LX/1W7;LX/0tO;LX/1V0;LX/14w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267457
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 267458
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->d:LX/1VH;

    .line 267459
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->e:LX/1W6;

    .line 267460
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->f:LX/1W7;

    .line 267461
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->g:LX/0tO;

    .line 267462
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->h:LX/1V0;

    .line 267463
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->i:LX/14w;

    .line 267464
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 267432
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->e:LX/1W6;

    const/4 v1, 0x0

    .line 267433
    new-instance v2, LX/BtD;

    invoke-direct {v2, v0}, LX/BtD;-><init>(LX/1W6;)V

    .line 267434
    iget-object v3, v0, LX/1W6;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BtC;

    .line 267435
    if-nez v3, :cond_0

    .line 267436
    new-instance v3, LX/BtC;

    invoke-direct {v3, v0}, LX/BtC;-><init>(LX/1W6;)V

    .line 267437
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/BtC;->a$redex0(LX/BtC;LX/1De;IILX/BtD;)V

    .line 267438
    move-object v2, v3

    .line 267439
    move-object v1, v2

    .line 267440
    move-object v0, v1

    .line 267441
    iget-object v1, v0, LX/BtC;->a:LX/BtD;

    iput-object p2, v1, LX/BtD;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267442
    iget-object v1, v0, LX/BtC;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 267443
    move-object v0, v0

    .line 267444
    iget-object v1, v0, LX/BtC;->a:LX/BtD;

    iput-object p3, v1, LX/BtD;->b:LX/1Pn;

    .line 267445
    iget-object v1, v0, LX/BtC;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 267446
    move-object v0, v0

    .line 267447
    invoke-static {p3}, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->b(LX/1Pn;)Z

    move-result v1

    .line 267448
    iget-object v2, v0, LX/BtC;->a:LX/BtD;

    iput-boolean v1, v2, LX/BtD;->c:Z

    .line 267449
    move-object v0, v0

    .line 267450
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 267451
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->d:LX/1VH;

    invoke-virtual {v1, p1}, LX/1VH;->c(LX/1De;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1XD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XD;

    move-result-object v1

    sget-object v2, LX/1EO;->NEWSFEED:LX/1EO;

    invoke-virtual {v1, v2}, LX/1XD;->a(LX/1EO;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XD;->a(LX/1X1;)LX/1XD;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 267452
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->h:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    .line 267453
    iget-object v3, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->i:LX/14w;

    invoke-virtual {v3, p2}, LX/14w;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v3

    if-lez v3, :cond_1

    .line 267454
    sget-object v3, LX/1Ua;->k:LX/1Ua;

    .line 267455
    :goto_0
    move-object v3, v3

    .line 267456
    invoke-direct {v2, v3}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v3, LX/1Ua;->e:LX/1Ua;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;
    .locals 11

    .prologue
    .line 267421
    const-class v1, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;

    monitor-enter v1

    .line 267422
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267423
    sput-object v2, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267424
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267425
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267426
    new-instance v3, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1VH;->a(LX/0QB;)LX/1VH;

    move-result-object v5

    check-cast v5, LX/1VH;

    invoke-static {v0}, LX/1W6;->a(LX/0QB;)LX/1W6;

    move-result-object v6

    check-cast v6, LX/1W6;

    invoke-static {v0}, LX/1W7;->a(LX/0QB;)LX/1W7;

    move-result-object v7

    check-cast v7, LX/1W7;

    invoke-static {v0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v8

    check-cast v8, LX/0tO;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v9

    check-cast v9, LX/1V0;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v10

    check-cast v10, LX/14w;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;-><init>(Landroid/content/Context;LX/1VH;LX/1W6;LX/1W7;LX/0tO;LX/1V0;LX/14w;)V

    .line 267427
    move-object v0, v3

    .line 267428
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267429
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267430
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/1Pn;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 267420
    check-cast p0, LX/1Po;

    invoke-interface {p0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->PERMALINK:LX/1Qt;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 267405
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 267419
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 267408
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267409
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267410
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 267411
    invoke-static {v0}, LX/1VF;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 267412
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 267413
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const p1, 0x285feb

    if-ne v1, p1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 267414
    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 267415
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v1

    .line 267416
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->a()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->k()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->l()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 267417
    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 267418
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;->g:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_3

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 267406
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267407
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
