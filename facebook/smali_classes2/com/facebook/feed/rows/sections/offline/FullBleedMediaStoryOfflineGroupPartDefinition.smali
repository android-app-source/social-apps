.class public Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

.field private final c:LX/0qn;

.field private final d:Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;LX/0qn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 264889
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 264890
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->a:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

    .line 264891
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->b:Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

    .line 264892
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->c:LX/0qn;

    .line 264893
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    .line 264894
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;
    .locals 7

    .prologue
    .line 264878
    const-class v1, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;

    monitor-enter v1

    .line 264879
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 264880
    sput-object v2, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 264881
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264882
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 264883
    new-instance p0, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v6

    check-cast v6, LX/0qn;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;LX/0qn;)V

    .line 264884
    move-object v0, p0

    .line 264885
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 264886
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264887
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 264888
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0qn;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0qn;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 264875
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 264876
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    .line 264877
    invoke-static {p0}, LX/1VF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 264869
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 264870
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->b:Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264871
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    new-instance v1, LX/Bsx;

    .line 264872
    new-instance v2, LX/Bsw;

    invoke-direct {v2, p0}, LX/Bsw;-><init>(Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;)V

    move-object v2, v2

    .line 264873
    invoke-direct {v1, p2, v2}, LX/Bsx;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/Bsv;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264874
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 264867
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 264868
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->c:LX/0qn;

    invoke-static {p1, v0}, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0qn;)Z

    move-result v0

    return v0
.end method
