.class public Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static s:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

.field public final d:Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

.field public final e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field public final f:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

.field public final i:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

.field private final j:Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

.field private final l:LX/0qn;

.field private final m:Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

.field public final n:Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

.field private final o:Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

.field public final p:Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

.field private final r:Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;LX/0Ot;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;LX/0qn;Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;",
            "Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;",
            "Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;",
            "Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;",
            "Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;",
            ">;",
            "Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;",
            "LX/0qn;",
            "Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;",
            "Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;",
            "Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;",
            "Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;",
            "Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 264947
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 264948
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->n:Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    .line 264949
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->j:Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    .line 264950
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->h:Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

    .line 264951
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 264952
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    .line 264953
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 264954
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->c:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    .line 264955
    iput-object p9, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->f:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    .line 264956
    iput-object p10, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->g:LX/0Ot;

    .line 264957
    iput-object p11, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->i:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    .line 264958
    iput-object p12, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->k:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    .line 264959
    iput-object p13, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->l:LX/0qn;

    .line 264960
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->d:Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

    .line 264961
    iput-object p14, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->m:Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    .line 264962
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->o:Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

    .line 264963
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->p:Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;

    .line 264964
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->q:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    .line 264965
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->r:Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    .line 264966
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;
    .locals 3

    .prologue
    .line 264984
    const-class v1, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    monitor-enter v1

    .line 264985
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->s:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 264986
    sput-object v2, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->s:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 264987
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264988
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 264989
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264990
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 264991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0qn;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0qn;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 264980
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 264981
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 264982
    invoke-virtual {p1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    .line 264983
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;
    .locals 20

    .prologue
    .line 264992
    new-instance v1, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    const/16 v11, 0x9a9

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-static/range {p0 .. p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v14

    check-cast v14, LX/0qn;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    move-result-object v15

    check-cast v15, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

    move-result-object v16

    check-cast v16, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;

    move-result-object v17

    check-cast v17, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    move-result-object v18

    check-cast v18, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;->a(LX/0QB;)Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    move-result-object v19

    check-cast v19, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    invoke-direct/range {v1 .. v19}, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;-><init>(Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;LX/0Ot;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;LX/0qn;Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;)V

    .line 264993
    return-object v1
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 264969
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 264970
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->o:Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264971
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->j:Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    new-instance v1, LX/Bsx;

    .line 264972
    new-instance v2, LX/Bsy;

    invoke-direct {v2, p0}, LX/Bsy;-><init>(Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;)V

    move-object v2, v2

    .line 264973
    invoke-direct {v1, p2, v2}, LX/Bsx;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/Bsv;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264974
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->n:Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    invoke-virtual {v0, p2}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264975
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->h:Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264976
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->r:Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264977
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->k:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264978
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->m:Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264979
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 264967
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 264968
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->l:LX/0qn;

    invoke-static {p1, v0}, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0qn;)Z

    move-result v0

    return v0
.end method
