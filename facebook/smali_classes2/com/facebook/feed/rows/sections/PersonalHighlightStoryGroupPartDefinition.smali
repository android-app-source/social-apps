.class public Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/PersonalHighlightSubgroupSelectorPartDefinition;

.field private final b:Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;

.field private final c:LX/1VF;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;Lcom/facebook/feed/rows/sections/PersonalHighlightSubgroupSelectorPartDefinition;LX/1VF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 264924
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 264925
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;->c:LX/1VF;

    .line 264926
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;->a:Lcom/facebook/feed/rows/sections/PersonalHighlightSubgroupSelectorPartDefinition;

    .line 264927
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;

    .line 264928
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;
    .locals 6

    .prologue
    .line 264929
    const-class v1, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;

    monitor-enter v1

    .line 264930
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 264931
    sput-object v2, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 264932
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264933
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 264934
    new-instance p0, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/PersonalHighlightSubgroupSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/PersonalHighlightSubgroupSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/PersonalHighlightSubgroupSelectorPartDefinition;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v5

    check-cast v5, LX/1VF;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;Lcom/facebook/feed/rows/sections/PersonalHighlightSubgroupSelectorPartDefinition;LX/1VF;)V

    .line 264935
    move-object v0, p0

    .line 264936
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 264937
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264938
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 264939
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1VF;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1VF;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 264940
    invoke-static {p0}, LX/1VF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 264941
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 264942
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264943
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;->a:Lcom/facebook/feed/rows/sections/PersonalHighlightSubgroupSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264944
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 264945
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 264946
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;->c:LX/1VF;

    invoke-static {p1, v0}, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1VF;)Z

    move-result v0

    return v0
.end method
