.class public Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static F:LX/0Xm;


# instance fields
.field private final A:Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

.field private final B:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

.field private final C:Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

.field private final D:Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;

.field private final E:LX/0tT;

.field private final a:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

.field private final d:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final h:Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

.field private final i:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

.field private final j:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final l:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

.field private final q:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

.field private final r:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;

.field private final u:Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

.field private final v:Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

.field private final w:Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

.field private final x:Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

.field private final z:Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;LX/0Ot;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;LX/0Ot;Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;LX/0Ot;LX/0Ot;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;LX/0tT;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;",
            "Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;",
            ">;",
            "Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;",
            "Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;",
            ">;",
            "Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;",
            ">;",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;",
            "Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;",
            "Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;",
            "Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;",
            "Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;",
            "Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;",
            "Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;",
            "Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;",
            "Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;",
            "Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;",
            "Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;",
            "Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;",
            "Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;",
            "LX/0tT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265572
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 265573
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    .line 265574
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 265575
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 265576
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    .line 265577
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;

    .line 265578
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->f:LX/0Ot;

    .line 265579
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->g:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 265580
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->i:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    .line 265581
    iput-object p9, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->j:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    .line 265582
    iput-object p10, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->k:LX/0Ot;

    .line 265583
    iput-object p11, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->h:Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    .line 265584
    iput-object p12, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->l:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 265585
    iput-object p13, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->n:LX/0Ot;

    .line 265586
    iput-object p14, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->m:LX/0Ot;

    .line 265587
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->o:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    .line 265588
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->p:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    .line 265589
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->q:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    .line 265590
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->r:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    .line 265591
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->s:Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;

    .line 265592
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->t:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;

    .line 265593
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->u:Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

    .line 265594
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->v:Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    .line 265595
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->w:Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

    .line 265596
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->x:Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    .line 265597
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->y:Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    .line 265598
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->z:Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;

    .line 265599
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->A:Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

    .line 265600
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->B:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    .line 265601
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->C:Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    .line 265602
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->D:Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;

    .line 265603
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->E:LX/0tT;

    .line 265604
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;
    .locals 3

    .prologue
    .line 265564
    const-class v1, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    monitor-enter v1

    .line 265565
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->F:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265566
    sput-object v2, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->F:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265567
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265568
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265569
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265570
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265571
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;
    .locals 33

    .prologue
    .line 265605
    new-instance v1, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;

    const/16 v7, 0x929

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    const/16 v11, 0x6ef

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    const/16 v14, 0x8f5

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x8f7

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    move-result-object v16

    check-cast v16, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    move-result-object v17

    check-cast v17, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    move-result-object v18

    check-cast v18, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    move-result-object v19

    check-cast v19, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;

    move-result-object v20

    check-cast v20, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;

    move-result-object v21

    check-cast v21, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

    move-result-object v22

    check-cast v22, Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    move-result-object v23

    check-cast v23, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

    move-result-object v24

    check-cast v24, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    move-result-object v25

    check-cast v25, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    move-result-object v26

    check-cast v26, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;

    move-result-object v27

    check-cast v27, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

    move-result-object v28

    check-cast v28, Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    move-result-object v29

    check-cast v29, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;->a(LX/0QB;)Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    move-result-object v30

    check-cast v30, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;

    move-result-object v31

    check-cast v31, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;

    invoke-static/range {p0 .. p0}, LX/0tT;->a(LX/0QB;)LX/0tT;

    move-result-object v32

    check-cast v32, LX/0tT;

    invoke-direct/range {v1 .. v32}, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;LX/0Ot;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;LX/0Ot;Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;LX/0Ot;LX/0Ot;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;LX/0tT;)V

    .line 265606
    return-object v1
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 265531
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 265532
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265533
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265534
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->A:Lcom/facebook/spam/displaytimeinvalidation/rows/DTIHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265535
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    new-instance v2, LX/1W0;

    invoke-direct {v2, v0, p3}, LX/1W0;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/1Pr;)V

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265536
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265537
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->D:Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265538
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->y:Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265539
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265540
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->B:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265541
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265542
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->i:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265543
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/TextOrTranslationSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265544
    invoke-static {p2}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->f:LX/0Ot;

    invoke-virtual {p1, v1, v2, p2}, LX/1RF;->a(ZLX/0Ot;Ljava/lang/Object;)Z

    .line 265545
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->g:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265546
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->h:Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265547
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->z:Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265548
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->w:Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265549
    invoke-static {p2}, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->k:LX/0Ot;

    invoke-virtual {p1, v1, v2, p2}, LX/1RF;->a(ZLX/0Ot;Ljava/lang/Object;)Z

    .line 265550
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->l:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265551
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    sget-object v2, LX/1Qt;->FEED:LX/1Qt;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->m:LX/0Ot;

    invoke-static {p1, v1, v2, p2}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->n:LX/0Ot;

    invoke-virtual {v1, v2, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 265552
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->j:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265553
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->C:Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265554
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->o:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265555
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->p:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265556
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->q:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265557
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->s:Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265558
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->r:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265559
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->t:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265560
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->v:Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265561
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->u:Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265562
    const/4 v0, 0x0

    return-object v0

    .line 265563
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 1

    .prologue
    .line 265527
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/lang/Void;

    check-cast p3, LX/1Pf;

    .line 265528
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V

    .line 265529
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->x:Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 265530
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 265525
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265526
    invoke-static {p1}, LX/1VF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->E:LX/0tT;

    invoke-virtual {v0}, LX/0tT;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
