.class public Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DeprecatedSuperclass"
    }
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 264767
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 264768
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;->a:LX/0Ot;

    .line 264769
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;
    .locals 4

    .prologue
    .line 264770
    const-class v1, Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;

    monitor-enter v1

    .line 264771
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 264772
    sput-object v2, Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 264773
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264774
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 264775
    new-instance v3, Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;

    const/16 p0, 0x701

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;-><init>(LX/0Ot;)V

    .line 264776
    move-object v0, v3

    .line 264777
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 264778
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264779
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 264780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 264781
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 264782
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 264783
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 264784
    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 264785
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 264786
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 264787
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;

    .line 264788
    invoke-virtual {v0}, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->g()Z

    move-result v0

    return v0
.end method
