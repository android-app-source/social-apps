.class public Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DeprecatedSuperclass"
    }
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/1Vc;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0pJ;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2tV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Vc;LX/0Ot;LX/0pJ;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1Vc;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0pJ;",
            "LX/0Ot",
            "<",
            "LX/2tV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 264824
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 264825
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->d:LX/1Vc;

    .line 264826
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->e:LX/0Ot;

    .line 264827
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->f:LX/0pJ;

    .line 264828
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->g:LX/0Ot;

    .line 264829
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/graphql/model/FeedUnit;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 264830
    invoke-direct {p0, p2}, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v0

    .line 264831
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->d:LX/1Vc;

    const/4 v2, 0x0

    .line 264832
    new-instance p0, LX/Brs;

    invoke-direct {p0, v1}, LX/Brs;-><init>(LX/1Vc;)V

    .line 264833
    sget-object p2, LX/1Vc;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/Brr;

    .line 264834
    if-nez p2, :cond_0

    .line 264835
    new-instance p2, LX/Brr;

    invoke-direct {p2}, LX/Brr;-><init>()V

    .line 264836
    :cond_0
    invoke-static {p2, p1, v2, v2, p0}, LX/Brr;->a$redex0(LX/Brr;LX/1De;IILX/Brs;)V

    .line 264837
    move-object p0, p2

    .line 264838
    move-object v2, p0

    .line 264839
    move-object v1, v2

    .line 264840
    iget-object v2, v1, LX/Brr;->a:LX/Brs;

    iput-object v0, v2, LX/Brs;->a:Ljava/lang/String;

    .line 264841
    iget-object v2, v1, LX/Brr;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 264842
    move-object v0, v1

    .line 264843
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;
    .locals 9

    .prologue
    .line 264813
    const-class v1, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;

    monitor-enter v1

    .line 264814
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 264815
    sput-object v2, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 264816
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264817
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 264818
    new-instance v3, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1Vc;->a(LX/0QB;)LX/1Vc;

    move-result-object v5

    check-cast v5, LX/1Vc;

    const/16 v6, 0xf9a

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v7

    check-cast v7, LX/0pJ;

    const/16 v8, 0x5fb

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;-><init>(Landroid/content/Context;LX/1Vc;LX/0Ot;LX/0pJ;LX/0Ot;)V

    .line 264819
    move-object v0, v3

    .line 264820
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 264821
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264822
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 264823
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 264793
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->f:LX/0pJ;

    const-string v1, "{\"ctr_multiply_values\" : {\"base_values\" : { \"weight_final\" : \"1\", \"seen\" : {\"viewed\" : \"-10000\"}, \"!fresh\" : {\"viewed\" : \"-2000\"}}},\"ctr_value_features\": {\"seen\": \"client_has_seen\", \"fresh\": \"cur_client_story_age_ms < 540001\"}}"

    invoke-virtual {v0, v1}, LX/0pJ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 264794
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2tV;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    .line 264795
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 264796
    iget-object v3, v1, LX/2tV;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0y5;

    invoke-virtual {v3, v2}, LX/0y5;->a(Ljava/lang/String;)LX/14s;

    move-result-object p0

    .line 264797
    iget-object v3, v1, LX/2tV;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0y6;

    .line 264798
    iget-object p1, v3, LX/0y6;->b:LX/0y8;

    move-object p1, p1

    .line 264799
    :try_start_0
    const-string v3, "{\n"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264800
    const-string v3, " \"client_features\":"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264801
    if-nez p0, :cond_0

    const-string v3, "{}"

    :goto_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264802
    const-string v3, ",\n"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264803
    const-string v3, " \"global_ranking_signals\":"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264804
    if-nez p1, :cond_1

    const-string v3, "{}"

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264805
    const-string v3, "\n}"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 264806
    :goto_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 264807
    move-object v1, v1

    .line 264808
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 264809
    :cond_0
    :try_start_1
    iget-object v3, v1, LX/2tV;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lB;

    invoke-virtual {v3}, LX/0lC;->h()LX/4ps;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 264810
    :cond_1
    iget-object v3, v1, LX/2tV;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lB;

    invoke-virtual {v3}, LX/0lC;->h()LX/4ps;

    move-result-object v3

    invoke-virtual {v3, p1}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch LX/28F; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_1

    .line 264811
    :catch_0
    move-exception v3

    .line 264812
    sget-object p0, LX/2tV;->a:Ljava/lang/String;

    const-string p1, "Error creating client feature json string"

    invoke-static {p0, p1, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 264844
    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->a(LX/1De;Lcom/facebook/graphql/model/FeedUnit;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 264792
    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->a(LX/1De;Lcom/facebook/graphql/model/FeedUnit;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 264791
    invoke-virtual {p0}, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->g()Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 264789
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    .line 264790
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/debug/ClientValueModelComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eJ;->o:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
