.class public Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/EdgeStoryGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/1VF;

.field private final k:LX/0qn;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1VF;LX/0qn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/debug/DebugInfoPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/EdgeStoryGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;",
            ">;",
            "LX/1VF;",
            "LX/0qn;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 264752
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 264753
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->i:LX/0Ot;

    .line 264754
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->a:LX/0Ot;

    .line 264755
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->b:LX/0Ot;

    .line 264756
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->c:LX/0Ot;

    .line 264757
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->d:LX/0Ot;

    .line 264758
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->e:LX/0Ot;

    .line 264759
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->f:LX/0Ot;

    .line 264760
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->g:LX/0Ot;

    .line 264761
    iput-object p9, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->h:LX/0Ot;

    .line 264762
    iput-object p11, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->m:LX/0Ot;

    .line 264763
    iput-object p10, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->l:LX/0Ot;

    .line 264764
    iput-object p12, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->j:LX/1VF;

    .line 264765
    iput-object p13, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->k:LX/0qn;

    .line 264766
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;
    .locals 3

    .prologue
    .line 264744
    const-class v1, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;

    monitor-enter v1

    .line 264745
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 264746
    sput-object v2, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 264747
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264748
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 264749
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264750
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 264751
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;
    .locals 14

    .prologue
    .line 264719
    new-instance v0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;

    const/16 v1, 0x702

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x6df

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x6e0

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x6dd

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x6ea

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x744

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x6e9

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x95f

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x8bd

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x742

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x948

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {p0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v12

    check-cast v12, LX/1VF;

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v13

    check-cast v13, LX/0qn;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1VF;LX/0qn;)V

    .line 264720
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 264722
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 264723
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264724
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->k:LX/0qn;

    invoke-static {p2, v0}, Lcom/facebook/feed/rows/sections/offline/FullBleedMediaStoryOfflineGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0qn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264725
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 264726
    :goto_0
    return-object v1

    .line 264727
    :cond_0
    invoke-static {p2}, LX/1VF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264728
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 264729
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->j:LX/1VF;

    invoke-static {p2, v0}, Lcom/facebook/feed/rows/sections/PersonalHighlightStoryGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1VF;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264730
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 264731
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->k:LX/0qn;

    invoke-static {p2, v0}, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0qn;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 264732
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 264733
    :cond_3
    invoke-static {p2}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 264734
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 264735
    :cond_4
    invoke-static {p2}, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 264736
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 264737
    :cond_5
    invoke-static {p2}, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 264738
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 264739
    :cond_6
    invoke-static {p2}, Lcom/facebook/feed/rows/sections/SharedStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 264740
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 264741
    :cond_7
    invoke-static {p2}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 264742
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 264743
    :cond_8
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 264721
    const/4 v0, 0x1

    return v0
.end method
