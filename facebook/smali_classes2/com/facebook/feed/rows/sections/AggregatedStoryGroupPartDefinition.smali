.class public Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

.field private final c:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

.field private final f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final g:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feed/rows/sections/header/TextHeaderSelectorPartDefinition;

.field private final i:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;Lcom/facebook/feed/rows/sections/header/TextHeaderSelectorPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265050
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 265051
    iput-object p9, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    .line 265052
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    .line 265053
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 265054
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    .line 265055
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    .line 265056
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 265057
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    .line 265058
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->h:Lcom/facebook/feed/rows/sections/header/TextHeaderSelectorPartDefinition;

    .line 265059
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->i:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    .line 265060
    iput-object p10, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->j:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    .line 265061
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;
    .locals 14

    .prologue
    .line 265039
    const-class v1, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

    monitor-enter v1

    .line 265040
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265041
    sput-object v2, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265042
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265043
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 265044
    new-instance v3, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/TextHeaderSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/TextHeaderSelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/TextHeaderSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;Lcom/facebook/feed/rows/sections/header/TextHeaderSelectorPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;)V

    .line 265045
    move-object v0, v3

    .line 265046
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265047
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265048
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 265035
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265036
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265037
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 265038
    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, LX/1WP;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1WQ;

    move-result-object v2

    invoke-static {v2}, LX/14w;->a(LX/1WQ;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, LX/1VF;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 265019
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 265020
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265021
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265022
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->i:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    new-instance v2, LX/1W0;

    invoke-direct {v2, v0, p3}, LX/1W0;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/1Pr;)V

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265023
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->h:Lcom/facebook/feed/rows/sections/header/TextHeaderSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265024
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265025
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265026
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265027
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265028
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265029
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265030
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->j:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265031
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpGroupPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265032
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 265033
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265034
    invoke-static {p1}, Lcom/facebook/feed/rows/sections/AggregatedStoryGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
