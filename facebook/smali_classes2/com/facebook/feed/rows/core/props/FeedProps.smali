.class public Lcom/facebook/feed/rows/core/props/FeedProps;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/17z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/flatbuffers/Flattenable;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/17z",
        "<TT;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/flatbuffers/Flattenable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 202378
    new-instance v0, LX/180;

    invoke-direct {v0}, LX/180;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/core/props/FeedProps;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 202371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202372
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    .line 202373
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 202374
    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v0, p1}, Lcom/facebook/feed/rows/core/props/FeedProps;-><init>(Landroid/os/Parcel;)V

    :goto_1
    iput-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 202375
    return-void

    .line 202376
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 202377
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private constructor <init>(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202368
    iput-object p1, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    .line 202369
    iput-object p2, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 202370
    return-void
.end method

.method public static a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 3
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(TS;",
            "LX/0Px",
            "<",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 202357
    if-eqz p0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 202358
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202359
    :cond_0
    invoke-static {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 202360
    :goto_1
    return-object v0

    .line 202361
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 202362
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 202363
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    move-object v2, v1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_3

    .line 202364
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 202365
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 202366
    :cond_3
    new-instance v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v0, p0, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;-><init>(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_1
.end method

.method public static c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(TS;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 202356
    new-instance v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;-><init>(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(TS;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TS;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202355
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v0, p1, p0}, Lcom/facebook/feed/rows/core/props/FeedProps;-><init>(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method

.method public final a()Lcom/facebook/flatbuffers/Flattenable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 202354
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    return-object v0
.end method

.method public final b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(TS;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 202353
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/flatbuffers/Flattenable;
    .locals 2

    .prologue
    .line 202379
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    .line 202380
    const/4 v0, 0x0

    .line 202381
    :goto_0
    return-object v0

    .line 202382
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 202383
    :goto_1
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_1

    .line 202384
    iget-object v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    goto :goto_1

    .line 202385
    :cond_1
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 202386
    goto :goto_0
.end method

.method public final c()Lcom/facebook/flatbuffers/Flattenable;
    .locals 1

    .prologue
    .line 202350
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 202351
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 202352
    goto :goto_0
.end method

.method public final d()Lcom/facebook/flatbuffers/Flattenable;
    .locals 1

    .prologue
    .line 202347
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 202348
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 202349
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 202346
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 202339
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 202340
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 202341
    :goto_0
    if-eqz v0, :cond_0

    .line 202342
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 202343
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 202344
    iget-object v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    goto :goto_0

    .line 202345
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 202335
    instance-of v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v1, :cond_1

    .line 202336
    :cond_0
    :goto_0
    return v0

    .line 202337
    :cond_1
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 202338
    iget-object v1, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    iget-object v2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202323
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method

.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 202334
    return-object p0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 202330
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 202331
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 202332
    return v0

    .line 202333
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 202324
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 202325
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 202326
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 202327
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->writeToParcel(Landroid/os/Parcel;I)V

    .line 202328
    :cond_0
    return-void

    .line 202329
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
