.class public final Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1Jf;


# direct methods
.method public constructor <init>(LX/1Jf;)V
    .locals 0

    .prologue
    .line 230321
    iput-object p1, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 230322
    iget-object v0, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    iget-object v0, v0, LX/1Jf;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 230323
    iget-object v0, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    iget-object v3, v0, LX/1Jf;->c:Ljava/lang/Object;

    monitor-enter v3

    .line 230324
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    iget-object v0, v0, LX/1Jf;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rb;

    .line 230325
    iget-object v1, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    iget-object v1, v1, LX/1Jf;->i:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 230326
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 230327
    iget-object v2, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    iget-object v2, v2, LX/1Jf;->e:Landroid/util/LruCache;

    invoke-virtual {v2, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 230328
    if-nez v2, :cond_1

    .line 230329
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 230330
    iget-object v5, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    iget-object v5, v5, LX/1Jf;->e:Landroid/util/LruCache;

    invoke-virtual {v5, v0, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230331
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 230332
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 230333
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    iget-object v0, v0, LX/1Jf;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 230334
    iget-object v0, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    iget-boolean v0, v0, LX/1Jf;->k:Z

    if-eqz v0, :cond_3

    .line 230335
    iget-object v0, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    iget-object v0, v0, LX/1Jf;->d:LX/1Jj;

    .line 230336
    iget-object v1, v0, LX/1Jj;->a:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 230337
    iget-boolean v1, v0, LX/1Jj;->j:Z

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 230338
    invoke-static {v0}, LX/1Jj;->d(LX/1Jj;)V

    .line 230339
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/rows/prefetch/MultiRowImagePrefetcherWrapperImpl$PrefetchRegistrationRunnable;->a:LX/1Jf;

    const/4 v1, 0x0

    .line 230340
    iput-object v1, v0, LX/1Jf;->l:Ljava/util/concurrent/ScheduledFuture;

    .line 230341
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
