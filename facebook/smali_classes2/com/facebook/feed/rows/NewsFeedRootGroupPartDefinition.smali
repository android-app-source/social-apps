.class public Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0Ot",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final c:LX/1T5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T5",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/HoldoutUnitPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;",
            "Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;",
            "Ljava/util/Set",
            "<",
            "LX/1TA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 249575
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 249576
    move-object v0, p2

    .line 249577
    invoke-static {}, LX/1T5;->a()LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-virtual {v1, v2, v0}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/HideableUnit;

    .line 249578
    move-object v2, p3

    .line 249579
    invoke-virtual {v0, v1, v2}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/GraphQLHoldoutAdFeedUnit;

    invoke-virtual {v0, v1, p4}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;->c:LX/1T5;

    .line 249580
    iput-object p5, p0, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;->a:LX/0Ot;

    .line 249581
    iput-object p6, p0, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;->d:Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;

    .line 249582
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 249583
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 249584
    new-instance v2, LX/1T8;

    invoke-direct {v2, v1}, LX/1T8;-><init>(LX/0P2;)V

    .line 249585
    invoke-interface {p7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1TA;

    .line 249586
    invoke-interface {v0, v2}, LX/1TA;->a(LX/1T8;)V

    goto :goto_0

    .line 249587
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;->b:Ljava/util/Map;

    .line 249588
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;
    .locals 11

    .prologue
    .line 249548
    const-class v1, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;

    monitor-enter v1

    .line 249549
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 249550
    sput-object v2, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 249551
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249552
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 249553
    new-instance v3, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;

    const/16 v4, 0x6e3

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x954

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x956

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x6e4

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x6fa

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;

    invoke-static {v0}, LX/1T4;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;Ljava/util/Set;)V

    .line 249554
    move-object v0, v3

    .line 249555
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 249556
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249557
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 249558
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 249560
    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    const/4 v4, 0x0

    .line 249561
    if-nez p2, :cond_1

    .line 249562
    :cond_0
    :goto_0
    return-object v4

    .line 249563
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;->c:LX/1T5;

    invoke-virtual {v0, p1, p2}, LX/1T5;->a(LX/1RF;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 249564
    iget-object v0, p0, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 249565
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 249566
    invoke-virtual {v1, p2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 249567
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 249568
    instance-of v1, p2, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v1, :cond_3

    .line 249569
    new-instance v1, LX/1VM;

    invoke-direct {v1, p0, v0}, LX/1VM;-><init>(Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;)V

    move-object v1, v1

    .line 249570
    new-instance v3, LX/1VN;

    move-object v0, p2

    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    invoke-direct {v3, v1, v0}, LX/1VN;-><init>(LX/1VM;Lcom/facebook/graphql/model/HideableUnit;)V

    .line 249571
    iget-object v0, p0, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;->d:Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;

    invoke-virtual {p1, v0, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    .line 249572
    :goto_1
    if-eqz v0, :cond_2

    goto :goto_0

    .line 249573
    :cond_3
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 249574
    :cond_4
    iget-object v0, p0, Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 249559
    const/4 v0, 0x1

    return v0
.end method
