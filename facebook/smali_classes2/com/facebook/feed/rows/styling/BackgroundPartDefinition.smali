.class public Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1X6;",
        "Landroid/graphics/drawable/Drawable;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1V2;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1dp;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1V8;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/14w;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1dr;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/1V2;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1dp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1V8;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/14w;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1dr;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 256326
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 256327
    iput-object p1, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a:Landroid/content/Context;

    .line 256328
    iput-object p2, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->b:LX/0Ot;

    .line 256329
    iput-object p3, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->c:LX/0Ot;

    .line 256330
    iput-object p4, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->d:LX/0Ot;

    .line 256331
    iput-object p5, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->e:LX/0Ot;

    .line 256332
    iput-object p6, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->f:LX/0Ot;

    .line 256333
    iput-object p7, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->g:LX/0Ot;

    .line 256334
    return-void
.end method

.method private a(LX/1aD;LX/1X6;LX/1Ps;)Landroid/graphics/drawable/Drawable;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<",
            "LX/1Ps;",
            ">;",
            "LX/1X6;",
            "LX/1Ps;",
            ")",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 256335
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 256336
    iget-object v0, p2, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v11, v0

    .line 256337
    :goto_0
    iget-object v1, p2, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14w;

    invoke-static {v1, v0}, LX/1X7;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/14w;)I

    move-result v0

    .line 256338
    iget-object v1, p2, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/1X6;->e:LX/1X9;

    iget-object v3, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1V2;

    invoke-interface/range {p3 .. p3}, LX/1Ps;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v4

    invoke-interface/range {p3 .. p3}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v5

    invoke-interface/range {p3 .. p3}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v6

    invoke-interface/range {p3 .. p3}, LX/1Ps;->i()Ljava/lang/Object;

    move-result-object v7

    invoke-interface/range {p3 .. p3}, LX/1Ps;->j()Ljava/lang/Object;

    move-result-object v8

    invoke-static/range {v0 .. v8}, LX/1X7;->a(ILcom/facebook/feed/rows/core/props/FeedProps;LX/1X9;LX/1V2;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)LX/1X9;

    move-result-object v1

    .line 256339
    iget-object v2, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1V8;

    iget-object v4, p2, LX/1X6;->b:LX/1Ua;

    iget-object v5, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a:Landroid/content/Context;

    move v2, v0

    move-object v6, v12

    invoke-static/range {v1 .. v6}, LX/1X7;->a(LX/1X9;ILX/1V8;LX/1Ua;Landroid/content/Context;Landroid/graphics/Rect;)V

    .line 256340
    iget v3, p2, LX/1X6;->c:I

    iget v4, p2, LX/1X6;->d:I

    iget-object v2, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1dp;

    iget-object v6, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a:Landroid/content/Context;

    iget-object v8, p2, LX/1X6;->b:LX/1Ua;

    iget-object v2, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/1V8;

    iget-object v2, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1dr;

    iget-object v7, p2, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v7}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v10

    move v2, v0

    move-object v7, v12

    invoke-static/range {v1 .. v10}, LX/1X7;->a(LX/1X9;IIILX/1dp;Landroid/content/Context;Landroid/graphics/Rect;LX/1Ua;LX/1V8;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 256341
    instance-of v0, v11, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 256342
    iget-object v0, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dr;

    iget-object v2, p2, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v2, v1}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/graphics/drawable/Drawable;)V

    .line 256343
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/1ds;

    iget v3, v12, Landroid/graphics/Rect;->left:I

    iget v4, v12, Landroid/graphics/Rect;->top:I

    iget v5, v12, Landroid/graphics/Rect;->right:I

    iget v6, v12, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v2, v3, v4, v5, v6}, LX/1ds;-><init>(IIII)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 256344
    return-object v1

    .line 256345
    :cond_1
    iget-object v0, p2, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    move-object v11, v0

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;
    .locals 11

    .prologue
    .line 256346
    const-class v1, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    monitor-enter v1

    .line 256347
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 256348
    sput-object v2, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 256349
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256350
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 256351
    new-instance v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x75b

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x755

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x759

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xb23

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x794

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xe19

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 256352
    move-object v0, v3

    .line 256353
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 256354
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256355
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 256356
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 256357
    check-cast p2, LX/1X6;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/1aD;LX/1X6;LX/1Ps;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5f0791e9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 256358
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 256359
    invoke-virtual {p4, p2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 256360
    const/16 v1, 0x1f

    const v2, 0x6bb57c7b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
