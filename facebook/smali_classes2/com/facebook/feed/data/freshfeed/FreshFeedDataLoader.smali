.class public Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;
.super LX/0gD;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0gC;


# static fields
.field public static final C:Ljava/lang/String;

.field private static final D:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Z

.field private E:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1jj;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private F:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/18E;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final G:LX/0SG;

.field public final H:LX/0pV;

.field private final I:LX/1ga;

.field private final J:LX/1gY;

.field private final K:LX/1gZ;

.field private final L:LX/1gc;

.field private final M:LX/1gb;

.field private final N:LX/1gd;

.field public final O:LX/0pJ;

.field private final P:LX/0Tf;

.field public final Q:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0YG",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final R:LX/0ad;

.field public final S:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final T:LX/0Sh;

.field public final U:LX/1ge;

.field public final V:LX/1EU;

.field public W:LX/1gi;

.field public X:LX/1gp;

.field public Y:LX/1EM;

.field public Z:I

.field private aa:J

.field public ab:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/data/freshfeed/FreshFeedCacheListener;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/1J0;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public q:Landroid/os/HandlerThread;

.field public r:LX/0jY;

.field public s:LX/1ge;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public t:LX/1gf;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public u:LX/1gg;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public v:Z
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public w:Z
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public x:Z
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public y:Z
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public z:LX/0YG;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YG",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 111525
    const-class v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    .line 111526
    const-class v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->D:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1gY;LX/0pV;LX/1gZ;LX/1ga;LX/0SG;LX/0pJ;LX/0Tf;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/1gb;LX/1gc;LX/1gd;LX/1EU;)V
    .locals 4
    .param p8    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 111283
    invoke-direct {p0, p1}, LX/0gD;-><init>(Landroid/content/Context;)V

    .line 111284
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->E:LX/0Ot;

    .line 111285
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->F:LX/0Ot;

    .line 111286
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->v:Z

    .line 111287
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->w:Z

    .line 111288
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->x:Z

    .line 111289
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->y:Z

    .line 111290
    const/4 v2, 0x0

    iput v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Z:I

    .line 111291
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->aa:J

    .line 111292
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->z:LX/0YG;

    .line 111293
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->A:Ljava/lang/String;

    .line 111294
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->B:Z

    .line 111295
    iput-object p6, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->G:LX/0SG;

    .line 111296
    iput-object p5, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->I:LX/1ga;

    .line 111297
    iput-object p3, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    .line 111298
    iput-object p2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->J:LX/1gY;

    .line 111299
    iput-object p4, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->K:LX/1gZ;

    .line 111300
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->L:LX/1gc;

    .line 111301
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->M:LX/1gb;

    .line 111302
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->N:LX/1gd;

    .line 111303
    const-string v2, "db_loader_status"

    move-object/from16 v0, p13

    invoke-virtual {v0, v2}, LX/1gc;->a(Ljava/lang/String;)LX/1ge;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->s:LX/1ge;

    .line 111304
    const-string v2, "network_loader"

    move-object/from16 v0, p12

    invoke-virtual {v0, v2}, LX/1gb;->a(Ljava/lang/String;)LX/1gf;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    .line 111305
    const-string v2, "collection_loader"

    move-object/from16 v0, p14

    invoke-virtual {v0, v2}, LX/1gd;->a(Ljava/lang/String;)LX/1gg;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    .line 111306
    iput-object p7, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->O:LX/0pJ;

    .line 111307
    iput-object p8, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->P:LX/0Tf;

    .line 111308
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Q:Ljava/util/concurrent/atomic/AtomicReference;

    .line 111309
    iput-object p10, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->S:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 111310
    iput-object p9, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->R:LX/0ad;

    .line 111311
    iput-object p11, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    .line 111312
    const-string v2, "delayed_cache_status"

    move-object/from16 v0, p13

    invoke-virtual {v0, v2}, LX/1gc;->a(Ljava/lang/String;)LX/1ge;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->U:LX/1ge;

    .line 111313
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->V:LX/1EU;

    .line 111314
    return-void
.end method

.method private static T(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V
    .locals 9
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 111315
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111316
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    if-eqz v0, :cond_1

    .line 111317
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->z:LX/0YG;

    if-eqz v0, :cond_0

    .line 111318
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->z:LX/0YG;

    invoke-interface {v0, v2}, LX/0YG;->cancel(Z)Z

    .line 111319
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->z:LX/0YG;

    .line 111320
    :cond_0
    :goto_0
    return-void

    .line 111321
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->L:LX/1gc;

    const-string v1, "db_loader_status"

    invoke-virtual {v0, v1}, LX/1gc;->a(Ljava/lang/String;)LX/1ge;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->s:LX/1ge;

    .line 111322
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->M:LX/1gb;

    const-string v1, "network_loader"

    invoke-virtual {v0, v1}, LX/1gb;->a(Ljava/lang/String;)LX/1gf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    .line 111323
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->N:LX/1gd;

    const-string v1, "collection_loader"

    invoke-virtual {v0, v1}, LX/1gd;->a(Ljava/lang/String;)LX/1gg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    .line 111324
    sget-object v0, LX/00a;->a:LX/00a;

    move-object v0, v0

    .line 111325
    invoke-virtual {v0}, LX/00a;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->A:Ljava/lang/String;

    .line 111326
    iput-boolean v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->B:Z

    .line 111327
    sget-object v0, LX/1KX;->a:LX/1KX;

    move-object v0, v0

    .line 111328
    invoke-virtual {v0, p0}, LX/1KX;->a(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111329
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    .line 111330
    iget-object v1, v0, LX/1gf;->a:LX/1ge;

    invoke-virtual {v1}, LX/1ge;->c()V

    .line 111331
    iget-object v1, v0, LX/1gf;->b:LX/1ge;

    invoke-virtual {v1}, LX/1ge;->c()V

    .line 111332
    iget-object v1, v0, LX/1gf;->c:LX/1ge;

    invoke-virtual {v1}, LX/1ge;->c()V

    .line 111333
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->s:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->c()V

    .line 111334
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    .line 111335
    iget-object v1, v0, LX/1gg;->a:LX/1ge;

    invoke-virtual {v1}, LX/1ge;->c()V

    .line 111336
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->U:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->c()V

    .line 111337
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->K:LX/1gZ;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, LX/0gD;->b:LX/0fz;

    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->s:LX/1ge;

    iget-object v4, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    iget-object v5, p0, LX/0gD;->m:LX/0qk;

    iget-object v6, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    .line 111338
    iget-object v7, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v8, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v7, v8}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111339
    new-instance v7, LX/1gh;

    invoke-direct {v7, p0}, LX/1gh;-><init>(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    move-object v7, v7

    .line 111340
    iget-object v8, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual/range {v0 .. v8}, LX/1gZ;->a(Landroid/os/Looper;LX/0fz;LX/1ge;LX/1gf;LX/0g3;LX/1gg;LX/1gh;Lcom/facebook/api/feedtype/FeedType;)LX/1gi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 111341
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 111342
    new-instance v1, LX/1gm;

    invoke-direct {v1, p0}, LX/1gm;-><init>(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    move-object v1, v1

    .line 111343
    iput-object v1, v0, LX/1gi;->x:LX/1gm;

    .line 111344
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "freshfeed_background_ui_work"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->q:Landroid/os/HandlerThread;

    .line 111345
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->q:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 111346
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->I:LX/1ga;

    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->q:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    iget-object v4, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    iget-object v5, p0, LX/0gD;->o:LX/0rB;

    invoke-virtual/range {v0 .. v5}, LX/1ga;->a(Landroid/os/Looper;LX/1gi;LX/1gf;Lcom/facebook/api/feedtype/FeedType;LX/0rB;)LX/0jY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    .line 111347
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->J:LX/1gY;

    iget-object v1, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    invoke-virtual {v0, v1, v2, v3}, LX/1gY;->a(Lcom/facebook/api/feedtype/FeedType;LX/1gi;LX/0jY;)LX/1gp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    .line 111348
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    .line 111349
    iput-object v1, v0, LX/0jY;->n:LX/1gp;

    .line 111350
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(I)V

    goto/16 :goto_0
.end method

.method private W()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 111351
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v2, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111352
    const-string v1, "FreshFeedDataLoader.doInitialize"

    const v2, -0x3d11aecf

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111353
    :try_start_0
    iget-object v1, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v1}, LX/0r0;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v1}, LX/0r0;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 111354
    :cond_0
    const v0, -0x278edfb3

    invoke-static {v0}, LX/02m;->a(I)V

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 111355
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v1}, LX/0r0;->b()V

    .line 111356
    iget-boolean v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->y:Z

    if-nez v1, :cond_2

    .line 111357
    sget-object v1, LX/0gf;->INITIALIZATION:LX/0gf;

    invoke-static {p0, v1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->c(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;LX/0gf;)V

    .line 111358
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->y:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111359
    :cond_2
    const v1, -0x6205e59b

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x750549fc

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private X()V
    .locals 11

    .prologue
    .line 111360
    const-string v0, "FreshFeedDataLoader.loadOlderData"

    const v1, -0x17fb3281

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111361
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    invoke-virtual {v0}, LX/1gf;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 111362
    const v0, -0x357631b6    # -4515621.0f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111363
    :goto_0
    return-void

    .line 111364
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18E;

    invoke-virtual {v0}, LX/18E;->b()V

    .line 111365
    iget-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->B:Z

    if-eqz v0, :cond_1

    .line 111366
    sget-object v0, LX/00a;->a:LX/00a;

    move-object v0, v0

    .line 111367
    invoke-virtual {v0}, LX/00a;->i()Ljava/lang/String;

    move-result-object v9

    .line 111368
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->B:Z

    .line 111369
    new-instance v1, LX/1J0;

    sget-object v2, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    iget-object v4, p0, LX/0gD;->o:LX/0rB;

    const-string v5, "oneway_feed_older_data_fetch"

    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->G:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    sget-object v10, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    invoke-direct/range {v1 .. v10}, LX/1J0;-><init>(LX/0rS;Lcom/facebook/api/feedtype/FeedType;LX/0rB;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;LX/0gf;)V

    iput-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->p:LX/1J0;

    .line 111370
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->s:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->a()V

    .line 111371
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    invoke-virtual {v0}, LX/1gg;->b()V

    .line 111372
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    sget-object v1, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    invoke-virtual {v0, v1}, LX/0qk;->a(LX/0gf;)V

    .line 111373
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    sget-object v2, LX/1gs;->LOAD_OLDER_DATA_FROM_NETWORK:LX/1gs;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V

    .line 111374
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->p:LX/1J0;

    .line 111375
    invoke-static {v0}, LX/1gp;->b(LX/1gp;)V

    .line 111376
    iget-object v2, v0, LX/1gp;->g:LX/1gu;

    .line 111377
    const/4 v0, 0x2

    invoke-virtual {v2, v0, v1}, LX/1gu;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1gu;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111378
    const v0, -0x77dcfcbf

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 111379
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->j()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v9

    goto :goto_1

    .line 111380
    :catchall_0
    move-exception v0

    const v1, -0x3c474b38

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static Y(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V
    .locals 3

    .prologue
    .line 111381
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "PTR Rerank stories"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111382
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->R:LX/0ad;

    sget-short v1, LX/0fe;->ag:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->b(I)V

    .line 111383
    return-void

    .line 111384
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private Z()V
    .locals 2

    .prologue
    .line 111385
    const-string v0, "FreshFeedDataLoader.fetchFreshStoriesIfNeeded"

    const v1, -0x67d067b0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111386
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    .line 111387
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/0jY;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jY;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111388
    const v0, -0xa0bfe6c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111389
    return-void

    .line 111390
    :catchall_0
    move-exception v0

    const v1, -0x2772d1a3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 111391
    const-string v0, "FreshFeedDataLoader.loadDataFromCache"

    const v1, 0x337aa6e7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111392
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->s:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111393
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "Already loading data from cache"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111394
    const v0, 0x1ffc0e87

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111395
    :goto_0
    return-void

    .line 111396
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18E;

    invoke-virtual {v0}, LX/18E;->b()V

    .line 111397
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->s:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->a()V

    .line 111398
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    sget-object v2, LX/1gs;->LOAD_DATA_FROM_CACHE:LX/1gs;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V

    .line 111399
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    invoke-virtual {v0, p1}, LX/1gp;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111400
    const v0, -0x78da8f8e

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x15b14f65

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private a(II)V
    .locals 5

    .prologue
    .line 111401
    const-string v0, "FreshFeedDataLoader.getStoriesForFeed"

    const v1, 0x41934473

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111402
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    invoke-virtual {v0}, LX/1gg;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 111403
    const v0, -0x7a9e03a7

    invoke-static {v0}, LX/02m;->a(I)V

    :goto_0
    return-void

    .line 111404
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    invoke-virtual {v0}, LX/1gg;->b()V

    .line 111405
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    sget-object v2, LX/1gs;->STORIES_FOR_FEED:LX/1gs;

    const-string v3, "numStories"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 111406
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    .line 111407
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, LX/0jY;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jY;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111408
    const v0, -0x152bcde3

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x3130ed51

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;",
            "LX/0Ot",
            "<",
            "LX/1jj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/18E;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 111409
    iput-object p1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->E:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->F:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;Ljava/lang/Runnable;J)V
    .locals 4

    .prologue
    .line 111410
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Q:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->P:LX/0Tf;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, p1, p2, p3, v2}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 111411
    return-void
.end method

.method private aa()J
    .locals 7

    .prologue
    .line 111412
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->O:LX/0pJ;

    const/16 v2, 0x9

    .line 111413
    sget-wide v4, LX/0X5;->df:J

    const/4 v6, 0x1

    invoke-virtual {v1, v4, v5, v6, v2}, LX/0pK;->a(JII)I

    move-result v4

    move v1, v4

    .line 111414
    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static ac(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)I
    .locals 5

    .prologue
    .line 111415
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->O:LX/0pJ;

    const/4 v1, 0x3

    .line 111416
    sget-wide v2, LX/0X5;->dj:J

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v4, v1}, LX/0pK;->a(JII)I

    move-result v2

    move v0, v2

    .line 111417
    return v0
.end method

.method private static ad(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)I
    .locals 8

    .prologue
    .line 111418
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jj;

    const/4 v1, 0x5

    .line 111419
    iget-object v2, v0, LX/1jj;->a:LX/0uf;

    sget-wide v4, LX/0X5;->dw:J

    int-to-long v6, v1

    invoke-virtual {v2, v4, v5, v6, v7}, LX/0uf;->a(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    move v0, v2

    .line 111420
    return v0
.end method

.method public static ae(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V
    .locals 3

    .prologue
    .line 111431
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v0}, LX/0r0;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111432
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    .line 111433
    sget-object v1, LX/0r1;->INITIALIZED:LX/0r1;

    iput-object v1, v0, LX/0r0;->a:LX/0r1;

    .line 111434
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "Initialization Status Finished"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111435
    :cond_0
    return-void
.end method

.method public static ah(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V
    .locals 9

    .prologue
    .line 111421
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->G:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 111422
    sget-object v2, LX/1KY;->a:LX/1KY;

    move-object v2, v2

    .line 111423
    iget-boolean v3, v2, LX/1KY;->b:Z

    move v2, v3

    .line 111424
    if-eqz v2, :cond_0

    .line 111425
    sget-object v2, LX/1KY;->a:LX/1KY;

    move-object v2, v2

    .line 111426
    invoke-direct {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->aa()J

    move-result-wide v4

    .line 111427
    iput-wide v4, v2, LX/1KY;->c:J

    .line 111428
    const-wide/16 v6, 0x0

    iput-wide v6, v2, LX/1KY;->d:J

    .line 111429
    :cond_0
    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->S:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0pP;->f:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 111430
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;
    .locals 17

    .prologue
    .line 111551
    new-instance v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const-class v3, LX/1gY;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1gY;

    invoke-static/range {p0 .. p0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v4

    check-cast v4, LX/0pV;

    const-class v5, LX/1gZ;

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1gZ;

    const-class v6, LX/1ga;

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1ga;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v8

    check-cast v8, LX/0pJ;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, LX/0Tf;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v12

    check-cast v12, LX/0Sh;

    const-class v13, LX/1gb;

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/1gb;

    const-class v14, LX/1gc;

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/1gc;

    const-class v15, LX/1gd;

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/1gd;

    invoke-static/range {p0 .. p0}, LX/1EU;->a(LX/0QB;)LX/1EU;

    move-result-object v16

    check-cast v16, LX/1EU;

    invoke-direct/range {v1 .. v16}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;-><init>(Landroid/content/Context;LX/1gY;LX/0pV;LX/1gZ;LX/1ga;LX/0SG;LX/0pJ;LX/0Tf;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/1gb;LX/1gc;LX/1gd;LX/1EU;)V

    .line 111552
    const/16 v2, 0xe0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x698

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;LX/0Ot;LX/0Ot;)V

    .line 111553
    return-object v1
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 111542
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111543
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->m()V

    .line 111544
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    invoke-virtual {v0}, LX/0qk;->a()V

    .line 111545
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    .line 111546
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, LX/0jY;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jY;->sendMessage(Landroid/os/Message;)Z

    .line 111547
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    invoke-virtual {v0}, LX/0jY;->c()V

    .line 111548
    invoke-direct {p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(I)V

    .line 111549
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Z:I

    .line 111550
    return-void
.end method

.method private b(LX/0gf;)V
    .locals 11

    .prologue
    .line 111527
    const-string v0, "FreshFeedDataLoader.loadNewDataFromNetwork"

    const v1, -0x498530d7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111528
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    invoke-virtual {v0}, LX/1gf;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 111529
    const v0, -0x35c426a0    # -3077720.0f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111530
    :goto_0
    return-void

    .line 111531
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18E;

    invoke-virtual {v0}, LX/18E;->b()V

    .line 111532
    new-instance v1, LX/1J0;

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    iget-object v4, p0, LX/0gD;->o:LX/0rB;

    const-string v5, "fresh_feed_new_data_fetch"

    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->G:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v10, p1

    invoke-direct/range {v1 .. v10}, LX/1J0;-><init>(LX/0rS;Lcom/facebook/api/feedtype/FeedType;LX/0rB;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;LX/0gf;)V

    iput-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->p:LX/1J0;

    .line 111533
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    .line 111534
    iget-object v1, v0, LX/1gf;->a:LX/1ge;

    invoke-virtual {v1}, LX/1ge;->a()V

    .line 111535
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    invoke-virtual {v0, p1}, LX/0qk;->a(LX/0gf;)V

    .line 111536
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    sget-object v2, LX/1gs;->LOAD_NEW_DATA_FROM_NETWORK:LX/1gs;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V

    .line 111537
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->p:LX/1J0;

    invoke-virtual {v0, v1}, LX/1gp;->a(LX/1J0;)V

    .line 111538
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->R:LX/0ad;

    sget-short v1, LX/0fe;->Y:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111539
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    .line 111540
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, LX/0jY;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jY;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111541
    :cond_1
    const v0, -0x701311eb

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x127343dc

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static c(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;LX/0gf;)V
    .locals 13

    .prologue
    .line 111505
    const-string v0, "FreshFeedDataLoader.startNewSession"

    const v1, -0x166c79c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111506
    :try_start_0
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111507
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->x:Z

    .line 111508
    invoke-virtual {p1}, LX/0gf;->needsReranking()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->v:Z

    .line 111509
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    sget-object v2, LX/1gs;->START_NEW_SESSION:LX/1gs;

    const-string v3, "fetchFeedCause"

    invoke-virtual {p1}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 111510
    sget-object v0, LX/0gf;->RERANK:LX/0gf;

    if-eq p1, v0, :cond_0

    .line 111511
    invoke-direct {p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->b(LX/0gf;)V

    .line 111512
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->v:Z

    if-eqz v0, :cond_1

    .line 111513
    sget-object v5, LX/0gf;->RERANK:LX/0gf;

    if-ne p1, v5, :cond_2

    const-wide/16 v5, 0x0

    .line 111514
    :goto_0
    iget-object v7, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v8, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v9, "Scheduling reranking"

    invoke-virtual {v7, v8, v9}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111515
    new-instance v7, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader$3;

    invoke-direct {v7, p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader$3;-><init>(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    invoke-static {p0, v7, v5, v6}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;Ljava/lang/Runnable;J)V

    .line 111516
    :cond_1
    sget-object v0, LX/00a;->a:LX/00a;

    move-object v0, v0

    .line 111517
    invoke-virtual {v0}, LX/00a;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->A:Ljava/lang/String;

    .line 111518
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->B:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111519
    const v0, -0x6b25c539

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111520
    return-void

    .line 111521
    :catchall_0
    move-exception v0

    const v1, -0x535f2d52

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 111522
    :cond_2
    iget-object v5, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->O:LX/0pJ;

    const/4 v6, 0x5

    .line 111523
    sget-wide v10, LX/0X5;->dg:J

    const/4 v12, 0x2

    invoke-virtual {v5, v10, v11, v12, v6}, LX/0pK;->a(JII)I

    move-result v10

    move v5, v10

    .line 111524
    int-to-long v5, v5

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V
    .locals 7

    .prologue
    .line 111554
    const-string v0, "FreshFeedDataLoader.addInitialStoriesToFeedUnitCollection"

    const v1, 0x3ca6cf38

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111555
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    sget-object v2, LX/1gs;->INITIALIZED:LX/1gs;

    const-string v3, "resultType"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "numStories"

    invoke-direct {p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->f(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111556
    invoke-direct {p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->f(I)I

    move-result v0

    invoke-direct {p0, v0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111557
    const v0, -0x4f604a30

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111558
    return-void

    .line 111559
    :catchall_0
    move-exception v0

    const v1, 0x744e5af5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private f(I)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 111498
    if-eqz p1, :cond_0

    const/4 v1, 0x6

    if-eq p1, v1, :cond_0

    const/4 v1, 0x7

    if-ne p1, v1, :cond_5

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 111499
    if-eqz v1, :cond_2

    .line 111500
    :cond_1
    :goto_1
    return v0

    .line 111501
    :cond_2
    const/16 v1, 0xa

    if-eq p1, v1, :cond_3

    const/16 v1, 0x9

    if-ne p1, v1, :cond_6

    :cond_3
    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 111502
    if-eqz v1, :cond_4

    .line 111503
    iget-object v1, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v1}, LX/0fz;->size()I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    iget-object v1, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v1}, LX/0fz;->size()I

    move-result v1

    rsub-int/lit8 v1, v1, 0x3

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1

    .line 111504
    :cond_4
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ac(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)I

    move-result v0

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public static h(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V
    .locals 8

    .prologue
    const/16 v7, 0x9

    const/4 v0, 0x1

    const/16 v6, 0xa

    const/4 v1, 0x0

    .line 111450
    const-string v2, "FreshFeedCollectionListener.onStoriesAdded"

    const v3, -0x45932e3c

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111451
    :try_start_0
    iget v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Z:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Z:I

    .line 111452
    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->U:LX/1ge;

    invoke-virtual {v2}, LX/1ge;->c()V

    .line 111453
    invoke-static {p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->l(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V

    .line 111454
    iget-boolean v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->v:Z

    if-eqz v2, :cond_2

    .line 111455
    if-eqz p1, :cond_0

    const/4 v2, 0x5

    if-eq p1, v2, :cond_0

    const/16 v2, 0xa

    if-ne p1, v2, :cond_d

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 111456
    if-eqz v2, :cond_2

    .line 111457
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 111458
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, LX/1gi;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1gi;->sendMessage(Landroid/os/Message;)Z

    .line 111459
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->v:Z

    .line 111460
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->x:Z

    .line 111461
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    invoke-virtual {v1}, LX/0jY;->c()V

    .line 111462
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, LX/1gp;->a(I)V

    .line 111463
    const/4 v1, 0x5

    if-ne p1, v1, :cond_1

    .line 111464
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->w:Z

    .line 111465
    :cond_1
    :goto_1
    invoke-static {p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->j(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)J

    move-result-wide v2

    .line 111466
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_8

    .line 111467
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v4, "scheduleDelayedCachedStoriesInsert"

    invoke-virtual {v0, v1, v4}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111468
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->U:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->a()V

    .line 111469
    new-instance v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader$4;

    invoke-direct {v0, p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader$4;-><init>(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V

    invoke-static {p0, v0, v2, v3}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;Ljava/lang/Runnable;J)V

    .line 111470
    :goto_2
    invoke-virtual {p0}, LX/0gD;->H()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111471
    const v0, 0x6231c2dc

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111472
    return-void

    .line 111473
    :cond_2
    :try_start_1
    iget-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->w:Z

    if-eqz v0, :cond_4

    if-eqz p1, :cond_3

    if-ne p1, v6, :cond_4

    .line 111474
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->w:Z

    .line 111475
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->x:Z

    .line 111476
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 111477
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, LX/1gi;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1gi;->sendMessage(Landroid/os/Message;)Z

    .line 111478
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    invoke-virtual {v0}, LX/0jY;->c()V

    move v0, v1

    goto :goto_1

    .line 111479
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->x:Z

    if-eqz v0, :cond_6

    if-eqz p1, :cond_5

    if-ne p1, v6, :cond_6

    .line 111480
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->x:Z

    .line 111481
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    invoke-virtual {v0}, LX/0jY;->c()V

    move v0, v1

    goto :goto_1

    .line 111482
    :cond_6
    if-ne p1, v7, :cond_7

    .line 111483
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->x:Z

    .line 111484
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    invoke-virtual {v0}, LX/0jY;->c()V

    .line 111485
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 111486
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, LX/1gi;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1gi;->sendMessage(Landroid/os/Message;)Z

    .line 111487
    :cond_7
    move v0, v1

    goto :goto_1

    .line 111488
    :cond_8
    if-ne p1, v7, :cond_9

    .line 111489
    invoke-static {p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->c$redex0(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 111490
    :catchall_0
    move-exception v0

    const v1, 0x4d062196    # 1.40646752E8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 111491
    :cond_9
    if-nez v0, :cond_a

    :try_start_2
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ac(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)I

    move-result v1

    if-lt v0, v1, :cond_a

    if-eqz p1, :cond_a

    if-ne p1, v6, :cond_b

    .line 111492
    :cond_a
    invoke-static {p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->c$redex0(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V

    .line 111493
    :goto_3
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ae(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    goto :goto_2

    .line 111494
    :cond_b
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    invoke-virtual {v0}, LX/0qk;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 111495
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "Getting stories because isAtBottom:true"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111496
    invoke-static {p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->k(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 111497
    :cond_c
    goto :goto_3

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private static j(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)J
    .locals 8

    .prologue
    .line 111447
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    invoke-virtual {v0}, LX/1gf;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111448
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ad(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)I

    move-result v0

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->G:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->aa:J

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 111449
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V
    .locals 5

    .prologue
    .line 111443
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->O:LX/0pJ;

    const/4 v1, 0x1

    .line 111444
    sget-wide v2, LX/0X5;->di:J

    const/4 v4, 0x4

    invoke-virtual {v0, v2, v3, v4, v1}, LX/0pK;->a(JII)I

    move-result v2

    move v0, v2

    .line 111445
    invoke-direct {p0, v0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(II)V

    .line 111446
    return-void
.end method

.method public static l(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V
    .locals 3

    .prologue
    .line 111437
    if-eqz p1, :cond_0

    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-ne p1, v0, :cond_1

    .line 111438
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "cancelScheduledRerank"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111439
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Q:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YG;

    .line 111440
    if-eqz v0, :cond_1

    .line 111441
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0YG;->cancel(Z)Z

    .line 111442
    :cond_1
    return-void
.end method


# virtual methods
.method public final A()I
    .locals 1

    .prologue
    .line 111436
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ac(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)I

    move-result v0

    return v0
.end method

.method public final B()LX/1EM;
    .locals 1

    .prologue
    .line 111273
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Y:LX/1EM;

    return-object v0
.end method

.method public final C()V
    .locals 3

    .prologue
    .line 111274
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111275
    const-string v0, "FreshFeedDataLoader.onUserLeftFeed"

    const v1, -0x482307c5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111276
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "onUserLeftFeed"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111277
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ah(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111278
    sget-object v0, LX/1KX;->a:LX/1KX;

    move-object v0, v0

    .line 111279
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1KX;->a(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111280
    const v0, -0x7bb4f1c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111281
    return-void

    .line 111282
    :catchall_0
    move-exception v0

    const v1, 0x7da8770b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final D()V
    .locals 1

    .prologue
    .line 111209
    sget-object v0, LX/1KX;->a:LX/1KX;

    move-object v0, v0

    .line 111210
    invoke-virtual {v0, p0}, LX/1KX;->a(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111211
    return-void
.end method

.method public final F()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 111208
    sget-object v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->D:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final G()V
    .locals 8

    .prologue
    .line 111199
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111200
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "stopFeedAutoRefresh"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111201
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Scheduling tearDown in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->aa()J

    move-result-wide v4

    const-wide/32 v6, 0xea60

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111202
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->z:LX/0YG;

    if-eqz v0, :cond_0

    .line 111203
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->z:LX/0YG;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0YG;->cancel(Z)Z

    .line 111204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->z:LX/0YG;

    .line 111205
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->P:LX/0Tf;

    new-instance v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader$2;

    invoke-direct {v1, p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader$2;-><init>(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    invoke-direct {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->aa()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->z:LX/0YG;

    .line 111206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->w:Z

    .line 111207
    return-void
.end method

.method public final O()V
    .locals 1

    .prologue
    .line 111196
    invoke-virtual {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->P()V

    .line 111197
    sget-object v0, LX/0gf;->WARM_START:LX/0gf;

    invoke-static {p0, v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->c(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;LX/0gf;)V

    .line 111198
    return-void
.end method

.method public final P()V
    .locals 3

    .prologue
    .line 111192
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "loadCacheForWarmStart"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111193
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->G:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->aa:J

    .line 111194
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->b(I)V

    .line 111195
    return-void
.end method

.method public final a(LX/0rS;LX/0gf;Z)LX/0uO;
    .locals 5

    .prologue
    .line 111171
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111172
    const-string v0, "FreshFeedDataLoader.loadAfterData"

    const v1, -0x5f2c0f08

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111173
    :try_start_0
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111174
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ah(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111175
    iget-object v0, p0, LX/0gD;->c:LX/0qy;

    .line 111176
    iget-boolean v1, v0, LX/0qy;->b:Z

    move v0, v1

    .line 111177
    if-eqz v0, :cond_0

    .line 111178
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    sget-object v2, LX/1gs;->END_OF_FEED:LX/1gs;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V

    .line 111179
    sget-object v0, LX/0uO;->END_OF_FEED:LX/0uO;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111180
    const v1, -0x55423505

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 111181
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v0}, LX/0r0;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    invoke-virtual {v0}, LX/1gg;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->U:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111182
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "Aborting loadAfterData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    invoke-virtual {v4}, LX/1gg;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->U:LX/1ge;

    invoke-virtual {v4}, LX/1ge;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111183
    sget-object v0, LX/0uO;->ALREADY_SCHEDULED:LX/0uO;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111184
    const v1, 0x4f2a8293

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 111185
    :cond_2
    const/4 v0, 0x1

    :try_start_2
    invoke-static {p0, v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->k(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V

    .line 111186
    invoke-direct {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Z()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 111187
    :goto_1
    :try_start_3
    sget-object v0, LX/0uO;->SUCCESS:LX/0uO;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 111188
    const v1, 0x40f9ca5a

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 111189
    :catch_0
    move-exception v0

    .line 111190
    :try_start_4
    const-string v1, "FreshFeed"

    const-string v2, "Error in loadAfterData."

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 111191
    :catchall_0
    move-exception v0

    const v1, -0x7ca36ef2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/feed/model/ClientFeedUnitEdge;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 111157
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    if-nez v0, :cond_0

    .line 111158
    const/4 v0, 0x0

    .line 111159
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    .line 111160
    invoke-static {v0}, LX/0jY;->n(LX/0jY;)LX/1jW;

    move-result-object v1

    const/4 v3, 0x0

    .line 111161
    iget-object v2, v1, LX/1jW;->e:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object v2, v3

    .line 111162
    :goto_1
    move-object v1, v2

    .line 111163
    move-object v0, v1

    .line 111164
    goto :goto_0

    .line 111165
    :cond_1
    iget-object v2, v1, LX/1jW;->a:Ljava/util/ArrayList;

    iget-object v4, v1, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 111166
    array-length p0, v2

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, p0, :cond_3

    aget-object v4, v2, v5

    .line 111167
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v2, v4

    .line 111168
    goto :goto_1

    .line 111169
    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    :cond_3
    move-object v2, v3

    .line 111170
    goto :goto_1
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 111151
    const-string v0, "FreshFeedDataLoader.addOrUpdateEdges"

    const v1, 0x64bb625a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111152
    :try_start_0
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 111153
    const v0, -0x7dc9d8a3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111154
    :goto_0
    return-void

    .line 111155
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    iget-object v1, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0, p1, v1}, LX/1gp;->a(LX/0Px;Lcom/facebook/api/feedtype/FeedType;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111156
    const v0, 0x305ac848

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x2732c154

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/0Px;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 111134
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v2, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111135
    const-string v1, "FreshFeedDataLoader.unStageStories"

    const v2, -0x17a0b821

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111136
    :try_start_0
    iget-object v1, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v1}, LX/0r0;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v1}, LX/0r0;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 111137
    :cond_0
    const v0, 0x64c5a7db

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111138
    :goto_0
    return-void

    .line 111139
    :cond_1
    :try_start_1
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111140
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v2, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v3, "unStageStories"

    invoke-virtual {v1, v2, v3}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111141
    if-nez p2, :cond_2

    .line 111142
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->v:Z

    .line 111143
    :cond_2
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->R:LX/0ad;

    sget-short v2, LX/0fe;->P:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 111144
    if-eqz p2, :cond_4

    const/16 v0, 0x9

    .line 111145
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    .line 111146
    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3, p1}, LX/0jY;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jY;->sendMessage(Landroid/os/Message;)Z

    .line 111147
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ah(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111148
    const v0, 0x47a8351d

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 111149
    :cond_4
    if-eqz v1, :cond_3

    const/16 v0, 0xa

    goto :goto_1

    .line 111150
    :catchall_0
    move-exception v0

    const v1, 0x7afbfccd

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/0g3;)V
    .locals 2

    .prologue
    .line 111125
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111126
    const-string v0, "FreshFeedDataLoader.setFragmentLoaderListener"

    const v1, -0x54384103

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111127
    :try_start_0
    invoke-super {p0, p1}, LX/0gD;->a(LX/0g3;)V

    .line 111128
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111129
    invoke-virtual {p0}, LX/0gD;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111130
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->c$redex0(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111131
    :cond_0
    const v0, 0x75ec2b0f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111132
    return-void

    .line 111133
    :catchall_0
    move-exception v0

    const v1, 0x2f1a87c3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/1EM;)V
    .locals 2

    .prologue
    .line 111267
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111268
    const/4 v0, 0x0

    .line 111269
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->O:LX/0pJ;

    invoke-virtual {v1, v0}, LX/0pJ;->a(Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->V:LX/1EU;

    invoke-virtual {v1}, LX/1EU;->b()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->V:LX/1EU;

    invoke-virtual {v1}, LX/1EU;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 111270
    if-eqz v0, :cond_2

    .line 111271
    iput-object p1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->Y:LX/1EM;

    .line 111272
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/api/feedtype/FeedType;)V
    .locals 1

    .prologue
    .line 111120
    invoke-super {p0, p1}, LX/0gD;->a(Lcom/facebook/api/feedtype/FeedType;)V

    .line 111121
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    if-eqz v0, :cond_0

    .line 111122
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 111123
    iput-object p1, v0, LX/1gi;->u:Lcom/facebook/api/feedtype/FeedType;

    .line 111124
    :cond_0
    return-void
.end method

.method public final a(LX/0gf;)Z
    .locals 5

    .prologue
    .line 111106
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111107
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ah(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111108
    sget-object v0, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    if-ne p1, v0, :cond_2

    .line 111109
    :try_start_0
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111110
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v0}, LX/0r0;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    invoke-virtual {v0}, LX/1gg;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->U:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111111
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "Aborting loadBeforeData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->u:LX/1gg;

    invoke-virtual {v4}, LX/1gg;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->U:LX/1ge;

    invoke-virtual {v4}, LX/1ge;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111112
    const/4 v0, 0x0

    .line 111113
    :goto_0
    return v0

    .line 111114
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 111115
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 111116
    :catch_0
    move-exception v0

    .line 111117
    const-string v1, "FreshFeed"

    const-string v2, "Error in loadBeforeData."

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 111118
    :cond_2
    invoke-static {p0, p1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->c(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;LX/0gf;)V

    .line 111119
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    invoke-virtual {v0}, LX/1gf;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/loader/FeedDataLoaderInitializationParams;)Z
    .locals 2

    .prologue
    .line 111104
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111105
    invoke-direct {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W()Z

    move-result v0

    return v0
.end method

.method public final b(LX/0g3;)V
    .locals 2

    .prologue
    .line 111101
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111102
    invoke-super {p0, p1}, LX/0gD;->b(LX/0g3;)V

    .line 111103
    return-void
.end method

.method public final b(LX/0rS;LX/0gf;Z)V
    .locals 0

    .prologue
    .line 111212
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 111213
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111214
    const-string v0, "FreshFeedDataLoader.onFragmentDetached"

    const v1, 0x693a8fa4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111215
    :try_start_0
    invoke-super {p0}, LX/0gD;->g()V

    .line 111216
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ah(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111217
    const v0, 0x6e80984f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111218
    return-void

    .line 111219
    :catchall_0
    move-exception v0

    const v1, 0x673f1772

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final i()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 111220
    invoke-virtual {p0}, LX/0gD;->I()Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 111221
    const/4 v0, 0x0

    return v0
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 111222
    return-void
.end method

.method public final l()Z
    .locals 14

    .prologue
    .line 111223
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111224
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111225
    iget-object v10, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->S:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v11, LX/0pP;->e:LX/0Tn;

    const-wide/16 v12, 0x0

    invoke-interface {v10, v11, v12, v13}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v10

    move-wide v4, v10

    .line 111226
    iget-object v6, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->S:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/0pP;->f:LX/0Tn;

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 111227
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    move-wide v0, v4

    .line 111228
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->G:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-direct {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->aa()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 111229
    invoke-virtual {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->O()V

    .line 111230
    const/4 v0, 0x1

    .line 111231
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 111232
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111233
    const-string v0, "FreshFeedDataLoader.forceRefresh"

    const v1, 0x5a95a89a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111234
    :try_start_0
    sget-object v0, LX/0gf;->AUTO_REFRESH:LX/0gf;

    invoke-static {p0, v0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->c(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;LX/0gf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111235
    const v0, -0x41302963

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111236
    return-void

    .line 111237
    :catchall_0
    move-exception v0

    const v1, 0x3f33c406

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final n()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 111238
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v2, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111239
    const-string v1, "FreshFeedDataLoader.doEagerInit"

    const v2, 0xcbdaa86

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111240
    :try_start_0
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ah(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111241
    iget-boolean v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->y:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 111242
    const v0, 0x94335b1

    invoke-static {v0}, LX/02m;->a(I)V

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 111243
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->G:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->aa:J

    .line 111244
    sget-object v1, LX/0gf;->INITIALIZATION:LX/0gf;

    invoke-static {p0, v1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->c(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;LX/0gf;)V

    .line 111245
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->y:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111246
    const v1, 0x1144a5ac

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x18b327a0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 111247
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111248
    const/4 v0, 0x0

    return v0
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 111249
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111250
    const-string v0, "FreshFeedDataLoader.onUserLeftApp"

    const v1, -0x40e42783

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 111251
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v2, "onUserLeftApp"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111252
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ah(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 111253
    invoke-virtual {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->G()V

    .line 111254
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18E;

    invoke-virtual {v0}, LX/18E;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111255
    const v0, -0x318769b4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 111256
    return-void

    .line 111257
    :catchall_0
    move-exception v0

    const v1, 0xb36c403

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 111258
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111259
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    invoke-virtual {v0}, LX/1gf;->c()Z

    move-result v0

    return v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 111260
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111261
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    invoke-virtual {v0}, LX/1gf;->d()Z

    move-result v0

    return v0
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 111262
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111263
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->s:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    invoke-virtual {v0}, LX/1gf;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 111264
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->T:LX/0Sh;

    const-string v1, "FreshFeedDataLoader must be called on UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 111265
    iget-object v0, p0, LX/0gD;->c:LX/0qy;

    invoke-virtual {v0}, LX/0qy;->d()V

    .line 111266
    return-void
.end method
