.class public Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Tf;

.field private final d:LX/0Wd;

.field public final e:LX/1ji;

.field public final f:LX/1jW;

.field private final g:LX/0pJ;

.field public final h:LX/1fA;

.field public final i:LX/0ad;

.field public final j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Tf;LX/0Wd;LX/1ji;LX/1jW;LX/0pJ;LX/1fA;LX/0ad;LX/0Ot;)V
    .locals 2
    .param p1    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p2    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # LX/1jW;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tf;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/1ji;",
            "LX/1jW;",
            "LX/0pJ;",
            "LX/1fA;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 123891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123892
    iput-object p1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->c:LX/0Tf;

    .line 123893
    iput-object p2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->d:LX/0Wd;

    .line 123894
    iput-object p3, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->e:LX/1ji;

    .line 123895
    iput-object p4, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->f:LX/1jW;

    .line 123896
    iput-object p5, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->g:LX/0pJ;

    .line 123897
    iput-object p6, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->h:LX/1fA;

    .line 123898
    iput-object p7, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->i:LX/0ad;

    .line 123899
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->j:Ljava/util/HashMap;

    .line 123900
    iput-object p8, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->k:LX/0Ot;

    .line 123901
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x19

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b:Ljava/util/List;

    .line 123902
    return-void
.end method

.method public static c(Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;)V
    .locals 1

    .prologue
    .line 123888
    invoke-direct {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->d()V

    .line 123889
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->d:LX/0Wd;

    invoke-interface {v0, p0}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 123890
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 123903
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 123904
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 123905
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 123906
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 123824
    invoke-direct {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->d()V

    .line 123825
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->c:LX/0Tf;

    new-instance v1, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer$1;

    invoke-direct {v1, p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer$1;-><init>(Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;)V

    const v2, -0x12de178e

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 123826
    return-void
.end method

.method public final run()V
    .locals 9

    .prologue
    .line 123827
    const-string v0, "FreshFeedMultiRowStoryPreparer.run"

    const v1, 0x2e23ebaa

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 123828
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->e:LX/1ji;

    invoke-virtual {v0}, LX/1ji;->a()LX/1kA;

    move-result-object v0

    .line 123829
    if-nez v0, :cond_0

    .line 123830
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->c(Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123831
    const v0, -0xaa95f27

    invoke-static {v0}, LX/02m;->a(I)V

    .line 123832
    :goto_0
    return-void

    .line 123833
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->i:LX/0ad;

    sget-short v2, LX/0fe;->ap:S

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123834
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1kA;->j:Z

    .line 123835
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->g:LX/0pJ;

    const/4 v2, 0x5

    .line 123836
    sget-wide v4, LX/0X5;->dh:J

    const/4 v6, 0x3

    invoke-virtual {v1, v4, v5, v6, v2}, LX/0pK;->a(JII)I

    move-result v4

    move v1, v4

    .line 123837
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 123838
    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->f:LX/1jW;

    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b:Ljava/util/List;

    invoke-virtual {v2, v1, v3}, LX/1jW;->a(ILjava/util/List;)V

    .line 123839
    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->i:LX/0ad;

    sget-short v3, LX/0fe;->ae:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 123840
    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->f:LX/1jW;

    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b:Ljava/util/List;

    .line 123841
    const/4 v5, 0x0

    move v7, v5

    .line 123842
    :goto_1
    iget-object v5, v2, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v7, v5, :cond_3

    .line 123843
    iget-object v5, v2, LX/1jW;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 123844
    iget-boolean v8, v5, Lcom/facebook/feed/model/ClientFeedUnitEdge;->B:Z

    move v8, v8

    .line 123845
    if-nez v8, :cond_2

    .line 123846
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123847
    :cond_2
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    .line 123848
    goto :goto_1

    .line 123849
    :cond_3
    move v3, v4

    .line 123850
    :goto_2
    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_8

    .line 123851
    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 123852
    if-eqz v2, :cond_7

    .line 123853
    invoke-virtual {v0, v2}, LX/1kA;->c(Ljava/lang/Object;)LX/1RA;

    move-result-object v5

    .line 123854
    if-nez v5, :cond_7

    .line 123855
    invoke-virtual {v0, v2}, LX/1kA;->b(Ljava/lang/Object;)LX/1RA;

    move-result-object v5

    .line 123856
    iget-object v7, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->k:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/16u;

    invoke-virtual {v7, v2}, LX/16u;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 123857
    iget-object v7, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->j:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123858
    if-eqz v5, :cond_7

    move v4, v6

    .line 123859
    :cond_4
    :goto_3
    move v2, v4

    .line 123860
    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 123861
    move v1, v2

    .line 123862
    sget-object v2, LX/1KX;->a:LX/1KX;

    move-object v2, v2

    .line 123863
    iget-boolean v3, v2, LX/1KX;->e:Z

    move v2, v3

    .line 123864
    if-eqz v2, :cond_5

    .line 123865
    sget-object v2, LX/1KX;->a:LX/1KX;

    move-object v2, v2

    .line 123866
    iget-object v3, v0, LX/1kA;->f:LX/0aq;

    invoke-virtual {v3}, LX/0aq;->d()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    move-object v0, v3

    .line 123867
    iput-object v0, v2, LX/1KX;->k:Ljava/util/Set;

    .line 123868
    :cond_5
    if-eqz v1, :cond_6

    .line 123869
    invoke-static {p0}, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->c(Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123870
    :cond_6
    const v0, -0x50e00204

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x6fd01fb6

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 123871
    :cond_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_8
    move v5, v4

    .line 123872
    :goto_4
    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_4

    .line 123873
    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->b:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 123874
    if-eqz v2, :cond_a

    .line 123875
    invoke-virtual {v0, v2}, LX/1kA;->c(Ljava/lang/Object;)LX/1RA;

    move-result-object v7

    .line 123876
    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->j:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 123877
    if-nez v3, :cond_9

    .line 123878
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 123879
    :cond_9
    if-eqz v7, :cond_a

    .line 123880
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 123881
    if-ltz v8, :cond_b

    invoke-virtual {v7}, LX/1RA;->a()I

    move-result v1

    if-ge v8, v1, :cond_b

    .line 123882
    invoke-virtual {v7, v8}, LX/1RA;->f(I)Z

    move-result v1

    .line 123883
    :goto_5
    move v7, v1

    .line 123884
    iget-object v8, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedMultiRowStoryPreparer;->j:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123885
    if-eqz v7, :cond_a

    move v4, v6

    .line 123886
    goto/16 :goto_3

    .line 123887
    :cond_a
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_4

    :cond_b
    const/4 v1, 0x0

    goto :goto_5
.end method
