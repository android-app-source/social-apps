.class public Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    b = true
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final mMaxEventAgeSec:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "max_event_age_sec"
    .end annotation
.end field

.field public final mMaxEventQueueSize:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "max_event_queue_size"
    .end annotation
.end field

.field public final mMaxEventRecycleListSize:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "max_event_recycle_list_size"
    .end annotation
.end field

.field public final mShouldReportReaction:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "report_reaction"
    .end annotation
.end field

.field public final mShouldReportVideoPlay:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "report_video_play"
    .end annotation
.end field

.field public final mVersion:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "version"
    .end annotation
.end field

.field public final mVideoPlayCountThresholdSec:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_play_count_threshold_sec"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 166112
    const-class v0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfigDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166113
    const-class v0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

    sput-object v0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 166114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166115
    const-string v0, "0"

    iput-object v0, p0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mVersion:Ljava/lang/String;

    .line 166116
    const/16 v0, 0xc8

    iput v0, p0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mMaxEventQueueSize:I

    .line 166117
    const/16 v0, 0xe10

    iput v0, p0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mMaxEventAgeSec:I

    .line 166118
    const/16 v0, 0x12c

    iput v0, p0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mMaxEventRecycleListSize:I

    .line 166119
    iput-boolean v1, p0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mShouldReportReaction:Z

    .line 166120
    iput-boolean v1, p0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mShouldReportVideoPlay:Z

    .line 166121
    iput v1, p0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mVideoPlayCountThresholdSec:I

    .line 166122
    return-void
.end method

.method public static newBuilder()LX/6VN;
    .locals 1

    .prologue
    .line 166123
    new-instance v0, LX/6VN;

    invoke-direct {v0}, LX/6VN;-><init>()V

    return-object v0
.end method
