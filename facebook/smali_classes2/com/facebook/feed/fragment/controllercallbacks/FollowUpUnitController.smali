.class public Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;
.super LX/0hD;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0hk;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/1LA;

.field public final c:LX/1LB;

.field private final d:LX/0bH;

.field private final e:LX/1B1;

.field public f:Landroid/content/Context;

.field public g:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/api/feed/data/LegacyFeedUnitUpdater;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 233556
    const-class v0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1B1;LX/1LA;LX/1LB;LX/0bH;)V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 233557
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 233558
    iput-object p2, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->b:LX/1LA;

    .line 233559
    iput-object p3, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->c:LX/1LB;

    .line 233560
    iput-object p4, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->d:LX/0bH;

    .line 233561
    iput-object p1, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->e:LX/1B1;

    .line 233562
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->e:LX/1B1;

    new-instance v1, LX/1LE;

    invoke-direct {v1, p0}, LX/1LE;-><init>(Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 233563
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->e:LX/1B1;

    new-instance v1, LX/1LG;

    invoke-direct {v1, p0}, LX/1LG;-><init>(Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 233564
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->e:LX/1B1;

    new-instance v1, LX/1LH;

    invoke-direct {v1, p0}, LX/1LH;-><init>(Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 233565
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->e:LX/1B1;

    new-instance v1, LX/1LJ;

    invoke-direct {v1, p0}, LX/1LJ;-><init>(Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 233566
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->e:LX/1B1;

    new-instance v1, LX/1LL;

    invoke-direct {v1, p0}, LX/1LL;-><init>(Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 233567
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->e:LX/1B1;

    new-instance v1, LX/1LM;

    invoke-direct {v1, p0}, LX/1LM;-><init>(Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 233568
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->e:LX/1B1;

    new-instance v1, LX/1LO;

    invoke-direct {v1, p0}, LX/1LO;-><init>(Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 233569
    return-void
.end method

.method public static a$redex0(Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 233570
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 233571
    :goto_0
    return-object v0

    .line 233572
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->g:LX/1Iu;

    .line 233573
    iget-object v2, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 233574
    check-cast v0, LX/0qq;

    invoke-virtual {v0, p1}, LX/0qq;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 233575
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 233576
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 233577
    instance-of v3, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_1

    .line 233578
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 233579
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)V
    .locals 3
    .param p0    # Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 233580
    invoke-static {p0, p1}, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->a$redex0(Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 233581
    if-nez v0, :cond_0

    .line 233582
    :goto_0
    return-void

    .line 233583
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->b:LX/1LA;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    sget-object v2, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, p2, v2}, LX/1LA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 233584
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->e:LX/1B1;

    iget-object v1, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->d:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 233585
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->c:LX/1LB;

    .line 233586
    invoke-static {v0}, LX/1LB;->d(LX/1LB;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 233587
    :goto_0
    return-void

    .line 233588
    :cond_0
    iget-object v1, v0, LX/1LB;->a:LX/1L1;

    iget-object p0, v0, LX/1LB;->c:LX/1LC;

    invoke-virtual {v1, p0}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 233589
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->e:LX/1B1;

    iget-object v1, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->d:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 233590
    iget-object v0, p0, Lcom/facebook/feed/fragment/controllercallbacks/FollowUpUnitController;->c:LX/1LB;

    .line 233591
    invoke-static {v0}, LX/1LB;->d(LX/1LB;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 233592
    :goto_0
    return-void

    .line 233593
    :cond_0
    iget-object v1, v0, LX/1LB;->a:LX/1L1;

    iget-object p0, v0, LX/1LB;->c:LX/1LC;

    invoke-virtual {v1, p0}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_0
.end method
