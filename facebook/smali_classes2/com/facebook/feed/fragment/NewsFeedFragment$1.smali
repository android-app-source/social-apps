.class public final Lcom/facebook/feed/fragment/NewsFeedFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/fragment/NewsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V
    .locals 0

    .prologue
    .line 207645
    iput-object p1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 207646
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207647
    :goto_0
    return-void

    .line 207648
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v0}, LX/0g7;->n()Z

    move-result v0

    .line 207649
    if-nez v0, :cond_2

    .line 207650
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->x()V

    .line 207651
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->W:LX/1Fn;

    invoke-virtual {v0}, LX/1Fn;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, LX/0g7;->d(I)V

    .line 207652
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->N:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v1, v1, Lcom/facebook/feed/fragment/NewsFeedFragment;->O:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    const v4, -0x611c619c    # -2.4100069E-20f

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 207653
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 207654
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->I:LX/1KS;

    .line 207655
    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 207656
    const/4 v1, 0x1

    move v1, v1

    .line 207657
    if-eqz v1, :cond_3

    .line 207658
    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Km;

    .line 207659
    iget-object v2, v1, LX/1Km;->a:LX/1Kt;

    const/4 v3, 0x1

    iget-object v4, v1, LX/1Km;->f:LX/1DK;

    invoke-interface {v4}, LX/1DK;->a()LX/0g8;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1Kt;->a(ZLX/0g8;)V

    .line 207660
    :cond_3
    iget-object v1, v0, LX/1KS;->U:LX/0Ot;

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/1KS;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/1KS;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Z1;

    invoke-virtual {v1}, LX/0hD;->kw_()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 207661
    iget-object v1, v0, LX/1KS;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 207662
    :cond_4
    iget-object v1, v0, LX/1KS;->V:LX/0Ot;

    if-eqz v1, :cond_5

    iget-object v1, v0, LX/1KS;->V:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, v0, LX/1KS;->V:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;

    invoke-virtual {v1}, LX/0hD;->kw_()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 207663
    iget-object v1, v0, LX/1KS;->V:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 207664
    :cond_5
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 207665
    invoke-static {v0}, LX/0g2;->ah(LX/0g2;)Z

    .line 207666
    invoke-virtual {v0}, LX/0g2;->C()V

    .line 207667
    goto/16 :goto_0
.end method
