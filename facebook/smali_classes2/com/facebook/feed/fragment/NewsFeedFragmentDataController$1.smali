.class public final Lcom/facebook/feed/fragment/NewsFeedFragmentDataController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0g2;


# direct methods
.method public constructor <init>(LX/0g2;)V
    .locals 0

    .prologue
    .line 313012
    iput-object p1, p0, Lcom/facebook/feed/fragment/NewsFeedFragmentDataController$1;->a:LX/0g2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 313013
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragmentDataController$1;->a:LX/0g2;

    iget-object v0, v0, LX/0g2;->c:LX/0fz;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragmentDataController$1;->a:LX/0g2;

    iget-object v1, v1, LX/0g2;->I:LX/0oy;

    .line 313014
    iget v4, v1, LX/0oy;->q:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 313015
    iget-object v4, v1, LX/0oy;->a:LX/0W3;

    sget-wide v6, LX/0X5;->gH:J

    const/16 v5, 0x64

    invoke-interface {v4, v6, v7, v5}, LX/0W4;->a(JI)I

    move-result v4

    iput v4, v1, LX/0oy;->q:I

    .line 313016
    :cond_0
    iget v4, v1, LX/0oy;->q:I

    move v1, v4

    .line 313017
    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragmentDataController$1;->a:LX/0g2;

    iget-object v2, v2, LX/0g2;->I:LX/0oy;

    .line 313018
    iget v4, v2, LX/0oy;->r:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    .line 313019
    iget-object v4, v2, LX/0oy;->a:LX/0W3;

    sget-wide v6, LX/0X5;->gI:J

    const/16 v5, 0x28

    invoke-interface {v4, v6, v7, v5}, LX/0W4;->a(JI)I

    move-result v4

    iput v4, v2, LX/0oy;->r:I

    .line 313020
    :cond_1
    iget v4, v2, LX/0oy;->r:I

    move v2, v4

    .line 313021
    iget-object v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragmentDataController$1;->a:LX/0g2;

    iget-object v3, v3, LX/0g2;->I:LX/0oy;

    .line 313022
    iget v4, v3, LX/0oy;->p:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    .line 313023
    iget-object v4, v3, LX/0oy;->a:LX/0W3;

    sget-wide v6, LX/0X5;->gG:J

    const/16 v5, 0xa

    invoke-interface {v4, v6, v7, v5}, LX/0W4;->a(JI)I

    move-result v4

    iput v4, v3, LX/0oy;->p:I

    .line 313024
    :cond_2
    iget v4, v3, LX/0oy;->p:I

    move v3, v4

    .line 313025
    invoke-virtual {v0}, LX/0fz;->v()I

    move-result v4

    if-ge v4, v1, :cond_4

    .line 313026
    const/4 v4, 0x0

    .line 313027
    :goto_0
    move v0, v4

    .line 313028
    if-eqz v0, :cond_3

    .line 313029
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragmentDataController$1;->a:LX/0g2;

    invoke-static {v0}, LX/0g2;->R(LX/0g2;)V

    .line 313030
    :cond_3
    return-void

    .line 313031
    :cond_4
    iget-object v4, v0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v4, v3}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v4

    instance-of v4, v4, Lcom/facebook/feed/model/GapFeedEdge;

    if-nez v4, :cond_5

    .line 313032
    iget-object v4, v0, LX/0fz;->g:LX/0qu;

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v4, v5}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v4

    .line 313033
    invoke-static {v0, v4}, LX/0fz;->j(LX/0fz;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v4

    .line 313034
    if-eqz v4, :cond_5

    .line 313035
    const-string v5, "Head_Fetch_Gap"

    invoke-static {v4, v5}, LX/239;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)Lcom/facebook/feed/model/GapFeedEdge;

    move-result-object v4

    .line 313036
    invoke-static {v0, v4}, LX/0fz;->c(LX/0fz;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 313037
    :cond_5
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0}, LX/0fz;->v()I

    move-result v5

    sub-int/2addr v5, v2

    .line 313038
    :goto_1
    if-le v5, v4, :cond_6

    .line 313039
    iget-object v1, v0, LX/0fz;->g:LX/0qu;

    add-int/lit8 v2, v5, -0x1

    invoke-virtual {v1, v2}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 313040
    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    .line 313041
    :cond_6
    const/4 v4, 0x1

    goto :goto_0
.end method
