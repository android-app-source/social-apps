.class public final Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/api/feedtype/FeedType;

.field public b:Ljava/lang/String;

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207586
    new-instance v0, LX/19H;

    invoke-direct {v0}, LX/19H;-><init>()V

    sput-object v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 207587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207588
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->c:Z

    .line 207589
    return-void
.end method


# virtual methods
.method public final d()Lcom/facebook/feed/fragment/NewsFeedFragment;
    .locals 4

    .prologue
    .line 207590
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->a:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v0

    .line 207591
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207592
    new-instance v0, Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-direct {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;-><init>()V

    .line 207593
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 207594
    const-string v2, "feed_type"

    .line 207595
    iget-object v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->a:Lcom/facebook/api/feedtype/FeedType;

    move-object v3, v3

    .line 207596
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 207597
    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->b:Ljava/lang/String;

    move-object v2, v2

    .line 207598
    if-eqz v2, :cond_0

    .line 207599
    const-string v2, "list_name"

    .line 207600
    iget-object v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->b:Ljava/lang/String;

    move-object v3, v3

    .line 207601
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 207602
    :cond_0
    const-string v2, "should_update_title_bar"

    .line 207603
    iget-boolean v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->c:Z

    move v3, v3

    .line 207604
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 207605
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 207606
    move-object v0, v0

    .line 207607
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 207608
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 207609
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->a:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 207610
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 207611
    iget-boolean v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 207612
    return-void

    .line 207613
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
