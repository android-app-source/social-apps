.class public Lcom/facebook/feed/fragment/NewsFeedFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements LX/0fh;
.implements LX/0fi;
.implements LX/0fj;
.implements LX/0fk;
.implements LX/0fl;
.implements LX/0fm;
.implements LX/0fn;
.implements LX/0fo;
.implements LX/0fp;
.implements LX/0fq;
.implements LX/0fr;
.implements LX/0fs;
.implements LX/0ft;
.implements LX/0fu;
.implements LX/0fv;
.implements LX/0fx;
.implements LX/0fw;
.implements LX/0fy;


# annotations
.annotation build Lcom/facebook/common/init/GenerateInitializer;
    task = LX/1Aw;
.end annotation

.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation

.annotation runtime Lcom/facebook/navigation/annotations/NavigationComponent;
    scrollableContent = .subannotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
        id = "android.R.id.list"
        type = .enum LX/8Cz;->RECYCLER_VIEW:LX/8Cz;
    .end subannotation
.end annotation

.annotation runtime Lcom/facebook/search/interfaces/GraphSearchTitleSupport;
.end annotation


# static fields
.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final d:I

.field private static final e:I


# instance fields
.field public A:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private B:LX/1AS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private C:LX/1AT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private D:LX/1AU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private E:LX/1AV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public F:Lcom/facebook/delights/floating/DelightsFireworks;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public G:LX/1Ar;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public H:LX/0Yi;

.field public I:LX/1KS;

.field private J:LX/1Iv;

.field public K:LX/0g2;

.field private L:LX/DJ6;

.field private M:LX/1Pf;

.field public final N:Landroid/os/Handler;

.field public final O:Ljava/lang/Runnable;

.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Nn;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field public g:I

.field public h:I

.field public i:LX/1DJ;

.field public j:LX/0g5;

.field public k:LX/1UQ;

.field public l:Lcom/facebook/widget/CustomFrameLayout;

.field private m:LX/0fx;

.field public n:Z

.field public o:Lcom/facebook/api/feedtype/FeedType;

.field private p:Z

.field private q:LX/1Z9;

.field public r:Z

.field private s:LX/0oh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/1Aw;

.field private u:LX/1AN;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private v:LX/1AP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DBb;",
            ">;"
        }
    .end annotation
.end field

.field private x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DbC;",
            ">;"
        }
    .end annotation
.end field

.field private y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;"
        }
    .end annotation
.end field

.field private z:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/967;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108988
    const-class v0, Lcom/facebook/feed/fragment/NewsFeedFragment;

    sput-object v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->c:Ljava/lang/Class;

    .line 108989
    const v0, 0x7f080ff8

    sput v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->d:I

    .line 108990
    const v0, 0x7f080ffa

    sput v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->e:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 108991
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    .line 108992
    iput v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->f:I

    .line 108993
    iput v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->g:I

    .line 108994
    iput v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->h:I

    .line 108995
    iput-boolean v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->n:Z

    .line 108996
    iput-boolean v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->r:Z

    .line 108997
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 108998
    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->w:LX/0Ot;

    .line 108999
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 109000
    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->x:LX/0Ot;

    .line 109001
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 109002
    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->y:LX/0Ot;

    .line 109003
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 109004
    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->z:LX/0Ot;

    .line 109005
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 109006
    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->A:LX/0Ot;

    .line 109007
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->N:Landroid/os/Handler;

    .line 109008
    new-instance v0, Lcom/facebook/feed/fragment/NewsFeedFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/fragment/NewsFeedFragment$1;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->O:Ljava/lang/Runnable;

    .line 109009
    return-void
.end method

.method private static B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;
    .locals 2

    .prologue
    .line 109010
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->o:Lcom/facebook/api/feedtype/FeedType;

    if-nez v0, :cond_0

    .line 109011
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 109012
    if-eqz v0, :cond_0

    .line 109013
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 109014
    const-string v1, "feed_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedtype/FeedType;

    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->o:Lcom/facebook/api/feedtype/FeedType;

    .line 109015
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->o:Lcom/facebook/api/feedtype/FeedType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->o:Lcom/facebook/api/feedtype/FeedType;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    goto :goto_0
.end method

.method private C()V
    .locals 3

    .prologue
    .line 109016
    new-instance v0, LX/1Nl;

    invoke-direct {v0, p0}, LX/1Nl;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    .line 109017
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->n:LX/1CF;

    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v2

    .line 109018
    iget-object p0, v2, LX/0fz;->d:LX/0qm;

    move-object v2, p0

    .line 109019
    invoke-virtual {v1, v0, v2}, LX/1CF;->a(LX/1CH;LX/0qm;)V

    .line 109020
    return-void
.end method

.method private G()V
    .locals 6

    .prologue
    .line 109021
    new-instance v0, LX/19N;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/19N;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/19N;->a()LX/0Yi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->H:LX/0Yi;

    .line 109022
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->H:LX/0Yi;

    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v1

    .line 109023
    const-wide/16 v2, -0x1

    iput-wide v2, v0, LX/0Yi;->w:J

    .line 109024
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/0Yi;->F:Z

    .line 109025
    const/4 v2, -0x1

    iput v2, v0, LX/0Yi;->H:I

    .line 109026
    invoke-static {v0}, LX/0Yi;->G(LX/0Yi;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109027
    iget-object v2, v0, LX/0Yi;->j:LX/0Yl;

    const v3, 0xa005a

    const-string v4, "NNFCold_MaintabCreateToFeedCreate"

    invoke-virtual {v2, v3, v4}, LX/0Yl;->c(ILjava/lang/String;)LX/0Yl;

    .line 109028
    iget-object v2, v0, LX/0Yi;->j:LX/0Yl;

    const v3, 0xa005b

    const-string v4, "NNFWarm_MaintabCreateToFeedCreate"

    invoke-virtual {v2, v3, v4}, LX/0Yl;->f(ILjava/lang/String;)LX/0Yl;

    .line 109029
    :goto_0
    iget-object v2, v0, LX/0Yi;->j:LX/0Yl;

    invoke-virtual {v0, v1}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v3

    const-string v4, "FragmentCreate"

    invoke-virtual {v0, v4, v1}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0Yl;->h(ILjava/lang/String;)LX/0Yl;

    .line 109030
    iget-object v2, v0, LX/0Yi;->j:LX/0Yl;

    invoke-virtual {v0, v1}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v3

    const-string v4, "FragmentOnCreateTime"

    invoke-virtual {v0, v4, v1}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, LX/0Yl;->b(ILjava/lang/String;Z)LX/0Yl;

    .line 109031
    return-void

    .line 109032
    :cond_0
    invoke-static {v0, v1}, LX/0Yi;->h(LX/0Yi;Lcom/facebook/api/feedtype/FeedType;)V

    goto :goto_0
.end method

.method private I()V
    .locals 5

    .prologue
    .line 109033
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->u:LX/0gw;

    invoke-virtual {v0}, LX/0gw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109034
    :goto_0
    return-void

    .line 109035
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->v:LX/0gy;

    sget-object v1, LX/0cQ;->NATIVE_NEWS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v0, v1}, LX/0gy;->b(LX/0cQ;)LX/0gz;

    move-result-object v0

    .line 109036
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v1}, LX/0g7;->ih_()Landroid/view/View;

    move-result-object v1

    .line 109037
    iget v2, v0, LX/0gz;->c:I

    move v2, v2

    .line 109038
    iget-object v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v3}, LX/0g7;->g()I

    move-result v3

    .line 109039
    iget v4, v0, LX/0gz;->d:I

    move v0, v4

    .line 109040
    iget-object v4, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v4}, LX/0g7;->e()I

    move-result v4

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method private static J(Lcom/facebook/feed/fragment/NewsFeedFragment;)I
    .locals 4

    .prologue
    .line 109041
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nn;

    .line 109042
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;

    .line 109043
    const/4 v2, 0x0

    .line 109044
    iget-object v3, v0, LX/1Nn;->b:LX/0ad;

    sget-short p0, LX/1Nu;->o:S

    invoke-interface {v3, p0, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/1Nn;->b:LX/0ad;

    sget-short p0, LX/0fe;->aG:S

    invoke-interface {v3, p0, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v0, v2

    .line 109045
    if-eqz v0, :cond_1

    .line 109046
    const v0, 0x7f030698

    .line 109047
    :goto_0
    return v0

    .line 109048
    :cond_1
    invoke-virtual {v1}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 109049
    const v0, 0x7f030699

    goto :goto_0

    .line 109050
    :cond_2
    const v0, 0x7f030694

    goto :goto_0
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/facebook/widget/CustomFrameLayout;
    .locals 4

    .prologue
    .line 109051
    const-string v0, "NewsFeedFragment.inflateNewsFeed"

    const v1, 0x9c909ba

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 109052
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->H:LX/0Yi;

    const-string v1, "FragmentViewInflate"

    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v1

    .line 109053
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->H:LX/0Yi;

    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v2

    .line 109054
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->h:LX/0Yl;

    invoke-virtual {v0, v2, v1}, LX/0Yl;->h(ILjava/lang/String;)LX/0Yl;

    .line 109055
    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->J(Lcom/facebook/feed/fragment/NewsFeedFragment;)I

    move-result v0

    .line 109056
    const/4 v3, 0x0

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    .line 109057
    iget-object v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v3, v3, LX/1Aw;->h:LX/0Yl;

    invoke-virtual {v3, v2, v1}, LX/0Yl;->i(ILjava/lang/String;)LX/0Yl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109058
    const v1, -0x41b92b3a

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x23e73517

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(Lcom/facebook/feed/fragment/NewsFeedFragment;LX/0Or;LX/0Or;LX/0oh;LX/1AN;LX/1AP;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1AS;LX/1AT;LX/1AU;LX/1AV;Lcom/facebook/delights/floating/DelightsFireworks;LX/1Ar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/fragment/NewsFeedFragment;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1Nn;",
            ">;",
            "LX/0oh;",
            "LX/1AN;",
            "LX/1AP;",
            "LX/0Ot",
            "<",
            "LX/DBb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DbC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/967;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/1AS;",
            "LX/1AT;",
            "LX/1AU;",
            "LX/1AV;",
            "Lcom/facebook/delights/floating/DelightsFireworks;",
            "LX/1Ar;",
            ")V"
        }
    .end annotation

    .prologue
    .line 109059
    iput-object p1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->s:LX/0oh;

    iput-object p4, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->u:LX/1AN;

    iput-object p5, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->v:LX/1AP;

    iput-object p6, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->w:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->x:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->y:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->z:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->A:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->B:LX/1AS;

    iput-object p12, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->C:LX/1AT;

    iput-object p13, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->D:LX/1AU;

    iput-object p14, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->E:LX/1AV;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->F:Lcom/facebook/delights/floating/DelightsFireworks;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->G:LX/1Ar;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 18

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/feed/fragment/NewsFeedFragment;

    const/16 v2, 0x65c

    move-object/from16 v0, v17

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x65b

    move-object/from16 v0, v17

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static/range {v17 .. v17}, LX/0oh;->a(LX/0QB;)LX/0oh;

    move-result-object v4

    check-cast v4, LX/0oh;

    invoke-static/range {v17 .. v17}, LX/1AN;->a(LX/0QB;)LX/1AN;

    move-result-object v5

    check-cast v5, LX/1AN;

    invoke-static/range {v17 .. v17}, LX/1AP;->a(LX/0QB;)LX/1AP;

    move-result-object v6

    check-cast v6, LX/1AP;

    const/16 v7, 0x1c8f

    move-object/from16 v0, v17

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2516

    move-object/from16 v0, v17

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1a75

    move-object/from16 v0, v17

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1a39

    move-object/from16 v0, v17

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xbc

    move-object/from16 v0, v17

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const-class v12, LX/1AS;

    move-object/from16 v0, v17

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/1AS;

    const-class v13, LX/1AT;

    move-object/from16 v0, v17

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/1AT;

    const-class v14, LX/1AU;

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/1AU;

    invoke-static/range {v17 .. v17}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v15

    check-cast v15, LX/1AV;

    invoke-static/range {v17 .. v17}, Lcom/facebook/delights/floating/DelightsFireworks;->a(LX/0QB;)Lcom/facebook/delights/floating/DelightsFireworks;

    move-result-object v16

    check-cast v16, Lcom/facebook/delights/floating/DelightsFireworks;

    invoke-static/range {v17 .. v17}, LX/1Ar;->a(LX/0QB;)LX/1Ar;

    move-result-object v17

    check-cast v17, LX/1Ar;

    invoke-static/range {v1 .. v17}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(Lcom/facebook/feed/fragment/NewsFeedFragment;LX/0Or;LX/0Or;LX/0oh;LX/1AN;LX/1AP;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1AS;LX/1AT;LX/1AU;LX/1AV;Lcom/facebook/delights/floating/DelightsFireworks;LX/1Ar;)V

    return-void
.end method

.method public static b(Lcom/facebook/feed/fragment/NewsFeedFragment;LX/0g8;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 109060
    if-ltz p2, :cond_0

    invoke-interface {p1}, LX/0g8;->s()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 109061
    :cond_0
    :goto_0
    return-void

    .line 109062
    :cond_1
    invoke-interface {p1, p2}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v0

    .line 109063
    instance-of v1, v0, LX/0jQ;

    if-eqz v1, :cond_0

    .line 109064
    check-cast v0, LX/0jQ;

    invoke-interface {v0}, LX/0jQ;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 109065
    instance-of v0, v1, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 109066
    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    .line 109067
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v3, :cond_0

    .line 109068
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->e:LX/0bH;

    if-eqz v0, :cond_0

    .line 109069
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v6, v0, LX/1Aw;->e:LX/0bH;

    new-instance v0, LX/1Nd;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    const/4 v5, 0x1

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 109070
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->e:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    .line 109071
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final S_()Z
    .locals 1

    .prologue
    .line 109072
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->i()Z

    move-result v0

    return v0
.end method

.method public final a(LX/0fx;)LX/0fx;
    .locals 1

    .prologue
    .line 109073
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->v:LX/1AP;

    invoke-virtual {v0, p1}, LX/1AP;->b(LX/0fx;)LX/1Ks;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109286
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->o:Lcom/facebook/api/feedtype/FeedType;

    if-nez v0, :cond_0

    invoke-static {}, LX/0Yi;->v()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->o:Lcom/facebook/api/feedtype/FeedType;

    .line 109287
    iget-object p0, v0, Lcom/facebook/api/feedtype/FeedType;->g:Ljava/lang/String;

    move-object v0, p0

    .line 109288
    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 109074
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v0, p1}, LX/0g2;->a(I)V

    .line 109075
    return-void
.end method

.method public final a(LX/0g8;I)V
    .locals 11

    .prologue
    .line 109244
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 109245
    iget-object v1, v0, LX/0g2;->c:LX/0fz;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/0g2;->ae:LX/1UQ;

    if-nez v1, :cond_1

    .line 109246
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->m:LX/0fx;

    invoke-interface {v0, p1, p2}, LX/0fx;->a(LX/0g8;I)V

    .line 109247
    return-void

    .line 109248
    :cond_1
    iget-object v1, v0, LX/0g2;->B:LX/0pT;

    invoke-interface {p1}, LX/0g8;->q()I

    move-result v2

    invoke-virtual {v1, p2, v2}, LX/0pT;->a(II)V

    .line 109249
    if-nez p2, :cond_0

    .line 109250
    iget-object v1, v0, LX/0g2;->C:LX/0pl;

    invoke-virtual {v1}, LX/0pl;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-interface {v1}, LX/0fv;->j()Z

    move-result v1

    .line 109251
    :goto_1
    if-eqz v1, :cond_4

    .line 109252
    iget-object v1, v0, LX/0g2;->n:LX/0pV;

    sget-object v2, LX/0g2;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/0rj;->REACHED_TOP:LX/0rj;

    invoke-virtual {v1, v2, v3}, LX/0pV;->a(Ljava/lang/String;LX/0rj;)V

    .line 109253
    iget-object v1, v0, LX/0g2;->B:LX/0pT;

    const/4 v10, 0x1

    const-wide/16 v8, 0x1388

    .line 109254
    iget-object v4, v1, LX/0pT;->m:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, v1, LX/0pT;->h:J

    sub-long/2addr v4, v6

    cmp-long v4, v4, v8

    if-ltz v4, :cond_2

    iget-object v4, v1, LX/0pT;->m:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, v1, LX/0pT;->i:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x2710

    cmp-long v4, v4, v6

    if-gez v4, :cond_5

    .line 109255
    :cond_2
    :goto_2
    invoke-static {v0}, LX/0g2;->ah(LX/0g2;)Z

    .line 109256
    invoke-virtual {v0}, LX/0g2;->C()V

    goto :goto_0

    .line 109257
    :cond_3
    iget-object v1, v0, LX/0g2;->V:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v1}, Lcom/facebook/feed/fragment/NewsFeedFragment;->z()Z

    move-result v1

    goto :goto_1

    .line 109258
    :cond_4
    iget-boolean v1, v0, LX/0g2;->d:Z

    if-eqz v1, :cond_9

    iget-object v1, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v1}, LX/0gC;->v()Z

    move-result v1

    if-nez v1, :cond_9

    .line 109259
    iget-object v1, v0, LX/0g2;->c:LX/0fz;

    move-object v1, v1

    .line 109260
    invoke-virtual {v1}, LX/0fz;->c()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 109261
    iget-object v1, v0, LX/0g2;->c:LX/0fz;

    move-object v1, v1

    .line 109262
    iget-object v2, v1, LX/0fz;->i:LX/0qv;

    move-object v1, v2

    .line 109263
    invoke-virtual {v1}, LX/0qv;->f()I

    move-result v1

    sget-object v2, LX/1lq;->AvoidNewStoryPill:LX/1lq;

    invoke-static {v0, v1, v2}, LX/0g2;->b(LX/0g2;ILX/1lq;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 109264
    if-eqz v1, :cond_0

    .line 109265
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/0g2;->d:Z

    .line 109266
    iget-object v1, v0, LX/0g2;->c:LX/0fz;

    move-object v1, v1

    .line 109267
    iget-object v2, v1, LX/0fz;->i:LX/0qv;

    move-object v1, v2

    .line 109268
    invoke-virtual {v1}, LX/0qv;->f()I

    move-result v1

    .line 109269
    invoke-static {v0, v1}, LX/0g2;->e(LX/0g2;I)V

    .line 109270
    if-lez v1, :cond_0

    iget-object v2, v0, LX/0g2;->Y:Lcom/facebook/api/feedtype/FeedType;

    .line 109271
    iget-object v3, v2, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v3

    .line 109272
    sget-object v3, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v2, v3}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109273
    iget-object v2, v0, LX/0g2;->y:LX/0iS;

    invoke-virtual {v2, v1}, LX/0iS;->a(I)V

    goto/16 :goto_0

    .line 109274
    :cond_5
    iget-object v4, v1, LX/0pT;->o:LX/0pU;

    .line 109275
    invoke-static {v4}, LX/0pU;->g(LX/0pU;)LX/3m0;

    move-result-object v5

    .line 109276
    if-nez v5, :cond_8

    sget-object v5, LX/391;->UNKNOWN:LX/391;

    :goto_4
    move-object v5, v5

    .line 109277
    sget-object v4, LX/391;->UNKNOWN:LX/391;

    if-eq v5, v4, :cond_2

    .line 109278
    sget-object v4, LX/391;->MANUAL:LX/391;

    if-ne v5, v4, :cond_6

    const-string v4, "ptr_scroll_to_top"

    :goto_5
    invoke-static {v1, v4}, LX/0pT;->a$redex0(LX/0pT;Ljava/lang/String;)V

    .line 109279
    iget-object v4, v1, LX/0pT;->m:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v6

    iput-wide v6, v1, LX/0pT;->h:J

    .line 109280
    sget-object v4, LX/391;->MANUAL:LX/391;

    if-ne v5, v4, :cond_7

    .line 109281
    iget-object v4, v1, LX/0pT;->p:Landroid/os/Handler;

    iget-object v5, v1, LX/0pT;->q:Ljava/lang/Runnable;

    const v6, 0x21881f3f

    invoke-static {v4, v5, v8, v9, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 109282
    iput-boolean v10, v1, LX/0pT;->t:Z

    goto/16 :goto_2

    .line 109283
    :cond_6
    const-string v4, "ptr_jump_to_top"

    goto :goto_5

    .line 109284
    :cond_7
    iget-object v4, v1, LX/0pT;->p:Landroid/os/Handler;

    iget-object v5, v1, LX/0pT;->r:Ljava/lang/Runnable;

    const v6, 0x3fd1790f

    invoke-static {v4, v5, v8, v9, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 109285
    iput-boolean v10, v1, LX/0pT;->u:Z

    goto/16 :goto_2

    :cond_8
    iget-object v5, v5, LX/3m0;->a:LX/391;

    goto :goto_4

    :cond_9
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 109242
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->m:LX/0fx;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0fx;->a(LX/0g8;III)V

    .line 109243
    return-void
.end method

.method public final a(LX/0gf;)V
    .locals 1

    .prologue
    .line 109240
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v0, p1}, LX/0g2;->c(LX/0gf;)V

    .line 109241
    return-void
.end method

.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 2

    .prologue
    .line 108651
    check-cast p1, LX/1UQ;

    iput-object p1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->k:LX/1UQ;

    .line 108652
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->k:LX/1UQ;

    invoke-virtual {v0, v1}, LX/0g2;->a(LX/1UQ;)V

    .line 108653
    iput-object p3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->M:LX/1Pf;

    .line 108654
    return-void
.end method

.method public final a(LX/5Mj;)V
    .locals 2

    .prologue
    .line 109237
    iget-object v0, p1, LX/5Mj;->f:LX/5Mk;

    move-object v0, v0

    .line 109238
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->k:LX/1UQ;

    invoke-virtual {v0, v1, p1}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 109239
    return-void
.end method

.method public final a(LX/69s;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 109220
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 109221
    if-nez v0, :cond_1

    .line 109222
    :cond_0
    :goto_0
    return-void

    .line 109223
    :cond_1
    sget-object v0, LX/69s;->LIKE:LX/69s;

    if-ne p1, v0, :cond_0

    .line 109224
    invoke-static {p2}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v0

    .line 109225
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->D:LX/1CW;

    invoke-virtual {v1, v0, v2, v2}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v1

    .line 109226
    invoke-static {v0}, LX/1CW;->b(Lcom/facebook/fbservice/service/ServiceException;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 109227
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->E:LX/1CX;

    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->F:Landroid/content/res/Resources;

    invoke-static {v2}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v2

    sget-object v3, LX/8iJ;->SENTRY_LIKE_BLOCK:LX/8iJ;

    invoke-virtual {v3}, LX/8iJ;->getTitleId()I

    move-result v3

    invoke-virtual {v2, v3}, LX/4mn;->a(I)LX/4mn;

    move-result-object v2

    .line 109228
    iput-object v1, v2, LX/4mn;->c:Ljava/lang/String;

    .line 109229
    move-object v1, v2

    .line 109230
    const v2, 0x7f080054

    invoke-virtual {v1, v2}, LX/4mn;->c(I)LX/4mn;

    move-result-object v1

    sget-object v2, LX/8iK;->a:Landroid/net/Uri;

    .line 109231
    iput-object v2, v1, LX/4mn;->e:Landroid/net/Uri;

    .line 109232
    move-object v1, v1

    .line 109233
    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    goto :goto_0

    .line 109234
    :cond_2
    iget-object v2, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v2

    .line 109235
    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-eq v0, v2, :cond_0

    .line 109236
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->G:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 19
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 109183
    if-eqz p1, :cond_0

    const-string v2, "feed_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109184
    const-string v2, "feed_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/feedtype/FeedType;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->o:Lcom/facebook/api/feedtype/FeedType;

    .line 109185
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->G()V

    .line 109186
    invoke-super/range {p0 .. p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 109187
    const/4 v2, -0x4

    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 109188
    const-string v2, "NewsfeedFragment.injectMe"

    const v3, -0x6507b453

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 109189
    :try_start_0
    const-class v2, Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-object/from16 v0, p0

    invoke-static {v2, v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(Ljava/lang/Class;LX/02k;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109190
    const v2, 0x65cc23d8

    invoke-static {v2}, LX/02m;->a(I)V

    .line 109191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->s:LX/0oh;

    const-class v3, Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v2, v3}, LX/0oh;->b(Ljava/lang/Class;)LX/1Av;

    move-result-object v2

    invoke-virtual {v2}, LX/1Av;->c()LX/1Ax;

    move-result-object v2

    check-cast v2, LX/1Aw;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    .line 109192
    new-instance v6, LX/1Iu;

    invoke-direct {v6}, LX/1Iu;-><init>()V

    .line 109193
    new-instance v7, LX/1Iu;

    invoke-direct {v7}, LX/1Iu;-><init>()V

    .line 109194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->D:LX/1AU;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v3, v3, LX/1Aw;->N:LX/1EM;

    invoke-virtual {v2, v3}, LX/1AU;->a(LX/1EM;)LX/1Iv;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->J:LX/1Iv;

    .line 109195
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->J:LX/1Iv;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Iv;->a(Lcom/facebook/api/feedtype/FeedType;)V

    .line 109196
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    if-nez v2, :cond_1

    .line 109197
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->K:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0g2;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 109198
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->L:LX/1CZ;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->J:LX/1Iv;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v5, v3, LX/1Aw;->N:LX/1EM;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v3}, LX/0g2;->t()LX/1J6;

    move-result-object v9

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v9}, LX/1CZ;->a(Lcom/facebook/feed/fragment/NewsFeedFragment;LX/1Iv;LX/1EM;LX/1Iu;LX/1Iu;Lcom/facebook/api/feedtype/FeedType;LX/1J6;)LX/1KS;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->I:LX/1KS;

    .line 109199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->I:LX/1KS;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v2, v0, v1}, LX/1KS;->a(Landroid/support/v4/app/Fragment;Landroid/os/Bundle;)V

    .line 109200
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v12, v2, LX/1Aw;->C:LX/1CY;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v13, v2, LX/1Aw;->e:LX/0bH;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->J:LX/1Iv;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v15, v2, LX/1Aw;->N:LX/1EM;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->I:LX/1KS;

    move-object/from16 v18, v0

    move-object/from16 v9, p0

    move-object/from16 v10, p0

    move-object/from16 v17, p1

    invoke-virtual/range {v8 .. v18}, LX/0g2;->a(Lcom/facebook/feed/fragment/NewsFeedFragment;Lcom/facebook/feed/fragment/NewsFeedFragment;Lcom/facebook/api/feedtype/FeedType;LX/1CY;LX/0bH;LX/1Iv;LX/1EM;Landroid/content/Context;Landroid/os/Bundle;LX/1KS;)V

    .line 109201
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->u()LX/0qq;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/1Iu;->a(Ljava/lang/Object;)V

    .line 109202
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v2

    invoke-virtual {v7, v2}, LX/1Iu;->a(Ljava/lang/Object;)V

    .line 109203
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->B:LX/0jU;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, LX/0jU;->a(LX/0fi;)V

    .line 109204
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v2}, LX/0g2;->u()V

    .line 109205
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->g:LX/1B1;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v3, v3, LX/1Aw;->z:LX/1CU;

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 109206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->g:LX/1B1;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->C:LX/1AT;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->u()LX/0qq;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1AT;->a(LX/0qq;)LX/1NM;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 109207
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->g:LX/1B1;

    new-instance v3, LX/1NO;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/1NO;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 109208
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->g:LX/1B1;

    new-instance v3, LX/1NQ;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/1NQ;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 109209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->g:LX/1B1;

    new-instance v3, LX/1NS;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/1NS;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 109210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->g:LX/1B1;

    new-instance v3, LX/1NU;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/1NU;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 109211
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->g:LX/1B1;

    new-instance v3, LX/1NW;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v4, v4, LX/1Aw;->e:LX/0bH;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v5, v5, LX/1Aw;->f:LX/189;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5}, LX/1NW;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;LX/0bH;LX/189;)V

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 109212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->g:LX/1B1;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->B:LX/1AS;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, LX/1AS;->a(Lcom/facebook/feed/fragment/NewsFeedFragment;)LX/1NY;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 109213
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->g:LX/1B1;

    new-instance v3, LX/1Na;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/1Na;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 109214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->g:LX/1B1;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v3, v3, LX/1Aw;->e:LX/0bH;

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b4;)V

    .line 109215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->z:LX/1CU;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->u()LX/0qq;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1CU;->a(LX/0qq;)V

    .line 109216
    invoke-direct/range {p0 .. p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->C()V

    .line 109217
    new-instance v2, LX/1Nm;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/1Nm;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(LX/0fx;)LX/0fx;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->m:LX/0fx;

    .line 109218
    return-void

    .line 109219
    :catchall_0
    move-exception v2

    const v3, 0x1b7598e9

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
.end method

.method public final a(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 109129
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->l:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 109130
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->i:LX/1DJ;

    invoke-interface {v0}, LX/1DK;->a()LX/0g8;

    move-result-object v0

    check-cast v0, LX/0g5;

    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    .line 109131
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    .line 109132
    iput-object v1, v0, LX/0g2;->af:LX/0g5;

    .line 109133
    iget-object v2, v0, LX/0g2;->S:LX/1NJ;

    .line 109134
    iput-object v1, v2, LX/1NJ;->i:LX/0g5;

    .line 109135
    invoke-direct {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->I()V

    .line 109136
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v0, p0}, LX/0g7;->a(Landroid/view/View$OnTouchListener;)V

    .line 109137
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    new-instance v1, LX/1Z6;

    invoke-direct {v1, p0}, LX/1Z6;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    invoke-virtual {v0, v1}, LX/0g7;->a(LX/1Z7;)V

    .line 109138
    new-instance v0, LX/1Z9;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->l:Lcom/facebook/widget/CustomFrameLayout;

    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    .line 109139
    iget-object v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v3, v3, LX/1Aw;->w:LX/0ad;

    sget-short v4, LX/0fe;->bQ:S

    const/4 p1, 0x0

    invoke-interface {v3, v4, p1}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 109140
    iget-object v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 109141
    new-instance v4, LX/1ZA;

    invoke-direct {v4, v3}, LX/1ZA;-><init>(LX/0g2;)V

    move-object v3, v4

    .line 109142
    :goto_0
    move-object v3, v3

    .line 109143
    iget-object v4, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->u:LX/1AN;

    invoke-direct {v0, v1, v2, v3, v4}, LX/1Z9;-><init>(Landroid/view/ViewGroup;LX/0g8;Landroid/view/View$OnClickListener;LX/1AN;)V

    iput-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->q:LX/1Z9;

    .line 109144
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->y:LX/0pJ;

    invoke-virtual {v0, v5}, LX/0pJ;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->Q:LX/1EU;

    invoke-virtual {v0}, LX/1EU;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109145
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->q:LX/1Z9;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 109146
    new-instance v2, LX/2r6;

    invoke-direct {v2, v1}, LX/2r6;-><init>(LX/0g2;)V

    move-object v1, v2

    .line 109147
    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->p:LX/1CM;

    .line 109148
    iput-object v2, v0, LX/1Z9;->e:LX/1CM;

    .line 109149
    iput-object v1, v0, LX/1Z9;->g:Landroid/view/View$OnClickListener;

    .line 109150
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->q:LX/1Z9;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->I:LX/1KS;

    .line 109151
    iput-object v1, v0, LX/1Z9;->i:LX/0fo;

    .line 109152
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->q:LX/1Z9;

    .line 109153
    iput-object v1, v0, LX/0g2;->ah:LX/1Z9;

    .line 109154
    iget-object v2, v0, LX/0g2;->S:LX/1NJ;

    .line 109155
    iput-object v1, v2, LX/1NJ;->g:LX/1Z9;

    .line 109156
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->l:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v1}, LX/0g2;->a(Landroid/view/View;)V

    .line 109157
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v0, p0}, LX/0g7;->b(LX/0fu;)V

    .line 109158
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->W:LX/1Fn;

    invoke-virtual {v0}, LX/1Fn;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109159
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v0, v6, v5}, LX/0g7;->c(II)V

    .line 109160
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 109161
    iget-boolean v1, v0, LX/0g2;->al:Z

    move v0, v1

    .line 109162
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->H:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZD;

    const-wide/16 v11, 0x0

    const/4 v7, 0x0

    .line 109163
    iget-object v8, v0, LX/1ZD;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v8

    if-nez v8, :cond_7

    .line 109164
    :cond_2
    :goto_1
    move v0, v7

    .line 109165
    if-eqz v0, :cond_3

    .line 109166
    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FIRST_NEWSFEED_AFTER_LOGIN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 109167
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->I:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/15W;->a(Landroid/app/Activity;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    .line 109168
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 109169
    iget v1, v0, LX/0g2;->ai:I

    if-lez v1, :cond_8

    iget-object v1, v0, LX/0g2;->c:LX/0fz;

    invoke-virtual {v1}, LX/0fz;->c()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {v0}, LX/0g2;->M(LX/0g2;)Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 109170
    if-eqz v0, :cond_4

    .line 109171
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->q:LX/1Z9;

    invoke-virtual {v0, v6}, LX/1Z9;->a(Z)Z

    .line 109172
    :cond_4
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->H:LX/0Yi;

    .line 109173
    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v1

    const v4, 0xa0039

    .line 109174
    const-string v2, "FragmentOnViewCreateTime"

    invoke-static {v0, v2, v1}, LX/0Yi;->b(LX/0Yi;Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)V

    .line 109175
    iget-object v2, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v3, "NNFFragmentViewCreate"

    invoke-interface {v2, v4, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 109176
    iget-object v2, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v3, "NNFFragmentViewCreate"

    invoke-interface {v2, v4, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 109177
    :cond_5
    return-void

    :cond_6
    new-instance v3, LX/2to;

    invoke-direct {v3, p0}, LX/2to;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    goto/16 :goto_0

    .line 109178
    :cond_7
    iget-object v8, v0, LX/1ZD;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/1ZE;->a:LX/0Tn;

    invoke-interface {v8, v9, v11, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v9

    .line 109179
    cmp-long v8, v9, v11

    if-lez v8, :cond_2

    .line 109180
    iget-object v8, v0, LX/1ZD;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v8

    sget-object v11, LX/1ZE;->a:LX/0Tn;

    invoke-interface {v8, v11}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v8

    invoke-interface {v8}, LX/0hN;->commit()V

    .line 109181
    iget-object v8, v0, LX/1ZD;->a:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v11

    sub-long v9, v11, v9

    .line 109182
    const-wide/32 v11, 0x1d4c0

    cmp-long v8, v9, v11

    if-gtz v8, :cond_2

    const/4 v7, 0x1

    goto/16 :goto_1

    :cond_8
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 109127
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->e:LX/0bH;

    new-instance v1, LX/1YL;

    invoke-direct {v1}, LX/1YL;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 109128
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 109122
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->u:LX/1AN;

    .line 109123
    iget-object v1, v0, LX/1AN;->k:Landroid/view/View;

    move-object v0, v1

    .line 109124
    if-eq p1, v0, :cond_0

    .line 109125
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->u:LX/1AN;

    invoke-virtual {v0}, LX/1AN;->d()Z

    .line 109126
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 109120
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v0, p1}, LX/0g2;->b(Z)V

    .line 109121
    return-void
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 109116
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 109117
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->k:LX/1UQ;

    if-eqz v0, :cond_0

    .line 109118
    new-instance v0, LX/5Mj;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/5Mk;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/5Mj;-><init>(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;LX/5Mk;)V

    invoke-virtual {p0, v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(LX/5Mj;)V

    .line 109119
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 5

    .prologue
    .line 108973
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->M:LX/0Sh;

    const-string v1, "scrollToTop should be called on the UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 108974
    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v0

    .line 108975
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0fz;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108976
    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v0

    invoke-virtual {v0}, LX/0fz;->g()V

    .line 108977
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z

    move-result v0

    if-nez v0, :cond_1

    .line 108978
    :goto_0
    return-void

    .line 108979
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->w:LX/0ad;

    sget-short v1, LX/0fe;->bQ:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108980
    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->x()V

    .line 108981
    :cond_2
    const/4 v0, 0x5

    move v0, v0

    .line 108982
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->k:LX/1UQ;

    invoke-interface {v1}, LX/1Qr;->d()I

    move-result v1

    if-eqz v1, :cond_4

    .line 108983
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->k:LX/1UQ;

    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->k:LX/1UQ;

    invoke-interface {v2}, LX/1Qr;->d()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-interface {v1, v0}, LX/1Qr;->m_(I)I

    move-result v0

    .line 108984
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v1}, LX/0g7;->q()I

    move-result v1

    if-le v1, v0, :cond_3

    .line 108985
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v1, v0}, LX/0g7;->g(I)V

    .line 108986
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->U:LX/0gp;

    invoke-virtual {v0}, LX/0gp;->c()V

    .line 108987
    :cond_4
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->N:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->O:Ljava/lang/Runnable;

    const-wide/16 v2, 0x0

    const v4, -0x3cc71110

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method public final g()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 109076
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    if-eqz v0, :cond_0

    .line 109077
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->N:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->O:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 109078
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->t:LX/1CT;

    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v2}, LX/0g5;->D()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/1CT;->a(LX/0g1;IZ)V

    .line 109079
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    const/4 v3, 0x0

    .line 109080
    iget-object v1, v0, LX/0g2;->X:LX/0bH;

    iget-object v2, v0, LX/0g2;->aj:LX/1NK;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 109081
    iget-object v1, v0, LX/0g2;->E:LX/0qa;

    invoke-virtual {v1}, LX/0qa;->c()V

    .line 109082
    iget-object v1, v0, LX/0g2;->af:LX/0g5;

    if-eqz v1, :cond_1

    .line 109083
    iput-object v3, v0, LX/0g2;->af:LX/0g5;

    .line 109084
    iget-object v1, v0, LX/0g2;->S:LX/1NJ;

    .line 109085
    iput-object v3, v1, LX/1NJ;->i:LX/0g5;

    .line 109086
    :cond_1
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroyView()V

    .line 109087
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v0, v4}, LX/0g2;->a(Landroid/os/Bundle;)V

    .line 109088
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->u:LX/1AN;

    const/4 v3, 0x0

    .line 109089
    iget-object v1, v0, LX/1AN;->e:Landroid/os/Handler;

    iget-object v2, v0, LX/1AN;->h:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 109090
    iget-object v1, v0, LX/1AN;->e:Landroid/os/Handler;

    iget-object v2, v0, LX/1AN;->i:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 109091
    iget-object v1, v0, LX/1AN;->l:LX/0wd;

    if-eqz v1, :cond_2

    .line 109092
    iget-object v1, v0, LX/1AN;->l:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->a()V

    .line 109093
    :cond_2
    iget-object v1, v0, LX/1AN;->o:LX/0g8;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/1AN;->q:LX/0fx;

    if-eqz v1, :cond_3

    .line 109094
    iget-object v1, v0, LX/1AN;->o:LX/0g8;

    iget-object v2, v0, LX/1AN;->q:LX/0fx;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 109095
    iput-object v3, v0, LX/1AN;->o:LX/0g8;

    .line 109096
    :cond_3
    iget-object v1, v0, LX/1AN;->k:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 109097
    iget-object v1, v0, LX/1AN;->k:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 109098
    :cond_4
    iput-object v3, v0, LX/1AN;->p:LX/30v;

    .line 109099
    iput-object v3, v0, LX/1AN;->k:Landroid/view/View;

    .line 109100
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    if-eqz v0, :cond_5

    .line 109101
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->a(Landroid/widget/ListAdapter;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109102
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v0}, LX/0g7;->w()V

    .line 109103
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v0}, LX/0g7;->x()V

    .line 109104
    :cond_5
    iput-object v4, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    .line 109105
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->l:Lcom/facebook/widget/CustomFrameLayout;

    const v1, 0x7f0d0d53

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 109106
    if-eqz v0, :cond_6

    .line 109107
    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109108
    :cond_6
    iput-object v4, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->q:LX/1Z9;

    .line 109109
    iput-object v4, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->l:Lcom/facebook/widget/CustomFrameLayout;

    .line 109110
    return-void

    .line 109111
    :catch_0
    move-exception v0

    .line 109112
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->k:LX/19P;

    if-eqz v1, :cond_7

    .line 109113
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->d:LX/03V;

    const-string v2, "feed_adapter_video_state"

    iget-object v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v3, v3, LX/1Aw;->k:LX/19P;

    invoke-virtual {v3}, LX/19P;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 109114
    :cond_7
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->d:LX/03V;

    const-string v2, "feed_adapter_exception"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 109115
    throw v0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108655
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DBb;

    iget v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->f:I

    iget v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->g:I

    .line 108656
    iget-object v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->k:LX/1UQ;

    move-object v3, v3

    .line 108657
    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v4

    .line 108658
    iget-object v5, v0, LX/DBb;->b:LX/0lC;

    invoke-virtual {v5}, LX/0lC;->g()LX/4ps;

    move-result-object v5

    invoke-virtual {v5}, LX/4ps;->a()LX/4ps;

    move-result-object v9

    .line 108659
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 108660
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 108661
    iget-object v5, v0, LX/DBb;->a:LX/0jU;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\nFeed stories("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "):\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108662
    const/4 v5, -0x1

    if-eq v1, v5, :cond_a

    const/4 v5, -0x1

    if-eq v2, v5, :cond_a

    .line 108663
    const/4 v7, 0x0

    .line 108664
    if-lez v1, :cond_4

    add-int/lit8 v5, v1, -0x1

    :goto_0
    move v8, v5

    .line 108665
    :goto_1
    if-ge v8, v2, :cond_a

    .line 108666
    invoke-interface {v3, v8}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    .line 108667
    instance-of v6, v5, LX/1Rk;

    if-eqz v6, :cond_c

    .line 108668
    check-cast v5, LX/1Rk;

    invoke-virtual {v5}, LX/1Rk;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v6

    .line 108669
    if-eqz v6, :cond_c

    if-eq v6, v7, :cond_c

    .line 108670
    instance-of v5, v6, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v5, :cond_6

    move-object v5, v6

    .line 108671
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 108672
    const/4 v7, 0x0

    .line 108673
    invoke-static {v5}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v12

    if-eqz v12, :cond_5

    .line 108674
    invoke-static {v5}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    .line 108675
    :cond_0
    :goto_2
    if-eqz v7, :cond_1

    .line 108676
    const-string v12, "[\r\n]+"

    const-string v13, " "

    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 108677
    :cond_1
    const-string v12, "\nStory message: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108678
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 108679
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/DBb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v12, "\n"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108680
    :cond_2
    const-string v7, "\nGraphQL response:\n"

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108681
    :try_start_0
    invoke-virtual {v9, v5}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108682
    :goto_3
    const-string v5, "\n\n"

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108683
    :cond_3
    :goto_4
    add-int/lit8 v5, v8, 0x1

    move v8, v5

    move-object v7, v6

    goto :goto_1

    .line 108684
    :cond_4
    const/4 v5, 0x0

    goto :goto_0

    .line 108685
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 108686
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    .line 108687
    :catch_0
    const-string v7, "ID: "

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v12, ", cache_id: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 108688
    :cond_6
    const-string v5, "\nGraphQL response:\n"

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108689
    :try_start_1
    invoke-virtual {v9, v6}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 108690
    :goto_5
    const-string v5, "\n\n"

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108691
    invoke-interface {v6}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->E_()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 108692
    invoke-interface {v6}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->E_()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/DBb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\n"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108693
    :cond_7
    instance-of v5, v6, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v5, :cond_3

    move-object v5, v6

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v5

    if-eqz v5, :cond_3

    move-object v5, v6

    .line 108694
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;->j()LX/0Px;

    move-result-object v12

    .line 108695
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v13

    const/4 v5, 0x0

    move v7, v5

    :goto_6
    if-ge v7, v13, :cond_3

    invoke-virtual {v12, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 108696
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_8

    .line 108697
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/DBb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "\n"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108698
    :cond_8
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_6

    .line 108699
    :catch_1
    const-string v5, "Type: "

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v6}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_9

    invoke-interface {v6}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    :goto_7
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", cache_id: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v6}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", fetch_time_ms: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v6}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v13

    invoke-virtual {v5, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    :cond_9
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    goto :goto_7

    .line 108700
    :cond_a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_b

    .line 108701
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 108702
    :goto_8
    new-instance v5, LX/4y1;

    invoke-direct {v5}, LX/4y1;-><init>()V

    const-string v6, "StoryDebugInfo"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v5

    const-string v6, "StoryZombies"

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v5

    .line 108703
    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    move-object v0, v5

    .line 108704
    return-object v0

    .line 108705
    :cond_b
    const-string v5, "No zombies were found because debug info was not available"

    invoke-static {v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    :cond_c
    move-object v6, v7

    goto/16 :goto_4
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 108650
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v0}, LX/0g7;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 108649
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    return-object v0
.end method

.method public final ko_()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 108635
    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v0

    .line 108636
    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->J:LX/199;

    .line 108637
    iget-wide v4, v2, LX/199;->a:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 108638
    iget-object v4, v2, LX/199;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v2, LX/199;->a:J

    .line 108639
    :cond_0
    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v2}, LX/0g7;->p()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->A:LX/0pW;

    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 108640
    iget-boolean v6, v2, LX/0pW;->e:Z

    if-nez v6, :cond_1

    .line 108641
    iput-boolean v4, v2, LX/0pW;->e:Z

    .line 108642
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x13

    if-le v6, v7, :cond_1

    .line 108643
    invoke-static {}, Lcom/facebook/common/dextricks/DalvikInternals;->disableOatMadviseSequential()V

    .line 108644
    :cond_1
    iput-boolean v5, v2, LX/0pW;->d:Z

    .line 108645
    iget-object v6, v2, LX/0pW;->a:LX/0Yi;

    invoke-virtual {v6, v3, v0}, LX/0Yi;->a(Lcom/facebook/api/feedtype/FeedType;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 108646
    iget-object v5, v2, LX/0pW;->c:LX/0ks;

    invoke-virtual {v5}, LX/0ks;->c()V

    .line 108647
    :goto_1
    move v0, v4

    .line 108648
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1

    :cond_3
    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    goto :goto_0

    :cond_4
    move v4, v5

    goto :goto_1
.end method

.method public final ku_()V
    .locals 4

    .prologue
    .line 108624
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    if-eqz v0, :cond_0

    .line 108625
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->t:LX/1CT;

    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v2}, LX/0g5;->D()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/1CT;->a(LX/0g1;IZ)V

    .line 108626
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 108627
    iget-object v1, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v1}, LX/0gC;->r()V

    .line 108628
    invoke-static {v0}, LX/0g2;->J(LX/0g2;)V

    .line 108629
    iget-object v1, v0, LX/0g2;->an:LX/1Iv;

    if-eqz v1, :cond_1

    .line 108630
    iget-object v1, v0, LX/0g2;->an:LX/1Iv;

    invoke-virtual {v1}, LX/1Iv;->h()V

    .line 108631
    :cond_1
    iget-object v1, v0, LX/0g2;->P:LX/1JH;

    .line 108632
    iget-object v2, v1, LX/1JH;->d:LX/1JR;

    invoke-virtual {v2}, LX/1JR;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 108633
    :goto_0
    return-void

    .line 108634
    :cond_2
    iget-object v2, v1, LX/1JH;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$5;

    invoke-direct {v3, v1}, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$5;-><init>(LX/1JH;)V

    const v0, -0x2b482879

    invoke-static {v2, v3, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final kv_()V
    .locals 12

    .prologue
    .line 108580
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 108581
    invoke-static {v0}, LX/0g2;->J(LX/0g2;)V

    .line 108582
    iget-object v1, v0, LX/0g2;->P:LX/1JH;

    .line 108583
    iget-object v2, v1, LX/1JH;->d:LX/1JR;

    invoke-virtual {v2}, LX/1JR;->a()Z

    move-result v2

    if-nez v2, :cond_7

    .line 108584
    :goto_0
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 108585
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    if-nez v0, :cond_1

    .line 108586
    :cond_0
    :goto_1
    return-void

    .line 108587
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    const/4 v3, 0x0

    .line 108588
    iget-object v1, v0, LX/0g2;->o:LX/0ad;

    sget-short v2, LX/0fe;->ab:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 108589
    invoke-static {v0}, LX/0g2;->ah(LX/0g2;)Z

    .line 108590
    invoke-virtual {v0}, LX/0g2;->C()V

    .line 108591
    :cond_2
    iget-object v1, v0, LX/0g2;->o:LX/0ad;

    sget-short v2, LX/0fe;->aa:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 108592
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/0g2;->c(LX/0g2;Z)V

    .line 108593
    :cond_3
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->H:LX/0Yi;

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 108594
    iget-object v2, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v2}, LX/0gC;->v()Z

    move-result v2

    move v2, v2

    .line 108595
    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    iget-boolean v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->r:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->z()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    const/4 v0, 0x1

    .line 108596
    :goto_2
    iget-wide v4, v1, LX/0Yi;->C:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    iget-object v4, v1, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v4, v3}, LX/0Ym;->a(Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 108597
    :cond_5
    const/4 v4, 0x0

    .line 108598
    :goto_3
    move v0, v4

    .line 108599
    if-eqz v0, :cond_0

    .line 108600
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    new-instance v1, LX/DBd;

    invoke-direct {v1, p0}, LX/DBd;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V

    invoke-virtual {v0, v1}, LX/0g7;->b(LX/0fu;)V

    goto :goto_1

    .line 108601
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 108602
    :cond_7
    iget-object v2, v1, LX/1JH;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$4;

    invoke-direct {v3, v1}, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsSubscriber$4;-><init>(LX/1JH;)V

    const v0, 0x5392fd43

    invoke-static {v2, v3, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 108603
    :cond_8
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    iget-wide v6, v1, LX/0Yi;->C:J

    const/4 v5, 0x6

    new-array v5, v5, [LX/0Yj;

    const/4 v8, 0x0

    new-instance v9, LX/0Yj;

    const v10, 0xa0006

    const-string v11, "NNFHotStart"

    invoke-direct {v9, v10, v11}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v9, v5, v8

    const/4 v8, 0x1

    new-instance v9, LX/0Yj;

    const v10, 0xa0038

    const-string v11, "NNFHotStartTTI"

    invoke-direct {v9, v10, v11}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v9, v5, v8

    const/4 v8, 0x2

    new-instance v9, LX/0Yj;

    const v10, 0xa002b

    const-string v11, "NNFHotStartAndRenderTime"

    invoke-direct {v9, v10, v11}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v9, v5, v8

    const/4 v8, 0x3

    new-instance v9, LX/0Yj;

    const v10, 0xa0049

    const-string v11, "NNFHotStartAndCachedRenderTime"

    invoke-direct {v9, v10, v11}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v9, v5, v8

    const/4 v8, 0x4

    new-instance v9, LX/0Yj;

    const v10, 0xa002c

    const-string v11, "NNFHotStartAndFreshRenderTime"

    invoke-direct {v9, v10, v11}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v9, v5, v8

    const/4 v8, 0x5

    new-instance v9, LX/0Yj;

    const v10, 0xa004a

    const-string v11, "NNFHotStartAndFreshRenderTimeNotVisible"

    invoke-direct {v9, v10, v11}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v9, v5, v8

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v6, v7, v5}, LX/0Yl;->a(JLjava/util/List;)LX/0Yl;

    .line 108604
    if-eqz v2, :cond_a

    .line 108605
    const/4 v4, 0x0

    iput-boolean v4, v1, LX/0Yi;->v:Z

    .line 108606
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa0049

    const-string v6, "NNFHotStartAndCachedRenderTime"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->j(ILjava/lang/String;)LX/0Yl;

    .line 108607
    if-eqz v0, :cond_9

    .line 108608
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa004a

    const-string v6, "NNFHotStartAndFreshRenderTimeNotVisible"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->j(ILjava/lang/String;)LX/0Yl;

    .line 108609
    :goto_4
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa0006

    const-string v6, "NNFHotStart"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->d(ILjava/lang/String;)LX/0Yl;

    .line 108610
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa0049

    const-string v6, "NNFHotStartAndCachedRenderTime"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 108611
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa002b

    const-string v6, "NNFHotStartAndRenderTime"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->d(ILjava/lang/String;)LX/0Yl;

    .line 108612
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa0049

    const-string v6, "NNFHotStartAndCachedRenderTime"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->d(ILjava/lang/String;)LX/0Yl;

    .line 108613
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa0038

    const-string v6, "NNFHotStartTTI"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->d(ILjava/lang/String;)LX/0Yl;

    .line 108614
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 108615
    :cond_9
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa002c

    const-string v6, "NNFHotStartAndFreshRenderTime"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->j(ILjava/lang/String;)LX/0Yl;

    goto :goto_4

    .line 108616
    :cond_a
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa002c

    const-string v6, "NNFHotStartAndFreshRenderTime"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->j(ILjava/lang/String;)LX/0Yl;

    .line 108617
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa004a

    const-string v6, "NNFHotStartAndFreshRenderTimeNotVisible"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->j(ILjava/lang/String;)LX/0Yl;

    goto :goto_4

    .line 108618
    :cond_b
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa004a

    const-string v6, "NNFHotStartAndFreshRenderTimeNotVisible"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 108619
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa002b

    const-string v6, "NNFHotStartAndRenderTime"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->d(ILjava/lang/String;)LX/0Yl;

    .line 108620
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa004a

    const-string v6, "NNFHotStartAndFreshRenderTimeNotVisible"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->d(ILjava/lang/String;)LX/0Yl;

    .line 108621
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    const v5, 0xa0038

    const-string v6, "NNFHotStartTTI"

    invoke-virtual {v4, v5, v6}, LX/0Yl;->d(ILjava/lang/String;)LX/0Yl;

    .line 108622
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 108623
    :cond_c
    const/4 v4, 0x1

    goto/16 :goto_3
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 108578
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v0}, LX/0g2;->l()V

    .line 108579
    return-void
.end method

.method public final lr_()V
    .locals 1

    .prologue
    .line 108572
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->lr_()V

    .line 108573
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->C:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->d()V

    .line 108574
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->i:LX/1B2;

    invoke-virtual {v0}, LX/1B2;->a()V

    .line 108575
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->j:LX/1BE;

    invoke-virtual {v0}, LX/1BE;->a()V

    .line 108576
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->q:LX/1B5;

    invoke-virtual {v0}, LX/1B5;->a()V

    .line 108577
    return-void
.end method

.method public final ls_()V
    .locals 1

    .prologue
    .line 108569
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->C:LX/1CY;

    if-eqz v0, :cond_0

    .line 108570
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->C:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->d()V

    .line 108571
    :cond_0
    return-void
.end method

.method public final lt_()V
    .locals 1

    .prologue
    .line 108566
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->C:LX/1CY;

    if-eqz v0, :cond_0

    .line 108567
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->C:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->c()V

    .line 108568
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 108564
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v0}, LX/0g2;->m()V

    .line 108565
    return-void
.end method

.method public final mJ_()V
    .locals 2

    .prologue
    .line 108562
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    sget-object v1, LX/0gf;->AUTO_REFRESH:LX/0gf;

    invoke-virtual {v0, v1}, LX/0g2;->c(LX/0gf;)V

    .line 108563
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 108558
    iput-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->k:LX/1UQ;

    .line 108559
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v0, v1}, LX/0g2;->a(LX/1UQ;)V

    .line 108560
    iput-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->M:LX/1Pf;

    .line 108561
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 108556
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v0}, LX/0g2;->o()V

    .line 108557
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 108545
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 108546
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 108547
    :cond_0
    :goto_0
    return-void

    .line 108548
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 108549
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->R:LX/1EV;

    .line 108550
    iget-object v1, v0, LX/1EV;->a:LX/0Uh;

    const/16 v2, 0x30a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 108551
    if-eqz v0, :cond_0

    .line 108552
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 108553
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->L:LX/DJ6;

    if-nez v1, :cond_2

    .line 108554
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->S:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DJ6;

    iput-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->L:LX/DJ6;

    .line 108555
    :cond_2
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->L:LX/DJ6;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    sget-object v2, LX/21D;->NEWSFEED:LX/21D;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, LX/DJ6;->a(Ljava/lang/String;LX/21D;Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6dc
        :pswitch_0
    .end packed-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 13

    .prologue
    .line 108706
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->E:LX/1AV;

    .line 108707
    iget-object v1, v0, LX/1AV;->a:LX/1Aa;

    .line 108708
    iget-object v0, v1, LX/1Aa;->k:Landroid/view/View;

    move-object v1, v0

    .line 108709
    move-object v0, v1

    .line 108710
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 108711
    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z

    move-result v1

    if-nez v1, :cond_1

    .line 108712
    :cond_0
    :goto_0
    return-void

    .line 108713
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->I()V

    .line 108714
    iget-boolean v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->p:Z

    if-eqz v1, :cond_0

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    instance-of v1, v0, LX/392;

    if-eqz v1, :cond_0

    .line 108715
    check-cast v0, LX/392;

    .line 108716
    invoke-interface {v0}, LX/392;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v3, v3, LX/1Aw;->x:LX/0sV;

    .line 108717
    iget-object v4, v3, LX/0sV;->w:LX/0Uh;

    const/16 v5, 0x2a9

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    move v3, v4

    .line 108718
    if-nez v3, :cond_3

    .line 108719
    :cond_2
    :goto_1
    goto :goto_0

    .line 108720
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, LX/0f8;

    invoke-static {v3, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0f8;

    .line 108721
    if-eqz v3, :cond_2

    invoke-interface {v3}, LX/0f8;->g()Z

    move-result v4

    if-nez v4, :cond_2

    .line 108722
    const/4 v10, 0x0

    .line 108723
    invoke-interface {v0}, LX/392;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v7

    .line 108724
    iget-object v8, v7, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v7, v8

    .line 108725
    invoke-static {v7}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v7

    .line 108726
    if-nez v7, :cond_5

    .line 108727
    :cond_4
    :goto_2
    move-object v4, v10

    .line 108728
    if-eqz v4, :cond_2

    .line 108729
    invoke-interface {v0}, LX/392;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v5

    .line 108730
    invoke-interface {v3}, LX/0f8;->i()LX/0hE;

    move-result-object v3

    .line 108731
    new-instance v6, LX/DBc;

    invoke-direct {v6, p0, v5, v4, v0}, LX/DBc;-><init>(Lcom/facebook/feed/fragment/NewsFeedFragment;Lcom/facebook/video/player/RichVideoPlayer;LX/395;LX/392;)V

    invoke-interface {v3, v6}, LX/0hE;->a(LX/394;)Ljava/lang/Object;

    .line 108732
    const/4 v5, 0x1

    .line 108733
    iput-boolean v5, v4, LX/395;->m:Z

    .line 108734
    invoke-interface {v3, v4}, LX/0hE;->a(LX/395;)V

    goto :goto_1

    .line 108735
    :cond_5
    invoke-static {v7}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    invoke-static {v8}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v11

    .line 108736
    if-eqz v11, :cond_4

    .line 108737
    new-instance v8, LX/0AV;

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v8

    .line 108738
    invoke-static {v7}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v12

    .line 108739
    new-instance v7, LX/0AW;

    invoke-static {v12}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v9

    invoke-direct {v7, v9}, LX/0AW;-><init>(LX/162;)V

    invoke-virtual {v7}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v9

    .line 108740
    new-instance v7, LX/395;

    invoke-direct/range {v7 .. v12}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 108741
    invoke-interface {v0}, LX/392;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v8

    .line 108742
    invoke-interface {v0}, LX/392;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v9

    .line 108743
    invoke-virtual {v7, v8}, LX/395;->a(I)LX/395;

    .line 108744
    invoke-virtual {v7, v9}, LX/395;->b(I)LX/395;

    .line 108745
    sget-object v8, LX/04D;->FEED:LX/04D;

    invoke-virtual {v7, v8}, LX/395;->a(LX/04D;)LX/395;

    .line 108746
    sget-object v8, LX/04g;->BY_ORIENTATION_CHANGE:LX/04g;

    .line 108747
    iput-object v8, v7, LX/395;->j:LX/04g;

    .line 108748
    move-object v10, v7

    .line 108749
    goto :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4acd6f96    # 6731723.0f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 108750
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->H:LX/0Yi;

    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    const/4 v8, 0x1

    .line 108751
    invoke-static {v1}, LX/0Yi;->G(LX/0Yi;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 108752
    const v5, 0xa0039

    const-string v6, "NNFFragmentViewCreate"

    invoke-static {v1, v5, v6, v8}, LX/0Yi;->a(LX/0Yi;ILjava/lang/String;Z)V

    .line 108753
    :goto_0
    const-string v5, "FragmentOnCreateTime"

    invoke-static {v1, v5, v2}, LX/0Yi;->b(LX/0Yi;Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)V

    .line 108754
    iget-object v5, v1, LX/0Yi;->j:LX/0Yl;

    invoke-virtual {v1, v2}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v6

    const-string v7, "FragmentOnViewCreateTime"

    invoke-virtual {v1, v7, v2}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v8}, LX/0Yl;->b(ILjava/lang/String;Z)LX/0Yl;

    .line 108755
    const-wide/16 v5, -0x1

    iput-wide v5, v1, LX/0Yi;->C:J

    .line 108756
    invoke-direct {p0, p1, p2}, Lcom/facebook/feed/fragment/NewsFeedFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/facebook/widget/CustomFrameLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->l:Lcom/facebook/widget/CustomFrameLayout;

    .line 108757
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->l:Lcom/facebook/widget/CustomFrameLayout;

    const/16 v2, 0x2b

    const v3, 0x55b1803c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1

    .line 108758
    :cond_0
    invoke-static {v1, v2}, LX/0Yi;->h(LX/0Yi;Lcom/facebook/api/feedtype/FeedType;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x51cf9bd1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 108759
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    const/4 v7, 0x0

    .line 108760
    iget-object v2, v1, LX/0g2;->n:LX/0pV;

    sget-object v4, LX/0g2;->e:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0rj;->RELEASED_LOADER:LX/0rj;

    iget-object v6, v1, LX/0g2;->b:LX/0gC;

    invoke-interface {v6}, LX/0gC;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v6}, LX/0pV;->a(Ljava/lang/String;LX/0rj;Ljava/lang/String;)V

    .line 108761
    iget-object v2, v1, LX/0g2;->p:LX/0jU;

    invoke-virtual {v2, v1}, LX/0jU;->b(LX/0fi;)V

    .line 108762
    iget-object v2, v1, LX/0g2;->k:LX/0pG;

    iget-object v4, v1, LX/0g2;->b:LX/0gC;

    .line 108763
    invoke-interface {v4}, LX/0gC;->g()V

    .line 108764
    invoke-interface {v4}, LX/0gC;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v5

    iget-object v6, v2, LX/0pG;->d:LX/0Ym;

    invoke-virtual {v6}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v6

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 108765
    :goto_0
    iget-object v2, v1, LX/0g2;->b:LX/0gC;

    invoke-interface {v2, v1}, LX/0gC;->b(LX/0g3;)V

    .line 108766
    iget-object v2, v1, LX/0g2;->b:LX/0gC;

    invoke-interface {v2}, LX/0gC;->hU_()V

    .line 108767
    iget-object v2, v1, LX/0g2;->b:LX/0gC;

    invoke-interface {v2, v7}, LX/0gC;->a(LX/1EM;)V

    .line 108768
    iget-object v2, v1, LX/0g2;->aa:LX/1EM;

    .line 108769
    iput-object v7, v2, LX/1EM;->g:LX/0pR;

    .line 108770
    iget-object v2, v1, LX/0g2;->an:LX/1Iv;

    if-eqz v2, :cond_0

    .line 108771
    iget-object v2, v1, LX/0g2;->an:LX/1Iv;

    .line 108772
    invoke-virtual {v2}, LX/1Iv;->h()V

    .line 108773
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->C:LX/1CY;

    invoke-virtual {v1}, LX/1CY;->a()V

    .line 108774
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->C:LX/1CY;

    invoke-virtual {v1}, LX/1CY;->e()V

    .line 108775
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->n:LX/1CF;

    if-eqz v1, :cond_1

    .line 108776
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->n:LX/1CF;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 108777
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->B:LX/0jU;

    invoke-virtual {v1, p0}, LX/0jU;->b(LX/0fi;)V

    .line 108778
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroy()V

    .line 108779
    const/16 v1, 0x2b

    const v2, -0x4d38c414

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 108780
    :cond_2
    invoke-interface {v4}, LX/0gC;->q()V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5aa65ec2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 108781
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->z:LX/1CU;

    .line 108782
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/1CU;->i:Z

    .line 108783
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->H:LX/0Yi;

    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    .line 108784
    iget-object v4, v1, LX/0Yi;->j:LX/0Yl;

    invoke-virtual {v1, v2}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v5

    const-string v6, "FragmentCreate"

    invoke-virtual {v1, v6, v2}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0Yl;->j(ILjava/lang/String;)LX/0Yl;

    .line 108785
    const-wide/16 v4, -0x1

    iput-wide v4, v1, LX/0Yi;->C:J

    .line 108786
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 108787
    if-eqz v1, :cond_0

    .line 108788
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->d:LX/03V;

    const-string v2, "news_feed_implementation"

    invoke-virtual {v1, v2}, LX/03V;->a(Ljava/lang/String;)V

    .line 108789
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->r:LX/1CN;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 108790
    invoke-static {v1}, LX/1CN;->a(LX/1CN;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 108791
    :goto_0
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->e:LX/0bH;

    if-eqz v1, :cond_1

    .line 108792
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->e:LX/0bH;

    new-instance v2, LX/1qr;

    invoke-direct {v2}, LX/1qr;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 108793
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->g:LX/1B1;

    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->e:LX/0bH;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 108794
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->L:LX/DJ6;

    if-eqz v1, :cond_2

    .line 108795
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->L:LX/DJ6;

    invoke-virtual {v1}, LX/DJ6;->a()V

    .line 108796
    :cond_2
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->N:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->O:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 108797
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->C:LX/1CY;

    invoke-virtual {v1}, LX/1CY;->c()V

    .line 108798
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->T:LX/1EW;

    .line 108799
    const/4 v2, 0x0

    iput-object v2, v1, LX/1EW;->c:Landroid/view/View;

    .line 108800
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onPause()V

    .line 108801
    const/16 v1, 0x2b

    const v2, 0x72bb9257

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 108802
    :cond_3
    iget-object v4, v1, LX/1CN;->d:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 108803
    iget-object v4, v1, LX/1CN;->d:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 108804
    iget-object v4, v1, LX/1CN;->e:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1CO;

    .line 108805
    iget-object v6, v1, LX/1CN;->a:LX/0bH;

    invoke-virtual {v6, v4}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_1

    .line 108806
    :cond_4
    goto :goto_0
.end method

.method public final onResume()V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x42d90130

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 108807
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->z:LX/1CU;

    .line 108808
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/1CU;->i:Z

    .line 108809
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->H:LX/0Yi;

    .line 108810
    invoke-static {v0}, LX/0Yi;->K(LX/0Yi;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0xa0023

    const-string v6, "NNFWarmStartAndRenderTime"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0xa003d

    invoke-interface {v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 108811
    iget-object v4, v0, LX/0Yi;->k:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v0, LX/0Yi;->C:J

    .line 108812
    :cond_0
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onResume()V

    .line 108813
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    const/4 v3, 0x0

    .line 108814
    iget-object v2, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v2}, LX/0gC;->D()V

    .line 108815
    iget-object v2, v0, LX/0g2;->o:LX/0ad;

    sget-short v4, LX/0fe;->aa:S

    invoke-interface {v2, v4, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_e

    const/4 v2, 0x1

    :goto_0
    invoke-static {v0, v2}, LX/0g2;->c(LX/0g2;Z)V

    .line 108816
    iget-object v2, v0, LX/0g2;->c:LX/0fz;

    move-object v2, v2

    .line 108817
    invoke-virtual {v2}, LX/0fz;->x()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108818
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 108819
    if-eqz v2, :cond_1

    iget-object v2, v0, LX/0g2;->J:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0eJ;->l:LX/0Tn;

    invoke-interface {v2, v4, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108820
    iget-object v2, v0, LX/0g2;->T:Landroid/content/Context;

    const-string v4, "Beta only: warm start re-ranking"

    invoke-static {v2, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 108821
    :cond_1
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 108822
    sget-boolean v2, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->b:Z

    move v2, v2

    .line 108823
    if-eqz v2, :cond_2

    .line 108824
    iget-object v2, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v2}, LX/0gC;->m()V

    .line 108825
    :cond_2
    invoke-static {v0}, LX/0g2;->L(LX/0g2;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v0, LX/0g2;->b:LX/0gC;

    invoke-interface {v2}, LX/0gC;->o()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 108826
    iget-object v2, v0, LX/0g2;->af:LX/0g5;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/0g7;->g(I)V

    .line 108827
    :cond_3
    invoke-static {v0}, LX/0g2;->J(LX/0g2;)V

    .line 108828
    iget-object v2, v0, LX/0g2;->B:LX/0pT;

    invoke-virtual {v2}, LX/0pT;->d()V

    .line 108829
    iget-object v2, v0, LX/0g2;->o:LX/0ad;

    sget-short v4, LX/0fe;->ab:S

    invoke-interface {v2, v4, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_f

    .line 108830
    invoke-static {v0}, LX/0g2;->ah(LX/0g2;)Z

    move-result v2

    .line 108831
    :goto_1
    if-eqz v2, :cond_4

    iget-object v2, v0, LX/0g2;->o:LX/0ad;

    sget-short v4, LX/0fe;->J:S

    invoke-interface {v2, v4, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 108832
    invoke-virtual {v0}, LX/0g2;->C()V

    .line 108833
    :cond_4
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->c:LX/1Ay;

    invoke-virtual {v0}, LX/1Ay;->a()V

    .line 108834
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Resuming NNFragment for feed "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 108835
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 108836
    if-eqz v0, :cond_5

    .line 108837
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->d:LX/03V;

    const-string v2, "news_feed_implementation"

    const-string v3, "native_feed"

    invoke-virtual {v0, v2, v3}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 108838
    :cond_5
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->e:LX/0bH;

    if-eqz v0, :cond_6

    .line 108839
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->g:LX/1B1;

    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v2, v2, LX/1Aw;->e:LX/0bH;

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b4;)V

    .line 108840
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->e:LX/0bH;

    new-instance v2, LX/0bI;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0bI;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 108841
    :cond_6
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->P:LX/1ER;

    invoke-virtual {v0}, LX/1ER;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 108842
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DbC;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    const/4 v6, -0x2

    .line 108843
    iget-object v3, v0, LX/DbC;->a:Landroid/widget/TextView;

    if-nez v3, :cond_8

    .line 108844
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, v0, LX/DbC;->a:Landroid/widget/TextView;

    .line 108845
    iget-object v3, v0, LX/DbC;->a:Landroid/widget/TextView;

    const-string v4, "2G Empathy Enabled"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108846
    iget-object v3, v0, LX/DbC;->a:Landroid/widget/TextView;

    const/4 v4, 0x2

    const/high16 v5, 0x41000000    # 8.0f

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 108847
    iget-object v3, v0, LX/DbC;->a:Landroid/widget/TextView;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 108848
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v3

    .line 108849
    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v4

    .line 108850
    iget-object v5, v0, LX/DbC;->a:Landroid/widget/TextView;

    invoke-virtual {v5, v3, v4, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 108851
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 108852
    const/16 v4, 0x10

    if-lt v3, v4, :cond_7

    .line 108853
    iget-object v3, v0, LX/DbC;->a:Landroid/widget/TextView;

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    const/4 v5, -0x1

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 108854
    :cond_7
    iget-object v3, v0, LX/DbC;->a:Landroid/widget/TextView;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 108855
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, 0x1

    invoke-direct {v3, v6, v6, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 108856
    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    iget-object v5, v0, LX/DbC;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v5, v3}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 108857
    :cond_8
    iget-object v3, v0, LX/DbC;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0dU;->m:LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    invoke-virtual {v0, v3}, LX/DbC;->a(Z)V

    .line 108858
    :cond_9
    iget-object v4, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v4, v4, LX/1Aw;->o:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/User;

    .line 108859
    if-nez v4, :cond_10

    .line 108860
    :cond_a
    :goto_2
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->C:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->d()V

    .line 108861
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->r:LX/1CN;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1CN;->a(Ljava/lang/Class;)V

    .line 108862
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->H:LX/0Yi;

    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v3

    invoke-virtual {v3}, LX/0fz;->x()Z

    const v7, 0xa0053

    const/4 v6, 0x1

    .line 108863
    const-string v3, "FragmentCreate"

    invoke-static {v0, v3, v2}, LX/0Yi;->b(LX/0Yi;Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)V

    .line 108864
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    invoke-virtual {v0, v2}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v4

    const-string v5, "FragmentResumeToRender"

    invoke-virtual {v0, v5, v2}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0Yl;->h(ILjava/lang/String;)LX/0Yl;

    .line 108865
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    const v4, 0xa0001

    const-string v5, "NNFColdStart"

    invoke-virtual {v3, v4, v5}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 108866
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    const-string v4, "NNFCold_FragmentCreateToDataFetched"

    invoke-virtual {v3, v7, v4, v6}, LX/0Yl;->a(ILjava/lang/String;Z)LX/0Yl;

    .line 108867
    iget-boolean v3, v0, LX/0Yi;->t:Z

    if-eqz v3, :cond_b

    .line 108868
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    const-string v4, "NNFCold_FragmentCreateToDataFetched"

    invoke-virtual {v3, v7, v4}, LX/0Yl;->c(ILjava/lang/String;)LX/0Yl;

    .line 108869
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    const v4, 0xa0048

    const-string v5, "NNFCold_DataFetchedToFirstRender"

    invoke-virtual {v3, v4, v5}, LX/0Yl;->a(ILjava/lang/String;)LX/0Yl;

    .line 108870
    :cond_b
    :goto_3
    iput-boolean v6, v0, LX/0Yi;->u:Z

    .line 108871
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v0, p0}, LX/0g7;->b(LX/0fu;)V

    .line 108872
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->T:LX/1EW;

    .line 108873
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 108874
    iput-object v2, v0, LX/1EW;->c:Landroid/view/View;

    .line 108875
    iget v3, v0, LX/1EW;->e:I

    if-lez v3, :cond_c

    iget-boolean v3, v0, LX/1EW;->f:Z

    if-nez v3, :cond_c

    .line 108876
    iget-object v3, v0, LX/1EW;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0130

    iget v5, v0, LX/1EW;->e:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v0, LX/1EW;->e:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/1EW;->a(LX/1EW;Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v0, LX/1EW;->f:Z

    .line 108877
    :cond_c
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->V:LX/0hg;

    .line 108878
    iget-boolean v2, v0, LX/0hg;->f:Z

    if-eqz v2, :cond_d

    .line 108879
    iget-object v2, v0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0xbc0002

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 108880
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/0hg;->f:Z

    .line 108881
    :cond_d
    const v0, -0x636d76d6

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    :cond_e
    move v2, v3

    .line 108882
    goto/16 :goto_0

    :cond_f
    move v2, v3

    goto/16 :goto_1

    .line 108883
    :cond_10
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v5

    .line 108884
    if-eqz v5, :cond_a

    .line 108885
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/facebook/attribution/AttributionIdService;->a(Landroid/content/Context;J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 108886
    :catch_0
    move-exception v4

    .line 108887
    sget-object v6, Lcom/facebook/feed/fragment/NewsFeedFragment;->c:Ljava/lang/Class;

    const-string v7, "Not a UserId: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    invoke-static {v6, v4, v7, v8}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 108888
    :cond_11
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    const v4, 0xa0004

    const-string v5, "NNFWarmStart"

    invoke-virtual {v3, v4, v5}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 108889
    iget-object v3, v0, LX/0Yi;->j:LX/0Yl;

    const v4, 0xa0047

    const-string v5, "NNFWarm_FragmentCreateToDataFetched"

    invoke-virtual {v3, v4, v5}, LX/0Yl;->b(ILjava/lang/String;)LX/0Yl;

    goto/16 :goto_3
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 108890
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 108891
    const-string v0, "feed_type"

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->o:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 108892
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v0, p1}, LX/0g2;->a(Landroid/os/Bundle;)V

    .line 108893
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x7e6d5a5e

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 108894
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStart()V

    .line 108895
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 108896
    iget-object v2, v0, LX/0g2;->h:LX/1J3;

    .line 108897
    iget-object v3, v2, LX/1J3;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 108898
    iget-object v3, v2, LX/1J3;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108899
    :cond_0
    iget-object v2, v0, LX/0g2;->i:LX/1J6;

    .line 108900
    iput-object v0, v2, LX/1J6;->c:LX/0g2;

    .line 108901
    iget-object v2, v0, LX/0g2;->af:LX/0g5;

    iget-object v3, v0, LX/0g2;->h:LX/1J3;

    invoke-virtual {v2, v3}, LX/0g7;->b(LX/0fx;)V

    .line 108902
    iget-object v2, v0, LX/0g2;->af:LX/0g5;

    iget-object v3, v0, LX/0g2;->ac:LX/0fx;

    invoke-virtual {v2, v3}, LX/0g7;->b(LX/0fx;)V

    .line 108903
    iget-object v2, v0, LX/0g2;->af:LX/0g5;

    iget-object v3, v0, LX/0g2;->ad:LX/0fx;

    invoke-virtual {v2, v3}, LX/0g7;->b(LX/0fx;)V

    .line 108904
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 108905
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 108906
    if-eqz v0, :cond_1

    const-string v3, "should_update_title_bar"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 108907
    const-string v3, "list_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 108908
    const-string v3, "list_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 108909
    :goto_0
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 108910
    :cond_1
    const v0, -0x503701f1

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 108911
    :cond_2
    sget-object v2, Lcom/facebook/api/feedtype/FeedType;->a:Lcom/facebook/api/feedtype/FeedType;

    invoke-static {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->B(Lcom/facebook/feed/fragment/NewsFeedFragment;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 108912
    sget v2, Lcom/facebook/feed/fragment/NewsFeedFragment;->e:I

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    goto :goto_0

    .line 108913
    :cond_3
    sget v2, Lcom/facebook/feed/fragment/NewsFeedFragment;->d:I

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2d7015a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 108914
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStop()V

    .line 108915
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 108916
    iget-object v2, v1, LX/0g2;->af:LX/0g5;

    iget-object v4, v1, LX/0g2;->h:LX/1J3;

    invoke-virtual {v2, v4}, LX/0g7;->c(LX/0fx;)V

    .line 108917
    iget-object v2, v1, LX/0g2;->af:LX/0g5;

    iget-object v4, v1, LX/0g2;->ac:LX/0fx;

    invoke-virtual {v2, v4}, LX/0g7;->c(LX/0fx;)V

    .line 108918
    iget-object v2, v1, LX/0g2;->af:LX/0g5;

    iget-object v4, v1, LX/0g2;->ad:LX/0fx;

    invoke-virtual {v2, v4}, LX/0g7;->c(LX/0fx;)V

    .line 108919
    iget-object v2, v1, LX/0g2;->h:LX/1J3;

    .line 108920
    iget-object v4, v2, LX/1J3;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 108921
    iget-object v2, v1, LX/0g2;->i:LX/1J6;

    const/4 v4, 0x0

    .line 108922
    iput-object v4, v2, LX/1J6;->c:LX/0g2;

    .line 108923
    iget-object v2, v1, LX/0g2;->i:LX/1J6;

    .line 108924
    iget-object v4, v2, LX/1J6;->p:LX/1J8;

    sget-object p0, LX/1J8;->TIMER_STARTED:LX/1J8;

    if-ne v4, p0, :cond_0

    .line 108925
    iget-object v4, v2, LX/1J6;->q:Landroid/os/Handler;

    iget-object p0, v2, LX/1J6;->r:Ljava/lang/Runnable;

    invoke-static {v4, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 108926
    sget-object v4, LX/1J8;->USER_LEFT_FEED:LX/1J8;

    .line 108927
    iput-object v4, v2, LX/1J6;->p:LX/1J8;

    .line 108928
    :cond_0
    invoke-static {v1}, LX/0g2;->J(LX/0g2;)V

    .line 108929
    iget-object v2, v1, LX/0g2;->b:LX/0gC;

    invoke-interface {v2}, LX/0gC;->C()V

    .line 108930
    const/16 v1, 0x2b

    const v2, 0x164d3e29

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 108931
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->N:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->O:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 108932
    const/4 v0, 0x0

    return v0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 108933
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    invoke-virtual {v0}, LX/0g2;->s()V

    .line 108934
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 4

    .prologue
    .line 108935
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->setUserVisibleHint(Z)V

    .line 108936
    iget-boolean v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->p:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    if-eqz v0, :cond_0

    .line 108937
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->t:LX/1CT;

    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v2}, LX/0g5;->D()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1CT;->a(LX/0g1;IZ)V

    .line 108938
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->p:Z

    .line 108939
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 108940
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 108941
    iget-object v1, v0, LX/0g2;->ag:LX/4nS;

    if-eqz v1, :cond_1

    .line 108942
    iget-object v1, v0, LX/0g2;->ag:LX/4nS;

    invoke-virtual {v1}, LX/4nS;->b()V

    .line 108943
    :cond_1
    return-void
.end method

.method public final t()LX/0fz;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 108944
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    if-nez v0, :cond_0

    .line 108945
    const/4 v0, 0x0

    .line 108946
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->K:LX/0g2;

    .line 108947
    iget-object p0, v0, LX/0g2;->c:LX/0fz;

    move-object v0, p0

    .line 108948
    goto :goto_0
.end method

.method public final u()LX/0qq;
    .locals 1

    .prologue
    .line 108949
    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v0

    .line 108950
    iget-object p0, v0, LX/0fz;->a:LX/0qq;

    move-object v0, p0

    .line 108951
    return-object v0
.end method

.method public final x()V
    .locals 4

    .prologue
    .line 108952
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->m:LX/13l;

    sget-object v1, LX/04g;->BY_MANAGER:LX/04g;

    invoke-virtual {v0, v1}, LX/13l;->a(LX/04g;)V

    .line 108953
    iget-object v0, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->I:LX/1KS;

    .line 108954
    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 108955
    const/4 v1, 0x1

    move v1, v1

    .line 108956
    if-eqz v1, :cond_0

    .line 108957
    iget-object v1, v0, LX/1KS;->D:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Km;

    .line 108958
    iget-object v2, v1, LX/1Km;->a:LX/1Kt;

    const/4 v3, 0x0

    iget-object p0, v1, LX/1Km;->f:LX/1DK;

    invoke-interface {p0}, LX/1DK;->a()LX/0g8;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/1Kt;->a(ZLX/0g8;)V

    .line 108959
    :cond_0
    iget-object v1, v0, LX/1KS;->U:LX/0Ot;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/1KS;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/1KS;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Z1;

    invoke-virtual {v1}, LX/0hD;->kw_()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108960
    iget-object v1, v0, LX/1KS;->U:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Z1;

    .line 108961
    iget-object v2, v1, LX/1Z1;->c:Landroid/support/design/widget/AppBarLayout;

    if-eqz v2, :cond_1

    .line 108962
    iget-object v2, v1, LX/1Z1;->c:Landroid/support/design/widget/AppBarLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/design/widget/AppBarLayout;->setExpanded(Z)V

    .line 108963
    :cond_1
    iget-object v1, v0, LX/1KS;->V:LX/0Ot;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/1KS;->V:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/1KS;->V:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;

    invoke-virtual {v1}, LX/0hD;->kw_()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 108964
    iget-object v1, v0, LX/1KS;->V:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;

    .line 108965
    iget-object v2, v1, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->k:Landroid/support/design/widget/AppBarLayout;

    if-eqz v2, :cond_2

    .line 108966
    iget-object v2, v1, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerHeaderBarController;->k:Landroid/support/design/widget/AppBarLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/design/widget/AppBarLayout;->setExpanded(Z)V

    .line 108967
    :cond_2
    return-void
.end method

.method public final z()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 108968
    invoke-virtual {p0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->A()Z

    move-result v1

    if-nez v1, :cond_1

    .line 108969
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v1}, LX/0g7;->n()Z

    move-result v1

    if-nez v1, :cond_0

    .line 108970
    iget-object v1, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->k:LX/1UQ;

    invoke-virtual {v1}, LX/1UQ;->b()I

    move-result v1

    .line 108971
    iget-object v2, p0, Lcom/facebook/feed/fragment/NewsFeedFragment;->j:LX/0g5;

    invoke-virtual {v2}, LX/0g7;->q()I

    move-result v2

    if-ge v2, v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 108972
    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
