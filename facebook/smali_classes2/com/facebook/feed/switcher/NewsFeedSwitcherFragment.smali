.class public Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fj;
.implements LX/0fk;
.implements LX/0fn;
.implements LX/0fv;


# annotations
.annotation runtime Lcom/facebook/search/interfaces/GraphSearchTitleSupport;
.end annotation


# static fields
.field public static final i:Ljava/lang/String;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1LV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Gdb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Gdc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0oJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Yn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GdQ;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0gE;

.field public m:Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

.field private n:Landroid/support/v4/view/ViewPager;

.field private o:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public p:LX/Gda;

.field public q:Z

.field public r:I

.field public s:I

.field private t:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111055
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 111056
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 111057
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    .line 111058
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->k:Ljava/util/List;

    .line 111059
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    .line 111060
    return-void
.end method

.method public static a(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;LX/GdQ;)V
    .locals 2

    .prologue
    .line 111061
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111062
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->k:Ljava/util/List;

    .line 111063
    iget-object v1, p1, LX/GdQ;->b:Ljava/lang/String;

    move-object v1, v1

    .line 111064
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111065
    return-void
.end method

.method public static a(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;LX/Gda;)Z
    .locals 2

    .prologue
    .line 111066
    invoke-virtual {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111067
    const/4 v0, 0x0

    .line 111068
    :goto_0
    return v0

    .line 111069
    :cond_0
    sget-object v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->i:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a$redex0(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;Ljava/lang/String;)I

    move-result v0

    .line 111070
    iget v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    if-ne v0, v1, :cond_1

    .line 111071
    :goto_1
    invoke-static {p0, v0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a$redex0(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;I)V

    .line 111072
    const/4 v0, 0x1

    goto :goto_0

    .line 111073
    :cond_1
    iput-object p1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->p:LX/Gda;

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 111074
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 111075
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GdQ;

    .line 111076
    iget-object v2, v0, LX/GdQ;->b:Ljava/lang/String;

    move-object v0, v2

    .line 111077
    invoke-static {v0, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111078
    :goto_1
    return v1

    .line 111079
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 111080
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;I)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 111081
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d:LX/Gdc;

    iget v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    iget-object v2, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->k:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/Gdc;->a(ILjava/util/List;)V

    .line 111082
    iget v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    if-ne v0, p1, :cond_1

    .line 111083
    invoke-virtual {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111084
    invoke-virtual {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->f()V

    .line 111085
    :cond_0
    :goto_0
    return-void

    .line 111086
    :cond_1
    invoke-static {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 111087
    if-eqz v0, :cond_2

    .line 111088
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 111089
    :cond_2
    invoke-static {p0, p1}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;I)V

    .line 111090
    invoke-static {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 111091
    if-eqz v0, :cond_0

    .line 111092
    iget-boolean v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->t:Z

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    goto :goto_0
.end method

.method public static d(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;I)V
    .locals 2

    .prologue
    .line 111093
    iput p1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    .line 111094
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 111095
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 111096
    :cond_0
    return-void
.end method

.method public static m(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 111097
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    if-eqz v0, :cond_0

    .line 111098
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    iget v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    invoke-virtual {v0, v1}, LX/0gF;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 111099
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 111100
    sget-object v0, LX/Gda;->BACK_BUTTON:LX/Gda;

    invoke-static {p0, v0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;LX/Gda;)Z

    move-result v0

    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 110933
    invoke-static {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 110934
    instance-of v1, v0, LX/0fh;

    if-eqz v1, :cond_0

    .line 110935
    check-cast v0, LX/0fh;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    .line 110936
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public final a(LX/5Mj;)V
    .locals 2

    .prologue
    .line 111033
    invoke-static {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 111034
    iget-object v1, p1, LX/5Mj;->f:LX/5Mk;

    move-object v1, v1

    .line 111035
    invoke-virtual {v1, v0, p1}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 111036
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 111037
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 111038
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    const/16 v3, 0x259

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v4

    check-cast v4, LX/1LV;

    new-instance v6, LX/Gdb;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v5

    check-cast v5, LX/0gh;

    invoke-direct {v6, v5}, LX/Gdb;-><init>(LX/0gh;)V

    move-object v5, v6

    check-cast v5, LX/Gdb;

    invoke-static {v0}, LX/Gdc;->b(LX/0QB;)LX/Gdc;

    move-result-object v6

    check-cast v6, LX/Gdc;

    invoke-static {v0}, LX/0oJ;->b(LX/0QB;)LX/0oJ;

    move-result-object v7

    check-cast v7, LX/0oJ;

    invoke-static {v0}, LX/0Yn;->a(LX/0QB;)LX/0Yn;

    move-result-object v8

    check-cast v8, LX/0Yn;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object p1

    check-cast p1, Landroid/os/Handler;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    iput-object v3, v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->b:LX/1LV;

    iput-object v5, v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->c:LX/Gdb;

    iput-object v6, v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d:LX/Gdc;

    iput-object v7, v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->e:LX/0oJ;

    iput-object v8, v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->f:LX/0Yn;

    iput-object p1, v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->g:Landroid/os/Handler;

    iput-object v0, v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->h:LX/0W3;

    .line 111039
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 111040
    if-eqz v0, :cond_0

    .line 111041
    const-string v1, "newsfeed_fragment_builder"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    iput-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m:Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    .line 111042
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m:Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    if-nez v0, :cond_1

    .line 111043
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "feeds_switcher_missing_fragment_builder"

    const-string v2, "No Params Passed for Building Home NewsFeedFragment"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111044
    new-instance v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    invoke-direct {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;-><init>()V

    sget-object v1, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 111045
    iput-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 111046
    move-object v0, v0

    .line 111047
    const/4 v1, 0x1

    .line 111048
    iput-boolean v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->c:Z

    .line 111049
    move-object v0, v0

    .line 111050
    iput-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m:Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    .line 111051
    :cond_1
    new-instance v0, LX/0gE;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/0gE;-><init>(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    .line 111052
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    .line 111053
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 111054
    :cond_2
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 110932
    iget v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    invoke-virtual {v1, v0}, LX/0gE;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 110937
    invoke-static {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 110938
    if-eqz v0, :cond_0

    .line 110939
    check-cast v0, LX/0fv;

    invoke-interface {v0}, LX/0fv;->f()V

    .line 110940
    :cond_0
    return-void
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110941
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 110942
    const-string v0, "NewsFeedSwitcherFragment_current_position: "

    iget v2, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110943
    const-string v2, "NewsFeedSwitcherFragment_current_position_itemid: "

    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    iget v3, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GdQ;

    .line 110944
    iget-object v3, v0, LX/GdQ;->b:Ljava/lang/String;

    move-object v0, v3

    .line 110945
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110946
    const-string v0, "NewsFeedSwitcherFragment_switcher_itemIds: "

    iget-object v2, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110947
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 110948
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 110949
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v3

    const-string v4, ""

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v2, v6}, LX/0gc;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 110950
    const-string v2, "NewsFeedSwitcherFragment_fragmentManager_dump: "

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110951
    const-string v0, "NewsFeedSwitcherFragment_local_fragment_hashmap: "

    iget-object v2, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    iget-object v2, v2, LX/0gE;->c:LX/0Ri;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110952
    invoke-static {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 110953
    if-eqz v0, :cond_0

    instance-of v2, v0, LX/0f6;

    if-eqz v2, :cond_0

    .line 110954
    check-cast v0, LX/0f6;

    invoke-interface {v0}, LX/0f6;->getDebugInfo()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 110955
    :cond_0
    return-object v1
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 110956
    invoke-static {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 110957
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/0fv;

    if-eqz v1, :cond_0

    check-cast v0, LX/0fv;

    invoke-interface {v0}, LX/0fv;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 110958
    invoke-static {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 110959
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/0fv;

    invoke-interface {v0}, LX/0fw;->k()LX/0g8;

    move-result-object v0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 110960
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->g:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment$2;-><init>(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;IILandroid/content/Intent;)V

    const v2, 0x54a2166f

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 110961
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x21baeb59

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v5

    .line 110962
    const v0, 0x7f030c06

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 110963
    const v0, 0x7f0d1dc7

    invoke-static {v6, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    .line 110964
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0f87

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 110965
    const v0, 0x7f0d1dc6

    invoke-static {v6, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->o:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 110966
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->h:LX/0W3;

    sget-wide v8, LX/0X5;->cd:J

    invoke-interface {v0, v8, v9}, LX/0W4;->a(J)Z

    move-result v0

    .line 110967
    invoke-virtual {v6}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0f8b

    invoke-static {v1, v2}, LX/0tP;->b(Landroid/content/res/Resources;I)I

    move-result v1

    .line 110968
    invoke-virtual {v6}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    int-to-float v1, v1

    invoke-static {v2, v1}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v1

    .line 110969
    if-eqz v0, :cond_0

    .line 110970
    const/16 v0, 0x30

    move v2, v1

    move v4, v3

    move v1, v0

    .line 110971
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 110972
    invoke-virtual {v0, v3, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 110973
    iget-object v2, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 110974
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->o:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 110975
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 110976
    iget-object v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->o:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 110977
    const v0, 0xe535517

    invoke-static {v0, v5}, LX/02F;->f(II)V

    return-object v6

    .line 110978
    :cond_0
    const/16 v0, 0x50

    move v2, v3

    move v4, v1

    move v1, v0

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 110979
    const-string v0, "current_position"

    iget v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 110980
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 110981
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 110982
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 110983
    iget-object v3, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 110984
    iget-object v3, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 110985
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 110986
    iget-object v4, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->f:LX/0Yn;

    invoke-virtual {v4}, LX/0Yn;->a()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 110987
    iget-object v4, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->f:LX/0Yn;

    .line 110988
    const v5, 0x7f0818b9

    invoke-virtual {v10, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 110989
    iget-object v6, v4, LX/0Yn;->e:LX/0am;

    invoke-virtual {v6}, LX/0am;->isPresent()Z

    move-result v6

    if-nez v6, :cond_0

    .line 110990
    iget-object v6, v4, LX/0Yn;->a:LX/0ad;

    invoke-static {v4}, LX/0Yn;->e(LX/0Yn;)LX/0ak;

    move-result-object v7

    invoke-interface {v7}, LX/0ak;->c()C

    move-result v7

    invoke-interface {v6, v7, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    iput-object v6, v4, LX/0Yn;->e:LX/0am;

    .line 110991
    :cond_0
    iget-object v6, v4, LX/0Yn;->e:LX/0am;

    invoke-virtual {v6, v5}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object v7, v5

    .line 110992
    iget-object v4, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->f:LX/0Yn;

    invoke-virtual {v4}, LX/0Yn;->b()Ljava/lang/String;

    move-result-object v4

    .line 110993
    new-instance v9, Lcom/facebook/api/feedtype/newsfeed/NewsFeedType;

    new-instance v5, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    const-string v6, "TOP_STORIES"

    invoke-direct {v5, v4, v6}, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v6, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-static {v4}, LX/2tT;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v9, v5, v6, v4}, Lcom/facebook/api/feedtype/newsfeed/NewsFeedType;-><init>(Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;Lcom/facebook/api/feedtype/FeedType$Name;Ljava/lang/String;)V

    .line 110994
    :goto_0
    new-instance v4, LX/GdS;

    sget-object v6, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->i:Ljava/lang/String;

    sget-object v8, LX/2zA;->MAIN_FEED:LX/2zA;

    move-object v5, p0

    invoke-direct/range {v4 .. v9}, LX/GdS;-><init>(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;Ljava/lang/String;Ljava/lang/String;LX/2zA;Lcom/facebook/api/feedtype/FeedType;)V

    invoke-static {p0, v4}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;LX/GdQ;)V

    .line 110995
    iget-object v4, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->e:LX/0oJ;

    invoke-virtual {v4}, LX/0oJ;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 110996
    new-instance v4, LX/GdT;

    const-string v5, "good_friends_feed"

    const v6, 0x7f0818ba

    invoke-virtual {v10, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, LX/2zA;->GOOD_FRIENDS_FEED:LX/2zA;

    invoke-direct {v4, p0, v5, v6, v7}, LX/GdT;-><init>(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;Ljava/lang/String;Ljava/lang/String;LX/2zA;)V

    invoke-static {p0, v4}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;LX/GdQ;)V

    .line 110997
    :cond_1
    iget-object v4, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->f:LX/0Yn;

    invoke-virtual {v4}, LX/0Yn;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 110998
    new-instance v4, LX/GdU;

    const-string v5, "split_feed"

    iget-object v6, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->f:LX/0Yn;

    .line 110999
    const v7, 0x7f0818b8

    invoke-virtual {v10, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 111000
    iget-object v8, v6, LX/0Yn;->f:LX/0am;

    invoke-virtual {v8}, LX/0am;->isPresent()Z

    move-result v8

    if-nez v8, :cond_2

    .line 111001
    iget-object v8, v6, LX/0Yn;->a:LX/0ad;

    invoke-static {v6}, LX/0Yn;->e(LX/0Yn;)LX/0ak;

    move-result-object v9

    invoke-interface {v9}, LX/0ak;->e()C

    move-result v9

    invoke-interface {v8, v9, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v8

    iput-object v8, v6, LX/0Yn;->f:LX/0am;

    .line 111002
    :cond_2
    iget-object v8, v6, LX/0Yn;->f:LX/0am;

    invoke-virtual {v8, v7}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object v6, v7

    .line 111003
    sget-object v7, LX/2zA;->SPLIT_FEED:LX/2zA;

    invoke-direct {v4, p0, v5, v6, v7}, LX/GdU;-><init>(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;Ljava/lang/String;Ljava/lang/String;LX/2zA;)V

    invoke-static {p0, v4}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;LX/GdQ;)V

    .line 111004
    :cond_3
    iget-object v3, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    if-eqz v3, :cond_4

    .line 111005
    iget-object v3, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    invoke-virtual {v3}, LX/0gG;->kV_()V

    .line 111006
    :cond_4
    if-nez p2, :cond_8

    .line 111007
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 111008
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a$redex0(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;I)V

    .line 111009
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    if-eqz v0, :cond_6

    .line 111010
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 111011
    :cond_6
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->o:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 111012
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->o:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v1, LX/GdR;

    invoke-direct {v1, p0}, LX/GdR;-><init>(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)V

    .line 111013
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 111014
    iget v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    .line 111015
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->n:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 111016
    :cond_7
    return-void

    .line 111017
    :cond_8
    const-string v0, "current_position"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;I)V

    .line 111018
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    iget v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    invoke-static {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 111019
    if-eqz v2, :cond_9

    if-ltz v1, :cond_9

    iget-object v3, v0, LX/0gE;->b:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v3, v3, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le v1, v3, :cond_b

    .line 111020
    :cond_9
    :goto_2
    goto :goto_1

    .line 111021
    :cond_a
    const v4, 0x7f0818b9

    invoke-virtual {v10, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 111022
    sget-object v9, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    goto/16 :goto_0

    .line 111023
    :cond_b
    iget-object v3, v0, LX/0gE;->b:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v3, v3, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->j:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/GdQ;

    .line 111024
    iget-object v4, v0, LX/0gE;->c:LX/0Ri;

    .line 111025
    iget-object v0, v3, LX/GdQ;->b:Ljava/lang/String;

    move-object v3, v0

    .line 111026
    invoke-interface {v4, v3, v2}, LX/0Ri;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method public final setUserVisibleHint(Z)V
    .locals 3

    .prologue
    .line 111027
    iput-boolean p1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->t:Z

    .line 111028
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->b:LX/1LV;

    if-eqz v0, :cond_0

    .line 111029
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->b:LX/1LV;

    invoke-virtual {p0}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1LV;->a(Ljava/lang/String;)V

    .line 111030
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d:LX/Gdc;

    if-eqz v0, :cond_1

    .line 111031
    iget-object v0, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d:LX/Gdc;

    iget v1, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    iget-object v2, p0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->k:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/Gdc;->a(ILjava/util/List;)V

    .line 111032
    :cond_1
    return-void
.end method
