.class public Lcom/facebook/feed/ui/NewsFeedRecyclerView;
.super Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.source ""


# instance fields
.field public i:LX/198;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 238777
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 238778
    invoke-direct {p0}, Lcom/facebook/feed/ui/NewsFeedRecyclerView;->p()V

    .line 238779
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 238774
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 238775
    invoke-direct {p0}, Lcom/facebook/feed/ui/NewsFeedRecyclerView;->p()V

    .line 238776
    return-void
.end method

.method private static a(III)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 238767
    if-ltz p0, :cond_0

    if-gtz p1, :cond_1

    .line 238768
    :cond_0
    :goto_0
    return v0

    .line 238769
    :cond_1
    if-nez p0, :cond_2

    .line 238770
    :goto_1
    int-to-float v1, p0

    int-to-float v2, p1

    int-to-float v0, v0

    int-to-float v3, p2

    div-float/2addr v0, v3

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    .line 238771
    :cond_2
    add-int v0, p0, p1

    if-ne v0, p2, :cond_3

    .line 238772
    add-int/lit8 v0, p2, -0x1

    goto :goto_1

    .line 238773
    :cond_3
    div-int/lit8 v0, p1, 0x2

    add-int/2addr v0, p0

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/ui/NewsFeedRecyclerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/ui/NewsFeedRecyclerView;

    invoke-static {v0}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v0

    check-cast v0, LX/198;

    iput-object v0, p0, Lcom/facebook/feed/ui/NewsFeedRecyclerView;->i:LX/198;

    return-void
.end method

.method private p()V
    .locals 1

    .prologue
    .line 238764
    const-class v0, Lcom/facebook/feed/ui/NewsFeedRecyclerView;

    invoke-static {v0, p0}, Lcom/facebook/feed/ui/NewsFeedRecyclerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 238765
    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->l()V

    .line 238766
    return-void
.end method


# virtual methods
.method public final o()I
    .locals 3

    .prologue
    .line 238763
    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getBetterLayoutManager()LX/1OS;

    move-result-object v0

    invoke-interface {v0}, LX/1OS;->l()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feed/ui/NewsFeedRecyclerView;->getChildCount()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v2

    invoke-virtual {v2}, LX/1OR;->D()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/facebook/feed/ui/NewsFeedRecyclerView;->a(III)I

    move-result v0

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 238753
    const-string v0, "NewsFeedRecyclerView.onLayout"

    const v1, 0x61cd2c3a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 238754
    :try_start_0
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->onLayout(ZIIII)V

    .line 238755
    iget-object v0, p0, Lcom/facebook/feed/ui/NewsFeedRecyclerView;->i:LX/198;

    .line 238756
    sget-object v1, LX/1gK;->a:LX/1gK;

    if-nez v1, :cond_0

    .line 238757
    new-instance v1, LX/1gK;

    invoke-direct {v1}, LX/1gK;-><init>()V

    sput-object v1, LX/1gK;->a:LX/1gK;

    .line 238758
    :cond_0
    sget-object v1, LX/1gK;->a:LX/1gK;

    move-object v1, v1

    .line 238759
    invoke-virtual {v0, v1}, LX/198;->c(LX/1YD;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238760
    const v0, -0x2669a4d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 238761
    return-void

    .line 238762
    :catchall_0
    move-exception v0

    const v1, 0x14899dc2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setOnScrollListener(LX/1OX;)V
    .locals 2

    .prologue
    .line 238752
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use addScrollListener instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
