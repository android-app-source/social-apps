.class public Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:I

.field public final b:LX/1JE;

.field public final c:LX/1JD;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/0tn;

.field public f:LX/1lx;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/1JD;LX/0tn;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 229882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229883
    iput-object p2, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->c:LX/1JD;

    .line 229884
    iput-object p3, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->e:LX/0tn;

    .line 229885
    new-instance v0, LX/1JE;

    invoke-direct {v0, p0}, LX/1JE;-><init>(Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;)V

    iput-object v0, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->b:LX/1JE;

    .line 229886
    iput-object p1, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->d:Ljava/util/concurrent/ExecutorService;

    .line 229887
    iput v1, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->a:I

    .line 229888
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;
    .locals 4

    .prologue
    .line 229889
    new-instance v3, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    const-class v1, LX/1JD;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/1JD;

    invoke-static {p0}, LX/0tn;->a(LX/0QB;)LX/0tn;

    move-result-object v2

    check-cast v2, LX/0tn;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;-><init>(Ljava/util/concurrent/ExecutorService;LX/1JD;LX/0tn;)V

    .line 229890
    move-object v0, v3

    .line 229891
    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;I)V
    .locals 9

    .prologue
    .line 229892
    const-string v0, "FeedPrefetchLoader.handleDataLoaded"

    const v1, 0x3c2643e0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 229893
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->b:LX/1JE;

    .line 229894
    iput-object p2, v0, LX/1JE;->b:LX/0ta;

    .line 229895
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 229896
    add-int/lit8 v2, p3, 0x1

    .line 229897
    iget-object v5, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->f:LX/1lx;

    if-nez v5, :cond_0

    .line 229898
    new-instance v5, LX/1lx;

    const/4 v6, 0x2

    new-array v6, v6, [LX/1JF;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->b:LX/1JE;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->c:LX/1JD;

    sget-object p1, LX/1Li;->NEWSFEED:LX/1Li;

    const-class p2, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    invoke-static {p2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p2

    invoke-virtual {v8, p1, p2}, LX/1JD;->a(LX/1Li;Lcom/facebook/common/callercontext/CallerContext;)LX/1ly;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-direct {v5, v6}, LX/1lx;-><init>([LX/1JF;)V

    iput-object v5, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->f:LX/1lx;

    .line 229899
    :cond_0
    iget-object v5, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->f:LX/1lx;

    move-object v5, v5

    .line 229900
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v6

    .line 229901
    if-eqz v6, :cond_1

    .line 229902
    instance-of v7, v6, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v7, :cond_3

    .line 229903
    new-instance v7, LX/1mT;

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v7, v6, p3}, LX/1mT;-><init>(Lcom/facebook/graphql/model/GraphQLStory;I)V

    invoke-virtual {v5, v7}, LX/1jx;->b(LX/0jT;)LX/0jT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229904
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move p3, v2

    goto :goto_0

    .line 229905
    :cond_2
    const v0, 0x69e29d10

    invoke-static {v0}, LX/02m;->a(I)V

    .line 229906
    return-void

    .line 229907
    :catchall_0
    move-exception v0

    const v1, -0x628be227

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 229908
    :cond_3
    invoke-virtual {v5, v6}, LX/1jx;->b(LX/0jT;)LX/0jT;

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 5

    .prologue
    .line 229909
    if-nez p1, :cond_1

    .line 229910
    :cond_0
    :goto_0
    return-void

    .line 229911
    :cond_1
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v0, v0

    .line 229912
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 229913
    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 229914
    iget v2, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->a:I

    .line 229915
    iget v3, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->a:I

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->a:I

    .line 229916
    iget-object v3, p0, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader$1;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader$1;-><init>(Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;I)V

    const v0, -0x1174f9e0

    invoke-static {v3, v4, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
