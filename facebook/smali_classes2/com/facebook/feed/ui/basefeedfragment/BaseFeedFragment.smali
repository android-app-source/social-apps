.class public abstract Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fg;


# instance fields
.field private a:LX/0fN;

.field public b:LX/19P;

.field public c:LX/13l;

.field private d:LX/1AM;

.field private final e:LX/19I;

.field private final f:LX/19L;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 109289
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 109290
    new-instance v0, LX/19I;

    invoke-direct {v0, p0}, LX/19I;-><init>(Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->e:LX/19I;

    .line 109291
    new-instance v0, LX/19L;

    invoke-direct {v0, p0}, LX/19L;-><init>(Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->f:LX/19L;

    .line 109292
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 109325
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 109326
    new-instance v0, LX/19O;

    invoke-direct {v0, p0}, LX/19O;-><init>(Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a:LX/0fN;

    .line 109327
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->kX_()LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/19P;->a(LX/0QB;)LX/19P;

    move-result-object v0

    check-cast v0, LX/19P;

    iput-object v0, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->b:LX/19P;

    .line 109328
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->kX_()LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/13l;->a(LX/0QB;)LX/13l;

    move-result-object v0

    check-cast v0, LX/13l;

    iput-object v0, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->c:LX/13l;

    .line 109329
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->kX_()LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v0

    check-cast v0, LX/1AM;

    iput-object v0, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->d:LX/1AM;

    .line 109330
    return-void
.end method

.method public lr_()V
    .locals 0

    .prologue
    .line 109331
    return-void
.end method

.method public ls_()V
    .locals 0

    .prologue
    .line 109323
    return-void
.end method

.method public lt_()V
    .locals 0

    .prologue
    .line 109324
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5d96a2d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 109320
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 109321
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a:LX/0fN;

    .line 109322
    const/16 v1, 0x2b

    const v2, -0x32b7e82b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3cd1ee33

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 109310
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a:LX/0fN;

    if-eqz v1, :cond_0

    .line 109311
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 109312
    iget-object v2, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a:LX/0fN;

    invoke-virtual {v1, v2}, LX/0gc;->b(LX/0fN;)V

    .line 109313
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->b:LX/19P;

    if-eqz v1, :cond_1

    .line 109314
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->c:LX/13l;

    sget-object v2, LX/04g;->BY_NEWSFEED_ONPAUSE:LX/04g;

    invoke-virtual {v1, v2}, LX/13l;->a(LX/04g;)V

    .line 109315
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->d:LX/1AM;

    if-eqz v1, :cond_2

    .line 109316
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->d:LX/1AM;

    iget-object v2, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->e:LX/19I;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 109317
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->d:LX/1AM;

    iget-object v2, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->f:LX/19L;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 109318
    :cond_2
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 109319
    const/16 v1, 0x2b

    const v2, 0x5e718d4b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2c112965

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 109299
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 109300
    invoke-virtual {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->lr_()V

    .line 109301
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a:LX/0fN;

    if-eqz v1, :cond_0

    .line 109302
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 109303
    iget-object v2, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a:LX/0fN;

    invoke-virtual {v1, v2}, LX/0gc;->a(LX/0fN;)V

    .line 109304
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->d:LX/1AM;

    if-eqz v1, :cond_1

    .line 109305
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->d:LX/1AM;

    iget-object v2, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->e:LX/19I;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 109306
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->d:LX/1AM;

    iget-object v2, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->f:LX/19L;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 109307
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->b:LX/19P;

    if-eqz v1, :cond_2

    .line 109308
    iget-object v1, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->b:LX/19P;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/19P;->a(Z)V

    .line 109309
    :cond_2
    const/16 v1, 0x2b

    const v2, -0x7dd228d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 109293
    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->c:LX/13l;

    if-eqz v0, :cond_1

    .line 109294
    iget-object v0, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->c:LX/13l;

    sget-object v1, LX/04g;->BY_NEWSFEED_OCCLUSION:LX/04g;

    invoke-virtual {v0, v1}, LX/13l;->a(LX/04g;)V

    .line 109295
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 109296
    return-void

    .line 109297
    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->c:LX/13l;

    if-eqz v0, :cond_0

    .line 109298
    iget-object v0, p0, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->c:LX/13l;

    sget-object v1, LX/04g;->BY_NEWSFEED_OCCLUSION:LX/04g;

    invoke-virtual {v0, v1}, LX/13l;->b(LX/04g;)V

    goto :goto_0
.end method
