.class public Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/1vv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 338191
    const-class v0, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1vv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 338187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338188
    iput-object p1, p0, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;->b:Landroid/content/Context;

    .line 338189
    iput-object p2, p0, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;->c:LX/1vv;

    .line 338190
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;
    .locals 5

    .prologue
    .line 338176
    const-class v1, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;

    monitor-enter v1

    .line 338177
    :try_start_0
    sget-object v0, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 338178
    sput-object v2, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 338179
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338180
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 338181
    new-instance p0, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1vv;->a(LX/0QB;)LX/1vv;

    move-result-object v4

    check-cast v4, LX/1vv;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;-><init>(Landroid/content/Context;LX/1vv;)V

    .line 338182
    move-object v0, p0

    .line 338183
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 338184
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 338185
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 338186
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
