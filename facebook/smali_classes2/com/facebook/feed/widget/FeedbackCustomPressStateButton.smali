.class public Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;
.super Lcom/facebook/fbui/widget/text/ImageWithTextView;
.source ""

# interfaces
.implements LX/20T;


# static fields
.field public static final a:I

.field public static final b:I

.field private static final c:I


# instance fields
.field private d:LX/1Wk;

.field private e:I

.field private f:LX/20U;

.field public g:Landroid/view/View$OnTouchListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/215;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 339327
    const v0, 0x7f020a82

    sput v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a:I

    .line 339328
    const v0, 0x7f0218c7

    sput v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->b:I

    .line 339329
    const v0, 0x7f0102a8

    sput v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 339302
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 339303
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 339330
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 339331
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 339342
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 339343
    sget-object v0, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    iput-object v0, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->d:LX/1Wk;

    .line 339344
    new-instance v0, LX/20U;

    invoke-direct {v0, p0}, LX/20U;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->f:LX/20U;

    .line 339345
    sget-object v0, LX/1vY;->FEEDBACK_SECTION:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 339346
    sget-object v0, LX/1vY;->ACTION_ICON:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 339347
    new-instance v0, LX/20V;

    invoke-direct {v0, p0}, LX/20V;-><init>(Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;)V

    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 339348
    return-void
.end method

.method private getBackgroundResource()I
    .locals 4

    .prologue
    .line 339332
    sget-object v0, LX/21f;->a:[I

    iget-object v1, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->d:LX/1Wk;

    invoke-virtual {v1}, LX/1Wk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 339333
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown DownState type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 339334
    :pswitch_0
    sget v1, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a:I

    .line 339335
    sget v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->c:I

    move v3, v0

    move v0, v1

    move v1, v3

    .line 339336
    :goto_0
    if-nez v1, :cond_0

    .line 339337
    :goto_1
    return v0

    .line 339338
    :pswitch_1
    sget v1, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->b:I

    .line 339339
    const/4 v0, 0x0

    move v3, v0

    move v0, v1

    move v1, v3

    .line 339340
    goto :goto_0

    .line 339341
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v0}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 339320
    iget-object v0, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->h:LX/215;

    if-nez v0, :cond_0

    .line 339321
    :goto_0
    return-void

    .line 339322
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->h:LX/215;

    invoke-virtual {v0}, LX/215;->a()V

    .line 339323
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->h:LX/215;

    goto :goto_0
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 339324
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageScaleX(F)V

    .line 339325
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageScaleY(F)V

    .line 339326
    return-void
.end method

.method public getSpring()LX/215;
    .locals 1

    .prologue
    .line 339319
    iget-object v0, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->h:LX/215;

    return-object v0
.end method

.method public setDownstateType(LX/1Wk;)V
    .locals 2

    .prologue
    .line 339315
    iput-object p1, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->d:LX/1Wk;

    .line 339316
    iget-object v0, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->d:LX/1Wk;

    if-eqz v0, :cond_0

    .line 339317
    iget-object v0, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->f:LX/20U;

    invoke-direct {p0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->getBackgroundResource()I

    move-result v1

    invoke-virtual {v0, v1}, LX/20U;->b(I)V

    .line 339318
    :cond_0
    return-void
.end method

.method public setDrawable(I)V
    .locals 1

    .prologue
    .line 339311
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->e:I

    if-ne p1, v0, :cond_0

    .line 339312
    :goto_0
    return-void

    .line 339313
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 339314
    iput p1, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->e:I

    goto :goto_0
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 339309
    iput-object p1, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->g:Landroid/view/View$OnTouchListener;

    .line 339310
    return-void
.end method

.method public setSpring(LX/215;)V
    .locals 0

    .prologue
    .line 339306
    invoke-virtual {p1, p0}, LX/215;->a(LX/20T;)V

    .line 339307
    iput-object p1, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->h:LX/215;

    .line 339308
    return-void
.end method

.method public setWarmupBackgroundResId(I)V
    .locals 1

    .prologue
    .line 339304
    iget-object v0, p0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->f:LX/20U;

    invoke-virtual {v0, p1}, LX/20U;->a(I)V

    .line 339305
    return-void
.end method
