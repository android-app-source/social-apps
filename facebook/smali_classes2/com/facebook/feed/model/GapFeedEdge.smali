.class public Lcom/facebook/feed/model/GapFeedEdge;
.super Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 187737
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;-><init>()V

    .line 187738
    new-instance v0, Lcom/facebook/feed/model/GapFeedEdge$GapFeedUnit;

    invoke-direct {v0, p4}, Lcom/facebook/feed/model/GapFeedEdge$GapFeedUnit;-><init>(Ljava/lang/String;)V

    invoke-super {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 187739
    invoke-super {p0, p3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b(Ljava/lang/String;)V

    .line 187740
    invoke-super {p0, p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d(Ljava/lang/String;)V

    .line 187741
    invoke-super {p0, p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Ljava/lang/String;)V

    .line 187742
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187743
    const-string v0, "FEED_GAP_ROW"

    return-object v0
.end method
