.class public Lcom/facebook/feed/model/ClientFeedUnitEdge;
.super Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
.source ""


# instance fields
.field public final A:Ljava/lang/String;

.field public final B:Z

.field public final C:Ljava/lang/String;

.field public final D:J

.field public E:Z

.field public F:D

.field public final m:Ljava/lang/String;

.field public final n:I

.field public final o:I

.field public final p:I

.field public final q:Z

.field public final r:Ljava/nio/ByteBuffer;

.field public final s:Ljava/nio/ByteBuffer;

.field public final t:Ljava/lang/String;

.field public final u:J

.field public final v:I

.field private final w:I

.field public final x:I

.field public final y:LX/15j;

.field private z:LX/0qN;


# direct methods
.method public constructor <init>(Ljava/lang/String;DLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLBumpReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZLjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/lang/String;JIIILX/15j;LX/0qN;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;J)V
    .locals 3

    .prologue
    .line 161623
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;-><init>()V

    .line 161624
    invoke-super {p0, p8}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b(Ljava/lang/String;)V

    .line 161625
    invoke-super {p0, p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d(Ljava/lang/String;)V

    .line 161626
    invoke-super {p0, p2, p3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(D)V

    .line 161627
    invoke-super {p0, p5}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)V

    .line 161628
    invoke-super {p0, p4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Ljava/lang/String;)V

    .line 161629
    move-object/from16 v0, p25

    invoke-super {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c(Ljava/lang/String;)V

    .line 161630
    move/from16 v0, p26

    invoke-super {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Z)V

    .line 161631
    const/4 v2, 0x0

    invoke-super {p0, v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 161632
    invoke-static {p0, p6}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 161633
    invoke-static {p0, p7}, LX/0x1;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 161634
    invoke-static/range {p20 .. p20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, LX/0x1;->c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 161635
    iput-object p9, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->m:Ljava/lang/String;

    .line 161636
    iput p10, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->n:I

    .line 161637
    iput p11, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->o:I

    .line 161638
    iput p12, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->p:I

    .line 161639
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->q:Z

    .line 161640
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->r:Ljava/nio/ByteBuffer;

    .line 161641
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->s:Ljava/nio/ByteBuffer;

    .line 161642
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->t:Ljava/lang/String;

    .line 161643
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->u:J

    .line 161644
    move/from16 v0, p19

    iput v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->v:I

    .line 161645
    move/from16 v0, p20

    iput v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->w:I

    .line 161646
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->y:LX/15j;

    .line 161647
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->z:LX/0qN;

    .line 161648
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A:Ljava/lang/String;

    .line 161649
    move/from16 v0, p21

    iput v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->x:I

    .line 161650
    move/from16 v0, p27

    iput-boolean v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->B:Z

    .line 161651
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    .line 161652
    move-wide/from16 v0, p29

    iput-wide v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->D:J

    .line 161653
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->E:Z

    .line 161654
    return-void
.end method


# virtual methods
.method public final A()I
    .locals 1

    .prologue
    .line 161620
    invoke-static {p0}, LX/0x1;->c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    .line 161621
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 161622
    return v0
.end method

.method public final C()I
    .locals 1

    .prologue
    .line 161619
    iget v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->x:I

    return v0
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161560
    iget-object v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final E()J
    .locals 2

    .prologue
    .line 161617
    iget-wide v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->D:J

    return-wide v0
.end method

.method public final F()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 161618
    iget v1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->w:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final I()D
    .locals 2

    .prologue
    .line 161616
    iget-wide v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->F:D

    return-wide v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 161615
    iget-object v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->z:LX/0qN;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->o:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 161613
    iput-boolean p1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->E:Z

    .line 161614
    return-void
.end method

.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 14

    .prologue
    .line 161562
    invoke-super {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 161563
    if-nez v0, :cond_1

    .line 161564
    const/4 v2, 0x0

    .line 161565
    iget-object v1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->m:Ljava/lang/String;

    move-object v1, v1

    .line 161566
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 161567
    iget v1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->o:I

    move v1, v1

    .line 161568
    if-lez v1, :cond_2

    .line 161569
    :try_start_0
    iget-object v1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->m:Ljava/lang/String;

    move-object v1, v1

    .line 161570
    iget v3, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->n:I

    move v3, v3

    .line 161571
    iget v4, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->o:I

    move v4, v4

    .line 161572
    invoke-static {v1, v3, v4}, LX/0qN;->a(Ljava/lang/String;II)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 161573
    :goto_0
    if-nez v1, :cond_3

    .line 161574
    :cond_0
    :goto_1
    move-object v0, v2

    .line 161575
    invoke-super {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 161576
    :cond_1
    return-object v0

    :catch_0
    :cond_2
    move-object v1, v2

    goto :goto_0

    .line 161577
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->q:Z

    move v2, v2

    .line 161578
    iget-object v3, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->r:Ljava/nio/ByteBuffer;

    move-object v3, v3

    .line 161579
    iget-object v4, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->s:Ljava/nio/ByteBuffer;

    move-object v4, v4

    .line 161580
    iget-object v5, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->y:LX/15j;

    move-object v5, v5

    .line 161581
    iget v6, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->p:I

    move v6, v6

    .line 161582
    const/4 v13, 0x0

    .line 161583
    if-nez v1, :cond_8

    move-object v7, v13

    .line 161584
    :cond_4
    :goto_2
    move-object v2, v7

    .line 161585
    if-eqz v2, :cond_0

    .line 161586
    instance-of v1, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_5

    move-object v1, v2

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ak()Z

    move-result v1

    if-eqz v1, :cond_5

    move-object v1, v2

    .line 161587
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 161588
    iget v3, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->v:I

    move v3, v3

    .line 161589
    if-eqz v3, :cond_7

    const/4 v3, 0x1

    :goto_3
    invoke-static {v1, v3}, LX/0x1;->c(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    .line 161590
    :cond_5
    iget-object v1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->t:Ljava/lang/String;

    move-object v1, v1

    .line 161591
    invoke-static {p0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v4

    .line 161592
    iget-wide v7, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->u:J

    move-wide v5, v7

    .line 161593
    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 161594
    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v6

    .line 161595
    if-eqz v6, :cond_6

    .line 161596
    const/4 v7, 0x0

    invoke-virtual {v6, v7, v1}, LX/15i;->a(ILjava/lang/Object;)V

    .line 161597
    const/4 v7, 0x1

    invoke-virtual {v6, v7, v3}, LX/15i;->a(ILjava/lang/Object;)V

    .line 161598
    const/4 v7, 0x2

    invoke-virtual {v6, v7, v4}, LX/15i;->a(ILjava/lang/Object;)V

    .line 161599
    const/4 v7, 0x3

    invoke-virtual {v6, v7, v5}, LX/15i;->a(ILjava/lang/Object;)V

    .line 161600
    :cond_6
    iget-wide v7, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->u:J

    move-wide v3, v7

    .line 161601
    invoke-static {v2, v3, v4}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    goto :goto_1

    .line 161602
    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    .line 161603
    :cond_8
    :try_start_1
    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 161604
    new-instance v7, LX/15i;

    move-object v8, v1

    move-object v9, v3

    move-object v10, v4

    move v11, v2

    move-object v12, v5

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 161605
    const-string v8, "FeedUnitInflater"

    invoke-virtual {v7, v8}, LX/15i;->a(Ljava/lang/String;)V

    .line 161606
    if-eqz v6, :cond_9

    .line 161607
    sget-object v8, LX/16Z;->a:LX/16Z;

    invoke-virtual {v7, v6, v8}, LX/15i;->a(ILX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/FeedUnit;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 161608
    :goto_4
    if-nez v7, :cond_4

    .line 161609
    const-string v8, "FlatBuffer"

    const-string v9, "unable to resolve root flattenable feed unit from flatbuffer"

    invoke-static {v8, v9}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 161610
    :cond_9
    :try_start_2
    sget-object v8, LX/16Z;->a:LX/16Z;

    invoke-virtual {v7, v8}, LX/15i;->a(LX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/FeedUnit;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    .line 161611
    :catch_1
    move-exception v7

    .line 161612
    const-string v8, "FlatBuffer"

    const-string v9, "Error deserializing feed unit from flatbuffer"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v8, v7, v9, v10}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v7, v13

    goto :goto_4
.end method

.method public final x()J
    .locals 2

    .prologue
    .line 161561
    iget-wide v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->u:J

    return-wide v0
.end method
