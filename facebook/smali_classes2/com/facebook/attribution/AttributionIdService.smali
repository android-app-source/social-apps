.class public Lcom/facebook/attribution/AttributionIdService;
.super LX/1ZN;
.source ""


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private b:LX/03V;

.field private c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Wd;

.field private e:LX/11H;

.field private f:LX/1mV;

.field private g:LX/0SG;

.field private h:LX/0W3;

.field private i:Landroid/content/pm/PackageManager;

.field private j:LX/0Zb;

.field private k:LX/0lC;

.field private l:Landroid/content/ContentResolver;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 274635
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/1ZO;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/attribution/AttributionIdService;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 274633
    const-string v0, "AttributionIdService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 274634
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 274609
    :try_start_0
    iget-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->l:Landroid/content/ContentResolver;

    sget-object v1, Lcom/facebook/attribution/AttributionIdService;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 274610
    if-nez v1, :cond_2

    .line 274611
    if-eqz v1, :cond_0

    .line 274612
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v6

    .line 274613
    :cond_1
    :goto_0
    return-object v0

    .line 274614
    :cond_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_4

    .line 274615
    if-eqz v1, :cond_3

    .line 274616
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v6

    goto :goto_0

    .line 274617
    :cond_4
    :try_start_2
    const-string v0, "attribution_json"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    .line 274618
    if-gez v0, :cond_6

    .line 274619
    if-eqz v1, :cond_5

    .line 274620
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v0, v6

    goto :goto_0

    .line 274621
    :cond_6
    :try_start_3
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 274622
    if-eqz v1, :cond_1

    .line 274623
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 274624
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 274625
    :goto_1
    :try_start_4
    iget-object v2, p0, Lcom/facebook/attribution/AttributionIdService;->b:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failure acquiring oxygen attribution."

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 274626
    if-eqz v1, :cond_7

    .line 274627
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    move-object v0, v6

    .line 274628
    goto :goto_0

    .line 274629
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_8

    .line 274630
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 274631
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 274632
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private a(LX/0lF;)V
    .locals 6

    .prologue
    .line 274590
    if-eqz p1, :cond_3

    .line 274591
    invoke-virtual {p1}, LX/0lF;->j()Ljava/util/Iterator;

    move-result-object v3

    .line 274592
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 274593
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 274594
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 274595
    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 274596
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0lF;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 274597
    const/4 v2, 0x0

    .line 274598
    invoke-virtual {v1}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 274599
    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/attribution/AttributionIdService;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 274600
    const/4 v1, 0x1

    .line 274601
    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 274602
    :cond_2
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 274603
    iget-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->k:LX/0lC;

    const-class v1, LX/0lF;

    invoke-virtual {v0, v4, v1}, LX/0lC;->a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 274604
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "app_presence"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "app_presence"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "native_newsfeed"

    .line 274605
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 274606
    move-object v0, v0

    .line 274607
    iget-object v1, p0, Lcom/facebook/attribution/AttributionIdService;->j:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 274608
    :cond_3
    return-void

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 3

    .prologue
    .line 274525
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 274526
    if-eqz v0, :cond_0

    .line 274527
    :goto_0
    return-void

    .line 274528
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/attribution/AttributionIdService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 274529
    const-string v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 274530
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 274531
    :catch_0
    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 274588
    :try_start_0
    iget-object v1, p0, Lcom/facebook/attribution/AttributionIdService;->i:Landroid/content/pm/PackageManager;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274589
    :goto_0
    return v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/attribution/AttributionIdService;JLjava/lang/String;)V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 274560
    invoke-virtual {p0}, Lcom/facebook/attribution/AttributionIdService;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    .line 274561
    invoke-static {v9}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v0

    check-cast v0, LX/0dC;

    invoke-virtual {v0}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v5

    .line 274562
    if-nez v5, :cond_0

    .line 274563
    iget-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->b:LX/03V;

    const-string v1, "uniqueIdForDevice null"

    const-string v2, "AttributionIdUpdate get uniqueIdForDevice null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 274564
    :goto_0
    return-void

    .line 274565
    :cond_0
    const/4 v0, 0x0

    .line 274566
    :try_start_0
    invoke-static {v9}, LX/1oW;->a(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_1

    .line 274567
    invoke-static {v9}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 274568
    :cond_1
    :goto_1
    if-nez v0, :cond_2

    .line 274569
    :try_start_1
    new-instance v4, LX/3cx;

    invoke-direct {v4}, LX/3cx;-><init>()V

    .line 274570
    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.google.android.gms.ads.identifier.service.START"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 274571
    const-string v6, "com.google.android.gms"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 274572
    const/4 v6, 0x1

    const v7, -0x3c75545

    invoke-static {v9, v2, v4, v6, v7}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v2

    if-eqz v2, :cond_2

    .line 274573
    :try_start_2
    new-instance v6, LX/BaE;

    invoke-virtual {v4}, LX/3cx;->a()Landroid/os/IBinder;

    move-result-object v2

    invoke-direct {v6, v2}, LX/BaE;-><init>(Landroid/os/IBinder;)V

    .line 274574
    new-instance v2, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    invoke-virtual {v6}, LX/BaE;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, LX/BaE;->b()Z

    move-result v6

    invoke-direct {v2, v7, v6}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;-><init>(Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 274575
    const v0, -0x5168bf9d

    :try_start_3
    invoke-static {v9, v4, v0}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 274576
    :goto_2
    new-instance v0, LX/3cy;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/attribution/AttributionIdService;->a()Ljava/lang/String;

    move-result-object v8

    move-object v4, p3

    move-wide v6, p1

    invoke-direct/range {v0 .. v8}, LX/3cy;-><init>(Ljava/lang/String;Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;ZLjava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 274577
    :try_start_4
    iget-object v1, p0, Lcom/facebook/attribution/AttributionIdService;->e:LX/11H;

    iget-object v2, p0, Lcom/facebook/attribution/AttributionIdService;->f:LX/1mV;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mb;

    .line 274578
    invoke-static {v9, v0}, LX/1ma;->a(Landroid/content/Context;LX/1mb;)V

    .line 274579
    iget-object v0, v0, LX/1mb;->f:LX/0lF;

    invoke-direct {p0, v0}, Lcom/facebook/attribution/AttributionIdService;->a(LX/0lF;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 274580
    :catch_0
    goto :goto_0

    .line 274581
    :catch_1
    move-exception v2

    .line 274582
    iget-object v4, p0, Lcom/facebook/attribution/AttributionIdService;->b:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Failure while using gms sdk to read advertising id"

    invoke-virtual {v4, v6, v7, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 274583
    :catchall_0
    move-exception v2

    const v3, 0x3abed905

    :try_start_5
    invoke-static {v9, v4, v3}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    throw v2
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 274584
    :catch_2
    move-exception v2

    move v3, v1

    move-object v10, v2

    move-object v2, v0

    move-object v0, v10

    .line 274585
    :goto_3
    iget-object v1, p0, Lcom/facebook/attribution/AttributionIdService;->b:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "Failure acquiring gms id via interop."

    invoke-virtual {v1, v4, v6, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    move v3, v1

    move-object v2, v0

    .line 274586
    goto :goto_2

    .line 274587
    :catch_3
    move-exception v0

    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x2

    const/16 v3, 0x24

    const v4, 0x345b54f4

    invoke-static {v0, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v5

    .line 274548
    :try_start_0
    const-string v0, "user_id"

    const-wide/16 v6, 0x0

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 274549
    iget-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 274550
    :try_start_1
    invoke-virtual {p0}, Lcom/facebook/attribution/AttributionIdService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/1ma;->a(Landroid/content/Context;)LX/1mb;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    move-object v4, v3

    .line 274551
    :goto_0
    :try_start_2
    iget-object v3, p0, Lcom/facebook/attribution/AttributionIdService;->h:LX/0W3;

    sget-wide v8, LX/0X5;->hR:J

    const/4 v10, 0x1

    invoke-interface {v3, v8, v9, v10}, LX/0W4;->a(JI)I

    move-result v3

    .line 274552
    if-gtz v3, :cond_3

    .line 274553
    :goto_1
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v0, v8, v6

    if-nez v0, :cond_1

    if-eqz v4, :cond_0

    iget-wide v8, v4, LX/1mb;->b:J

    cmp-long v0, v8, v6

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v8

    iget-wide v10, v4, LX/1mb;->c:J

    int-to-long v2, v2

    const-wide/32 v12, 0x36ee80

    mul-long/2addr v2, v12

    add-long/2addr v2, v10

    cmp-long v0, v8, v2

    if-lez v0, :cond_1

    .line 274554
    :cond_0
    if-eqz v4, :cond_2

    iget-object v0, v4, LX/1mb;->e:Ljava/lang/String;

    .line 274555
    :goto_2
    iget-object v1, p0, Lcom/facebook/attribution/AttributionIdService;->d:LX/0Wd;

    new-instance v2, Lcom/facebook/attribution/AttributionIdService$1;

    invoke-direct {v2, p0, v6, v7, v0}, Lcom/facebook/attribution/AttributionIdService$1;-><init>(Lcom/facebook/attribution/AttributionIdService;JLjava/lang/String;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 274556
    :cond_1
    :goto_3
    const v0, -0x362e1495

    invoke-static {v0, v5}, LX/02F;->d(II)V

    return-void

    :catch_0
    move-object v4, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 274557
    goto :goto_2

    .line 274558
    :catch_1
    move-exception v0

    .line 274559
    iget-object v1, p0, Lcom/facebook/attribution/AttributionIdService;->b:LX/03V;

    const-string v2, "AttributionRefresh"

    const-string v3, "failure processing refresh"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x9619d5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 274532
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 274533
    invoke-virtual {p0}, Lcom/facebook/attribution/AttributionIdService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 274534
    invoke-static {v0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 274535
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/attribution/AttributionIdService;->i:Landroid/content/pm/PackageManager;

    .line 274536
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->l:Landroid/content/ContentResolver;

    .line 274537
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    .line 274538
    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->b:LX/03V;

    .line 274539
    const/16 v0, 0x15e7

    invoke-static {v2, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->c:LX/0Or;

    .line 274540
    invoke-static {v2}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v0

    check-cast v0, LX/0Wd;

    iput-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->d:LX/0Wd;

    .line 274541
    invoke-static {v2}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v0

    check-cast v0, LX/11H;

    iput-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->e:LX/11H;

    .line 274542
    invoke-static {v2}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    iput-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->g:LX/0SG;

    .line 274543
    invoke-static {v2}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    iput-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->h:LX/0W3;

    .line 274544
    invoke-static {v2}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iput-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->j:LX/0Zb;

    .line 274545
    invoke-static {v2}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    iput-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->k:LX/0lC;

    .line 274546
    new-instance v0, LX/1mV;

    iget-object v2, p0, Lcom/facebook/attribution/AttributionIdService;->g:LX/0SG;

    invoke-direct {v0, v2}, LX/1mV;-><init>(LX/0SG;)V

    iput-object v0, p0, Lcom/facebook/attribution/AttributionIdService;->f:LX/1mV;

    .line 274547
    const/16 v0, 0x25

    const v2, -0x6a81bc79

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
