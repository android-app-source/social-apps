.class public final Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1hi;

.field public final synthetic b:I

.field public final synthetic c:Lorg/apache/http/HttpResponse;

.field public final synthetic d:LX/1iL;


# direct methods
.method public constructor <init>(LX/1iL;LX/1hi;ILorg/apache/http/HttpResponse;)V
    .locals 0

    .prologue
    .line 327958
    iput-object p1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->d:LX/1iL;

    iput-object p2, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->a:LX/1hi;

    iput p3, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->b:I

    iput-object p4, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->c:Lorg/apache/http/HttpResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 327959
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->d:LX/1iL;

    iget-object v0, v0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->f:LX/0TU;

    invoke-interface {v0}, LX/0TU;->b()I

    move-result v0

    .line 327960
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->d:LX/1iL;

    iget-object v1, v1, LX/1iL;->a:LX/16j;

    iget-object v1, v1, LX/16j;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    .line 327961
    :try_start_0
    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->a:LX/1hi;

    iget v3, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->b:I

    invoke-virtual {v2, v3, v0, v1}, LX/1hi;->a(III)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327962
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->a:LX/1hi;

    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->c:Lorg/apache/http/HttpResponse;

    .line 327963
    invoke-virtual {v0}, LX/1hi;->b()Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 327964
    iget-object v2, v0, LX/1hi;->j:Lorg/apache/http/client/ResponseHandler;

    invoke-interface {v2, v1}, Lorg/apache/http/client/ResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    .line 327965
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->a:LX/1hi;

    iget v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->b:I

    iget-object v3, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->d:LX/1iL;

    iget-object v3, v3, LX/1iL;->a:LX/16j;

    invoke-virtual {v1, v2, v0, v3}, LX/1hi;->a(ILjava/lang/Object;LX/16j;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327966
    :cond_0
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->a:LX/1hi;

    iget v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->b:I

    invoke-virtual {v0, v1}, LX/1hi;->a(I)V

    .line 327967
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->d:LX/1iL;

    iget-object v0, v0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 327968
    :goto_0
    return-void

    .line 327969
    :catch_0
    move-exception v0

    .line 327970
    :try_start_1
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->a:LX/1hi;

    iget v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->b:I

    iget-object v3, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->d:LX/1iL;

    iget-object v3, v3, LX/1iL;->a:LX/16j;

    invoke-virtual {v1, v2, v0, v3}, LX/1hi;->a(ILjava/lang/Exception;LX/16j;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327971
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->a:LX/1hi;

    iget v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->b:I

    invoke-virtual {v0, v1}, LX/1hi;->a(I)V

    .line 327972
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->d:LX/1iL;

    iget-object v0, v0, LX/1iL;->a:LX/16j;

    iget-object v0, v0, LX/16j;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    goto :goto_0

    .line 327973
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->a:LX/1hi;

    iget v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->b:I

    invoke-virtual {v1, v2}, LX/1hi;->a(I)V

    .line 327974
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapterImpl$ClientCallbacks$1;->d:LX/1iL;

    iget-object v1, v1, LX/1iL;->a:LX/16j;

    iget-object v1, v1, LX/16j;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    throw v0
.end method
