.class public Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:LX/0ad;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/03V;

.field private final g:LX/14J;

.field public final h:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 296423
    const-class v0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;

    sput-object v0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0TU;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;LX/14J;LX/0Uh;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/UserAgentString;
        .end annotation
    .end param
    .param p2    # LX/0TU;
        .annotation runtime Lcom/facebook/tigon/httpclientadapter/ResponseHandlerThreadPool;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0TU;",
            "LX/0ad;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/tigon/httpclientadapter/TigonExperiment;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 296414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296415
    iput-object p1, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->b:LX/0Or;

    .line 296416
    iput-object p2, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->c:Ljava/util/concurrent/Executor;

    .line 296417
    iput-object p3, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->d:LX/0ad;

    .line 296418
    iput-object p4, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 296419
    iput-object p5, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->f:LX/03V;

    .line 296420
    iput-object p6, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->g:LX/14J;

    .line 296421
    iput-object p7, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->h:LX/0Uh;

    .line 296422
    return-void
.end method

.method private static a(ILcom/facebook/tigon/iface/TigonRequestBuilder;Lorg/apache/http/Header;)V
    .locals 2

    .prologue
    .line 296411
    invoke-interface {p2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 296412
    invoke-virtual {p1, v0, v1}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 296413
    return-void
.end method

.method private static a(Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;Lcom/facebook/tigon/iface/TigonRequestBuilder;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 296386
    const/4 v0, 0x0

    .line 296387
    if-nez p3, :cond_7

    .line 296388
    :cond_0
    :goto_0
    move v1, v0

    .line 296389
    const/4 v0, 0x0

    .line 296390
    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->h:LX/0Uh;

    const/16 v3, 0x296

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_b

    .line 296391
    :cond_1
    :goto_1
    move v0, v0

    .line 296392
    const/4 v2, 0x0

    .line 296393
    iget-object v3, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->h:LX/0Uh;

    const/16 p2, 0x297

    invoke-virtual {v3, p2, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-nez v3, :cond_c

    .line 296394
    :cond_2
    :goto_2
    move v2, v2

    .line 296395
    if-nez v0, :cond_3

    if-eqz v2, :cond_6

    :cond_3
    const/4 v0, 0x1

    .line 296396
    :goto_3
    if-nez v1, :cond_4

    if-eqz v0, :cond_5

    .line 296397
    :cond_4
    sget-object v2, LX/1iP;->e:LX/1he;

    new-instance v3, LX/1v1;

    invoke-direct {v3, v1, v0}, LX/1v1;-><init>(IZ)V

    invoke-virtual {p1, v2, v3}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 296398
    :cond_5
    return-void

    .line 296399
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 296400
    :cond_7
    if-eqz p2, :cond_8

    .line 296401
    const/4 v0, 0x1

    .line 296402
    :cond_8
    const-string v1, "image"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 296403
    const/4 v0, 0x3

    goto :goto_0

    .line 296404
    :cond_9
    const-string v1, "rangeRequestForVideo"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "getVideo-1RT"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "getLiveVideo"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 296405
    :cond_a
    const/4 v0, 0x5

    goto :goto_0

    .line 296406
    :cond_b
    const-string v2, "image"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 296407
    const-string v2, "ImagePushSubscriber"

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 296408
    const/4 v0, 0x1

    goto :goto_1

    .line 296409
    :cond_c
    const-string v3, "getLiveVideo"

    invoke-virtual {v3, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 296410
    const/4 v2, 0x1

    goto :goto_2
.end method

.method private static a(Lcom/facebook/tigon/iface/TigonRequestBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 296384
    invoke-virtual {p0, p1, p2}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 296385
    return-void
.end method

.method public static b(Lorg/apache/http/HttpRequest;)LX/1iI;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 296424
    instance-of v0, p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v0, :cond_3

    .line 296425
    check-cast p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    .line 296426
    invoke-interface {p0}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    .line 296427
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    .line 296428
    const-wide/32 v8, 0x7fffffff

    cmp-long v0, v6, v8

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v5, "Unexpected request length while extracting body: %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v1, v2

    invoke-static {v0, v5, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 296429
    cmp-long v0, v6, v10

    if-nez v0, :cond_1

    move-object v0, v3

    .line 296430
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 296431
    goto :goto_0

    .line 296432
    :cond_1
    cmp-long v0, v6, v10

    if-gez v0, :cond_2

    new-instance v0, LX/1iI;

    invoke-direct {v0}, LX/1iI;-><init>()V

    .line 296433
    :goto_2
    invoke-interface {v4, v0}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    goto :goto_1

    .line 296434
    :cond_2
    new-instance v0, LX/1iI;

    long-to-int v1, v6

    invoke-direct {v0, v1}, LX/1iI;-><init>(I)V

    goto :goto_2

    :cond_3
    move-object v0, v3

    .line 296435
    goto :goto_1
.end method

.method public static b(LX/0QB;)Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;
    .locals 8

    .prologue
    .line 296309
    new-instance v0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;

    const/16 v1, 0x15f5

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/1AF;->a(LX/0QB;)LX/0TU;

    move-result-object v2

    check-cast v2, LX/0TU;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/14I;->a(LX/0QB;)LX/14J;

    move-result-object v6

    check-cast v6, LX/14J;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;-><init>(LX/0Or;LX/0TU;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;LX/14J;LX/0Uh;)V

    .line 296310
    return-object v0
.end method

.method private static b(Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 296371
    const-string v0, "getLiveVideo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 296372
    const-string v0, ""

    .line 296373
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->d:LX/0ad;

    .line 296374
    sget-object p0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, p0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 296375
    const-string p1, ".mpd"

    invoke-virtual {p0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 296376
    sget-char p0, LX/0ws;->dA:C

    const-string p1, ""

    invoke-interface {v0, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 296377
    :goto_1
    move-object v0, p0

    .line 296378
    goto :goto_0

    .line 296379
    :cond_1
    const-string p1, ".m4v"

    invoke-virtual {p0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 296380
    sget-char p0, LX/0ws;->dz:C

    const-string p1, ""

    invoke-interface {v0, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 296381
    :cond_2
    const-string p1, ".m4a"

    invoke-virtual {p0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 296382
    sget-char p0, LX/0ws;->dy:C

    const-string p1, ""

    invoke-interface {v0, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 296383
    :cond_3
    const-string p0, ""

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/String;)LX/1iO;
    .locals 6

    .prologue
    const/4 v0, 0x2

    .line 296367
    invoke-virtual {p1}, Lcom/facebook/http/interfaces/RequestPriority;->getNumericValue()I

    move-result v1

    .line 296368
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    if-eq v1, v0, :cond_0

    .line 296369
    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->f:LX/03V;

    const-string v3, "Tigon unknown priority"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "value="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 296370
    :goto_0
    new-instance v1, LX/1iO;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, LX/1iO;-><init>(II)V

    return-object v1

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Lorg/apache/http/HttpRequest;)LX/HcQ;
    .locals 6

    .prologue
    .line 296358
    instance-of v0, p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v0, :cond_0

    .line 296359
    check-cast p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    .line 296360
    invoke-interface {p1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 296361
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v2

    .line 296362
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->g:LX/14J;

    .line 296363
    iget-object v4, v0, LX/14J;->a:LX/0ad;

    sget v5, LX/14p;->d:I

    const p1, 0x7fffffff

    invoke-interface {v4, v5, p1}, LX/0ad;->a(II)I

    move-result v4

    move v0, v4

    .line 296364
    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    .line 296365
    new-instance v0, LX/HcQ;

    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->c:Ljava/util/concurrent/Executor;

    invoke-direct {v0, v1, v2}, LX/HcQ;-><init>(Lorg/apache/http/HttpEntity;Ljava/util/concurrent/Executor;)V

    .line 296366
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;ZLX/14P;Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequest;
    .locals 10
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 296311
    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    .line 296312
    new-instance v4, Lcom/facebook/tigon/iface/TigonRequestBuilder;

    invoke-direct {v4}, Lcom/facebook/tigon/iface/TigonRequestBuilder;-><init>()V

    .line 296313
    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    move-result-object v2

    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->b(Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    move-result-object v2

    invoke-virtual {p0, p5, p3}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/String;)LX/1iO;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1iO;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 296314
    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v3

    array-length v5, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v3, v2

    .line 296315
    invoke-static {p1, v4, v6}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(ILcom/facebook/tigon/iface/TigonRequestBuilder;Lorg/apache/http/Header;)V

    .line 296316
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 296317
    :cond_0
    const-string v2, "Host"

    invoke-interface {p2, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    if-nez v2, :cond_1

    .line 296318
    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 296319
    const-string v3, "Host"

    invoke-static {v4, v3, v2}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(Lcom/facebook/tigon/iface/TigonRequestBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 296320
    :cond_1
    const-string v2, "User-Agent"

    invoke-interface {p2, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    if-nez v2, :cond_2

    .line 296321
    const-string v3, "User-Agent"

    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v4, v3, v2}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(Lcom/facebook/tigon/iface/TigonRequestBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 296322
    :cond_2
    instance-of v2, p2, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v2, :cond_5

    move-object v2, p2

    .line 296323
    check-cast v2, Lorg/apache/http/HttpEntityEnclosingRequest;

    .line 296324
    invoke-interface {v2}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 296325
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->isChunked()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-gez v3, :cond_b

    .line 296326
    :cond_3
    const-string v3, "Transfer-Encoding"

    const-string v5, "chunked"

    invoke-virtual {v4, v3, v5}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 296327
    :goto_1
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 296328
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v3

    invoke-static {p1, v4, v3}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(ILcom/facebook/tigon/iface/TigonRequestBuilder;Lorg/apache/http/Header;)V

    .line 296329
    :cond_4
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 296330
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v2

    invoke-static {p1, v4, v2}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(ILcom/facebook/tigon/iface/TigonRequestBuilder;Lorg/apache/http/Header;)V

    .line 296331
    :cond_5
    move/from16 v0, p6

    move-object/from16 v1, p8

    invoke-static {p0, v4, v0, p3, v1}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;Lcom/facebook/tigon/iface/TigonRequestBuilder;ZLjava/lang/String;Ljava/lang/String;)V

    .line 296332
    sget-object v2, LX/1iP;->b:LX/1he;

    new-instance v3, Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;

    invoke-direct {v3, p3, p4}, Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;-><init>(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v4, v2, v3}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 296333
    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->d:LX/0ad;

    sget v3, LX/0by;->G:I

    const/16 v5, 0x2710

    invoke-interface {v2, v3, v5}, LX/0ad;->a(II)I

    move-result v2

    .line 296334
    iget-object v3, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->d:LX/0ad;

    sget v5, LX/0by;->u:I

    const/16 v6, 0x2710

    invoke-interface {v3, v5, v6}, LX/0ad;->a(II)I

    move-result v3

    .line 296335
    iget-object v5, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/0dU;->n:LX/0Tn;

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v5

    .line 296336
    new-instance v6, LX/1iQ;

    const/4 v7, 0x0

    invoke-direct {v6, v2, v3, v7, v5}, LX/1iQ;-><init>(IIZZ)V

    .line 296337
    const/4 v2, 0x1

    .line 296338
    invoke-virtual {v6}, LX/1iQ;->isFlowTimeSampled()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 296339
    const/4 v2, 0x3

    .line 296340
    :cond_6
    invoke-virtual {v6}, LX/1iQ;->isCertSampled()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 296341
    or-int/lit8 v2, v2, 0x4

    .line 296342
    :cond_7
    invoke-virtual {v6}, LX/1iQ;->isCellTowerSampled()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 296343
    or-int/lit8 v2, v2, 0x8

    .line 296344
    :cond_8
    invoke-virtual {v6}, LX/1iQ;->shouldPrintTraceEvents()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 296345
    or-int/lit8 v2, v2, 0x10

    .line 296346
    :cond_9
    sget-object v3, LX/1iP;->i:LX/1he;

    new-instance v5, LX/1iS;

    invoke-direct {v5, v2}, LX/1iS;-><init>(I)V

    invoke-virtual {v4, v3, v5}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 296347
    if-eqz p4, :cond_c

    invoke-virtual {p4}, Lcom/facebook/common/callercontext/CallerContext;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 296348
    invoke-virtual {p4}, Lcom/facebook/common/callercontext/CallerContext;->a()Ljava/lang/String;

    move-result-object v2

    .line 296349
    :goto_2
    sget-object v5, LX/1iP;->d:LX/1he;

    new-instance v6, LX/1iT;

    sget-object v3, LX/14P;->RETRY_SAFE:LX/14P;

    move-object/from16 v0, p7

    if-ne v0, v3, :cond_e

    const/4 v3, 0x1

    :goto_3
    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v7

    invoke-virtual {v7}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, p3, v7}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->b(Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v3, v2, v7}, LX/1iT;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 296350
    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/client/params/HttpClientParams;->isRedirecting(Lorg/apache/http/params/HttpParams;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 296351
    sget-object v2, LX/1iP;->f:LX/1he;

    new-instance v3, Lcom/facebook/tigon/iface/AndroidRedirectRequestInfoImpl;

    const/4 v5, 0x0

    invoke-direct {v3, v5}, Lcom/facebook/tigon/iface/AndroidRedirectRequestInfoImpl;-><init>(Z)V

    invoke-virtual {v4, v2, v3}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 296352
    :cond_a
    invoke-virtual {v4}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a()Lcom/facebook/tigon/iface/TigonRequest;

    move-result-object v2

    return-object v2

    .line 296353
    :cond_b
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 296354
    const-string v5, "Content-Length"

    invoke-static {v4, v5, v3}, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->a(Lcom/facebook/tigon/iface/TigonRequestBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 296355
    :cond_c
    if-eqz p4, :cond_d

    .line 296356
    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonFbRequestBuilder;->f:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Tigon CallerContext.getCallingClassName "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "callerContext.getCallingClassName() should not be null"

    invoke-virtual {v2, v3, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    move-object v2, p3

    goto :goto_2

    .line 296357
    :cond_e
    const/4 v3, 0x0

    goto :goto_3
.end method
