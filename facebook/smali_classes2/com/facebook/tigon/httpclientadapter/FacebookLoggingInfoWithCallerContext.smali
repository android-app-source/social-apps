.class public Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 298383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298384
    iput-object p1, p0, Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;->a:Ljava/lang/String;

    .line 298385
    if-nez p2, :cond_0

    sget-object p2, Lcom/facebook/common/callercontext/CallerContext;->a:Lcom/facebook/common/callercontext/CallerContext;

    :cond_0
    iput-object p2, p0, Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 298386
    return-void
.end method


# virtual methods
.method public final logName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298382
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final logNamespace()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298381
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
