.class public Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Lorg/apache/http/protocol/HttpContext;


# instance fields
.field public final a:LX/1hd;

.field public final b:Lorg/apache/http/client/methods/HttpUriRequest;

.field private final c:LX/2BD;

.field private final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:I

.field public g:LX/1iW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1iZ;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lorg/apache/http/HttpResponse;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z


# direct methods
.method public constructor <init>(LX/1hd;Lorg/apache/http/client/methods/HttpUriRequest;LX/2BD;)V
    .locals 2
    .param p3    # LX/2BD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 297979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297980
    iput v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->e:I

    .line 297981
    iput v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->f:I

    .line 297982
    iput-object p1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    .line 297983
    iput-object p2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 297984
    iput-object p3, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->c:LX/2BD;

    .line 297985
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->d:Ljava/util/HashMap;

    .line 297986
    return-void
.end method

.method public static a(Lcom/facebook/tigon/tigonapi/TigonError;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 297978
    iget v0, p0, Lcom/facebook/tigon/tigonapi/TigonError;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, "cancelled"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "error"

    goto :goto_0
.end method

.method public static a(Ljava/io/IOException;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 297784
    instance-of v0, p0, LX/2BH;

    if-eqz v0, :cond_0

    .line 297785
    check-cast p0, LX/2BH;

    iget-object v0, p0, LX/2BH;->tigonError:Lcom/facebook/tigon/tigonapi/TigonError;

    invoke-static {v0}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a(Lcom/facebook/tigon/tigonapi/TigonError;)Ljava/lang/String;

    move-result-object v0

    .line 297786
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "error"

    goto :goto_0
.end method

.method public static a(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;LX/1pK;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 297876
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 297877
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297878
    iput-object p2, v0, LX/1iW;->i:Ljava/lang/String;

    .line 297879
    if-eqz p1, :cond_7

    .line 297880
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 297881
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297882
    iget-object v2, v1, LX/1iW;->i:Ljava/lang/String;

    move-object v1, v2

    .line 297883
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 297884
    sget-object v1, LX/1pY;->c:LX/1pZ;

    invoke-virtual {p1, v1}, LX/1pK;->a(LX/1pZ;)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, LX/1pI;

    .line 297885
    if-eqz v7, :cond_1

    .line 297886
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    .line 297887
    iget v2, v7, LX/1pI;->i:I

    move v2, v2

    .line 297888
    iget-wide v8, v7, LX/1pI;->j:J

    move-wide v3, v8

    .line 297889
    iget-wide v8, v7, LX/1pI;->p:J

    move-wide v5, v8

    .line 297890
    iget-object v8, v1, LX/1hd;->n:LX/1hg;

    int-to-long v10, v2

    invoke-virtual {v8, v10, v11, v3, v4}, LX/1hg;->a(JJ)V

    .line 297891
    iget-object v8, v1, LX/1hd;->f:LX/0p8;

    long-to-double v10, v5

    invoke-virtual {v8, v10, v11}, LX/0p8;->a(D)V

    .line 297892
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    iget-object v1, v1, LX/1iW;->requestHeaderBytes:LX/1iX;

    .line 297893
    iget v2, v7, LX/1pI;->c:I

    move v2, v2

    .line 297894
    int-to-long v3, v2

    .line 297895
    iput-wide v3, v1, LX/1iX;->a:J

    .line 297896
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    iget-object v1, v1, LX/1iW;->requestBodyBytes:LX/1iX;

    .line 297897
    iget v2, v7, LX/1pI;->e:I

    move v2, v2

    .line 297898
    int-to-long v3, v2

    .line 297899
    iput-wide v3, v1, LX/1iX;->a:J

    .line 297900
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    iget-object v1, v1, LX/1iW;->responseHeaderBytes:LX/1iX;

    .line 297901
    iget v2, v7, LX/1pI;->f:I

    move v2, v2

    .line 297902
    int-to-long v3, v2

    .line 297903
    iput-wide v3, v1, LX/1iX;->a:J

    .line 297904
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    iget-object v1, v1, LX/1iW;->responseBodyBytes:LX/1iX;

    .line 297905
    iget v2, v7, LX/1pI;->i:I

    move v2, v2

    .line 297906
    int-to-long v3, v2

    .line 297907
    iput-wide v3, v1, LX/1iX;->a:J

    .line 297908
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    iget-object v1, v1, LX/1iW;->bytesReadByApp:LX/1iX;

    .line 297909
    iget v2, v7, LX/1pI;->h:I

    move v2, v2

    .line 297910
    int-to-long v3, v2

    .line 297911
    iput-wide v3, v1, LX/1iX;->a:J

    .line 297912
    iget-object v1, v7, LX/1pI;->a:Ljava/lang/String;

    move-object v1, v1

    .line 297913
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 297914
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297915
    iget-object v2, v7, LX/1pI;->a:Ljava/lang/String;

    move-object v2, v2

    .line 297916
    iput-object v2, v1, LX/1iW;->e:Ljava/lang/String;

    .line 297917
    :cond_0
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297918
    iget-boolean v2, v7, LX/1pI;->b:Z

    move v2, v2

    .line 297919
    invoke-virtual {v1, v2}, LX/1iW;->b(Z)V

    .line 297920
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297921
    iget-boolean v2, v7, LX/1pI;->k:Z

    move v2, v2

    .line 297922
    invoke-virtual {v1, v2}, LX/1iW;->a(Z)V

    .line 297923
    :cond_1
    sget-object v1, LX/1pY;->d:LX/1pZ;

    invoke-virtual {p1, v1}, LX/1pK;->a(LX/1pZ;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HcU;

    .line 297924
    if-eqz v1, :cond_3

    .line 297925
    invoke-static {p0}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->c(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;)Ljava/util/Map;

    move-result-object v2

    .line 297926
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "mobile_http_flow"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 297927
    const-string v4, "RequestStats"

    .line 297928
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 297929
    invoke-virtual {v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297930
    iget-object v2, v1, LX/HcU;->a:Ljava/util/Map;

    move-object v1, v2

    .line 297931
    invoke-virtual {v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297932
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    .line 297933
    iget-object v2, v1, LX/1hd;->m:LX/1MY;

    invoke-interface {v2}, LX/1MY;->a()LX/763;

    move-result-object v2

    move-object v1, v2

    .line 297934
    sget-object v2, LX/763;->UNAVAILABLE:LX/763;

    if-eq v1, v2, :cond_2

    .line 297935
    const-string v2, "mqtt_status"

    invoke-virtual {v1}, LX/763;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297936
    :cond_2
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    invoke-virtual {v1, v3}, LX/1hd;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 297937
    :cond_3
    sget-object v1, LX/1pY;->f:LX/1pZ;

    invoke-virtual {p1, v1}, LX/1pK;->a(LX/1pZ;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HcT;

    .line 297938
    if-eqz v1, :cond_5

    .line 297939
    iget-object v2, v1, LX/HcT;->a:Ljava/util/Map;

    move-object v2, v2

    .line 297940
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 297941
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "cert_verification"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 297942
    const-string v3, "RequestStats"

    .line 297943
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 297944
    const-string v3, "weight"

    const-wide/16 v5, 0x1388

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297945
    iget-object v3, v1, LX/HcT;->a:Ljava/util/Map;

    move-object v1, v3

    .line 297946
    invoke-virtual {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297947
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    .line 297948
    iget-object v3, v1, LX/1hd;->k:LX/0YR;

    invoke-interface {v3}, LX/0YR;->b()LX/1hP;

    move-result-object v3

    move-object v1, v3

    .line 297949
    if-eqz v1, :cond_4

    .line 297950
    invoke-virtual {v1}, LX/1hP;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297951
    :cond_4
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    invoke-virtual {v1, v2}, LX/1hd;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 297952
    :cond_5
    iget v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->f:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_7

    .line 297953
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "cell_tower_info"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 297954
    const-string v2, "RequestStats"

    .line 297955
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 297956
    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    .line 297957
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 297958
    iget-object v4, v2, LX/1hd;->l:LX/1hI;

    invoke-virtual {v4, v3}, LX/1hI;->a(Ljava/util/Map;)V

    .line 297959
    iget-object v4, v2, LX/1hd;->l:LX/1hI;

    invoke-virtual {v4, v3}, LX/1hI;->b(Ljava/util/Map;)V

    .line 297960
    move-object v2, v3

    .line 297961
    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297962
    invoke-static {p0}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->c(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297963
    if-eqz v7, :cond_6

    .line 297964
    const-string v2, "request_header_size"

    .line 297965
    iget v3, v7, LX/1pI;->d:I

    move v3, v3

    .line 297966
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297967
    const-string v2, "request_body_size"

    .line 297968
    iget v3, v7, LX/1pI;->e:I

    move v3, v3

    .line 297969
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297970
    const-string v2, "response_header_size"

    .line 297971
    iget v3, v7, LX/1pI;->g:I

    move v3, v3

    .line 297972
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297973
    const-string v2, "response_body_size"

    .line 297974
    iget v3, v7, LX/1pI;->i:I

    move v3, v3

    .line 297975
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 297976
    :cond_6
    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    invoke-virtual {v2, v1}, LX/1hd;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 297977
    :cond_7
    return-void
.end method

.method public static b(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;)V
    .locals 6

    .prologue
    .line 297859
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->c:LX/2BD;

    .line 297860
    new-instance v2, LX/1iW;

    const-string v3, "Tigon"

    iget-object v4, v0, LX/1hd;->g:LX/0p7;

    iget-object v5, v0, LX/1hd;->h:LX/0So;

    invoke-direct {v2, v3, v4, v5, v1}, LX/1iW;-><init>(Ljava/lang/String;LX/0p7;LX/0So;LX/2BD;)V

    .line 297861
    iget-object v3, v0, LX/1hd;->d:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v3

    .line 297862
    iput-object v3, v2, LX/1iW;->g:Ljava/lang/String;

    .line 297863
    iget-object v3, v0, LX/1hd;->d:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v3

    .line 297864
    iput-object v3, v2, LX/1iW;->h:Ljava/lang/String;

    .line 297865
    iget-object v3, v0, LX/1hd;->d:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->j()Ljava/lang/String;

    move-result-object v3

    .line 297866
    iput-object v3, v2, LX/1iW;->f:Ljava/lang/String;

    .line 297867
    move-object v0, v2

    .line 297868
    iput-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297869
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->j:Z

    .line 297870
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    .line 297871
    iget-object v1, v0, LX/1hd;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    move-object v0, v1

    .line 297872
    iput-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    .line 297873
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iZ;

    .line 297874
    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v3, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    invoke-interface {v0, v2, p0, v3}, LX/1iZ;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;LX/1iW;)V

    goto :goto_0

    .line 297875
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;Ljava/io/IOException;)V
    .locals 7

    .prologue
    .line 297853
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 297854
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 297855
    iget-boolean v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->j:Z

    if-nez v0, :cond_1

    .line 297856
    :cond_0
    return-void

    .line 297857
    :cond_1
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iZ;

    .line 297858
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->i:Lorg/apache/http/HttpResponse;

    if-nez v1, :cond_2

    sget-object v1, LX/1iv;->HTTP_CLIENT_EXECUTE:LX/1iv;

    :goto_1
    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v3, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->i:Lorg/apache/http/HttpResponse;

    move-object v4, p0

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, LX/1iZ;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    goto :goto_0

    :cond_2
    sget-object v1, LX/1iv;->READ_RESPONSE_BODY:LX/1iv;

    goto :goto_1
.end method

.method public static c(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;)Ljava/util/Map;
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadArgumentPlacement"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297805
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 297806
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 297807
    const-string v1, "http_stack"

    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297808
    iget-object v3, v2, LX/1iW;->c:Ljava/lang/String;

    move-object v2, v3

    .line 297809
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297810
    const-string v1, "connection_type"

    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297811
    iget-object v3, v2, LX/1iW;->g:Ljava/lang/String;

    move-object v2, v3

    .line 297812
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297813
    const-string v1, "connection_subtype"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297814
    iget-object v4, v3, LX/1iW;->g:Ljava/lang/String;

    move-object v3, v4

    .line 297815
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297816
    iget-object v4, v3, LX/1iW;->h:Ljava/lang/String;

    move-object v3, v4

    .line 297817
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297818
    const-string v1, "request_queue_time_ms"

    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297819
    iget-wide v8, v2, LX/1iW;->a:J

    move-wide v2, v8

    .line 297820
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297821
    invoke-static {p0}, LX/1iV;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;

    move-result-object v1

    .line 297822
    const-string v2, "request_friendly_name"

    .line 297823
    iget-object v3, v1, LX/1iV;->a:Ljava/lang/String;

    move-object v3, v3

    .line 297824
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297825
    iget-object v2, v1, LX/1iV;->f:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v2

    .line 297826
    if-eqz v1, :cond_0

    .line 297827
    const-string v2, "request_call_path"

    .line 297828
    iget-object v3, v1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v3, v3

    .line 297829
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297830
    const-string v2, "request_analytics_tag"

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297831
    const-string v2, "request_module_analytics_tag"

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297832
    const-string v2, "request_feature_tag"

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297833
    :cond_0
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    .line 297834
    iget-object v2, v1, LX/1hd;->e:LX/0oz;

    move-object v1, v2

    .line 297835
    if-eqz v1, :cond_1

    .line 297836
    const-string v2, "connqual"

    invoke-virtual {v1}, LX/0oz;->c()LX/0p3;

    move-result-object v3

    invoke-virtual {v3}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297837
    const-string v2, "conncls_bandwidth_qual"

    invoke-virtual {v1}, LX/0oz;->b()LX/0p3;

    move-result-object v3

    invoke-virtual {v3}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297838
    const-string v2, "conncls_bandwidth_bps"

    invoke-virtual {v1}, LX/0oz;->f()D

    move-result-wide v4

    const-wide v6, 0x405f400000000000L    # 125.0

    mul-double/2addr v4, v6

    double-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297839
    const-string v2, "conncls_latency_qual"

    invoke-virtual {v1}, LX/0oz;->e()LX/0p3;

    move-result-object v3

    invoke-virtual {v3}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297840
    const-string v2, "conncls_latency_ms"

    invoke-virtual {v1}, LX/0oz;->k()D

    move-result-wide v4

    double-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297841
    :cond_1
    const-string v1, "request_method"

    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297842
    const-string v1, "weight"

    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    .line 297843
    iget-object v3, v2, LX/1hd;->c:LX/0ad;

    sget v4, LX/0by;->G:I

    const/16 v5, 0x2710

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    move v2, v3

    .line 297844
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297845
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->a:LX/1hd;

    .line 297846
    iget-object v2, v1, LX/1hd;->k:LX/0YR;

    invoke-interface {v2}, LX/0YR;->a()LX/1hM;

    move-result-object v2

    move-object v1, v2

    .line 297847
    if-eqz v1, :cond_2

    .line 297848
    invoke-virtual {v1}, LX/1hM;->e()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 297849
    :cond_2
    const-string v1, "request_status"

    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->g:LX/1iW;

    .line 297850
    iget-object v3, v2, LX/1iW;->i:Ljava/lang/String;

    move-object v2, v3

    .line 297851
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297852
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/tigon/iface/TigonRequest;I)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 297791
    sget-object v0, LX/1iP;->b:LX/1he;

    invoke-interface {p1, v0}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;

    .line 297792
    invoke-interface {p1}, Lcom/facebook/tigon/iface/TigonRequest;->d()LX/1iO;

    move-result-object v1

    iget v1, v1, LX/1iO;->a:I

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->DEFAULT_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-static {v1, v2}, Lcom/facebook/http/interfaces/RequestPriority;->fromNumericValue(ILcom/facebook/http/interfaces/RequestPriority;)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/http/interfaces/RequestPriority;->toString()Ljava/lang/String;

    move-result-object v3

    .line 297793
    sget-object v1, LX/1iP;->i:LX/1he;

    invoke-interface {p1, v1}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1iS;

    .line 297794
    if-eqz v1, :cond_0

    .line 297795
    iget v2, v1, LX/1iS;->a:I

    move v1, v2

    .line 297796
    iput v1, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->f:I

    .line 297797
    :cond_0
    instance-of v1, v0, Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;

    .line 297798
    iget-object v2, v1, Lcom/facebook/tigon/httpclientadapter/FacebookLoggingInfoWithCallerContext;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object v7, v2

    .line 297799
    :goto_0
    new-instance v1, LX/1iV;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;->logName()Ljava/lang/String;

    move-result-object v2

    :goto_1
    const-wide/16 v4, 0x0

    move v6, p2

    invoke-direct/range {v1 .. v8}, LX/1iV;-><init>(Ljava/lang/String;Ljava/lang/String;JILcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)V

    .line 297800
    invoke-virtual {v1, p0}, LX/1iV;->b(Lorg/apache/http/protocol/HttpContext;)V

    .line 297801
    invoke-static {p0}, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->b(Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;)V

    .line 297802
    return-void

    .line 297803
    :cond_1
    const-class v2, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;->logNamespace()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-static {v2, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    goto :goto_0

    :cond_2
    move-object v1, v8

    goto :goto_2

    .line 297804
    :cond_3
    const-string v2, "null"

    goto :goto_1
.end method

.method public final getAttribute(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 297790
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final removeAttribute(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 297789
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final setAttribute(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 297787
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297788
    return-void
.end method
