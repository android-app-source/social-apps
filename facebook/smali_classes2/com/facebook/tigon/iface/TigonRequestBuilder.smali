.class public Lcom/facebook/tigon/iface/TigonRequestBuilder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1iO;

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1he",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 298351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298352
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->c:Ljava/util/Map;

    .line 298353
    new-instance v0, LX/1iO;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/1iO;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->d:LX/1iO;

    .line 298354
    return-void
.end method

.method public constructor <init>(Lcom/facebook/tigon/iface/TigonRequest;)V
    .locals 4

    .prologue
    .line 298338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298339
    invoke-interface {p1}, Lcom/facebook/tigon/iface/TigonRequest;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a:Ljava/lang/String;

    .line 298340
    invoke-interface {p1}, Lcom/facebook/tigon/iface/TigonRequest;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->b:Ljava/lang/String;

    .line 298341
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/facebook/tigon/iface/TigonRequest;->c()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->c:Ljava/util/Map;

    .line 298342
    invoke-interface {p1}, Lcom/facebook/tigon/iface/TigonRequest;->d()LX/1iO;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->d:LX/1iO;

    .line 298343
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, LX/1iP;->k:[LX/1he;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->e:Ljava/util/Map;

    .line 298344
    const/4 v0, 0x0

    :goto_0
    sget-object v1, LX/1iP;->k:[LX/1he;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 298345
    sget-object v1, LX/1iP;->k:[LX/1he;

    aget-object v1, v1, v0

    .line 298346
    invoke-interface {p1, v1}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v2

    .line 298347
    if-eqz v2, :cond_0

    .line 298348
    iget-object v3, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->e:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298349
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 298350
    :cond_1
    return-void
.end method

.method private static create(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/facebook/tigon/iface/FacebookLoggingRequestInfoImpl;Lcom/facebook/tigon/iface/TigonRetrierRequestInfoImpl;)Lcom/facebook/tigon/iface/TigonRequest;
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 298320
    new-instance v0, Lcom/facebook/tigon/iface/TigonRequestBuilder;

    invoke-direct {v0}, Lcom/facebook/tigon/iface/TigonRequestBuilder;-><init>()V

    .line 298321
    iput-object p0, v0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a:Ljava/lang/String;

    .line 298322
    move-object v0, v0

    .line 298323
    iput-object p1, v0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->b:Ljava/lang/String;

    .line 298324
    move-object v0, v0

    .line 298325
    new-instance v1, LX/1iO;

    invoke-direct {v1, p3, p4}, LX/1iO;-><init>(II)V

    .line 298326
    iput-object v1, v0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->d:LX/1iO;

    .line 298327
    move-object v1, v0

    .line 298328
    array-length v0, p2

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 298329
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "must have even number of flattened headers"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298330
    :cond_0
    const/4 v0, 0x0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 298331
    aget-object v2, p2, v0

    add-int/lit8 v3, v0, 0x1

    aget-object v3, p2, v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 298332
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 298333
    :cond_1
    if-eqz p5, :cond_2

    .line 298334
    sget-object v0, LX/1iP;->b:LX/1he;

    invoke-virtual {v1, v0, p5}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 298335
    :cond_2
    if-eqz p6, :cond_3

    .line 298336
    sget-object v0, LX/1iP;->h:LX/1he;

    invoke-virtual {v1, v0, p6}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 298337
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a()Lcom/facebook/tigon/iface/TigonRequest;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/tigon/iface/TigonRequest;
    .locals 1

    .prologue
    .line 298319
    new-instance v0, LX/1iU;

    invoke-direct {v0, p0}, LX/1iU;-><init>(Lcom/facebook/tigon/iface/TigonRequestBuilder;)V

    return-object v0
.end method

.method public final a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1he",
            "<TT;>;TT;)",
            "Lcom/facebook/tigon/iface/TigonRequestBuilder;"
        }
    .end annotation

    .prologue
    .line 298306
    iget-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->e:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 298307
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->e:Ljava/util/Map;

    .line 298308
    :cond_0
    iget-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->e:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298309
    return-object p0
.end method

.method public final a(LX/1iO;)Lcom/facebook/tigon/iface/TigonRequestBuilder;
    .locals 0

    .prologue
    .line 298317
    iput-object p1, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->d:LX/1iO;

    .line 298318
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;
    .locals 0

    .prologue
    .line 298315
    iput-object p1, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a:Ljava/lang/String;

    .line 298316
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;
    .locals 1

    .prologue
    .line 298312
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 298313
    iget-object v0, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298314
    :cond_0
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;
    .locals 0

    .prologue
    .line 298310
    iput-object p1, p0, Lcom/facebook/tigon/iface/TigonRequestBuilder;->b:Ljava/lang/String;

    .line 298311
    return-object p0
.end method
