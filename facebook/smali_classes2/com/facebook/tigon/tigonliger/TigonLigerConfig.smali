.class public Lcom/facebook/tigon/tigonliger/TigonLigerConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field public static final a:[I


# instance fields
.field public final forwardableHeaders:[Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final maxJavaBufferSize:J
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final maxStreamingCachedBufferSize:J
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final nonTransientErrorRetryLimit:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final queueSizeImmediate:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final queueSizeLow:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final queueSizeNormal:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final redirectErrorCodes:[I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final requestTypeAndLimit:[I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final transientErrorRetryLimit:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210254
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x12c
        0x12d
        0x12e
        0x12f
        0x131
        0x133
    .end array-data
.end method

.method public constructor <init>(LX/14J;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x3

    .line 210255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210256
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->requestTypeAndLimit:[I

    .line 210257
    iget-object v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->requestTypeAndLimit:[I

    const/4 v1, 0x0

    aput v3, v0, v1

    .line 210258
    iget-object v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->requestTypeAndLimit:[I

    const/4 v1, 0x1

    aput v3, v0, v1

    .line 210259
    iget-object v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->requestTypeAndLimit:[I

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 210260
    sget-object v0, LX/1AJ;->a:Ljava/util/Set;

    sget-object v1, LX/1AJ;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->forwardableHeaders:[Ljava/lang/String;

    .line 210261
    sget-object v0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->a:[I

    iput-object v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->redirectErrorCodes:[I

    .line 210262
    const-wide/32 v0, 0x19000

    iput-wide v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->maxStreamingCachedBufferSize:J

    .line 210263
    iget-object v4, p1, LX/14J;->a:LX/0ad;

    sget v5, LX/14p;->a:I

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    int-to-long v4, v4

    move-wide v0, v4

    .line 210264
    iput-wide v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->maxJavaBufferSize:J

    .line 210265
    iget-object v4, p1, LX/14J;->b:LX/0W3;

    sget-wide v6, LX/0X5;->cH:J

    invoke-interface {v4, v6, v7}, LX/0W4;->c(J)J

    move-result-wide v4

    long-to-int v4, v4

    move v0, v4

    .line 210266
    iput v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->nonTransientErrorRetryLimit:I

    .line 210267
    iget-object v4, p1, LX/14J;->b:LX/0W3;

    sget-wide v6, LX/0X5;->cI:J

    invoke-interface {v4, v6, v7}, LX/0W4;->c(J)J

    move-result-wide v4

    long-to-int v4, v4

    move v0, v4

    .line 210268
    iput v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->transientErrorRetryLimit:I

    .line 210269
    const/16 v0, 0xa

    move v0, v0

    .line 210270
    iput v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->queueSizeLow:I

    .line 210271
    const/4 v0, 0x0

    move v0, v0

    .line 210272
    iput v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->queueSizeNormal:I

    .line 210273
    const/4 v0, 0x0

    move v0, v0

    .line 210274
    iput v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->queueSizeImmediate:I

    .line 210275
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/tigon/tigonliger/TigonLigerConfig;
    .locals 2

    .prologue
    .line 210276
    new-instance v1, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;

    invoke-static {p0}, LX/14I;->a(LX/0QB;)LX/14J;

    move-result-object v0

    check-cast v0, LX/14J;

    invoke-direct {v1, v0}, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;-><init>(LX/14J;)V

    .line 210277
    return-object v1
.end method
