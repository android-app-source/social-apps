.class public Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final a:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 210323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210324
    iput-object p1, p0, Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;->a:LX/03V;

    .line 210325
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;
    .locals 2

    .prologue
    .line 210326
    new-instance v1, Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v0}, Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;-><init>(LX/03V;)V

    .line 210327
    return-object v1
.end method


# virtual methods
.method public crashReport(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210328
    if-eqz p2, :cond_0

    .line 210329
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210330
    :goto_0
    iget-object v1, p0, Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;->a:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Tigon: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p1, p2, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 210331
    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method
