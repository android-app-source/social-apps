.class public Lcom/facebook/tigon/tigonliger/TigonLigerService;
.super Lcom/facebook/tigon/tigonapi/TigonXplatService;
.source ""

# interfaces
.implements LX/1AG;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:Lcom/facebook/tigon/tigonliger/TigonLigerService;


# instance fields
.field private b:LX/1MP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210192
    const-class v0, Lcom/facebook/tigon/tigonliger/TigonLigerService;

    sput-object v0, Lcom/facebook/tigon/tigonliger/TigonLigerService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/14F;LX/0Ot;Lcom/facebook/tigon/tigonliger/TigonLigerConfig;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;)V
    .locals 3
    .param p4    # Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/executors/liger/iface/LibraryLoader;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/liger/LigerHttpClientProvider;",
            ">;",
            "Lcom/facebook/tigon/tigonliger/TigonLigerConfig;",
            "Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;",
            "Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 210227
    invoke-static {p1, p2, p3, p4, p5}, Lcom/facebook/tigon/tigonliger/TigonLigerService;->a(LX/14F;LX/0Ot;Lcom/facebook/tigon/tigonliger/TigonLigerConfig;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    .line 210228
    iget-object v1, p5, Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;->a:LX/03V;

    move-object v1, v1

    .line 210229
    invoke-direct {p0, v0, v1}, Lcom/facebook/tigon/tigonapi/TigonXplatService;-><init>(Lcom/facebook/jni/HybridData;LX/03V;)V

    .line 210230
    :try_start_0
    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MP;

    iput-object v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerService;->b:LX/1MP;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 210231
    :goto_0
    return-void

    .line 210232
    :catch_0
    move-exception v0

    .line 210233
    sget-object v1, Lcom/facebook/tigon/tigonliger/TigonLigerService;->a:Ljava/lang/Class;

    const-string v2, "Can\'t initialize tigon"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(LX/14F;LX/0Ot;Lcom/facebook/tigon/tigonliger/TigonLigerConfig;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;)Lcom/facebook/jni/HybridData;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/executors/liger/iface/LibraryLoader;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/liger/LigerHttpClientProvider;",
            ">;",
            "Lcom/facebook/tigon/tigonliger/TigonLigerConfig;",
            "Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;",
            "Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;",
            ")",
            "Lcom/facebook/jni/HybridData;"
        }
    .end annotation

    .prologue
    .line 210207
    :try_start_0
    const-string v0, "tigonliger"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 210208
    invoke-virtual {p0}, LX/14F;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 210209
    sget-object v0, Lcom/facebook/tigon/tigonliger/TigonLigerService;->a:Ljava/lang/Class;

    const-string v1, "Can\'t load liger"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 210210
    new-instance v0, Lcom/facebook/jni/HybridData;

    invoke-direct {v0}, Lcom/facebook/jni/HybridData;-><init>()V

    .line 210211
    :cond_0
    :goto_0
    return-object v0

    .line 210212
    :catch_0
    move-exception v0

    .line 210213
    const-string v1, "failed to load native tigonliger lib"

    invoke-virtual {p4, v1, v0}, Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;->crashReport(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 210214
    throw v0

    .line 210215
    :cond_1
    :try_start_1
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MP;

    .line 210216
    iget-object v1, v0, LX/1MP;->r:Lcom/facebook/proxygen/HTTPClient;

    move-object v1, v1

    .line 210217
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MP;

    .line 210218
    iget-object v2, v0, LX/1MP;->s:LX/1hL;

    move-object v2, v2

    .line 210219
    iget-object v0, v1, Lcom/facebook/proxygen/HTTPClient;->mEventBase:Lcom/facebook/proxygen/EventBase;

    move-object v0, v0

    .line 210220
    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/tigon/tigonliger/TigonLigerService;->initHybrid(Lcom/facebook/proxygen/EventBase;Lcom/facebook/proxygen/HTTPClient;Lcom/facebook/proxygen/NetworkStatusMonitor;Lcom/facebook/tigon/tigonliger/TigonLigerConfig;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    .line 210221
    if-nez v0, :cond_0

    .line 210222
    sget-object v0, Lcom/facebook/tigon/tigonliger/TigonLigerService;->a:Ljava/lang/Class;

    const-string v1, "Can\'t load liger pointers"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 210223
    new-instance v0, Lcom/facebook/jni/HybridData;

    invoke-direct {v0}, Lcom/facebook/jni/HybridData;-><init>()V
    :try_end_1
    .catch LX/6XY; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 210224
    :catch_1
    move-exception v0

    .line 210225
    sget-object v1, Lcom/facebook/tigon/tigonliger/TigonLigerService;->a:Ljava/lang/Class;

    const-string v2, "Can\'t initialize liger"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 210226
    new-instance v0, Lcom/facebook/jni/HybridData;

    invoke-direct {v0}, Lcom/facebook/jni/HybridData;-><init>()V

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/tigon/tigonliger/TigonLigerService;
    .locals 3

    .prologue
    .line 210197
    sget-object v0, Lcom/facebook/tigon/tigonliger/TigonLigerService;->c:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    if-nez v0, :cond_1

    .line 210198
    const-class v1, Lcom/facebook/tigon/tigonliger/TigonLigerService;

    monitor-enter v1

    .line 210199
    :try_start_0
    sget-object v0, Lcom/facebook/tigon/tigonliger/TigonLigerService;->c:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 210200
    if-eqz v2, :cond_0

    .line 210201
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/tigon/tigonliger/TigonLigerService;->b(LX/0QB;)Lcom/facebook/tigon/tigonliger/TigonLigerService;

    move-result-object v0

    sput-object v0, Lcom/facebook/tigon/tigonliger/TigonLigerService;->c:Lcom/facebook/tigon/tigonliger/TigonLigerService;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210202
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 210203
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 210204
    :cond_1
    sget-object v0, Lcom/facebook/tigon/tigonliger/TigonLigerService;->c:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    return-object v0

    .line 210205
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 210206
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/tigon/tigonliger/TigonLigerService;
    .locals 6

    .prologue
    .line 210195
    new-instance v0, Lcom/facebook/tigon/tigonliger/TigonLigerService;

    invoke-static {p0}, LX/14F;->b(LX/0QB;)LX/14F;

    move-result-object v1

    check-cast v1, LX/14F;

    const/16 v2, 0xb66

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;->b(LX/0QB;)Lcom/facebook/tigon/tigonliger/TigonLigerConfig;

    move-result-object v3

    check-cast v3, Lcom/facebook/tigon/tigonliger/TigonLigerConfig;

    invoke-static {p0}, LX/1AK;->a(LX/0QB;)Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;

    invoke-static {p0}, Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;->b(LX/0QB;)Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;

    move-result-object v5

    check-cast v5, Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/tigon/tigonliger/TigonLigerService;-><init>(LX/14F;LX/0Ot;Lcom/facebook/tigon/tigonliger/TigonLigerConfig;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;)V

    .line 210196
    return-object v0
.end method

.method private static native initHybrid(Lcom/facebook/proxygen/EventBase;Lcom/facebook/proxygen/HTTPClient;Lcom/facebook/proxygen/NetworkStatusMonitor;Lcom/facebook/tigon/tigonliger/TigonLigerConfig;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Lcom/facebook/tigon/tigonliger/TigonLigerCrashReporter;)Lcom/facebook/jni/HybridData;
    .param p2    # Lcom/facebook/proxygen/NetworkStatusMonitor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 210193
    iget-object v0, p0, Lcom/facebook/tigon/tigonliger/TigonLigerService;->b:LX/1MP;

    invoke-virtual {v0}, LX/1MP;->c()V

    .line 210194
    return-void
.end method

.method public native cancelAllRequests()V
.end method

.method public native reconfigure(Lcom/facebook/tigon/tigonliger/TigonLigerConfig;)V
.end method
