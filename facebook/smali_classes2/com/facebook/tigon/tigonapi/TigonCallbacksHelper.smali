.class public Lcom/facebook/tigon/tigonapi/TigonCallbacksHelper;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 299798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static onEOM(Lcom/facebook/tigon/tigonapi/TigonBaseCallbacks;Ljava/nio/ByteBuffer;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 299785
    invoke-static {p1}, LX/1j4;->c(Ljava/nio/ByteBuffer;)Landroid/util/Pair;

    move-result-object v0

    .line 299786
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/1pK;

    invoke-interface {p0, v0}, Lcom/facebook/tigon/tigonapi/TigonBaseCallbacks;->onEOM(LX/1pK;)V

    .line 299787
    return-void
.end method

.method public static onError(Lcom/facebook/tigon/tigonapi/TigonBaseCallbacks;Ljava/nio/ByteBuffer;)V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 299795
    invoke-static {p1}, LX/1j4;->c(Ljava/nio/ByteBuffer;)Landroid/util/Pair;

    move-result-object v1

    .line 299796
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/tigon/tigonapi/TigonError;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, LX/1pK;

    invoke-interface {p0, v0, v1}, Lcom/facebook/tigon/tigonapi/TigonBaseCallbacks;->onError(Lcom/facebook/tigon/tigonapi/TigonError;LX/1pK;)V

    .line 299797
    return-void
.end method

.method public static onResponse(Lcom/facebook/tigon/tigonapi/TigonBaseCallbacks;Ljava/nio/ByteBuffer;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 299793
    invoke-static {p1}, LX/1j4;->b(Ljava/nio/ByteBuffer;)LX/1pB;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/facebook/tigon/tigonapi/TigonBaseCallbacks;->onResponse(LX/1pB;)V

    .line 299794
    return-void
.end method

.method public static onStarted(Lcom/facebook/tigon/tigonapi/TigonBaseCallbacks;Ljava/nio/ByteBuffer;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 299791
    invoke-static {p1}, LX/1j4;->a(Ljava/nio/ByteBuffer;)Lcom/facebook/tigon/iface/TigonRequest;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/facebook/tigon/tigonapi/TigonBaseCallbacks;->onStarted(Lcom/facebook/tigon/iface/TigonRequest;)V

    .line 299792
    return-void
.end method

.method public static onWillRetry(Lcom/facebook/tigon/tigonapi/TigonBaseCallbacks;Ljava/nio/ByteBuffer;)V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 299788
    invoke-static {p1}, LX/1j4;->c(Ljava/nio/ByteBuffer;)Landroid/util/Pair;

    move-result-object v1

    .line 299789
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/tigon/tigonapi/TigonError;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, LX/1pK;

    invoke-interface {p0, v0, v1}, Lcom/facebook/tigon/tigonapi/TigonBaseCallbacks;->onWillRetry(Lcom/facebook/tigon/tigonapi/TigonError;LX/1pK;)V

    .line 299790
    return-void
.end method
