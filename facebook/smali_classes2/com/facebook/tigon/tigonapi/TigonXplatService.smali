.class public Lcom/facebook/tigon/tigonapi/TigonXplatService;
.super Lcom/facebook/tigon/iface/TigonServiceHolder;
.source ""

# interfaces
.implements LX/1AG;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/jni/HybridData;LX/03V;)V
    .locals 3

    .prologue
    .line 210241
    invoke-direct {p0, p1}, Lcom/facebook/tigon/iface/TigonServiceHolder;-><init>(Lcom/facebook/jni/HybridData;)V

    .line 210242
    :try_start_0
    invoke-static {}, LX/1hc;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 210243
    return-void

    .line 210244
    :catch_0
    move-exception v0

    .line 210245
    if-eqz p2, :cond_0

    .line 210246
    const-string v1, "Tigon: TigonXplatBodyStream"

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 210247
    :cond_0
    throw v0
.end method

.method private native sendRequestBodyBufferByteBuffer(Lcom/facebook/tigon/iface/TigonRequest;Ljava/nio/ByteBuffer;ILcom/facebook/tigon/tigonapi/TigonBodyProvider;Lcom/facebook/tigon/tigonapi/TigonDirectBufferCallbacks;Ljava/util/concurrent/Executor;)Lcom/facebook/tigon/tigonapi/TigonXplatRequestToken;
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method private native sendRequestByteBuffer(Lcom/facebook/tigon/iface/TigonRequest;Ljava/nio/ByteBuffer;I[Ljava/nio/ByteBuffer;ILcom/facebook/tigon/tigonapi/TigonDirectBufferCallbacks;Ljava/util/concurrent/Executor;)Lcom/facebook/tigon/tigonapi/TigonXplatRequestToken;
    .param p4    # [Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method private native sendRequestJavaBuffer(Lcom/facebook/tigon/iface/TigonRequest;Ljava/nio/ByteBuffer;I[BLcom/facebook/tigon/tigonapi/TigonCallbacks;Ljava/util/concurrent/Executor;)Lcom/facebook/tigon/tigonapi/TigonXplatRequestToken;
    .param p4    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method


# virtual methods
.method public final a(Lcom/facebook/tigon/iface/TigonRequest;Lcom/facebook/tigon/tigonapi/TigonBodyProvider;Lcom/facebook/tigon/tigonapi/TigonDirectBufferCallbacks;Ljava/util/concurrent/Executor;)Lcom/facebook/tigon/tigonapi/TigonRequestToken;
    .locals 7

    .prologue
    .line 210238
    invoke-virtual {p0}, Lcom/facebook/tigon/tigonapi/TigonXplatService;->b()V

    .line 210239
    invoke-static {p1}, LX/1iz;->a(Lcom/facebook/tigon/iface/TigonRequest;)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 210240
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/tigon/tigonapi/TigonXplatService;->sendRequestBodyBufferByteBuffer(Lcom/facebook/tigon/iface/TigonRequest;Ljava/nio/ByteBuffer;ILcom/facebook/tigon/tigonapi/TigonBodyProvider;Lcom/facebook/tigon/tigonapi/TigonDirectBufferCallbacks;Ljava/util/concurrent/Executor;)Lcom/facebook/tigon/tigonapi/TigonXplatRequestToken;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/tigon/iface/TigonRequest;[Ljava/nio/ByteBuffer;ILcom/facebook/tigon/tigonapi/TigonDirectBufferCallbacks;Ljava/util/concurrent/Executor;)Lcom/facebook/tigon/tigonapi/TigonRequestToken;
    .locals 8
    .param p2    # [Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 210248
    invoke-virtual {p0}, Lcom/facebook/tigon/tigonapi/TigonXplatService;->b()V

    .line 210249
    invoke-static {p1}, LX/1iz;->a(Lcom/facebook/tigon/iface/TigonRequest;)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 210250
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/facebook/tigon/tigonapi/TigonXplatService;->sendRequestByteBuffer(Lcom/facebook/tigon/iface/TigonRequest;Ljava/nio/ByteBuffer;I[Ljava/nio/ByteBuffer;ILcom/facebook/tigon/tigonapi/TigonDirectBufferCallbacks;Ljava/util/concurrent/Executor;)Lcom/facebook/tigon/tigonapi/TigonXplatRequestToken;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 210235
    iget-object v0, p0, Lcom/facebook/tigon/iface/TigonServiceHolder;->mHybridData:Lcom/facebook/jni/HybridData;

    if-nez v0, :cond_0

    .line 210236
    const/4 v0, 0x0

    .line 210237
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/tigon/iface/TigonServiceHolder;->mHybridData:Lcom/facebook/jni/HybridData;

    invoke-virtual {v0}, Lcom/facebook/jni/HybridData;->isValid()Z

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 210234
    return-void
.end method

.method public native hasSecretaryService()Z
.end method

.method public native releaseBodyBuffer(Ljava/nio/ByteBuffer;)V
.end method
