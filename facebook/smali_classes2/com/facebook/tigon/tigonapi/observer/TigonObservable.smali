.class public Lcom/facebook/tigon/tigonapi/observer/TigonObservable;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field public final b:LX/0ou;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ou",
            "<",
            "Lcom/facebook/tigon/tigonapi/observer/TigonObservable$TigonObservableRunnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210349
    const-string v0, "tigonliger"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 210350
    return-void
.end method

.method private a(ILcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V
    .locals 3
    .param p1    # I
        .annotation build Lcom/facebook/tigon/tigonapi/observer/TigonObservable$ObserverStep;
        .end annotation
    .end param

    .prologue
    .line 210344
    iget-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObservable;->b:LX/0ou;

    invoke-virtual {v0}, LX/0ou;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tigon/tigonapi/observer/TigonObservable$TigonObservableRunnable;

    .line 210345
    iput p1, v0, Lcom/facebook/tigon/tigonapi/observer/TigonObservable$TigonObservableRunnable;->b:I

    .line 210346
    iput-object p2, v0, Lcom/facebook/tigon/tigonapi/observer/TigonObservable$TigonObservableRunnable;->c:Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;

    .line 210347
    iget-object v1, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObservable;->a:Ljava/util/concurrent/Executor;

    const v2, -0x2b0ec00f

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 210348
    return-void
.end method

.method private native initHybrid(Lcom/facebook/tigon/tigonapi/TigonXplatService;)Lcom/facebook/jni/HybridData;
.end method

.method private onAdded(Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210342
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/facebook/tigon/tigonapi/observer/TigonObservable;->a(ILcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V

    .line 210343
    return-void
.end method

.method private onEOM(Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210340
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lcom/facebook/tigon/tigonapi/observer/TigonObservable;->a(ILcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V

    .line 210341
    return-void
.end method

.method private onError(Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210332
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Lcom/facebook/tigon/tigonapi/observer/TigonObservable;->a(ILcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V

    .line 210333
    return-void
.end method

.method private onResponse(Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210338
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/facebook/tigon/tigonapi/observer/TigonObservable;->a(ILcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V

    .line 210339
    return-void
.end method

.method private onStarted(Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210336
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/facebook/tigon/tigonapi/observer/TigonObservable;->a(ILcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V

    .line 210337
    return-void
.end method

.method private onWillRetry(Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210334
    const/4 v0, 0x5

    invoke-direct {p0, v0, p1}, Lcom/facebook/tigon/tigonapi/observer/TigonObservable;->a(ILcom/facebook/tigon/tigonapi/observer/TigonObserverData;)V

    .line 210335
    return-void
.end method
