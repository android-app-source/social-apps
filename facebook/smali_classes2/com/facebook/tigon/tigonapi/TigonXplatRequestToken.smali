.class public Lcom/facebook/tigon/tigonapi/TigonXplatRequestToken;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/tigon/tigonapi/TigonRequestToken;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 235241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235242
    iput-object p1, p0, Lcom/facebook/tigon/tigonapi/TigonXplatRequestToken;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 235243
    return-void
.end method

.method private native changePriority(II)V
.end method


# virtual methods
.method public final a(LX/1iO;)V
    .locals 2

    .prologue
    .line 235244
    iget v0, p1, LX/1iO;->a:I

    iget v1, p1, LX/1iO;->b:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/tigon/tigonapi/TigonXplatRequestToken;->changePriority(II)V

    .line 235245
    return-void
.end method

.method public native cancel()V
.end method
