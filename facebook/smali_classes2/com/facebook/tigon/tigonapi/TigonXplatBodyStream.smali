.class public Lcom/facebook/tigon/tigonapi/TigonXplatBodyStream;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/tigon/tigonapi/TigonBodyStream;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    .prologue
    .line 235239
    invoke-static {}, LX/1hc;->a()V

    .line 235240
    return-void
.end method

.method public constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0

    .prologue
    .line 235236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235237
    iput-object p1, p0, Lcom/facebook/tigon/tigonapi/TigonXplatBodyStream;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 235238
    return-void
.end method

.method private native reportErrorNative(Ljava/nio/ByteBuffer;I)V
.end method

.method private native transferBytesNative(Ljava/nio/ByteBuffer;I)I
.end method

.method private native writeEOMNative()V
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;I)I
    .locals 1

    .prologue
    .line 235235
    invoke-direct {p0, p1, p2}, Lcom/facebook/tigon/tigonapi/TigonXplatBodyStream;->transferBytesNative(Ljava/nio/ByteBuffer;I)I

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 235230
    invoke-direct {p0}, Lcom/facebook/tigon/tigonapi/TigonXplatBodyStream;->writeEOMNative()V

    .line 235231
    return-void
.end method

.method public final a(Lcom/facebook/tigon/tigonapi/TigonError;)V
    .locals 2

    .prologue
    .line 235232
    invoke-static {p1}, LX/1iz;->a(Lcom/facebook/tigon/tigonapi/TigonError;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 235233
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/tigon/tigonapi/TigonXplatBodyStream;->reportErrorNative(Ljava/nio/ByteBuffer;I)V

    .line 235234
    return-void
.end method
