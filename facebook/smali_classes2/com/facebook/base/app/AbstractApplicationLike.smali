.class public abstract Lcom/facebook/base/app/AbstractApplicationLike;
.super Lcom/facebook/base/app/ApplicationLike;
.source ""

# interfaces
.implements LX/02k;


# static fields
.field public static final i:Ljava/lang/String;


# instance fields
.field public final a:Landroid/app/Application;

.field public b:LX/0Se;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public volatile d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Vt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ra;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/03S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/00H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ul;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:Ljava/util/concurrent/atomic/AtomicInteger;

.field private k:LX/005;

.field private l:LX/0QA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54192
    const-class v0, Lcom/facebook/base/app/AbstractApplicationLike;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/base/app/AbstractApplicationLike;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;LX/00H;LX/005;)V
    .locals 1

    .prologue
    .line 54186
    invoke-direct {p0}, Lcom/facebook/base/app/ApplicationLike;-><init>()V

    .line 54187
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 54188
    iput-object p1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->a:Landroid/app/Application;

    .line 54189
    sput-object p2, LX/00H;->a:LX/00H;

    .line 54190
    iput-object p3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:LX/005;

    .line 54191
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 54178
    sget-object v0, LX/0Um;->a:[I

    iget-object v1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->g:LX/00H;

    .line 54179
    iget-object p0, v1, LX/00H;->i:LX/01S;

    move-object v1, p0

    .line 54180
    invoke-virtual {v1}, LX/01S;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 54181
    const/4 v0, 0x5

    .line 54182
    :goto_0
    invoke-static {v0}, LX/01m;->a(I)V

    .line 54183
    return-void

    .line 54184
    :pswitch_0
    const/4 v0, 0x3

    .line 54185
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final declared-synchronized a()LX/0QA;
    .locals 2

    .prologue
    .line 54172
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->l:LX/0QA;

    if-nez v0, :cond_0

    .line 54173
    const v0, 0x56e79620

    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 54174
    :catch_0
    move-exception v0

    .line 54175
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 54176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 54177
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->l:LX/0QA;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 54079
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ra;

    .line 54080
    sparse-switch p1, :sswitch_data_0

    .line 54081
    :goto_0
    return-void

    .line 54082
    :sswitch_0
    invoke-virtual {v0}, LX/0ra;->a()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xf -> :sswitch_0
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(LX/00G;)V
    .locals 0

    .prologue
    .line 54171
    return-void
.end method

.method public b()V
    .locals 15

    .prologue
    const/4 v8, 0x0

    .line 54089
    const-class v0, LX/0Pv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 54090
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 54091
    if-eqz v0, :cond_0

    .line 54092
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->a(I)V

    .line 54093
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 54094
    invoke-static {}, LX/00m;->c()V

    .line 54095
    :try_start_0
    const-string v2, "android.os.AsyncTask"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54096
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/facebook/base/app/AbstractApplicationLike;->a:Landroid/app/Application;

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 54097
    :goto_1
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v2

    .line 54098
    iget-object v3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v3

    .line 54099
    const-string v4, "app_on_create_count"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 54100
    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v2

    .line 54101
    invoke-virtual {p0, v2}, Lcom/facebook/base/app/AbstractApplicationLike;->a(LX/00G;)V

    .line 54102
    iget-object v3, v2, LX/00G;->b:Ljava/lang/String;

    move-object v3, v3

    .line 54103
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 54104
    const-string v3, "empty"

    .line 54105
    :goto_2
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v4

    const-string v5, "process_name_on_start"

    invoke-virtual {v4, v5, v3}, LX/009;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 54106
    monitor-enter p0

    .line 54107
    :try_start_2
    iget-object v3, v2, LX/00G;->c:LX/014;

    move-object v3, v3

    .line 54108
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    new-instance v5, LX/0Q1;

    invoke-direct {v5, v3}, LX/0Q1;-><init>(LX/014;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v3

    invoke-static {v2}, LX/0Q5;->a(LX/00G;)LX/0Q6;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v3, v3

    .line 54109
    iget-object v4, v2, LX/00G;->c:LX/014;

    move-object v4, v4

    .line 54110
    iget-object v5, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:LX/005;

    const-string v6, "ColdStart/FBInjector.create"

    const v7, 0x700004

    invoke-virtual {v5, v6, v7}, LX/005;->b(Ljava/lang/String;I)LX/00X;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 54111
    :try_start_3
    iget-object v5, p0, Lcom/facebook/base/app/AbstractApplicationLike;->a:Landroid/app/Application;

    move-object v5, v5

    .line 54112
    sget-boolean v6, LX/007;->i:Z

    move v6, v6

    .line 54113
    new-instance v9, LX/0Q8;

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 54114
    new-instance v10, LX/0R2;

    invoke-direct {v10, v11, v11, v12, v12}, LX/0R2;-><init>(ZZLX/4ff;LX/4fe;)V

    move-object v12, v10

    .line 54115
    move-object v10, v5

    move-object v11, v3

    move-object v13, v4

    move v14, v6

    invoke-direct/range {v9 .. v14}, LX/0Q8;-><init>(Landroid/content/Context;Ljava/util/List;LX/0R2;LX/014;Z)V

    move-object v3, v9

    .line 54116
    iput-object v3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->l:LX/0QA;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 54117
    :try_start_4
    iget-object v3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:LX/005;

    const-string v4, "ColdStart/FBInjector.create"

    invoke-virtual {v3, v4}, LX/005;->a(Ljava/lang/String;)V

    .line 54118
    const-string v3, "ApplicationLike.onCreate#notifyAll"

    const v4, -0x14147071

    invoke-static {v3, v4}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 54119
    const v3, -0x445e58c6

    :try_start_5
    invoke-static {p0, v3}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 54120
    const v3, -0x3c10f1a7

    :try_start_6
    invoke-static {v3}, LX/02m;->a(I)V

    .line 54121
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 54122
    iget-object v3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:LX/005;

    const-string v4, "ColdStart/FBInjector.inject"

    const v5, 0x700005

    invoke-virtual {v3, v4, v5}, LX/005;->b(Ljava/lang/String;I)LX/00X;

    .line 54123
    :try_start_7
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    move-object v5, p0

    check-cast v5, Lcom/facebook/base/app/AbstractApplicationLike;

    invoke-static {v3}, LX/0Se;->a(LX/0QB;)LX/0Se;

    move-result-object v6

    check-cast v6, LX/0Se;

    invoke-static {v3}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    const/16 v9, 0x10c5

    invoke-static {v3, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2c6

    invoke-static {v3, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v3}, LX/03S;->a(LX/0QB;)LX/03S;

    move-result-object v11

    check-cast v11, LX/03S;

    const-class v12, LX/00H;

    invoke-interface {v3, v12}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/00H;

    invoke-static {v3}, LX/0Ul;->a(LX/0QB;)LX/0Ul;

    move-result-object v3

    check-cast v3, LX/0Ul;

    iput-object v6, v5, Lcom/facebook/base/app/AbstractApplicationLike;->b:LX/0Se;

    iput-object v7, v5, Lcom/facebook/base/app/AbstractApplicationLike;->c:Ljava/util/concurrent/Executor;

    iput-object v9, v5, Lcom/facebook/base/app/AbstractApplicationLike;->d:LX/0Ot;

    iput-object v10, v5, Lcom/facebook/base/app/AbstractApplicationLike;->e:LX/0Ot;

    iput-object v11, v5, Lcom/facebook/base/app/AbstractApplicationLike;->f:LX/03S;

    iput-object v12, v5, Lcom/facebook/base/app/AbstractApplicationLike;->g:LX/00H;

    iput-object v3, v5, Lcom/facebook/base/app/AbstractApplicationLike;->h:LX/0Ul;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 54124
    iget-object v3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:LX/005;

    const-string v4, "ColdStart/FBInjector.inject"

    invoke-virtual {v3, v4}, LX/005;->a(Ljava/lang/String;)V

    .line 54125
    iput-object v8, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:LX/005;

    .line 54126
    invoke-direct {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->g()V

    .line 54127
    invoke-virtual {p0, v2}, Lcom/facebook/base/app/AbstractApplicationLike;->b(LX/00G;)V

    .line 54128
    const-string v2, "FbAppInitializer.run"

    const v3, 0x492f9f80    # 719352.0f

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 54129
    :try_start_8
    iget-object v2, p0, Lcom/facebook/base/app/AbstractApplicationLike;->b:LX/0Se;

    .line 54130
    iget-object v3, v2, LX/0Se;->a:LX/0Sf;

    .line 54131
    invoke-static {v3}, LX/0Sf;->c$redex0(LX/0Sf;)V

    .line 54132
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 54133
    iget-boolean v5, v3, LX/0Sf;->A:Z

    if-nez v5, :cond_3

    move v5, v6

    :goto_3
    const-string v7, "FbAppInitializer should only be run once."

    invoke-static {v5, v7}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 54134
    iput-boolean v6, v3, LX/0Sf;->A:Z

    .line 54135
    invoke-static {v3}, LX/0Sf;->d(LX/0Sf;)LX/0TD;

    move-result-object v5

    .line 54136
    iget-object v6, v3, LX/0Sf;->e:LX/0Sg;

    const-string v7, "FbAppInitializer-HiPri"

    new-instance v8, Lcom/facebook/common/init/impl/FbAppInitializerInternal$1;

    invoke-direct {v8, v3, v5}, Lcom/facebook/common/init/impl/FbAppInitializerInternal$1;-><init>(LX/0Sf;LX/0TD;)V

    sget-object v9, LX/0VZ;->STARTUP_INITIALIZATION:LX/0VZ;

    invoke-virtual {v6, v7, v8, v9, v5}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;Ljava/util/concurrent/ExecutorService;)LX/0Va;

    move-result-object v6

    .line 54137
    iget-object v5, v3, LX/0Sf;->k:LX/0TE;

    iget-object v7, v3, LX/0Sf;->j:Ljava/util/concurrent/Executor;

    invoke-static {v6, v5, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 54138
    if-nez v4, :cond_1

    .line 54139
    iget-object v5, v3, LX/0Sf;->e:LX/0Sg;

    const-string v7, "FbAppInitializer-lowPriUiThread"

    new-instance v8, Lcom/facebook/common/init/impl/FbAppInitializerInternal$2;

    invoke-direct {v8, v3}, Lcom/facebook/common/init/impl/FbAppInitializerInternal$2;-><init>(LX/0Sf;)V

    sget-object v9, LX/0VZ;->APPLICATION_LOADED_HIGH_PRIORITY:LX/0VZ;

    sget-object v2, LX/0Vm;->UI:LX/0Vm;

    invoke-virtual {v5, v7, v8, v9, v2}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    move-result-object v5

    .line 54140
    iget-object v7, v3, LX/0Sf;->k:LX/0TE;

    iget-object v8, v3, LX/0Sf;->j:Ljava/util/concurrent/Executor;

    invoke-static {v5, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 54141
    iget-object v5, v3, LX/0Sf;->p:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 54142
    const/4 v7, 0x1

    invoke-static {v3, v5, v7}, LX/0Sf;->a(LX/0Sf;Ljava/util/Iterator;Z)V

    .line 54143
    const/4 v5, 0x0

    iput-object v5, v3, LX/0Sf;->p:LX/0Ot;

    .line 54144
    :cond_1
    iget-object v5, v3, LX/0Sf;->f:LX/0Sg;

    invoke-virtual {v5}, LX/0Sg;->d()V

    .line 54145
    move-object v4, v6

    .line 54146
    move-object v3, v4

    .line 54147
    move-object v2, v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 54148
    const v3, 0x6698d2a1

    invoke-static {v3}, LX/02m;->a(I)V

    .line 54149
    iget-object v3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    .line 54150
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long v0, v4, v0

    const-wide/32 v4, 0xf4240

    div-long/2addr v0, v4

    .line 54151
    iget-object v3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->h:LX/0Ul;

    .line 54152
    iget-wide v9, v3, LX/0Ul;->a:J

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-eqz v9, :cond_4

    .line 54153
    :goto_4
    new-instance v0, LX/0XS;

    invoke-direct {v0, p0}, LX/0XS;-><init>(Lcom/facebook/base/app/AbstractApplicationLike;)V

    iget-object v1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->c:Ljava/util/concurrent/Executor;

    invoke-static {v2, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 54154
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->f:LX/03S;

    .line 54155
    new-instance v1, LX/03c;

    invoke-direct {v1, v0}, LX/03c;-><init>(LX/03S;)V

    invoke-static {v1}, LX/00k;->a(LX/00j;)V

    .line 54156
    new-instance v0, LX/0XT;

    invoke-direct {v0}, LX/0XT;-><init>()V

    invoke-static {v0}, LX/00k;->a(LX/00j;)V

    .line 54157
    return-void

    .line 54158
    :catchall_0
    move-exception v0

    :try_start_9
    iget-object v1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:LX/005;

    const-string v2, "ColdStart/FBInjector.create"

    invoke-virtual {v1, v2}, LX/005;->a(Ljava/lang/String;)V

    throw v0

    .line 54159
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v0

    .line 54160
    :catchall_2
    move-exception v0

    const v1, -0x6adcb975

    :try_start_a
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 54161
    :catchall_3
    move-exception v0

    iget-object v1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:LX/005;

    const-string v2, "ColdStart/FBInjector.inject"

    invoke-virtual {v1, v2}, LX/005;->a(Ljava/lang/String;)V

    .line 54162
    iput-object v8, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:LX/005;

    throw v0

    .line 54163
    :catchall_4
    move-exception v0

    const v1, 0x1947ca41

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 54164
    :catch_0
    move-exception v2

    .line 54165
    sget-object v3, Lcom/facebook/base/app/AbstractApplicationLike;->i:Ljava/lang/String;

    const-string v4, "Exception trying to initialize AsyncTask"

    invoke-static {v3, v4, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_1
    goto/16 :goto_1

    .line 54166
    :catch_2
    goto/16 :goto_1

    .line 54167
    :cond_2
    iget-object v3, v2, LX/00G;->b:Ljava/lang/String;

    move-object v3, v3

    .line 54168
    goto/16 :goto_2

    .line 54169
    :cond_3
    :try_start_b
    const/4 v5, 0x0

    goto/16 :goto_3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 54170
    :cond_4
    iget-object v9, v3, LX/0Ul;->b:LX/0So;

    invoke-interface {v9}, LX/0So;->now()J

    move-result-wide v9

    sub-long/2addr v9, v0

    iput-wide v9, v3, LX/0Ul;->a:J

    goto :goto_4
.end method

.method public b(LX/00G;)V
    .locals 0

    .prologue
    .line 54088
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 54086
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ra;

    invoke-virtual {v0}, LX/0ra;->a()V

    .line 54087
    return-void
.end method

.method public final synthetic getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 54084
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->a:Landroid/app/Application;

    move-object v0, v0

    .line 54085
    return-object v0
.end method

.method public final l_()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 54083
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->d:LX/0Ot;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vt;

    goto :goto_0
.end method
