.class public Lcom/facebook/base/activity/FbFragmentActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source ""

# interfaces
.implements LX/0ev;
.implements LX/0ew;
.implements LX/0ex;
.implements LX/0ey;
.implements LX/0ez;
.implements LX/0Xn;
.implements LX/0f0;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field private static final p:Ljava/lang/Class;


# instance fields
.field public A:Landroid/view/LayoutInflater$Factory;

.field public B:LX/0js;

.field public C:LX/0jd;

.field public D:LX/0Uq;

.field public E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jo;",
            ">;"
        }
    .end annotation
.end field

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oh;",
            ">;"
        }
    .end annotation
.end field

.field private G:Ljava/lang/String;

.field private final q:LX/0jb;

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/common/touchlistener/FbTouchEventActivityListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Z

.field public t:LX/0Vt;

.field public u:LX/0jf;

.field public v:LX/0jh;

.field public w:LX/03V;

.field public x:Ljava/lang/String;

.field public y:Z

.field public z:LX/0VI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105739
    const-class v0, Lcom/facebook/base/activity/FbFragmentActivity;

    sput-object v0, Lcom/facebook/base/activity/FbFragmentActivity;->p:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 105727
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 105728
    new-instance v0, LX/0jb;

    invoke-direct {v0}, LX/0jb;-><init>()V

    iput-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->q:LX/0jb;

    .line 105729
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->G:Ljava/lang/String;

    .line 105730
    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/FbFragmentActivity;)V
    .locals 0

    .prologue
    .line 105726
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->iA_()V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/FbFragmentActivity;ILandroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 105725
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 105723
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->t:LX/0Vt;

    invoke-virtual {v0}, LX/0Vt;->a()V

    .line 105724
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 105722
    invoke-static {p0, p1}, LX/0jc;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 105721
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0T2;)V
    .locals 1

    .prologue
    .line 105689
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(LX/0T2;)V

    .line 105690
    return-void
.end method

.method public a(LX/0je;)V
    .locals 1

    .prologue
    .line 105714
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->u:LX/0jf;

    invoke-virtual {v0, p1}, LX/0jf;->a(LX/0je;)V

    .line 105715
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 105712
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->s:Z

    .line 105713
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105711
    return-void
.end method

.method public a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 105708
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 105709
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/support/v4/app/Fragment;)V

    .line 105710
    return-void
.end method

.method public a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 105707
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public final addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 105704
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105705
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 105706
    :cond_0
    return-void
.end method

.method public attachBaseContext(Landroid/content/Context;)V
    .locals 11

    .prologue
    .line 105696
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->attachBaseContext(Landroid/content/Context;)V

    .line 105697
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iy_()V

    .line 105698
    const-string v0, "%s.attachBaseContext()"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, -0x24308c2e

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 105699
    :try_start_0
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v1, p0

    check-cast v1, Lcom/facebook/base/activity/FbFragmentActivity;

    invoke-static {p1}, LX/0Vs;->a(LX/0QB;)LX/0Vs;

    move-result-object v2

    check-cast v2, LX/0Vt;

    invoke-static {p1}, LX/0jf;->b(LX/0QB;)LX/0jf;

    move-result-object v3

    check-cast v3, LX/0jf;

    invoke-static {p1}, LX/0jg;->a(LX/0QB;)LX/0jh;

    move-result-object v4

    check-cast v4, LX/0jh;

    new-instance v5, LX/0ji;

    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    invoke-direct {v5, v6}, LX/0ji;-><init>(LX/0QB;)V

    move-object v5, v5

    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    invoke-static {v5, v6}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v5

    move-object v5, v5

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    const-class v7, Landroid/content/Context;

    invoke-interface {p1, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    const/4 v7, 0x0

    move-object v7, v7

    move-object v7, v7

    check-cast v7, Landroid/view/LayoutInflater$Factory;

    invoke-static {p1}, LX/0jd;->b(LX/0QB;)LX/0jd;

    move-result-object v8

    check-cast v8, LX/0jd;

    invoke-static {p1}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v9

    check-cast v9, LX/0Uq;

    const/16 v10, 0x28a

    invoke-static {p1, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v0, 0x2a8

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, v1, Lcom/facebook/base/activity/FbFragmentActivity;->t:LX/0Vt;

    iput-object v3, v1, Lcom/facebook/base/activity/FbFragmentActivity;->u:LX/0jf;

    iput-object v4, v1, Lcom/facebook/base/activity/FbFragmentActivity;->v:LX/0jh;

    iput-object v5, v1, Lcom/facebook/base/activity/FbFragmentActivity;->r:LX/0Ot;

    iput-object v6, v1, Lcom/facebook/base/activity/FbFragmentActivity;->w:LX/03V;

    iput-object v7, v1, Lcom/facebook/base/activity/FbFragmentActivity;->A:Landroid/view/LayoutInflater$Factory;

    iput-object v8, v1, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    iput-object v9, v1, Lcom/facebook/base/activity/FbFragmentActivity;->D:LX/0Uq;

    iput-object v10, v1, Lcom/facebook/base/activity/FbFragmentActivity;->E:LX/0Ot;

    iput-object p1, v1, Lcom/facebook/base/activity/FbFragmentActivity;->F:LX/0Ot;

    .line 105700
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->q()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105701
    const v0, -0x2d2fed48

    invoke-static {v0}, LX/02m;->a(I)V

    .line 105702
    return-void

    .line 105703
    :catchall_0
    move-exception v0

    const v1, 0x32d884c2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public b(Landroid/content/Intent;)LX/0cQ;
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 105691
    if-nez p1, :cond_1

    .line 105692
    :cond_0
    :goto_0
    return-object v0

    .line 105693
    :cond_1
    const-string v1, "target_fragment"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105694
    const-string v0, "target_fragment"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 105695
    invoke-static {v0}, LX/0jj;->a(I)LX/0cQ;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 105737
    invoke-virtual {p1, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105738
    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0T2;)V
    .locals 1

    .prologue
    .line 105735
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->b(LX/0T2;)V

    .line 105736
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105772
    return-void
.end method

.method public final c(I)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)",
            "LX/0am",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 105771
    invoke-static {p0, p1}, LX/0jc;->a(Landroid/app/Activity;I)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105770
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 105758
    const-string v0, "FbFragmentActivity.dispatchTouchEvent"

    const v1, -0x38cab968

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 105759
    :try_start_0
    const-string v0, "FbActivityListeners.onTouchEvent"

    const v1, -0x5ade19b0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 105760
    :try_start_1
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jk;

    .line 105761
    iget-object v2, v0, LX/0jk;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    .line 105762
    iget-object v2, v0, LX/0jk;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jl;

    .line 105763
    invoke-interface {v2, v4, v5}, LX/0jl;->a(J)V

    goto :goto_1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105764
    :cond_0
    goto :goto_0

    .line 105765
    :catchall_0
    move-exception v0

    const v1, 0x5ecb42c0

    :try_start_2
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 105766
    :catchall_1
    move-exception v0

    const v1, 0x556f47d6

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 105767
    :cond_1
    const v0, -0x715f1c5e

    :try_start_3
    invoke-static {v0}, LX/02m;->a(I)V

    .line 105768
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v0

    .line 105769
    const v1, -0x23eda516

    invoke-static {v1}, LX/02m;->a(I)V

    return v0
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 105755
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 105756
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->m()V

    .line 105757
    return-void
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 105753
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->c()Landroid/view/MenuInflater;

    move-result-object v0

    .line 105754
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    goto :goto_0
.end method

.method public getProperty(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105752
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->q:LX/0jb;

    invoke-virtual {v0, p1}, LX/0jb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 105751
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->t:LX/0Vt;

    return-object v0
.end method

.method public final iA_()V
    .locals 1

    .prologue
    .line 105748
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105749
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->iA_()V

    .line 105750
    :cond_0
    return-void
.end method

.method public iy_()V
    .locals 0

    .prologue
    .line 105747
    return-void
.end method

.method public iz_()LX/0QA;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 105746
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public lj_()V
    .locals 1

    .prologue
    .line 105743
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->lj_()V

    .line 105744
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->i()V

    .line 105745
    return-void
.end method

.method public mp_()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0T2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105740
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    .line 105741
    iget-object p0, v0, LX/0jd;->b:Ljava/util/Set;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p0

    move-object v0, p0

    .line 105742
    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 105575
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 105576
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1, p2, p3}, LX/0jd;->a(IILandroid/content/Intent;)V

    .line 105577
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 105716
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 105717
    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 105718
    :cond_0
    :goto_0
    return-void

    .line 105719
    :cond_1
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105720
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 105731
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->q()V

    .line 105732
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 105733
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/content/res/Configuration;)V

    .line 105734
    return-void
.end method

.method public onContentChanged()V
    .locals 1

    .prologue
    .line 105569
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onContentChanged()V

    .line 105570
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->n()V

    .line 105571
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, -0xb091c40

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 105532
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->D:LX/0Uq;

    invoke-virtual {v0}, LX/0Uq;->b()V

    .line 105533
    const-string v0, "%s.onCreate"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x27200e5b

    invoke-static {v0, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 105534
    if-eqz p1, :cond_0

    .line 105535
    :try_start_0
    const-class v0, Lcom/facebook/base/activity/FbFragmentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 105536
    :cond_0
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    .line 105537
    new-instance v2, LX/0jm;

    invoke-direct {v2, p0}, LX/0jm;-><init>(Lcom/facebook/base/activity/FbFragmentActivity;)V

    move-object v2, v2

    .line 105538
    invoke-virtual {v0, p0, v2}, LX/0jd;->a(Landroid/app/Activity;LX/0jn;)V

    .line 105539
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/os/Bundle;)Z

    move-result v0

    .line 105540
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 105541
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105542
    if-eqz v0, :cond_1

    .line 105543
    const v0, 0x206adfa2

    invoke-static {v0}, LX/02m;->a(I)V

    const/16 v0, 0x23

    const v2, 0x9db23b5

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 105544
    :goto_0
    return-void

    .line 105545
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 105546
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->b(Landroid/os/Bundle;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 105547
    const v0, -0x1328ad80

    invoke-static {v0}, LX/02m;->a(I)V

    const v0, -0x3408b3f6

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_0

    .line 105548
    :cond_2
    :try_start_2
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/content/Intent;)LX/0cQ;

    move-result-object v0

    .line 105549
    if-nez v0, :cond_4

    .line 105550
    :cond_3
    :goto_1
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 105551
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->d()V

    .line 105552
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_FLAG_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->G:Ljava/lang/String;

    .line 105553
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->w:LX/03V;

    iget-object v2, p0, Lcom/facebook/base/activity/FbFragmentActivity;->G:Ljava/lang/String;

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 105554
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 105555
    iput-boolean v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->y:Z

    .line 105556
    iget-boolean v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->y:Z

    if-nez v0, :cond_6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 105557
    :goto_2
    const v0, -0x2d6abc00

    invoke-static {v0}, LX/02m;->a(I)V

    .line 105558
    const v0, -0x28d699cd

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_0

    .line 105559
    :catchall_0
    move-exception v0

    const v2, 0xbf3c857

    invoke-static {v2}, LX/02m;->a(I)V

    const v2, 0x206d9953

    invoke-static {v2, v1}, LX/02F;->c(II)V

    throw v0

    .line 105560
    :cond_4
    :try_start_3
    iget-object v2, p0, Lcom/facebook/base/activity/FbFragmentActivity;->E:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jo;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-interface {v2, v3}, LX/0jo;->b(I)Ljava/lang/Class;

    move-result-object v2

    .line 105561
    if-eqz v2, :cond_3

    .line 105562
    const-class v3, LX/0jp;

    invoke-virtual {v3, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 105563
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 105564
    iget-object v2, p0, Lcom/facebook/base/activity/FbFragmentActivity;->E:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jo;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-interface {v2, v3}, LX/0jo;->a(I)LX/0jq;

    move-result-object v2

    .line 105565
    check-cast v2, LX/0jp;

    iget-object v3, p0, Lcom/facebook/base/activity/FbFragmentActivity;->F:LX/0Ot;

    invoke-interface {v2, v3}, LX/0jp;->a(LX/0Ot;)V

    goto/16 :goto_1

    .line 105566
    :cond_5
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    goto/16 :goto_1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 105567
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "dumpsys activity "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->x:Ljava/lang/String;

    .line 105568
    new-instance v0, LX/0jr;

    invoke-direct {v0, p0}, LX/0jr;-><init>(Lcom/facebook/base/activity/FbFragmentActivity;)V

    iput-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->z:LX/0VI;

    goto :goto_2
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 105530
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->c(I)Landroid/app/Dialog;

    move-result-object v0

    .line 105531
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 105528
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/view/Menu;)V

    .line 105529
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 105526
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(ILandroid/view/Menu;)LX/0am;

    move-result-object v0

    .line 105527
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 105524
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(I)LX/0am;

    move-result-object v0

    .line 105525
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 2

    .prologue
    .line 105520
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 105521
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/facebook/base/activity/FbFragmentActivity;->A:Landroid/view/LayoutInflater$Factory;

    if-eqz v1, :cond_0

    .line 105522
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->A:Landroid/view/LayoutInflater$Factory;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/LayoutInflater$Factory;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 105523
    :cond_0
    return-object v0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x6d7373bd

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 105513
    :try_start_0
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->u:LX/0jf;

    invoke-virtual {v0}, LX/0jf;->a()V

    .line 105514
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105515
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 105516
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->w:LX/03V;

    iget-object v2, p0, Lcom/facebook/base/activity/FbFragmentActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/03V;->a(Ljava/lang/String;)V

    .line 105517
    const v0, 0x1dc033b9

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 105518
    :catchall_0
    move-exception v0

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 105519
    iget-object v2, p0, Lcom/facebook/base/activity/FbFragmentActivity;->w:LX/03V;

    iget-object v3, p0, Lcom/facebook/base/activity/FbFragmentActivity;->G:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/03V;->a(Ljava/lang/String;)V

    const v2, 0x6fbe9e42

    invoke-static {v2, v1}, LX/02F;->c(II)V

    throw v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 105511
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(ILandroid/view/KeyEvent;)LX/0am;

    move-result-object v0

    .line 105512
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 105509
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->b(ILandroid/view/KeyEvent;)LX/0am;

    move-result-object v0

    .line 105510
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 105507
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(ILandroid/view/MenuItem;)LX/0am;

    move-result-object v0

    .line 105508
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onNewIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 105500
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 105501
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->v:LX/0jh;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/DeliverOnNewIntentWhenFinishing;

    invoke-virtual {v0, v1, v2}, LX/0jh;->a(Ljava/lang/reflect/AnnotatedElement;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105502
    :goto_0
    return-void

    .line 105503
    :cond_0
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/content/Intent;)V

    .line 105504
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->s:Z

    .line 105505
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 105506
    iget-boolean v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->s:Z

    const-string v1, "onActivityNewIntent didn\'t call super.onActivityNewIntent()"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 105499
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x3a78013f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 105495
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 105496
    iget-object v1, p0, Lcom/facebook/base/activity/FbFragmentActivity;->w:LX/03V;

    iget-object v2, p0, Lcom/facebook/base/activity/FbFragmentActivity;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/03V;->b(Ljava/lang/String;)V

    .line 105497
    iget-object v1, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v1}, LX/0jd;->g()V

    .line 105498
    const/16 v1, 0x23

    const v2, -0x6f53e48e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105492
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 105493
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->d(Landroid/os/Bundle;)V

    .line 105494
    return-void
.end method

.method public onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 105489
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(ILandroid/app/Dialog;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105490
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 105491
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 105601
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->b(Landroid/view/Menu;)V

    .line 105602
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 105687
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1, p2, p3}, LX/0jd;->a(ILandroid/view/View;Landroid/view/Menu;)LX/0am;

    move-result-object v0

    .line 105688
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 10

    .prologue
    .line 105623
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 105624
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->B:LX/0js;

    if-eqz v0, :cond_2

    .line 105625
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->B:LX/0js;

    .line 105626
    iget-object v1, v0, LX/0js;->a:LX/0i5;

    .line 105627
    if-nez p1, :cond_1

    .line 105628
    const/4 v2, 0x0

    .line 105629
    array-length v3, p3

    if-gtz v3, :cond_c

    .line 105630
    :cond_0
    :goto_0
    move v2, v2

    .line 105631
    if-eqz v2, :cond_3

    .line 105632
    iget-object v2, v1, LX/0i5;->g:LX/6Zx;

    invoke-interface {v2}, LX/6Zx;->a()V

    .line 105633
    :cond_1
    :goto_1
    iget-object v2, v1, LX/0i5;->f:Landroid/app/Activity;

    instance-of v2, v2, Lcom/facebook/base/activity/FbFragmentActivity;

    if-eqz v2, :cond_2

    .line 105634
    iget-object v2, v1, LX/0i5;->f:Landroid/app/Activity;

    check-cast v2, Lcom/facebook/base/activity/FbFragmentActivity;

    const/4 v3, 0x0

    .line 105635
    iput-object v3, v2, Lcom/facebook/base/activity/FbFragmentActivity;->B:LX/0js;

    .line 105636
    :cond_2
    return-void

    .line 105637
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 105638
    array-length v4, p2

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_5

    aget-object v5, p2, v2

    .line 105639
    iget-object p1, v1, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {p1, v5}, Landroid/app/Activity;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_4

    .line 105640
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105641
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 105642
    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object v2, v2

    .line 105643
    invoke-static {v1, p2}, LX/0i5;->j(LX/0i5;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 105644
    invoke-static {v1, p2}, LX/0i5;->k(LX/0i5;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 105645
    iget-object v5, v1, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    iget-object v5, v5, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->c:LX/0jt;

    iget-boolean v5, v5, LX/0jt;->shouldShowForSettingsStep:Z

    if-eqz v5, :cond_9

    array-length v5, v4

    if-lez v5, :cond_9

    iget-boolean v5, v1, LX/0i5;->e:Z

    if-eqz v5, :cond_9

    .line 105646
    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 105647
    iget-object v5, v1, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    .line 105648
    const v7, 0x7f031248

    iget-object v5, v1, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v6, v7, v5, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 105649
    new-instance v6, LX/0ju;

    iget-object v7, v1, LX/0i5;->f:Landroid/app/Activity;

    invoke-direct {v6, v7}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v5}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v6

    const v7, 0x7f081978

    new-instance v8, LX/0jv;

    invoke-direct {v8, v1, v2, v3, v4}, LX/0jv;-><init>(LX/0i5;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v6

    const v7, 0x7f081977

    new-instance v8, LX/0jw;

    invoke-direct {v8, v1, v3, v4}, LX/0jw;-><init>(LX/0i5;[Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v6

    .line 105650
    invoke-virtual {v6}, LX/0ju;->a()LX/2EJ;

    move-result-object v8

    .line 105651
    new-instance v6, LX/0jx;

    invoke-direct {v6, v1, v3, v4}, LX/0jx;-><init>(LX/0i5;[Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v8, v6}, LX/2EJ;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 105652
    const v6, 0x7f0d2b00

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/resources/ui/FbTextView;

    .line 105653
    const v7, 0x7f0d2b01

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/resources/ui/FbTextView;

    .line 105654
    iget-object v9, v1, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    iget-object v9, v9, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->a:Ljava/lang/String;

    if-eqz v9, :cond_e

    .line 105655
    iget-object v9, v1, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    iget-object v9, v9, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->a:Ljava/lang/String;

    invoke-virtual {v6, v9}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 105656
    :goto_3
    iget-object v6, v1, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    iget-object v6, v6, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->b:Ljava/lang/String;

    if-eqz v6, :cond_10

    .line 105657
    iget-object v6, v1, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    iget-object v6, v6, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->b:Ljava/lang/String;

    invoke-virtual {v7, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 105658
    :goto_4
    const v6, 0x7f0d2b02

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/resources/ui/FbTextView;

    .line 105659
    iget-object v6, v1, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f081974

    new-array v9, p3, [Ljava/lang/Object;

    .line 105660
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 105661
    array-length p1, v4

    const/4 p0, 0x0

    :goto_5
    if-ge p0, p1, :cond_6

    aget-object p3, v4, p0

    .line 105662
    invoke-static {p3}, LX/2rM;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 105663
    add-int/lit8 p0, p0, 0x1

    goto :goto_5

    .line 105664
    :cond_6
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 105665
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 105666
    iget-object p3, v1, LX/0i5;->f:Landroid/app/Activity;

    invoke-static {p0}, LX/2rM;->c(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {p3, p0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105667
    const-string p0, ","

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 105668
    :cond_7
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result p0

    if-lez p0, :cond_8

    .line 105669
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 105670
    :cond_8
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    move-object p0, p0

    .line 105671
    aput-object p0, v9, p2

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 105672
    invoke-virtual {v8}, LX/2EJ;->show()V

    .line 105673
    goto/16 :goto_1

    .line 105674
    :cond_9
    iget-boolean v2, v1, LX/0i5;->e:Z

    if-nez v2, :cond_a

    array-length v2, v4

    if-lez v2, :cond_b

    :cond_a
    const/4 v2, 0x1

    :goto_7
    iput-boolean v2, v1, LX/0i5;->e:Z

    .line 105675
    iget-object v2, v1, LX/0i5;->g:LX/6Zx;

    invoke-interface {v2, v3, v4}, LX/6Zx;->a([Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_1

    .line 105676
    :cond_b
    const/4 v2, 0x0

    goto :goto_7

    .line 105677
    :cond_c
    array-length v4, p3

    move v3, v2

    :goto_8
    if-ge v3, v4, :cond_d

    aget v5, p3, v3

    .line 105678
    if-nez v5, :cond_0

    .line 105679
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 105680
    :cond_d
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 105681
    :cond_e
    invoke-static {v4}, LX/0i5;->d([Ljava/lang/String;)I

    move-result v9

    if-le v9, p3, :cond_f

    .line 105682
    iget-object v9, v1, LX/0i5;->f:Landroid/app/Activity;

    const p0, 0x7f081958

    new-array v0, p3, [Ljava/lang/Object;

    iget-object p1, v1, LX/0i5;->j:Ljava/lang/String;

    aput-object p1, v0, p2

    invoke-virtual {v9, p0, v0}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 105683
    :cond_f
    iget-object v9, v1, LX/0i5;->f:Landroid/app/Activity;

    aget-object p0, v4, p2

    invoke-static {p0}, LX/2rM;->d(Ljava/lang/String;)I

    move-result p0

    new-array v0, p3, [Ljava/lang/Object;

    iget-object p1, v1, LX/0i5;->j:Ljava/lang/String;

    aput-object p1, v0, p2

    invoke-virtual {v9, p0, v0}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 105684
    :cond_10
    invoke-static {v4}, LX/0i5;->d([Ljava/lang/String;)I

    move-result v6

    if-le v6, p3, :cond_11

    .line 105685
    invoke-static {v1, v4}, LX/0i5;->f(LX/0i5;[Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 105686
    :cond_11
    iget-object v6, v1, LX/0i5;->f:Landroid/app/Activity;

    aget-object v9, v4, p2

    invoke-static {v9}, LX/2rM;->e(Ljava/lang/String;)I

    move-result v9

    new-array p0, p3, [Ljava/lang/Object;

    iget-object v0, v1, LX/0i5;->j:Ljava/lang/String;

    aput-object v0, p0, p2

    invoke-virtual {v6, v9, p0}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x228549a5

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 105614
    const-string v0, "%s.onResume"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const v3, -0x4b81b5dd

    invoke-static {v0, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 105615
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->q()V

    .line 105616
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 105617
    iget-boolean v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->y:Z

    if-eqz v0, :cond_0

    .line 105618
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->w:LX/03V;

    iget-object v2, p0, Lcom/facebook/base/activity/FbFragmentActivity;->x:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/base/activity/FbFragmentActivity;->z:LX/0VI;

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;LX/0VI;)V

    .line 105619
    :cond_0
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105620
    const v0, -0x1f732675

    invoke-static {v0}, LX/02m;->a(I)V

    .line 105621
    const v0, -0x14a321b7

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 105622
    :catchall_0
    move-exception v0

    const v2, 0x64b2a0b7

    invoke-static {v2}, LX/02m;->a(I)V

    const v2, -0x1bbbfcd6

    invoke-static {v2, v1}, LX/02F;->c(II)V

    throw v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 105611
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 105612
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->c(Landroid/os/Bundle;)V

    .line 105613
    return-void
.end method

.method public onSearchRequested()Z
    .locals 2

    .prologue
    .line 105609
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->l()LX/0am;

    move-result-object v0

    .line 105610
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onSearchRequested()Z

    move-result v0

    goto :goto_0
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x3b3958f1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 105606
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 105607
    iget-object v1, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v1}, LX/0jd;->e()V

    .line 105608
    const/16 v1, 0x23

    const v2, -0x3b9d940e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x6966d942

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 105603
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 105604
    iget-object v1, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v1}, LX/0jd;->f()V

    .line 105605
    const/16 v1, 0x23

    const v2, -0xe4f7e20

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 105572
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 105573
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(Ljava/lang/CharSequence;I)V

    .line 105574
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 105598
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onTrimMemory(I)V

    .line 105599
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->d(I)V

    .line 105600
    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .prologue
    .line 105595
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onUserInteraction()V

    .line 105596
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->k()V

    .line 105597
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 105592
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onWindowFocusChanged(Z)V

    .line 105593
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Z)V

    .line 105594
    return-void
.end method

.method public setContentView(I)V
    .locals 3

    .prologue
    .line 105586
    const-string v0, "setContentView(%s)"

    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v1

    const v2, -0x3508524f    # -8115928.5f

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 105587
    :try_start_0
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105588
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->setContentView(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105589
    :cond_0
    const v0, -0x513d59a7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 105590
    return-void

    .line 105591
    :catchall_0
    move-exception v0

    const v1, 0x25320380

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 105583
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105584
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->setContentView(Landroid/view/View;)V

    .line 105585
    :cond_0
    return-void
.end method

.method public final setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 105580
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->C:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105581
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 105582
    :cond_0
    return-void
.end method

.method public setProperty(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 105578
    iget-object v0, p0, Lcom/facebook/base/activity/FbFragmentActivity;->q:LX/0jb;

    invoke-virtual {v0, p1, p2}, LX/0jb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 105579
    return-void
.end method
