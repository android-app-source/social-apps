.class public Lcom/facebook/base/fragment/FbFragment;
.super Landroid/support/v4/app/Fragment;
.source ""

# interfaces
.implements LX/0ew;
.implements LX/0f0;
.implements LX/02k;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:LX/11r;

.field private b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106541
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 106542
    invoke-virtual {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 106543
    if-eqz v0, :cond_0

    .line 106544
    :goto_0
    return-object v0

    .line 106545
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 106546
    instance-of v1, v0, LX/0f0;

    if-eqz v1, :cond_1

    .line 106547
    check-cast v0, LX/0f0;

    invoke-interface {v0, p1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 106548
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 106549
    instance-of v1, v0, LX/0f0;

    if-eqz v1, :cond_2

    .line 106550
    check-cast v0, LX/0f0;

    invoke-interface {v0, p1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 106551
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1Lb;)V
    .locals 1

    .prologue
    .line 106552
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1}, LX/11r;->a(LX/1Lb;)V

    .line 106553
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 106554
    return-void
.end method

.method public b(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 106555
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 106556
    invoke-static {v0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 106557
    invoke-virtual {p1, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106558
    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b(LX/1Lb;)V
    .locals 1

    .prologue
    .line 106559
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1}, LX/11r;->b(LX/1Lb;)V

    .line 106560
    return-void
.end method

.method public c(I)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)",
            "LX/0am",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 106561
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 106562
    invoke-static {v0, p1}, LX/0jc;->a(Landroid/view/View;I)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final dispatchGetMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 106563
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0}, LX/11r;->c()Landroid/view/MenuInflater;

    move-result-object v0

    .line 106564
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->dispatchGetMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    goto :goto_0
.end method

.method public final dispatchOnActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 106601
    const-string v0, "%s.onActivityCreated"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x3a9043a

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 106602
    :try_start_0
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1}, LX/11r;->a(Landroid/os/Bundle;)V

    .line 106603
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->dispatchOnActivityCreated(Landroid/os/Bundle;)V

    .line 106604
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1}, LX/11r;->b(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106605
    const v0, 0x4cadef57    # 9.1191992E7f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 106606
    return-void

    .line 106607
    :catchall_0
    move-exception v0

    const v1, 0x6603ad1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final dispatchOnCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 106565
    const-string v0, "%s.onCreate"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x35116704

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 106566
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->dispatchOnCreate(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106567
    const v0, 0x13ce74e6

    invoke-static {v0}, LX/02m;->a(I)V

    .line 106568
    return-void

    .line 106569
    :catchall_0
    move-exception v0

    const v1, -0x3646fd1f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final dispatchOnCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 106576
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1, p2}, LX/11r;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106577
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->dispatchOnCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 106578
    :cond_0
    return-void
.end method

.method public final dispatchOnCreateOptionsView()Landroid/view/View;
    .locals 1

    .prologue
    .line 106579
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0}, LX/11r;->b()LX/0am;

    move-result-object v0

    .line 106580
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->dispatchOnCreateOptionsView()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final dispatchOnCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 106581
    const-string v0, "%s.onCreateView"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x67319ecc

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 106582
    :try_start_0
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1, p2, p3}, LX/11r;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)LX/0am;

    move-result-object v0

    .line 106583
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 106584
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106585
    :goto_0
    const v1, -0x649f7dc0

    invoke-static {v1}, LX/02m;->a(I)V

    .line 106586
    return-object v0

    .line 106587
    :cond_0
    :try_start_1
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->dispatchOnCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 106588
    :catchall_0
    move-exception v0

    const v1, 0x345f043c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final dispatchOnDestroy()V
    .locals 3

    .prologue
    .line 106589
    const-string v0, "%s.onDestroy"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, -0x6d6ddfba

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 106590
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->dispatchOnDestroy()V

    .line 106591
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0}, LX/11r;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106592
    const v0, -0x625e55e0

    invoke-static {v0}, LX/02m;->a(I)V

    .line 106593
    return-void

    .line 106594
    :catchall_0
    move-exception v0

    const v1, -0x46ed5f86

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final dispatchOnDestroyView()V
    .locals 3

    .prologue
    .line 106595
    const-string v0, "%s.onDestroyView"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x27633d60

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 106596
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->dispatchOnDestroyView()V

    .line 106597
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0}, LX/11r;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106598
    const v0, 0x6c71ae50

    invoke-static {v0}, LX/02m;->a(I)V

    .line 106599
    return-void

    .line 106600
    :catchall_0
    move-exception v0

    const v1, 0x1c302074

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final dispatchOnInvalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 106537
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    if-eqz v0, :cond_0

    .line 106538
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0}, LX/11r;->a()V

    .line 106539
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->dispatchOnInvalidateOptionsMenu()V

    .line 106540
    return-void
.end method

.method public final dispatchOnPause()V
    .locals 3

    .prologue
    .line 106570
    const-string v0, "%s.onPause"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x51daf5df

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 106571
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->dispatchOnPause()V

    .line 106572
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0}, LX/11r;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106573
    const v0, 0xb09f07d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 106574
    return-void

    .line 106575
    :catchall_0
    move-exception v0

    const v1, -0x2d907a83

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final dispatchOnResume()V
    .locals 3

    .prologue
    .line 106464
    const-string v0, "%s.onResume"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, -0x4d341857

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 106465
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->dispatchOnResume()V

    .line 106466
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0}, LX/11r;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106467
    const v0, 0x2a81e46

    invoke-static {v0}, LX/02m;->a(I)V

    .line 106468
    return-void

    .line 106469
    :catchall_0
    move-exception v0

    const v1, 0x42545b37

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final dispatchOnStart()V
    .locals 3

    .prologue
    .line 106472
    const-string v0, "%s.onStart"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x3f56497f

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 106473
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->dispatchOnStart()V

    .line 106474
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0}, LX/11r;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106475
    const v0, -0x3dcf4ac

    invoke-static {v0}, LX/02m;->a(I)V

    .line 106476
    return-void

    .line 106477
    :catchall_0
    move-exception v0

    const v1, 0x74fc4a62

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final dispatchOnStop()V
    .locals 3

    .prologue
    .line 106478
    const-string v0, "%s.onStop"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, -0x1dc70123

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 106479
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->dispatchOnStop()V

    .line 106480
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0}, LX/11r;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106481
    const v0, -0x6f788895

    invoke-static {v0}, LX/02m;->a(I)V

    .line 106482
    return-void

    .line 106483
    :catchall_0
    move-exception v0

    const v1, -0x7d81b0aa

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public ds_()Z
    .locals 1

    .prologue
    .line 106484
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mRemoving:Z

    move v0, v0

    .line 106485
    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106486
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v0

    .line 106487
    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 106488
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->b:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 106489
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 106490
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v1, v1

    .line 106491
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 106492
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->b:Landroid/view/LayoutInflater;

    .line 106493
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->b:Landroid/view/LayoutInflater;

    return-object v0

    .line 106494
    :cond_1
    iput-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->b:Landroid/view/LayoutInflater;

    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 106495
    new-instance v0, LX/1Iu;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Iu;-><init>(Ljava/lang/Object;)V

    .line 106496
    iget-object v1, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    if-eqz v1, :cond_0

    .line 106497
    iget-object v1, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v1, v0}, LX/11r;->a(LX/1Iu;)V

    .line 106498
    :cond_0
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 106499
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final iA_()V
    .locals 0

    .prologue
    .line 106500
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->invalidateOptionsMenu()V

    .line 106501
    return-void
.end method

.method public final iC_()LX/0gc;
    .locals 1

    .prologue
    .line 106470
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 106471
    return-object v0
.end method

.method public final jG_()Landroid/app/Activity;
    .locals 2

    .prologue
    .line 106502
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method public final kX_()LX/0QA;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 106503
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 106504
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 106505
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    if-eqz v0, :cond_0

    .line 106506
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1, p2, p3}, LX/11r;->a(IILandroid/content/Intent;)V

    .line 106507
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 106508
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    if-eqz v0, :cond_0

    .line 106509
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1}, LX/11r;->a(Landroid/content/res/Configuration;)V

    .line 106510
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6d405988

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 106511
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 106512
    new-instance v1, LX/11r;

    .line 106513
    new-instance v2, LX/11s;

    invoke-direct {v2, p0}, LX/11s;-><init>(Lcom/facebook/base/fragment/FbFragment;)V

    move-object v2, v2

    .line 106514
    invoke-direct {v1, p0, v2}, LX/11r;-><init>(Landroid/support/v4/app/Fragment;LX/11s;)V

    iput-object v1, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    .line 106515
    invoke-virtual {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 106516
    const/16 v1, 0x2b

    const v2, 0x489848a7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 1

    .prologue
    .line 106517
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onHiddenChanged(Z)V

    .line 106518
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    if-eqz v0, :cond_0

    .line 106519
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1}, LX/11r;->a(Z)V

    .line 106520
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 106521
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1}, LX/11r;->a(Landroid/view/MenuItem;)LX/0am;

    move-result-object v0

    .line 106522
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 106523
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 106524
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    if-eqz v0, :cond_0

    .line 106525
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1}, LX/11r;->c(Landroid/os/Bundle;)V

    .line 106526
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 106527
    const-string v0, "%s: FbFragment.onViewCreated"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, -0x4830c025

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 106528
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 106529
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1, p2}, LX/11r;->a(Landroid/view/View;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106530
    const v0, 0x5e2eb43f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 106531
    return-void

    .line 106532
    :catchall_0
    move-exception v0

    const v1, 0x60e0181a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 106533
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 106534
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    if-eqz v0, :cond_0

    .line 106535
    iget-object v0, p0, Lcom/facebook/base/fragment/FbFragment;->a:LX/11r;

    invoke-virtual {v0, p1}, LX/11r;->b(Z)V

    .line 106536
    :cond_0
    return-void
.end method
