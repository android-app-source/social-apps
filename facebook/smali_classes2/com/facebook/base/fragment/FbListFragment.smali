.class public Lcom/facebook/base/fragment/FbListFragment;
.super Landroid/support/v4/app/ListFragment;
.source ""

# interfaces
.implements LX/0ew;
.implements LX/0f0;
.implements LX/02k;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 114005
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 113997
    invoke-virtual {p1, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113998
    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 114003
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 114004
    invoke-static {v0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 114006
    invoke-direct {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 114007
    if-eqz v0, :cond_0

    .line 114008
    :goto_0
    return-object v0

    .line 114009
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 114010
    instance-of v1, v0, LX/0f0;

    if-eqz v1, :cond_1

    .line 114011
    check-cast v0, LX/0f0;

    invoke-interface {v0, p1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 114012
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 114013
    instance-of v1, v0, LX/0f0;

    if-eqz v1, :cond_2

    .line 114014
    check-cast v0, LX/0f0;

    invoke-interface {v0, p1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 114015
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iA_()V
    .locals 0

    .prologue
    .line 114001
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->invalidateOptionsMenu()V

    .line 114002
    return-void
.end method

.method public final iC_()LX/0gc;
    .locals 1

    .prologue
    .line 113999
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 114000
    return-object v0
.end method
