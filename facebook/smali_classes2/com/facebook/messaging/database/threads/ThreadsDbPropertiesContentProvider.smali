.class public Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;
.super LX/0PK;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/0PL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56049
    const-class v0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;

    sput-object v0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56046
    invoke-direct {p0}, LX/0PK;-><init>()V

    .line 56047
    new-instance v0, LX/0PL;

    invoke-direct {v0}, LX/0PL;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->d:LX/0PL;

    .line 56048
    return-void
.end method

.method private static a(Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6dS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56045
    iput-object p1, p0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->b:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;

    const/16 v1, 0x274b

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x274d

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->a(Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;LX/0Or;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56050
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 5

    .prologue
    .line 56035
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, LX/0PK;->a()V

    .line 56036
    const-string v0, "ThreadsDbPropertiesContentProvider.onInitialize"

    const v1, 0x1429d727

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 56037
    :try_start_1
    const-class v0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;

    invoke-static {v0, p0}, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->a(Ljava/lang/Class;LX/02k;)V

    .line 56038
    iget-object v0, p0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dS;

    .line 56039
    new-instance v1, LX/0PL;

    invoke-direct {v1}, LX/0PL;-><init>()V

    iput-object v1, p0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->d:LX/0PL;

    .line 56040
    iget-object v1, p0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->d:LX/0PL;

    invoke-virtual {v0}, LX/6dS;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "properties"

    new-instance v3, LX/6dR;

    invoke-direct {v3, p0}, LX/6dR;-><init>(Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;)V

    invoke-virtual {v1, v0, v2, v3}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56041
    const v0, -0x3f4e55f3

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 56042
    monitor-exit p0

    return-void

    .line 56043
    :catchall_0
    move-exception v0

    const v1, 0x7331ec0a

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 56044
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 56028
    const-string v0, "ThreadsDbPropertiesContentProvider.doDelete"

    const v1, 0x33254330

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 56029
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->d:LX/0PL;

    invoke-virtual {v0, p1}, LX/0PL;->a(Landroid/net/Uri;)LX/2Rg;

    move-result-object v0

    .line 56030
    invoke-virtual {v0, p2, p3}, LX/2Rg;->a(Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 56031
    const v1, 0x13e2df0

    invoke-static {v1}, LX/02m;->b(I)J

    .line 56032
    return v0

    .line 56033
    :catchall_0
    move-exception v0

    const v1, -0x70097d87

    invoke-static {v1}, LX/02m;->b(I)J

    .line 56034
    throw v0
.end method

.method public final b(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 56021
    const-string v0, "ThreadsDbPropertiesContentProvider.doQuery"

    const v1, -0x42561e55

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 56022
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->d:LX/0PL;

    invoke-virtual {v0, p1}, LX/0PL;->a(Landroid/net/Uri;)LX/2Rg;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 56023
    invoke-virtual/range {v0 .. v5}, LX/2Rg;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 56024
    const v1, 0x3f9246a5    # 1.1427809f

    invoke-static {v1}, LX/02m;->b(I)J

    .line 56025
    return-object v0

    .line 56026
    :catchall_0
    move-exception v0

    const v1, -0x6cb543f3

    invoke-static {v1}, LX/02m;->b(I)J

    .line 56027
    throw v0
.end method

.method public final b(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 56014
    const-string v0, "ThreadsDbPropertiesContentProvider.doInsert"

    const v1, 0x54af5052

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 56015
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/database/threads/ThreadsDbPropertiesContentProvider;->d:LX/0PL;

    invoke-virtual {v0, p1}, LX/0PL;->a(Landroid/net/Uri;)LX/2Rg;

    move-result-object v0

    .line 56016
    invoke-virtual {v0, p2}, LX/2Rg;->a(Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 56017
    const v1, 0x526d26be

    invoke-static {v1}, LX/02m;->b(I)J

    .line 56018
    return-object v0

    .line 56019
    :catchall_0
    move-exception v0

    const v1, -0x5a05fea1

    invoke-static {v1}, LX/02m;->b(I)J

    .line 56020
    throw v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 56013
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
