.class public Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;
.super LX/0PK;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dB;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dC;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/0PL;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55871
    invoke-direct {p0}, LX/0PK;-><init>()V

    .line 55872
    return-void
.end method

.method private static a(Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;",
            "LX/0Or",
            "<",
            "LX/6dB;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6dt;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6dC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55870
    iput-object p1, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->d:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;

    const/16 v1, 0x2747

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x274b

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x274f

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2748

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p0, v1, v2, v3, v0}, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->a(Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55869
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 6

    .prologue
    .line 55845
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, LX/0PK;->a()V

    .line 55846
    const-string v0, "MessagesDbContentProvider.onInitialize"

    const v1, 0x56bcd603

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 55847
    :try_start_1
    const-class v0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;

    invoke-static {v0, p0}, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->a(Ljava/lang/Class;LX/02k;)V

    .line 55848
    iget-object v0, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dB;

    .line 55849
    new-instance v1, LX/0PL;

    invoke-direct {v1}, LX/0PL;-><init>()V

    iput-object v1, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->f:LX/0PL;

    .line 55850
    iget-object v1, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->f:LX/0PL;

    iget-object v2, v0, LX/6dB;->a:Ljava/lang/String;

    const-string v3, "thread_summaries"

    new-instance v4, LX/6d7;

    iget-object v5, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->c:LX/0Or;

    invoke-direct {v4, v5}, LX/6d7;-><init>(LX/0Or;)V

    invoke-virtual {v1, v2, v3, v4}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V

    .line 55851
    iget-object v1, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->f:LX/0PL;

    iget-object v0, v0, LX/6dB;->a:Ljava/lang/String;

    const-string v2, "messages"

    new-instance v3, LX/6d7;

    iget-object v4, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->d:LX/0Or;

    invoke-direct {v3, v4}, LX/6d7;-><init>(LX/0Or;)V

    invoke-virtual {v1, v0, v2, v3}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55852
    const v0, -0x426285d4

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 55853
    monitor-exit p0

    return-void

    .line 55854
    :catchall_0
    move-exception v0

    const v1, -0x5a1506f5

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 55855
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 55865
    iget-object v0, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dB;

    iget-object v0, v0, LX/6dB;->e:LX/6d8;

    iget-object v0, v0, LX/6d8;->a:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55866
    iget-object v0, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 55867
    const/4 v0, 0x0

    return v0

    .line 55868
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 55858
    const-string v0, "MessagesDbContentProvider.doQuery"

    const v1, 0x5e208fc3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 55859
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/database/threads/MessagesDbContentProvider;->f:LX/0PL;

    invoke-virtual {v0, p1}, LX/0PL;->a(Landroid/net/Uri;)LX/2Rg;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 55860
    invoke-virtual/range {v0 .. v5}, LX/2Rg;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 55861
    const v1, -0x717ce9eb

    invoke-static {v1}, LX/02m;->b(I)J

    .line 55862
    return-object v0

    .line 55863
    :catchall_0
    move-exception v0

    const v1, 0x4f97adcc

    invoke-static {v1}, LX/02m;->b(I)J

    .line 55864
    throw v0
.end method

.method public final b(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 55857
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 55856
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
