.class public Lcom/facebook/apptab/ui/DownloadableImageTabView;
.super LX/0hO;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final i:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:I

.field public n:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 181567
    const-class v0, Lcom/facebook/apptab/ui/DownloadableImageTabView;

    const-string v1, "friend_notification_tab"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 181563
    invoke-direct {p0, p1}, LX/0hO;-><init>(Landroid/content/Context;)V

    .line 181564
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->k:Z

    .line 181565
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;

    const/16 v0, 0x509

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->j:LX/0Or;

    .line 181566
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x55f8f47e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 181559
    invoke-super {p0}, LX/0hO;->onAttachedToWindow()V

    .line 181560
    iget-object v1, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    if-eqz v1, :cond_0

    .line 181561
    iget-object v1, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 181562
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x7207bd24

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x422b2b88

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 181555
    invoke-super {p0}, LX/0hO;->onDetachedFromWindow()V

    .line 181556
    iget-object v1, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    if-eqz v1, :cond_0

    .line 181557
    iget-object v1, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 181558
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x52550687

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 181551
    invoke-super {p0}, LX/0hO;->onFinishTemporaryDetach()V

    .line 181552
    iget-object v0, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    if-eqz v0, :cond_0

    .line 181553
    iget-object v0, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 181554
    :cond_0
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 181547
    invoke-super {p0}, LX/0hO;->onStartTemporaryDetach()V

    .line 181548
    iget-object v0, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    if-eqz v0, :cond_0

    .line 181549
    iget-object v0, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->n:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 181550
    :cond_0
    return-void
.end method

.method public setGlyphImage(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 181537
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->k:Z

    .line 181538
    invoke-super {p0, p1}, LX/0hO;->setGlyphImage(Landroid/graphics/drawable/Drawable;)V

    .line 181539
    return-void
.end method

.method public setGlyphImageWithoutChangeColorSize(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 181544
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->k:Z

    .line 181545
    invoke-super {p0, p1}, LX/0hO;->setGlyphImageWithoutChangeSize(Landroid/graphics/drawable/Drawable;)V

    .line 181546
    return-void
.end method

.method public setTabIconImageResource(I)V
    .locals 1

    .prologue
    .line 181540
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->k:Z

    .line 181541
    iput p1, p0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->m:I

    .line 181542
    invoke-super {p0, p1}, LX/0hO;->setTabIconImageResource(I)V

    .line 181543
    return-void
.end method
