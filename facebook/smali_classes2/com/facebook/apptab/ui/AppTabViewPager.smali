.class public Lcom/facebook/apptab/ui/AppTabViewPager;
.super Lcom/facebook/widget/CustomViewPager;
.source ""

# interfaces
.implements LX/0vZ;


# instance fields
.field public a:LX/0gp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0hJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 158014
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewPager;-><init>(Landroid/content/Context;)V

    .line 158015
    invoke-direct {p0}, Lcom/facebook/apptab/ui/AppTabViewPager;->g()V

    .line 158016
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 158011
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 158012
    invoke-direct {p0}, Lcom/facebook/apptab/ui/AppTabViewPager;->g()V

    .line 158013
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/apptab/ui/AppTabViewPager;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/apptab/ui/AppTabViewPager;

    invoke-static {v1}, LX/0hJ;->a(LX/0QB;)LX/0hJ;

    move-result-object v0

    check-cast v0, LX/0hJ;

    iput-object v0, p0, Lcom/facebook/apptab/ui/AppTabViewPager;->b:LX/0hJ;

    invoke-static {v1}, LX/0gp;->a(LX/0QB;)LX/0gp;

    move-result-object v0

    check-cast v0, LX/0gp;

    iput-object v0, p0, Lcom/facebook/apptab/ui/AppTabViewPager;->a:LX/0gp;

    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 158009
    const-class v0, Lcom/facebook/apptab/ui/AppTabViewPager;

    invoke-static {v0, p0}, Lcom/facebook/apptab/ui/AppTabViewPager;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 158010
    return-void
.end method


# virtual methods
.method public final a(IFI)V
    .locals 1

    .prologue
    .line 158006
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewPager;->a(IFI)V

    .line 158007
    iget-object v0, p0, Lcom/facebook/apptab/ui/AppTabViewPager;->a:LX/0gp;

    invoke-virtual {v0}, LX/0gp;->c()V

    .line 158008
    return-void
.end method

.method public final a(FF)Z
    .locals 1

    .prologue
    .line 158005
    const/4 v0, 0x0

    return v0
.end method

.method public final b(IZ)V
    .locals 2

    .prologue
    .line 158004
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Since this class dynamically adjusts to the height of the title bar, manually setting the height is not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    .line 157966
    iget-object v0, p0, Lcom/facebook/apptab/ui/AppTabViewPager;->a:LX/0gp;

    invoke-virtual {v0}, LX/0gp;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157967
    iget-object v0, p0, Lcom/facebook/apptab/ui/AppTabViewPager;->b:LX/0hJ;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/high16 v6, -0x80000000

    .line 157968
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 157969
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 157970
    if-ne v1, v6, :cond_1

    move v1, v2

    :goto_0
    const-string v5, "Parent is not laying us out with wrap_content so we can\'t infer the correct height to request without drawing over the title bar!"

    invoke-static {v1, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 157971
    iget-object v1, v0, LX/0hJ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 157972
    if-eqz v1, :cond_2

    :goto_1
    const-string v3, "Header is now null.  Was it garbage collected?"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 157973
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 157974
    :goto_2
    move p2, p2

    .line 157975
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomViewPager;->onMeasure(II)V

    .line 157976
    return-void

    :cond_1
    move v1, v3

    .line 157977
    goto :goto_0

    :cond_2
    move v2, v3

    .line 157978
    goto :goto_1

    .line 157979
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v4, v1

    .line 157980
    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    goto :goto_2
.end method

.method public final onNestedFling(Landroid/view/View;FFZ)Z
    .locals 1

    .prologue
    .line 157999
    if-eqz p4, :cond_0

    .line 158000
    iget-object v0, p0, Lcom/facebook/apptab/ui/AppTabViewPager;->a:LX/0gp;

    .line 158001
    const/4 p0, 0x0

    cmpl-float p0, p3, p0

    if-lez p0, :cond_0

    iget-boolean p0, v0, LX/0gp;->h:Z

    if-eqz p0, :cond_0

    .line 158002
    invoke-static {v0}, LX/0gp;->g(LX/0gp;)V

    .line 158003
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final onNestedPreFling(Landroid/view/View;FF)Z
    .locals 1

    .prologue
    .line 157998
    const/4 v0, 0x0

    return v0
.end method

.method public final onNestedPreScroll(Landroid/view/View;II[I)V
    .locals 1

    .prologue
    .line 157996
    iget-object v0, p0, Lcom/facebook/apptab/ui/AppTabViewPager;->a:LX/0gp;

    invoke-virtual {v0, p3}, LX/0gp;->a(I)V

    .line 157997
    return-void
.end method

.method public final onNestedScroll(Landroid/view/View;IIII)V
    .locals 1

    .prologue
    .line 157993
    if-eqz p3, :cond_0

    .line 157994
    iget-object v0, p0, Lcom/facebook/apptab/ui/AppTabViewPager;->a:LX/0gp;

    invoke-virtual {v0, p3}, LX/0gp;->a(I)V

    .line 157995
    :cond_0
    return-void
.end method

.method public final onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 157992
    return-void
.end method

.method public final onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 157982
    iget-object v0, p0, Lcom/facebook/apptab/ui/AppTabViewPager;->a:LX/0gp;

    const/4 p1, 0x0

    .line 157983
    const/4 p0, 0x2

    if-eq p3, p0, :cond_0

    move p0, p1

    .line 157984
    :goto_0
    move v0, p0

    .line 157985
    return v0

    .line 157986
    :cond_0
    iget-object p0, v0, LX/0gp;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/support/v4/app/Fragment;

    .line 157987
    if-eqz p0, :cond_1

    iget-object p0, v0, LX/0gp;->c:LX/0nx;

    if-nez p0, :cond_2

    :cond_1
    move p0, p1

    .line 157988
    goto :goto_0

    .line 157989
    :cond_2
    iget-object p0, v0, LX/0gp;->c:LX/0nx;

    .line 157990
    iget-object v0, p0, LX/0nx;->a:Landroid/view/View;

    move-object p0, v0

    .line 157991
    if-ne p0, p2, :cond_3

    const/4 p0, 0x1

    goto :goto_0

    :cond_3
    move p0, p1

    goto :goto_0
.end method

.method public final onStopNestedScroll(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 157981
    return-void
.end method
