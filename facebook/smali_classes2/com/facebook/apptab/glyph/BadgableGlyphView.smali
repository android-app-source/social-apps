.class public Lcom/facebook/apptab/glyph/BadgableGlyphView;
.super Landroid/view/View;
.source ""


# instance fields
.field public final a:Landroid/graphics/Rect;

.field public b:Landroid/graphics/drawable/Drawable;

.field public c:I

.field public d:Z

.field public e:LX/0xk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:[I

.field private final m:Landroid/graphics/Rect;

.field private final n:Landroid/graphics/Paint;

.field private final o:F

.field private final p:LX/0xi;

.field private final q:LX/0xi;

.field private r:I

.field private s:I

.field private t:Landroid/graphics/drawable/GradientDrawable;

.field public u:F

.field private v:F

.field private w:Ljava/lang/String;

.field private x:LX/0wd;

.field private y:LX/0wd;

.field private z:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 116052
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116053
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 116050
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 116051
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 116028
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 116029
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a:Landroid/graphics/Rect;

    .line 116030
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    .line 116031
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->m:Landroid/graphics/Rect;

    .line 116032
    new-instance v0, LX/0xg;

    invoke-direct {v0, p0}, LX/0xg;-><init>(Lcom/facebook/apptab/glyph/BadgableGlyphView;)V

    iput-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->p:LX/0xi;

    .line 116033
    new-instance v0, LX/0xj;

    invoke-direct {v0, p0}, LX/0xj;-><init>(Lcom/facebook/apptab/glyph/BadgableGlyphView;)V

    iput-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->q:LX/0xi;

    .line 116034
    const-class v0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    invoke-static {v0, p0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 116035
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 116036
    const v0, 0x7f0b040b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->h:I

    .line 116037
    const v0, 0x7f0b040c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->i:I

    .line 116038
    const v0, 0x7f0b040d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->j:I

    .line 116039
    const v0, 0x7f0b040f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->k:I

    .line 116040
    const v0, 0x7f0218eb

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    iput-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->t:Landroid/graphics/drawable/GradientDrawable;

    .line 116041
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    .line 116042
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    const v2, 0x7f0b040e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 116043
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 116044
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v2, LX/0xr;->BOLD:LX/0xr;

    iget-object v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {p1, v1, v2, v3}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 116045
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 116046
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 116047
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v0

    iget-object v1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    sub-float/2addr v0, v1

    .line 116048
    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->o:F

    .line 116049
    return-void
.end method

.method private static a(Lcom/facebook/apptab/glyph/BadgableGlyphView;LX/0xk;LX/0wM;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/apptab/glyph/BadgableGlyphView;",
            "LX/0xk;",
            "LX/0wM;",
            "LX/0Ot",
            "<",
            "LX/0wW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116027
    iput-object p1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->e:LX/0xk;

    iput-object p2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->f:LX/0wM;

    iput-object p3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->g:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;

    invoke-static {v2}, LX/0xk;->a(LX/0QB;)LX/0xk;

    move-result-object v0

    check-cast v0, LX/0xk;

    invoke-static {v2}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    const/16 v3, 0x11fa

    invoke-static {v2, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a(Lcom/facebook/apptab/glyph/BadgableGlyphView;LX/0xk;LX/0wM;LX/0Ot;)V

    return-void
.end method

.method private declared-synchronized getBadgeSpring()LX/0wd;
    .locals 7

    .prologue
    .line 116014
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->y:LX/0wd;

    if-eqz v0, :cond_0

    .line 116015
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->y:LX/0wd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116016
    :goto_0
    monitor-exit p0

    return-object v0

    .line 116017
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x402a000000000000L    # 13.0

    const-wide/high16 v4, 0x402e000000000000L    # 15.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    const-wide v2, 0x3f847ae147ae147bL    # 0.01

    .line 116018
    iput-wide v2, v0, LX/0wd;->l:D

    .line 116019
    move-object v0, v0

    .line 116020
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 116021
    iput-wide v2, v0, LX/0wd;->k:D

    .line 116022
    move-object v0, v0

    .line 116023
    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->y:LX/0wd;

    .line 116024
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->y:LX/0wd;

    iget-object v1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->p:LX/0xi;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 116025
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->y:LX/0wd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116026
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getSelectionSpring()LX/0wd;
    .locals 7

    .prologue
    .line 115998
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->x:LX/0wd;

    if-eqz v0, :cond_0

    .line 115999
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->x:LX/0wd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116000
    :goto_0
    monitor-exit p0

    return-object v0

    .line 116001
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    const-wide v2, 0x3f847ae147ae147bL    # 0.01

    .line 116002
    iput-wide v2, v0, LX/0wd;->l:D

    .line 116003
    move-object v0, v0

    .line 116004
    const-wide v2, 0x3fc999999999999aL    # 0.2

    .line 116005
    iput-wide v2, v0, LX/0wd;->k:D

    .line 116006
    move-object v0, v0

    .line 116007
    const/4 v1, 0x1

    .line 116008
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 116009
    move-object v0, v0

    .line 116010
    iput-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->x:LX/0wd;

    .line 116011
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->x:LX/0wd;

    iget-object v1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->q:LX/0xi;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 116012
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->x:LX/0wd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116013
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 115995
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->f:LX/0wM;

    invoke-virtual {v0, p2}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 115996
    invoke-virtual {p0, p1}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setGlyphImage(Landroid/graphics/drawable/Drawable;)V

    .line 115997
    return-void
.end method

.method public getUnreadCount()I
    .locals 1

    .prologue
    .line 115994
    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 115990
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 115991
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    .line 115992
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->invalidate(Landroid/graphics/Rect;)V

    .line 115993
    :cond_0
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/high16 v2, 0x42200000    # 40.0f

    const/4 v5, 0x0

    .line 115977
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 115978
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 115979
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 115980
    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->u:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->w:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115981
    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->u:F

    mul-float/2addr v0, v2

    sub-float v0, v2, v0

    .line 115982
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 115983
    iget v1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->u:F

    iget v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->u:F

    iget-object v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    aget v3, v3, v5

    int-to-float v3, v3

    iget-object v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    aget v4, v4, v6

    int-to-float v4, v4

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 115984
    iget-object v1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    aget v1, v1, v5

    int-to-float v1, v1

    iget-object v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    aget v2, v2, v6

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 115985
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->t:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->m:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 115986
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->t:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 115987
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->w:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    aget v1, v1, v5

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->v:F

    iget-object v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 115988
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 115989
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 115910
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 115911
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 115912
    div-int/lit8 v2, v0, 0x2

    .line 115913
    div-int/lit8 v3, v1, 0x2

    .line 115914
    iget-object v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a:Landroid/graphics/Rect;

    iget v5, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->r:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v2, v5

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 115915
    iget-object v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a:Landroid/graphics/Rect;

    iget v5, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->r:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v2, v5

    iput v2, v4, Landroid/graphics/Rect;->right:I

    .line 115916
    iget-object v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a:Landroid/graphics/Rect;

    iget v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->s:I

    div-int/lit8 v4, v4, 0x2

    sub-int v4, v3, v4

    iput v4, v2, Landroid/graphics/Rect;->top:I

    .line 115917
    iget-object v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a:Landroid/graphics/Rect;

    iget v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->s:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 115918
    iget v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    if-lez v2, :cond_0

    .line 115919
    iget-object v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->e:LX/0xk;

    iget v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    iget-boolean v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->d:Z

    .line 115920
    if-eqz v4, :cond_1

    const/16 v5, 0x9

    :goto_0
    if-le v3, v5, :cond_3

    if-eqz v4, :cond_2

    iget-object v5, v2, LX/0xk;->b:Ljava/lang/String;

    :goto_1
    move-object v2, v5

    .line 115921
    iput-object v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->w:Ljava/lang/String;

    .line 115922
    iget v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->j:I

    mul-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->n:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->w:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    float-to-int v3, v3

    iget v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->k:I

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 115923
    iget-object v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    iget-object v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget v5, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->h:I

    add-int/2addr v4, v5

    aput v4, v3, v7

    .line 115924
    iget-object v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    iget-object v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->i:I

    add-int/2addr v4, v5

    aput v4, v3, v6

    .line 115925
    iget-object v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->m:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    aget v4, v4, v7

    sub-int/2addr v4, v2

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 115926
    iget-object v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->m:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    aget v4, v4, v6

    iget v5, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->j:I

    sub-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 115927
    iget-object v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->m:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    aget v4, v4, v7

    add-int/2addr v2, v4

    iput v2, v3, Landroid/graphics/Rect;->right:I

    .line 115928
    iget-object v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->m:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    aget v3, v3, v6

    iget v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->j:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 115929
    iget-object v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->l:[I

    aget v2, v2, v6

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->o:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->v:F

    .line 115930
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setMeasuredDimension(II)V

    .line 115931
    return-void

    :cond_1
    const/16 v5, 0x63

    goto :goto_0

    :cond_2
    iget-object v5, v2, LX/0xk;->c:Ljava/lang/String;

    goto :goto_1

    :cond_3
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method public setBadgeColor(I)V
    .locals 1

    .prologue
    .line 115932
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->t:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 115933
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->t:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 115934
    return-void
.end method

.method public setBadgeOutlineColor(I)V
    .locals 4

    .prologue
    .line 115935
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->z:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->z:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 115936
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->z:Ljava/lang/Integer;

    .line 115937
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->t:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 115938
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->t:Landroid/graphics/drawable/GradientDrawable;

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1, p1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 115939
    :cond_1
    return-void
.end method

.method public setGlyphImage(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 115940
    iput-object p1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    .line 115941
    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->r:I

    .line 115942
    iget v1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->s:I

    .line 115943
    iget-object v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iput v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->r:I

    .line 115944
    iget-object v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    iput v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->s:I

    .line 115945
    iget v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->r:I

    if-ne v2, v0, :cond_0

    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->s:I

    if-eq v0, v1, :cond_1

    .line 115946
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->requestLayout()V

    .line 115947
    :cond_1
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 115948
    return-void
.end method

.method public setGlyphImageWithoutChangeSize(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 115949
    iput-object p1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    .line 115950
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 115951
    return-void
.end method

.method public setSelected(Z)V
    .locals 3

    .prologue
    .line 115952
    invoke-direct {p0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->getSelectionSpring()LX/0wd;

    move-result-object v2

    if-eqz p1, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    invoke-virtual {v2, v0, v1}, LX/0wd;->b(D)LX/0wd;

    .line 115953
    return-void

    .line 115954
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public setSelectionPercentage(F)V
    .locals 4

    .prologue
    .line 115955
    invoke-direct {p0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->getSelectionSpring()LX/0wd;

    move-result-object v0

    float-to-double v2, p1

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    float-to-double v2, p1

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 115956
    return-void
.end method

.method public setTabIconImageResource(I)V
    .locals 1

    .prologue
    .line 115957
    invoke-virtual {p0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setGlyphImage(Landroid/graphics/drawable/Drawable;)V

    .line 115958
    return-void
.end method

.method public setUnreadCount(I)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 115959
    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    if-ne v0, p1, :cond_0

    .line 115960
    :goto_0
    return-void

    .line 115961
    :cond_0
    if-lez p1, :cond_2

    .line 115962
    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    if-gtz v0, :cond_1

    .line 115963
    invoke-direct {p0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->getBadgeSpring()LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const/4 v1, 0x0

    .line 115964
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 115965
    move-object v0, v0

    .line 115966
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 115967
    :goto_1
    iput p1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    .line 115968
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->requestLayout()V

    goto :goto_0

    .line 115969
    :cond_1
    invoke-direct {p0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->getBadgeSpring()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    goto :goto_1

    .line 115970
    :cond_2
    invoke-direct {p0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->getBadgeSpring()LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 115971
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 115972
    move-object v0, v0

    .line 115973
    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_2
.end method

.method public setUseSmallUnreadCountCap(Z)V
    .locals 0

    .prologue
    .line 115974
    iput-boolean p1, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->d:Z

    .line 115975
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 115976
    iget-object v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->b:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
