.class public Lcom/facebook/apptab/state/NavigationConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/apptab/state/NavigationConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/apptab/state/TabTag;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163362
    new-instance v0, LX/0xf;

    invoke-direct {v0}, LX/0xf;-><init>()V

    sput-object v0, Lcom/facebook/apptab/state/NavigationConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 163363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163364
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 163365
    const-class v1, Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 163366
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    .line 163367
    return-void
.end method

.method public constructor <init>(ZLX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/apptab/state/TabTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163369
    iput-boolean p1, p0, Lcom/facebook/apptab/state/NavigationConfig;->b:Z

    .line 163370
    iget-boolean v0, p0, Lcom/facebook/apptab/state/NavigationConfig;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/0Px;->reverse()LX/0Px;

    move-result-object p2

    :cond_0
    iput-object p2, p0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    .line 163371
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 163372
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 163373
    instance-of v0, p1, Lcom/facebook/apptab/state/NavigationConfig;

    if-nez v0, :cond_0

    .line 163374
    const/4 v0, 0x0

    .line 163375
    :goto_0
    return v0

    .line 163376
    :cond_0
    check-cast p1, Lcom/facebook/apptab/state/NavigationConfig;

    .line 163377
    iget-object v0, p0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    iget-object v1, p1, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 163378
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 163379
    iget-object v0, p0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 163380
    return-void
.end method
