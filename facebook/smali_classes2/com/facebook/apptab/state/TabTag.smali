.class public abstract Lcom/facebook/apptab/state/TabTag;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0cQ;

.field public final c:I

.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:I

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:I

.field public final k:I


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p2    # LX/0cQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 85889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85890
    iput-object p1, p0, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    .line 85891
    iput-object p2, p0, Lcom/facebook/apptab/state/TabTag;->b:LX/0cQ;

    .line 85892
    iput p3, p0, Lcom/facebook/apptab/state/TabTag;->c:I

    .line 85893
    iput-boolean p4, p0, Lcom/facebook/apptab/state/TabTag;->d:Z

    .line 85894
    iput-object p5, p0, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    .line 85895
    iput p6, p0, Lcom/facebook/apptab/state/TabTag;->f:I

    .line 85896
    iput p7, p0, Lcom/facebook/apptab/state/TabTag;->g:I

    .line 85897
    iput-object p8, p0, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    .line 85898
    iput-object p9, p0, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    .line 85899
    iput p10, p0, Lcom/facebook/apptab/state/TabTag;->j:I

    .line 85900
    iput p11, p0, Lcom/facebook/apptab/state/TabTag;->k:I

    .line 85901
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 85888
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 85887
    return-void
.end method
