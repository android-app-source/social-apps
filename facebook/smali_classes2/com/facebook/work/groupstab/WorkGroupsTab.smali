.class public Lcom/facebook/work/groupstab/WorkGroupsTab;
.super Lcom/facebook/apptab/state/TabTag;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/work/groupstab/WorkGroupsTab;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/facebook/work/groupstab/WorkGroupsTab;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114209
    new-instance v0, Lcom/facebook/work/groupstab/WorkGroupsTab;

    invoke-direct {v0}, Lcom/facebook/work/groupstab/WorkGroupsTab;-><init>()V

    sput-object v0, Lcom/facebook/work/groupstab/WorkGroupsTab;->l:Lcom/facebook/work/groupstab/WorkGroupsTab;

    .line 114210
    new-instance v0, LX/HfL;

    invoke-direct {v0}, LX/HfL;-><init>()V

    sput-object v0, Lcom/facebook/work/groupstab/WorkGroupsTab;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const v6, 0x63000e

    .line 114211
    sget-object v1, LX/0ax;->ad:Ljava/lang/String;

    sget-object v2, LX/0cQ;->WORK_GROUPS_TAB:LX/0cQ;

    const v3, 0x7f021075

    const/4 v4, 0x0

    const-string v5, "work_groups_tab"

    const v10, 0x7f080a2a

    const v11, 0x7f0d003f

    move-object v0, p0

    move v7, v6

    move-object v9, v8

    invoke-direct/range {v0 .. v11}, Lcom/facebook/apptab/state/TabTag;-><init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V

    .line 114212
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114213
    const-string v0, "WorkGroups"

    return-object v0
.end method
