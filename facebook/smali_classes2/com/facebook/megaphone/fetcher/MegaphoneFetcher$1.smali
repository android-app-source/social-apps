.class public final Lcom/facebook/megaphone/fetcher/MegaphoneFetcher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/3TD;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

.field public final synthetic b:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic c:LX/18S;


# direct methods
.method public constructor <init>(LX/18S;Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 206947
    iput-object p1, p0, Lcom/facebook/megaphone/fetcher/MegaphoneFetcher$1;->c:LX/18S;

    iput-object p2, p0, Lcom/facebook/megaphone/fetcher/MegaphoneFetcher$1;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    iput-object p3, p0, Lcom/facebook/megaphone/fetcher/MegaphoneFetcher$1;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 206948
    iget-object v0, p0, Lcom/facebook/megaphone/fetcher/MegaphoneFetcher$1;->c:LX/18S;

    iget-object v1, p0, Lcom/facebook/megaphone/fetcher/MegaphoneFetcher$1;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    iget-object v2, p0, Lcom/facebook/megaphone/fetcher/MegaphoneFetcher$1;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 206949
    new-instance v3, Lcom/facebook/megaphone/api/FetchMegaphoneParams;

    iget v4, v0, LX/18S;->a:I

    iget v5, v0, LX/18S;->b:I

    invoke-direct {v3, v1, v4, v5}, Lcom/facebook/megaphone/api/FetchMegaphoneParams;-><init>(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;II)V

    .line 206950
    iget-object v4, v0, LX/18S;->f:LX/18V;

    invoke-virtual {v4}, LX/18V;->a()LX/2VK;

    move-result-object v4

    .line 206951
    iget-object v5, v0, LX/18S;->d:LX/18T;

    invoke-static {v5, v3}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v5

    const-string p0, "fetch_megaphone"

    .line 206952
    iput-object p0, v5, LX/2Vk;->c:Ljava/lang/String;

    .line 206953
    move-object v5, v5

    .line 206954
    invoke-virtual {v5}, LX/2Vk;->a()LX/2Vj;

    move-result-object v5

    invoke-interface {v4, v5}, LX/2VK;->a(LX/2Vj;)V

    .line 206955
    iget-object v5, v0, LX/18S;->e:LX/18U;

    invoke-static {v5, v3}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v3

    const-string v5, "fetch_megaphone_layout"

    .line 206956
    iput-object v5, v3, LX/2Vk;->c:Ljava/lang/String;

    .line 206957
    move-object v3, v3

    .line 206958
    invoke-virtual {v3}, LX/2Vk;->a()LX/2Vj;

    move-result-object v3

    invoke-interface {v4, v3}, LX/2VK;->a(LX/2Vj;)V

    .line 206959
    new-instance v3, LX/14U;

    invoke-direct {v3}, LX/14U;-><init>()V

    .line 206960
    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 206961
    iput-object v5, v3, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 206962
    const-string v5, "fetchLayoutAndMegaphoneRequest"

    if-eqz v2, :cond_0

    :goto_0
    invoke-interface {v4, v5, v2, v3}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/14U;)V

    .line 206963
    const-string v3, "fetch_megaphone"

    invoke-interface {v4, v3}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/megaphone/api/FetchMegaphoneResult;

    .line 206964
    const-string v5, "fetch_megaphone_layout"

    invoke-interface {v4, v5}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/megaphone/api/FetchMegaphoneLayoutResult;

    .line 206965
    iget-object v5, v3, Lcom/facebook/megaphone/api/FetchMegaphoneResult;->a:Lcom/facebook/graphql/model/GraphQLMegaphone;

    if-eqz v5, :cond_1

    iget-object v5, v3, Lcom/facebook/megaphone/api/FetchMegaphoneResult;->a:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMegaphone;->l()Ljava/lang/String;

    move-result-object v5

    iget-object p0, v4, Lcom/facebook/megaphone/api/FetchMegaphoneLayoutResult;->a:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 206966
    new-instance v4, LX/3TD;

    const/4 v5, 0x0

    iget-object v3, v3, Lcom/facebook/megaphone/api/FetchMegaphoneResult;->a:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-direct {v4, v5, v3}, LX/3TD;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLMegaphone;)V

    move-object v3, v4

    .line 206967
    :goto_1
    move-object v0, v3

    .line 206968
    return-object v0

    .line 206969
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    goto :goto_0

    .line 206970
    :cond_1
    new-instance v5, LX/3TD;

    iget-object v4, v4, Lcom/facebook/megaphone/api/FetchMegaphoneLayoutResult;->b:Ljava/lang/String;

    iget-object v3, v3, Lcom/facebook/megaphone/api/FetchMegaphoneResult;->a:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-direct {v5, v4, v3}, LX/3TD;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLMegaphone;)V

    move-object v3, v5

    goto :goto_1
.end method
