.class public final Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/15K;


# direct methods
.method public constructor <init>(LX/15K;)V
    .locals 0

    .prologue
    .line 180473
    iput-object p1, p0, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;->a:LX/15K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 180474
    iget-object v0, p0, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;->a:LX/15K;

    iget-object v0, v0, LX/15K;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;->a:LX/15K;

    iget-object v0, v0, LX/15K;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v2, v0

    .line 180475
    :goto_0
    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;->a:LX/15K;

    invoke-static {v0}, LX/15K;->b$redex0(LX/15K;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 180476
    iget-object v0, p0, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;->a:LX/15K;

    .line 180477
    invoke-static {v0}, LX/15K;->d(LX/15K;)Lorg/json/JSONObject;

    move-result-object v1

    .line 180478
    :try_start_0
    invoke-static {v0, v2}, LX/15K;->b(LX/15K;Landroid/view/View;)Lorg/json/JSONObject;

    move-result-object v3

    .line 180479
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 180480
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 180481
    const-string v3, "children"

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180482
    :goto_1
    move-object v3, v1

    .line 180483
    iget-object v0, p0, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;->a:LX/15K;

    iget-object v0, v0, LX/15K;->b:LX/0kx;

    invoke-virtual {v0}, LX/0kx;->c()LX/148;

    move-result-object v4

    .line 180484
    const-string v1, "null"

    .line 180485
    const-string v0, "none"

    .line 180486
    if-eqz v4, :cond_1

    .line 180487
    iget-object v0, v4, LX/148;->b:Ljava/lang/String;

    move-object v1, v0

    .line 180488
    iget-object v0, v4, LX/148;->a:Ljava/lang/String;

    move-object v0, v0

    .line 180489
    if-eqz v0, :cond_0

    const-string v4, "unknown"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 180490
    :cond_0
    const-string v0, "none"

    .line 180491
    :cond_1
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "ui_metrics_views_hierarchy"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 180492
    const-string v5, "ui_metrics"

    .line 180493
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 180494
    const-string v5, "views"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 180495
    const-string v5, "module"

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 180496
    const-string v0, "screen"

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 180497
    const-string v0, "os_type"

    const-string v1, "android"

    invoke-virtual {v4, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 180498
    const-string v1, "analytics_group"

    iget-object v0, p0, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;->a:LX/15K;

    .line 180499
    iget-object v5, v0, LX/15K;->c:LX/0Uh;

    const/16 v6, 0x65e

    invoke-virtual {v5, v6}, LX/0Uh;->a(I)LX/03R;

    move-result-object v5

    sget-object v6, LX/03R;->YES:LX/03R;

    if-ne v5, v6, :cond_5

    const/4 v5, 0x1

    :goto_2
    move v0, v5

    .line 180500
    if-eqz v0, :cond_4

    const-string v0, "ui_metrics_analytics_fig_war_room"

    :goto_3
    invoke-virtual {v4, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 180501
    iget-object v0, p0, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;->a:LX/15K;

    iget-object v0, v0, LX/15K;->a:LX/0Zb;

    invoke-interface {v0, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 180502
    iget-object v0, p0, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;->a:LX/15K;

    invoke-static {v0, v3}, LX/15K;->a$redex0(LX/15K;Lorg/json/JSONObject;)V

    .line 180503
    const-wide/16 v0, 0x1388

    invoke-virtual {v2, p0, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 180504
    :cond_2
    return-void

    .line 180505
    :cond_3
    const/4 v0, 0x0

    move-object v2, v0

    goto/16 :goto_0

    .line 180506
    :cond_4
    const-string v0, "none"

    goto :goto_3

    :catch_0
    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    goto :goto_2
.end method
