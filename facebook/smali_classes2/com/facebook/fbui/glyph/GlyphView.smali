.class public Lcom/facebook/fbui/glyph/GlyphView;
.super Lcom/facebook/widget/FbImageView;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 159685
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 159686
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 159720
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 159721
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 159722
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/FbImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 159723
    const-class v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-static {v0, p0}, Lcom/facebook/fbui/glyph/GlyphView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 159724
    sget-object v0, LX/03r;->GlyphColorizer:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 159725
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 159726
    if-lez v1, :cond_1

    .line 159727
    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 159728
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "XML should not specify both android:src and fb:source. Please only use fb:source"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159729
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 159730
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 159731
    :cond_1
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 159732
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 159733
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 159734
    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 159735
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/fbui/glyph/GlyphView;->a:LX/0wM;

    return-void
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 3

    .prologue
    .line 159713
    invoke-super {p0}, Lcom/facebook/widget/FbImageView;->drawableStateChanged()V

    .line 159714
    iget-object v0, p0, Lcom/facebook/fbui/glyph/GlyphView;->b:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    .line 159715
    iget-object v0, p0, Lcom/facebook/fbui/glyph/GlyphView;->b:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphView;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 159716
    iget-object v1, p0, Lcom/facebook/fbui/glyph/GlyphView;->a:LX/0wM;

    invoke-virtual {v1, v0}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 159717
    :goto_0
    return-void

    .line 159718
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public getGlyphColor()Landroid/content/res/ColorStateList;
    .locals 1

    .prologue
    .line 159719
    iget-object v0, p0, Lcom/facebook/fbui/glyph/GlyphView;->b:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x33b8883c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 159708
    invoke-super {p0}, Lcom/facebook/widget/FbImageView;->onAttachedToWindow()V

    .line 159709
    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 159710
    if-eqz v1, :cond_0

    .line 159711
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 159712
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x2522085f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2c

    const v1, 0x54a0ee07

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 159703
    invoke-super {p0}, Lcom/facebook/widget/FbImageView;->onDetachedFromWindow()V

    .line 159704
    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 159705
    if-eqz v1, :cond_0

    .line 159706
    invoke-virtual {v1, v2, v2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 159707
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x93a7134

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 3

    .prologue
    .line 159698
    invoke-super {p0}, Lcom/facebook/widget/FbImageView;->onFinishTemporaryDetach()V

    .line 159699
    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 159700
    if-eqz v0, :cond_0

    .line 159701
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 159702
    :cond_0
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 159693
    invoke-super {p0}, Lcom/facebook/widget/FbImageView;->onStartTemporaryDetach()V

    .line 159694
    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 159695
    if-eqz v0, :cond_0

    .line 159696
    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 159697
    :cond_0
    return-void
.end method

.method public setGlyphColor(I)V
    .locals 1

    .prologue
    .line 159691
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 159692
    return-void
.end method

.method public setGlyphColor(Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 159687
    iput-object p1, p0, Lcom/facebook/fbui/glyph/GlyphView;->b:Landroid/content/res/ColorStateList;

    .line 159688
    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphView;->refreshDrawableState()V

    .line 159689
    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphView;->invalidate()V

    .line 159690
    return-void
.end method
