.class public Lcom/facebook/fbui/widget/text/GlyphWithTextView;
.super Lcom/facebook/fbui/widget/text/ImageWithTextView;
.source ""


# static fields
.field private static final a:[I


# instance fields
.field private b:Z

.field private c:Landroid/content/res/ColorStateList;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 276143
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a2

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 276141
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 276142
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 276139
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 276140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 276133
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 276134
    sget-object v0, LX/03r;->GlyphColorizer:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 276135
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276136
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 276137
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 276138
    return-void
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 4

    .prologue
    .line 276144
    invoke-super {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->drawableStateChanged()V

    .line 276145
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 276146
    if-nez v0, :cond_0

    .line 276147
    :goto_0
    return-void

    .line 276148
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->c:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_1

    .line 276149
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->c:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 276150
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 276151
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v0, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0

    .line 276152
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 276153
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public final onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 276128
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->b:Z

    if-nez v0, :cond_0

    .line 276129
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 276130
    :goto_0
    return-object v0

    .line 276131
    :cond_0
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 276132
    sget-object v1, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->a:[I

    invoke-static {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->mergeDrawableStates([I[I)[I

    goto :goto_0
.end method

.method public setActive(Z)V
    .locals 1

    .prologue
    .line 276122
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 276123
    if-nez v0, :cond_0

    .line 276124
    :goto_0
    return-void

    .line 276125
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->b:Z

    .line 276126
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->refreshDrawableState()V

    .line 276127
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->invalidate()V

    goto :goto_0
.end method

.method public setGlyphColor(I)V
    .locals 1

    .prologue
    .line 276116
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 276117
    return-void
.end method

.method public setGlyphColor(Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 276118
    iput-object p1, p0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->c:Landroid/content/res/ColorStateList;

    .line 276119
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->refreshDrawableState()V

    .line 276120
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->invalidate()V

    .line 276121
    return-void
.end method
