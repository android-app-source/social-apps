.class public Lcom/facebook/fbui/widget/text/ImageWithTextView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;

.field private b:Z

.field private c:F

.field private d:F

.field private e:I

.field private f:I

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:I

.field public l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 276283
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 276284
    iput-boolean v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->b:Z

    .line 276285
    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->c:F

    .line 276286
    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->d:F

    .line 276287
    iput-boolean v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->l:Z

    .line 276288
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setGravity(I)V

    .line 276289
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 276253
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 276254
    iput-boolean v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->b:Z

    .line 276255
    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->c:F

    .line 276256
    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->d:F

    .line 276257
    iput-boolean v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->l:Z

    .line 276258
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 276259
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 276260
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 276261
    iput-boolean v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->b:Z

    .line 276262
    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->c:F

    .line 276263
    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->d:F

    .line 276264
    iput-boolean v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->l:Z

    .line 276265
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 276266
    return-void
.end method

.method private static a(IF)F
    .locals 8

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 276267
    int-to-float v1, p0

    div-float v1, v0, v1

    float-to-double v2, v1

    .line 276268
    float-to-double v4, p1

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    cmpg-double v1, v4, v2

    if-gez v1, :cond_0

    move p1, v0

    :cond_0
    return p1
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 276269
    sget-object v0, LX/03r;->ImageWithTextView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 276270
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 276271
    if-eqz v1, :cond_0

    .line 276272
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 276273
    :cond_0
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    .line 276274
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 276275
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setGravity(I)V

    .line 276276
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 276277
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 276278
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->g:F

    add-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->h:F

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 276279
    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->c:F

    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->d:F

    iget v2, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->i:F

    iget v3, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->j:F

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 276280
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 276281
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 276282
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 276290
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 276291
    iget-boolean v2, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->l:Z

    move v2, v2

    .line 276292
    if-eqz v2, :cond_2

    iget v2, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 276293
    iget-boolean v2, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->l:Z

    move v2, v2

    .line 276294
    if-eqz v2, :cond_2

    iget v2, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 276295
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 276296
    :goto_0
    return-void

    .line 276297
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 276298
    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    int-to-float v0, v0

    div-float/2addr v0, v3

    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->i:F

    .line 276299
    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    int-to-float v0, v0

    div-float/2addr v0, v3

    .line 276300
    :cond_1
    :goto_1
    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->j:F

    goto :goto_0

    .line 276301
    :cond_2
    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 276302
    :cond_3
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    int-to-float v0, v0

    :cond_4
    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->i:F

    .line 276303
    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    int-to-float v0, v0

    div-float/2addr v0, v3

    goto :goto_1

    .line 276304
    :cond_5
    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    iput v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->i:F

    .line 276305
    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    int-to-float v0, v0

    goto :goto_1
.end method

.method private setupDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 276306
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 276307
    :goto_0
    return-void

    .line 276308
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 276309
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 276310
    :cond_1
    iput-object p1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    .line 276311
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 276312
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 276313
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 276314
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 276315
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    .line 276316
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    .line 276317
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    iget v2, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 276318
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->refreshDrawableState()V

    .line 276319
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f()V

    goto :goto_0

    .line 276320
    :cond_3
    iput v3, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    .line 276321
    iput v3, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 276322
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->b:Z

    if-eq v0, p1, :cond_0

    .line 276323
    iput-boolean p1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->b:Z

    .line 276324
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->requestLayout()V

    .line 276325
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->invalidate()V

    .line 276326
    :cond_0
    return-void
.end method

.method public drawableStateChanged()V
    .locals 2

    .prologue
    .line 276327
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->drawableStateChanged()V

    .line 276328
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276329
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 276330
    :cond_0
    return-void
.end method

.method public getCompoundPaddingBottom()I
    .locals 3

    .prologue
    .line 276331
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundPaddingBottom()I

    move-result v0

    .line 276332
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 276333
    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getCompoundDrawablePadding()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 276334
    :cond_0
    return v0
.end method

.method public getCompoundPaddingLeft()I
    .locals 3

    .prologue
    .line 276245
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundPaddingLeft()I

    move-result v0

    .line 276246
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276247
    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getCompoundDrawablePadding()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 276248
    :cond_0
    return v0
.end method

.method public getCompoundPaddingRight()I
    .locals 3

    .prologue
    .line 276249
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundPaddingRight()I

    move-result v0

    .line 276250
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276251
    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getCompoundDrawablePadding()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 276252
    :cond_0
    return v0
.end method

.method public getCompoundPaddingTop()I
    .locals 3

    .prologue
    .line 276154
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundPaddingTop()I

    move-result v0

    .line 276155
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 276156
    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getCompoundDrawablePadding()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 276157
    :cond_0
    return v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 276158
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getImageScaleX()F
    .locals 1

    .prologue
    .line 276159
    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->c:F

    return v0
.end method

.method public getImageScaleY()F
    .locals 1

    .prologue
    .line 276160
    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->d:F

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 276161
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_1

    .line 276162
    if-eqz p1, :cond_0

    .line 276163
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    .line 276164
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    .line 276165
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f()V

    .line 276166
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->invalidate()V

    .line 276167
    :goto_0
    return-void

    .line 276168
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 276169
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->jumpDrawablesToCurrentState()V

    .line 276170
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 276171
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 276172
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 276173
    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 276174
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 276175
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(Landroid/graphics/Canvas;)V

    .line 276176
    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/16 v0, 0x2c

    const v3, 0x353a95b

    invoke-static {v8, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 276177
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;->onMeasure(II)V

    .line 276178
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->b:Z

    if-nez v0, :cond_0

    .line 276179
    const/16 v0, 0x2d

    const v1, -0x16a0eff1

    invoke-static {v8, v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 276180
    :goto_0
    return-void

    .line 276181
    :cond_0
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->l:Z

    .line 276182
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getPaddingTop()I

    move-result v0

    .line 276183
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getPaddingBottom()I

    move-result v4

    .line 276184
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getPaddingLeft()I

    move-result v5

    .line 276185
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getPaddingRight()I

    move-result v6

    .line 276186
    iget v7, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    if-eqz v7, :cond_1

    iget v7, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    if-ne v7, v8, :cond_4

    .line 276187
    :cond_1
    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 276188
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getMeasuredWidth()I

    move-result v1

    .line 276189
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getMeasuredHeight()I

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 276190
    invoke-virtual {p0, v1, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setMeasuredDimension(II)V

    .line 276191
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getCompoundDrawablePadding()I

    move-result v4

    .line 276192
    iget v7, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    add-int/2addr v4, v7

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getLayout()Landroid/text/Layout;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v2

    add-float/2addr v2, v4

    .line 276193
    int-to-float v4, v1

    sub-float v2, v4, v2

    int-to-float v4, v5

    sub-float/2addr v2, v4

    int-to-float v4, v6

    sub-float/2addr v2, v4

    div-float/2addr v2, v9

    .line 276194
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->d()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 276195
    int-to-float v1, v5

    add-float/2addr v1, v2

    iput v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->g:F

    .line 276196
    :goto_2
    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v9

    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->h:F

    .line 276197
    :goto_3
    const v0, -0x25fd1ef

    invoke-static {v0, v3}, LX/02F;->g(II)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 276198
    goto :goto_1

    .line 276199
    :cond_3
    int-to-float v1, v1

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    int-to-float v2, v6

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->g:F

    goto :goto_2

    .line 276200
    :cond_4
    iget v5, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getPaddingLeft()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    .line 276201
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getMeasuredWidth()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 276202
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getMeasuredHeight()I

    move-result v6

    .line 276203
    invoke-virtual {p0, v5, v6}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setMeasuredDimension(II)V

    .line 276204
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getCompoundDrawablePadding()I

    move-result v7

    .line 276205
    iget v8, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getLayout()Landroid/text/Layout;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v2

    add-int/2addr v2, v7

    int-to-float v2, v2

    .line 276206
    iget v7, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    sub-int/2addr v5, v7

    int-to-float v5, v5

    div-float/2addr v5, v9

    iput v5, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->g:F

    .line 276207
    int-to-float v5, v6

    sub-float v2, v5, v2

    int-to-float v5, v0

    sub-float/2addr v2, v5

    int-to-float v5, v4

    sub-float/2addr v2, v5

    div-float/2addr v2, v9

    .line 276208
    iget v5, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    if-ne v5, v1, :cond_5

    .line 276209
    int-to-float v0, v0

    add-float/2addr v0, v2

    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->h:F

    goto :goto_3

    .line 276210
    :cond_5
    int-to-float v0, v6

    sub-float/2addr v0, v2

    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    int-to-float v1, v4

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->h:F

    goto :goto_3
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 276211
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setupDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 276212
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->requestLayout()V

    .line 276213
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->invalidate()V

    .line 276214
    return-void
.end method

.method public setImageResource(I)V
    .locals 1

    .prologue
    .line 276215
    if-eqz p1, :cond_0

    .line 276216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->b:Z

    .line 276217
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setupDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 276218
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->requestLayout()V

    .line 276219
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->invalidate()V

    .line 276220
    return-void

    .line 276221
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->b:Z

    goto :goto_0
.end method

.method public setImageScale(F)V
    .locals 0

    .prologue
    .line 276242
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageScaleX(F)V

    .line 276243
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageScaleY(F)V

    .line 276244
    return-void
.end method

.method public setImageScaleX(F)V
    .locals 2

    .prologue
    .line 276222
    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->e:I

    invoke-static {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(IF)F

    move-result v0

    .line 276223
    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->c:F

    invoke-static {v1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_0

    .line 276224
    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->c:F

    .line 276225
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->invalidate()V

    .line 276226
    :cond_0
    return-void
.end method

.method public setImageScaleY(F)V
    .locals 2

    .prologue
    .line 276227
    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f:I

    invoke-static {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(IF)F

    move-result v0

    .line 276228
    iget v1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->d:F

    invoke-static {v1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_0

    .line 276229
    iput v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->d:F

    .line 276230
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->invalidate()V

    .line 276231
    :cond_0
    return-void
.end method

.method public setOrientation(I)V
    .locals 1

    .prologue
    .line 276232
    iget v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    if-eq v0, p1, :cond_0

    .line 276233
    iput p1, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->k:I

    .line 276234
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f()V

    .line 276235
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->requestLayout()V

    .line 276236
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->invalidate()V

    .line 276237
    :cond_0
    return-void
.end method

.method public final setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 0

    .prologue
    .line 276238
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 276239
    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->f()V

    .line 276240
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 276241
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
