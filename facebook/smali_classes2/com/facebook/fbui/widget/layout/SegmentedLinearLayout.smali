.class public Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 276103
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 276104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 276009
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 276010
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a:Landroid/graphics/Rect;

    .line 276011
    sget-object v0, LX/03r;->SegmentedLinearLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 276012
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 276013
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 276014
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->f:I

    .line 276015
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 276016
    const/16 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 276017
    const/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->g:I

    .line 276018
    :goto_0
    const/16 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 276019
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->h:I

    .line 276020
    :goto_1
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->e:I

    .line 276021
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 276022
    return-void

    .line 276023
    :cond_0
    iput v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->g:I

    goto :goto_0

    .line 276024
    :cond_1
    iput v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->h:I

    goto :goto_1
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 276089
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildCount()I

    move-result v2

    .line 276090
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 276091
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 276092
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-eq v0, v4, :cond_0

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276093
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 276094
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v0, v3, v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a(Landroid/graphics/Canvas;I)V

    .line 276095
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 276096
    :cond_1
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 276097
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 276098
    if-nez v0, :cond_3

    .line 276099
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getSegmentedDividerThickness()I

    move-result v1

    sub-int/2addr v0, v1

    .line 276100
    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a(Landroid/graphics/Canvas;I)V

    .line 276101
    :cond_2
    return-void

    .line 276102
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    goto :goto_1
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .locals 4

    .prologue
    .line 276084
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276085
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->g:I

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->h:I

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getSegmentedDividerThickness()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 276086
    :goto_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 276087
    return-void

    .line 276088
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->h:I

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->g:I

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getSegmentedDividerThickness()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 276076
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/ColorDrawable;

    if-nez v0, :cond_1

    .line 276077
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 276078
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 276079
    :goto_0
    return-void

    .line 276080
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 276081
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 276082
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 276083
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 276075
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 276053
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildCount()I

    move-result v2

    .line 276054
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a()Z

    move-result v3

    .line 276055
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 276056
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 276057
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-eq v0, v5, :cond_0

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276058
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 276059
    if-eqz v3, :cond_1

    .line 276060
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v0, v4, v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b(Landroid/graphics/Canvas;I)V

    .line 276061
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 276062
    :cond_1
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b(Landroid/graphics/Canvas;I)V

    goto :goto_1

    .line 276063
    :cond_2
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 276064
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 276065
    if-eqz v3, :cond_5

    .line 276066
    if-nez v0, :cond_4

    .line 276067
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getSegmentedDividerThickness()I

    move-result v1

    sub-int/2addr v0, v1

    .line 276068
    :goto_2
    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b(Landroid/graphics/Canvas;I)V

    .line 276069
    :cond_3
    :goto_3
    return-void

    .line 276070
    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    goto :goto_2

    .line 276071
    :cond_5
    if-nez v0, :cond_6

    .line 276072
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getPaddingLeft()I

    move-result v0

    .line 276073
    :goto_4
    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b(Landroid/graphics/Canvas;I)V

    goto :goto_3

    .line 276074
    :cond_6
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    goto :goto_4
.end method

.method private b(Landroid/graphics/Canvas;I)V
    .locals 5

    .prologue
    .line 276050
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->g:I

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getSegmentedDividerThickness()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->h:I

    sub-int/2addr v3, v4

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 276051
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 276052
    return-void
.end method


# virtual methods
.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 276039
    instance-of v0, p3, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_2

    .line 276040
    check-cast p3, Landroid/widget/LinearLayout$LayoutParams;

    .line 276041
    iget v0, p3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 276042
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getOrientation()I

    move-result v0

    .line 276043
    if-nez v0, :cond_1

    iget v1, p3, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-nez v1, :cond_1

    .line 276044
    iput v2, p3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 276045
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 276046
    :goto_1
    return-void

    .line 276047
    :cond_1
    if-ne v0, v2, :cond_0

    iget v0, p3, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-nez v0, :cond_0

    .line 276048
    iput v2, p3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_0

    .line 276049
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method public final b_(II)V
    .locals 1

    .prologue
    .line 276030
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->g:I

    if-eq v0, p1, :cond_0

    .line 276031
    iput p1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->g:I

    .line 276032
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->requestLayout()V

    .line 276033
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->invalidate()V

    .line 276034
    :cond_0
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->h:I

    if-eq v0, p2, :cond_1

    .line 276035
    iput p2, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->h:I

    .line 276036
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->requestLayout()V

    .line 276037
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->invalidate()V

    .line 276038
    :cond_1
    return-void
.end method

.method public final c(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 276105
    if-nez p1, :cond_2

    .line 276106
    iget v2, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->f:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 276107
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 276108
    goto :goto_0

    .line 276109
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildCount()I

    move-result v2

    if-ne p1, v2, :cond_3

    .line 276110
    iget v2, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->f:I

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 276111
    :cond_3
    iget v2, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->f:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_4

    .line 276112
    add-int/lit8 v2, p1, -0x1

    :goto_1
    if-ltz v2, :cond_5

    .line 276113
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    .line 276114
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 276115
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public getSegmentedDivider()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 275951
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getSegmentedDividerPadding()I
    .locals 2

    .prologue
    .line 275952
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->g:I

    iget v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->h:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getSegmentedDividerThickness()I
    .locals 2

    .prologue
    .line 275953
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 275954
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->e:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->e:I

    .line 275955
    :goto_0
    return v0

    .line 275956
    :cond_0
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->d:I

    goto :goto_0

    .line 275957
    :cond_1
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->e:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->e:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c:I

    goto :goto_0
.end method

.method public getShowSegmentedDividers()I
    .locals 1

    .prologue
    .line 275958
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->f:I

    return v0
.end method

.method public final measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, -0x6ad26b2a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 275959
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 275960
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 275961
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildCount()I

    move-result v3

    .line 275962
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 275963
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getSegmentedDividerThickness()I

    move-result v4

    .line 275964
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getOrientation()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 275965
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 275966
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 275967
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 275968
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 275969
    :cond_0
    add-int/lit8 v5, v3, -0x1

    if-ne v2, v5, :cond_1

    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 275970
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 275971
    :cond_1
    :goto_0
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 275972
    const v0, 0x498b3252    # 1140298.2f

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 275973
    :cond_2
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 275974
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 275975
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 275976
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 275977
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 275978
    :cond_3
    add-int/lit8 v5, v3, -0x1

    if-ne v2, v5, :cond_1

    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 275979
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_0

    .line 275980
    :cond_4
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 275981
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 275982
    :cond_5
    add-int/lit8 v5, v3, -0x1

    if-ne v2, v5, :cond_1

    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 275983
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 275984
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 275985
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 275986
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->a(Landroid/graphics/Canvas;)V

    .line 275987
    :cond_0
    :goto_0
    return-void

    .line 275988
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 275989
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    if-eq v1, p1, :cond_1

    .line 275990
    iput-object p1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    .line 275991
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 275992
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c:I

    .line 275993
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->d:I

    .line 275994
    :goto_0
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setWillNotDraw(Z)V

    .line 275995
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->requestLayout()V

    .line 275996
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->invalidate()V

    .line 275997
    :cond_1
    return-void

    .line 275998
    :cond_2
    iput v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->d:I

    iput v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->c:I

    goto :goto_0
.end method

.method public setSegmentedDividerPadding(I)V
    .locals 1

    .prologue
    .line 275999
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->g:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->h:I

    if-eq v0, p1, :cond_1

    .line 276000
    :cond_0
    iput p1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->h:I

    iput p1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->g:I

    .line 276001
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->requestLayout()V

    .line 276002
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->invalidate()V

    .line 276003
    :cond_1
    return-void
.end method

.method public setSegmentedDividerThickness(I)V
    .locals 1

    .prologue
    .line 276004
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->e:I

    if-eq v0, p1, :cond_0

    .line 276005
    iput p1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->e:I

    .line 276006
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->requestLayout()V

    .line 276007
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->invalidate()V

    .line 276008
    :cond_0
    return-void
.end method

.method public setShowSegmentedDividers(I)V
    .locals 1

    .prologue
    .line 276025
    iget v0, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->f:I

    if-eq v0, p1, :cond_0

    .line 276026
    iput p1, p0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->f:I

    .line 276027
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->requestLayout()V

    .line 276028
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->invalidate()V

    .line 276029
    :cond_0
    return-void
.end method
