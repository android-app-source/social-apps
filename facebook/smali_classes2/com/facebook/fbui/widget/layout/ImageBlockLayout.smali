.class public Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.super Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;
.source ""


# instance fields
.field private A:Z

.field private final B:Landroid/graphics/Rect;

.field private final C:Landroid/graphics/Rect;

.field public a:Landroid/view/View;

.field public b:I

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:Landroid/graphics/Paint;

.field public v:I

.field public w:I

.field public x:I

.field public y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 277461
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 277462
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 277387
    const v0, 0x7f01019e

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 277388
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 277382
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 277383
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->B:Landroid/graphics/Rect;

    .line 277384
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->C:Landroid/graphics/Rect;

    .line 277385
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 277386
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/16 v3, 0x30

    const/4 v6, -0x2

    const/4 v1, 0x0

    .line 277332
    new-instance v0, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->u:Landroid/graphics/Paint;

    .line 277333
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->u:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 277334
    sget-object v0, LX/03r;->ImageBlockLayout:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 277335
    const/16 v0, 0x6

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 277336
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setLayout(I)V

    .line 277337
    const/16 v0, 0x7

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 277338
    if-eqz v0, :cond_0

    .line 277339
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277340
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 277341
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277342
    const/16 v0, 0x0

    invoke-virtual {v5, v0, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b:I

    .line 277343
    const/16 v0, 0xd

    invoke-virtual {v5, v0, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->n:I

    .line 277344
    const/16 v0, 0xe

    const/16 v2, 0x11

    invoke-virtual {v5, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->k:I

    .line 277345
    const/16 v0, 0xb

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->l:I

    .line 277346
    const/16 v0, 0xc

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->m:I

    .line 277347
    const/16 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277348
    const/16 v0, 0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 277349
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setPadding(IIII)V

    .line 277350
    :goto_0
    const/16 v0, 0x12

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 277351
    const/16 v0, 0x12

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 277352
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    .line 277353
    :goto_1
    const/16 v0, 0x11

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 277354
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setBorderColor(I)V

    .line 277355
    const/16 v0, 0x17

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 277356
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setClipBorderToPadding(Z)V

    .line 277357
    const/16 v0, 0x9

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    .line 277358
    const/16 v2, 0xa

    invoke-virtual {v5, v2, v6}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v2

    .line 277359
    invoke-virtual {p0, v0, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->d(II)V

    .line 277360
    const/16 v0, 0xf

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 277361
    const/16 v0, 0x10

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setAuxViewPadding(I)V

    .line 277362
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 277363
    return-void

    .line 277364
    :cond_1
    const/16 v0, 0x2

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 277365
    const/16 v0, 0x2

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 277366
    :goto_2
    const/16 v2, 0x4

    invoke-virtual {v5, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 277367
    const/16 v2, 0x4

    invoke-virtual {v5, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 277368
    :goto_3
    const/16 v3, 0x3

    invoke-virtual {v5, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 277369
    const/16 v3, 0x3

    invoke-virtual {v5, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 277370
    :goto_4
    const/16 v4, 0x5

    invoke-virtual {v5, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 277371
    const/16 v4, 0x5

    invoke-virtual {v5, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 277372
    :goto_5
    invoke-virtual {p0, v0, v3, v2, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setPadding(IIII)V

    goto/16 :goto_0

    .line 277373
    :cond_2
    const/16 v0, 0x15

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 277374
    const/16 v0, 0x15

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 277375
    :goto_6
    const/16 v2, 0x16

    invoke-virtual {v5, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 277376
    const/16 v2, 0x16

    invoke-virtual {v5, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 277377
    :goto_7
    const/16 v3, 0x13

    invoke-virtual {v5, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 277378
    const/16 v3, 0x13

    invoke-virtual {v5, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 277379
    :goto_8
    const/16 v4, 0x14

    invoke-virtual {v5, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 277380
    const/16 v4, 0x14

    invoke-virtual {v5, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 277381
    :goto_9
    invoke-virtual {p0, v0, v3, v2, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    goto/16 :goto_1

    :cond_3
    move v4, v1

    goto :goto_9

    :cond_4
    move v3, v1

    goto :goto_8

    :cond_5
    move v2, v1

    goto :goto_7

    :cond_6
    move v0, v1

    goto :goto_6

    :cond_7
    move v4, v1

    goto :goto_5

    :cond_8
    move v3, v1

    goto :goto_4

    :cond_9
    move v2, v1

    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_2
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 277326
    if-nez p1, :cond_1

    .line 277327
    :cond_0
    :goto_0
    return-void

    .line 277328
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 277329
    iput-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->f:Landroid/view/View;

    goto :goto_0

    .line 277330
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 277331
    iput-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    goto :goto_0
.end method

.method private static b(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 277324
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1au;

    .line 277325
    iget-boolean v1, v0, LX/1au;->a:Z

    if-nez v1, :cond_0

    iget-boolean v1, v0, LX/1au;->b:Z

    if-nez v1, :cond_0

    iget-boolean v0, v0, LX/1au;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v5, -0x1

    const/4 v4, -0x2

    .line 277302
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 277303
    :goto_0
    return-void

    .line 277304
    :cond_0
    sget-object v1, LX/1aU;->a:[I

    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getThumbnailType()LX/1aV;

    move-result-object v2

    invoke-virtual {v2}, LX/1aV;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_1
    move v3, v0

    .line 277305
    :goto_1
    iget v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->l:I

    if-ne v1, v5, :cond_4

    move v1, v3

    .line 277306
    :goto_2
    iget v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->m:I

    if-ne v2, v5, :cond_6

    move v2, v0

    .line 277307
    :goto_3
    iget-object v4, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->B:Landroid/graphics/Rect;

    iget v5, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->q:I

    iget v6, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    iget v7, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->q:I

    add-int/2addr v3, v7

    iget v7, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    add-int/2addr v0, v7

    invoke-virtual {v4, v5, v6, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 277308
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->k:I

    iget-object v3, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->B:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->C:Landroid/graphics/Rect;

    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v5

    invoke-static/range {v0 .. v5}, LX/1uf;->a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 277309
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->C:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 277310
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 277311
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_4
    move v3, v1

    .line 277312
    goto :goto_1

    .line 277313
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 277314
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->g:I

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 277315
    :goto_5
    iget v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->h:I

    if-gez v1, :cond_3

    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_4

    .line 277316
    :cond_2
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->g:I

    goto :goto_5

    .line 277317
    :cond_3
    iget v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->h:I

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_4

    .line 277318
    :cond_4
    iget v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->l:I

    if-ne v1, v4, :cond_5

    .line 277319
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    goto :goto_2

    .line 277320
    :cond_5
    iget v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->l:I

    goto :goto_2

    .line 277321
    :cond_6
    iget v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->l:I

    if-ne v2, v4, :cond_7

    .line 277322
    iget-object v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    goto :goto_3

    .line 277323
    :cond_7
    iget v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->m:I

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private f()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 277297
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 277298
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 277299
    :cond_0
    :goto_0
    return v0

    .line 277300
    :cond_1
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 277301
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->i:Z

    goto :goto_0
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 277216
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getThumbnailType()LX/1aV;
    .locals 2

    .prologue
    .line 277290
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 277291
    sget-object v0, LX/1aV;->VIEW:LX/1aV;

    .line 277292
    :goto_0
    return-object v0

    .line 277293
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->i:Z

    if-eqz v0, :cond_1

    .line 277294
    sget-object v0, LX/1aV;->DRAWABLE:LX/1aV;

    goto :goto_0

    .line 277295
    :cond_1
    sget-object v0, LX/1aV;->NONE:LX/1aV;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/util/AttributeSet;)LX/1au;
    .locals 2

    .prologue
    .line 277289
    new-instance v0, LX/1au;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/1au;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup$LayoutParams;)LX/1au;
    .locals 1

    .prologue
    .line 277286
    instance-of v0, p1, LX/1au;

    if-eqz v0, :cond_0

    .line 277287
    check-cast p1, LX/1au;

    .line 277288
    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b()LX/1au;

    move-result-object p1

    goto :goto_0
.end method

.method public a(II)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 277273
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getChildCount()I

    move-result v9

    move v8, v3

    move v6, v3

    move v7, v3

    .line 277274
    :goto_0
    if-ge v8, v9, :cond_0

    .line 277275
    invoke-virtual {p0, v8}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 277276
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_1

    invoke-static {v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277277
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1au;

    .line 277278
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int v10, v2, v4

    .line 277279
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v11, v2, v0

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    .line 277280
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 277281
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v10

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 277282
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v11

    add-int/2addr v0, v6

    move v1, v2

    .line 277283
    :goto_1
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v6, v0

    move v7, v1

    goto :goto_0

    .line 277284
    :cond_0
    invoke-virtual {p0, v7, v6}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(II)V

    .line 277285
    return-void

    :cond_1
    move v0, v6

    move v1, v7

    goto :goto_1
.end method

.method public a(IIII)V
    .locals 12

    .prologue
    .line 277244
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v4

    .line 277245
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getChildCount()I

    move-result v5

    .line 277246
    const/4 v0, 0x0

    move v3, v0

    move v2, p2

    :goto_0
    if-ge v3, v5, :cond_6

    .line 277247
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 277248
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_7

    invoke-static {v6}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 277249
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1au;

    .line 277250
    iget v1, v0, LX/1au;->d:I

    if-gez v1, :cond_0

    const v1, 0x800003

    .line 277251
    :goto_1
    and-int/lit8 v1, v1, 0x7

    .line 277252
    invoke-static {v0}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v7

    .line 277253
    invoke-static {v0}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v8

    .line 277254
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 277255
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 277256
    if-eqz v4, :cond_3

    .line 277257
    const/4 v11, 0x3

    if-ne v1, v11, :cond_1

    .line 277258
    add-int v1, p1, v7

    .line 277259
    :goto_2
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v7

    .line 277260
    add-int v7, v1, v9

    add-int v8, v2, v10

    invoke-virtual {v6, v1, v2, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 277261
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v10

    add-int/2addr v0, v2

    .line 277262
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 277263
    :cond_0
    iget v1, v0, LX/1au;->d:I

    goto :goto_1

    .line 277264
    :cond_1
    const/4 v11, 0x5

    if-ne v1, v11, :cond_2

    .line 277265
    sub-int v1, p3, v8

    sub-int/2addr v1, v9

    goto :goto_2

    .line 277266
    :cond_2
    sub-int v1, p3, p1

    sub-int/2addr v1, v7

    sub-int/2addr v1, v9

    sub-int/2addr v1, v8

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p1

    add-int/2addr v1, v7

    goto :goto_2

    .line 277267
    :cond_3
    const/4 v11, 0x3

    if-ne v1, v11, :cond_4

    .line 277268
    sub-int v1, p3, v7

    sub-int/2addr v1, v9

    goto :goto_2

    .line 277269
    :cond_4
    const/4 v11, 0x5

    if-ne v1, v11, :cond_5

    .line 277270
    add-int v1, p1, v8

    goto :goto_2

    .line 277271
    :cond_5
    sub-int v1, p3, p1

    sub-int/2addr v1, v7

    sub-int/2addr v1, v9

    sub-int/2addr v1, v8

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p1

    add-int/2addr v1, v8

    goto :goto_2

    .line 277272
    :cond_6
    return-void

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 277228
    instance-of v0, p3, LX/1au;

    if-eqz v0, :cond_2

    move-object v0, p3

    .line 277229
    check-cast v0, LX/1au;

    .line 277230
    iget-boolean v1, v0, LX/1au;->a:Z

    if-eqz v1, :cond_3

    .line 277231
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 277232
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->removeView(Landroid/view/View;)V

    .line 277233
    :cond_0
    iget v1, v0, LX/1au;->d:I

    if-gez v1, :cond_1

    .line 277234
    const/16 v1, 0x30

    iput v1, v0, LX/1au;->d:I

    .line 277235
    :cond_1
    iput-object p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->f:Landroid/view/View;

    .line 277236
    :cond_2
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 277237
    return-void

    .line 277238
    :cond_3
    iget-boolean v1, v0, LX/1au;->b:Z

    if-eqz v1, :cond_2

    .line 277239
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 277240
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->removeView(Landroid/view/View;)V

    .line 277241
    :cond_4
    iget v1, v0, LX/1au;->d:I

    if-gez v1, :cond_5

    .line 277242
    const/16 v1, 0x10

    iput v1, v0, LX/1au;->d:I

    .line 277243
    :cond_5
    iput-object p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    goto :goto_0
.end method

.method public b()LX/1au;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 277227
    new-instance v0, LX/1au;

    invoke-direct {v0, v1, v1}, LX/1au;-><init>(II)V

    return-object v0
.end method

.method public final b(II)V
    .locals 0

    .prologue
    .line 277224
    iput p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->s:I

    .line 277225
    iput p2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->t:I

    .line 277226
    return-void
.end method

.method public final b(IIII)V
    .locals 0

    .prologue
    .line 277217
    iput p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->v:I

    .line 277218
    iput p3, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->x:I

    .line 277219
    iput p2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->w:I

    .line 277220
    iput p4, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->y:I

    .line 277221
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->requestLayout()V

    .line 277222
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->invalidate()V

    .line 277223
    return-void
.end method

.method public final c(II)V
    .locals 0

    .prologue
    .line 277457
    iput p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->l:I

    .line 277458
    iput p2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->m:I

    .line 277459
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->e()V

    .line 277460
    return-void
.end method

.method public checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 277390
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/1au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 277441
    iput p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->g:I

    .line 277442
    iput p2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->h:I

    .line 277443
    sget-object v0, LX/1aU;->a:[I

    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getThumbnailType()LX/1aV;

    move-result-object v1

    invoke-virtual {v1}, LX/1aV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 277444
    :goto_0
    return-void

    .line 277445
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1au;

    .line 277446
    if-nez v0, :cond_0

    .line 277447
    new-instance v0, LX/1au;

    invoke-direct {v0, p1, p2}, LX/1au;-><init>(II)V

    .line 277448
    iput-boolean v2, v0, LX/1au;->a:Z

    .line 277449
    const/16 v1, 0x30

    iput v1, v0, LX/1au;->d:I

    .line 277450
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 277451
    :cond_0
    iput p1, v0, LX/1au;->width:I

    .line 277452
    iput p2, v0, LX/1au;->height:I

    .line 277453
    iput-boolean v2, v0, LX/1au;->a:Z

    .line 277454
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_0

    .line 277455
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->requestLayout()V

    .line 277456
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->invalidate()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 277406
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 277407
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceTop()I

    move-result v3

    .line 277408
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceBottom()I

    move-result v4

    .line 277409
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceLeft()I

    move-result v0

    .line 277410
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceRight()I

    move-result v5

    .line 277411
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getMeasuredWidth()I

    move-result v9

    .line 277412
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getMeasuredHeight()I

    move-result v8

    .line 277413
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getThumbnailType()LX/1aV;

    move-result-object v6

    .line 277414
    sget-object v7, LX/1aV;->DRAWABLE:LX/1aV;

    if-ne v6, v7, :cond_0

    .line 277415
    iget-object v6, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 277416
    :cond_0
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->f()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_1

    .line 277417
    iget-object v6, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 277418
    :cond_1
    iget-boolean v6, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->A:Z

    if-eqz v6, :cond_9

    move v6, v0

    .line 277419
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->A:Z

    if-eqz v0, :cond_a

    sub-int v0, v9, v5

    move v7, v0

    .line 277420
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->A:Z

    if-eqz v0, :cond_b

    move v10, v3

    .line 277421
    :goto_2
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->A:Z

    if-eqz v0, :cond_c

    sub-int v0, v8, v4

    move v11, v0

    .line 277422
    :goto_3
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->w:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->y:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->v:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->x:I

    if-eqz v0, :cond_d

    :cond_2
    const/4 v1, 0x1

    move v12, v1

    .line 277423
    :goto_4
    if-eqz v12, :cond_3

    .line 277424
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 277425
    :cond_3
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->w:I

    if-eqz v0, :cond_4

    .line 277426
    int-to-float v1, v6

    int-to-float v3, v7

    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->w:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->u:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 277427
    :cond_4
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->y:I

    if-eqz v0, :cond_5

    .line 277428
    int-to-float v4, v6

    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->y:I

    sub-int v0, v8, v0

    int-to-float v5, v0

    int-to-float v6, v7

    int-to-float v7, v8

    iget-object v8, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->u:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 277429
    :cond_5
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->v:I

    if-eqz v0, :cond_6

    .line 277430
    int-to-float v3, v10

    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->v:I

    int-to-float v4, v0

    int-to-float v5, v11

    iget-object v6, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->u:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 277431
    :cond_6
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->x:I

    if-eqz v0, :cond_7

    .line 277432
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->x:I

    sub-int v0, v9, v0

    int-to-float v1, v0

    int-to-float v2, v10

    int-to-float v3, v9

    int-to-float v4, v11

    iget-object v5, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->u:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 277433
    :cond_7
    if-eqz v12, :cond_8

    .line 277434
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 277435
    :cond_8
    return-void

    :cond_9
    move v6, v1

    .line 277436
    goto :goto_0

    :cond_a
    move v7, v9

    .line 277437
    goto :goto_1

    :cond_b
    move v10, v1

    .line 277438
    goto :goto_2

    :cond_c
    move v11, v8

    .line 277439
    goto :goto_3

    :cond_d
    move v12, v1

    .line 277440
    goto :goto_4
.end method

.method public drawableStateChanged()V
    .locals 2

    .prologue
    .line 277402
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->drawableStateChanged()V

    .line 277403
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277404
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 277405
    :cond_0
    return-void
.end method

.method public synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 277401
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b()LX/1au;

    move-result-object v0

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 277400
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a(Landroid/util/AttributeSet;)LX/1au;

    move-result-object v0

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 277399
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a(Landroid/view/ViewGroup$LayoutParams;)LX/1au;

    move-result-object v0

    return-object v0
.end method

.method public getAuxView()Landroid/view/View;
    .locals 1

    .prologue
    .line 277398
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    return-object v0
.end method

.method public getAuxViewPadding()I
    .locals 1

    .prologue
    .line 277397
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->p:I

    return v0
.end method

.method public getBorderBottom()I
    .locals 1

    .prologue
    .line 277396
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->y:I

    return v0
.end method

.method public getBorderColor()I
    .locals 1

    .prologue
    .line 277395
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->z:I

    return v0
.end method

.method public getBorderLeft()I
    .locals 1

    .prologue
    .line 277394
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->v:I

    return v0
.end method

.method public getBorderRight()I
    .locals 1

    .prologue
    .line 277393
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->x:I

    return v0
.end method

.method public getBorderTop()I
    .locals 1

    .prologue
    .line 277392
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->w:I

    return v0
.end method

.method public getFeatureTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277391
    const-string v0, "thumbnail"

    return-object v0
.end method

.method public getGravity()I
    .locals 1

    .prologue
    .line 277296
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b:I

    return v0
.end method

.method public getInsetBottom()I
    .locals 1

    .prologue
    .line 277389
    const/4 v0, 0x0

    return v0
.end method

.method public getInsetLeft()I
    .locals 1

    .prologue
    .line 277033
    const/4 v0, 0x0

    return v0
.end method

.method public getInsetRight()I
    .locals 1

    .prologue
    .line 277071
    const/4 v0, 0x0

    return v0
.end method

.method public getInsetTop()I
    .locals 1

    .prologue
    .line 277070
    const/4 v0, 0x0

    return v0
.end method

.method public getLocaleGravity()I
    .locals 2

    .prologue
    .line 277068
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 277069
    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v0, v0, 0xc0

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x5

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public getMeasuredContentHeight()I
    .locals 1

    .prologue
    .line 277067
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->t:I

    return v0
.end method

.method public getMeasuredContentWidth()I
    .locals 1

    .prologue
    .line 277066
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->s:I

    return v0
.end method

.method public getOverlayGravity()I
    .locals 1

    .prologue
    .line 277065
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->k:I

    return v0
.end method

.method public getSpaceBottom()I
    .locals 2

    .prologue
    .line 277060
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getPaddingBottom()I

    move-result v0

    .line 277061
    iget v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->y:I

    move v1, v1

    .line 277062
    add-int/2addr v0, v1

    .line 277063
    const/4 v1, 0x0

    move v1, v1

    .line 277064
    add-int/2addr v0, v1

    return v0
.end method

.method public getSpaceLeft()I
    .locals 2

    .prologue
    .line 277055
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getPaddingLeft()I

    move-result v0

    .line 277056
    iget v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->v:I

    move v1, v1

    .line 277057
    add-int/2addr v0, v1

    .line 277058
    const/4 v1, 0x0

    move v1, v1

    .line 277059
    add-int/2addr v0, v1

    return v0
.end method

.method public getSpaceRight()I
    .locals 2

    .prologue
    .line 277076
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getPaddingRight()I

    move-result v0

    .line 277077
    iget v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->x:I

    move v1, v1

    .line 277078
    add-int/2addr v0, v1

    .line 277079
    const/4 v1, 0x0

    move v1, v1

    .line 277080
    add-int/2addr v0, v1

    return v0
.end method

.method public getSpaceTop()I
    .locals 2

    .prologue
    .line 277050
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getPaddingTop()I

    move-result v0

    .line 277051
    iget v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->w:I

    move v1, v1

    .line 277052
    add-int/2addr v0, v1

    .line 277053
    const/4 v1, 0x0

    move v1, v1

    .line 277054
    add-int/2addr v0, v1

    return v0
.end method

.method public getThumbnailGravity()I
    .locals 2

    .prologue
    .line 277046
    sget-object v0, LX/1aU;->a:[I

    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getThumbnailType()LX/1aV;

    move-result-object v1

    invoke-virtual {v1}, LX/1aV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 277047
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->n:I

    :goto_0
    return v0

    .line 277048
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1au;

    .line 277049
    iget v0, v0, LX/1au;->d:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getThumbnailPadding()I
    .locals 1

    .prologue
    .line 277045
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->o:I

    return v0
.end method

.method public getThumbnailView()Landroid/view/View;
    .locals 1

    .prologue
    .line 277044
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    return-object v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 277040
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->jumpDrawablesToCurrentState()V

    .line 277041
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 277042
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 277043
    :cond_0
    return-void
.end method

.method public final jx_()Z
    .locals 1

    .prologue
    .line 277039
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2c

    const v1, 0x66d1f3d1

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 277034
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 277035
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, p3

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p2, v2, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getChildMeasureSpec(III)I

    move-result v2

    .line 277036
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v3, p5

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p4, v3, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getChildMeasureSpec(III)I

    move-result v0

    .line 277037
    invoke-virtual {p1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 277038
    const/16 v0, 0x2d

    const v2, -0x16dffcd1

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 23

    .prologue
    .line 276946
    sub-int v13, p4, p2

    .line 276947
    sub-int v14, p5, p3

    .line 276948
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v15

    .line 276949
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceTop()I

    move-result v12

    .line 276950
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceBottom()I

    move-result v16

    .line 276951
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceLeft()I

    move-result v17

    .line 276952
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceRight()I

    move-result v18

    .line 276953
    const/4 v7, 0x0

    .line 276954
    const/4 v6, 0x0

    .line 276955
    const/4 v3, 0x0

    .line 276956
    const/4 v11, 0x0

    .line 276957
    const/4 v10, 0x0

    .line 276958
    const/4 v9, 0x0

    .line 276959
    const/4 v5, 0x0

    .line 276960
    const/4 v4, 0x0

    .line 276961
    const/4 v8, 0x0

    .line 276962
    invoke-direct/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getThumbnailType()LX/1aV;

    move-result-object v19

    .line 276963
    sget-object v2, LX/1aU;->a:[I

    invoke-virtual/range {v19 .. v19}, LX/1aV;->ordinal()I

    move-result v20

    aget v2, v2, v20

    packed-switch v2, :pswitch_data_0

    move/from16 v22, v4

    move v4, v5

    move v5, v3

    move/from16 v3, v22

    .line 276964
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->g()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 276965
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/1au;

    .line 276966
    iget v8, v2, LX/1au;->d:I

    if-gez v8, :cond_c

    const/16 v8, 0x11

    .line 276967
    :goto_1
    and-int/lit8 v11, v8, 0x70

    .line 276968
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    .line 276969
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 276970
    invoke-static {v2}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v8

    .line 276971
    const/16 v20, 0x30

    move/from16 v0, v20

    if-ne v11, v0, :cond_d

    .line 276972
    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v12

    move/from16 v22, v8

    move v8, v2

    move/from16 v2, v22

    .line 276973
    :goto_2
    move-object/from16 v0, p0

    iget v11, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b:I

    and-int/lit8 v11, v11, 0x70

    .line 276974
    const/16 v20, 0x10

    move/from16 v0, v20

    if-ne v11, v0, :cond_f

    .line 276975
    move-object/from16 v0, p0

    iget v11, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->t:I

    sub-int v11, v14, v11

    sub-int/2addr v11, v12

    sub-int v11, v11, v16

    div-int/lit8 v11, v11, 0x2

    add-int/2addr v11, v12

    .line 276976
    :goto_3
    if-eqz v15, :cond_11

    .line 276977
    add-int v4, v4, v17

    move-object/from16 v0, p0

    iput v4, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->q:I

    .line 276978
    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->q:I

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 276979
    sub-int v4, v13, v18

    sub-int/2addr v4, v10

    sub-int v2, v4, v2

    .line 276980
    :goto_4
    sget-object v4, LX/1aU;->a:[I

    invoke-virtual/range {v19 .. v19}, LX/1aV;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 276981
    :goto_5
    invoke-direct/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->e()V

    .line 276982
    invoke-direct/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->g()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 276983
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    add-int v5, v2, v10

    add-int v6, v8, v9

    invoke-virtual {v4, v2, v8, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 276984
    :cond_0
    if-eqz v15, :cond_12

    move v2, v3

    :goto_6
    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->s:I

    add-int/2addr v3, v4

    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->t:I

    add-int/2addr v4, v11

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11, v3, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a(IIII)V

    .line 276985
    return-void

    .line 276986
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/1au;

    .line 276987
    iget v3, v2, LX/1au;->d:I

    if-gez v3, :cond_2

    const/16 v3, 0x30

    .line 276988
    :goto_7
    and-int/lit8 v20, v3, 0x70

    .line 276989
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 276990
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 276991
    if-nez v7, :cond_3

    const/4 v3, 0x0

    .line 276992
    :goto_8
    invoke-static {v2}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v5

    .line 276993
    invoke-static {v2}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v4

    .line 276994
    const/16 v21, 0x10

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    .line 276995
    iget v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v20, v0

    iget v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    add-int v20, v20, v12

    add-int v20, v20, v16

    .line 276996
    sub-int v21, v14, v6

    sub-int v20, v21, v20

    div-int/lit8 v20, v20, 0x2

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int v2, v2, v20

    add-int/2addr v2, v12

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    move/from16 v22, v4

    move v4, v5

    move v5, v3

    move/from16 v3, v22

    goto/16 :goto_0

    .line 276997
    :cond_2
    iget v3, v2, LX/1au;->d:I

    goto :goto_7

    .line 276998
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->o:I

    add-int/2addr v3, v7

    goto :goto_8

    .line 276999
    :cond_4
    const/16 v21, 0x50

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    .line 277000
    sub-int v20, v14, v16

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v2, v20, v2

    sub-int/2addr v2, v6

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    move/from16 v22, v4

    move v4, v5

    move v5, v3

    move/from16 v3, v22

    goto/16 :goto_0

    .line 277001
    :cond_5
    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v12

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    move/from16 v22, v4

    move v4, v5

    move v5, v3

    move/from16 v3, v22

    .line 277002
    goto/16 :goto_0

    .line 277003
    :pswitch_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->n:I

    if-gez v2, :cond_6

    const/16 v2, 0x30

    .line 277004
    :goto_9
    and-int/lit8 v20, v2, 0x70

    .line 277005
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_14

    .line 277006
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->g:I

    if-gez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 277007
    :goto_a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->h:I

    if-gez v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    :goto_b
    move v6, v2

    move v2, v3

    .line 277008
    :goto_c
    if-nez v6, :cond_9

    const/4 v3, 0x0

    .line 277009
    :goto_d
    const/16 v7, 0x10

    move/from16 v0, v20

    if-ne v0, v7, :cond_a

    .line 277010
    add-int v7, v12, v16

    .line 277011
    sub-int v20, v14, v2

    sub-int v7, v20, v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    move v7, v6

    move v6, v2

    move/from16 v22, v3

    move v3, v4

    move v4, v5

    move/from16 v5, v22

    goto/16 :goto_0

    .line 277012
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->n:I

    goto :goto_9

    .line 277013
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->g:I

    goto :goto_a

    .line 277014
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->h:I

    goto :goto_b

    .line 277015
    :cond_9
    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->o:I

    add-int/2addr v3, v6

    goto :goto_d

    .line 277016
    :cond_a
    const/16 v7, 0x50

    move/from16 v0, v20

    if-ne v0, v7, :cond_b

    .line 277017
    sub-int v7, v14, v16

    sub-int/2addr v7, v2

    move-object/from16 v0, p0

    iput v7, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    move v7, v6

    move v6, v2

    move/from16 v22, v3

    move v3, v4

    move v4, v5

    move/from16 v5, v22

    goto/16 :goto_0

    .line 277018
    :cond_b
    move-object/from16 v0, p0

    iput v12, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    move v7, v6

    move v6, v2

    move/from16 v22, v3

    move v3, v4

    move v4, v5

    move/from16 v5, v22

    goto/16 :goto_0

    .line 277019
    :cond_c
    iget v8, v2, LX/1au;->d:I

    goto/16 :goto_1

    .line 277020
    :cond_d
    const/16 v20, 0x50

    move/from16 v0, v20

    if-ne v11, v0, :cond_e

    .line 277021
    sub-int v11, v14, v16

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v2, v11, v2

    sub-int/2addr v2, v9

    move/from16 v22, v8

    move v8, v2

    move/from16 v2, v22

    goto/16 :goto_2

    .line 277022
    :cond_e
    iget v11, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v20, v0

    add-int v11, v11, v20

    add-int/2addr v11, v12

    add-int v11, v11, v16

    .line 277023
    sub-int v20, v14, v9

    sub-int v11, v20, v11

    div-int/lit8 v11, v11, 0x2

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v11

    add-int/2addr v2, v12

    move/from16 v22, v8

    move v8, v2

    move/from16 v2, v22

    goto/16 :goto_2

    .line 277024
    :cond_f
    const/16 v20, 0x30

    move/from16 v0, v20

    if-ne v11, v0, :cond_10

    move v11, v12

    .line 277025
    goto/16 :goto_3

    .line 277026
    :cond_10
    sub-int v11, v14, v16

    move-object/from16 v0, p0

    iget v12, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->t:I

    sub-int/2addr v11, v12

    goto/16 :goto_3

    .line 277027
    :cond_11
    sub-int v12, v13, v18

    sub-int/2addr v12, v4

    sub-int/2addr v12, v7

    move-object/from16 v0, p0

    iput v12, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->q:I

    .line 277028
    sub-int v12, v13, v18

    sub-int v5, v12, v5

    sub-int v4, v5, v4

    sub-int v3, v4, v3

    .line 277029
    add-int v2, v2, v17

    goto/16 :goto_4

    .line 277030
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->q:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->q:I

    add-int/2addr v7, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    add-int/2addr v6, v13

    invoke-virtual {v4, v5, v12, v7, v6}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_5

    .line 277031
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->q:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->q:I

    add-int/2addr v7, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->r:I

    add-int/2addr v6, v13

    invoke-virtual {v4, v5, v12, v7, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto/16 :goto_5

    .line 277032
    :cond_12
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->s:I

    sub-int v2, v3, v2

    goto/16 :goto_6

    :cond_13
    move v2, v8

    move v8, v9

    move v9, v10

    move v10, v11

    goto/16 :goto_2

    :cond_14
    move v2, v6

    move v6, v7

    goto/16 :goto_c

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onMeasure(II)V
    .locals 16

    .prologue
    .line 277115
    const/4 v10, 0x0

    .line 277116
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->s:I

    .line 277117
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->t:I

    .line 277118
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceLeft()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceRight()I

    move-result v2

    add-int v4, v1, v2

    .line 277119
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceTop()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSpaceBottom()I

    move-result v2

    add-int v6, v1, v2

    .line 277120
    const/4 v5, 0x0

    .line 277121
    const/4 v3, 0x0

    .line 277122
    const/4 v7, 0x0

    .line 277123
    const/4 v13, 0x0

    .line 277124
    const/4 v2, 0x0

    .line 277125
    const/4 v1, 0x0

    .line 277126
    const/4 v12, 0x0

    .line 277127
    const/4 v11, 0x0

    .line 277128
    invoke-direct/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getThumbnailType()LX/1aV;

    move-result-object v14

    .line 277129
    sget-object v8, LX/1aU;->a:[I

    invoke-virtual {v14}, LX/1aV;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    :cond_0
    move v8, v2

    move v9, v7

    move v7, v1

    .line 277130
    :goto_0
    sget-object v1, LX/1aV;->NONE:LX/1aV;

    if-eq v14, v1, :cond_1

    .line 277131
    add-int v2, v4, v5

    .line 277132
    if-nez v8, :cond_7

    const/4 v1, 0x0

    :goto_1
    add-int v4, v2, v1

    .line 277133
    const/4 v1, 0x0

    add-int v2, v7, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v10, v1

    .line 277134
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->g()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 277135
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/1au;

    .line 277136
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int v12, v2, v3

    .line 277137
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v13, v2, v3

    .line 277138
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_8

    const/4 v1, 0x1

    move v11, v1

    .line 277139
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object/from16 v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 277140
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 277141
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 277142
    if-nez v3, :cond_9

    const/4 v1, 0x0

    :goto_3
    add-int/2addr v4, v1

    .line 277143
    add-int v1, v2, v13

    invoke-static {v10, v1}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 277144
    :goto_4
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v1, v4

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 277145
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    sub-int/2addr v5, v6

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v12

    invoke-static {v5, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 277146
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v5}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a(II)V

    .line 277147
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->s:I

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 277148
    move-object/from16 v0, p0

    iget v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->t:I

    invoke-static {v10, v1}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 277149
    if-eqz v9, :cond_2

    if-eq v7, v10, :cond_2

    .line 277150
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/1au;

    .line 277151
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    iget v9, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v9, v10, v9

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v1, v9, v1

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v7, v8, v1}, Landroid/view/View;->measure(II)V

    .line 277152
    :cond_2
    if-eqz v11, :cond_3

    if-eq v2, v10, :cond_3

    .line 277153
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/1au;

    .line 277154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v7, v10, v7

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v1, v7, v1

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/view/View;->measure(II)V

    .line 277155
    :cond_3
    add-int v1, v5, v4

    .line 277156
    add-int v2, v10, v6

    .line 277157
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSuggestedMinimumWidth()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 277158
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getSuggestedMinimumHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 277159
    move/from16 v0, p1

    invoke-static {v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->resolveSize(II)I

    move-result v1

    move/from16 v0, p2

    invoke-static {v2, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->resolveSize(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setMeasuredDimension(II)V

    .line 277160
    return-void

    .line 277161
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/1au;

    .line 277162
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int v9, v2, v3

    .line 277163
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v8, v2, v3

    .line 277164
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    const/4 v1, 0x1

    move v7, v1

    .line 277165
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    move-object/from16 v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 277166
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 277167
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move v3, v8

    move v5, v9

    :goto_6
    move v8, v2

    move v9, v7

    move v7, v1

    .line 277168
    goto/16 :goto_0

    .line 277169
    :cond_4
    const/4 v1, 0x0

    move v7, v1

    goto :goto_5

    .line 277170
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v8, :cond_0

    .line 277171
    move-object/from16 v0, p0

    iget v1, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->g:I

    if-gez v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 277172
    :goto_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->h:I

    if-gez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    move v15, v2

    move v2, v1

    move v1, v15

    goto :goto_6

    .line 277173
    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->g:I

    goto :goto_7

    .line 277174
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->h:I

    move v15, v2

    move v2, v1

    move v1, v15

    goto :goto_6

    .line 277175
    :cond_7
    move-object/from16 v0, p0

    iget v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->o:I

    add-int/2addr v1, v8

    goto/16 :goto_1

    .line 277176
    :cond_8
    const/4 v1, 0x0

    move v11, v1

    goto/16 :goto_2

    .line 277177
    :cond_9
    add-int v1, v3, v12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getAuxViewPadding()I

    move-result v5

    add-int/2addr v1, v5

    goto/16 :goto_3

    :cond_a
    move v2, v11

    move v3, v12

    move v11, v13

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public removeView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 277213
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a(Landroid/view/View;)V

    .line 277214
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->removeView(Landroid/view/View;)V

    .line 277215
    return-void
.end method

.method public removeViewInLayout(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 277210
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a(Landroid/view/View;)V

    .line 277211
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->removeViewInLayout(Landroid/view/View;)V

    .line 277212
    return-void
.end method

.method public setAuxViewPadding(I)V
    .locals 1

    .prologue
    .line 277206
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->p:I

    if-eq v0, p1, :cond_0

    .line 277207
    iput p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->p:I

    .line 277208
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->requestLayout()V

    .line 277209
    :cond_0
    return-void
.end method

.method public setBorderColor(I)V
    .locals 1

    .prologue
    .line 277201
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->z:I

    if-eq v0, p1, :cond_0

    .line 277202
    iput p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->z:I

    .line 277203
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 277204
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->invalidate()V

    .line 277205
    :cond_0
    return-void
.end method

.method public setClipBorderToPadding(Z)V
    .locals 1

    .prologue
    .line 277196
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->A:Z

    if-eq v0, p1, :cond_0

    .line 277197
    iput-boolean p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->A:Z

    .line 277198
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->requestLayout()V

    .line 277199
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->invalidate()V

    .line 277200
    :cond_0
    return-void
.end method

.method public setGravity(I)V
    .locals 1

    .prologue
    .line 277192
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b:I

    if-eq v0, p1, :cond_0

    .line 277193
    iput p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b:I

    .line 277194
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->requestLayout()V

    .line 277195
    :cond_0
    return-void
.end method

.method public setLayout(I)V
    .locals 2

    .prologue
    .line 277189
    if-lez p1, :cond_0

    .line 277190
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 277191
    :cond_0
    return-void
.end method

.method public setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 277178
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 277179
    :goto_0
    return-void

    .line 277180
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 277181
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 277182
    :cond_1
    iput-object p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    .line 277183
    if-eqz p1, :cond_2

    .line 277184
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 277185
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 277186
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 277187
    :cond_2
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->e()V

    .line 277188
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->invalidate()V

    goto :goto_0
.end method

.method public setOverlayGravity(I)V
    .locals 1

    .prologue
    .line 277072
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b:I

    if-eq v0, p1, :cond_0

    .line 277073
    iput p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b:I

    .line 277074
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->e()V

    .line 277075
    :cond_0
    return-void
.end method

.method public setOverlayResource(I)V
    .locals 1

    .prologue
    .line 277112
    if-lez p1, :cond_0

    .line 277113
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277114
    :cond_0
    return-void
.end method

.method public setShowAuxView(Z)V
    .locals 2

    .prologue
    .line 277108
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 277109
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 277110
    :cond_0
    return-void

    .line 277111
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setThumbnailGravity(I)V
    .locals 2

    .prologue
    .line 277101
    iput p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->n:I

    .line 277102
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getThumbnailType()LX/1aV;

    move-result-object v0

    sget-object v1, LX/1aV;->VIEW:LX/1aV;

    if-ne v0, v1, :cond_0

    .line 277103
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1au;

    .line 277104
    iput p1, v0, LX/1au;->d:I

    .line 277105
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->requestLayout()V

    .line 277106
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->invalidate()V

    .line 277107
    return-void
.end method

.method public setThumbnailPadding(I)V
    .locals 1

    .prologue
    .line 277096
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->o:I

    if-eq v0, p1, :cond_0

    .line 277097
    iput p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->o:I

    .line 277098
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->requestLayout()V

    .line 277099
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->invalidate()V

    .line 277100
    :cond_0
    return-void
.end method

.method public setThumbnailSize(I)V
    .locals 0

    .prologue
    .line 277094
    invoke-virtual {p0, p1, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->d(II)V

    .line 277095
    return-void
.end method

.method public setThumbnailView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 277082
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 277083
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->removeView(Landroid/view/View;)V

    .line 277084
    :cond_0
    if-nez p1, :cond_1

    .line 277085
    :goto_0
    return-void

    .line 277086
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 277087
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 277088
    check-cast v0, LX/1au;

    .line 277089
    :goto_1
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1au;->a:Z

    .line 277090
    const/4 v1, -0x1

    invoke-virtual {p0, p1, v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 277091
    :cond_2
    if-nez v0, :cond_3

    .line 277092
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b()LX/1au;

    move-result-object v0

    goto :goto_1

    .line 277093
    :cond_3
    new-instance v1, LX/1au;

    invoke-direct {v1, v0}, LX/1au;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 277081
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->j:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
