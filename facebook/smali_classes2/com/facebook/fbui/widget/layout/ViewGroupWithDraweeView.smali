.class public Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:Lcom/facebook/common/callercontext/CallerContext;

.field public b:LX/1Ad;

.field public c:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/graphics/drawable/Drawable;

.field public f:Landroid/view/View;

.field public g:I

.field public h:I

.field public i:Z

.field private j:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 277528
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 277529
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 277530
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 277531
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 277532
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 277533
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 277534
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->i:Z

    .line 277535
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    check-cast p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;

    invoke-static {p3}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object p2

    check-cast p2, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    const/16 v0, 0x509

    invoke-static {p3, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p3

    iput-object p2, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->c:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p3, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->d:LX/0Or;

    .line 277536
    return-void
.end method

.method private getThumbnailDraweeHolder()LX/1aX;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277537
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->j:LX/1aX;

    if-nez v0, :cond_0

    .line 277538
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->iW_()LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->j:LX/1aX;

    .line 277539
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->j:LX/1aX;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 277540
    const/4 v0, 0x0

    .line 277541
    if-eqz p1, :cond_2

    .line 277542
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-static {p2}, LX/1ny;->a(Ljava/lang/String;)LX/1ny;

    move-result-object v1

    .line 277543
    iput-object v1, v0, LX/1bX;->m:LX/1ny;

    .line 277544
    move-object v1, v0

    .line 277545
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->b:LX/1Ad;

    if-nez v0, :cond_0

    .line 277546
    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 277547
    if-nez v0, :cond_4

    .line 277548
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getCallerContext()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    move-object v2, v0

    .line 277549
    :goto_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->b:LX/1Ad;

    .line 277550
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->b:LX/1Ad;

    move-object v2, v0

    .line 277551
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-nez v0, :cond_3

    .line 277552
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getThumbnailDraweeHolder()LX/1aX;

    move-result-object v0

    .line 277553
    iget-object v3, v0, LX/1aX;->f:LX/1aZ;

    move-object v0, v3

    .line 277554
    invoke-virtual {v2, v0}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    .line 277555
    iget v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->g:I

    iget v3, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->h:I

    invoke-static {v0, v3}, LX/1o9;->a(II)LX/1o9;

    move-result-object v0

    .line 277556
    iput-object v0, v1, LX/1bX;->c:LX/1o9;

    .line 277557
    :cond_1
    :goto_1
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 277558
    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 277559
    :cond_2
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailController(LX/1aZ;)V

    .line 277560
    return-void

    .line 277561
    :cond_3
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/drawee/view/DraweeView;

    if-eqz v0, :cond_1

    .line 277562
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    .line 277563
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v3, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-static {v0, v3}, LX/1o9;->a(II)LX/1o9;

    move-result-object v0

    .line 277564
    iput-object v0, v1, LX/1bX;->c:LX/1o9;

    .line 277565
    goto :goto_1

    :cond_4
    move-object v2, v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 277566
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 277567
    return-void

    .line 277568
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277569
    const-string v0, "unknown"

    return-object v0
.end method

.method public final getCallerContext()Lcom/facebook/common/callercontext/CallerContext;
    .locals 3

    .prologue
    .line 277570
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v0, :cond_0

    .line 277571
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 277572
    :goto_0
    return-object v0

    .line 277573
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getAnalyticsTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getFeatureTag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 277574
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->a:Lcom/facebook/common/callercontext/CallerContext;

    goto :goto_0
.end method

.method public getController()LX/1aZ;
    .locals 1

    .prologue
    .line 277575
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getThumbnailDraweeHolder()LX/1aX;

    move-result-object v0

    .line 277576
    iget-object p0, v0, LX/1aX;->f:LX/1aZ;

    move-object v0, p0

    .line 277577
    return-object v0
.end method

.method public getFeatureTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277578
    const/4 v0, 0x0

    return-object v0
.end method

.method public getImageRequest()LX/1bf;
    .locals 1

    .prologue
    .line 277579
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->b:LX/1Ad;

    if-eqz v0, :cond_0

    .line 277580
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->b:LX/1Ad;

    .line 277581
    iget-object p0, v0, LX/1Ae;->f:Ljava/lang/Object;

    move-object v0, p0

    .line 277582
    check-cast v0, LX/1bf;

    .line 277583
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iW_()LX/1aX;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277525
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 277526
    new-instance v1, LX/1Uo;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 277527
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-static {v1, v0}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7eab6149

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 277522
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onAttachedToWindow()V

    .line 277523
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getThumbnailDraweeHolder()LX/1aX;

    move-result-object v1

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 277524
    const/16 v1, 0x2d

    const v2, 0x1bcb4d87

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x63614ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 277463
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onDetachedFromWindow()V

    .line 277464
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getThumbnailDraweeHolder()LX/1aX;

    move-result-object v1

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 277465
    const/16 v1, 0x2d

    const v2, -0xb8e5b56

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 277466
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onFinishTemporaryDetach()V

    .line 277467
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getThumbnailDraweeHolder()LX/1aX;

    move-result-object v0

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 277468
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 277469
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onStartTemporaryDetach()V

    .line 277470
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getThumbnailDraweeHolder()LX/1aX;

    move-result-object v0

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 277471
    return-void
.end method

.method public setShowThumbnail(Z)V
    .locals 2

    .prologue
    .line 277472
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-nez v0, :cond_0

    .line 277473
    iput-boolean p1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->i:Z

    .line 277474
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->requestLayout()V

    .line 277475
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->invalidate()V

    .line 277476
    :goto_0
    return-void

    .line 277477
    :cond_0
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public setThumbnailController(LX/1aZ;)V
    .locals 2

    .prologue
    .line 277478
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-nez v0, :cond_0

    .line 277479
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getThumbnailDraweeHolder()LX/1aX;

    move-result-object v0

    .line 277480
    invoke-virtual {v0, p1}, LX/1aX;->a(LX/1aZ;)V

    .line 277481
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277482
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->requestLayout()V

    .line 277483
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->invalidate()V

    .line 277484
    :goto_0
    return-void

    .line 277485
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/drawee/view/DraweeView;

    if-nez v0, :cond_1

    .line 277486
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Thumbnail view is not an DraweeView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277487
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    .line 277488
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_0
.end method

.method public setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 277489
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_1

    .line 277490
    :cond_0
    :goto_0
    return-void

    .line 277491
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 277492
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 277493
    :cond_2
    iput-object p1, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    .line 277494
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 277495
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 277496
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->requestLayout()V

    .line 277497
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->invalidate()V

    goto :goto_0
.end method

.method public setThumbnailPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 277498
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-nez v0, :cond_1

    .line 277499
    invoke-direct {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getThumbnailDraweeHolder()LX/1aX;

    move-result-object v0

    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 277500
    :goto_0
    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v0, p1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 277501
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-nez v0, :cond_0

    .line 277502
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->requestLayout()V

    .line 277503
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->invalidate()V

    .line 277504
    :cond_0
    return-void

    .line 277505
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/drawee/view/DraweeView;

    if-nez v0, :cond_2

    .line 277506
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Thumbnail view is not an DraweeView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277507
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    .line 277508
    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    goto :goto_0

    .line 277509
    :cond_3
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {p1, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_1
.end method

.method public setThumbnailPlaceholderResource(I)V
    .locals 1

    .prologue
    .line 277510
    if-lez p1, :cond_0

    .line 277511
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277512
    :cond_0
    return-void
.end method

.method public setThumbnailResource(I)V
    .locals 1

    .prologue
    .line 277513
    if-lez p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277514
    return-void

    .line 277515
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setThumbnailUri(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 277516
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 277517
    return-void
.end method

.method public setThumbnailUri(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 277518
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 277519
    return-void

    .line 277520
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 277521
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->i:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
