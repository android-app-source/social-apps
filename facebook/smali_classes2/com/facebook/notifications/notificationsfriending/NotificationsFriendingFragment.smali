.class public Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;
.super Lcom/facebook/base/fragment/FbListFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fk;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0o6;
.implements LX/0fu;
.implements LX/0fv;
.implements LX/0fw;


# annotations
.annotation runtime Lcom/facebook/navigation/annotations/NavigationComponent;
    scrollableContent = .subannotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
        id = "android.R.id.list"
        type = .enum LX/8Cz;->LIST_VIEW:LX/8Cz;
    .end subannotation
.end annotation


# static fields
.field public static final Z:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final i:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/3DA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0gh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/16I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/3TJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/3TK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/1qv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/1rn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/2ix;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0xW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/3D1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/2jO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/2Yg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/3Cm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/3TL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/2hd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/3Fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public V:LX/1Kt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public W:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public X:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aA:I

.field public aB:Z

.field public aC:I

.field public aD:I

.field public aE:Z

.field public aF:Ljava/lang/String;

.field public aG:I

.field public aH:I

.field public aa:LX/0Yb;

.field public ab:LX/0Yb;

.field public ac:LX/2hs;

.field private ad:LX/0Yb;

.field private ae:LX/3TX;

.field public af:LX/3Te;

.field public ag:LX/4nS;

.field public ah:Landroid/widget/TextView;

.field public ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public aj:LX/3TN;

.field private ak:LX/2Ip;

.field private al:LX/2hG;

.field private am:LX/2hO;

.field private an:LX/2hK;

.field private ao:LX/0dN;

.field private ap:LX/0xA;

.field public final aq:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field public ar:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public as:Landroid/view/animation/Animation;

.field public at:Landroid/view/animation/Animation;

.field private au:Z

.field public av:I

.field private aw:Z

.field private ax:I

.field public ay:J

.field private az:Z

.field public j:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/2hU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/2hS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/2dj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/2do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/2hW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/2A8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0id;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0xB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Db;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 139131
    const-class v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    sput-object v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Z:Ljava/lang/Class;

    .line 139132
    const-class v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    const-string v1, "notifications_friending"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 139133
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbListFragment;-><init>()V

    .line 139134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aq:Ljava/util/List;

    .line 139135
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ar:LX/0am;

    .line 139136
    iput v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->av:I

    .line 139137
    iput v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ax:I

    .line 139138
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ay:J

    .line 139139
    iput-boolean v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->az:Z

    .line 139140
    iput v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aA:I

    .line 139141
    iput-boolean v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aB:Z

    .line 139142
    iput v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aC:I

    .line 139143
    iput v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aD:I

    .line 139144
    iput-boolean v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aE:Z

    .line 139145
    iput v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aG:I

    .line 139146
    iput v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aH:I

    .line 139147
    return-void
.end method

.method public static D(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 2

    .prologue
    .line 139148
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ac:LX/2hs;

    new-instance v1, LX/Ivp;

    invoke-direct {v1, p0}, LX/Ivp;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    invoke-virtual {v0, v1}, LX/2hs;->d(LX/0TF;)V

    .line 139149
    return-void
.end method

.method public static E(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 2

    .prologue
    .line 139150
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ac:LX/2hs;

    new-instance v1, LX/Ivq;

    invoke-direct {v1, p0}, LX/Ivq;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    invoke-virtual {v0, v1}, LX/2hs;->b(LX/0TF;)V

    .line 139151
    return-void
.end method

.method public static G(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 1

    .prologue
    .line 139152
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->T:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 139153
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->s:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->j()V

    .line 139154
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->b$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V

    .line 139155
    return-void
.end method

.method public static H(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 3

    .prologue
    .line 139156
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v0}, LX/3Te;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-nez v0, :cond_1

    .line 139157
    :cond_0
    :goto_0
    return-void

    .line 139158
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    .line 139159
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v1}, LX/3Te;->h()I

    move-result v1

    invoke-interface {v0}, LX/0g8;->t()I

    move-result v2

    add-int/2addr v1, v2

    .line 139160
    invoke-interface {v0}, LX/0g8;->r()I

    move-result v0

    if-gt v1, v0, :cond_0

    .line 139161
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H:LX/1rn;

    invoke-virtual {v0}, LX/1rn;->a()V

    goto :goto_0
.end method

.method private J()V
    .locals 8

    .prologue
    .line 139017
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v0}, LX/3Te;->i()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 139018
    iget-object v1, v0, LX/3Te;->f:LX/3UE;

    invoke-virtual {v1}, LX/3UE;->o()Z

    move-result v1

    move v0, v1

    .line 139019
    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    .line 139020
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 139021
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->E:LX/3TK;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v1}, LX/3Te;->i()I

    move-result v1

    iget v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aG:I

    .line 139022
    const-string v3, "intermixed_tab_friend_request_impression"

    const-string v4, "shown_count"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "impression_count"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v4

    invoke-static {v0, v3, v4}, LX/3TK;->a(LX/3TK;Ljava/lang/String;Ljava/util/Map;)V

    .line 139023
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 139024
    iget-object v1, v0, LX/3Te;->g:LX/3UK;

    .line 139025
    iget-object v2, v1, LX/3UK;->h:LX/3UL;

    sget-object v0, LX/3UL;->SUCCESS:LX/3UL;

    invoke-virtual {v2, v0}, LX/3UL;->equals(Ljava/lang/Object;)Z

    move-result v2

    move v1, v2

    .line 139026
    move v0, v1

    .line 139027
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v0}, LX/3Te;->j()I

    move-result v0

    if-lez v0, :cond_3

    .line 139028
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->E:LX/3TK;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v1}, LX/3Te;->j()I

    move-result v1

    iget v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aH:I

    .line 139029
    const-string v3, "intermixed_tab_pymk_impression"

    const-string v4, "shown_count"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "impression_count"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v4

    invoke-static {v0, v3, v4}, LX/3TK;->a(LX/3TK;Ljava/lang/String;Ljava/util/Map;)V

    .line 139030
    :cond_3
    return-void

    .line 139031
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static K(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Z
    .locals 2

    .prologue
    .line 139162
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->L(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139163
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 139164
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static L(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Z
    .locals 3

    .prologue
    .line 139165
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    .line 139166
    iget-object v1, v0, LX/0xW;->a:LX/0ad;

    sget-short v2, LX/0xc;->k:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 139167
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static N(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 1

    .prologue
    .line 139168
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139169
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 139170
    :cond_0
    return-void
.end method

.method public static O(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 1

    .prologue
    .line 139171
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 139172
    return-void
.end method

.method private Q()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 139173
    iget v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aA:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aB:Z

    if-nez v0, :cond_1

    .line 139174
    :cond_0
    :goto_0
    return-void

    .line 139175
    :cond_1
    iget v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aA:I

    iget v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aD:I

    if-le v0, v2, :cond_2

    const/4 v0, 0x1

    .line 139176
    :goto_1
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "notification_scroll_depth_v2"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "notifications_friending"

    .line 139177
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 139178
    move-object v2, v2

    .line 139179
    const-string v3, "scroll_depth"

    iget v4, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aA:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "jewel_count"

    iget v4, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ax:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "visible_item_count"

    iget v4, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aD:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "passed_scroll_threshold"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 139180
    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k:LX/0Zb;

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 139181
    iput-boolean v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aB:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 139182
    goto :goto_1
.end method

.method public static R(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 3

    .prologue
    .line 139183
    new-instance v0, LX/62h;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62h;-><init>(Landroid/content/Context;)V

    .line 139184
    new-instance v1, LX/Ivt;

    invoke-direct {v1, p0}, LX/Ivt;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    invoke-virtual {v0, v1}, LX/62h;->setRetryClickListener(Landroid/view/View$OnClickListener;)V

    .line 139185
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->m:LX/2hU;

    const/16 v2, 0x2710

    invoke-virtual {v1, v0, v2}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ag:LX/4nS;

    .line 139186
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139187
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ag:LX/4nS;

    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v1

    .line 139188
    iput-object v1, v0, LX/4nS;->i:Landroid/view/View;

    .line 139189
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ag:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->a()V

    .line 139190
    return-void
.end method

.method private a(LX/2lq;LX/5P2;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 139322
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 139323
    const-string v1, "timeline_friend_request_ref"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 139324
    invoke-interface {p1}, LX/2lr;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/2lq;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/2lr;->b()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V

    .line 139325
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p:LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->bE:Ljava/lang/String;

    invoke-interface {p1}, LX/2lr;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 139326
    return-void
.end method

.method private static a(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;LX/0Sh;LX/0Zb;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/2hU;LX/0SG;LX/03V;LX/17W;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2hS;LX/2dj;LX/2do;LX/2hW;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/2A8;LX/0id;LX/0xB;LX/0Or;LX/3DA;LX/0gh;LX/16I;LX/3TJ;LX/3TK;LX/1qv;Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;LX/1rn;LX/2ix;LX/0xW;LX/3D1;LX/2jO;LX/2Yg;LX/3Cm;LX/3TL;Lcom/facebook/performancelogger/PerformanceLogger;LX/2hd;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/3Fx;LX/1Ck;LX/0SI;LX/1Kt;LX/0Xl;LX/0Or;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Zb;",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            "LX/2hU;",
            "LX/0SG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/17W;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2hS;",
            "LX/2dj;",
            "LX/2do;",
            "LX/2hW;",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            "LX/2A8;",
            "LX/0id;",
            "LX/0xB;",
            "LX/0Or",
            "<",
            "LX/1Db;",
            ">;",
            "Lcom/facebook/notifications/util/NotificationStoryLauncher;",
            "LX/0gh;",
            "LX/16I;",
            "LX/3TJ;",
            "LX/3TK;",
            "LX/1qv;",
            "Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;",
            "LX/1rn;",
            "LX/2ix;",
            "LX/0xW;",
            "LX/3D1;",
            "LX/2jO;",
            "LX/2Yg;",
            "LX/3Cm;",
            "LX/3TL;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/2hd;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/3Fx;",
            "LX/1Ck;",
            "LX/0SI;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139191
    iput-object p1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->j:LX/0Sh;

    iput-object p2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k:LX/0Zb;

    iput-object p3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->l:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p4, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->m:LX/2hU;

    iput-object p5, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->n:LX/0SG;

    iput-object p6, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->o:LX/03V;

    iput-object p7, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p:LX/17W;

    iput-object p8, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p9, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->r:LX/2hS;

    iput-object p10, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->s:LX/2dj;

    iput-object p11, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t:LX/2do;

    iput-object p12, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->u:LX/2hW;

    iput-object p13, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->v:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iput-object p14, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->w:LX/2A8;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->x:LX/0id;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->z:LX/0Or;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->A:LX/3DA;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->B:LX/0gh;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->C:LX/16I;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->D:LX/3TJ;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->E:LX/3TK;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->F:LX/1qv;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->G:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H:LX/1rn;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->I:LX/2ix;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->K:LX/3D1;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->L:LX/2jO;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->M:LX/2Yg;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->N:LX/3Cm;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->O:LX/3TL;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Q:LX/2hd;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->R:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->S:LX/3Fx;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->T:LX/1Ck;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->U:LX/0SI;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->V:LX/1Kt;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->W:LX/0Xl;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->X:LX/0Or;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Y:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 139192
    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->U:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 139193
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 139194
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v2

    invoke-virtual {v2}, LX/0k5;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v1

    .line 139195
    :goto_0
    if-nez v2, :cond_0

    invoke-static {v3}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 139196
    :cond_1
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v1, v0}, LX/3TN;->b(Z)V

    .line 139197
    :cond_2
    return-void

    :cond_3
    move v2, v0

    .line 139198
    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 46

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v44

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static/range {v44 .. v44}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static/range {v44 .. v44}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static/range {v44 .. v44}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v5

    check-cast v5, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static/range {v44 .. v44}, LX/2hU;->a(LX/0QB;)LX/2hU;

    move-result-object v6

    check-cast v6, LX/2hU;

    invoke-static/range {v44 .. v44}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static/range {v44 .. v44}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static/range {v44 .. v44}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v9

    check-cast v9, LX/17W;

    invoke-static/range {v44 .. v44}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v11, LX/2hS;

    move-object/from16 v0, v44

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/2hS;

    invoke-static/range {v44 .. v44}, LX/2dj;->a(LX/0QB;)LX/2dj;

    move-result-object v12

    check-cast v12, LX/2dj;

    invoke-static/range {v44 .. v44}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v13

    check-cast v13, LX/2do;

    invoke-static/range {v44 .. v44}, LX/2hW;->a(LX/0QB;)LX/2hW;

    move-result-object v14

    check-cast v14, LX/2hW;

    invoke-static/range {v44 .. v44}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v15

    check-cast v15, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static/range {v44 .. v44}, LX/2A8;->a(LX/0QB;)LX/2A8;

    move-result-object v16

    check-cast v16, LX/2A8;

    invoke-static/range {v44 .. v44}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v17

    check-cast v17, LX/0id;

    invoke-static/range {v44 .. v44}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v18

    check-cast v18, LX/0xB;

    const/16 v19, 0x6c9

    move-object/from16 v0, v44

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    invoke-static/range {v44 .. v44}, LX/3DA;->a(LX/0QB;)LX/3DA;

    move-result-object v20

    check-cast v20, LX/3DA;

    invoke-static/range {v44 .. v44}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v21

    check-cast v21, LX/0gh;

    invoke-static/range {v44 .. v44}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v22

    check-cast v22, LX/16I;

    const-class v23, LX/3TJ;

    move-object/from16 v0, v44

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/3TJ;

    invoke-static/range {v44 .. v44}, LX/3TK;->a(LX/0QB;)LX/3TK;

    move-result-object v24

    check-cast v24, LX/3TK;

    invoke-static/range {v44 .. v44}, LX/1qv;->a(LX/0QB;)LX/1qv;

    move-result-object v25

    check-cast v25, LX/1qv;

    invoke-static/range {v44 .. v44}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a(LX/0QB;)Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    move-result-object v26

    check-cast v26, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    invoke-static/range {v44 .. v44}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v27

    check-cast v27, LX/1rn;

    invoke-static/range {v44 .. v44}, LX/2ix;->a(LX/0QB;)LX/2ix;

    move-result-object v28

    check-cast v28, LX/2ix;

    invoke-static/range {v44 .. v44}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v29

    check-cast v29, LX/0xW;

    invoke-static/range {v44 .. v44}, LX/3D1;->a(LX/0QB;)LX/3D1;

    move-result-object v30

    check-cast v30, LX/3D1;

    invoke-static/range {v44 .. v44}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v31

    check-cast v31, LX/2jO;

    invoke-static/range {v44 .. v44}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v32

    check-cast v32, LX/2Yg;

    invoke-static/range {v44 .. v44}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v33

    check-cast v33, LX/3Cm;

    invoke-static/range {v44 .. v44}, LX/3TL;->a(LX/0QB;)LX/3TL;

    move-result-object v34

    check-cast v34, LX/3TL;

    invoke-static/range {v44 .. v44}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v35

    check-cast v35, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {v44 .. v44}, LX/2hd;->a(LX/0QB;)LX/2hd;

    move-result-object v36

    check-cast v36, LX/2hd;

    invoke-static/range {v44 .. v44}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v37

    check-cast v37, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v44 .. v44}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v38

    check-cast v38, LX/3Fx;

    invoke-static/range {v44 .. v44}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v39

    check-cast v39, LX/1Ck;

    invoke-static/range {v44 .. v44}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v40

    check-cast v40, LX/0SI;

    invoke-static/range {v44 .. v44}, LX/23N;->a(LX/0QB;)LX/23N;

    move-result-object v41

    check-cast v41, LX/1Kt;

    invoke-static/range {v44 .. v44}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v42

    check-cast v42, LX/0Xl;

    const/16 v43, 0x15e7

    move-object/from16 v0, v44

    move/from16 v1, v43

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v43

    const/16 v45, 0x64

    invoke-static/range {v44 .. v45}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v44

    invoke-static/range {v2 .. v44}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;LX/0Sh;LX/0Zb;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/2hU;LX/0SG;LX/03V;LX/17W;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2hS;LX/2dj;LX/2do;LX/2hW;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/2A8;LX/0id;LX/0xB;LX/0Or;LX/3DA;LX/0gh;LX/16I;LX/3TJ;LX/3TK;LX/1qv;Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;LX/1rn;LX/2ix;LX/0xW;LX/3D1;LX/2jO;LX/2Yg;LX/3Cm;LX/3TL;Lcom/facebook/performancelogger/PerformanceLogger;LX/2hd;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/3Fx;LX/1Ck;LX/0SI;LX/1Kt;LX/0Xl;LX/0Or;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;LX/2ub;)V
    .locals 0

    .prologue
    .line 139199
    invoke-direct {p0, p1}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->b(LX/2ub;)V

    .line 139200
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->D(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 139201
    return-void
.end method

.method public static a$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Lcom/facebook/feed/rows/core/props/FeedProps;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 139202
    if-nez p1, :cond_0

    .line 139203
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0030

    const-string v2, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 139204
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->x:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    .line 139205
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->o:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Z:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_null_notif_story"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Null notif story in adapter"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 139206
    :goto_0
    return-void

    .line 139207
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 139208
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 139209
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v3, LX/0hM;->c:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 139210
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->B:LX/0gh;

    const-string v3, "tap_notification_jewel"

    invoke-virtual {v1, v3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 139211
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    .line 139212
    :goto_1
    new-instance v3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v3}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v4

    .line 139213
    iput-object v4, v3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    .line 139214
    move-object v3, v3

    .line 139215
    iput-boolean v2, v3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    .line 139216
    move-object v3, v3

    .line 139217
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v3

    .line 139218
    iput-boolean v1, v3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    .line 139219
    move-object v3, v3

    .line 139220
    iput p2, v3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->u:I

    .line 139221
    move-object v3, v3

    .line 139222
    iget-object v4, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->M:LX/2Yg;

    invoke-virtual {v4, v3}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 139223
    if-eqz v1, :cond_2

    .line 139224
    iput-boolean v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->au:Z

    .line 139225
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H:LX/1rn;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iget-object v4, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->U:LX/0SI;

    invoke-interface {v4}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/1rn;->a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 139226
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 139227
    iget-object v2, v1, LX/3Te;->e:LX/3Tk;

    .line 139228
    invoke-virtual {v2}, LX/3Tk;->g()I

    move-result v3

    if-lt p2, v3, :cond_6

    .line 139229
    :cond_1
    :goto_2
    const/16 v1, 0x1e

    if-ge p2, v1, :cond_5

    .line 139230
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->v:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    .line 139231
    :cond_2
    :goto_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 139232
    if-nez v1, :cond_7

    .line 139233
    :cond_3
    :goto_4
    goto/16 :goto_0

    .line 139234
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 139235
    :cond_5
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->v:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    goto :goto_3

    .line 139236
    :cond_6
    invoke-static {v2, p2}, LX/3Tk;->d(LX/3Tk;I)LX/2nq;

    move-result-object v3

    .line 139237
    if-eqz v3, :cond_1

    .line 139238
    invoke-static {v3}, LX/BDX;->a(LX/2nq;)LX/2nq;

    move-result-object v3

    .line 139239
    iget-object v1, v2, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v1, p2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 139240
    iget-object v3, v2, LX/3Tk;->i:LX/3Tg;

    invoke-interface {v3}, LX/3Tg;->e()V

    goto :goto_2

    .line 139241
    :cond_7
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 139242
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 139243
    invoke-static {v0}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 139244
    if-eqz v2, :cond_8

    iget-object v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p:LX/17W;

    invoke-virtual {v3, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 139245
    :goto_5
    if-nez v1, :cond_3

    .line 139246
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0030

    const-string v3, "NNF_PermalinkNotificationLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 139247
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->x:LX/0id;

    invoke-virtual {v1}, LX/0id;->a()V

    .line 139248
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->o:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Z:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_launch_failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not launch notification story "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 139249
    :cond_8
    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->A:LX/3DA;

    invoke-virtual {v2, v1, p1, p2}, LX/3DA;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;I)Z

    move-result v1

    goto :goto_5
.end method

.method private b(LX/2ub;)V
    .locals 6

    .prologue
    .line 139250
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ag:LX/4nS;

    if-eqz v0, :cond_0

    .line 139251
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ag:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    .line 139252
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->U:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-nez v0, :cond_1

    .line 139253
    :goto_0
    return-void

    .line 139254
    :cond_1
    sget-object v0, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    if-ne p1, v0, :cond_2

    .line 139255
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->R:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x910005

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 139256
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->L:LX/2jO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2jO;->a(LX/DqC;)V

    .line 139257
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->G:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->U:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 139258
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->j:LX/0Sh;

    new-instance v2, LX/3cv;

    iget-object v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->U:LX/0SI;

    invoke-interface {v3}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 139259
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 139260
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LX/3cv;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Ljava/lang/Long;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 139261
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/FriendRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 139262
    invoke-direct {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J()V

    .line 139263
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->n:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aF:Ljava/lang/String;

    .line 139264
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aF:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Te;->b(Ljava/lang/String;)V

    .line 139265
    iput v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aG:I

    .line 139266
    iput v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aH:I

    .line 139267
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 139268
    iget-object v1, v0, LX/3Te;->f:LX/3UE;

    .line 139269
    sget-object v0, LX/3UG;->LOADING:LX/3UG;

    iput-object v0, v1, LX/3UE;->l:LX/3UG;

    .line 139270
    iget-object v0, v1, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 139271
    iget-object v0, v1, LX/3UE;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 139272
    iget-object v0, v1, LX/3UE;->i:LX/3Tg;

    invoke-interface {v0}, LX/3Tg;->e()V

    .line 139273
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 139274
    iget-object v3, v0, LX/3Te;->f:LX/3UE;

    .line 139275
    sget-object v4, LX/3UG;->SUCCESS:LX/3UG;

    iput-object v4, v3, LX/3UE;->l:LX/3UG;

    .line 139276
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friends/model/FriendRequest;

    .line 139277
    iget-object v6, v3, LX/3UE;->k:Ljava/util/Set;

    invoke-virtual {v4}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 139278
    iget-object v6, v3, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139279
    iget-object v6, v3, LX/3UE;->k:Ljava/util/Set;

    invoke-virtual {v4}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139280
    :cond_1
    iget-object v4, v3, LX/3UE;->i:LX/3Tg;

    invoke-interface {v4}, LX/3Tg;->e()V

    .line 139281
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 139282
    iget-object v1, v0, LX/3Te;->g:LX/3UK;

    .line 139283
    sget-object v0, LX/3UL;->LOADING:LX/3UL;

    iput-object v0, v1, LX/3UK;->h:LX/3UL;

    .line 139284
    iget-object v0, v1, LX/3UK;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 139285
    iget-object v0, v1, LX/3UK;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 139286
    iget-object v0, v1, LX/3UK;->e:LX/3Tg;

    invoke-interface {v0}, LX/3Tg;->e()V

    .line 139287
    return-void
.end method

.method public static b$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 139288
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->T:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139289
    :cond_0
    :goto_0
    return-void

    .line 139290
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->r()Z

    move-result v5

    .line 139291
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v0}, LX/3Te;->k()Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz v5, :cond_2

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 139292
    iget-object v3, v0, LX/3Te;->g:LX/3UK;

    invoke-virtual {v3}, LX/3UK;->m()Z

    move-result v3

    move v0, v3

    .line 139293
    if-eqz v0, :cond_4

    :cond_2
    move v0, v1

    .line 139294
    :goto_1
    iget-object v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->s:LX/2dj;

    invoke-virtual {v3}, LX/2dj;->g()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v3}, LX/3Te;->j()I

    move-result v3

    if-lez v3, :cond_5

    move v3, v1

    .line 139295
    :goto_2
    iget-object v4, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->s:LX/2dj;

    invoke-virtual {v4}, LX/2dj;->e()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v4}, LX/3Te;->l()Z

    move-result v4

    if-nez v4, :cond_6

    move v4, v1

    .line 139296
    :goto_3
    iget-object v6, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->s:LX/2dj;

    invoke-virtual {v6}, LX/2dj;->d()Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 139297
    iget-object v7, v6, LX/3Te;->g:LX/3UK;

    .line 139298
    iget-object v8, v7, LX/3UK;->h:LX/3UL;

    sget-object v6, LX/3UL;->FAILURE:LX/3UL;

    invoke-virtual {v8, v6}, LX/3UL;->equals(Ljava/lang/Object;)Z

    move-result v8

    move v7, v8

    .line 139299
    move v6, v7

    .line 139300
    if-nez v6, :cond_7

    .line 139301
    :goto_4
    if-nez v5, :cond_8

    if-nez p1, :cond_3

    if-eqz v4, :cond_8

    .line 139302
    :cond_3
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->N(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 139303
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->T:LX/1Ck;

    const-string v1, "FETCH_FRIEND_REQUESTS_TASK"

    new-instance v2, LX/3cY;

    invoke-direct {v2, p0}, LX/3cY;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    new-instance v3, LX/3cZ;

    invoke-direct {v3, p0}, LX/3cZ;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 139304
    goto :goto_0

    :cond_4
    move v0, v2

    .line 139305
    goto :goto_1

    :cond_5
    move v3, v2

    .line 139306
    goto :goto_2

    :cond_6
    move v4, v2

    .line 139307
    goto :goto_3

    :cond_7
    move v1, v2

    .line 139308
    goto :goto_4

    .line 139309
    :cond_8
    if-nez p1, :cond_9

    if-eqz v1, :cond_a

    if-nez v0, :cond_a

    if-nez v3, :cond_a

    .line 139310
    :cond_9
    invoke-static {p0, p1}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->c(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V

    goto/16 :goto_0

    .line 139311
    :cond_a
    if-eqz v3, :cond_0

    .line 139312
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->O(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    goto/16 :goto_0
.end method

.method public static c(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V
    .locals 4

    .prologue
    .line 139313
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->N(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 139314
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->T:LX/1Ck;

    const-string v1, "FETCH_PYMK_TASK"

    new-instance v2, LX/3da;

    invoke-direct {v2, p0}, LX/3da;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    new-instance v3, LX/3db;

    invoke-direct {v3, p0, p1}, LX/3db;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 139315
    return-void
.end method

.method public static p$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Lcom/facebook/widget/FbSwipeRefreshLayout;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 139316
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getSwipeRefreshLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getSwipeRefreshLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 3

    .prologue
    .line 139317
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-nez v0, :cond_1

    .line 139318
    :cond_0
    :goto_0
    return-void

    .line 139319
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H:LX/1rn;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->U:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1rn;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 139320
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139321
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0xB;->a(LX/12j;I)V

    goto :goto_0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 139118
    iget-boolean v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->au:Z

    if-eqz v0, :cond_0

    .line 139119
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    const v1, 0x45e3a6b7

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 139120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->au:Z

    .line 139121
    :cond_0
    return-void
.end method

.method public static u$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 3

    .prologue
    .line 139122
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H:LX/1rn;

    invoke-virtual {v0}, LX/1rn;->c()V

    .line 139123
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 139124
    iget-object v1, v0, LX/3Te;->e:LX/3Tk;

    .line 139125
    sget-object v0, LX/3dV;->FAILURE:LX/3dV;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, v1, LX/3Tk;->o:LX/0am;

    .line 139126
    iget-object v0, v1, LX/3Tk;->i:LX/3Tg;

    invoke-interface {v0}, LX/3Tg;->e()V

    .line 139127
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V

    .line 139128
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->v$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 139129
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->R:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x910005

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 139130
    return-void
.end method

.method public static v$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 2

    .prologue
    .line 138755
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    if-eqz v0, :cond_1

    .line 138756
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getSwipeRefreshLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138757
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getSwipeRefreshLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 138758
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getRefreshableContainerLike()LX/62k;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 138759
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getRefreshableContainerLike()LX/62k;

    move-result-object v0

    invoke-interface {v0}, LX/62k;->f()V

    .line 138760
    :cond_1
    return-void
.end method

.method public static w(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 7

    .prologue
    .line 138878
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    .line 138879
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 138880
    :cond_0
    :goto_0
    return-void

    .line 138881
    :cond_1
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    sget-object v2, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v1, v2}, LX/0xB;->a(LX/12j;)I

    move-result v1

    .line 138882
    if-eqz v1, :cond_2

    iget-boolean v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aE:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v2}, LX/3Te;->n()Z

    move-result v2

    if-nez v2, :cond_3

    .line 138883
    :cond_2
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->x(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    goto :goto_0

    .line 138884
    :cond_3
    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v2}, LX/3Te;->h()I

    move-result v2

    invoke-interface {v0}, LX/0g8;->t()I

    move-result v3

    add-int/2addr v2, v3

    .line 138885
    invoke-interface {v0}, LX/0g8;->r()I

    move-result v0

    if-le v2, v0, :cond_0

    .line 138886
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0180

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138887
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 138888
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->as:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public static x(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 2

    .prologue
    .line 138875
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->at:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->at:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138876
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->at:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 138877
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138874
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 9

    .prologue
    const v3, 0xa0030

    .line 138835
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eq p1, v0, :cond_0

    .line 138836
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v0

    .line 138837
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    if-nez v0, :cond_1

    .line 138838
    :cond_0
    :goto_0
    return-void

    .line 138839
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v0, p2}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    .line 138840
    instance-of v0, v7, LX/2lq;

    if-eqz v0, :cond_3

    .line 138841
    instance-of v0, v7, Lcom/facebook/friends/model/PersonYouMayKnow;

    if-eqz v0, :cond_2

    .line 138842
    sget-object v8, LX/5P2;->PYMK_JEWEL:LX/5P2;

    move-object v0, v7

    .line 138843
    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 138844
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Q:LX/2hd;

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aF:Ljava/lang/String;

    .line 138845
    iget-object v3, v0, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    move-object v3, v3

    .line 138846
    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    sget-object v6, LX/2hC;->JEWEL:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->d(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    move-object v0, v8

    .line 138847
    :goto_1
    check-cast v7, LX/2lq;

    invoke-direct {p0, v7, v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a(LX/2lq;LX/5P2;)V

    goto :goto_0

    .line 138848
    :cond_2
    sget-object v0, LX/5P2;->JEWEL:LX/5P2;

    goto :goto_1

    .line 138849
    :cond_3
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eq p1, v0, :cond_0

    .line 138850
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v0

    .line 138851
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/facebook/notifications/widget/PostFeedbackView;

    if-nez v0, :cond_0

    instance-of v0, p1, LX/3Cv;

    if-eqz v0, :cond_4

    check-cast p1, LX/3Cv;

    .line 138852
    iget-object v0, p1, LX/3Cv;->f:LX/3Cw;

    move-object v0, v0

    .line 138853
    sget-object v1, LX/3Cw;->NOTIFICATION:LX/3Cw;

    if-ne v0, v1, :cond_0

    .line 138854
    :cond_4
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->x:LX/0id;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->a(Ljava/lang/String;)V

    .line 138855
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v3, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 138856
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_PermalinkNotificationLoad"

    const-string v2, "NotificationsFriendingFragment"

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->e(ILjava/lang/String;Ljava/lang/String;)V

    .line 138857
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0, p2}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v0

    .line 138858
    instance-of v1, v0, LX/2nq;

    if-eqz v1, :cond_0

    .line 138859
    check-cast v0, LX/2nq;

    .line 138860
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 138861
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->v:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 138862
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v2

    .line 138863
    if-nez v2, :cond_6

    :goto_2
    move v2, p2

    .line 138864
    iget-object v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 138865
    invoke-virtual {v3, v2}, LX/3Tf;->d(I)[I

    move-result-object v4

    .line 138866
    iget-object v5, v3, LX/3Te;->h:Ljava/util/List;

    const/4 v6, 0x0

    aget v6, v4, v6

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    iget-object v6, v3, LX/3Te;->e:LX/3Tk;

    if-ne v5, v6, :cond_7

    .line 138867
    const/4 v5, 0x1

    aget v4, v4, v5

    .line 138868
    :goto_3
    move v2, v4

    .line 138869
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 138870
    if-nez v1, :cond_5

    .line 138871
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H:LX/1rn;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/1rn;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 138872
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->j:LX/0Sh;

    new-instance v3, LX/Ivv;

    invoke-direct {v3, p0, v2}, LX/Ivv;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;I)V

    invoke-virtual {v1, v0, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_0

    .line 138873
    :cond_5
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    goto/16 :goto_0

    :cond_6
    invoke-interface {v2}, LX/0g8;->t()I

    move-result v2

    sub-int/2addr p2, v2

    goto :goto_2

    :cond_7
    const/4 v4, -0x1

    goto :goto_3
.end method

.method public final a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138794
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 138795
    if-eqz p1, :cond_1

    .line 138796
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 138797
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 138798
    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aq:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 138799
    :cond_1
    const/4 v0, 0x0

    .line 138800
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aq:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 138801
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 138802
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0, p1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 138803
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 138804
    goto :goto_1

    .line 138805
    :cond_2
    move v0, v1

    .line 138806
    iput v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aC:I

    .line 138807
    iget v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aC:I

    if-lez v0, :cond_3

    .line 138808
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->K:LX/3D1;

    invoke-virtual {v0}, LX/3D1;->a()V

    .line 138809
    const-string v0, "notification_auto_update_list_update"

    const-string v1, "new_notifications_notif_fragment"

    .line 138810
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "notifications_friending"

    .line 138811
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 138812
    move-object v2, v2

    .line 138813
    const-string p1, "reason"

    invoke-virtual {v2, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 138814
    iget-object p1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k:LX/0Zb;

    invoke-interface {p1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 138815
    :cond_3
    const/4 v8, 0x0

    .line 138816
    iput v8, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aC:I

    .line 138817
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v3

    .line 138818
    new-instance v4, LX/2i4;

    invoke-direct {v4}, LX/2i4;-><init>()V

    .line 138819
    if-eqz v3, :cond_4

    .line 138820
    invoke-interface {v3, v4}, LX/0g8;->a(LX/2i4;)V

    .line 138821
    :cond_4
    iget-object v5, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    iget-object v6, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aq:Ljava/util/List;

    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->K(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Z

    move-result v7

    invoke-virtual {v5, v6, v7}, LX/3Te;->b(Ljava/util/List;Z)V

    .line 138822
    iget-object v5, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->T:LX/1Ck;

    invoke-virtual {v5}, LX/1Ck;->a()Z

    move-result v5

    if-nez v5, :cond_5

    .line 138823
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->O(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 138824
    :cond_5
    if-eqz v3, :cond_6

    .line 138825
    invoke-interface {v3, v4}, LX/0g8;->b(LX/2i4;)V

    .line 138826
    :cond_6
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 138827
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 138828
    :cond_7
    iget-object v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->R:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x910005

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 138829
    invoke-static {p0, v8}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V

    .line 138830
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->s(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 138831
    iget-object v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->n:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ay:J

    .line 138832
    iget-object v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->s:LX/2dj;

    invoke-virtual {v3}, LX/2dj;->e()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 138833
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->w(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 138834
    :cond_8
    return-void

    :cond_9
    move v0, v1

    goto/16 :goto_2
.end method

.method public final b()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138793
    const-string v0, "notifications_count"

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    sget-object v2, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v1, v2}, LX/0xB;->a(LX/12j;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "friend_requests_count"

    iget-object v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    sget-object v4, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v3, v4}, LX/0xB;->a(LX/12j;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 138791
    sget-object v0, LX/2ub;->FRAGMENT_LOADED:LX/2ub;

    invoke-direct {p0, v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->b(LX/2ub;)V

    .line 138792
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 138788
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138789
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->d(I)V

    .line 138790
    :cond_0
    return-void
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138763
    iget-boolean v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aw:Z

    if-nez v0, :cond_0

    .line 138764
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 138765
    :goto_0
    return-object v0

    .line 138766
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 138767
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 138768
    iget-object v1, v0, LX/3Te;->e:LX/3Tk;

    invoke-virtual {v1}, LX/3Tk;->g()I

    move-result v1

    move v0, v1

    .line 138769
    const-string v1, "\nLoaded Notifications: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 138770
    const-string v1, "\nLast Load Time: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ay:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 138771
    iget v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aD:I

    if-ltz v1, :cond_5

    iget v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->av:I

    if-ltz v1, :cond_5

    .line 138772
    const-string v1, "\nVisible Notification Ids: [\n"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138773
    iget v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->av:I

    iget v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aD:I

    add-int/2addr v1, v2

    .line 138774
    if-le v1, v0, :cond_1

    move v1, v0

    .line 138775
    :cond_1
    iget v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->av:I

    move v2, v0

    :goto_1
    if-ge v2, v1, :cond_4

    .line 138776
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v0, v2}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 138777
    instance-of v4, v0, LX/2nq;

    if-eqz v4, :cond_2

    .line 138778
    check-cast v0, LX/2nq;

    .line 138779
    if-eqz v0, :cond_3

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 138780
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138781
    :goto_2
    const-string v0, "\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138782
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 138783
    :cond_3
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 138784
    :cond_4
    const-string v0, "]\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138785
    :cond_5
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->F:LX/1qv;

    if-eqz v0, :cond_6

    .line 138786
    const-string v0, "Last Updated Time: "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->F:LX/1qv;

    invoke-virtual {v1}, LX/1qv;->a()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138787
    :cond_6
    const-string v0, "NotificationsFriendingFragment"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 138762
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 138761
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    goto :goto_0
.end method

.method public final kn_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 138750
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138751
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 138752
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, LX/3Te;->b(Ljava/util/List;Z)V

    .line 138753
    invoke-static {p0, v2}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V

    .line 138754
    return-void
.end method

.method public final ko_()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 138889
    iget-boolean v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->az:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->O:LX/3TL;

    .line 138890
    iget-boolean v1, v0, LX/3TL;->j:Z

    move v0, v1

    .line 138891
    if-eqz v0, :cond_0

    .line 138892
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x910009

    const-string v2, "NotifListLoadTimeCold"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 138893
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x91000a

    const-string v2, "NotifListLoadTimeWarm"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 138894
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    const-string v3, "load_type"

    const-string v4, "hot"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 138895
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 138896
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    const-string v3, "load_type"

    const-string v4, "hot"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 138897
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 138898
    iput-boolean v5, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->az:Z

    .line 138899
    :cond_0
    return v5
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x44886640

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 138900
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 138901
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->C:LX/16I;

    sget-object v2, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v3, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment$1;

    invoke-direct {v3, p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment$1;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    invoke-virtual {v1, v2, v3}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ad:LX/0Yb;

    .line 138902
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->D:LX/3TJ;

    .line 138903
    new-instance v4, LX/3Te;

    invoke-static {v1}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v6

    check-cast v6, LX/23P;

    invoke-static {v1}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v7

    check-cast v7, LX/0xW;

    const-class v5, LX/3Th;

    invoke-interface {v1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/3Th;

    const-class v5, LX/3Ti;

    invoke-interface {v1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/3Ti;

    const-class v5, LX/3Tj;

    invoke-interface {v1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/3Tj;

    move-object v5, p0

    invoke-direct/range {v4 .. v10}, LX/3Te;-><init>(Landroid/support/v4/app/Fragment;LX/23P;LX/0xW;LX/3Th;LX/3Ti;LX/3Tj;)V

    .line 138904
    move-object v1, v4

    .line 138905
    iput-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 138906
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 138907
    iget-object v2, v1, LX/3Te;->g:LX/3UK;

    .line 138908
    iget-object v1, v2, LX/3UK;->c:LX/2iS;

    invoke-virtual {v1}, LX/2iS;->a()V

    .line 138909
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aF:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/3Te;->b(Ljava/lang/String;)V

    .line 138910
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {p0, v1}, Landroid/support/v4/app/ListFragment;->a(Landroid/widget/ListAdapter;)V

    .line 138911
    new-instance v1, LX/3cX;

    invoke-direct {v1, p0}, LX/3cX;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    move-object v1, v1

    .line 138912
    iput-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ap:LX/0xA;

    .line 138913
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ap:LX/0xA;

    invoke-virtual {v1, v2}, LX/0xB;->a(LX/0xA;)V

    .line 138914
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    sget-object v2, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v1, v2}, LX/0xB;->a(LX/12j;)I

    move-result v1

    if-lez v1, :cond_0

    .line 138915
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->D(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 138916
    :goto_0
    const v1, -0x364d0edb

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 138917
    :cond_0
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->G(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 138918
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 138919
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    invoke-static {v0}, LX/62G;->a(LX/0g8;)V

    .line 138920
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    if-eqz v0, :cond_0

    .line 138921
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->a()V

    .line 138922
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x2

    const/16 v0, 0x2a

    const v1, 0x7c634cad

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 138923
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 138924
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 138925
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->r:LX/2hS;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->T:LX/1Ck;

    invoke-virtual {v0, v1}, LX/2hS;->a(LX/1Ck;)LX/2hs;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ac:LX/2hs;

    .line 138926
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->O:LX/3TL;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->U:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->w:LX/2A8;

    iget-object v5, v1, LX/2A8;->b:Landroid/net/Uri;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    .line 138927
    iget-object v6, v1, LX/0xW;->a:LX/0ad;

    sget v7, LX/0xc;->j:I

    const/16 p1, 0xa

    invoke-interface {v6, v7, p1}, LX/0ad;->a(II)I

    move-result v6

    move v6, v6

    .line 138928
    const/4 v7, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, LX/3TL;->a(LX/0o6;LX/0k5;Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/net/Uri;ILjava/lang/String;)V

    .line 138929
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->W:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    .line 138930
    new-instance v2, LX/2s7;

    invoke-direct {v2, p0}, LX/2s7;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    move-object v2, v2

    .line 138931
    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aa:LX/0Yb;

    .line 138932
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aa:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 138933
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->W:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    .line 138934
    new-instance v2, LX/3TM;

    invoke-direct {v2, p0}, LX/3TM;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    move-object v2, v2

    .line 138935
    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ab:LX/0Yb;

    .line 138936
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ab:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 138937
    const/16 v0, 0x2b

    const v1, -0x43ae6e27

    invoke-static {v9, v0, v1, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x600ecea6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 138938
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "ANDROID_NOTIFICATIONS_FRIENDING"

    invoke-static {v0, v1}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v0

    .line 138939
    new-instance v1, LX/3TN;

    invoke-direct {v1, v0}, LX/3TN;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    .line 138940
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0, v7}, LX/3TN;->a(Z)V

    .line 138941
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0, v7}, LX/3TN;->c(Z)V

    .line 138942
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    invoke-virtual {v0}, LX/3TN;->getListView()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Db;

    .line 138943
    new-instance v2, LX/3TS;

    invoke-direct {v2, v0}, LX/3TS;-><init>(LX/1Db;)V

    move-object v0, v2

    .line 138944
    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/BetterListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 138945
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    const-string v2, "jewel_popup_notifications"

    invoke-static {v1, v2, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 138946
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    .line 138947
    iget-object v1, v0, LX/0xW;->a:LX/0ad;

    sget-short v2, LX/0xc;->f:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 138948
    if-eqz v0, :cond_0

    .line 138949
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    const v1, 0x7f0d14dc

    invoke-virtual {v0, v1}, LX/3TN;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 138950
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030bf0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    .line 138951
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 138952
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040028

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->as:Landroid/view/animation/Animation;

    .line 138953
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040029

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->at:Landroid/view/animation/Animation;

    .line 138954
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->as:Landroid/view/animation/Animation;

    new-instance v1, LX/Ivw;

    invoke-direct {v1, p0}, LX/Ivw;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 138955
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->at:Landroid/view/animation/Animation;

    new-instance v1, LX/Ivx;

    invoke-direct {v1, p0}, LX/Ivx;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 138956
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    new-instance v1, LX/Ivy;

    invoke-direct {v1, p0}, LX/Ivy;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138957
    :cond_0
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    .line 138958
    if-eqz v0, :cond_1

    .line 138959
    new-instance v1, LX/3TT;

    invoke-direct {v1, p0, v0}, LX/3TT;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Lcom/facebook/widget/FbSwipeRefreshLayout;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 138960
    :cond_1
    const/4 v0, 0x1

    move v0, v0

    .line 138961
    if-eqz v0, :cond_2

    .line 138962
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    if-nez v0, :cond_8

    .line 138963
    :cond_2
    :goto_0
    new-instance v0, LX/3TX;

    invoke-direct {v0, p0}, LX/3TX;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ae:LX/3TX;

    .line 138964
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->V:LX/1Kt;

    .line 138965
    new-instance v1, LX/3TY;

    invoke-direct {v1, p0}, LX/3TY;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    move-object v1, v1

    .line 138966
    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 138967
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 138968
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ae:LX/3TX;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 138969
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->V:LX/1Kt;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 138970
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0, p0}, LX/0g8;->a(LX/0fu;)V

    .line 138971
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->I:LX/2ix;

    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2ix;->a(LX/0g8;)V

    .line 138972
    const v0, 0x7f031399

    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v1

    invoke-interface {v1}, LX/0g8;->b()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 138973
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v0, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 138974
    :cond_3
    iput-boolean v5, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aw:Z

    .line 138975
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->n:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aF:Ljava/lang/String;

    .line 138976
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->v:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v4, "cold"

    .line 138977
    :goto_1
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    const-string v3, "load_type"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 138978
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    const-string v3, "load_type"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 138979
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 138980
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->v:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 138981
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    const v2, 0x910009

    const-string v3, "NotifListLoadTimeCold"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    sget-object v3, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v3, v3, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 138982
    iput-boolean v5, v1, LX/0Yj;->n:Z

    .line 138983
    move-object v1, v1

    .line 138984
    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 138985
    :goto_2
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->R:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x91000b

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/util/Collection;)V

    .line 138986
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->R:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x910005

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/util/Collection;)V

    .line 138987
    iput-boolean v5, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->az:Z

    .line 138988
    :cond_4
    new-instance v0, LX/3Ta;

    invoke-direct {v0, p0}, LX/3Ta;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    move-object v0, v0

    .line 138989
    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ak:LX/2Ip;

    .line 138990
    new-instance v0, LX/3Tb;

    invoke-direct {v0, p0}, LX/3Tb;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    move-object v0, v0

    .line 138991
    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->al:LX/2hG;

    .line 138992
    new-instance v0, LX/3Tc;

    invoke-direct {v0, p0}, LX/3Tc;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    move-object v0, v0

    .line 138993
    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->am:LX/2hO;

    .line 138994
    new-instance v0, LX/3Td;

    invoke-direct {v0, p0}, LX/3Td;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    move-object v0, v0

    .line 138995
    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->an:LX/2hK;

    .line 138996
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t:LX/2do;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ak:LX/2Ip;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 138997
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t:LX/2do;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->al:LX/2hG;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 138998
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t:LX/2do;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->am:LX/2hO;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 138999
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t:LX/2do;

    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->an:LX/2hK;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 139000
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->X:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->s()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 139001
    new-instance v0, LX/Ivu;

    invoke-direct {v0, p0}, LX/Ivu;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    move-object v0, v0

    .line 139002
    iput-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ao:LX/0dN;

    .line 139003
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->X:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ao:LX/0dN;

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 139004
    :cond_5
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    const v1, 0x221e593e

    invoke-static {v1, v6}, LX/02F;->f(II)V

    return-object v0

    .line 139005
    :cond_6
    const-string v4, "warm"

    goto/16 :goto_1

    .line 139006
    :cond_7
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    const v2, 0x91000a

    const-string v3, "NotifListLoadTimeWarm"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    sget-object v3, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    iget-object v3, v3, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 139007
    iput-boolean v5, v1, LX/0Yj;->n:Z

    .line 139008
    move-object v1, v1

    .line 139009
    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    goto/16 :goto_2

    .line 139010
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    new-instance v1, LX/3TU;

    invoke-direct {v1, p0}, LX/3TU;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/3TV;)V

    goto/16 :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x58327e86

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139011
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDestroy()V

    .line 139012
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aa:LX/0Yb;

    if-eqz v1, :cond_0

    .line 139013
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aa:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 139014
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ab:LX/0Yb;

    if-eqz v1, :cond_1

    .line 139015
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ab:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 139016
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x734e0654

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1de61e3b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 139032
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDestroyView()V

    .line 139033
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t:LX/2do;

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ak:LX/2Ip;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 139034
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t:LX/2do;

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->al:LX/2hG;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 139035
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t:LX/2do;

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->am:LX/2hO;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 139036
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t:LX/2do;

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->an:LX/2hK;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 139037
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->X:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ao:LX/0dN;

    if-eqz v0, :cond_0

    .line 139038
    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->X:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ao:LX/0dN;

    invoke-interface {v2, v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;LX/0dN;)V

    .line 139039
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ap:LX/0xA;

    if-eqz v0, :cond_1

    .line 139040
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ap:LX/0xA;

    invoke-virtual {v0, v2}, LX/0xB;->b(LX/0xA;)V

    .line 139041
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ad:LX/0Yb;

    if-eqz v0, :cond_2

    .line 139042
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ad:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 139043
    :cond_2
    invoke-direct {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J()V

    .line 139044
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 139045
    iget-object v2, v0, LX/3Te;->g:LX/3UK;

    .line 139046
    iget-object v0, v2, LX/3UK;->c:LX/2iS;

    invoke-virtual {v0}, LX/2iS;->b()V

    .line 139047
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 139048
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ae:LX/3TX;

    invoke-interface {v0, v2}, LX/0g8;->c(LX/0fx;)V

    .line 139049
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->V:LX/1Kt;

    invoke-interface {v0, v2}, LX/0g8;->c(LX/0fx;)V

    .line 139050
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0}, LX/0g8;->w()V

    .line 139051
    :cond_3
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 139052
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->p$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 139053
    :cond_4
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 139054
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ah:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 139055
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->as:Landroid/view/animation/Animation;

    if-eqz v0, :cond_5

    .line 139056
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->as:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 139057
    :cond_5
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->at:Landroid/view/animation/Animation;

    if-eqz v0, :cond_6

    .line 139058
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->at:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 139059
    :cond_6
    iput-object v5, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aj:LX/3TN;

    .line 139060
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x910009

    const-string v3, "NotifListLoadTimeCold"

    invoke-interface {v0, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 139061
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x91000a

    const-string v3, "NotifListLoadTimeWarm"

    invoke-interface {v0, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 139062
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->T:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 139063
    const/16 v0, 0x2b

    const v2, -0x2375ee91

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xaa32bf4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139064
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onPause()V

    .line 139065
    invoke-direct {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Q()V

    .line 139066
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->L:LX/2jO;

    const/4 v2, 0x0

    .line 139067
    iput-object v2, v1, LX/2jO;->h:Ljava/lang/Runnable;

    .line 139068
    const/4 v1, 0x0

    .line 139069
    sput-boolean v1, LX/3Cc;->a:Z

    .line 139070
    const/16 v1, 0x2b

    const v2, -0x59ea9b31

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x2a

    const v1, -0x65578b03

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139071
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onResume()V

    .line 139072
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0030

    const-string v3, "NNF_PermalinkNotificationLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 139073
    invoke-direct {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t()V

    .line 139074
    invoke-static {p0, v4}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V

    .line 139075
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 139076
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->L:LX/2jO;

    new-instance v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment$2;

    invoke-direct {v2, p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment$2;-><init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 139077
    iput-object v2, v1, LX/2jO;->h:Ljava/lang/Runnable;

    .line 139078
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139079
    sput-boolean v4, LX/3Cc;->a:Z

    .line 139080
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x58c1b5fb

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5f172e68

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139081
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onStart()V

    .line 139082
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aA:I

    .line 139083
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbListFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 139084
    if-eqz v1, :cond_0

    .line 139085
    const v2, 0x7f081153

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 139086
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 139087
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x5a99f1f1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5de2fdf3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139088
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onStop()V

    .line 139089
    iget-object v1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->L:LX/2jO;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/2jO;->a(LX/DqC;)V

    .line 139090
    const/16 v1, 0x2b

    const v2, -0x5a62e0b6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 10

    .prologue
    .line 139091
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    .line 139092
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->setUserVisibleHint(Z)V

    .line 139093
    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 139094
    :cond_0
    :goto_0
    return-void

    .line 139095
    :cond_1
    if-eqz p1, :cond_5

    .line 139096
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->s(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 139097
    invoke-static {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 139098
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    if-eqz v0, :cond_2

    .line 139099
    invoke-direct {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->t()V

    .line 139100
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->y:LX/0xB;

    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ax:I

    .line 139101
    iget v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ax:I

    if-lez v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 139102
    invoke-virtual {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0}, LX/0g8;->v()V

    .line 139103
    :cond_3
    :goto_1
    if-eqz p1, :cond_4

    .line 139104
    iget-object v2, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->J:LX/0xW;

    invoke-virtual {v2}, LX/0xW;->B()J

    move-result-wide v2

    .line 139105
    sget-object v4, LX/0SF;->a:LX/0SF;

    move-object v4, v4

    .line 139106
    invoke-virtual {v4}, LX/0SF;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    iget-object v6, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H:LX/1rn;

    .line 139107
    iget-wide v8, v6, LX/1rn;->n:J

    move-wide v6, v8

    .line 139108
    sub-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-gtz v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 139109
    if-eqz v0, :cond_4

    .line 139110
    sget-object v0, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    invoke-static {p0, v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;LX/2ub;)V

    .line 139111
    :cond_4
    iput-boolean p1, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->az:Z

    .line 139112
    sput-boolean p1, LX/3Cc;->a:Z

    .line 139113
    goto :goto_0

    .line 139114
    :cond_5
    invoke-direct {p0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Q()V

    .line 139115
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->L:LX/2jO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2jO;->a(LX/DqC;)V

    .line 139116
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ag:LX/4nS;

    if-eqz v0, :cond_3

    .line 139117
    iget-object v0, p0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->ag:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method
