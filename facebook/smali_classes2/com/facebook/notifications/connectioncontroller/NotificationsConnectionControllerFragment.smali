.class public Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fk;
.implements LX/0fu;
.implements LX/0fv;


# annotations
.annotation runtime Lcom/facebook/navigation/annotations/NavigationComponent;
    scrollableContent = .subannotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
        id = "android.R.id.list"
        type = .enum LX/8Cz;->RECYCLER_VIEW:LX/8Cz;
    .end subannotation
.end annotation


# instance fields
.field private A:LX/0fx;

.field public B:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public C:LX/4nS;

.field private D:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

.field private E:LX/2jZ;

.field private F:LX/2jY;

.field public G:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public H:LX/0g7;

.field public I:Z

.field private J:Z

.field public K:Z

.field private L:Z

.field public M:I

.field public N:I

.field public O:J

.field public a:LX/2hU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0xB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2iu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/16I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2iv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/2iw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/2ix;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1ro;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/2iy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/2iz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1rU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/2jO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final u:Ljava/lang/Runnable;

.field public v:LX/2kw;

.field private w:LX/2je;

.field public x:LX/2kg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kg",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "LX/3DK;",
            ">;"
        }
    .end annotation
.end field

.field public y:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private z:LX/0Yb;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 137625
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 137626
    new-instance v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment$1;-><init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->u:Ljava/lang/Runnable;

    .line 137627
    iput-boolean v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->I:Z

    .line 137628
    iput-boolean v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->J:Z

    .line 137629
    iput-boolean v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->K:Z

    .line 137630
    iput-boolean v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->L:Z

    .line 137631
    iput v2, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->M:I

    .line 137632
    iput v2, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->N:I

    .line 137633
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->O:J

    return-void
.end method

.method public static a(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;Landroid/content/Context;)LX/2kj;
    .locals 11

    .prologue
    .line 137788
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->e:LX/2iu;

    .line 137789
    sget-object v1, LX/2kh;->a:LX/2kh;

    move-object v2, v1

    .line 137790
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->u:Ljava/lang/Runnable;

    iget-object v6, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->D:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v7, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->E:LX/2jZ;

    iget-object v8, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->F:LX/2jY;

    new-instance v9, LX/2ki;

    invoke-direct {v9, p0}, LX/2ki;-><init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    iget-object v10, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    move-object v1, p1

    move-object v3, p0

    invoke-virtual/range {v0 .. v10}, LX/2iu;->a(Landroid/content/Context;LX/1PT;Landroid/support/v4/app/Fragment;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/2jZ;LX/2jY;LX/1PY;LX/2iv;)LX/2kj;

    move-result-object v0

    .line 137791
    return-object v0
.end method

.method private static a(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2hU;LX/0SG;LX/193;LX/0xB;LX/2iu;LX/16I;LX/2iv;LX/2iw;LX/2ix;Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1ro;LX/2iy;LX/2iz;LX/0SI;Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;Ljava/util/concurrent/ExecutorService;LX/1rU;LX/2jO;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;",
            "LX/2hU;",
            "LX/0SG;",
            "LX/193;",
            "LX/0xB;",
            "LX/2iu;",
            "LX/16I;",
            "LX/2iv;",
            "LX/2iw;",
            "LX/2ix;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/1ro;",
            "LX/2iy;",
            "LX/2iz;",
            "LX/0SI;",
            "Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/1rU;",
            "LX/2jO;",
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137792
    iput-object p1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a:LX/2hU;

    iput-object p2, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->b:LX/0SG;

    iput-object p3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->c:LX/193;

    iput-object p4, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->d:LX/0xB;

    iput-object p5, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->e:LX/2iu;

    iput-object p6, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->f:LX/16I;

    iput-object p7, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    iput-object p8, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->h:LX/2iw;

    iput-object p9, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->i:LX/2ix;

    iput-object p10, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    iput-object p11, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p12, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->l:LX/1ro;

    iput-object p13, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->m:LX/2iy;

    iput-object p14, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->n:LX/2iz;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->o:LX/0SI;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->q:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->r:LX/1rU;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->s:LX/2jO;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->t:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 23

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v21

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    invoke-static/range {v21 .. v21}, LX/2hU;->a(LX/0QB;)LX/2hU;

    move-result-object v2

    check-cast v2, LX/2hU;

    invoke-static/range {v21 .. v21}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    const-class v4, LX/193;

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/193;

    invoke-static/range {v21 .. v21}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v5

    check-cast v5, LX/0xB;

    const-class v6, LX/2iu;

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2iu;

    invoke-static/range {v21 .. v21}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v7

    check-cast v7, LX/16I;

    invoke-static/range {v21 .. v21}, LX/2iv;->a(LX/0QB;)LX/2iv;

    move-result-object v8

    check-cast v8, LX/2iv;

    const-class v9, LX/2iw;

    move-object/from16 v0, v21

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/2iw;

    invoke-static/range {v21 .. v21}, LX/2ix;->a(LX/0QB;)LX/2ix;

    move-result-object v10

    check-cast v10, LX/2ix;

    invoke-static/range {v21 .. v21}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v11

    check-cast v11, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {v21 .. v21}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v12

    check-cast v12, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v21 .. v21}, LX/1ro;->a(LX/0QB;)LX/1ro;

    move-result-object v13

    check-cast v13, LX/1ro;

    const-class v14, LX/2iy;

    move-object/from16 v0, v21

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/2iy;

    invoke-static/range {v21 .. v21}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v15

    check-cast v15, LX/2iz;

    invoke-static/range {v21 .. v21}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v16

    check-cast v16, LX/0SI;

    invoke-static/range {v21 .. v21}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a(LX/0QB;)Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    move-result-object v17

    check-cast v17, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-static/range {v21 .. v21}, LX/0UA;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v18

    check-cast v18, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v21 .. v21}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v19

    check-cast v19, LX/1rU;

    invoke-static/range {v21 .. v21}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v20

    check-cast v20, LX/2jO;

    const/16 v22, 0x64

    invoke-static/range {v21 .. v22}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    invoke-static/range {v1 .. v21}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2hU;LX/0SG;LX/193;LX/0xB;LX/2iu;LX/16I;LX/2iv;LX/2iw;LX/2ix;Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1ro;LX/2iy;LX/2iz;LX/0SI;Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;Ljava/util/concurrent/ExecutorService;LX/1rU;LX/2jO;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2kM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137793
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->J:Z

    .line 137794
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    .line 137795
    iput-object p1, v0, LX/2iv;->a:LX/2kM;

    .line 137796
    invoke-interface {p1}, LX/2kM;->b()LX/2nj;

    move-result-object v0

    .line 137797
    iget-boolean p1, v0, LX/2nj;->d:Z

    move v0, p1

    .line 137798
    iput-boolean v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->I:Z

    .line 137799
    return-void
.end method

.method public static a$redex0(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2ub;)V
    .locals 3

    .prologue
    .line 137800
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->o:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/Drn;)V

    .line 137801
    return-void
.end method

.method public static c(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V
    .locals 5

    .prologue
    .line 137834
    const/4 v4, 0x0

    .line 137835
    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v3}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v3}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v3

    invoke-interface {v3}, LX/2kM;->c()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v3}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v3

    invoke-interface {v3, v4}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v3}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v3

    invoke-interface {v3, v4}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    if-nez v3, :cond_1

    .line 137836
    :cond_0
    const/4 v3, 0x0

    .line 137837
    :goto_0
    move-object v0, v3

    .line 137838
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->q:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment$3;

    invoke-direct {v2, p0, v0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment$3;-><init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;Ljava/lang/Long;)V

    const v0, -0x4d0ed349

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 137839
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->d:LX/0xB;

    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0xB;->a(LX/12j;I)V

    .line 137840
    return-void

    :cond_1
    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v3}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v3

    invoke-interface {v3, v4}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0
.end method

.method public static c(LX/2nk;)Z
    .locals 1

    .prologue
    .line 137802
    sget-object v0, LX/2nk;->AFTER:LX/2nk;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 137803
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->o()I

    move-result v0

    if-lez v0, :cond_0

    .line 137804
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->g(I)V

    .line 137805
    :cond_0
    return-void
.end method

.method private o()I
    .locals 2

    .prologue
    .line 137806
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->d:LX/0xB;

    if-eqz v0, :cond_0

    .line 137807
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->d:LX/0xB;

    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    .line 137808
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 137809
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    const-string v3, "load_type"

    const-string v4, "warm"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 137810
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    const-string v3, "load_type"

    const-string v4, "warm"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 137811
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137812
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    const v2, 0x35000a

    const-string v3, "NotifListLoadTimeWarm"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v4, v4, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 137813
    iput-boolean v5, v1, LX/0Yj;->n:Z

    .line 137814
    move-object v1, v1

    .line 137815
    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 137816
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000d

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/util/Collection;)V

    .line 137817
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350005

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/util/Collection;)V

    .line 137818
    iput-boolean v5, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->L:Z

    .line 137819
    return-void
.end method

.method public static s(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V
    .locals 4

    .prologue
    .line 137820
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->f:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137821
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wL;

    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->B:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0805ec

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 137822
    invoke-static {v0, v1, v2}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 137823
    sget-object v0, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    invoke-static {p0, v0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a$redex0(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2ub;)V

    .line 137824
    :goto_0
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->s:LX/2jO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2jO;->a(LX/DqC;)V

    .line 137825
    return-void

    .line 137826
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->B:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 137827
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->C:LX/4nS;

    if-eqz v0, :cond_1

    .line 137828
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->C:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    .line 137829
    :cond_1
    new-instance v0, LX/62h;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62h;-><init>(Landroid/content/Context;)V

    .line 137830
    new-instance v1, LX/H4l;

    invoke-direct {v1, p0}, LX/H4l;-><init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    invoke-virtual {v0, v1}, LX/62h;->setRetryClickListener(Landroid/view/View$OnClickListener;)V

    .line 137831
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a:LX/2hU;

    const/16 v2, 0x2710

    invoke-virtual {v1, v0, v2}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->C:LX/4nS;

    .line 137832
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->C:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->a()V

    .line 137833
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137774
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 137775
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 137776
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 137777
    new-instance v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    const-string v1, "ANDROID_NOTIFICATIONS"

    const-string v2, "unknown"

    invoke-direct {v0, v3, v1, v2, v3}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->D:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 137778
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 137779
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->n:LX/2iz;

    const-string v2, "ANDROID_NOTIFICATIONS"

    invoke-virtual {v1, v0, v2}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v1

    .line 137780
    iget-object v2, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->n:LX/2iz;

    invoke-virtual {v2, v0}, LX/2iz;->c(Ljava/lang/String;)V

    .line 137781
    move-object v0, v1

    .line 137782
    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->F:LX/2jY;

    .line 137783
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->m:LX/2iy;

    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->F:LX/2jY;

    .line 137784
    new-instance v4, LX/2jZ;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a(LX/0QB;)Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    move-result-object v8

    check-cast v8, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v10

    check-cast v10, LX/0gh;

    invoke-static {v0}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v11

    check-cast v11, LX/2Yg;

    invoke-static {v0}, LX/2j3;->a(LX/0QB;)LX/2j3;

    move-result-object v12

    check-cast v12, LX/2j3;

    invoke-static {v0}, LX/1vC;->a(LX/0QB;)LX/1vC;

    move-result-object v13

    check-cast v13, LX/1vC;

    move-object v5, v1

    move-object v6, v3

    invoke-direct/range {v4 .. v13}, LX/2jZ;-><init>(LX/2jY;LX/1P1;LX/0TD;Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;LX/03V;LX/0gh;LX/2Yg;LX/2j3;LX/1vC;)V

    .line 137785
    move-object v0, v4

    .line 137786
    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->E:LX/2jZ;

    .line 137787
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137771
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 137772
    const-string v1, "jewel_count"

    invoke-direct {p0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->o()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137773
    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 137768
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    if-eqz v0, :cond_0

    .line 137769
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->d(I)V

    .line 137770
    :cond_0
    return-void
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137750
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 137751
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    invoke-virtual {v0}, LX/2iv;->size()I

    move-result v0

    .line 137752
    :goto_0
    const-string v1, "\n  Loaded Notifications: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 137753
    const-string v1, "\n  Last Load Time: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->O:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 137754
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->N:I

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->M:I

    if-ltz v1, :cond_3

    .line 137755
    const-string v1, "\n  Visible Notification Ids: [\n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137756
    iget v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->M:I

    iget v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->N:I

    add-int/2addr v1, v3

    .line 137757
    if-le v1, v0, :cond_4

    .line 137758
    :goto_1
    iget v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->M:I

    :goto_2
    if-ge v1, v0, :cond_2

    .line 137759
    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    invoke-virtual {v3, v1}, LX/2iv;->b(I)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v3

    .line 137760
    if-eqz v3, :cond_1

    invoke-interface {v3}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 137761
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "    "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137762
    :goto_3
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137763
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 137764
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 137765
    :cond_1
    const-string v3, "    null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 137766
    :cond_2
    const-string v0, "  ]\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137767
    :cond_3
    const-string v0, "\nNotificationsConnectionControllerFragment Debug Info: "

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 137749
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->n()Z

    move-result v0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 137748
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    return-object v0
.end method

.method public final ko_()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 137740
    iget-boolean v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->L:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->J:Z

    if-eqz v0, :cond_0

    .line 137741
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x35000a

    const-string v2, "NotifListLoadTimeWarm"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 137742
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    const-string v3, "load_type"

    const-string v4, "hot"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 137743
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 137744
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    const-string v3, "load_type"

    const-string v4, "hot"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 137745
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 137746
    iput-boolean v5, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->L:Z

    .line 137747
    :cond_0
    return v5
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 137737
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 137738
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    invoke-static {v0}, LX/62G;->a(LX/0g8;)V

    .line 137739
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, -0x4db7eae5

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 137693
    const v0, 0x7f030c2e

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 137694
    const v0, 0x102000a

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->G:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 137695
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->G:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 137696
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->G:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 137697
    new-instance v0, LX/0g7;

    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->G:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v0, v3}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    .line 137698
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->k()V

    .line 137699
    new-instance v0, LX/2jd;

    invoke-direct {v0, p0}, LX/2jd;-><init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    move-object v0, v0

    .line 137700
    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->A:LX/0fx;

    .line 137701
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->A:LX/0fx;

    invoke-virtual {v0, v3}, LX/0g7;->b(LX/0fx;)V

    .line 137702
    new-instance v0, LX/2je;

    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->c:LX/193;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_notification_connection_controller_scroll_perf"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v3

    invoke-direct {v0, v3}, LX/2je;-><init>(LX/195;)V

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->w:LX/2je;

    .line 137703
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->w:LX/2je;

    invoke-virtual {v0, v3}, LX/0g7;->b(LX/0fx;)V

    .line 137704
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    invoke-virtual {v0, p0}, LX/0g7;->a(LX/0fu;)V

    .line 137705
    new-instance v3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0e0a41

    invoke-direct {v3, v0, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 137706
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 137707
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v5, 0x7f0101e2

    const/4 v6, 0x1

    invoke-virtual {v0, v5, v4, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 137708
    const v0, 0x7f0d05b0

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->y:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 137709
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->y:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget v5, v4, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v5}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setBackgroundResource(I)V

    .line 137710
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->y:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 137711
    iget v0, v4, Landroid/util/TypedValue;->resourceId:I

    .line 137712
    const v4, 0x7f0d0595

    invoke-static {v2, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v4, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->B:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 137713
    iget-object v4, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->B:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v4, v0}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setBackgroundResource(I)V

    .line 137714
    iget-object v4, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->B:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v5, LX/2jg;

    invoke-direct {v5, p0}, LX/2jg;-><init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 137715
    iget-object v4, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->B:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 137716
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->i:LX/2ix;

    iget-object v4, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    invoke-virtual {v0, v4}, LX/2ix;->a(LX/0g8;)V

    .line 137717
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->i:LX/2ix;

    iget-object v4, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    iget-object v5, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->G:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v4, v5}, LX/2ix;->a(LX/0g8;Landroid/support/v7/widget/RecyclerView;)V

    .line 137718
    invoke-direct {p0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p()V

    .line 137719
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137720
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->y:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 137721
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a$redex0(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2kM;)V

    .line 137722
    :cond_0
    :goto_0
    new-instance v0, LX/2kd;

    invoke-direct {v0, p0}, LX/2kd;-><init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    .line 137723
    new-instance v4, LX/2kf;

    invoke-direct {v4, p0, v0}, LX/2kf;-><init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2ke;)V

    move-object v0, v4

    .line 137724
    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->x:LX/2kg;

    .line 137725
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a()LX/2kW;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->x:LX/2kg;

    invoke-virtual {v0, v4}, LX/2kW;->a(LX/1vq;)V

    .line 137726
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    if-nez v0, :cond_1

    .line 137727
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->h:LX/2iw;

    invoke-static {p0, v3}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;Landroid/content/Context;)LX/2kj;

    move-result-object v4

    new-instance v5, LX/2kv;

    invoke-direct {v5}, LX/2kv;-><init>()V

    iget-object v6, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    invoke-virtual {v0, v4, v5, v6}, LX/2iw;->a(LX/1PW;LX/1DZ;LX/0g1;)LX/2kw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    .line 137728
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    new-instance v4, LX/2kz;

    invoke-direct {v4, p0}, LX/2kz;-><init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    .line 137729
    iput-object v4, v0, LX/2kw;->j:Landroid/view/View$OnClickListener;

    .line 137730
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    sget-object v4, LX/2kx;->FINISHED:LX/2kx;

    invoke-virtual {v0, v4}, LX/2kw;->a(LX/2kx;)V

    .line 137731
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    iget-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    invoke-virtual {v0, v3}, LX/0g7;->a(LX/1OO;)V

    .line 137732
    const/16 v0, 0x2b

    const v3, 0x7fb5b656

    invoke-static {v7, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2

    .line 137733
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a()LX/2kW;

    move-result-object v0

    .line 137734
    iget-boolean v4, v0, LX/2kW;->p:Z

    move v0, v4

    .line 137735
    if-eqz v0, :cond_0

    .line 137736
    sget-object v0, LX/2ub;->FRAGMENT_LOADED:LX/2ub;

    invoke-static {p0, v0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a$redex0(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2ub;)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x2dc94b3f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137672
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 137673
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    iget-object v2, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->A:LX/0fx;

    invoke-virtual {v1, v2}, LX/0g7;->c(LX/0fx;)V

    .line 137674
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    iget-object v2, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->w:LX/2je;

    invoke-virtual {v1, v2}, LX/0g7;->c(LX/0fx;)V

    .line 137675
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    invoke-virtual {v1, v3}, LX/0g7;->a(Landroid/view/View$OnTouchListener;)V

    .line 137676
    iput-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    .line 137677
    iput-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->G:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 137678
    iput-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->A:LX/0fx;

    .line 137679
    iput-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->w:LX/2je;

    .line 137680
    iput-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->y:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 137681
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->B:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 137682
    iput-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->B:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 137683
    iput-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->C:LX/4nS;

    .line 137684
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    if-eqz v1, :cond_0

    .line 137685
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    .line 137686
    iput-object v3, v1, LX/2iv;->a:LX/2kM;

    .line 137687
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    if-eqz v1, :cond_1

    .line 137688
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v1}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a()LX/2kW;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->x:LX/2kg;

    invoke-virtual {v1, v2}, LX/2kW;->b(LX/1vq;)V

    .line 137689
    iput-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->x:LX/2kg;

    .line 137690
    iput-object v3, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    .line 137691
    :cond_1
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x35000a

    const-string v3, "NotifListLoadTimeWarm"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 137692
    const/16 v1, 0x2b

    const v2, 0x609fe97b

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3515795a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137662
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 137663
    const/4 v1, 0x0

    .line 137664
    sput-boolean v1, LX/3Cc;->a:Z

    .line 137665
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->z:LX/0Yb;

    if-eqz v1, :cond_0

    .line 137666
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->z:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 137667
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->s:LX/2jO;

    .line 137668
    iput-object v2, v1, LX/2jO;->j:LX/2kW;

    .line 137669
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->s:LX/2jO;

    .line 137670
    iput-object v2, v1, LX/2jO;->h:Ljava/lang/Runnable;

    .line 137671
    const/16 v1, 0x2b

    const v2, -0x6304c500

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4d3f1edf    # 2.00404464E8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137649
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 137650
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137651
    const/4 v1, 0x1

    .line 137652
    sput-boolean v1, LX/3Cc;->a:Z

    .line 137653
    invoke-direct {p0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->n()V

    .line 137654
    invoke-static {p0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->c(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    .line 137655
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0030

    const-string v3, "NNF_PermalinkNotificationLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 137656
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->f:LX/16I;

    sget-object v2, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v3, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment$2;

    invoke-direct {v3, p0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment$2;-><init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    invoke-virtual {v1, v2, v3}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->z:LX/0Yb;

    .line 137657
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->s:LX/2jO;

    iget-object v2, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v2}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a()LX/2kW;

    move-result-object v2

    .line 137658
    iput-object v2, v1, LX/2jO;->j:LX/2kW;

    .line 137659
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->s:LX/2jO;

    iget-object v2, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->u:Ljava/lang/Runnable;

    .line 137660
    iput-object v2, v1, LX/2jO;->h:Ljava/lang/Runnable;

    .line 137661
    const/16 v1, 0x2b

    const v2, -0x5faca8e7

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x236ed44e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137646
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 137647
    iget-object v1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->s:LX/2jO;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/2jO;->a(LX/DqC;)V

    .line 137648
    const/16 v1, 0x2b

    const v2, 0x2a4cdce7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 137634
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    .line 137635
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 137636
    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 137637
    :cond_0
    :goto_0
    return-void

    .line 137638
    :cond_1
    if-eqz p1, :cond_3

    .line 137639
    invoke-direct {p0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->n()V

    .line 137640
    invoke-static {p0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->c(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    .line 137641
    :cond_2
    :goto_1
    sput-boolean p1, LX/3Cc;->a:Z

    .line 137642
    iput-boolean p1, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->L:Z

    goto :goto_0

    .line 137643
    :cond_3
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->s:LX/2jO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2jO;->a(LX/DqC;)V

    .line 137644
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->C:LX/4nS;

    if-eqz v0, :cond_2

    .line 137645
    iget-object v0, p0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->C:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    goto :goto_1
.end method
