.class public Lcom/facebook/notifications/tab/NotificationsTab;
.super Lcom/facebook/apptab/state/TabTag;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/tab/NotificationsTab;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lcom/facebook/notifications/tab/NotificationsTab;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85880
    new-instance v0, Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-direct {v0}, Lcom/facebook/notifications/tab/NotificationsTab;-><init>()V

    sput-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    .line 85881
    new-instance v0, LX/0cR;

    invoke-direct {v0}, LX/0cR;-><init>()V

    sput-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    .prologue
    .line 85882
    sget-object v1, LX/0ax;->dd:Ljava/lang/String;

    sget-object v2, LX/0cQ;->NOTIFICATIONS_FRAGMENT:LX/0cQ;

    const v3, 0x7f02107a

    const/4 v4, 0x0

    const-string v5, "notifications"

    const v6, 0x63000a

    const v7, 0x63000b

    const-string v8, "LoadTab_Notifications"

    const-string v9, "LoadTab_Notifications_NoAnim"

    const v10, 0x7f080a2d

    const v11, 0x7f0d0036

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, Lcom/facebook/apptab/state/TabTag;-><init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V

    .line 85883
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p2    # LX/0cQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 85884
    invoke-direct/range {p0 .. p11}, Lcom/facebook/apptab/state/TabTag;-><init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V

    .line 85885
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85886
    const-string v0, "Notifications"

    return-object v0
.end method
