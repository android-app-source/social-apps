.class public Lcom/facebook/notifications/tab/NotificationsFriendingTab;
.super Lcom/facebook/notifications/tab/NotificationsTab;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/tab/NotificationsFriendingTab;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 175721
    new-instance v0, Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    invoke-direct {v0}, Lcom/facebook/notifications/tab/NotificationsFriendingTab;-><init>()V

    sput-object v0, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    .line 175722
    new-instance v0, LX/12k;

    invoke-direct {v0}, LX/12k;-><init>()V

    sput-object v0, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    .prologue
    .line 175723
    sget-object v1, LX/0ax;->iA:Ljava/lang/String;

    sget-object v2, LX/0cQ;->NOTIFICATIONS_FRIENDING_FRAGMENT:LX/0cQ;

    const v3, 0x7f02107a

    const/4 v4, 0x0

    const-string v5, "notifications_friending"

    const v6, 0x630011

    const v7, 0x630012

    const-string v8, "LoadTab_NotificationsFriending"

    const-string v9, "LoadTab_NotificationsFriending_NoAnim"

    const v10, 0x7f080a2d

    const v11, 0x7f0d0036

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, Lcom/facebook/notifications/tab/NotificationsTab;-><init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V

    .line 175724
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175725
    const-string v0, "NotificationsFriending"

    return-object v0
.end method
