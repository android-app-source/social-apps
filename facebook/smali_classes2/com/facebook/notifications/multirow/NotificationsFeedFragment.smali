.class public Lcom/facebook/notifications/multirow/NotificationsFeedFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fk;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0o5;
.implements LX/0o6;
.implements LX/0o8;
.implements LX/0fu;
.implements LX/0fv;


# annotations
.annotation build Lcom/facebook/common/init/GenerateInitializer;
    task = LX/33V;
.end annotation

.annotation runtime Lcom/facebook/navigation/annotations/NavigationComponent;
    scrollableContent = .subannotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
        id = "android.R.id.list"
        type = .enum LX/8Cz;->RECYCLER_VIEW:LX/8Cz;
    .end subannotation
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/3Tp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public B:LX/3Fx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public C:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public D:LX/1rU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public E:LX/0SI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public F:LX/1Kt;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public G:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/1QH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public I:LX/1DL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public J:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field public K:LX/0xB;

.field public L:LX/16I;

.field public M:LX/33W;

.field public N:LX/33Z;

.field public O:LX/33b;

.field public P:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

.field public Q:LX/1rn;

.field public R:LX/2iz;

.field public S:Lcom/facebook/reaction/ReactionUtil;

.field public T:LX/2kw;

.field public U:Landroid/content/Context;

.field private V:LX/195;

.field private W:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private X:LX/0Yb;

.field public Y:LX/3Tu;

.field public Z:LX/CSB;

.field private aa:LX/H53;

.field private ab:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

.field private ac:LX/2jY;

.field public ad:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private ae:LX/0g7;

.field public af:LX/1DI;

.field public ag:LX/4nS;

.field public ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field private ai:LX/3U5;

.field private aj:LX/1Jg;

.field public ak:Z

.field private al:I

.field private am:I

.field private an:Z

.field public ao:I

.field public ap:Z

.field public aq:I

.field public ar:I

.field private as:J

.field public c:LX/0oh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/2hU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/3Tn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/193;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/2A8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/1rx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0V6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/0So;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/2iw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/H5D;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/3To;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/H5i;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/2ix;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/0xW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/BE6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/1qv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/3Cl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:LX/2jO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/3TL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public y:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public z:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 138470
    const-class v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    const-string v1, "notifications"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 138471
    const-class v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    sput-object v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 138472
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 138473
    iput-boolean v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ak:Z

    .line 138474
    iput v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->al:I

    .line 138475
    const/16 v0, 0x64

    iput v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->am:I

    .line 138476
    iput-boolean v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->an:Z

    .line 138477
    iput v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ao:I

    .line 138478
    iput-boolean v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ap:Z

    .line 138479
    iput v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aq:I

    .line 138480
    iput v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ar:I

    .line 138481
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->as:J

    .line 138482
    return-void
.end method

.method private A()V
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 138483
    move v1, v0

    .line 138484
    :goto_0
    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v2}, LX/H5D;->a()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 138485
    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v2, v0}, LX/H5D;->b(I)LX/2nq;

    move-result-object v2

    .line 138486
    iget-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->t:LX/BE6;

    const/4 v5, 0x0

    .line 138487
    invoke-interface {v2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 138488
    invoke-interface {v2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v7, v5

    .line 138489
    :goto_1
    if-ge v7, v9, :cond_6

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    .line 138490
    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v6, v5

    :goto_2
    if-ge v6, v11, :cond_5

    invoke-virtual {v10, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;

    .line 138491
    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->a()I

    move-result v12

    if-lez v12, :cond_7

    invoke-interface {v2}, LX/2nq;->l()I

    move-result v12

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->a()I

    move-result v13

    if-lt v12, v13, :cond_7

    const/4 v12, 0x1

    :goto_3
    move v12, v12

    .line 138492
    if-nez v12, :cond_0

    invoke-static {v3, v4}, LX/BE6;->a(LX/BE6;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 138493
    :cond_0
    const/4 v4, 0x1

    .line 138494
    :goto_4
    move v3, v4

    .line 138495
    if-eqz v3, :cond_1

    .line 138496
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ai:LX/3U5;

    invoke-static {v2}, LX/BE6;->a(LX/2nq;)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/3U5;->a(LX/2nq;Z)Z

    .line 138497
    const/4 v1, 0x1

    .line 138498
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138499
    :cond_2
    if-eqz v1, :cond_3

    .line 138500
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    invoke-virtual {v0}, LX/2kw;->e()V

    .line 138501
    :cond_3
    return-void

    .line 138502
    :cond_4
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_2

    .line 138503
    :cond_5
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_1

    :cond_6
    move v4, v5

    .line 138504
    goto :goto_4

    :cond_7
    const/4 v12, 0x0

    goto :goto_3
.end method

.method private B()Z
    .locals 1

    .prologue
    .line 138505
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Landroid/content/Context;)LX/3U5;
    .locals 12

    .prologue
    .line 138506
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->g:LX/3Tn;

    .line 138507
    sget-object v1, LX/2kh;->a:LX/2kh;

    move-object v2, v1

    .line 138508
    const/4 v3, 0x0

    new-instance v4, Lcom/facebook/notifications/multirow/NotificationsFeedFragment$3;

    invoke-direct {v4, p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment$3;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    iget-object v5, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ab:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->A:LX/3Tp;

    invoke-virtual {v1, p1, p0}, LX/3Tp;->a(Landroid/content/Context;LX/0o8;)LX/3Tv;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Y:LX/3Tu;

    iget-object v8, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ac:LX/2jY;

    new-instance v9, LX/H4y;

    invoke-direct {v9, p0}, LX/H4y;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    iget-object v10, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->H:LX/1QH;

    iget-object v11, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aj:LX/1Jg;

    invoke-virtual {v1, v11}, LX/1QH;->a(LX/1Jg;)LX/1QW;

    move-result-object v11

    move-object v1, p1

    invoke-virtual/range {v0 .. v11}, LX/3Tn;->a(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/3Tv;LX/3Tu;LX/2jY;LX/1PY;LX/3Tt;LX/1QW;)LX/3U5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ai:LX/3U5;

    .line 138509
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ai:LX/3U5;

    return-object v0
.end method

.method private static a(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;LX/0oh;LX/0Zb;LX/2hU;LX/0SG;LX/3Tn;LX/03V;LX/193;LX/2A8;LX/1rx;LX/0V6;LX/0So;LX/2iw;LX/H5D;LX/3To;LX/H5i;LX/2ix;LX/0xW;LX/BE6;LX/1qv;LX/3Cl;LX/2jO;LX/3TL;Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/3Tp;LX/3Fx;LX/0Sh;LX/1rU;LX/0SI;LX/1Kt;LX/0Ot;LX/1QH;LX/1DL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/multirow/NotificationsFeedFragment;",
            "LX/0oh;",
            "LX/0Zb;",
            "LX/2hU;",
            "LX/0SG;",
            "LX/3Tn;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/193;",
            "LX/2A8;",
            "LX/1rx;",
            "LX/0V6;",
            "LX/0So;",
            "LX/2iw;",
            "LX/H5D;",
            "LX/3To;",
            "LX/H5i;",
            "LX/2ix;",
            "LX/0xW;",
            "LX/BE6;",
            "LX/1qv;",
            "Lcom/facebook/notifications/widget/NotificationsRenderer;",
            "LX/2jO;",
            "LX/3TL;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/3Tp;",
            "LX/3Fx;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/1rU;",
            "LX/0SI;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;",
            "LX/1QH;",
            "LX/1DL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 138510
    iput-object p1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->c:LX/0oh;

    iput-object p2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->d:LX/0Zb;

    iput-object p3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->e:LX/2hU;

    iput-object p4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->f:LX/0SG;

    iput-object p5, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->g:LX/3Tn;

    iput-object p6, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->h:LX/03V;

    iput-object p7, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->i:LX/193;

    iput-object p8, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->j:LX/2A8;

    iput-object p9, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->k:LX/1rx;

    iput-object p10, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->l:LX/0V6;

    iput-object p11, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->m:LX/0So;

    iput-object p12, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->n:LX/2iw;

    iput-object p13, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    iput-object p14, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->p:LX/3To;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->q:LX/H5i;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->r:LX/2ix;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->s:LX/0xW;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->t:LX/BE6;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->u:LX/1qv;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->v:LX/3Cl;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->w:LX/2jO;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->x:LX/3TL;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->A:LX/3Tp;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->B:LX/3Fx;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->C:LX/0Sh;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->D:LX/1rU;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->E:LX/0SI;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->G:LX/0Ot;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->H:LX/1QH;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->I:LX/1DL;

    return-void
.end method

.method public static a(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 138511
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->E:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    .line 138512
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->W:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_3

    if-eqz v4, :cond_3

    .line 138513
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v0}, LX/H5D;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    .line 138514
    :goto_0
    iget-object v5, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->W:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_5

    move v3, v1

    :goto_1
    invoke-virtual {v5, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 138515
    if-eqz v0, :cond_6

    if-eqz p1, :cond_6

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    invoke-virtual {v0}, LX/0k5;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 138516
    :goto_2
    iget-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    invoke-static {v4}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v1, v2

    .line 138517
    :cond_2
    if-eqz v1, :cond_7

    .line 138518
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->W:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 138519
    :cond_3
    :goto_3
    return-void

    :cond_4
    move v0, v1

    .line 138520
    goto :goto_0

    .line 138521
    :cond_5
    const/16 v3, 0x8

    goto :goto_1

    :cond_6
    move v0, v1

    .line 138522
    goto :goto_2

    .line 138523
    :cond_7
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->W:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_3
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 36

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v35

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-static/range {v35 .. v35}, LX/0oh;->a(LX/0QB;)LX/0oh;

    move-result-object v3

    check-cast v3, LX/0oh;

    invoke-static/range {v35 .. v35}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static/range {v35 .. v35}, LX/2hU;->a(LX/0QB;)LX/2hU;

    move-result-object v5

    check-cast v5, LX/2hU;

    invoke-static/range {v35 .. v35}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    const-class v7, LX/3Tn;

    move-object/from16 v0, v35

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/3Tn;

    invoke-static/range {v35 .. v35}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    const-class v9, LX/193;

    move-object/from16 v0, v35

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/193;

    invoke-static/range {v35 .. v35}, LX/2A8;->a(LX/0QB;)LX/2A8;

    move-result-object v10

    check-cast v10, LX/2A8;

    invoke-static/range {v35 .. v35}, LX/1rx;->a(LX/0QB;)LX/1rx;

    move-result-object v11

    check-cast v11, LX/1rx;

    invoke-static/range {v35 .. v35}, LX/0V4;->a(LX/0QB;)LX/0V6;

    move-result-object v12

    check-cast v12, LX/0V6;

    invoke-static/range {v35 .. v35}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v13

    check-cast v13, LX/0So;

    const-class v14, LX/2iw;

    move-object/from16 v0, v35

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/2iw;

    invoke-static/range {v35 .. v35}, LX/H5D;->a(LX/0QB;)LX/H5D;

    move-result-object v15

    check-cast v15, LX/H5D;

    const-class v16, LX/3To;

    move-object/from16 v0, v35

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/3To;

    invoke-static/range {v35 .. v35}, LX/H5i;->a(LX/0QB;)LX/H5i;

    move-result-object v17

    check-cast v17, LX/H5i;

    invoke-static/range {v35 .. v35}, LX/2ix;->a(LX/0QB;)LX/2ix;

    move-result-object v18

    check-cast v18, LX/2ix;

    invoke-static/range {v35 .. v35}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v19

    check-cast v19, LX/0xW;

    invoke-static/range {v35 .. v35}, LX/BE6;->a(LX/0QB;)LX/BE6;

    move-result-object v20

    check-cast v20, LX/BE6;

    invoke-static/range {v35 .. v35}, LX/1qv;->a(LX/0QB;)LX/1qv;

    move-result-object v21

    check-cast v21, LX/1qv;

    invoke-static/range {v35 .. v35}, LX/3Cl;->a(LX/0QB;)LX/3Cl;

    move-result-object v22

    check-cast v22, LX/3Cl;

    invoke-static/range {v35 .. v35}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v23

    check-cast v23, LX/2jO;

    invoke-static/range {v35 .. v35}, LX/3TL;->a(LX/0QB;)LX/3TL;

    move-result-object v24

    check-cast v24, LX/3TL;

    invoke-static/range {v35 .. v35}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v25

    check-cast v25, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {v35 .. v35}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v26

    check-cast v26, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-class v27, LX/3Tp;

    move-object/from16 v0, v35

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/3Tp;

    invoke-static/range {v35 .. v35}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v28

    check-cast v28, LX/3Fx;

    invoke-static/range {v35 .. v35}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v29

    check-cast v29, LX/0Sh;

    invoke-static/range {v35 .. v35}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v30

    check-cast v30, LX/1rU;

    invoke-static/range {v35 .. v35}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v31

    check-cast v31, LX/0SI;

    invoke-static/range {v35 .. v35}, LX/23N;->a(LX/0QB;)LX/23N;

    move-result-object v32

    check-cast v32, LX/1Kt;

    const/16 v33, 0x64

    move-object/from16 v0, v35

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v33

    const-class v34, LX/1QH;

    move-object/from16 v0, v35

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v34

    check-cast v34, LX/1QH;

    invoke-static/range {v35 .. v35}, LX/1DL;->a(LX/0QB;)LX/1DL;

    move-result-object v35

    check-cast v35, LX/1DL;

    invoke-static/range {v2 .. v35}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;LX/0oh;LX/0Zb;LX/2hU;LX/0SG;LX/3Tn;LX/03V;LX/193;LX/2A8;LX/1rx;LX/0V6;LX/0So;LX/2iw;LX/H5D;LX/3To;LX/H5i;LX/2ix;LX/0xW;LX/BE6;LX/1qv;LX/3Cl;LX/2jO;LX/3TL;Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/3Tp;LX/3Fx;LX/0Sh;LX/1rU;LX/0SI;LX/1Kt;LX/0Ot;LX/1QH;LX/1DL;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;LX/2ub;)V
    .locals 6

    .prologue
    .line 138524
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ag:LX/4nS;

    if-eqz v0, :cond_0

    .line 138525
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ag:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    .line 138526
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->E:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-nez v0, :cond_1

    .line 138527
    :goto_0
    return-void

    .line 138528
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    sget-object v1, LX/2kx;->LOADING:LX/2kx;

    invoke-virtual {v0, v1}, LX/2kw;->a(LX/2kx;)V

    .line 138529
    sget-object v0, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    if-ne p1, v0, :cond_2

    .line 138530
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350005

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 138531
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->w:LX/2jO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2jO;->a(LX/DqC;)V

    .line 138532
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->P:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->E:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 138533
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->C:LX/0Sh;

    new-instance v2, LX/H4w;

    iget-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->E:LX/0SI;

    invoke-interface {v3}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 138534
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 138535
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LX/H4w;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Ljava/lang/Long;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 138536
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Z)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 138537
    new-instance v2, LX/2i4;

    invoke-direct {v2}, LX/2i4;-><init>()V

    .line 138538
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ad:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 138539
    :goto_0
    if-eqz v0, :cond_0

    .line 138540
    iget-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v3, v2}, LX/0g7;->a(LX/2i4;)V

    .line 138541
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->c(Ljava/util/List;)V

    .line 138542
    if-eqz v0, :cond_1

    .line 138543
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v0, v2}, LX/0g7;->b(LX/2i4;)V

    .line 138544
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_2

    .line 138545
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 138546
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x350005

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 138547
    invoke-static {p0, v1}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Z)V

    .line 138548
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->p()V

    .line 138549
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->as:J

    .line 138550
    return-void

    :cond_3
    move v0, v1

    .line 138551
    goto :goto_0
.end method

.method private c(Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138552
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    .line 138553
    iget-object v1, v0, LX/H5D;->d:LX/H5C;

    invoke-virtual {v1}, LX/H5C;->b()V

    .line 138554
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 138555
    :goto_0
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    invoke-virtual {v0}, LX/2kw;->e()V

    .line 138556
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138557
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/0g8;)V

    .line 138558
    :cond_0
    return-void

    .line 138559
    :cond_1
    iget-object v1, v0, LX/H5D;->d:LX/H5C;

    invoke-virtual {v1, p1}, LX/H5C;->a(Ljava/util/Collection;)V

    .line 138560
    iget-boolean v1, v0, LX/H5D;->f:Z

    if-nez v1, :cond_2

    .line 138561
    iget-object v1, v0, LX/H5D;->c:LX/H5C;

    iget-object v2, v0, LX/H5D;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/H5C;->a(Ljava/util/Collection;)V

    .line 138562
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/H5D;->f:Z

    .line 138563
    :cond_2
    invoke-static {v0}, LX/H5D;->e(LX/H5D;)V

    goto :goto_0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 138564
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-nez v0, :cond_1

    .line 138565
    :cond_0
    :goto_0
    return-void

    .line 138566
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Q:LX/1rn;

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->E:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1rn;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    goto :goto_0
.end method

.method public static r(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V
    .locals 3

    .prologue
    .line 138567
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->W:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_0

    .line 138568
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->W:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->U:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->af:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 138569
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Q:LX/1rn;

    invoke-virtual {v0}, LX/1rn;->c()V

    .line 138570
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350005

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 138571
    return-void
.end method

.method private u()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 138572
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->J:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v4, "cold"

    .line 138573
    :goto_0
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    const-string v3, "load_type"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 138574
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    const-string v3, "load_type"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 138575
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138576
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->J:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138577
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    const v2, 0x350009

    const-string v3, "NotifListLoadTimeCold"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    sget-object v3, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v3, v3, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 138578
    iput-boolean v5, v1, LX/0Yj;->n:Z

    .line 138579
    move-object v1, v1

    .line 138580
    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 138581
    :goto_1
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000d

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/util/Collection;)V

    .line 138582
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350005

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/util/Collection;)V

    .line 138583
    iput-boolean v5, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->an:Z

    .line 138584
    :cond_0
    return-void

    .line 138585
    :cond_1
    const-string v4, "warm"

    goto :goto_0

    .line 138586
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    const v2, 0x35000a

    const-string v3, "NotifListLoadTimeWarm"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    sget-object v3, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v3, v3, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 138587
    iput-boolean v5, v1, LX/0Yj;->n:Z

    .line 138588
    move-object v1, v1

    .line 138589
    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    goto :goto_1
.end method

.method public static v(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V
    .locals 5

    .prologue
    .line 138590
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v0}, LX/H5D;->a()I

    move-result v0

    iget v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->am:I

    if-le v0, v1, :cond_1

    .line 138591
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    sget-object v1, LX/2kx;->FINISHED:LX/2kx;

    invoke-virtual {v0, v1}, LX/2kw;->a(LX/2kx;)V

    .line 138592
    :cond_0
    :goto_0
    return-void

    .line 138593
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    const/4 v2, 0x0

    .line 138594
    invoke-virtual {v0}, LX/H5D;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    .line 138595
    :goto_1
    move-object v0, v1

    .line 138596
    if-eqz v0, :cond_0

    .line 138597
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x35000d

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 138598
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ak:Z

    .line 138599
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->P:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->E:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    sget-object v3, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v4}, LX/H5D;->a()I

    move-result v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a(Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 138600
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->C:LX/0Sh;

    new-instance v2, LX/H4s;

    invoke-direct {v2, p0}, LX/H4s;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0

    .line 138601
    :cond_2
    iget-object v1, v0, LX/H5D;->c:LX/H5C;

    invoke-virtual {v1}, LX/H5C;->a()I

    move-result v1

    if-lez v1, :cond_3

    .line 138602
    iget-object v1, v0, LX/H5D;->c:LX/H5C;

    iget-object v1, v1, LX/H5C;->b:Ljava/util/List;

    iget-object v3, v0, LX/H5D;->c:LX/H5C;

    invoke-virtual {v3}, LX/H5C;->a()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2nq;

    .line 138603
    :goto_2
    if-nez v1, :cond_4

    move-object v1, v2

    goto :goto_1

    .line 138604
    :cond_3
    iget-object v1, v0, LX/H5D;->d:LX/H5C;

    invoke-virtual {v1}, LX/H5C;->a()I

    move-result v1

    if-lez v1, :cond_5

    .line 138605
    iget-object v1, v0, LX/H5D;->d:LX/H5C;

    iget-object v1, v1, LX/H5C;->b:Ljava/util/List;

    iget-object v3, v0, LX/H5D;->d:LX/H5C;

    invoke-virtual {v3}, LX/H5C;->a()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2nq;

    goto :goto_2

    .line 138606
    :cond_4
    invoke-interface {v1}, LX/2nq;->ia_()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_2
.end method

.method private w()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 138607
    iget v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ao:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ap:Z

    if-nez v0, :cond_1

    .line 138608
    :cond_0
    :goto_0
    return-void

    .line 138609
    :cond_1
    iget v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ao:I

    iget v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aq:I

    if-le v0, v2, :cond_2

    const/4 v0, 0x1

    .line 138610
    :goto_1
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "notification_scroll_depth_v2"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "notifications"

    .line 138611
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 138612
    move-object v2, v2

    .line 138613
    const-string v3, "scroll_depth"

    iget v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ao:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "jewel_count"

    iget v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->al:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "visible_item_count"

    iget v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aq:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "passed_scroll_threshold"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 138614
    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->d:LX/0Zb;

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 138615
    iput-boolean v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ap:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 138616
    goto :goto_1
.end method

.method public static x(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V
    .locals 3

    .prologue
    .line 138617
    new-instance v0, LX/62h;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62h;-><init>(Landroid/content/Context;)V

    .line 138618
    new-instance v1, LX/H4t;

    invoke-direct {v1, p0}, LX/H4t;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    invoke-virtual {v0, v1}, LX/62h;->setRetryClickListener(Landroid/view/View$OnClickListener;)V

    .line 138619
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->e:LX/2hU;

    const/16 v2, 0x2710

    invoke-virtual {v1, v0, v2}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ag:LX/4nS;

    .line 138620
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ag:LX/4nS;

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 138621
    iput-object v1, v0, LX/4nS;->i:Landroid/view/View;

    .line 138622
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ag:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->a()V

    .line 138623
    return-void
.end method

.method private z()V
    .locals 15

    .prologue
    .line 138624
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->q:LX/H5i;

    .line 138625
    iget-object v1, v0, LX/H5i;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v2, v1

    .line 138626
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 138627
    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v4, v0}, LX/H5D;->b(Ljava/lang/String;)I

    move-result v0

    .line 138628
    if-ltz v0, :cond_2

    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v4}, LX/H5D;->a()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 138629
    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v4, v0}, LX/H5D;->b(I)LX/2nq;

    move-result-object v0

    .line 138630
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 138631
    invoke-interface {v0}, LX/2nq;->l()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    .line 138632
    iget-object v5, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Q:LX/1rn;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v6

    .line 138633
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138634
    iget-object v7, v5, LX/1rn;->d:LX/0TD;

    new-instance v8, Lcom/facebook/notifications/util/NotificationsUtils$6;

    invoke-direct {v8, v5, v6, v4}, Lcom/facebook/notifications/util/NotificationsUtils$6;-><init>(LX/1rn;Ljava/lang/String;I)V

    const v9, 0x61891217

    invoke-static {v7, v8, v9}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 138635
    iget-object v5, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->J:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    .line 138636
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 138637
    iget-object v8, v5, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->o:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v7, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138638
    iget-object v7, v5, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v4}, LX/2AD;->a(Ljava/lang/String;I)Z

    .line 138639
    iget-object v7, v5, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 v8, 0x0

    .line 138640
    iput-boolean v8, v7, LX/2AF;->b:Z

    .line 138641
    iget-object v7, v5, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v4}, LX/2AD;->a(Ljava/lang/String;I)Z

    .line 138642
    iget-object v7, v5, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 v8, 0x1

    .line 138643
    iput-boolean v8, v7, LX/2AF;->b:Z

    .line 138644
    iget-object v5, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-static {v0, v4}, LX/BDX;->a(LX/2nq;I)LX/2nq;

    move-result-object v4

    invoke-virtual {v5, v0, v4}, LX/H5D;->a(LX/2nq;LX/2nq;)V

    .line 138645
    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->q:LX/H5i;

    .line 138646
    invoke-static {v4, v0}, LX/H5i;->b(LX/H5i;LX/2nq;)LX/H5j;

    move-result-object v8

    .line 138647
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v9

    .line 138648
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v10, "notification_unbatched_true_impression"

    invoke-direct {v7, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v10, "cache_id"

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v10, "graphql_id"

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v10, "width_of_notification"

    .line 138649
    iget v11, v8, LX/H5j;->c:I

    move v11, v11

    .line 138650
    invoke-virtual {v7, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v10, "height_of_notification"

    const/4 v11, 0x0

    .line 138651
    move v12, v11

    move v13, v11

    .line 138652
    :goto_1
    iget-object v11, v8, LX/H5j;->b:Landroid/util/SparseArray;

    invoke-virtual {v11}, Landroid/util/SparseArray;->size()I

    move-result v11

    if-ge v12, v11, :cond_0

    .line 138653
    iget-object v11, v8, LX/H5j;->b:Landroid/util/SparseArray;

    invoke-virtual {v11, v12}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v11

    .line 138654
    iget-object v14, v8, LX/H5j;->b:Landroid/util/SparseArray;

    invoke-virtual {v14, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    add-int/2addr v13, v11

    .line 138655
    add-int/lit8 v11, v12, 0x1

    move v12, v11

    goto :goto_1

    .line 138656
    :cond_0
    move v11, v13

    .line 138657
    invoke-virtual {v7, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "is_rich_notif"

    invoke-interface {v0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-interface {v0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v7

    if-eqz v7, :cond_4

    const/4 v7, 0x1

    :goto_2
    invoke-virtual {v10, v11, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v10, "is_rich_notif_expanded"

    .line 138658
    iget-object v11, v8, LX/H5j;->f:LX/03R;

    move-object v11, v11

    .line 138659
    invoke-virtual {v11}, LX/03R;->asBooleanObject()Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v10, "notif_type"

    const/4 v11, 0x0

    .line 138660
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v12

    if-nez v12, :cond_5

    .line 138661
    :cond_1
    :goto_3
    move-object v11, v11

    .line 138662
    invoke-virtual {v7, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v10, "offset_from_top"

    iget v11, v4, LX/H5i;->b:I

    .line 138663
    iget v12, v8, LX/H5j;->a:I

    mul-int/2addr v12, v11

    move v11, v12

    .line 138664
    invoke-virtual {v7, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v10, "row_number"

    .line 138665
    iget v11, v8, LX/H5j;->a:I

    move v11, v11

    .line 138666
    invoke-virtual {v7, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v10, "seen_state"

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v10, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v9, "visible_duration"

    .line 138667
    iget-wide v13, v8, LX/H5j;->e:J

    move-wide v11, v13

    .line 138668
    invoke-virtual {v7, v9, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    sget-object v8, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v8, v8, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    .line 138669
    iput-object v8, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 138670
    move-object v7, v7

    .line 138671
    iget-object v8, v4, LX/H5i;->a:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 138672
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 138673
    :cond_3
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->q:LX/H5i;

    .line 138674
    iget-object v1, v0, LX/H5i;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 138675
    return-void

    .line 138676
    :cond_4
    const/4 v7, 0x0

    goto :goto_2

    .line 138677
    :cond_5
    :try_start_0
    new-instance v12, Lorg/json/JSONObject;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 138678
    const-string v13, "notif_type"

    invoke-virtual {v12, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 138679
    const-string v13, "notif_type"

    invoke-virtual {v12, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    goto :goto_3

    :catch_0
    goto :goto_3
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138680
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    .line 138681
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 138682
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 138683
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->c:LX/0oh;

    const-class v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-virtual {v0, v1}, LX/0oh;->b(Ljava/lang/Class;)LX/1Av;

    move-result-object v0

    invoke-virtual {v0}, LX/1Av;->c()LX/1Ax;

    move-result-object v0

    check-cast v0, LX/33V;

    .line 138684
    iget-object v1, v0, LX/33V;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 138685
    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->J:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 138686
    iget-object v1, v0, LX/33V;->b:LX/0xB;

    .line 138687
    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->K:LX/0xB;

    .line 138688
    iget-object v1, v0, LX/33V;->c:LX/16I;

    .line 138689
    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->L:LX/16I;

    .line 138690
    iget-object v1, v0, LX/33V;->d:LX/33W;

    .line 138691
    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->M:LX/33W;

    .line 138692
    iget-object v1, v0, LX/33V;->e:LX/33Z;

    .line 138693
    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->N:LX/33Z;

    .line 138694
    iget-object v1, v0, LX/33V;->f:LX/33b;

    .line 138695
    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->O:LX/33b;

    .line 138696
    iget-object v1, v0, LX/33V;->g:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    .line 138697
    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->P:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    .line 138698
    iget-object v1, v0, LX/33V;->h:LX/1rn;

    .line 138699
    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Q:LX/1rn;

    .line 138700
    iget-object v1, v0, LX/33V;->i:LX/2iz;

    .line 138701
    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->R:LX/2iz;

    .line 138702
    iget-object v1, v0, LX/33V;->j:Lcom/facebook/reaction/ReactionUtil;

    .line 138703
    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->S:Lcom/facebook/reaction/ReactionUtil;

    .line 138704
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->O:LX/33b;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->v:LX/3Cl;

    iget-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->U:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/3Cl;->a(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/33b;->a(Landroid/view/Display;I)I

    move-result v6

    .line 138705
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->x:LX/3TL;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->E:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->j:LX/2A8;

    iget-object v5, v1, LX/2A8;->b:Landroid/net/Uri;

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->S:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/facebook/reaction/ReactionUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, LX/3TL;->a(LX/0o6;LX/0k5;Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/net/Uri;ILjava/lang/String;)V

    .line 138706
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->l:LX/0V6;

    invoke-virtual {v0}, LX/0V6;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138707
    const/16 v0, 0x1e

    iput v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->am:I

    .line 138708
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 138709
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->R:LX/2iz;

    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v1

    .line 138710
    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->R:LX/2iz;

    invoke-virtual {v2, v0}, LX/2iz;->c(Ljava/lang/String;)V

    .line 138711
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->p:LX/3To;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v0, v1, v2, v3}, LX/3To;->a(LX/2jY;LX/1P1;LX/3Tt;)LX/3Tu;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Y:LX/3Tu;

    .line 138712
    move-object v0, v1

    .line 138713
    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ac:LX/2jY;

    .line 138714
    new-instance v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o()Ljava/lang/String;

    move-result-object v1

    const-string v2, "unknown"

    invoke-direct {v0, v8, v1, v2, v8}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ab:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 138715
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->i:LX/193;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->D:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_notification_multirow_components_scroll_perf"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->V:LX/195;

    .line 138716
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->I:LX/1DL;

    invoke-virtual {v0}, LX/1DL;->a()LX/1Jg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aj:LX/1Jg;

    .line 138717
    return-void

    .line 138718
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_notification_multirow_scroll_perf"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 138719
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    .line 138720
    iget-object v1, v0, LX/H5D;->c:LX/H5C;

    invoke-virtual {v1}, LX/H5C;->b()V

    .line 138721
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 138722
    :goto_0
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    invoke-virtual {v0}, LX/2kw;->e()V

    .line 138723
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138724
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/0g8;)V

    .line 138725
    :cond_0
    return-void

    .line 138726
    :cond_1
    iget-object v1, v0, LX/H5D;->c:LX/H5C;

    invoke-virtual {v1, p1}, LX/H5C;->a(Ljava/util/Collection;)V

    .line 138727
    invoke-static {v0}, LX/H5D;->e(LX/H5D;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138728
    invoke-static {p0, p1}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->b(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Ljava/util/List;)V

    .line 138729
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    .line 138730
    const/4 v1, 0x0

    .line 138731
    iget-object v2, v0, LX/H5D;->d:LX/H5C;

    iget-object v2, v2, LX/H5C;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2nq;

    .line 138732
    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 138733
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v1, p1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 138734
    add-int/lit8 v1, v2, 0x1

    :goto_1
    move v2, v1

    .line 138735
    goto :goto_0

    .line 138736
    :cond_0
    move v0, v2

    .line 138737
    if-lez v0, :cond_1

    .line 138738
    const-string v0, "notification_auto_update_list_update"

    const-string v1, "new_notifications_notif_fragment"

    .line 138739
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "notifications"

    .line 138740
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 138741
    move-object v2, v2

    .line 138742
    const-string v3, "reason"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 138743
    iget-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->d:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 138744
    :cond_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 138468
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 138469
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 138281
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138282
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 138283
    const-string v1, "jewel_count"

    iget v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->al:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138284
    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 138285
    sget-object v0, LX/2ub;->FRAGMENT_LOADED:LX/2ub;

    invoke-static {p0, v0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a$redex0(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;LX/2ub;)V

    .line 138286
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 138287
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    if-eqz v0, :cond_0

    .line 138288
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->d(I)V

    .line 138289
    :cond_0
    return-void
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138290
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 138291
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v0}, LX/H5D;->a()I

    move-result v0

    .line 138292
    :goto_0
    const-string v1, "\n  Loaded Notifications: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 138293
    const-string v1, "\n  Last Load Time: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->as:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 138294
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aq:I

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ar:I

    if-ltz v1, :cond_3

    .line 138295
    const-string v1, "\n  Visible Notification Ids: [\n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138296
    iget v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ar:I

    iget v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aq:I

    add-int/2addr v1, v3

    .line 138297
    if-le v1, v0, :cond_5

    .line 138298
    :goto_1
    iget v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ar:I

    :goto_2
    if-ge v1, v0, :cond_2

    .line 138299
    iget-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v3, v1}, LX/H5D;->b(I)LX/2nq;

    move-result-object v3

    .line 138300
    if-eqz v3, :cond_1

    invoke-interface {v3}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 138301
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "    "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138302
    :goto_3
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138303
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 138304
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 138305
    :cond_1
    const-string v3, "    null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 138306
    :cond_2
    const-string v0, "  ]\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138307
    :cond_3
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->u:LX/1qv;

    if-eqz v0, :cond_4

    .line 138308
    const-string v0, "  Last Updated Time: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->u:LX/1qv;

    invoke-virtual {v1}, LX/1qv;->a()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138309
    :cond_4
    const-string v0, "\nNotificationsFeedFragment Debug Info: "

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    return-object v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 138330
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->n()Z

    move-result v0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 138310
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    return-object v0
.end method

.method public final kI_()Landroid/view/ViewGroup;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 138311
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ad:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public final kn_()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138312
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 138313
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 138314
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->c(Ljava/util/List;)V

    .line 138315
    invoke-static {p0, v1}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Z)V

    .line 138316
    return-void
.end method

.method public final ko_()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 138317
    iget-boolean v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->an:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->x:LX/3TL;

    .line 138318
    iget-boolean v1, v0, LX/3TL;->j:Z

    move v0, v1

    .line 138319
    if-eqz v0, :cond_0

    .line 138320
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x350009

    const-string v2, "NotifListLoadTimeCold"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 138321
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x35000a

    const-string v2, "NotifListLoadTimeWarm"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 138322
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    const-string v3, "load_type"

    const-string v4, "hot"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 138323
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 138324
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    const-string v3, "load_type"

    const-string v4, "hot"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 138325
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 138326
    iput-boolean v5, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->an:Z

    .line 138327
    :cond_0
    return v5
.end method

.method public final m()LX/2ja;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 138328
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Y:LX/3Tu;

    return-object v0
.end method

.method public final mZ_()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 138329
    return-object p0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138278
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ac:LX/2jY;

    .line 138279
    iget-object p0, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, p0

    .line 138280
    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 138331
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->s:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ANDROID_NOTIFICATIONS_FRIENDING"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "ANDROID_NOTIFICATIONS"

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x542717b8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 138332
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 138333
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->L:LX/16I;

    sget-object v2, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v3, Lcom/facebook/notifications/multirow/NotificationsFeedFragment$9;

    invoke-direct {v3, p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment$9;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    invoke-virtual {v1, v2, v3}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->X:LX/0Yb;

    .line 138334
    const/16 v1, 0x2b

    const v2, 0x5b1483f8

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 138335
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 138336
    iput-object p1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->U:Landroid/content/Context;

    .line 138337
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 138338
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 138339
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-static {v0}, LX/62G;->a(LX/0g8;)V

    .line 138340
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/16 v0, 0x2a

    const v1, 0x38f004be

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 138341
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v2

    .line 138342
    invoke-virtual {p1, v2}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f030c2e

    invoke-virtual {v0, v3, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 138343
    const v0, 0x7f0d05b0

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->W:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 138344
    const v0, 0x102000a

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ad:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 138345
    const v0, 0x7f0d0595

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 138346
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 138347
    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ad:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 138348
    new-instance v0, LX/0g7;

    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ad:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v0, v4}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    .line 138349
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 138350
    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    const v5, 0x7f0101e2

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v0, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 138351
    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v4}, LX/0g7;->k()V

    .line 138352
    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->W:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget v5, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setBackgroundResource(I)V

    .line 138353
    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v0}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setBackgroundResource(I)V

    .line 138354
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    if-nez v0, :cond_0

    .line 138355
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->n:LX/2iw;

    invoke-static {p0, v2}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Landroid/content/Context;)LX/3U5;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->o:LX/H5D;

    invoke-virtual {v0, v4, v5, v6}, LX/2iw;->a(LX/1PW;LX/1DZ;LX/0g1;)LX/2kw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    .line 138356
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    new-instance v4, LX/H4z;

    invoke-direct {v4, p0}, LX/H4z;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    .line 138357
    iput-object v4, v0, LX/2kw;->j:Landroid/view/View$OnClickListener;

    .line 138358
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aj:LX/1Jg;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    .line 138359
    iget-object v4, v2, LX/2kw;->h:LX/1Rq;

    invoke-interface {v4}, LX/1Qr;->e()LX/1R4;

    move-result-object v4

    move-object v2, v4

    .line 138360
    invoke-interface {v0, v2}, LX/1Jg;->a(LX/1R4;)V

    .line 138361
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Y:LX/3Tu;

    new-instance v2, LX/1Oz;

    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ad:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v2, v4}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->m:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5, v7}, LX/2ja;->a(LX/1P1;JZ)V

    .line 138362
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    invoke-virtual {v0, v2}, LX/0g7;->a(LX/1OO;)V

    .line 138363
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->J:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0, p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0o5;)V

    .line 138364
    new-instance v0, LX/H53;

    invoke-direct {v0, p0}, LX/H53;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aa:LX/H53;

    .line 138365
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aa:LX/H53;

    invoke-virtual {v0, v2}, LX/0g7;->b(LX/0fx;)V

    .line 138366
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    new-instance v2, LX/2je;

    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->V:LX/195;

    invoke-direct {v2, v4}, LX/2je;-><init>(LX/195;)V

    invoke-virtual {v0, v2}, LX/0g7;->b(LX/0fx;)V

    .line 138367
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v0, p0}, LX/0g7;->a(LX/0fu;)V

    .line 138368
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v0, v7, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 138369
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->q:LX/H5i;

    invoke-virtual {v0, v2}, LX/1Kt;->a(LX/1Ce;)V

    .line 138370
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    invoke-virtual {v0, v2}, LX/0g7;->b(LX/0fx;)V

    .line 138371
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->r:LX/2ix;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v0, v2}, LX/2ix;->a(LX/0g8;)V

    .line 138372
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->r:LX/2ix;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ad:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2, v4}, LX/2ix;->a(LX/0g8;Landroid/support/v7/widget/RecyclerView;)V

    .line 138373
    new-instance v0, LX/H50;

    invoke-direct {v0, p0}, LX/H50;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->af:LX/1DI;

    .line 138374
    const v0, 0x7f0d1ded

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v2, LX/H51;

    invoke-direct {v2, p0}, LX/H51;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138375
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v2, LX/H52;

    invoke-direct {v2, p0}, LX/H52;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 138376
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 138377
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->u()V

    .line 138378
    const/16 v0, 0x2b

    const v2, 0x57aed6b1

    invoke-static {v8, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v3
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x52768a48

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 138379
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 138380
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->X:LX/0Yb;

    if-eqz v1, :cond_0

    .line 138381
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->X:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 138382
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    if-eqz v1, :cond_1

    .line 138383
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->J:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v1, p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(LX/0o5;)V

    .line 138384
    :cond_1
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 138385
    iput-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 138386
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->D:LX/1rU;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    if-eqz v1, :cond_2

    .line 138387
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->q:LX/H5i;

    invoke-virtual {v1, v2}, LX/1Kt;->b(LX/1Ce;)V

    .line 138388
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    invoke-virtual {v1, v2}, LX/0g7;->c(LX/0fx;)V

    .line 138389
    :cond_2
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aa:LX/H53;

    invoke-virtual {v1, v2}, LX/0g7;->c(LX/0fx;)V

    .line 138390
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v1, v3}, LX/0g7;->a(Landroid/view/View$OnTouchListener;)V

    .line 138391
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v1}, LX/0g7;->w()V

    .line 138392
    iput-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    .line 138393
    iput-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aa:LX/H53;

    .line 138394
    iput-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ad:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 138395
    iput-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->W:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 138396
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x350009

    const-string v3, "NotifListLoadTimeCold"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 138397
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x35000a

    const-string v3, "NotifListLoadTimeWarm"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 138398
    const/16 v1, 0x2b

    const v2, -0x7937d404

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x569c4b50

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 138399
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 138400
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aj:LX/1Jg;

    invoke-interface {v1}, LX/1Jg;->b()V

    .line 138401
    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->k()LX/0g8;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aj:LX/1Jg;

    invoke-interface {v2}, LX/1Jg;->a()LX/0fx;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 138402
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->w()V

    .line 138403
    sput-boolean v3, LX/3Cc;->a:Z

    .line 138404
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->w:LX/2jO;

    const/4 v2, 0x0

    .line 138405
    iput-object v2, v1, LX/2jO;->h:Ljava/lang/Runnable;

    .line 138406
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v1, v3, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 138407
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z()V

    .line 138408
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->N:LX/33Z;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Z:LX/CSB;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 138409
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->A()V

    .line 138410
    const/16 v1, 0x2b

    const v2, -0xfd5cf2f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x2a

    const v1, -0x704b0f71

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 138411
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 138412
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aj:LX/1Jg;

    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->k()LX/0g8;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Jg;->a(LX/0g8;)V

    .line 138413
    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->k()LX/0g8;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aj:LX/1Jg;

    invoke-interface {v2}, LX/1Jg;->a()LX/0fx;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fx;)V

    .line 138414
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->y:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0030

    const-string v3, "NNF_PermalinkNotificationLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 138415
    invoke-static {p0, v4}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Z)V

    .line 138416
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138417
    sput-boolean v4, LX/3Cc;->a:Z

    .line 138418
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->B()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138419
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v1, v4, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 138420
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->w:LX/2jO;

    new-instance v2, Lcom/facebook/notifications/multirow/NotificationsFeedFragment$1;

    invoke-direct {v2, p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment$1;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    .line 138421
    iput-object v2, v1, LX/2jO;->h:Ljava/lang/Runnable;

    .line 138422
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Z:LX/CSB;

    if-nez v1, :cond_1

    .line 138423
    new-instance v1, LX/H4u;

    invoke-direct {v1, p0}, LX/H4u;-><init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    iput-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Z:LX/CSB;

    .line 138424
    :cond_1
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->N:LX/33Z;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Z:LX/CSB;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 138425
    const/16 v1, 0x2b

    const v2, -0x39c27250

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x773b29f3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 138426
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 138427
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ao:I

    .line 138428
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 138429
    if-eqz v0, :cond_0

    .line 138430
    const v2, 0x7f081153

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 138431
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 138432
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x31dedfb4

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7d18e65

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 138433
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 138434
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->w:LX/2jO;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/2jO;->a(LX/DqC;)V

    .line 138435
    const/16 v1, 0x2b

    const v2, -0x2530889e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 13

    .prologue
    .line 138436
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    .line 138437
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 138438
    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 138439
    :cond_0
    :goto_0
    return-void

    .line 138440
    :cond_1
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->B()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138441
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v0, p1, v1}, LX/1Kt;->a(ZLX/0g8;)V

    .line 138442
    :cond_2
    if-eqz p1, :cond_6

    .line 138443
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->p()V

    .line 138444
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->K:LX/0xB;

    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->al:I

    .line 138445
    iget v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->al:I

    if-lez v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 138446
    invoke-virtual {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0}, LX/0g8;->v()V

    .line 138447
    :cond_3
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->B()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 138448
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->r()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v1}, LX/0g7;->q()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 138449
    iget-object v1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->F:LX/1Kt;

    iget-object v2, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    iget-object v3, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v3}, LX/0g7;->q()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ae:LX/0g7;

    invoke-virtual {v4}, LX/0g7;->s()I

    move-result v4

    invoke-interface {v1, v2, v3, v0, v4}, LX/0fx;->a(LX/0g8;III)V

    .line 138450
    :cond_4
    :goto_1
    if-eqz p1, :cond_5

    .line 138451
    iget-object v5, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->s:LX/0xW;

    invoke-virtual {v5}, LX/0xW;->B()J

    move-result-wide v5

    .line 138452
    sget-object v7, LX/0SF;->a:LX/0SF;

    move-object v7, v7

    .line 138453
    invoke-virtual {v7}, LX/0SF;->a()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    iget-object v9, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Q:LX/1rn;

    .line 138454
    iget-wide v11, v9, LX/1rn;->n:J

    move-wide v9, v11

    .line 138455
    sub-long/2addr v7, v9

    cmp-long v5, v5, v7

    if-gtz v5, :cond_8

    const/4 v5, 0x1

    :goto_2
    move v0, v5

    .line 138456
    if-eqz v0, :cond_5

    .line 138457
    sget-object v0, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    invoke-static {p0, v0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a$redex0(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;LX/2ub;)V

    .line 138458
    :cond_5
    iput-boolean p1, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->an:Z

    .line 138459
    sput-boolean p1, LX/3Cc;->a:Z

    .line 138460
    goto :goto_0

    .line 138461
    :cond_6
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->A()V

    .line 138462
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->w()V

    .line 138463
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->w:LX/2jO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2jO;->a(LX/DqC;)V

    .line 138464
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ag:LX/4nS;

    if-eqz v0, :cond_7

    .line 138465
    iget-object v0, p0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ag:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    .line 138466
    :cond_7
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->B()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 138467
    invoke-direct {p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z()V

    goto :goto_1

    :cond_8
    const/4 v5, 0x0

    goto :goto_2
.end method
