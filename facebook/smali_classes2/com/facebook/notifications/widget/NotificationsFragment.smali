.class public Lcom/facebook/notifications/widget/NotificationsFragment;
.super Lcom/facebook/base/fragment/FbListFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fk;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0o5;
.implements LX/0o6;
.implements LX/0fu;
.implements LX/0fv;
.implements LX/0fw;


# annotations
.annotation runtime Lcom/facebook/navigation/annotations/NavigationComponent;
    scrollableContent = .subannotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
        id = "android.R.id.list"
        type = .enum LX/8Cz;->LIST_VIEW:LX/8Cz;
    .end subannotation
.end annotation


# static fields
.field public static final i:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/33Z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/3DA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/33b;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/16I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/3Cm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/3TL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/0xB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/0id;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/2hU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/3D1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/2jO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/33W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/2PY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/1rx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/DsH;

.field public U:Ljava/lang/String;

.field public V:LX/3TO;

.field public W:J

.field private X:Z

.field public Y:Landroid/content/Context;

.field private Z:LX/DsV;

.field public aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public ab:LX/4nS;

.field private final ac:LX/CSB;

.field public ad:Z

.field private ae:LX/195;

.field private af:LX/DsW;

.field private ag:LX/0Yb;

.field public ah:I

.field public ai:I

.field public aj:I

.field private ak:I

.field public al:Z

.field private am:Z

.field public an:Z

.field public ao:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field private ap:Z

.field private aq:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DsH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1qv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/2A8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1rn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/2ix;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0gh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/2Yg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 138231
    const-class v0, Lcom/facebook/notifications/widget/NotificationsFragment;

    sput-object v0, Lcom/facebook/notifications/widget/NotificationsFragment;->i:Ljava/lang/Class;

    .line 138232
    const-class v0, Lcom/facebook/notifications/widget/NotificationsFragment;

    const-string v1, "notifications"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/widget/NotificationsFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 138086
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbListFragment;-><init>()V

    .line 138087
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->W:J

    .line 138088
    new-instance v0, LX/DsO;

    invoke-direct {v0, p0}, LX/DsO;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ac:LX/CSB;

    .line 138089
    iput v3, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ah:I

    .line 138090
    iput v3, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ai:I

    .line 138091
    iput v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aj:I

    .line 138092
    iput v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ak:I

    .line 138093
    iput-boolean v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->al:Z

    .line 138094
    iput-boolean v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->am:Z

    .line 138095
    iput-boolean v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->an:Z

    .line 138096
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ao:Ljava/util/List;

    .line 138097
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aq:LX/0am;

    .line 138098
    return-void
.end method

.method private static a(Lcom/facebook/notifications/widget/NotificationsFragment;LX/0Ot;LX/1qv;LX/2A8;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/193;LX/1rn;LX/2ix;LX/0Sh;LX/0Zb;LX/0gh;LX/2Yg;Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0SI;LX/33Z;LX/3DA;LX/33b;LX/16I;LX/3Cm;LX/3TL;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/17W;Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;LX/0xB;LX/0id;LX/2hU;LX/3D1;LX/2jO;LX/33W;LX/2PY;LX/1rx;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/widget/NotificationsFragment;",
            "LX/0Ot",
            "<",
            "LX/DsH;",
            ">;",
            "LX/1qv;",
            "LX/2A8;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "LX/193;",
            "LX/1rn;",
            "LX/2ix;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Zb;",
            "LX/0gh;",
            "LX/2Yg;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            "LX/0SI;",
            "LX/33Z;",
            "Lcom/facebook/notifications/util/NotificationStoryLauncher;",
            "LX/33b;",
            "LX/16I;",
            "LX/3Cm;",
            "LX/3TL;",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            "LX/17W;",
            "Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;",
            "LX/0xB;",
            "LX/0id;",
            "LX/2hU;",
            "LX/3D1;",
            "LX/2jO;",
            "LX/33W;",
            "LX/2PY;",
            "LX/1rx;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138099
    iput-object p1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->k:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->l:LX/1qv;

    iput-object p3, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->m:LX/2A8;

    iput-object p4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->n:LX/03V;

    iput-object p5, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p6, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->p:LX/0SG;

    iput-object p7, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->q:LX/193;

    iput-object p8, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->r:LX/1rn;

    iput-object p9, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->s:LX/2ix;

    iput-object p10, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->t:LX/0Sh;

    iput-object p11, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->u:LX/0Zb;

    iput-object p12, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->v:LX/0gh;

    iput-object p13, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->w:LX/2Yg;

    iput-object p14, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->z:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->A:LX/0SI;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->B:LX/33Z;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->C:LX/3DA;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->D:LX/33b;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->E:LX/16I;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->F:LX/3Cm;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->G:LX/3TL;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->H:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->I:LX/17W;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->J:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->K:LX/0xB;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->L:LX/0id;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->M:LX/2hU;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->N:LX/3D1;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->O:LX/2jO;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->P:LX/33W;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Q:LX/2PY;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->R:LX/1rx;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->S:LX/0Or;

    return-void
.end method

.method public static a(Lcom/facebook/notifications/widget/NotificationsFragment;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 138100
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->A:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 138101
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    .line 138102
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {v0}, LX/DsH;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    .line 138103
    :goto_0
    iget-object v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v4, v0}, LX/3TO;->a(Z)V

    .line 138104
    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    invoke-virtual {v0}, LX/0k5;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 138105
    :goto_1
    iget-object v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    if-eqz v4, :cond_1

    if-nez v0, :cond_1

    invoke-static {v3}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v1, v2

    .line 138106
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v0, v1}, LX/3TO;->b(Z)V

    .line 138107
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 138108
    goto :goto_0

    :cond_5
    move v0, v1

    .line 138109
    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 38

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v36

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/notifications/widget/NotificationsFragment;

    const/16 v2, 0x2b12

    move-object/from16 v0, v36

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static/range {v36 .. v36}, LX/1qv;->a(LX/0QB;)LX/1qv;

    move-result-object v3

    check-cast v3, LX/1qv;

    invoke-static/range {v36 .. v36}, LX/2A8;->a(LX/0QB;)LX/2A8;

    move-result-object v4

    check-cast v4, LX/2A8;

    invoke-static/range {v36 .. v36}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static/range {v36 .. v36}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v36 .. v36}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    const-class v8, LX/193;

    move-object/from16 v0, v36

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/193;

    invoke-static/range {v36 .. v36}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v9

    check-cast v9, LX/1rn;

    invoke-static/range {v36 .. v36}, LX/2ix;->a(LX/0QB;)LX/2ix;

    move-result-object v10

    check-cast v10, LX/2ix;

    invoke-static/range {v36 .. v36}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v11

    check-cast v11, LX/0Sh;

    invoke-static/range {v36 .. v36}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v12

    check-cast v12, LX/0Zb;

    invoke-static/range {v36 .. v36}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v13

    check-cast v13, LX/0gh;

    invoke-static/range {v36 .. v36}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v14

    check-cast v14, LX/2Yg;

    invoke-static/range {v36 .. v36}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v15

    check-cast v15, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {v36 .. v36}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v16

    check-cast v16, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v36 .. v36}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v17

    check-cast v17, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static/range {v36 .. v36}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v18

    check-cast v18, LX/0SI;

    invoke-static/range {v36 .. v36}, LX/33Z;->a(LX/0QB;)LX/33Z;

    move-result-object v19

    check-cast v19, LX/33Z;

    invoke-static/range {v36 .. v36}, LX/3DA;->a(LX/0QB;)LX/3DA;

    move-result-object v20

    check-cast v20, LX/3DA;

    invoke-static/range {v36 .. v36}, LX/33b;->a(LX/0QB;)LX/33b;

    move-result-object v21

    check-cast v21, LX/33b;

    invoke-static/range {v36 .. v36}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v22

    check-cast v22, LX/16I;

    invoke-static/range {v36 .. v36}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v23

    check-cast v23, LX/3Cm;

    invoke-static/range {v36 .. v36}, LX/3TL;->a(LX/0QB;)LX/3TL;

    move-result-object v24

    check-cast v24, LX/3TL;

    invoke-static/range {v36 .. v36}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v25

    check-cast v25, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static/range {v36 .. v36}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v26

    check-cast v26, LX/17W;

    invoke-static/range {v36 .. v36}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a(LX/0QB;)Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    move-result-object v27

    check-cast v27, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    invoke-static/range {v36 .. v36}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v28

    check-cast v28, LX/0xB;

    invoke-static/range {v36 .. v36}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v29

    check-cast v29, LX/0id;

    invoke-static/range {v36 .. v36}, LX/2hU;->a(LX/0QB;)LX/2hU;

    move-result-object v30

    check-cast v30, LX/2hU;

    invoke-static/range {v36 .. v36}, LX/3D1;->a(LX/0QB;)LX/3D1;

    move-result-object v31

    check-cast v31, LX/3D1;

    invoke-static/range {v36 .. v36}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v32

    check-cast v32, LX/2jO;

    invoke-static/range {v36 .. v36}, LX/33W;->a(LX/0QB;)LX/33W;

    move-result-object v33

    check-cast v33, LX/33W;

    invoke-static/range {v36 .. v36}, LX/2PY;->a(LX/0QB;)LX/2PY;

    move-result-object v34

    check-cast v34, LX/2PY;

    invoke-static/range {v36 .. v36}, LX/1rx;->a(LX/0QB;)LX/1rx;

    move-result-object v35

    check-cast v35, LX/1rx;

    const/16 v37, 0x122d

    invoke-static/range {v36 .. v37}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v36

    invoke-static/range {v1 .. v36}, Lcom/facebook/notifications/widget/NotificationsFragment;->a(Lcom/facebook/notifications/widget/NotificationsFragment;LX/0Ot;LX/1qv;LX/2A8;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/193;LX/1rn;LX/2ix;LX/0Sh;LX/0Zb;LX/0gh;LX/2Yg;Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0SI;LX/33Z;LX/3DA;LX/33b;LX/16I;LX/3Cm;LX/3TL;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/17W;Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;LX/0xB;LX/0id;LX/2hU;LX/3D1;LX/2jO;LX/33W;LX/2PY;LX/1rx;LX/0Or;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/notifications/widget/NotificationsFragment;Lcom/facebook/feed/rows/core/props/FeedProps;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 138110
    if-nez p1, :cond_0

    .line 138111
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0030

    const-string v2, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 138112
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->L:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    .line 138113
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->n:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/notifications/widget/NotificationsFragment;->i:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_null_notif_story"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Null notif story in adapter"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 138114
    :goto_0
    return-void

    .line 138115
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 138116
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 138117
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v3, LX/0hM;->c:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 138118
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->v:LX/0gh;

    const-string v3, "tap_notification_jewel"

    invoke-virtual {v1, v3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 138119
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    .line 138120
    :goto_1
    new-instance v3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v3}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v4

    .line 138121
    iput-object v4, v3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    .line 138122
    move-object v3, v3

    .line 138123
    iput-boolean v2, v3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    .line 138124
    move-object v3, v3

    .line 138125
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v3

    .line 138126
    iput-boolean v1, v3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    .line 138127
    move-object v3, v3

    .line 138128
    iput p2, v3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->u:I

    .line 138129
    move-object v3, v3

    .line 138130
    iget-object v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->w:LX/2Yg;

    invoke-virtual {v4, v3}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 138131
    if-eqz v1, :cond_2

    .line 138132
    iput-boolean v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ap:Z

    .line 138133
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->r:LX/1rn;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iget-object v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->A:LX/0SI;

    invoke-interface {v4}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/1rn;->a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 138134
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    .line 138135
    invoke-virtual {v1}, LX/DsH;->getCount()I

    move-result v2

    if-lt p2, v2, :cond_7

    .line 138136
    :cond_1
    :goto_2
    const/16 v1, 0x1e

    if-ge p2, v1, :cond_6

    .line 138137
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->H:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    .line 138138
    :cond_2
    :goto_3
    const/4 v1, 0x0

    .line 138139
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 138140
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 138141
    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Y:Landroid/content/Context;

    if-eqz v2, :cond_3

    .line 138142
    invoke-static {v0}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    .line 138143
    if-eqz v1, :cond_8

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->I:LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 138144
    :cond_3
    :goto_4
    if-nez v1, :cond_4

    .line 138145
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0030

    const-string v3, "NNF_PermalinkNotificationLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 138146
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->L:LX/0id;

    invoke-virtual {v1}, LX/0id;->a()V

    .line 138147
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->n:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/facebook/notifications/widget/NotificationsFragment;->i:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_launch_failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not launch notification story "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 138148
    :cond_4
    goto/16 :goto_0

    .line 138149
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 138150
    :cond_6
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->H:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    goto :goto_3

    .line 138151
    :cond_7
    invoke-virtual {v1, p2}, LX/DsH;->b(I)LX/2nq;

    move-result-object v2

    .line 138152
    if-eqz v2, :cond_1

    .line 138153
    invoke-static {v2}, LX/BDX;->a(LX/2nq;)LX/2nq;

    move-result-object v2

    .line 138154
    invoke-static {v1, p2}, LX/DsH;->e(LX/DsH;I)I

    move-result v3

    .line 138155
    iget-object v4, v1, LX/DsH;->g:LX/DsF;

    .line 138156
    iget-object v1, v4, LX/DsF;->d:Ljava/util/List;

    invoke-interface {v1, v3, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 138157
    goto/16 :goto_2

    .line 138158
    :cond_8
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->C:LX/3DA;

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Y:Landroid/content/Context;

    invoke-virtual {v1, v2, p1, p2}, LX/3DA;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;I)Z

    move-result v1

    goto :goto_4
.end method

.method public static i(Lcom/facebook/notifications/widget/NotificationsFragment;)V
    .locals 7

    .prologue
    .line 138159
    new-instance v1, LX/2i4;

    invoke-direct {v1}, LX/2i4;-><init>()V

    .line 138160
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 138161
    :goto_0
    if-eqz v0, :cond_0

    .line 138162
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v2

    invoke-interface {v2, v1}, LX/0g8;->a(LX/2i4;)V

    .line 138163
    :cond_0
    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    iget-object v3, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ao:Ljava/util/List;

    invoke-virtual {v2, v3}, LX/DsH;->a(Ljava/util/Collection;)V

    .line 138164
    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    const v3, -0x4330339d

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 138165
    if-eqz v0, :cond_1

    .line 138166
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0, v1}, LX/0g8;->b(LX/2i4;)V

    .line 138167
    :cond_1
    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->m(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    .line 138168
    iget-object v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x350005

    const/4 v6, 0x2

    invoke-interface {v4, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 138169
    const/4 v4, 0x0

    invoke-static {p0, v4}, Lcom/facebook/notifications/widget/NotificationsFragment;->a(Lcom/facebook/notifications/widget/NotificationsFragment;Z)V

    .line 138170
    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->p(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    .line 138171
    iget-object v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->p:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->W:J

    .line 138172
    return-void

    .line 138173
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/notifications/widget/NotificationsFragment;)V
    .locals 2

    .prologue
    .line 138174
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    if-eqz v0, :cond_1

    .line 138175
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->v()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138176
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->v()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 138177
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v0}, LX/3TO;->getRefreshableContainerLike()LX/62k;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 138178
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v0}, LX/3TO;->getRefreshableContainerLike()LX/62k;

    move-result-object v0

    invoke-interface {v0}, LX/62k;->f()V

    .line 138179
    :cond_1
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 138063
    iget-boolean v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ap:Z

    if-eqz v0, :cond_0

    .line 138064
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    const v1, -0xeea1a04

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 138065
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ap:Z

    .line 138066
    :cond_0
    return-void
.end method

.method public static p(Lcom/facebook/notifications/widget/NotificationsFragment;)V
    .locals 2

    .prologue
    .line 138180
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-nez v0, :cond_1

    .line 138181
    :cond_0
    :goto_0
    return-void

    .line 138182
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->r:LX/1rn;

    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->A:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1rn;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    goto :goto_0
.end method

.method public static q(Lcom/facebook/notifications/widget/NotificationsFragment;)V
    .locals 3

    .prologue
    .line 138183
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    if-eqz v0, :cond_0

    .line 138184
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/3TO;->c(Z)V

    .line 138185
    :cond_0
    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->m(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    .line 138186
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350005

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 138187
    return-void
.end method

.method public static s(Lcom/facebook/notifications/widget/NotificationsFragment;)V
    .locals 5

    .prologue
    .line 138188
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    const/4 v1, 0x0

    .line 138189
    invoke-virtual {v0}, LX/DsH;->c()I

    move-result v2

    .line 138190
    if-nez v2, :cond_4

    .line 138191
    :cond_0
    :goto_0
    move-object v0, v1

    .line 138192
    if-nez v0, :cond_1

    .line 138193
    :goto_1
    return-void

    .line 138194
    :cond_1
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x35000d

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 138195
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v1}, LX/3TO;->getListView()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getFooterViewsCount()I

    move-result v1

    if-nez v1, :cond_2

    .line 138196
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v1, v2}, LX/0g8;->e(Landroid/view/View;)V

    .line 138197
    :cond_2
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f()Z

    move-result v1

    if-nez v1, :cond_3

    .line 138198
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 138199
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ad:Z

    .line 138200
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->J:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->A:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    sget-object v3, Lcom/facebook/notifications/widget/NotificationsFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {v4}, LX/DsH;->c()I

    move-result v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a(Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 138201
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->t:LX/0Sh;

    new-instance v2, LX/DsK;

    invoke-direct {v2, p0}, LX/DsK;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_1

    .line 138202
    :cond_4
    iget-object v3, v0, LX/DsH;->g:LX/DsF;

    add-int/lit8 v2, v2, -0x1

    .line 138203
    iget-object v4, v3, LX/DsF;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    sub-int v4, v2, v4

    .line 138204
    if-ltz v2, :cond_5

    iget-object v0, v3, LX/DsF;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 138205
    iget-object v4, v3, LX/DsF;->b:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2nq;

    .line 138206
    :goto_2
    move-object v2, v4

    .line 138207
    if-eqz v2, :cond_0

    invoke-interface {v2}, LX/2nq;->ia_()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 138208
    :cond_5
    if-ltz v4, :cond_6

    iget-object v0, v3, LX/DsF;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_6

    .line 138209
    iget-object v0, v3, LX/DsF;->c:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2nq;

    goto :goto_2

    .line 138210
    :cond_6
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private t()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 138211
    iget v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aj:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->an:Z

    if-nez v0, :cond_1

    .line 138212
    :cond_0
    :goto_0
    return-void

    .line 138213
    :cond_1
    iget v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aj:I

    iget v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ai:I

    if-le v0, v2, :cond_2

    const/4 v0, 0x1

    .line 138214
    :goto_1
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "notification_scroll_depth_v2"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "notifications"

    .line 138215
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 138216
    move-object v2, v2

    .line 138217
    const-string v3, "scroll_depth"

    iget v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aj:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "jewel_count"

    iget v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ak:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "visible_item_count"

    iget v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ai:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "passed_scroll_threshold"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 138218
    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->u:LX/0Zb;

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 138219
    iput-boolean v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->an:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 138220
    goto :goto_1
.end method

.method public static u(Lcom/facebook/notifications/widget/NotificationsFragment;)V
    .locals 3

    .prologue
    .line 138221
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 138222
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->n:LX/03V;

    sget-object v1, Lcom/facebook/notifications/widget/NotificationsFragment;->i:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "showPTRRetryToast has null context"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 138223
    :goto_0
    return-void

    .line 138224
    :cond_0
    new-instance v0, LX/62h;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62h;-><init>(Landroid/content/Context;)V

    .line 138225
    new-instance v1, LX/DsL;

    invoke-direct {v1, p0}, LX/DsL;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    invoke-virtual {v0, v1}, LX/62h;->setRetryClickListener(Landroid/view/View$OnClickListener;)V

    .line 138226
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->M:LX/2hU;

    const/16 v2, 0x2710

    invoke-virtual {v1, v0, v2}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ab:LX/4nS;

    .line 138227
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->v()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 138228
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ab:LX/4nS;

    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->v()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v1

    .line 138229
    iput-object v1, v0, LX/4nS;->i:Landroid/view/View;

    .line 138230
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ab:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->a()V

    goto :goto_0
.end method

.method private v()Lcom/facebook/widget/FbSwipeRefreshLayout;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 138233
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v0}, LX/3TO;->getSwipeRefreshLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v0}, LX/3TO;->getSwipeRefreshLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138234
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/2ub;)V
    .locals 6

    .prologue
    .line 138235
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ab:LX/4nS;

    if-eqz v0, :cond_0

    .line 138236
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ab:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    .line 138237
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->A:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-nez v0, :cond_1

    .line 138238
    :goto_0
    return-void

    .line 138239
    :cond_1
    const/4 v0, 0x0

    .line 138240
    iput-boolean v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->al:Z

    .line 138241
    sget-object v0, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    if-ne p1, v0, :cond_2

    .line 138242
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350005

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 138243
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->O:LX/2jO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2jO;->a(LX/DqC;)V

    .line 138244
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->J:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->A:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 138245
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->t:LX/0Sh;

    new-instance v2, LX/DsP;

    iget-object v3, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->A:LX/0SI;

    invoke-interface {v3}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 138246
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 138247
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LX/DsP;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;Ljava/lang/Long;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 138248
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->a(Lcom/facebook/notifications/widget/NotificationsFragment;Z)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 4

    .prologue
    const v3, 0xa0030

    .line 138249
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eq p1, v0, :cond_0

    .line 138250
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v0

    .line 138251
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/facebook/notifications/widget/PostFeedbackView;

    if-nez v0, :cond_0

    instance-of v0, p1, LX/3Cv;

    if-eqz v0, :cond_1

    check-cast p1, LX/3Cv;

    .line 138252
    iget-object v0, p1, LX/3Cv;->f:LX/3Cw;

    move-object v0, v0

    .line 138253
    sget-object v1, LX/3Cw;->NOTIFICATION:LX/3Cw;

    if-eq v0, v1, :cond_1

    .line 138254
    :cond_0
    :goto_0
    return-void

    .line 138255
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->L:LX/0id;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->a(Ljava/lang/String;)V

    .line 138256
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v3, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 138257
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_PermalinkNotificationLoad"

    const-string v2, "NotificationsFragment"

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->e(ILjava/lang/String;Ljava/lang/String;)V

    .line 138258
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0, p2}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 138259
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 138260
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->H:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 138261
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v2

    .line 138262
    if-nez v2, :cond_3

    :goto_1
    move v2, p2

    .line 138263
    if-nez v1, :cond_2

    .line 138264
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->r:LX/1rn;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/1rn;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 138265
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->t:LX/0Sh;

    new-instance v3, LX/DsI;

    invoke-direct {v3, p0, v2}, LX/DsI;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;I)V

    invoke-virtual {v1, v0, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0

    .line 138266
    :cond_2
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/facebook/notifications/widget/NotificationsFragment;->a$redex0(Lcom/facebook/notifications/widget/NotificationsFragment;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    goto :goto_0

    :cond_3
    invoke-interface {v2}, LX/0g8;->t()I

    move-result v2

    sub-int/2addr p2, v2

    goto :goto_1
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 138267
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    .line 138268
    iget-object v1, v0, LX/DsH;->g:LX/DsF;

    .line 138269
    iget-object v0, v1, LX/DsF;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 138270
    invoke-virtual {v1, p1}, LX/DsF;->c(Ljava/util/Collection;)V

    .line 138271
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    const v1, 0x77c5e1a1

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 138272
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138067
    if-eqz p1, :cond_2

    :goto_0
    iput-object p1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ao:Ljava/util/List;

    .line 138068
    const/4 v0, 0x0

    .line 138069
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ao:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 138070
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 138071
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0, p1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 138072
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 138073
    goto :goto_1

    .line 138074
    :cond_0
    move v0, v1

    .line 138075
    if-lez v0, :cond_1

    .line 138076
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->N:LX/3D1;

    invoke-virtual {v0}, LX/3D1;->a()V

    .line 138077
    const-string v0, "notification_auto_update_list_update"

    const-string v1, "new_notifications_notif_fragment"

    .line 138078
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "notifications"

    .line 138079
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 138080
    move-object v2, v2

    .line 138081
    const-string p1, "reason"

    invoke-virtual {v2, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 138082
    iget-object p1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->u:LX/0Zb;

    invoke-interface {p1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 138083
    :cond_1
    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->i(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    .line 138084
    return-void

    .line 138085
    :cond_2
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137902
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 137903
    const-string v1, "jewel_count"

    iget v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ak:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137904
    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 137900
    sget-object v0, LX/2ub;->FRAGMENT_LOADED:LX/2ub;

    invoke-virtual {p0, v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->a(LX/2ub;)V

    .line 137901
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 137897
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 137898
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->d(I)V

    .line 137899
    :cond_0
    return-void
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137874
    iget-boolean v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->X:Z

    if-nez v0, :cond_0

    .line 137875
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 137876
    :goto_0
    return-object v0

    .line 137877
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 137878
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {v0}, LX/DsH;->c()I

    move-result v0

    .line 137879
    :goto_1
    const-string v1, "\nLoaded Notifications: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 137880
    const-string v1, "\nLast Load Time: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->W:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 137881
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ai:I

    if-ltz v1, :cond_5

    iget v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ah:I

    if-ltz v1, :cond_5

    .line 137882
    const-string v1, "\nVisible Notification Ids: [\n"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137883
    iget v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ah:I

    iget v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ai:I

    add-int/2addr v1, v2

    .line 137884
    if-le v1, v0, :cond_1

    move v1, v0

    .line 137885
    :cond_1
    iget v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ah:I

    move v2, v0

    :goto_2
    if-ge v2, v1, :cond_4

    .line 137886
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {v0, v2}, LX/DsH;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 137887
    if-eqz v0, :cond_3

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 137888
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137889
    :goto_3
    const-string v0, "\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137890
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 137891
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 137892
    :cond_3
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 137893
    :cond_4
    const-string v0, "]\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137894
    :cond_5
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->l:LX/1qv;

    if-eqz v0, :cond_6

    .line 137895
    const-string v0, "Last Updated Time: "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->l:LX/1qv;

    invoke-virtual {v1}, LX/1qv;->a()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137896
    :cond_6
    const-string v0, "NotificationsFragment"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 137873
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 137872
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v0}, LX/3TO;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v0}, LX/3TO;->getScrollingViewProxy()LX/0g8;

    move-result-object v0

    goto :goto_0
.end method

.method public final kn_()V
    .locals 2

    .prologue
    .line 138058
    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->m(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    .line 138059
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/DsH;->a(Ljava/util/Collection;)V

    .line 138060
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    const v1, 0x1194c140

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 138061
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->a(Lcom/facebook/notifications/widget/NotificationsFragment;Z)V

    .line 138062
    return-void
.end method

.method public final ko_()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 137861
    iget-boolean v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->am:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->G:LX/3TL;

    .line 137862
    iget-boolean v1, v0, LX/3TL;->j:Z

    move v0, v1

    .line 137863
    if-eqz v0, :cond_0

    .line 137864
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x350009

    const-string v2, "NotifListLoadTimeCold"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 137865
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x35000a

    const-string v2, "NotifListLoadTimeWarm"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 137866
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    const-string v3, "load_type"

    const-string v4, "hot"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 137867
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 137868
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    const-string v3, "load_type"

    const-string v4, "hot"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 137869
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 137870
    iput-boolean v5, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->am:Z

    .line 137871
    :cond_0
    return v5
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x73b936a8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137856
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 137857
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->E:LX/16I;

    sget-object v2, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v3, Lcom/facebook/notifications/widget/NotificationsFragment$9;

    invoke-direct {v3, p0}, Lcom/facebook/notifications/widget/NotificationsFragment$9;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    invoke-virtual {v1, v2, v3}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ag:LX/0Yb;

    .line 137858
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    new-instance v2, LX/DsQ;

    invoke-direct {v2, p0}, LX/DsQ;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    new-instance v3, LX/DsR;

    invoke-direct {v3, p0}, LX/DsR;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    invoke-interface {v1, v2, v3}, LX/3TO;->a(Landroid/view/View$OnClickListener;LX/1DI;)V

    .line 137859
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {p0, v1}, Landroid/support/v4/app/ListFragment;->a(Landroid/widget/ListAdapter;)V

    .line 137860
    const/16 v1, 0x2b

    const v2, 0x71c29efe

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 137851
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onAttach(Landroid/content/Context;)V

    .line 137852
    iput-object p1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Y:Landroid/content/Context;

    .line 137853
    instance-of v0, p1, LX/DsV;

    if-eqz v0, :cond_0

    .line 137854
    check-cast p1, LX/DsV;

    iput-object p1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Z:LX/DsV;

    .line 137855
    :cond_0
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 137846
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 137847
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    invoke-static {v0}, LX/62G;->a(LX/0g8;)V

    .line 137848
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    if-eqz v0, :cond_0

    .line 137849
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v0}, LX/3TO;->a()V

    .line 137850
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x7edac28d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 137905
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 137906
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/notifications/widget/NotificationsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 137907
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 137908
    if-nez v0, :cond_2

    move-object v0, v7

    :goto_0
    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->U:Ljava/lang/String;

    .line 137909
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->U:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137910
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081153

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->U:Ljava/lang/String;

    .line 137911
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    if-nez v0, :cond_1

    .line 137912
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DsH;

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    .line 137913
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->D:LX/33b;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    .line 137914
    iget-object v3, v2, LX/DsH;->c:LX/3Cl;

    move-object v2, v3

    .line 137915
    iget-object v3, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Y:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/3Cl;->a(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/33b;->a(Landroid/view/Display;I)I

    move-result v6

    .line 137916
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->G:LX/3TL;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->A:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->m:LX/2A8;

    iget-object v5, v1, LX/2A8;->b:Landroid/net/Uri;

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, LX/3TL;->a(LX/0o6;LX/0k5;Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/net/Uri;ILjava/lang/String;)V

    .line 137917
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->q:LX/193;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_notification_classic_scroll_perf"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ae:LX/195;

    .line 137918
    const v0, 0x5d35ebaa

    invoke-static {v0, v8}, LX/02F;->f(II)V

    return-void

    .line 137919
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 137920
    const-string v1, "fragment_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x364d27b2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 137921
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 137922
    new-instance v1, LX/3TN;

    invoke-direct {v1, v0}, LX/3TN;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 137923
    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    .line 137924
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    check-cast v0, Landroid/view/View;

    const-string v2, "jewel_popup_notifications"

    invoke-static {v0, v2, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 137925
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->v()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 137926
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->v()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v0

    new-instance v1, LX/DsS;

    invoke-direct {v1, p0}, LX/DsS;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 137927
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v0}, LX/3TO;->getRefreshableContainerLike()LX/62k;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 137928
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v0}, LX/3TO;->getRefreshableContainerLike()LX/62k;

    move-result-object v0

    new-instance v1, LX/DsT;

    invoke-direct {v1, p0}, LX/DsT;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    invoke-interface {v0, v1}, LX/62k;->setOnRefreshListener(LX/62n;)V

    .line 137929
    :cond_1
    new-instance v0, LX/DsW;

    invoke-direct {v0, p0}, LX/DsW;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->af:LX/DsW;

    .line 137930
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 137931
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->af:LX/DsW;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 137932
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    new-instance v1, LX/2je;

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ae:LX/195;

    invoke-direct {v1, v2}, LX/2je;-><init>(LX/195;)V

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 137933
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0, p0}, LX/0g8;->a(LX/0fu;)V

    .line 137934
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->s:LX/2ix;

    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2ix;->a(LX/0g8;)V

    .line 137935
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->H:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0, p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0o5;)V

    .line 137936
    iput-boolean v5, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->X:Z

    .line 137937
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 137938
    const v0, 0x7f030c34

    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v1

    invoke-interface {v1}, LX/0g8;->b()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 137939
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v0, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 137940
    :cond_3
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Q:LX/2PY;

    invoke-virtual {v0}, LX/2PY;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 137941
    new-instance v0, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Y:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;-><init>(Landroid/content/Context;)V

    .line 137942
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Y:Landroid/content/Context;

    const v2, 0x7f081153

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 137943
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Q:LX/2PY;

    .line 137944
    iget-object v2, v1, LX/2PY;->a:LX/0ad;

    sget-short v3, LX/15r;->u:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v1, v2

    .line 137945
    if-eqz v1, :cond_4

    .line 137946
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Y:Landroid/content/Context;

    const v2, 0x7f08117d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setActionText(Ljava/lang/CharSequence;)V

    .line 137947
    new-instance v1, LX/DsN;

    invoke-direct {v1, p0}, LX/DsN;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137948
    :cond_4
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v1}, LX/3TO;->getListView()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 137949
    :cond_5
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->H:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v4, "cold"

    .line 137950
    :goto_0
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->f:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->h:Ljava/lang/String;

    const-string v3, "load_type"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 137951
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget v1, v1, Lcom/facebook/apptab/state/TabTag;->g:I

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->i:Ljava/lang/String;

    const-string v3, "load_type"

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 137952
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 137953
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->H:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 137954
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    const v2, 0x350009

    const-string v3, "NotifListLoadTimeCold"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    sget-object v3, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v3, v3, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 137955
    iput-boolean v5, v1, LX/0Yj;->n:Z

    .line 137956
    move-object v1, v1

    .line 137957
    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 137958
    :goto_1
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000d

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/util/Collection;)V

    .line 137959
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350005

    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/util/Collection;)V

    .line 137960
    iput-boolean v5, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->am:Z

    .line 137961
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 137962
    const/4 v0, 0x1

    move v0, v0

    .line 137963
    if-eqz v0, :cond_7

    .line 137964
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    new-instance v1, LX/DsU;

    invoke-direct {v1, p0}, LX/DsU;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/3TV;)V

    .line 137965
    :cond_7
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    check-cast v0, Landroid/view/View;

    const v1, -0x26aa5225

    invoke-static {v1, v6}, LX/02F;->f(II)V

    return-object v0

    .line 137966
    :cond_8
    const-string v4, "warm"

    goto/16 :goto_0

    .line 137967
    :cond_9
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    const v2, 0x35000a

    const-string v3, "NotifListLoadTimeWarm"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    sget-object v3, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v3, v3, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 137968
    iput-boolean v5, v1, LX/0Yj;->n:Z

    .line 137969
    move-object v1, v1

    .line 137970
    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    goto :goto_1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x33f9e328    # -3.5156832E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137971
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDestroyView()V

    .line 137972
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ag:LX/0Yb;

    if-eqz v1, :cond_0

    .line 137973
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ag:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 137974
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    if-eqz v1, :cond_1

    .line 137975
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->H:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v1, p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(LX/0o5;)V

    .line 137976
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 137977
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->af:LX/DsW;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 137978
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v1

    invoke-interface {v1}, LX/0g8;->w()V

    .line 137979
    :cond_2
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->v()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 137980
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->v()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 137981
    :cond_3
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v1}, LX/3TO;->getRefreshableContainerLike()LX/62k;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 137982
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    invoke-interface {v1}, LX/3TO;->getRefreshableContainerLike()LX/62k;

    move-result-object v1

    invoke-interface {v1, v3}, LX/62k;->setOnRefreshListener(LX/62n;)V

    .line 137983
    :cond_4
    iput-object v3, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    .line 137984
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x350009

    const-string v3, "NotifListLoadTimeCold"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 137985
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x35000a

    const-string v3, "NotifListLoadTimeWarm"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 137986
    const/16 v1, 0x2b

    const v2, 0x65a9e486

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetach()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5fd484cb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137987
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDetach()V

    .line 137988
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->Z:LX/DsV;

    .line 137989
    const/16 v1, 0x2b

    const v2, -0x1e524f1d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x62ef526

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 137990
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onPause()V

    .line 137991
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->t()V

    .line 137992
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->O:LX/2jO;

    const/4 v2, 0x0

    .line 137993
    iput-object v2, v1, LX/2jO;->h:Ljava/lang/Runnable;

    .line 137994
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->B:LX/33Z;

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ac:LX/CSB;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 137995
    const/4 v1, 0x0

    .line 137996
    sput-boolean v1, LX/3Cc;->a:Z

    .line 137997
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    .line 137998
    iget-object v2, v1, LX/DsH;->b:LX/DqA;

    const/4 v4, 0x0

    .line 137999
    iput-object v4, v2, LX/DqA;->l:Ljava/lang/Runnable;

    .line 138000
    iget-object v2, v1, LX/DsH;->b:LX/DqA;

    .line 138001
    iget-object v4, v2, LX/DqA;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p0, LX/0hM;->J:LX/0Tn;

    iget-object v1, v2, LX/DqA;->d:LX/0dN;

    invoke-interface {v4, p0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;LX/0dN;)V

    .line 138002
    const/16 v1, 0x2b

    const v2, -0x57ba80bb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x2a

    const v1, 0x460a4875

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 138003
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onResume()V

    .line 138004
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0030

    const-string v3, "NNF_PermalinkNotificationLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 138005
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->o()V

    .line 138006
    const/4 v1, 0x0

    .line 138007
    iput-boolean v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->al:Z

    .line 138008
    invoke-static {p0, v4}, Lcom/facebook/notifications/widget/NotificationsFragment;->a(Lcom/facebook/notifications/widget/NotificationsFragment;Z)V

    .line 138009
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->O:LX/2jO;

    new-instance v2, Lcom/facebook/notifications/widget/NotificationsFragment$2;

    invoke-direct {v2, p0}, Lcom/facebook/notifications/widget/NotificationsFragment$2;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    .line 138010
    iput-object v2, v1, LX/2jO;->h:Ljava/lang/Runnable;

    .line 138011
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->B:LX/33Z;

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ac:LX/CSB;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 138012
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138013
    sput-boolean v4, LX/3Cc;->a:Z

    .line 138014
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    .line 138015
    iget-object v2, v1, LX/DsH;->b:LX/DqA;

    new-instance v3, Lcom/facebook/notifications/widget/NotificationsAdapter$1;

    invoke-direct {v3, v1}, Lcom/facebook/notifications/widget/NotificationsAdapter$1;-><init>(LX/DsH;)V

    .line 138016
    iput-object v3, v2, LX/DqA;->l:Ljava/lang/Runnable;

    .line 138017
    iget-object v2, v1, LX/DsH;->b:LX/DqA;

    .line 138018
    iget-object v3, v2, LX/DqA;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0hM;->J:LX/0Tn;

    iget-object v1, v2, LX/DqA;->d:LX/0dN;

    invoke-interface {v3, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 138019
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/DsH;->a(Z)V

    .line 138020
    const/16 v1, 0x2b

    const v2, -0x2dc8bb0e

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x559c9c01

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 138021
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onStart()V

    .line 138022
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aj:I

    .line 138023
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbListFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 138024
    if-eqz v1, :cond_0

    .line 138025
    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->U:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 138026
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 138027
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x63213617

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x20418c50

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 138028
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onStop()V

    .line 138029
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->O:LX/2jO;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/2jO;->a(LX/DqC;)V

    .line 138030
    const/16 v1, 0x2b

    const v2, 0x242b251f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 4

    .prologue
    .line 138031
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    .line 138032
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->setUserVisibleHint(Z)V

    .line 138033
    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 138034
    :cond_0
    :goto_0
    return-void

    .line 138035
    :cond_1
    if-eqz p1, :cond_5

    .line 138036
    invoke-static {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->p(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    .line 138037
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    if-eqz v0, :cond_2

    .line 138038
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->o()V

    .line 138039
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/DsH;->a(Z)V

    .line 138040
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->K:LX/0xB;

    sget-object v1, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ak:I

    .line 138041
    iget v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ak:I

    if-lez v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 138042
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0}, LX/0g8;->v()V

    .line 138043
    :cond_3
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->p:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aq:LX/0am;

    .line 138044
    :cond_4
    :goto_1
    iput-boolean p1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->am:Z

    .line 138045
    sput-boolean p1, LX/3Cc;->a:Z

    .line 138046
    goto :goto_0

    .line 138047
    :cond_5
    invoke-direct {p0}, Lcom/facebook/notifications/widget/NotificationsFragment;->t()V

    .line 138048
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->S:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "time_user_exit"

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->p:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v0

    .line 138049
    iget-object v1, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aq:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 138050
    const-string v1, "time_user_entered"

    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->aq:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    .line 138051
    :cond_6
    const-string v1, "1744531889109723"

    .line 138052
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 138053
    move-object v0, v0

    .line 138054
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    .line 138055
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->O:LX/2jO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2jO;->a(LX/DqC;)V

    .line 138056
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ab:LX/4nS;

    if-eqz v0, :cond_4

    .line 138057
    iget-object v0, p0, Lcom/facebook/notifications/widget/NotificationsFragment;->ab:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    goto :goto_1
.end method
