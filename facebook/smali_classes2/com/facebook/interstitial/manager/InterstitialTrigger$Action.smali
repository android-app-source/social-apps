.class public final enum Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/interstitial/manager/InterstitialTrigger_ActionDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum ACTIVITY_CONFIGURATION_CHANGED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum ACTIVITY_CREATED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum ADMIN_GROUP_ADS_ELIGIBLE_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum ADMIN_GROUP_MALL_MULTITIER_ENABLED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum AD_INTERFACES_ADD_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum AD_INTERFACES_LINK_OBJECTIVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum AD_INTERFACES_VIEW_RESULTS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum APP_FOREGROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum APP_INVITE_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum APP_INVITE_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum BACKSTAGE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum BASIC_SERVICES_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum BIRTHDAY_QUICK_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum BOOKMARK_TAB_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum BUILT_IN_BROWSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum CHANNEL_FEED_SAVE_OVERLAY_BUTTON_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum CHOOSE_LOVE_REACTION:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum COMMENT_UFI_LIKE_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum DIVEBAR_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_COVER_PHOTO_SELECTOR_THEME_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_CREATION_LOCKED_PRIVACY_EDUCATION_WIDGET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_EXTENDED_INVITE_ADD_NOTE_BUTTON_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_EXTENDED_INVITE_ENTRYPOINT_FB_NONSYNC_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_EXTENDED_INVITE_ENTRYPOINT_FB_SYNC_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_EXTENDED_INVITE_ENTRYPOINT_OFF_FB_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_EXTENDED_INVITE_IMPORT_CONTACTS_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_EXTENDED_INVITE_LANNISTER_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_EXTENDED_INVITE_NOTE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_GUEST_LIST_INVITED_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENTS_INVITE_THROUGH_MESSENGER_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENT_REMINDER_ENTRY_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum EVENT_TICKETING_SEAT_SELECTION_NOTE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FACECAST_BROADCAST_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FACECAST_INLINE_SPROUT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEATHER_OVERLAY_SHOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEEDBACK_COMPOSER_INIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEED_INLINE_COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEED_PYMK_FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEED_PYMK_SCROLLED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEED_PYMK_XOUTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEED_STORY_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEED_STORY_CARET_AUTOPLAY_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEED_STORY_CARET_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEED_STORY_LOADED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FEED_STORY_ONLY_ME_SHARE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FIRST_NEWSFEED_AFTER_LOGIN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUPS_DISCOVER_TAB_TOOL_TIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUP_ADD_MODERATOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUP_ADS_ELIGIBLE_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUP_CREATE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUP_INFO_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_RANK_FEED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_TYPE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUP_ROOMS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUP_SCOPED_SEARCH:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum GROUP_SEEDS_COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum HEADING_INDICATOR_SHOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum INSPIRATION_CAMERA:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum INSPIRATION_CAMERA_IN_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum INSPIRATION_CAPTURE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum INSPIRATION_SHARE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum INSTANT_ARTICLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum INSTANT_SHOPPING_SAVE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum ITEM_SAVED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum ITEM_SAVED_IN_CHECKIN_STORY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum ITEM_SAVED_IN_NOTIFICATIONS_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum LEAD_GEN_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MAPS_LAUNCH_EXTERNAL_MAP_APP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MEDIA_GALLERY_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MEMBER_BIO_ELIGIBLE_GROUP_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSAGES_DIODE_CANONICAL_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSAGES_DIODE_CREATE_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSAGES_DIODE_GROUP_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSAGES_DIODE_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSAGES_DIODE_TAB_BADGEABLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_APP_DID_BECOME_ACTIVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_AUDIO_CLIP_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_COMPOSE_NEW_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_DID_SHOW_OMNI_M_SUGGESTIONS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_LAUNCH_EXTERNAL_URL:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_NEW_USER_SETUP_COMPLETE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_RECENT_THREAD_LIST:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_RTC_PRESENCE_CHANGE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_THREAD_LIST:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_THREAD_LIST_BLOCKING:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_THREAD_LIST_PTR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_THREAD_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_THREAD_SETTINGS_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MESSENGER_VIEW_HOME:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MINUTIAE_TAG_PICKER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum MOD_GROUP_MALL_MULTITIER_ENABLED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum NEARBY_FRIENDS_WAVE_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum NEWSFEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum NEWSFEED_USER_REFRESH:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum NEXT_NOTIFICATION_UI_RENDERED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum NOTIFICATIONS_ADAPTER_CREATION:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum NOW_COMPOSER_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum NOW_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum NOW_PUBLISHED_STATUS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum OFFLINE_FEED_AIRPORT_TRIGGER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum OFFLINE_FEED_BOOKMARK_TRIGGER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGES_MANAGER_ADS_MANAGER_BOOKMARK_TAPPED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGES_MANAGER_APP_COMMS_HUB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGES_MANAGER_APP_COMMS_HUB_ALL_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGES_MANAGER_APP_COMMS_HUB_FACEBOOK_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGES_MANAGER_APP_COMMS_HUB_INSTAGRAM_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGES_MANAGER_APP_COMMS_HUB_MARK_AS_DONE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGES_MANAGER_APP_COMMS_HUB_MOVE_TO_FOLLOW_UP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGES_MANAGER_APP_DID_BECOME_ACTIVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGES_MANAGER_APP_MORE_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGES_MANAGER_APP_OPENED_NEW_PAGE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGE_ACTIONBAR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGE_ADMIN_OVERVIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGE_ADMIN_TIMELINE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGE_ADMIN_TIMELINE_VIEW_VERIFY_ELIGIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGE_CALL_TO_ACTION_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGE_CONTACT_INBOX_ENTRY_POINT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGE_NONADMIN_TIMELINE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PAGE_STORY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PEOPLE_HIGHLIGHTS_TAB_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PERMALINK_DRAFT_SAVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PERMALINK_STORY_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PERSONAL_PROFILE_FRIEND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PERSONAL_PROFILE_NONFRIEND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PERSONAL_PROFILE_OWNER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PHOTO_ATTATCHMENT_PRODUCT_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PHOTO_PICKER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PHOTO_PICKER_DETECTED_RECENT_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PHOTO_PICKER_HIGHLIGHT_CLUSTER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum POLITICAL_PIVOT_FOLLOW_UP_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum POST_CREATED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum PROFILE_FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum REQUEST_TAB_FRIENDING_ACTION_PERFORMED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SAVED_DASHBOARD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SAVED_MAIN_TAB_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SEARCH_BAR_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SEARCH_NULL_STATE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SEARCH_TUTORIAL_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SEE_FIRST_INDICATOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SHARESHEET_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SIMPLE_SEARCH_CLEAR_TEXT_ICON_CLICK:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SOCIAL_SEARCH_COMMENTS_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SOCIAL_SEARCH_REMOVE_PLACE_H_SCROLL:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SOCIAL_SEARCH_SEEKER_FEED_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SOUVENIR_EDIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SPATIAL_AUDIO_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum SPECIFIC_IDS_GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum STAGING_GROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TAB_NAVIGATION_ATTACHED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TAB_NAVIGATION_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TAB_NAVIGATION_FRIEND_REQUESTS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TAB_NAVIGATION_MESSAGES:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TAB_NAVIGATION_MORE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TAB_NAVIGATION_NOTIFICATIONS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TAROT_END_CARD_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum THREAD_LIST_INTERSTITIAL_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum THREAD_LIST_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TIMELINE_FEATURED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TIMELINE_INTRO_CARD_BIO_POST_TO_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TIMELINE_INTRO_CARD_FAV_PHOTOS_POST_TO_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TIMELINE_INTRO_CARD_SUGGESTED_BIO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TIMELINE_INTRO_CARD_SUGGESTED_PHOTOS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TIMELINE_INTRO_CAR_FAV_PHOTOS_WYSWIG_EDITOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TIMELINE_INTRO_CAR_FAV_PHOTOS_WYSWIG_EDITOR_DRAG_AND_DROP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum TO_ADS_MANAGER_M_SITE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum UFI_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum UFI_LONG_PRESSED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum UNFOLLOW_IN_NFX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum UNKNOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_AUTOPLAY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_DOWNLOAD_BUTTON_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_DOWNLOAD_COMPLETE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_DOWNLOAD_NOTIFICATION_FIRED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_DOWNLOAD_STARTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_HOME_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_HOME_NOTIF_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_HOME_TAB_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_QUALITY_LABEL_INLINE_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_STALLING:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VIDEO_WATCHED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VOIP_CALL_END:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

.field public static final enum VOIP_CALL_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 120374
    const-class v0, Lcom/facebook/interstitial/manager/InterstitialTrigger_ActionDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 120186
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "CHOOSE_LOVE_REACTION"

    invoke-direct {v0, v1, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->CHOOSE_LOVE_REACTION:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120187
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEED_STORY_LOADED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_LOADED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120188
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "NEWSFEED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEWSFEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120189
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEED_STORY_CARET"

    invoke-direct {v0, v1, v6}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120190
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEED_STORY_CARET_VIDEO"

    invoke-direct {v0, v1, v7}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120191
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEED_STORY_CARET_AUTOPLAY_VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET_AUTOPLAY_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120192
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEED_STORY_ONLY_ME_SHARE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_ONLY_ME_SHARE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120193
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_APP_DID_BECOME_ACTIVE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_APP_DID_BECOME_ACTIVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120194
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_NEW_USER_SETUP_COMPLETE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_NEW_USER_SETUP_COMPLETE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120195
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_THREAD_LIST_BLOCKING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_THREAD_LIST_BLOCKING:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120196
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_THREAD_LIST"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_THREAD_LIST:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120197
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_THREAD_LIST_PTR"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_THREAD_LIST_PTR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120198
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_RECENT_THREAD_LIST"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_RECENT_THREAD_LIST:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120199
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_THREAD_OPEN"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_THREAD_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120200
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_THREAD_SETTINGS_OPEN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_THREAD_SETTINGS_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120201
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_COMPOSE_NEW_THREAD"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_COMPOSE_NEW_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120202
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_AUDIO_CLIP_VISIBLE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_AUDIO_CLIP_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120203
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_DID_SHOW_OMNI_M_SUGGESTIONS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_DID_SHOW_OMNI_M_SUGGESTIONS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120204
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_RTC_PRESENCE_CHANGE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_RTC_PRESENCE_CHANGE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120205
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_LAUNCH_EXTERNAL_URL"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_LAUNCH_EXTERNAL_URL:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120206
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSENGER_VIEW_HOME"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_VIEW_HOME:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120207
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MAPS_LAUNCH_EXTERNAL_MAP_APP"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MAPS_LAUNCH_EXTERNAL_MAP_APP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120208
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGES_MANAGER_APP_COMMS_HUB"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120209
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGES_MANAGER_APP_COMMS_HUB_MARK_AS_DONE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB_MARK_AS_DONE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120210
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGES_MANAGER_APP_COMMS_HUB_MOVE_TO_FOLLOW_UP"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB_MOVE_TO_FOLLOW_UP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120211
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGES_MANAGER_APP_COMMS_HUB_ALL_TAB"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB_ALL_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120212
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGES_MANAGER_APP_COMMS_HUB_FACEBOOK_TAB"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB_FACEBOOK_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120213
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGES_MANAGER_APP_COMMS_HUB_INSTAGRAM_TAB"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB_INSTAGRAM_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120214
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGES_MANAGER_APP_DID_BECOME_ACTIVE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_DID_BECOME_ACTIVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120215
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGES_MANAGER_APP_OPENED_NEW_PAGE"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_OPENED_NEW_PAGE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120216
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGES_MANAGER_APP_MORE_TAB"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_MORE_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120217
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGES_MANAGER_ADS_MANAGER_BOOKMARK_TAPPED"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_ADS_MANAGER_BOOKMARK_TAPPED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120218
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VOIP_CALL_START"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VOIP_CALL_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120219
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VOIP_CALL_END"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VOIP_CALL_END:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120220
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "ACTIVITY_CONFIGURATION_CHANGED"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ACTIVITY_CONFIGURATION_CHANGED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120221
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "ACTIVITY_CREATED"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ACTIVITY_CREATED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120222
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "APP_FOREGROUND"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_FOREGROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120223
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "BASIC_SERVICES_OPEN"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BASIC_SERVICES_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120224
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "BUILT_IN_BROWSER"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BUILT_IN_BROWSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120225
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "COMPOSER"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120226
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "DIVEBAR_OPEN"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->DIVEBAR_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120227
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "BACKSTAGE_NUX"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BACKSTAGE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120228
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FRIEND_REQUEST_SENT"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120229
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MINUTIAE_TAG_PICKER"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MINUTIAE_TAG_PICKER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120230
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PHOTO_PICKER"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120231
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PHOTO_PICKER_DETECTED_RECENT_VIDEO"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER_DETECTED_RECENT_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120232
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PHOTO_PICKER_HIGHLIGHT_CLUSTER"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER_HIGHLIGHT_CLUSTER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120233
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "NEWSFEED_USER_REFRESH"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEWSFEED_USER_REFRESH:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120234
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGE_ACTIONBAR"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ACTIONBAR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120235
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGE_ADMIN_TIMELINE_VIEW"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ADMIN_TIMELINE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120236
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGE_NONADMIN_TIMELINE_VIEW"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_NONADMIN_TIMELINE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120237
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGE_ADMIN_TIMELINE_VIEW_VERIFY_ELIGIBLE"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ADMIN_TIMELINE_VIEW_VERIFY_ELIGIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120238
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGE_ADMIN_OVERVIEW"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ADMIN_OVERVIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120239
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGE_STORY"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_STORY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120240
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGE_CALL_TO_ACTION_BUTTON"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_CALL_TO_ACTION_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120241
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PAGE_CONTACT_INBOX_ENTRY_POINT"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_CONTACT_INBOX_ENTRY_POINT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120242
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PEOPLE_HIGHLIGHTS_TAB_START"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PEOPLE_HIGHLIGHTS_TAB_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120243
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "POST_CREATED"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->POST_CREATED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120244
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PROFILE_FRIEND_REQUEST_SENT"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PROFILE_FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120245
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PERSONAL_PROFILE_OWNER"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERSONAL_PROFILE_OWNER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120246
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PERSONAL_PROFILE_FRIEND"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERSONAL_PROFILE_FRIEND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120247
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PERSONAL_PROFILE_NONFRIEND"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERSONAL_PROFILE_NONFRIEND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120248
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SAVED_DASHBOARD_START"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_DASHBOARD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120249
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SAVED_MAIN_TAB_VISIBLE"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_MAIN_TAB_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120250
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "ITEM_SAVED"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ITEM_SAVED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120251
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "ITEM_SAVED_IN_CHECKIN_STORY"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ITEM_SAVED_IN_CHECKIN_STORY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120252
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "ITEM_SAVED_IN_NOTIFICATIONS_TAB"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ITEM_SAVED_IN_NOTIFICATIONS_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120253
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "CHANNEL_FEED_SAVE_OVERLAY_BUTTON_VISIBLE"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->CHANNEL_FEED_SAVE_OVERLAY_BUTTON_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120254
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SESSION_COLD_START"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120255
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SIMPLE_SEARCH_CLEAR_TEXT_ICON_CLICK"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SIMPLE_SEARCH_CLEAR_TEXT_ICON_CLICK:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120256
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SEARCH_BAR_TOOLTIP"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEARCH_BAR_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120257
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SEARCH_NULL_STATE"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEARCH_NULL_STATE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120258
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SEARCH_TUTORIAL_NUX"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEARCH_TUTORIAL_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120259
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TAB_NAVIGATION_FEED"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120260
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TAB_NAVIGATION_MESSAGES"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_MESSAGES:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120261
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "THREAD_LIST_INTERSTITIAL_OPEN"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->THREAD_LIST_INTERSTITIAL_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120262
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TAB_NAVIGATION_NOTIFICATIONS"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_NOTIFICATIONS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120263
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TAB_NAVIGATION_FRIEND_REQUESTS"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_FRIEND_REQUESTS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120264
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TAB_NAVIGATION_MORE"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_MORE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120265
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TAB_NAVIGATION_ATTACHED"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_ATTACHED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120266
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "BOOKMARK_TAB_OPEN"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BOOKMARK_TAB_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120267
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "REQUEST_TAB_FRIENDING_ACTION_PERFORMED"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->REQUEST_TAB_FRIENDING_ACTION_PERFORMED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120268
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEED_PYMK_FRIEND_REQUEST_SENT"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_PYMK_FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120269
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEED_PYMK_SCROLLED"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_PYMK_SCROLLED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120270
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEED_PYMK_XOUTED"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_PYMK_XOUTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120271
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "THREAD_LIST_OPEN"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->THREAD_LIST_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120272
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "UNFOLLOW_IN_NFX"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UNFOLLOW_IN_NFX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120273
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TIMELINE"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120274
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "STAGING_GROUND"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->STAGING_GROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120275
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TIMELINE_INTRO_CARD_SUGGESTED_PHOTOS"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CARD_SUGGESTED_PHOTOS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120276
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TIMELINE_INTRO_CARD_SUGGESTED_BIO"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CARD_SUGGESTED_BIO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120277
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TIMELINE_INTRO_CARD_BIO_POST_TO_FEED"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CARD_BIO_POST_TO_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120278
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TIMELINE_INTRO_CARD_FAV_PHOTOS_POST_TO_FEED"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CARD_FAV_PHOTOS_POST_TO_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120279
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TIMELINE_FEATURED"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_FEATURED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120280
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TIMELINE_INTRO_CAR_FAV_PHOTOS_WYSWIG_EDITOR"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CAR_FAV_PHOTOS_WYSWIG_EDITOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120281
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TIMELINE_INTRO_CAR_FAV_PHOTOS_WYSWIG_EDITOR_DRAG_AND_DROP"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CAR_FAV_PHOTOS_WYSWIG_EDITOR_DRAG_AND_DROP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120282
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUP_CREATE_VIEW"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_CREATE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120283
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUP_INFO_VIEW"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_INFO_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120284
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUP_MALL_VIEW"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120285
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "ADMIN_GROUP_MALL_MULTITIER_ENABLED_VIEW"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ADMIN_GROUP_MALL_MULTITIER_ENABLED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120286
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MOD_GROUP_MALL_MULTITIER_ENABLED_VIEW"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MOD_GROUP_MALL_MULTITIER_ENABLED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120287
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SPECIFIC_IDS_GROUP_MALL_VIEW"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SPECIFIC_IDS_GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120288
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_TYPE_VIEW"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_TYPE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120289
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_RANK_FEED_VIEW"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_RANK_FEED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120290
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MEMBER_BIO_ELIGIBLE_GROUP_MALL_VISIT"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MEMBER_BIO_ELIGIBLE_GROUP_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120291
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUP_ADD_MODERATOR"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_ADD_MODERATOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120292
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUP_SCOPED_SEARCH"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_SCOPED_SEARCH:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120293
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUP_SEEDS_COMPOSER"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_SEEDS_COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120294
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUPS_DISCOVER_TAB_TOOL_TIP"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUPS_DISCOVER_TAB_TOOL_TIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120295
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUP_ADS_ELIGIBLE_MALL_VISIT"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_ADS_ELIGIBLE_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120296
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "ADMIN_GROUP_ADS_ELIGIBLE_MALL_VISIT"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ADMIN_GROUP_ADS_ELIGIBLE_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120297
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_HOME_TAB_TOOLTIP"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_HOME_TAB_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120298
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_HOME_NOTIF_TOOLTIP"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_HOME_NOTIF_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120299
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_HOME_FEED"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_HOME_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120300
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PERMALINK_STORY_OPEN"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERMALINK_STORY_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120301
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PERMALINK_DRAFT_SAVE"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERMALINK_DRAFT_SAVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120302
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "APP_INVITE_CARET"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_INVITE_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120303
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "APP_INVITE_FEED"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_INVITE_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120304
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSAGES_DIODE_CANONICAL_THREAD"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_CANONICAL_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120305
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSAGES_DIODE_GROUP_THREAD"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_GROUP_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120306
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSAGES_DIODE_CREATE_THREAD"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_CREATE_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120307
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSAGES_DIODE_TAB"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120308
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MESSAGES_DIODE_TAB_BADGEABLE"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_TAB_BADGEABLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120309
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SEE_FIRST_INDICATOR"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEE_FIRST_INDICATOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120310
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "LEAD_GEN_OPEN"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->LEAD_GEN_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120311
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "NOW_PUBLISHED_STATUS"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NOW_PUBLISHED_STATUS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120312
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "NOW_COMPOSER_OPENED"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NOW_COMPOSER_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120313
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "NOW_OPENED"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NOW_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120314
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FACECAST_BROADCAST_NUX"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FACECAST_BROADCAST_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120315
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FACECAST_INLINE_SPROUT"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FACECAST_INLINE_SPROUT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120316
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_AUTOPLAY"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_AUTOPLAY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120317
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_QUALITY_LABEL_INLINE_VISIBLE"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_QUALITY_LABEL_INLINE_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120318
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_DOWNLOAD_STARTED"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_STARTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120319
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_DOWNLOAD_COMPLETE"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_COMPLETE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120320
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_DOWNLOAD_BUTTON_VISIBLE"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_BUTTON_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120321
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_DOWNLOAD_NOTIFICATION_FIRED"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_NOTIFICATION_FIRED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120322
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_STALLING"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_STALLING:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120323
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "VIDEO_WATCHED"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_WATCHED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120324
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "MEDIA_GALLERY_OPENED"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MEDIA_GALLERY_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120325
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TO_ADS_MANAGER_M_SITE"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TO_ADS_MANAGER_M_SITE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120326
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEED_INLINE_COMPOSER"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_INLINE_COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120327
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_GUEST_LIST_INVITED_TAB"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_GUEST_LIST_INVITED_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120328
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "UFI_CLICKED"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UFI_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120329
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "UFI_LONG_PRESSED"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UFI_LONG_PRESSED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120330
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "COMMENT_UFI_LIKE_CLICKED"

    const/16 v2, 0x90

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMMENT_UFI_LIKE_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120331
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_CREATION_LOCKED_PRIVACY_EDUCATION_WIDGET"

    const/16 v2, 0x91

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_CREATION_LOCKED_PRIVACY_EDUCATION_WIDGET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120332
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_COVER_PHOTO_SELECTOR_THEME_NUX"

    const/16 v2, 0x92

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_COVER_PHOTO_SELECTOR_THEME_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120333
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "BIRTHDAY_QUICK_VIDEO"

    const/16 v2, 0x93

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BIRTHDAY_QUICK_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120334
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "INSTANT_ARTICLE"

    const/16 v2, 0x94

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSTANT_ARTICLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120335
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "INSTANT_SHOPPING_SAVE_NUX"

    const/16 v2, 0x95

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSTANT_SHOPPING_SAVE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120336
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FIRST_NEWSFEED_AFTER_LOGIN"

    const/16 v2, 0x96

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FIRST_NEWSFEED_AFTER_LOGIN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120337
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_INVITE_THROUGH_MESSENGER_NUX"

    const/16 v2, 0x97

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_INVITE_THROUGH_MESSENGER_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120338
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_EXTENDED_INVITE_IMPORT_CONTACTS_NUX"

    const/16 v2, 0x98

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_IMPORT_CONTACTS_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120339
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_EXTENDED_INVITE_ADD_NOTE_BUTTON_NUX"

    const/16 v2, 0x99

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_ADD_NOTE_BUTTON_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120340
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_EXTENDED_INVITE_NOTE_NUX"

    const/16 v2, 0x9a

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_NOTE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120341
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_EXTENDED_INVITE_LANNISTER_NUX"

    const/16 v2, 0x9b

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_LANNISTER_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120342
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_EXTENDED_INVITE_ENTRYPOINT_FB_NONSYNC_NUX"

    const/16 v2, 0x9c

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_ENTRYPOINT_FB_NONSYNC_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120343
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_EXTENDED_INVITE_ENTRYPOINT_FB_SYNC_NUX"

    const/16 v2, 0x9d

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_ENTRYPOINT_FB_SYNC_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120344
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENTS_EXTENDED_INVITE_ENTRYPOINT_OFF_FB_NUX"

    const/16 v2, 0x9e

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_ENTRYPOINT_OFF_FB_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120345
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENT_REMINDER_ENTRY_NUX"

    const/16 v2, 0x9f

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENT_REMINDER_ENTRY_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120346
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "EVENT_TICKETING_SEAT_SELECTION_NOTE_NUX"

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENT_TICKETING_SEAT_SELECTION_NOTE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120347
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "AD_INTERFACES_VIEW_RESULTS"

    const/16 v2, 0xa1

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->AD_INTERFACES_VIEW_RESULTS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120348
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SOUVENIR_EDIT"

    const/16 v2, 0xa2

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOUVENIR_EDIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120349
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SHARESHEET_NUX"

    const/16 v2, 0xa3

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SHARESHEET_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120350
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "NOTIFICATIONS_ADAPTER_CREATION"

    const/16 v2, 0xa4

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NOTIFICATIONS_ADAPTER_CREATION:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120351
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEEDBACK_COMPOSER_INIT"

    const/16 v2, 0xa5

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEEDBACK_COMPOSER_INIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120352
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "FEATHER_OVERLAY_SHOWN"

    const/16 v2, 0xa6

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEATHER_OVERLAY_SHOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120353
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "HEADING_INDICATOR_SHOWN"

    const/16 v2, 0xa7

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->HEADING_INDICATOR_SHOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120354
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SPATIAL_AUDIO_NUX"

    const/16 v2, 0xa8

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SPATIAL_AUDIO_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120355
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "AD_INTERFACES_ADD_BUTTON"

    const/16 v2, 0xa9

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->AD_INTERFACES_ADD_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120356
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "INSPIRATION_CAMERA"

    const/16 v2, 0xaa

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAMERA:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120357
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "INSPIRATION_CAPTURE"

    const/16 v2, 0xab

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAPTURE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120358
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "INSPIRATION_SHARE"

    const/16 v2, 0xac

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_SHARE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120359
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "INSPIRATION_CAMERA_IN_FEED"

    const/16 v2, 0xad

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAMERA_IN_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120360
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "AD_INTERFACES_LINK_OBJECTIVE"

    const/16 v2, 0xae

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->AD_INTERFACES_LINK_OBJECTIVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120361
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "POLITICAL_PIVOT_FOLLOW_UP_NUX"

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->POLITICAL_PIVOT_FOLLOW_UP_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120362
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SOCIAL_SEARCH_REMOVE_PLACE_H_SCROLL"

    const/16 v2, 0xb0

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_REMOVE_PLACE_H_SCROLL:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120363
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SOCIAL_SEARCH_SEEKER_FEED_NUX"

    const/16 v2, 0xb1

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_SEEKER_FEED_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120364
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "SOCIAL_SEARCH_COMMENTS_NUX"

    const/16 v2, 0xb2

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_COMMENTS_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120365
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "OFFLINE_FEED_AIRPORT_TRIGGER"

    const/16 v2, 0xb3

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->OFFLINE_FEED_AIRPORT_TRIGGER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120366
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "OFFLINE_FEED_BOOKMARK_TRIGGER"

    const/16 v2, 0xb4

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->OFFLINE_FEED_BOOKMARK_TRIGGER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120367
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "PHOTO_ATTATCHMENT_PRODUCT_NUX"

    const/16 v2, 0xb5

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_ATTATCHMENT_PRODUCT_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120368
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "NEXT_NOTIFICATION_UI_RENDERED"

    const/16 v2, 0xb6

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEXT_NOTIFICATION_UI_RENDERED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120369
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "NEARBY_FRIENDS_WAVE_BUTTON"

    const/16 v2, 0xb7

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEARBY_FRIENDS_WAVE_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120370
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "TAROT_END_CARD_OPENED"

    const/16 v2, 0xb8

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAROT_END_CARD_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120371
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "GROUP_ROOMS"

    const/16 v2, 0xb9

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_ROOMS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120372
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xba

    invoke-direct {v0, v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UNKNOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    .line 120373
    const/16 v0, 0xbb

    new-array v0, v0, [Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->CHOOSE_LOVE_REACTION:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_LOADED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEWSFEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET_AUTOPLAY_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_ONLY_ME_SHARE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_APP_DID_BECOME_ACTIVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_NEW_USER_SETUP_COMPLETE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_THREAD_LIST_BLOCKING:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_THREAD_LIST:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_THREAD_LIST_PTR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_RECENT_THREAD_LIST:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_THREAD_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_THREAD_SETTINGS_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_COMPOSE_NEW_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_AUDIO_CLIP_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_DID_SHOW_OMNI_M_SUGGESTIONS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_RTC_PRESENCE_CHANGE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_LAUNCH_EXTERNAL_URL:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSENGER_VIEW_HOME:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MAPS_LAUNCH_EXTERNAL_MAP_APP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB_MARK_AS_DONE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB_MOVE_TO_FOLLOW_UP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB_ALL_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB_FACEBOOK_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_COMMS_HUB_INSTAGRAM_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_DID_BECOME_ACTIVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_OPENED_NEW_PAGE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_APP_MORE_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGES_MANAGER_ADS_MANAGER_BOOKMARK_TAPPED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VOIP_CALL_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VOIP_CALL_END:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ACTIVITY_CONFIGURATION_CHANGED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ACTIVITY_CREATED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_FOREGROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BASIC_SERVICES_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BUILT_IN_BROWSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->DIVEBAR_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BACKSTAGE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MINUTIAE_TAG_PICKER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER_DETECTED_RECENT_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER_HIGHLIGHT_CLUSTER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEWSFEED_USER_REFRESH:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ACTIONBAR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ADMIN_TIMELINE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_NONADMIN_TIMELINE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ADMIN_TIMELINE_VIEW_VERIFY_ELIGIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ADMIN_OVERVIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_STORY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_CALL_TO_ACTION_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_CONTACT_INBOX_ENTRY_POINT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PEOPLE_HIGHLIGHTS_TAB_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->POST_CREATED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PROFILE_FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERSONAL_PROFILE_OWNER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERSONAL_PROFILE_FRIEND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERSONAL_PROFILE_NONFRIEND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_DASHBOARD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_MAIN_TAB_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ITEM_SAVED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ITEM_SAVED_IN_CHECKIN_STORY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ITEM_SAVED_IN_NOTIFICATIONS_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->CHANNEL_FEED_SAVE_OVERLAY_BUTTON_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SIMPLE_SEARCH_CLEAR_TEXT_ICON_CLICK:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEARCH_BAR_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEARCH_NULL_STATE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEARCH_TUTORIAL_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_MESSAGES:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->THREAD_LIST_INTERSTITIAL_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_NOTIFICATIONS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_FRIEND_REQUESTS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_MORE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_ATTACHED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BOOKMARK_TAB_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->REQUEST_TAB_FRIENDING_ACTION_PERFORMED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_PYMK_FRIEND_REQUEST_SENT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_PYMK_SCROLLED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_PYMK_XOUTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->THREAD_LIST_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UNFOLLOW_IN_NFX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->STAGING_GROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CARD_SUGGESTED_PHOTOS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CARD_SUGGESTED_BIO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CARD_BIO_POST_TO_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CARD_FAV_PHOTOS_POST_TO_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_FEATURED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CAR_FAV_PHOTOS_WYSWIG_EDITOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CAR_FAV_PHOTOS_WYSWIG_EDITOR_DRAG_AND_DROP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_CREATE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_INFO_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ADMIN_GROUP_MALL_MULTITIER_ENABLED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MOD_GROUP_MALL_MULTITIER_ENABLED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SPECIFIC_IDS_GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_TYPE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_RANK_FEED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MEMBER_BIO_ELIGIBLE_GROUP_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_ADD_MODERATOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_SCOPED_SEARCH:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_SEEDS_COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUPS_DISCOVER_TAB_TOOL_TIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_ADS_ELIGIBLE_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ADMIN_GROUP_ADS_ELIGIBLE_MALL_VISIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_HOME_TAB_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_HOME_NOTIF_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_HOME_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERMALINK_STORY_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERMALINK_DRAFT_SAVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_INVITE_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_INVITE_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_CANONICAL_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_GROUP_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_CREATE_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_TAB_BADGEABLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEE_FIRST_INDICATOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->LEAD_GEN_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NOW_PUBLISHED_STATUS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NOW_COMPOSER_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NOW_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FACECAST_BROADCAST_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FACECAST_INLINE_SPROUT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_AUTOPLAY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_QUALITY_LABEL_INLINE_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_STARTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_COMPLETE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_BUTTON_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_NOTIFICATION_FIRED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_STALLING:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_WATCHED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MEDIA_GALLERY_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TO_ADS_MANAGER_M_SITE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_INLINE_COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_GUEST_LIST_INVITED_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UFI_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UFI_LONG_PRESSED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMMENT_UFI_LIKE_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_CREATION_LOCKED_PRIVACY_EDUCATION_WIDGET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_COVER_PHOTO_SELECTOR_THEME_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BIRTHDAY_QUICK_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSTANT_ARTICLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSTANT_SHOPPING_SAVE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FIRST_NEWSFEED_AFTER_LOGIN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_INVITE_THROUGH_MESSENGER_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_IMPORT_CONTACTS_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_ADD_NOTE_BUTTON_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_NOTE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_LANNISTER_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_ENTRYPOINT_FB_NONSYNC_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_ENTRYPOINT_FB_SYNC_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENTS_EXTENDED_INVITE_ENTRYPOINT_OFF_FB_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENT_REMINDER_ENTRY_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->EVENT_TICKETING_SEAT_SELECTION_NOTE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->AD_INTERFACES_VIEW_RESULTS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOUVENIR_EDIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SHARESHEET_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NOTIFICATIONS_ADAPTER_CREATION:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEEDBACK_COMPOSER_INIT:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEATHER_OVERLAY_SHOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->HEADING_INDICATOR_SHOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SPATIAL_AUDIO_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->AD_INTERFACES_ADD_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAMERA:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xab

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAPTURE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xac

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_SHARE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xad

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAMERA_IN_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xae

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->AD_INTERFACES_LINK_OBJECTIVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->POLITICAL_PIVOT_FOLLOW_UP_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_REMOVE_PLACE_H_SCROLL:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_SEEKER_FEED_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_COMMENTS_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->OFFLINE_FEED_AIRPORT_TRIGGER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->OFFLINE_FEED_BOOKMARK_TRIGGER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_ATTATCHMENT_PRODUCT_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEXT_NOTIFICATION_UI_RENDERED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEARBY_FRIENDS_WAVE_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAROT_END_CARD_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_ROOMS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xba

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UNKNOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->$VALUES:[Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 120185
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonCreator;
    .end annotation

    .prologue
    .line 120183
    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->valueOf(Ljava/lang/String;)Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 120184
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UNKNOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;
    .locals 1

    .prologue
    .line 120181
    const-class v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    return-object v0
.end method

.method public static values()[Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;
    .locals 1

    .prologue
    .line 120182
    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->$VALUES:[Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    return-object v0
.end method
