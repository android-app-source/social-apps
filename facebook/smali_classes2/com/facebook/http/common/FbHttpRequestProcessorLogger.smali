.class public Lcom/facebook/http/common/FbHttpRequestProcessorLogger;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0kb;

.field private final c:LX/0oz;

.field private final d:LX/14G;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/15D",
            "<*>;",
            "LX/0oG;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0kb;LX/0oz;LX/14G;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 178471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178472
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->e:Ljava/util/Map;

    .line 178473
    iput-object p1, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->a:LX/0Zb;

    .line 178474
    iput-object p2, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->b:LX/0kb;

    .line 178475
    iput-object p3, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->c:LX/0oz;

    .line 178476
    iput-object p4, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->d:LX/14G;

    .line 178477
    return-void
.end method

.method public static a()J
    .locals 2

    .prologue
    .line 178478
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessorLogger;
    .locals 7

    .prologue
    .line 178479
    sget-object v0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->f:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    if-nez v0, :cond_1

    .line 178480
    const-class v1, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    monitor-enter v1

    .line 178481
    :try_start_0
    sget-object v0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->f:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 178482
    if-eqz v2, :cond_0

    .line 178483
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 178484
    new-instance p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v5

    check-cast v5, LX/0oz;

    invoke-static {v0}, LX/14G;->a(LX/0QB;)LX/14G;

    move-result-object v6

    check-cast v6, LX/14G;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;-><init>(LX/0Zb;LX/0kb;LX/0oz;LX/14G;)V

    .line 178485
    move-object v0, p0

    .line 178486
    sput-object v0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->f:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178487
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 178488
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 178489
    :cond_1
    sget-object v0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->f:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    return-object v0

    .line 178490
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 178491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(Lcom/facebook/http/common/FbHttpRequestProcessorLogger;LX/15D;Ljava/lang/Throwable;)V
    .locals 10
    .param p1    # LX/15D;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 178492
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->b(Lcom/facebook/http/common/FbHttpRequestProcessorLogger;LX/15D;)LX/0oG;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 178493
    if-nez v2, :cond_0

    .line 178494
    :goto_0
    monitor-exit p0

    return-void

    .line 178495
    :cond_0
    :try_start_1
    const-string v0, "total_time"

    invoke-static {}, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->a()J

    move-result-wide v4

    .line 178496
    iget-wide v8, p1, LX/15D;->n:J

    move-wide v6, v8

    .line 178497
    sub-long/2addr v4, v6

    invoke-virtual {v2, v0, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 178498
    const-string v1, "success"

    if-nez p2, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v1, v0}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 178499
    if-eqz p2, :cond_1

    .line 178500
    const-string v0, "exception"

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 178501
    const-string v0, "exception_msg"

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 178502
    :cond_1
    iget-object v0, p1, LX/15D;->i:LX/15F;

    move-object v0, v0

    .line 178503
    iget-object v1, v0, LX/15F;->f:Ljava/util/Map;

    move-object v0, v1

    .line 178504
    if-eqz v0, :cond_3

    .line 178505
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 178506
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 178507
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 178508
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 178509
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178510
    invoke-virtual {v2}, LX/0oG;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized b(Lcom/facebook/http/common/FbHttpRequestProcessorLogger;LX/15D;)LX/0oG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)",
            "LX/0oG;"
        }
    .end annotation

    .prologue
    .line 178511
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oG;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/15D;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 178512
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->a:LX/0Zb;

    const-string v1, "fb4a_http_processor"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 178513
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 178514
    :cond_0
    :goto_0
    return-void

    .line 178515
    :cond_1
    monitor-enter p0

    .line 178516
    :try_start_0
    iget-object v1, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178517
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178518
    const-string v1, "engine_name"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 178519
    const-string v1, "stack_name"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 178520
    const-string v1, "friendly_name"

    .line 178521
    iget-object v2, p1, LX/15D;->c:Ljava/lang/String;

    move-object v2, v2

    .line 178522
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 178523
    const-string v1, "initial_priority"

    invoke-virtual {p1}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/http/interfaces/RequestPriority;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 178524
    const-string v1, "pre_queue_time"

    invoke-static {}, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->a()J

    move-result-wide v2

    .line 178525
    iget-wide v6, p1, LX/15D;->n:J

    move-wide v4, v6

    .line 178526
    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 178527
    const-string v1, "connection_class"

    iget-object v2, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->c:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 178528
    const-string v1, "network_type"

    iget-object v2, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->b:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 178529
    iget-object v1, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->d:LX/14G;

    invoke-virtual {v1}, LX/14G;->a()LX/0am;

    move-result-object v1

    .line 178530
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 178531
    const-string v2, "airplane_mode_on"

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 178532
    :cond_2
    iget-object v1, p0, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->d:LX/14G;

    invoke-virtual {v1}, LX/14G;->b()LX/0am;

    move-result-object v1

    .line 178533
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 178534
    const-string v2, "mobile_data_enabled"

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 178535
    :cond_3
    iget-object v1, p1, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v1

    .line 178536
    if-eqz v1, :cond_0

    .line 178537
    const-string v2, "calling_class"

    .line 178538
    iget-object v3, v1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v3, v3

    .line 178539
    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 178540
    const-string v2, "analytics_tag"

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 178541
    const-string v2, "feature_tag"

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 178542
    const-string v2, "module_tag"

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto/16 :goto_0

    .line 178543
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
