.class public Lcom/facebook/http/common/FbHttpRequestProcessor;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile s:Lcom/facebook/http/common/FbHttpRequestProcessor;


# instance fields
.field private final b:LX/0Sh;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0ad;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1hj;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4bO;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4bU;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/14D;

.field private final j:LX/0bx;

.field private final k:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

.field private final l:LX/14H;

.field public final m:LX/14J;

.field private final n:Ljava/util/concurrent/ExecutorService;

.field public volatile o:LX/1hk;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private p:Ljava/lang/Exception;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private q:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public volatile r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 178330
    const-class v0, Lcom/facebook/http/common/FbHttpRequestProcessor;

    sput-object v0, Lcom/facebook/http/common/FbHttpRequestProcessor;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0Or;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/14D;LX/0bx;Lcom/facebook/http/common/FbHttpRequestProcessorLogger;LX/14H;Ljava/util/concurrent/ExecutorService;LX/14J;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/ShouldEnableNetworkPrioritization;
        .end annotation
    .end param
    .param p12    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/tigon/httpclientadapter/TigonHttpClientAdapter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1hj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/4bO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/4bU;",
            ">;",
            "LX/14D;",
            "LX/0bx;",
            "Lcom/facebook/http/common/FbHttpRequestProcessorLogger;",
            "LX/14H;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/tigon/httpclientadapter/TigonExperiment;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 178331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178332
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->q:Z

    .line 178333
    iput-object p1, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->b:LX/0Sh;

    .line 178334
    iput-object p2, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->c:LX/0Or;

    .line 178335
    iput-object p3, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->d:LX/0ad;

    .line 178336
    iput-object p4, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->e:LX/0Ot;

    .line 178337
    iput-object p5, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->f:LX/0Ot;

    .line 178338
    iput-object p6, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->g:LX/0Ot;

    .line 178339
    iput-object p7, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->h:LX/0Ot;

    .line 178340
    iput-object p8, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->i:LX/14D;

    .line 178341
    iput-object p9, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->j:LX/0bx;

    .line 178342
    iput-object p10, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->k:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    .line 178343
    iput-object p11, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->l:LX/14H;

    .line 178344
    iput-object p12, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->n:Ljava/util/concurrent/ExecutorService;

    .line 178345
    iput-object p13, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->m:LX/14J;

    .line 178346
    const/4 p1, 0x1

    move v0, p1

    .line 178347
    if-eqz v0, :cond_0

    .line 178348
    invoke-direct {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->g()V

    .line 178349
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;
    .locals 3

    .prologue
    .line 178350
    sget-object v0, Lcom/facebook/http/common/FbHttpRequestProcessor;->s:Lcom/facebook/http/common/FbHttpRequestProcessor;

    if-nez v0, :cond_1

    .line 178351
    const-class v1, Lcom/facebook/http/common/FbHttpRequestProcessor;

    monitor-enter v1

    .line 178352
    :try_start_0
    sget-object v0, Lcom/facebook/http/common/FbHttpRequestProcessor;->s:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 178353
    if-eqz v2, :cond_0

    .line 178354
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v0

    sput-object v0, Lcom/facebook/http/common/FbHttpRequestProcessor;->s:Lcom/facebook/http/common/FbHttpRequestProcessor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178355
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 178356
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 178357
    :cond_1
    sget-object v0, Lcom/facebook/http/common/FbHttpRequestProcessor;->s:Lcom/facebook/http/common/FbHttpRequestProcessor;

    return-object v0

    .line 178358
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 178359
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/util/concurrent/ExecutionException;)Ljava/lang/RuntimeException;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 178408
    invoke-virtual {p0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 178409
    const-class v1, Ljava/io/IOException;

    invoke-static {v0, v1}, LX/1Bz;->propagateIfInstanceOf(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 178410
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;
    .locals 14

    .prologue
    .line 178360
    new-instance v0, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    const/16 v2, 0x14c8

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    const/16 v4, 0x1246

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xb53

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x24f2

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x24f4

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/14D;->a(LX/0QB;)LX/14D;

    move-result-object v8

    check-cast v8, LX/14D;

    invoke-static {p0}, LX/0bx;->a(LX/0QB;)LX/0bx;

    move-result-object v9

    check-cast v9, LX/0bx;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    invoke-static {p0}, LX/14H;->a(LX/0QB;)LX/14H;

    move-result-object v11

    check-cast v11, LX/14H;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/14I;->a(LX/0QB;)LX/14J;

    move-result-object v13

    check-cast v13, LX/14J;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/http/common/FbHttpRequestProcessor;-><init>(LX/0Sh;LX/0Or;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/14D;LX/0bx;Lcom/facebook/http/common/FbHttpRequestProcessorLogger;LX/14H;Ljava/util/concurrent/ExecutorService;LX/14J;)V

    .line 178361
    return-object v0
.end method

.method private d(LX/15D;)LX/15D;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)",
            "LX/15D",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 178362
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->j:LX/0bx;

    .line 178363
    iget-object v1, p1, LX/15D;->g:Lorg/apache/http/client/ResponseHandler;

    move-object v1, v1

    .line 178364
    iget-object v2, p1, LX/15D;->c:Ljava/lang/String;

    move-object v2, v2

    .line 178365
    iget-object v3, v0, LX/0bx;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/15s;

    .line 178366
    const/4 v6, 0x0

    .line 178367
    sget-object v4, LX/01T;->MESSENGER:LX/01T;

    iget-object v5, v3, LX/15s;->c:LX/01T;

    invoke-virtual {v4, v5}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 178368
    iget-object v4, v3, LX/15s;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/4gn;->b:LX/0Tn;

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    .line 178369
    :goto_0
    move v4, v4

    .line 178370
    if-nez v4, :cond_1

    .line 178371
    const/4 v3, 0x0

    .line 178372
    :goto_1
    move-object v0, v3

    .line 178373
    if-nez v0, :cond_0

    .line 178374
    :goto_2
    return-object p1

    :cond_0
    invoke-static {p1}, LX/15D;->a(LX/15D;)LX/15E;

    move-result-object v1

    .line 178375
    iput-object v0, v1, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 178376
    move-object v0, v1

    .line 178377
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object p1

    goto :goto_2

    .line 178378
    :cond_1
    sget-object v4, LX/01T;->MESSENGER:LX/01T;

    iget-object v5, v3, LX/15s;->c:LX/01T;

    invoke-virtual {v4, v5}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 178379
    iget-object v4, v3, LX/15s;->b:LX/0ad;

    sget-object v5, LX/0c0;->Live:LX/0c0;

    sget v6, LX/4gm;->d:I

    const/16 v7, 0xbb8

    invoke-interface {v4, v5, v6, v7}, LX/0ad;->a(LX/0c0;II)I

    move-result v4

    .line 178380
    :goto_3
    move v4, v4

    .line 178381
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 178382
    const-string v6, "video"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 178383
    sget-object v4, LX/01T;->MESSENGER:LX/01T;

    iget-object v5, v3, LX/15s;->c:LX/01T;

    invoke-virtual {v4, v5}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 178384
    iget-object v4, v3, LX/15s;->b:LX/0ad;

    sget-object v5, LX/0c0;->Live:LX/0c0;

    sget v6, LX/4gm;->l:I

    const/16 v7, 0xbb8

    invoke-interface {v4, v5, v6, v7}, LX/0ad;->a(LX/0c0;II)I

    move-result v4

    .line 178385
    :goto_4
    move v4, v4

    .line 178386
    :cond_2
    :goto_5
    const/4 v5, 0x0

    .line 178387
    sget-object v6, LX/01T;->MESSENGER:LX/01T;

    iget-object v7, v3, LX/15s;->c:LX/01T;

    invoke-virtual {v6, v7}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 178388
    iget-object v6, v3, LX/15s;->b:LX/0ad;

    sget-object v7, LX/0c0;->Live:LX/0c0;

    sget-short v8, LX/4gm;->i:S

    invoke-interface {v6, v7, v8, v5}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v5

    .line 178389
    :cond_3
    move v5, v5

    .line 178390
    if-eqz v5, :cond_4

    .line 178391
    iget-object v5, v0, LX/0bx;->b:Ljava/util/Random;

    invoke-virtual {v5, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    .line 178392
    :cond_4
    const/4 v5, 0x0

    .line 178393
    sget-object v6, LX/01T;->MESSENGER:LX/01T;

    iget-object v7, v3, LX/15s;->c:LX/01T;

    invoke-virtual {v6, v7}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 178394
    iget-object v6, v3, LX/15s;->b:LX/0ad;

    sget-object v7, LX/0c0;->Live:LX/0c0;

    sget-short v8, LX/4gm;->k:S

    const/4 p0, 0x0

    invoke-interface {v6, v7, v8, p0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v6

    .line 178395
    :goto_6
    move v6, v6

    .line 178396
    if-eqz v6, :cond_6

    iget v6, v0, LX/0bx;->f:I

    add-int/lit8 v7, v6, -0x1

    iput v7, v0, LX/0bx;->f:I

    if-gez v6, :cond_6

    .line 178397
    sget-object v5, LX/01T;->MESSENGER:LX/01T;

    iget-object v6, v3, LX/15s;->c:LX/01T;

    invoke-virtual {v5, v6}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 178398
    iget-object v5, v3, LX/15s;->b:LX/0ad;

    sget-object v6, LX/0c0;->Live:LX/0c0;

    sget v7, LX/4gm;->j:I

    const/16 v8, 0xf

    invoke-interface {v5, v6, v7, v8}, LX/0ad;->a(LX/0c0;II)I

    move-result v5

    .line 178399
    :goto_7
    move v3, v5

    .line 178400
    iget-object v5, v0, LX/0bx;->b:Ljava/util/Random;

    invoke-virtual {v5, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    iput v3, v0, LX/0bx;->f:I

    .line 178401
    const/4 v3, 0x1

    .line 178402
    :goto_8
    new-instance v5, LX/4cZ;

    invoke-direct {v5, v1, v2, v4, v3}, LX/4cZ;-><init>(Lorg/apache/http/client/ResponseHandler;Ljava/lang/String;IZ)V

    move-object v3, v5

    goto/16 :goto_1

    .line 178403
    :cond_5
    const-string v6, "image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 178404
    sget-object v4, LX/01T;->MESSENGER:LX/01T;

    iget-object v5, v3, LX/15s;->c:LX/01T;

    invoke-virtual {v4, v5}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 178405
    iget-object v4, v3, LX/15s;->b:LX/0ad;

    sget-object v5, LX/0c0;->Live:LX/0c0;

    sget v6, LX/4gm;->h:I

    const/16 v7, 0xbb8

    invoke-interface {v4, v5, v6, v7}, LX/0ad;->a(LX/0c0;II)I

    move-result v4

    .line 178406
    :goto_9
    move v4, v4

    .line 178407
    goto/16 :goto_5

    :cond_6
    move v3, v5

    goto :goto_8

    :cond_7
    iget-object v4, v3, LX/15s;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0dU;->m:LX/0Tn;

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    goto/16 :goto_0

    :cond_8
    const/16 v4, 0x2710

    goto/16 :goto_3

    :cond_9
    const/16 v4, 0x2710

    goto/16 :goto_4

    :cond_a
    const/4 v6, 0x1

    goto :goto_6

    :cond_b
    const/16 v5, 0xa

    goto :goto_7

    :cond_c
    const/16 v4, 0x3a98

    goto :goto_9
.end method

.method private g()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 178313
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->n:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/http/common/FbHttpRequestProcessor$1;

    invoke-direct {v1, p0}, Lcom/facebook/http/common/FbHttpRequestProcessor$1;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    const v2, -0x738d4a12

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 178314
    return-void
.end method

.method public static j(Lcom/facebook/http/common/FbHttpRequestProcessor;)LX/1hk;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InvalidAccessToGuardedField"
        }
    .end annotation

    .prologue
    .line 178315
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->o:LX/1hk;

    if-eqz v0, :cond_0

    .line 178316
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->o:LX/1hk;

    .line 178317
    :goto_0
    return-object v0

    .line 178318
    :cond_0
    monitor-enter p0

    .line 178319
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->o:LX/1hk;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->p:Ljava/lang/Exception;

    if-nez v0, :cond_2

    .line 178320
    iget-boolean v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->q:Z

    if-nez v0, :cond_1

    .line 178321
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->q:Z

    .line 178322
    invoke-direct {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178323
    :cond_1
    const v0, -0x6c8b0017

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 178324
    :catch_0
    move-exception v0

    .line 178325
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 178326
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 178327
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->p:Ljava/lang/Exception;

    if-eqz v0, :cond_3

    .line 178328
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->p:Ljava/lang/Exception;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 178329
    :cond_3
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->o:LX/1hk;

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized k(Lcom/facebook/http/common/FbHttpRequestProcessor;)V
    .locals 6

    .prologue
    .line 178285
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->o:LX/1hk;

    if-nez v0, :cond_0

    .line 178286
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 178287
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->m:LX/14J;

    .line 178288
    iget-object v3, v0, LX/14J;->a:LX/0ad;

    sget-short v4, LX/14p;->c:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    move v0, v3

    .line 178289
    if-nez v0, :cond_3

    move v0, v1

    .line 178290
    :goto_0
    move v0, v0

    .line 178291
    if-eqz v0, :cond_1

    .line 178292
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hk;

    iput-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->o:LX/1hk;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178293
    :cond_0
    :goto_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->q:Z

    .line 178294
    const v0, 0x554aa908

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178295
    monitor-exit p0

    return-void

    .line 178296
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 178297
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hk;

    iput-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->o:LX/1hk;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 178298
    :catch_0
    move-exception v0

    .line 178299
    :try_start_3
    iput-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->p:Ljava/lang/Exception;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 178300
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 178301
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hk;

    iput-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->o:LX/1hk;
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 178302
    :catch_1
    :try_start_5
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hk;

    iput-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->o:LX/1hk;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 178303
    :cond_3
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->i:LX/14D;

    invoke-virtual {v0}, LX/14D;->a()LX/15e;

    move-result-object v0

    .line 178304
    sget-object v3, LX/15e;->LIGER:LX/15e;

    if-ne v0, v3, :cond_4

    move v0, v2

    .line 178305
    :goto_2
    if-nez v0, :cond_5

    move v0, v1

    .line 178306
    goto :goto_0

    :cond_4
    move v0, v1

    .line 178307
    goto :goto_2

    .line 178308
    :cond_5
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16j;

    .line 178309
    iget-object v3, v0, LX/16j;->a:Lcom/facebook/tigon/tigonliger/TigonLigerService;

    invoke-interface {v3}, LX/1AG;->a()Z

    move-result v3

    move v0, v3

    .line 178310
    if-nez v0, :cond_7

    :cond_6
    move v0, v1

    .line 178311
    goto :goto_0

    :cond_7
    move v0, v2

    .line 178312
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/2Ee;
    .locals 1

    .prologue
    .line 178284
    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->j(Lcom/facebook/http/common/FbHttpRequestProcessor;)LX/1hk;

    move-result-object v0

    invoke-interface {v0}, LX/1hk;->b()LX/2Ee;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15D;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 178276
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 178277
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v0

    .line 178278
    iget-object v1, v0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1

    .line 178279
    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 178280
    :catch_0
    move-exception v0

    .line 178281
    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(Ljava/util/concurrent/ExecutionException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 178282
    :catch_1
    move-exception v0

    .line 178283
    new-instance v1, LX/4bL;

    invoke-direct {v1, v0}, LX/4bL;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 178274
    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->j(Lcom/facebook/http/common/FbHttpRequestProcessor;)LX/1hk;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1hk;->a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 178275
    return-void
.end method

.method public final b(LX/15D;)LX/1j2;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)",
            "LX/1j2",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 178233
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->l:LX/14H;

    .line 178234
    iget-object v1, v0, LX/14H;->b:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 178235
    const/4 v1, 0x0

    .line 178236
    iget-object v2, v0, LX/14H;->c:LX/0ad;

    sget-char v3, LX/0by;->P:C

    const-string v4, ""

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 178237
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178238
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 178239
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v1, v4, v2

    .line 178240
    const-string v6, "::"

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 178241
    array-length v6, v1

    const/4 v7, 0x4

    if-lt v6, v7, :cond_0

    .line 178242
    const/4 v6, 0x2

    :try_start_0
    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    sget-object v7, Lcom/facebook/http/interfaces/RequestPriority;->UNUSED_REQUEST_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-static {v6, v7}, Lcom/facebook/http/interfaces/RequestPriority;->fromNumericValue(ILcom/facebook/http/interfaces/RequestPriority;)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v6

    .line 178243
    sget-object v7, Lcom/facebook/http/interfaces/RequestPriority;->UNUSED_REQUEST_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    if-ne v6, v7, :cond_1

    .line 178244
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 178245
    :cond_1
    const/4 v7, 0x3

    aget-object v7, v1, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    sget-object v8, Lcom/facebook/http/interfaces/RequestPriority;->UNUSED_REQUEST_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-static {v7, v8}, Lcom/facebook/http/interfaces/RequestPriority;->fromNumericValue(ILcom/facebook/http/interfaces/RequestPriority;)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v7

    .line 178246
    sget-object v8, Lcom/facebook/http/interfaces/RequestPriority;->UNUSED_REQUEST_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    if-eq v7, v8, :cond_0

    .line 178247
    new-instance v8, LX/4cB;

    const/4 v9, 0x0

    aget-object v9, v1, v9

    const/4 v10, 0x1

    aget-object v1, v1, v10

    invoke-direct {v8, v9, v1, v6, v7}, LX/4cB;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 178248
    :catch_0
    move-exception v1

    .line 178249
    sget-object v6, LX/14H;->a:Ljava/lang/Class;

    const-string v7, "Ignore error parsing bad config"

    invoke-static {v6, v7, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 178250
    :cond_2
    iput-object v3, v0, LX/14H;->b:Ljava/util/ArrayList;

    .line 178251
    :cond_3
    iget-object v1, v0, LX/14H;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_4

    iget-object v1, v0, LX/14H;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4cB;

    .line 178252
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178253
    iget-object v4, v1, LX/4cB;->a:Ljava/lang/String;

    .line 178254
    iget-object v5, p1, LX/15D;->c:Ljava/lang/String;

    move-object v5, v5

    .line 178255
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, v1, LX/4cB;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/facebook/http/common/FbHttpUtils;->b(LX/15D;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, v1, LX/4cB;->c:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {p1}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v5

    if-ne v4, v5, :cond_8

    const/4 v4, 0x1

    :goto_3
    move v4, v4

    .line 178256
    if-eqz v4, :cond_7

    .line 178257
    invoke-static {p1}, LX/15D;->a(LX/15D;)LX/15E;

    move-result-object v2

    iget-object v1, v1, LX/4cB;->d:Lcom/facebook/http/interfaces/RequestPriority;

    .line 178258
    iput-object v1, v2, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 178259
    move-object v1, v2

    .line 178260
    invoke-virtual {v1}, LX/15E;->a()LX/15D;

    move-result-object p1

    .line 178261
    :cond_4
    move-object v0, p1

    .line 178262
    invoke-direct {p0, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->d(LX/15D;)LX/15D;

    move-result-object v1

    .line 178263
    iget-object v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->k:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->j(Lcom/facebook/http/common/FbHttpRequestProcessor;)LX/1hk;

    move-result-object v2

    invoke-interface {v2}, LX/1hk;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->j(Lcom/facebook/http/common/FbHttpRequestProcessor;)LX/1hk;

    move-result-object v3

    invoke-interface {v3}, LX/1hk;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->a(LX/15D;Ljava/lang/String;Ljava/lang/String;)V

    .line 178264
    iget-object v0, v1, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 178265
    iget-boolean v2, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->r:Z

    if-eqz v2, :cond_6

    if-eqz v0, :cond_5

    const-string v2, "MAGIC_LOGOUT_TAG"

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 178266
    :cond_5
    new-instance v0, Ljava/io/IOException;

    const-string v2, "In lame duck mode"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 178267
    :goto_4
    iget-object v2, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->k:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    .line 178268
    new-instance v3, LX/1j1;

    invoke-direct {v3, v2, v1}, LX/1j1;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessorLogger;LX/15D;)V

    .line 178269
    sget-object v4, LX/131;->INSTANCE:LX/131;

    move-object v4, v4

    .line 178270
    invoke-static {v0, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 178271
    new-instance v2, LX/1j2;

    invoke-direct {v2, v1, v0, p0}, LX/1j2;-><init>(LX/15D;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    return-object v2

    .line 178272
    :cond_6
    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->j(Lcom/facebook/http/common/FbHttpRequestProcessor;)LX/1hk;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1hk;->a(LX/15D;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_4

    .line 178273
    :cond_7
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_2

    :cond_8
    const/4 v4, 0x0

    goto :goto_3
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 178231
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/http/common/FbHttpRequestProcessor;->r:Z

    .line 178232
    return-void
.end method

.method public final c(LX/15D;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 178230
    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->j(Lcom/facebook/http/common/FbHttpRequestProcessor;)LX/1hk;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1hk;->b(LX/15D;)Z

    move-result v0

    return v0
.end method
