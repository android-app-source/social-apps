.class public Lcom/facebook/http/common/FbHttpUtils;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297693
    return-void
.end method

.method public static a(Ljava/util/ArrayList;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/15D",
            "<*>;>;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297679
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 297680
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 297681
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    .line 297682
    iget-object v2, v0, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 297683
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 297684
    iget-object v5, v0, LX/15D;->c:Ljava/lang/String;

    move-object v5, v5

    .line 297685
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/http/interfaces/RequestPriority;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez v2, :cond_0

    const-string v2, "null"

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ";;"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 297686
    iget-object v4, v0, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v0, v4

    .line 297687
    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 297688
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 297689
    :cond_0
    iget-object v5, v2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v5

    .line 297690
    goto :goto_1

    .line 297691
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/15D;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 297672
    iget-object v0, p0, LX/15D;->l:Ljava/lang/String;

    move-object v0, v0

    .line 297673
    if-eqz v0, :cond_0

    .line 297674
    :goto_0
    return-object v0

    .line 297675
    :cond_0
    iget-object v0, p0, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 297676
    if-eqz v0, :cond_1

    .line 297677
    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 297678
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/http/interfaces/RequestPriority;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 297655
    sget-object v0, LX/4b9;->a:[I

    invoke-virtual {p0}, Lcom/facebook/http/interfaces/RequestPriority;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 297656
    invoke-virtual {p0}, Lcom/facebook/http/interfaces/RequestPriority;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 297657
    :pswitch_0
    const-string v0, "?"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/15D",
            "<*>;>;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 297667
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    .line 297668
    invoke-virtual {v0}, LX/15D;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297669
    add-int/lit8 v0, v1, 0x1

    .line 297670
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 297671
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static b(LX/15D;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 297658
    iget-object v0, p0, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v0

    .line 297659
    const-string v0, "Unknown"

    .line 297660
    if-eqz v1, :cond_1

    .line 297661
    iget-object v0, v1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v0, v0

    .line 297662
    if-eqz v0, :cond_0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 297663
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 297664
    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "$"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 297665
    const/4 v1, 0x0

    const/16 v2, 0x24

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 297666
    :cond_1
    return-object v0
.end method
