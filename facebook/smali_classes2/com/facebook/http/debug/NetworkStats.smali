.class public Lcom/facebook/http/debug/NetworkStats;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:Lcom/facebook/http/debug/NetworkStats;


# instance fields
.field private final a:LX/0So;

.field private final b:LX/0SG;

.field private c:J

.field private d:J

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1jT;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0So;LX/0SG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 299172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299173
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/http/debug/NetworkStats;->e:Ljava/util/Map;

    .line 299174
    iput-object p1, p0, Lcom/facebook/http/debug/NetworkStats;->a:LX/0So;

    .line 299175
    iput-object p2, p0, Lcom/facebook/http/debug/NetworkStats;->b:LX/0SG;

    .line 299176
    invoke-interface {p1}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/http/debug/NetworkStats;->c:J

    .line 299177
    invoke-interface {p2}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/http/debug/NetworkStats;->d:J

    .line 299178
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/http/debug/NetworkStats;
    .locals 5

    .prologue
    .line 299126
    sget-object v0, Lcom/facebook/http/debug/NetworkStats;->f:Lcom/facebook/http/debug/NetworkStats;

    if-nez v0, :cond_1

    .line 299127
    const-class v1, Lcom/facebook/http/debug/NetworkStats;

    monitor-enter v1

    .line 299128
    :try_start_0
    sget-object v0, Lcom/facebook/http/debug/NetworkStats;->f:Lcom/facebook/http/debug/NetworkStats;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 299129
    if-eqz v2, :cond_0

    .line 299130
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 299131
    new-instance p0, Lcom/facebook/http/debug/NetworkStats;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, Lcom/facebook/http/debug/NetworkStats;-><init>(LX/0So;LX/0SG;)V

    .line 299132
    move-object v0, p0

    .line 299133
    sput-object v0, Lcom/facebook/http/debug/NetworkStats;->f:Lcom/facebook/http/debug/NetworkStats;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 299134
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 299135
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 299136
    :cond_1
    sget-object v0, Lcom/facebook/http/debug/NetworkStats;->f:Lcom/facebook/http/debug/NetworkStats;

    return-object v0

    .line 299137
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 299138
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized b(Lcom/facebook/http/debug/NetworkStats;Lorg/apache/http/HttpHost;Ljava/lang/String;)LX/1jT;
    .locals 2
    .param p1    # Lorg/apache/http/HttpHost;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299163
    monitor-enter p0

    if-nez p2, :cond_0

    .line 299164
    :try_start_0
    const-string p2, "<not-specified>"

    .line 299165
    :cond_0
    iget-object v0, p0, Lcom/facebook/http/debug/NetworkStats;->e:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jT;

    .line 299166
    if-nez v0, :cond_1

    .line 299167
    new-instance v0, LX/1jT;

    invoke-direct {v0, p2}, LX/1jT;-><init>(Ljava/lang/String;)V

    .line 299168
    iget-object v1, p0, Lcom/facebook/http/debug/NetworkStats;->e:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299169
    :cond_1
    iget-object v1, v0, LX/1jT;->a:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299170
    monitor-exit p0

    return-object v0

    .line 299171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lorg/apache/http/HttpHost;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299159
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/facebook/http/debug/NetworkStats;->b(Lcom/facebook/http/debug/NetworkStats;Lorg/apache/http/HttpHost;Ljava/lang/String;)LX/1jT;

    move-result-object v0

    .line 299160
    iget v1, v0, LX/1jT;->numConnections:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/1jT;->numConnections:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299161
    monitor-exit p0

    return-void

    .line 299162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lorg/apache/http/HttpHost;Ljava/lang/String;J)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299155
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/facebook/http/debug/NetworkStats;->b(Lcom/facebook/http/debug/NetworkStats;Lorg/apache/http/HttpHost;Ljava/lang/String;)LX/1jT;

    move-result-object v0

    .line 299156
    iget-object v0, v0, LX/1jT;->bytesPayload:LX/1jU;

    iget-wide v2, v0, LX/1jU;->recvd:J

    add-long/2addr v2, p3

    iput-wide v2, v0, LX/1jU;->recvd:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299157
    monitor-exit p0

    return-void

    .line 299158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lorg/apache/http/HttpHost;Ljava/lang/String;JJJ)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299148
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/facebook/http/debug/NetworkStats;->b(Lcom/facebook/http/debug/NetworkStats;Lorg/apache/http/HttpHost;Ljava/lang/String;)LX/1jT;

    move-result-object v0

    .line 299149
    iget-object v1, v0, LX/1jT;->bytesHeaders:LX/1jU;

    iget-wide v2, v1, LX/1jU;->sent:J

    add-long/2addr v2, p3

    iput-wide v2, v1, LX/1jU;->sent:J

    .line 299150
    const-wide/16 v2, 0x0

    cmp-long v1, p5, v2

    if-ltz v1, :cond_0

    .line 299151
    iget-object v1, v0, LX/1jT;->bytesPayload:LX/1jU;

    iget-wide v2, v1, LX/1jU;->sent:J

    add-long/2addr v2, p5

    iput-wide v2, v1, LX/1jU;->sent:J

    .line 299152
    :cond_0
    iget-object v0, v0, LX/1jT;->bytesHeaders:LX/1jU;

    iget-wide v2, v0, LX/1jU;->recvd:J

    add-long/2addr v2, p7

    iput-wide v2, v0, LX/1jU;->recvd:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299153
    monitor-exit p0

    return-void

    .line 299154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lorg/apache/http/HttpHost;Ljava/lang/String;Lorg/apache/http/HttpRequest;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299139
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/facebook/http/debug/NetworkStats;->b(Lcom/facebook/http/debug/NetworkStats;Lorg/apache/http/HttpHost;Ljava/lang/String;)LX/1jT;

    move-result-object v0

    .line 299140
    iget v1, v0, LX/1jT;->totalHttpFlows:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/1jT;->totalHttpFlows:I

    .line 299141
    invoke-interface {p3}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v1

    .line 299142
    const-string v2, "GET"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 299143
    iget v1, v0, LX/1jT;->numGets:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/1jT;->numGets:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299144
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 299145
    :cond_1
    :try_start_1
    const-string v2, "POST"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 299146
    iget v1, v0, LX/1jT;->numPosts:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/1jT;->numPosts:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 299147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
