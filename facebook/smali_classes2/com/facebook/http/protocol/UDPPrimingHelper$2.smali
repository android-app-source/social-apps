.class public final Lcom/facebook/http/protocol/UDPPrimingHelper$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/0rz;


# direct methods
.method public constructor <init>(LX/0rz;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 168684
    iput-object p1, p0, Lcom/facebook/http/protocol/UDPPrimingHelper$2;->c:LX/0rz;

    iput-object p2, p0, Lcom/facebook/http/protocol/UDPPrimingHelper$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/http/protocol/UDPPrimingHelper$2;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 168685
    iget-object v0, p0, Lcom/facebook/http/protocol/UDPPrimingHelper$2;->c:LX/0rz;

    .line 168686
    sget-object v1, LX/00a;->a:LX/00a;

    move-object v1, v1

    .line 168687
    invoke-static {v0}, LX/0rz;->e(LX/0rz;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 168688
    iget-boolean v3, v1, LX/00a;->l:Z

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-ne v3, v4, :cond_3

    .line 168689
    :cond_0
    :goto_0
    invoke-static {v0}, LX/0rz;->f(LX/0rz;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 168690
    iget-boolean v3, v1, LX/00a;->m:Z

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-ne v3, v4, :cond_4

    .line 168691
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/http/protocol/UDPPrimingHelper$2;->c:LX/0rz;

    iget-object v1, p0, Lcom/facebook/http/protocol/UDPPrimingHelper$2;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/http/protocol/UDPPrimingHelper$2;->b:Ljava/lang/String;

    .line 168692
    sget-object v3, LX/00a;->a:LX/00a;

    move-object v3, v3

    .line 168693
    iget-object v4, v0, LX/0rz;->b:Ljava/lang/String;

    .line 168694
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    if-nez v4, :cond_5

    .line 168695
    :cond_2
    :goto_2
    return-void

    .line 168696
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, v1, LX/00a;->l:Z

    .line 168697
    iget-object v3, v1, LX/00a;->c:Landroid/content/SharedPreferences;

    if-eqz v3, :cond_0

    .line 168698
    iget-object v3, v1, LX/00a;->c:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 168699
    const-string v4, "COLD_START_PRIME_INFO/COLD_START_ADVANCE_PRIME_ENABLED"

    iget-boolean v5, v1, LX/00a;->l:Z

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 168700
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 168701
    :cond_4
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, v1, LX/00a;->m:Z

    .line 168702
    iget-object v3, v1, LX/00a;->c:Landroid/content/SharedPreferences;

    if-eqz v3, :cond_1

    .line 168703
    iget-object v3, v1, LX/00a;->c:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 168704
    const-string v4, "COLD_START_PRIME_INFO/COLD_START_ADVANCE_PRIME_FROM_NODEX_ENABLED"

    iget-boolean v5, v1, LX/00a;->m:Z

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 168705
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 168706
    :cond_5
    const/4 v5, 0x1

    iput-boolean v5, v3, LX/00a;->n:Z

    .line 168707
    iget-object v5, v3, LX/00a;->d:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, v3, LX/00a;->j:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, v3, LX/00a;->i:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 168708
    :cond_6
    iput-object v1, v3, LX/00a;->d:Ljava/lang/String;

    .line 168709
    iput-object v4, v3, LX/00a;->i:Ljava/lang/String;

    .line 168710
    iput-object v2, v3, LX/00a;->j:Ljava/lang/String;

    .line 168711
    iget-object v5, v3, LX/00a;->c:Landroid/content/SharedPreferences;

    if-eqz v5, :cond_2

    .line 168712
    iget-object v5, v3, LX/00a;->c:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 168713
    const-string p0, "COLD_START_PRIME_INFO/FIRST_FETCH_STRING"

    iget-object v0, v3, LX/00a;->d:Ljava/lang/String;

    invoke-interface {v5, p0, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 168714
    const-string p0, "COLD_START_PRIME_INFO/USER_AGENT"

    iget-object v0, v3, LX/00a;->i:Ljava/lang/String;

    invoke-interface {v5, p0, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 168715
    const-string p0, "COLD_START_PRIME_INFO/USER_ID"

    iget-object v0, v3, LX/00a;->j:Ljava/lang/String;

    invoke-interface {v5, p0, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 168716
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_2
.end method
