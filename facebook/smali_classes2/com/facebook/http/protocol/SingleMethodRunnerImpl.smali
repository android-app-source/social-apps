.class public Lcom/facebook/http/protocol/SingleMethodRunnerImpl;
.super LX/11H;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile C:Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:LX/0ad;

.field private B:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0s9;

.field private final d:LX/0s9;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/00I;

.field private final o:LX/0lp;

.field private final p:LX/0lC;

.field private final q:LX/11M;

.field private final r:LX/11N;

.field private final s:Lcom/facebook/common/perftest/PerfTestConfig;

.field private final t:LX/11P;

.field private final u:LX/11Q;

.field private final v:LX/0rz;

.field private final w:LX/0SG;

.field private final x:LX/11S;

.field private final y:LX/0oz;

.field private final z:LX/0WJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 171760
    sget-object v0, LX/11I;->CONFIRM_CONTACTPOINT_PRECONFIRMATION:LX/11I;

    iget-object v0, v0, LX/11I;->requestNameString:Ljava/lang/String;

    sget-object v1, LX/11I;->INITIATE_PRECONFIRMATION:LX/11I;

    iget-object v1, v1, LX/11I;->requestNameString:Ljava/lang/String;

    sget-object v2, LX/11I;->REGISTER_ACCOUNT:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    sget-object v3, LX/11I;->RESET_PASSWORD_PRECONFIRMATION:LX/11I;

    iget-object v3, v3, LX/11I;->requestNameString:Ljava/lang/String;

    sget-object v4, LX/11I;->VALIDATE_REGISTRATION_DATA:LX/11I;

    iget-object v4, v4, LX/11I;->requestNameString:Ljava/lang/String;

    sget-object v5, LX/11I;->SYNC_X_CONFIGS:LX/11I;

    iget-object v5, v5, LX/11I;->requestNameString:Ljava/lang/String;

    const/16 v6, 0x13

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, LX/11I;->SESSIONLESS_GK:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, LX/11I;->GK_INFO:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, LX/11I;->REGISTER_PUSH:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, LX/11I;->UNREGISTER_PUSH:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    sget-object v8, LX/11I;->LOGOUT:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    sget-object v8, LX/11I;->AUTHENTICATE:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    sget-object v8, LX/11I;->BOOKMARK_SYNC:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x7

    sget-object v8, LX/11I;->GET_LOGGED_IN_USER_QUERY:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0x8

    sget-object v8, LX/11I;->REQUEST_MESSENGER_ONLY_CODE:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0x9

    sget-object v8, LX/11I;->CONFIRM_MESSENGER_ONLY_CODE:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0xa

    sget-object v8, LX/11I;->LOGIN_MESSENGER_CREDS_BYPASS:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0xb

    sget-object v8, LX/11I;->CREATE_MESSENGER_ACCOUNT:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0xc

    sget-object v8, LX/11I;->MQTT_CONFIG:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0xd

    sget-object v8, LX/11I;->FETCH_ZERO_TOKEN_QUERY:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0xe

    sget-object v8, LX/11I;->FETCH_ZERO_MESSAGE_QUOTA_QUERY:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0xf

    sget-object v8, LX/11I;->FETCH_ZERO_IP_TEST:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0x10

    sget-object v8, LX/11I;->ZERO_IP_TEST_SUBMIT:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0x11

    sget-object v8, LX/11I;->SMS_INVITE:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0x12

    sget-object v8, LX/11I;->MESSENGER_ONLY_MIGRATE_ACCOUNT:LX/11I;

    iget-object v8, v8, LX/11I;->requestNameString:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0s9;LX/0s9;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/00I;LX/0lp;LX/0lC;LX/11M;LX/11N;Lcom/facebook/common/perftest/PerfTestConfig;LX/11P;LX/11Q;LX/0rz;LX/0SG;LX/11S;LX/0WJ;LX/0oz;LX/0ad;)V
    .locals 1
    .param p2    # LX/0s9;
        .annotation runtime Lcom/facebook/http/annotations/ProductionPlatformAppHttpConfig;
        .end annotation
    .end param
    .param p3    # LX/0s9;
        .annotation runtime Lcom/facebook/http/annotations/BootstrapPlatformAppHttpConfig;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/IsPhpProfilingEnabled;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/IsTeakProfilingEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/IsWirehogProfilingEnabled;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/IsArtilleryTracingEnabled;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/config/server/IsRedirectToSandboxEnabled;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/ShouldIncludeDateHeader;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;",
            "LX/0s9;",
            "LX/0s9;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/00I;",
            "LX/0lp;",
            "LX/0lC;",
            "LX/11M;",
            "LX/11N;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "LX/11P;",
            "LX/11Q;",
            "LX/0rz;",
            "LX/0SG;",
            "LX/11S;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0oz;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 171761
    invoke-direct {p0}, LX/11H;-><init>()V

    .line 171762
    iput-object p1, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->b:LX/0Or;

    .line 171763
    iput-object p2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->c:LX/0s9;

    .line 171764
    iput-object p3, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->d:LX/0s9;

    .line 171765
    iput-object p4, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->e:LX/0Ot;

    .line 171766
    iput-object p5, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->f:LX/0Or;

    .line 171767
    iput-object p6, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->g:LX/0Or;

    .line 171768
    iput-object p7, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->h:LX/0Or;

    .line 171769
    iput-object p8, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->i:LX/0Or;

    .line 171770
    iput-object p9, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->j:LX/0Or;

    .line 171771
    iput-object p10, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->k:LX/0Or;

    .line 171772
    iput-object p11, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->l:LX/0Or;

    .line 171773
    iput-object p12, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->m:LX/0Or;

    .line 171774
    iput-object p13, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->n:LX/00I;

    .line 171775
    iput-object p14, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->o:LX/0lp;

    .line 171776
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->p:LX/0lC;

    .line 171777
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->q:LX/11M;

    .line 171778
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->r:LX/11N;

    .line 171779
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->s:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 171780
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->t:LX/11P;

    .line 171781
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->u:LX/11Q;

    .line 171782
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->v:LX/0rz;

    .line 171783
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->w:LX/0SG;

    .line 171784
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->x:LX/11S;

    .line 171785
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->y:LX/0oz;

    .line 171786
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->z:LX/0WJ;

    .line 171787
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->A:LX/0ad;

    .line 171788
    return-void
.end method

.method private a(LX/14N;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)LX/0n9;
    .locals 5
    .param p2    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171789
    iget-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->r:LX/11N;

    invoke-virtual {v0, p1}, LX/11N;->a(LX/14N;)LX/0n9;

    move-result-object v1

    .line 171790
    iget-object v0, p1, LX/14N;->c:Ljava/lang/String;

    move-object v0, v0

    .line 171791
    const-string v2, "method/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 171792
    const-string v2, "method"

    const/4 v3, 0x7

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 171793
    invoke-static {v1, v2, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171794
    :cond_0
    :goto_0
    const-string v0, "fb_api_req_friendly_name"

    .line 171795
    iget-object v2, p1, LX/14N;->a:Ljava/lang/String;

    move-object v2, v2

    .line 171796
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171797
    const-string v0, "fb_api_caller_class"

    .line 171798
    iget-object v2, p3, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 171799
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171800
    invoke-static {p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->b(LX/14N;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171801
    invoke-static {p0, v1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/0n9;)V

    .line 171802
    :cond_1
    const/4 v0, 0x0

    .line 171803
    invoke-static {p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->d(LX/14N;)Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz p2, :cond_6

    .line 171804
    iget-object v2, p2, LX/14U;->i:Ljava/lang/String;

    move-object v2, v2

    .line 171805
    if-eqz v2, :cond_6

    .line 171806
    iget-object v0, p2, LX/14U;->i:Ljava/lang/String;

    move-object v0, v0

    .line 171807
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    .line 171808
    const-string v2, "access_token"

    .line 171809
    invoke-static {v1, v2, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171810
    :cond_3
    return-object v1

    .line 171811
    :cond_4
    const-string v0, "DELETE"

    .line 171812
    iget-object v2, p1, LX/14N;->b:Ljava/lang/String;

    move-object v2, v2

    .line 171813
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 171814
    const-string v0, "method"

    const-string v2, "DELETE"

    .line 171815
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171816
    goto :goto_0

    .line 171817
    :cond_5
    const-string v0, "GET"

    .line 171818
    iget-object v2, p1, LX/14N;->b:Ljava/lang/String;

    move-object v2, v2

    .line 171819
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171820
    const-string v0, "method"

    const-string v2, "GET"

    .line 171821
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171822
    goto :goto_0

    .line 171823
    :cond_6
    invoke-static {p0, p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->c(Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/14N;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 171824
    const-string v0, "|"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->n:LX/00I;

    invoke-interface {v4}, LX/00I;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->n:LX/00I;

    invoke-interface {v4}, LX/00I;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(LX/0e6;)LX/0rp;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;*>;)",
            "LX/0rp",
            "<TPARAMS;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 171825
    if-eqz p0, :cond_0

    instance-of v0, p0, LX/0rp;

    if-eqz v0, :cond_0

    .line 171826
    check-cast p0, LX/0rp;

    .line 171827
    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/common/callercontext/CallerContext;LX/0e6;LX/14N;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 2
    .param p0    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171600
    if-eqz p0, :cond_0

    .line 171601
    iget-object v0, p2, LX/14N;->a:Ljava/lang/String;

    move-object v0, v0

    .line 171602
    invoke-static {p0, v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 171603
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 171604
    iget-object v1, p2, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171605
    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/14N;LX/14U;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 2

    .prologue
    .line 171828
    invoke-virtual {p0}, LX/14N;->g()LX/0zW;

    move-result-object v0

    .line 171829
    iget-object v1, p1, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v1, v1

    .line 171830
    if-eqz v1, :cond_0

    .line 171831
    invoke-virtual {v0, v1}, LX/0zW;->a(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 171832
    :cond_0
    invoke-virtual {v0}, LX/0zW;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;
    .locals 3

    .prologue
    .line 171833
    sget-object v0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->C:Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    if-nez v0, :cond_1

    .line 171834
    const-class v1, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    monitor-enter v1

    .line 171835
    :try_start_0
    sget-object v0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->C:Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 171836
    if-eqz v2, :cond_0

    .line 171837
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->b(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v0

    sput-object v0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->C:Lcom/facebook/http/protocol/SingleMethodRunnerImpl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171838
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 171839
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 171840
    :cond_1
    sget-object v0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->C:Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    return-object v0

    .line 171841
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 171842
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0ro;LX/14N;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;
    .locals 7
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/http/protocol/GraphQlPersistedApiMethod",
            "<TPARAMS;TRESU",
            "LT;",
            ">;",
            "LX/14N;",
            "TPARAMS;",
            "LX/14U;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 171843
    :try_start_0
    invoke-static {p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0e6;)LX/0rp;

    move-result-object v4

    move-object v0, p0

    move-object v1, p2

    move-object v2, p4

    move-object v3, p1

    move-object v5, p3

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/14N;LX/14U;LX/0e6;LX/0rp;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)LX/1pW;
    :try_end_0
    .catch LX/4cz; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/4d0; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 171844
    :goto_0
    iget-object v1, v0, LX/1pW;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 171845
    return-object v0

    .line 171846
    :catch_0
    move-exception v0

    .line 171847
    const-string v1, "SingleMethodRunnerImpl"

    const-string v2, "Invalid persisted graphql query id"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171848
    invoke-interface {p1, p3}, LX/0e6;->a(Ljava/lang/Object;)LX/14N;

    move-result-object v1

    .line 171849
    invoke-static {p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0e6;)LX/0rp;

    move-result-object v4

    move-object v0, p0

    move-object v2, p4

    move-object v3, p1

    move-object v5, p3

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/14N;LX/14U;LX/0e6;LX/0rp;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)LX/1pW;

    move-result-object v0

    goto :goto_0

    .line 171850
    :catch_1
    invoke-interface {p1, p3}, LX/0e6;->a(Ljava/lang/Object;)LX/14N;

    move-result-object v1

    .line 171851
    invoke-static {p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0e6;)LX/0rp;

    move-result-object v4

    move-object v0, p0

    move-object v2, p4

    move-object v3, p1

    move-object v5, p3

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/14N;LX/14U;LX/0e6;LX/0rp;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)LX/1pW;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/14N;LX/0n9;LX/4ck;)Lorg/apache/http/HttpEntity;
    .locals 2
    .param p3    # LX/4ck;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171852
    invoke-virtual {p1}, LX/14N;->m()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/14N;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 171853
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot add attachment to string entities"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171854
    :cond_0
    new-instance v0, LX/14d;

    invoke-direct {v0, p2}, LX/14d;-><init>(LX/0n9;)V

    .line 171855
    if-eqz p3, :cond_1

    .line 171856
    new-instance v1, LX/4d6;

    invoke-direct {v1, p0, p3}, LX/4d6;-><init>(Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/4ck;)V

    .line 171857
    iput-object v1, v0, LX/14d;->b:LX/4cJ;

    .line 171858
    :cond_1
    return-object v0
.end method

.method private static a(Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/14N;LX/4ck;)Lorg/apache/http/HttpEntity;
    .locals 7
    .param p1    # LX/14N;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171859
    iget-object v0, p1, LX/14N;->z:LX/4cx;

    move-object v0, v0

    .line 171860
    iget v1, v0, LX/4cx;->c:I

    move v1, v1

    .line 171861
    iget v2, v0, LX/4cx;->b:I

    move v3, v2

    .line 171862
    iget-object v2, v0, LX/4cx;->a:Ljava/io/File;

    move-object v2, v2

    .line 171863
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 171864
    new-instance v6, LX/4cK;

    .line 171865
    iget-object v2, v0, LX/4cx;->a:Ljava/io/File;

    move-object v0, v2

    .line 171866
    invoke-direct {v6, v0, v3, v1}, LX/4cK;-><init>(Ljava/io/File;II)V

    .line 171867
    if-eqz p2, :cond_0

    .line 171868
    new-instance v0, LX/4d7;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/4d7;-><init>(Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/4ck;IJ)V

    .line 171869
    iput-object v0, v6, LX/4cK;->d:LX/4cJ;

    .line 171870
    :cond_0
    return-object v6
.end method

.method private a(Landroid/net/Uri;LX/14N;LX/4ck;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 6
    .param p3    # LX/4ck;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171871
    const-string v0, "GET"

    .line 171872
    iget-object v1, p2, LX/14N;->b:Ljava/lang/String;

    move-object v1, v1

    .line 171873
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171874
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 171875
    :goto_0
    invoke-virtual {p2}, LX/14N;->h()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 171876
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v5, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171877
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 171878
    :cond_0
    const-string v0, "POST"

    .line 171879
    iget-object v1, p2, LX/14N;->b:Ljava/lang/String;

    move-object v1, v1

    .line 171880
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171881
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 171882
    iget-object v1, p2, LX/14N;->v:LX/14R;

    move-object v1, v1

    .line 171883
    sget-object v2, LX/14R;->FILE_PART_ENTITY:LX/14R;

    if-ne v1, v2, :cond_1

    .line 171884
    invoke-static {p0, p2, p3}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/14N;LX/4ck;)Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 171885
    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    :cond_1
    move-object v1, v0

    .line 171886
    goto :goto_0

    .line 171887
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported method: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 171888
    iget-object v2, p2, LX/14N;->b:Ljava/lang/String;

    move-object v2, v2

    .line 171889
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171890
    :cond_3
    return-object v1
.end method

.method private static a(Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/0n9;)V
    .locals 7

    .prologue
    .line 171724
    const-string v0, "api_key"

    iget-object v1, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->n:LX/00I;

    invoke-interface {v1}, LX/00I;->d()Ljava/lang/String;

    move-result-object v1

    .line 171725
    invoke-static {p1, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171726
    const/4 v0, 0x0

    .line 171727
    iget v1, p1, LX/0n9;->c:I

    move v2, v1

    .line 171728
    new-array v3, v2, [I

    .line 171729
    new-array v4, v2, [Landroid/util/Pair;

    move v1, v0

    .line 171730
    :goto_0
    if-ge v1, v2, :cond_0

    .line 171731
    invoke-virtual {p1, v1}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    aput-object v5, v4, v1

    .line 171732
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 171733
    :cond_0
    new-instance v1, LX/2Wv;

    invoke-direct {v1}, LX/2Wv;-><init>()V

    invoke-static {v4, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    move v1, v0

    .line 171734
    :goto_1
    if-ge v1, v2, :cond_1

    .line 171735
    aget-object v0, v4, v1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 171736
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 171737
    :cond_1
    move-object v0, v3

    .line 171738
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 171739
    :try_start_0
    invoke-static {v1, p1, v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(Ljava/io/Writer;LX/0n9;[I)V

    .line 171740
    iget-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->n:LX/00I;

    invoke-interface {v0}, LX/00I;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/StringWriter;->append(Ljava/lang/CharSequence;)Ljava/io/StringWriter;

    .line 171741
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/03l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171742
    const-string v1, "sig"

    .line 171743
    invoke-static {p1, v1, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171744
    return-void

    .line 171745
    :catch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "StringWriter cannot throw IOException"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/io/Writer;LX/0n9;[I)V
    .locals 4

    .prologue
    .line 171746
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p2

    if-ge v1, v0, :cond_4

    .line 171747
    aget v0, p2, v1

    .line 171748
    invoke-virtual {p1, v0}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 171749
    const/16 v2, 0x3d

    invoke-virtual {p0, v2}, Ljava/io/Writer;->write(I)V

    .line 171750
    invoke-virtual {p1, v0}, LX/0n9;->c(I)Ljava/lang/Object;

    move-result-object v0

    .line 171751
    if-nez v0, :cond_0

    .line 171752
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 171753
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 171754
    :cond_0
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_1

    instance-of v2, v0, Ljava/lang/Number;

    if-nez v2, :cond_1

    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 171755
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 171756
    :cond_2
    instance-of v2, v0, LX/0nA;

    if-eqz v2, :cond_3

    .line 171757
    check-cast v0, LX/0nA;

    invoke-virtual {v0, p0}, LX/0nA;->a(Ljava/io/Writer;)V

    goto :goto_1

    .line 171758
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported value type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 171759
    :cond_4
    return-void
.end method

.method private static a(Lorg/apache/http/client/methods/HttpUriRequest;LX/14N;)V
    .locals 4

    .prologue
    .line 171717
    iget-object v0, p1, LX/14N;->e:LX/0Px;

    move-object v0, v0

    .line 171718
    if-eqz v0, :cond_0

    .line 171719
    iget-object v0, p1, LX/14N;->e:LX/0Px;

    move-object v2, v0

    .line 171720
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 171721
    invoke-interface {p0, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Lorg/apache/http/Header;)V

    .line 171722
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 171723
    :cond_0
    return-void
.end method

.method private static a(Lorg/apache/http/client/methods/HttpUriRequest;LX/14U;)V
    .locals 4

    .prologue
    .line 171710
    iget-object v0, p1, LX/14U;->f:LX/0Px;

    move-object v0, v0

    .line 171711
    if-eqz v0, :cond_0

    .line 171712
    iget-object v0, p1, LX/14U;->f:LX/0Px;

    move-object v2, v0

    .line 171713
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 171714
    invoke-interface {p0, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Lorg/apache/http/Header;)V

    .line 171715
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 171716
    :cond_0
    return-void
.end method

.method private static a(LX/14N;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 171701
    iget-boolean v1, p0, LX/14N;->q:Z

    move v1, v1

    .line 171702
    if-eqz v1, :cond_1

    .line 171703
    :cond_0
    :goto_0
    return v0

    .line 171704
    :cond_1
    iget-boolean v1, p0, LX/14N;->t:Z

    move v1, v1

    .line 171705
    if-nez v1, :cond_0

    .line 171706
    iget-object v1, p0, LX/14N;->c:Ljava/lang/String;

    move-object v1, v1

    .line 171707
    const-string v2, "method/auth.login"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/auth.getSessionForApp"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/user.confirmContactpointPreconfirmation"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/user.initiatePreconfirmation"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/user.register"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/user.prefillorautocompletecontactpoint"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/user.resetPasswordPreconfirmation"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/user.validateregistrationdata"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "getSsoUserMethod"

    .line 171708
    iget-object v3, p0, LX/14N;->a:Ljava/lang/String;

    move-object v3, v3

    .line 171709
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/auth.extendSSOAccessToken"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/user.sendMessengerOnlyPhoneConfirmationCode"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/user.confirmMessengerOnlyPhone"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/user.createMessengerOnlyAccount"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/user.bypassLoginWithConfirmedMessengerCredentials"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "method/intl.getLocaleSuggestions"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "350685531728/nonuserpushtokens"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "256002347743983/nonuserpushtokens"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "181425161904154/nonuserpushtokens"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "moments_phone_confirmation_code"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "register_moments_only_user"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private b(LX/14N;LX/0n9;LX/4ck;)LX/4cL;
    .locals 6
    .param p3    # LX/4ck;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171687
    new-instance v1, LX/4cM;

    invoke-direct {v1}, LX/4cM;-><init>()V

    .line 171688
    invoke-virtual {v1, p2}, LX/4cM;->a(LX/0n9;)V

    .line 171689
    invoke-virtual {p1}, LX/14N;->m()Ljava/util/List;

    move-result-object v0

    .line 171690
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 171691
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4cQ;

    .line 171692
    iget-object v3, v0, LX/4cQ;->a:Ljava/lang/String;

    move-object v3, v3

    .line 171693
    iget-object v4, v0, LX/4cQ;->c:LX/4cO;

    move-object v0, v4

    .line 171694
    invoke-virtual {v1, v3, v0}, LX/4cL;->a(Ljava/lang/String;LX/4cO;)V

    goto :goto_0

    .line 171695
    :cond_0
    if-eqz p3, :cond_1

    .line 171696
    invoke-virtual {v1}, LX/4cL;->getContentLength()J

    move-result-wide v2

    .line 171697
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 171698
    new-instance v0, LX/4d8;

    invoke-direct {v0, p0, p3, v2, v3}, LX/4d8;-><init>(Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/4ck;J)V

    .line 171699
    iput-object v0, v1, LX/4cM;->a:LX/4cJ;

    .line 171700
    :cond_1
    return-object v1
.end method

.method private static b(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;
    .locals 28

    .prologue
    .line 171685
    new-instance v1, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    const/16 v2, 0xb5a

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static/range {p0 .. p0}, LX/11J;->a(LX/0QB;)LX/0s9;

    move-result-object v3

    check-cast v3, LX/0s9;

    invoke-static/range {p0 .. p0}, LX/11L;->a(LX/0QB;)LX/0s9;

    move-result-object v4

    check-cast v4, LX/0s9;

    const/16 v5, 0xb45

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x19e

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x14c3

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x14c5

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x14c6

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x14c2

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x1473

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x14c9

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const-class v14, LX/00I;

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/00I;

    invoke-static/range {p0 .. p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v15

    check-cast v15, LX/0lp;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v16

    check-cast v16, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/11M;->a(LX/0QB;)LX/11M;

    move-result-object v17

    check-cast v17, LX/11M;

    invoke-static/range {p0 .. p0}, LX/11N;->a(LX/0QB;)LX/11N;

    move-result-object v18

    check-cast v18, LX/11N;

    invoke-static/range {p0 .. p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v19

    check-cast v19, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static/range {p0 .. p0}, LX/11P;->a(LX/0QB;)LX/11P;

    move-result-object v20

    check-cast v20, LX/11P;

    invoke-static/range {p0 .. p0}, LX/11Q;->a(LX/0QB;)LX/11Q;

    move-result-object v21

    check-cast v21, LX/11Q;

    invoke-static/range {p0 .. p0}, LX/0rz;->a(LX/0QB;)LX/0rz;

    move-result-object v22

    check-cast v22, LX/0rz;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v23

    check-cast v23, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v24

    check-cast v24, LX/11S;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v25

    check-cast v25, LX/0WJ;

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v26

    check-cast v26, LX/0oz;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v27

    check-cast v27, LX/0ad;

    invoke-direct/range {v1 .. v27}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;-><init>(LX/0Or;LX/0s9;LX/0s9;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/00I;LX/0lp;LX/0lC;LX/11M;LX/11N;Lcom/facebook/common/perftest/PerfTestConfig;LX/11P;LX/11Q;LX/0rz;LX/0SG;LX/11S;LX/0WJ;LX/0oz;LX/0ad;)V

    .line 171686
    return-object v1
.end method

.method private static b(LX/14N;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 171667
    iget-boolean v2, p0, LX/14N;->q:Z

    move v2, v2

    .line 171668
    if-eqz v2, :cond_1

    .line 171669
    :cond_0
    :goto_0
    return v0

    .line 171670
    :cond_1
    iget-boolean v2, p0, LX/14N;->u:Z

    move v2, v2

    .line 171671
    if-eqz v2, :cond_0

    .line 171672
    iget-boolean v2, p0, LX/14N;->t:Z

    move v2, v2

    .line 171673
    if-eqz v2, :cond_2

    move v0, v1

    .line 171674
    goto :goto_0

    .line 171675
    :cond_2
    iget-object v2, p0, LX/14N;->c:Ljava/lang/String;

    move-object v2, v2

    .line 171676
    const-string v3, "method/auth.login"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/user.confirmContactpointPreconfirmation"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/user.initiatePreconfirmation"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/user.register"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/user.prefillorautocompletecontactpoint"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/user.resetPasswordPreconfirmation"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/user.validateregistrationdata"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/user.sendMessengerOnlyPhoneConfirmationCode"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/user.confirmMessengerOnlyPhone"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/user.createMessengerOnlyAccount"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/user.bypassLoginWithConfirmedMessengerCredentials"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "method/intl.getLocaleSuggestions"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "accountRecoverySendConfirmationCode"

    .line 171677
    iget-object v3, p0, LX/14N;->a:Ljava/lang/String;

    move-object v3, v3

    .line 171678
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "accountRecoveryValidateCode"

    .line 171679
    iget-object v3, p0, LX/14N;->a:Ljava/lang/String;

    move-object v3, v3

    .line 171680
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "requestCaptcha"

    .line 171681
    iget-object v3, p0, LX/14N;->a:Ljava/lang/String;

    move-object v3, v3

    .line 171682
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "validateCaptcha"

    .line 171683
    iget-object v3, p0, LX/14N;->a:Ljava/lang/String;

    move-object v3, v3

    .line 171684
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method private static c(Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/14N;)Z
    .locals 2

    .prologue
    .line 171606
    const-string v0, "aldrin_logged_out_status"

    .line 171607
    iget-object v1, p1, LX/14N;->c:Ljava/lang/String;

    move-object v1, v1

    .line 171608
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "getLanguagePackInfo"

    .line 171609
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171610
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "method/logging.clientevent"

    .line 171611
    iget-object v1, p1, LX/14N;->c:Ljava/lang/String;

    move-object v1, v1

    .line 171612
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "logging_client_events"

    .line 171613
    iget-object v1, p1, LX/14N;->c:Ljava/lang/String;

    move-object v1, v1

    .line 171614
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dbl_remove_nonce"

    .line 171615
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171616
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dbl_change_nonce"

    .line 171617
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171618
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dbl_check_nonce"

    .line 171619
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171620
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dbl_check_password"

    .line 171621
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171622
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dbl_password_set_nonce"

    .line 171623
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171624
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dbl_face_rec"

    .line 171625
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171626
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "messenger_invites"

    .line 171627
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171628
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "recover_accounts"

    .line 171629
    iget-object v1, p1, LX/14N;->c:Ljava/lang/String;

    move-object v1, v1

    .line 171630
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "accountRecoverySendConfirmationCode"

    .line 171631
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171632
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "accountRecoveryValidateCode"

    .line 171633
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171634
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "openidConnectAccountRecovery"

    .line 171635
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171636
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "requestCaptcha"

    .line 171637
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171638
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "validateCaptcha"

    .line 171639
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171640
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "at_work_self_invite"

    .line 171641
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171642
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "checkApprovedMachine"

    .line 171643
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171644
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "login_approval_resend_code"

    .line 171645
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171646
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "at_work_pre_login_info"

    .line 171647
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171648
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "at_work_invite_check"

    .line 171649
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171650
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "at_work_invite_company_info"

    .line 171651
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171652
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "at_work_claim_account"

    .line 171653
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171654
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "install_notifier"

    .line 171655
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171656
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "moments_phone_confirmation_code"

    .line 171657
    iget-object v1, p1, LX/14N;->c:Ljava/lang/String;

    move-object v1, v1

    .line 171658
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "register_moments_only_user"

    .line 171659
    iget-object v1, p1, LX/14N;->c:Ljava/lang/String;

    move-object v1, v1

    .line 171660
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "logged_out_push"

    .line 171661
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171662
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "logged_out_badge"

    .line 171663
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171664
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "UserSemClickTrackingMutation"

    .line 171665
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171666
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->z:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/14N;)Z
    .locals 2

    .prologue
    .line 171587
    const-string v0, "FetchWorkCommunitiesFromPersonalAccountQuery"

    .line 171588
    iget-object v1, p0, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171589
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "at_work_claim_account"

    .line 171590
    iget-object v1, p0, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171591
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "at_work_fetch_account_details"

    .line 171592
    iget-object v1, p0, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171593
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "FetchEmailQuery"

    .line 171594
    iget-object v1, p0, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171595
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/11I;->LOGOUT:LX/11I;

    iget-object v0, v0, LX/11I;->requestNameString:Ljava/lang/String;

    .line 171596
    iget-object v1, p0, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171597
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "at_work_get_name"

    .line 171598
    iget-object v1, p0, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171599
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized e(LX/14N;)Z
    .locals 3

    .prologue
    .line 171575
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->B:LX/0Rf;

    if-nez v0, :cond_1

    .line 171576
    iget-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->A:LX/0ad;

    sget-char v1, LX/0by;->C:C

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171577
    if-nez v0, :cond_0

    .line 171578
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 171579
    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->B:LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171580
    const/4 v0, 0x0

    .line 171581
    :goto_0
    monitor-exit p0

    return v0

    .line 171582
    :cond_0
    :try_start_1
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf([Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->B:LX/0Rf;

    .line 171583
    :cond_1
    iget-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->B:LX/0Rf;

    .line 171584
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 171585
    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 171586
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/14N;LX/14U;LX/0e6;LX/0rp;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)LX/1pW;
    .locals 13
    .param p2    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0rp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/14N;",
            "LX/14U;",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;",
            "LX/0rp",
            "<TPARAMS;>;TPARAMS;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/1pW",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171439
    move-object/from16 v0, p6

    move-object/from16 v1, p3

    invoke-static {v0, v1, p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(Lcom/facebook/common/callercontext/CallerContext;LX/0e6;LX/14N;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v9

    .line 171440
    if-nez p2, :cond_0

    .line 171441
    new-instance p2, LX/14U;

    invoke-direct {p2}, LX/14U;-><init>()V

    .line 171442
    :cond_0
    sget-object v2, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a:LX/0Rf;

    invoke-virtual {p1}, LX/14N;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 171443
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->u:LX/11Q;

    invoke-virtual {v2, p2}, LX/11Q;->a(LX/14U;)V

    .line 171444
    :cond_1
    invoke-virtual {p1}, LX/14N;->f()Ljava/lang/String;

    move-result-object v5

    .line 171445
    sget-object v2, LX/14X;->a:[I

    invoke-virtual {p2}, LX/14U;->b()LX/14V;

    move-result-object v3

    invoke-virtual {v3}, LX/14V;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 171446
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0s9;

    move-object v3, v2

    .line 171447
    :goto_0
    invoke-virtual {p1}, LX/14N;->o()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 171448
    invoke-interface {v3}, LX/0s9;->c()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object v4, v2

    .line 171449
    :goto_1
    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171450
    invoke-static {p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->b(LX/14N;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 171451
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->h:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 171452
    const-string v2, "phprof_sample"

    const-string v6, "1"

    invoke-virtual {v4, v2, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171453
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 171454
    if-eqz v2, :cond_2

    .line 171455
    const-string v6, "phprof_user"

    invoke-virtual {v4, v6, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171456
    :cond_2
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 171457
    const-string v2, "teak_sample"

    const-string v6, "1"

    invoke-virtual {v4, v2, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171458
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 171459
    if-eqz v2, :cond_3

    .line 171460
    const-string v6, "teak_user"

    invoke-virtual {v4, v6, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171461
    :cond_3
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 171462
    const-string v2, "wirehog_sample"

    const-string v6, "1"

    invoke-virtual {v4, v2, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171463
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 171464
    if-eqz v2, :cond_4

    .line 171465
    const-string v6, "wirehog_user"

    invoke-virtual {v4, v6, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171466
    :cond_4
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->k:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 171467
    const-string v2, "artillery_sample"

    const-string v6, "1"

    invoke-virtual {v4, v2, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171468
    :cond_5
    invoke-virtual {p1}, LX/14N;->v()LX/14R;

    move-result-object v2

    .line 171469
    sget-object v6, LX/14R;->AUTO:LX/14R;

    if-ne v2, v6, :cond_6

    .line 171470
    invoke-virtual {p1}, LX/14N;->m()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_10

    invoke-virtual {p1}, LX/14N;->m()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_10

    .line 171471
    sget-object v2, LX/14R;->MULTI_PART_ENTITY:LX/14R;

    .line 171472
    :cond_6
    :goto_2
    invoke-virtual {p1}, LX/14N;->r()Z

    move-result v6

    if-nez v6, :cond_7

    invoke-virtual {p1}, LX/14N;->A()Z

    move-result v6

    if-eqz v6, :cond_11

    :cond_7
    const-string v6, "GET"

    invoke-virtual {p1}, LX/14N;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 171473
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->r:LX/11N;

    invoke-virtual {v2, p1}, LX/11N;->a(LX/14N;)LX/0n9;

    move-result-object v2

    .line 171474
    invoke-static {v2}, LX/14d;->a(LX/0n9;)Ljava/lang/String;

    move-result-object v2

    .line 171475
    const-string v6, "method/mobile.zeroBalanceRedirect"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 171476
    const-string v5, "http"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171477
    :cond_8
    invoke-virtual {v4, v2}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171478
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    move-object v11, v2

    .line 171479
    :goto_3
    invoke-static {p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/14N;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 171480
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 171481
    if-eqz v2, :cond_19

    invoke-virtual {v2}, Lcom/facebook/auth/viewercontext/ViewerContext;->b()Ljava/lang/String;

    move-result-object v2

    .line 171482
    :goto_4
    if-nez v2, :cond_1a

    invoke-static {p0, p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->c(Lcom/facebook/http/protocol/SingleMethodRunnerImpl;LX/14N;)Z

    move-result v4

    if-nez v4, :cond_1a

    invoke-static {p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->d(LX/14N;)Z

    move-result v4

    if-nez v4, :cond_1a

    .line 171483
    new-instance v2, LX/4cl;

    const-string v3, "auth token is null, user logged out?"

    invoke-direct {v2, v3}, LX/4cl;-><init>(Ljava/lang/String;)V

    throw v2

    .line 171484
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->c:LX/0s9;

    move-object v3, v2

    .line 171485
    goto/16 :goto_0

    .line 171486
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->l:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_9

    .line 171487
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->d:LX/0s9;

    move-object v3, v2

    goto/16 :goto_0

    .line 171488
    :cond_9
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0s9;

    move-object v3, v2

    .line 171489
    goto/16 :goto_0

    .line 171490
    :cond_a
    invoke-virtual {p1}, LX/14N;->p()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 171491
    invoke-interface {v3}, LX/0s9;->d()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_1

    .line 171492
    :cond_b
    invoke-virtual {p1}, LX/14N;->s()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 171493
    invoke-interface {v3}, LX/0s9;->f()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_1

    .line 171494
    :cond_c
    invoke-virtual {p1}, LX/14N;->t()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 171495
    invoke-interface {v3}, LX/0s9;->e()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_1

    .line 171496
    :cond_d
    invoke-virtual {p1}, LX/14N;->r()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 171497
    invoke-interface {v3}, LX/0s9;->g()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_1

    .line 171498
    :cond_e
    const-string v2, "method"

    invoke-virtual {v5, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 171499
    invoke-interface {v3}, LX/0s9;->a()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_1

    .line 171500
    :cond_f
    invoke-interface {v3}, LX/0s9;->b()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_1

    .line 171501
    :cond_10
    sget-object v2, LX/14R;->SINGLE_STRING_ENTITY:LX/14R;

    goto/16 :goto_2

    .line 171502
    :cond_11
    invoke-virtual {p1}, LX/14N;->p()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 171503
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p2}, LX/14U;->a()LX/4ck;

    move-result-object v4

    invoke-direct {p0, v2, p1, v4}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(Landroid/net/Uri;LX/14N;LX/4ck;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    move-object v11, v2

    goto/16 :goto_3

    .line 171504
    :cond_12
    const-string v5, "GET"

    invoke-virtual {p1}, LX/14N;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_13

    const-string v5, "POST"

    invoke-virtual {p1}, LX/14N;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_13

    const-string v5, "DELETE"

    invoke-virtual {p1}, LX/14N;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 171505
    :cond_13
    new-instance v5, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 171506
    invoke-direct {p0, p1, p2, v9}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/14N;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)LX/0n9;

    move-result-object v6

    .line 171507
    sget-object v7, LX/14R;->SINGLE_STRING_ENTITY:LX/14R;

    if-ne v2, v7, :cond_17

    .line 171508
    invoke-direct {p0, p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->e(LX/14N;)Z

    move-result v2

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->A:LX/0ad;

    sget-short v7, LX/0by;->z:S

    const/4 v8, 0x0

    invoke-interface {v2, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_16

    const/4 v2, 0x1

    .line 171509
    :goto_5
    invoke-virtual {p1}, LX/14N;->k()Z

    move-result v7

    if-eqz v7, :cond_14

    if-nez v2, :cond_14

    .line 171510
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->v:LX/0rz;

    invoke-virtual {v2, v6}, LX/0rz;->a(LX/0n9;)V

    .line 171511
    invoke-static {v5}, LX/0rz;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 171512
    :cond_14
    invoke-virtual {p1}, LX/14N;->l()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 171513
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->v:LX/0rz;

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0rz;->a(Landroid/net/Uri;)V

    .line 171514
    :cond_15
    invoke-virtual {p2}, LX/14U;->a()LX/4ck;

    move-result-object v2

    invoke-direct {p0, p1, v6, v2}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/14N;LX/0n9;LX/4ck;)Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 171515
    :goto_6
    invoke-static {v2}, LX/14f;->a(Lorg/apache/http/HttpEntity;)Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 171516
    invoke-virtual {v5, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    move-object v11, v5

    .line 171517
    goto/16 :goto_3

    .line 171518
    :cond_16
    const/4 v2, 0x0

    goto :goto_5

    .line 171519
    :cond_17
    invoke-virtual {p2}, LX/14U;->a()LX/4ck;

    move-result-object v2

    invoke-direct {p0, p1, v6, v2}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->b(LX/14N;LX/0n9;LX/4ck;)LX/4cL;

    move-result-object v2

    goto :goto_6

    .line 171520
    :cond_18
    new-instance v2, Ljava/lang/Exception;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported method: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/14N;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2

    .line 171521
    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 171522
    :cond_1a
    const-string v4, "Authorization"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "OAuth "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v4, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171523
    :cond_1b
    invoke-virtual {p1}, LX/14N;->b()Ljava/lang/String;

    move-result-object v2

    .line 171524
    if-nez v2, :cond_1c

    .line 171525
    invoke-interface {v3}, LX/0s9;->h()Ljava/lang/String;

    move-result-object v2

    .line 171526
    :cond_1c
    if-eqz v2, :cond_1d

    .line 171527
    const-string v4, "User-Agent"

    invoke-interface {v11, v4, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171528
    :cond_1d
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->A:LX/0ad;

    sget-short v4, LX/0by;->B:S

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-direct {p0, p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->e(LX/14N;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 171529
    const-string v2, "X-FB-Image-Push-Requested"

    const-string v4, "true"

    invoke-interface {v11, v2, v4}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171530
    :cond_1e
    invoke-static {v11, p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(Lorg/apache/http/client/methods/HttpUriRequest;LX/14N;)V

    .line 171531
    invoke-static {v11, p2}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(Lorg/apache/http/client/methods/HttpUriRequest;LX/14U;)V

    .line 171532
    invoke-interface {v3}, LX/0s9;->i()Ljava/lang/String;

    move-result-object v2

    .line 171533
    if-eqz v2, :cond_1f

    .line 171534
    const-string v3, "X-FB-Connection-Type"

    invoke-interface {v11, v3, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171535
    :cond_1f
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->y:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    .line 171536
    if-eqz v2, :cond_20

    .line 171537
    const-string v3, "X-FB-Connection-Quality"

    invoke-virtual {v2}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v3, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171538
    :cond_20
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->y:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->f()D

    move-result-wide v2

    .line 171539
    const-wide/16 v4, 0x0

    cmpl-double v4, v2, v4

    if-lez v4, :cond_21

    .line 171540
    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    double-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 171541
    const-string v3, "X-FB-Connection-Bandwidth"

    invoke-interface {v11, v3, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171542
    :cond_21
    invoke-virtual {p2}, LX/14U;->e()Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v2

    sget-object v3, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-eq v2, v3, :cond_22

    .line 171543
    const-string v2, "X-FBTrace-Sampled"

    const-string v3, "true"

    invoke-interface {v11, v2, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171544
    const-string v2, "X-FBTrace-Meta"

    invoke-virtual {p2}, LX/14U;->e()Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/fbtrace/FbTraceNode;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v11, v2, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171545
    :cond_22
    invoke-virtual {p1}, LX/14N;->o()Z

    move-result v2

    if-eqz v2, :cond_23

    invoke-virtual {p1}, LX/14N;->q()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 171546
    const-string v2, "X-FB-Video-Upload-Method"

    const-string v3, "chunked"

    invoke-interface {v11, v2, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171547
    const-string v2, "X_FB_VIDEO_WATERFALL_ID"

    invoke-virtual {p1}, LX/14N;->z()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v11, v2, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171548
    :cond_23
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->m:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 171549
    const-string v2, "Date"

    iget-object v3, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->x:LX/11S;

    sget-object v4, LX/1lB;->RFC1123_STYLE:LX/1lB;

    iget-object v5, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->w:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v3, v4, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v11, v2, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171550
    :cond_24
    if-eqz p4, :cond_25

    .line 171551
    invoke-interface/range {p4 .. p5}, LX/0rp;->a_(Ljava/lang/Object;)V

    .line 171552
    :cond_25
    invoke-virtual {p2}, LX/14U;->c()LX/4d1;

    move-result-object v12

    .line 171553
    if-eqz v12, :cond_26

    .line 171554
    invoke-virtual {v12, v11}, LX/4d1;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 171555
    :cond_26
    new-instance v2, LX/159;

    iget-object v6, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->q:LX/11M;

    iget-object v7, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->o:LX/0lp;

    iget-object v8, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->p:LX/0lC;

    move-object v3, p1

    move-object/from16 v4, p5

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v8}, LX/159;-><init>(LX/14N;Ljava/lang/Object;LX/0e6;LX/11M;LX/0lp;LX/0lC;)V

    .line 171556
    invoke-static {p1, p2}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/14N;LX/14U;)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v5

    .line 171557
    invoke-virtual {p1}, LX/14N;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/14N;->u()LX/14Q;

    move-result-object v6

    invoke-virtual {p1}, LX/14N;->w()LX/14P;

    move-result-object v10

    move-object v4, v11

    move-object v7, v2

    move-object v8, p2

    invoke-static/range {v3 .. v10}, LX/14f;->a(Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/facebook/http/interfaces/RequestPriority;LX/14Q;Lorg/apache/http/client/ResponseHandler;LX/14U;Lcom/facebook/common/callercontext/CallerContext;LX/14P;)LX/15D;

    move-result-object v3

    .line 171558
    new-instance v4, LX/15H;

    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-direct {v4, v2, v3}, LX/15H;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/15D;)V

    .line 171559
    invoke-virtual {p1}, LX/14N;->g()LX/0zW;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0zW;->a(LX/15I;)V

    .line 171560
    :try_start_0
    iget-object v2, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v2, v3}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1pW;

    .line 171561
    if-eqz p4, :cond_27

    .line 171562
    invoke-interface/range {p4 .. p5}, LX/0rp;->c(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171563
    :cond_27
    invoke-static {v11}, LX/14f;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 171564
    if-eqz v12, :cond_28

    .line 171565
    const/4 v3, 0x0

    invoke-virtual {v12, v3}, LX/4d1;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    :cond_28
    return-object v2

    .line 171566
    :catch_0
    move-exception v2

    .line 171567
    :try_start_1
    invoke-static {v2}, LX/2BF;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v2

    .line 171568
    if-eqz p4, :cond_29

    .line 171569
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-interface {v0, v1, v2}, LX/0rp;->a(Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v3

    .line 171570
    invoke-static {v3, v2}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Exception;

    .line 171571
    :cond_29
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171572
    :catchall_0
    move-exception v2

    invoke-static {v11}, LX/14f;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 171573
    if-eqz v12, :cond_2a

    .line 171574
    const/4 v3, 0x0

    invoke-virtual {v12, v3}, LX/4d1;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    :cond_2a
    throw v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;
    .locals 13
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;",
            "LX/14U;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 171424
    invoke-static/range {p4 .. p4}, LX/129;->b(Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    .line 171425
    instance-of v0, p1, LX/0ro;

    if-eqz v0, :cond_2

    move-object v1, p1

    .line 171426
    check-cast v1, LX/0ro;

    .line 171427
    invoke-virtual {v1, p2}, LX/0ro;->d(Ljava/lang/Object;)LX/14N;

    move-result-object v2

    .line 171428
    if-eqz v2, :cond_2

    .line 171429
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->t:LX/11P;

    invoke-virtual {v0, v2}, LX/11P;->a(LX/14N;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171430
    iget-object v0, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->t:LX/11P;

    invoke-virtual {v0, v2}, LX/11P;->b(LX/14N;)Ljava/lang/Object;

    move-result-object v0

    .line 171431
    if-nez v0, :cond_0

    move-object v0, p0

    move-object v3, p2

    move-object/from16 v4, p3

    .line 171432
    invoke-direct/range {v0 .. v5}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0ro;LX/14N;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    .line 171433
    iget-object v1, p0, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->t:LX/11P;

    invoke-virtual {v1, v2, v0}, LX/11P;->a(LX/14N;Ljava/lang/Object;)V

    .line 171434
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    move-object v3, p2

    move-object/from16 v4, p3

    .line 171435
    invoke-direct/range {v0 .. v5}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0ro;LX/14N;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 171436
    :cond_2
    invoke-interface {p1, p2}, LX/0e6;->a(Ljava/lang/Object;)LX/14N;

    move-result-object v7

    .line 171437
    invoke-static {p1}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0e6;)LX/0rp;

    move-result-object v10

    move-object v6, p0

    move-object/from16 v8, p3

    move-object v9, p1

    move-object v11, p2

    move-object v12, v5

    invoke-virtual/range {v6 .. v12}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/14N;LX/14U;LX/0e6;LX/0rp;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)LX/1pW;

    move-result-object v0

    .line 171438
    invoke-virtual {v0}, LX/1pW;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
