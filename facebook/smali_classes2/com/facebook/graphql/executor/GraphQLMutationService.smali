.class public Lcom/facebook/graphql/executor/GraphQLMutationService;
.super LX/0te;
.source ""


# instance fields
.field public a:LX/3G7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0td;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1r1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public i:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:LX/1ql;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 155184
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 155185
    iput v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->h:I

    .line 155186
    iput v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->i:I

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 155182
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/graphql/executor/GraphQLMutationService$2;

    invoke-direct {v1, p0}, Lcom/facebook/graphql/executor/GraphQLMutationService$2;-><init>(Lcom/facebook/graphql/executor/GraphQLMutationService;)V

    const v2, -0x222ba8fc

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 155183
    return-void
.end method

.method private declared-synchronized a(IJ)V
    .locals 4

    .prologue
    .line 155175
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->h:I

    if-nez v0, :cond_0

    .line 155176
    invoke-direct {p0}, Lcom/facebook/graphql/executor/GraphQLMutationService;->b()LX/1ql;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->j:LX/1ql;

    .line 155177
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->h:I

    .line 155178
    iput p1, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->i:I

    .line 155179
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->d:LX/0TD;

    new-instance v1, Lcom/facebook/graphql/executor/GraphQLMutationService$1;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/facebook/graphql/executor/GraphQLMutationService$1;-><init>(Lcom/facebook/graphql/executor/GraphQLMutationService;JI)V

    const v2, 0x456b7314

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155180
    monitor-exit p0

    return-void

    .line 155181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(LX/1ql;)V
    .locals 1
    .param p0    # LX/1ql;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 155172
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/1ql;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155173
    invoke-virtual {p0}, LX/1ql;->d()V

    .line 155174
    :cond_0
    return-void
.end method

.method private static a(LX/399;J)V
    .locals 1

    .prologue
    .line 155170
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/399;->a:LX/0zP;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 155171
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/graphql/executor/GraphQLMutationService;LX/3G7;LX/0td;Ljava/util/concurrent/ExecutorService;LX/0TD;LX/1r1;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0

    .prologue
    .line 155169
    iput-object p1, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->a:LX/3G7;

    iput-object p2, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->b:LX/0td;

    iput-object p3, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->c:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->d:LX/0TD;

    iput-object p5, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->e:LX/1r1;

    iput-object p6, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->f:LX/03V;

    iput-object p7, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLMutationService;

    invoke-static {v7}, LX/3G7;->b(LX/0QB;)LX/3G7;

    move-result-object v1

    check-cast v1, LX/3G7;

    invoke-static {v7}, LX/0td;->a(LX/0QB;)LX/0td;

    move-result-object v2

    check-cast v2, LX/0td;

    invoke-static {v7}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v7}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v7}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v5

    check-cast v5, LX/1r1;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v7}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v7

    check-cast v7, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v0 .. v7}, Lcom/facebook/graphql/executor/GraphQLMutationService;->a(Lcom/facebook/graphql/executor/GraphQLMutationService;LX/3G7;LX/0td;Ljava/util/concurrent/ExecutorService;LX/0TD;LX/1r1;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/graphql/executor/GraphQLMutationService;J)V
    .locals 7

    .prologue
    .line 155154
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->b:LX/0td;

    invoke-virtual {v0, p1, p2}, LX/0td;->a(J)Landroid/util/Pair;

    move-result-object v1

    .line 155155
    if-nez v1, :cond_0

    .line 155156
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->f:LX/03V;

    const-string v1, "GraphQLMutationService_Invalid_ID"

    const-string v2, "Unknown operation ID %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 155157
    :goto_0
    return-void

    .line 155158
    :cond_0
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/common/util/concurrent/SettableFuture;

    .line 155159
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, LX/3G6;

    .line 155160
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310029    # 4.499997E-39f

    .line 155161
    iget v4, v1, LX/3G6;->f:I

    move v4, v4

    .line 155162
    const/16 v5, 0xf

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 155163
    iget-object v2, v1, LX/3G6;->b:LX/399;

    move-object v2, v2

    .line 155164
    invoke-static {v2, p1, p2}, Lcom/facebook/graphql/executor/GraphQLMutationService;->a(LX/399;J)V

    .line 155165
    iget-object v2, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->a:LX/3G7;

    const-string v3, "_withservice"

    .line 155166
    iget-object v4, v2, LX/3G7;->h:LX/0Sh;

    invoke-virtual {v4}, LX/0Sh;->b()V

    .line 155167
    invoke-static {v2, v1, v0, v3}, LX/3G7;->b(LX/3G7;LX/3G6;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;)V

    .line 155168
    goto :goto_0
.end method

.method private b()LX/1ql;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 155147
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->e:LX/1r1;

    const/4 v1, 0x1

    const-string v2, "GraphQLMutationService"

    invoke-virtual {v0, v1, v2}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    .line 155148
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1ql;->a(Z)V

    .line 155149
    invoke-virtual {v0}, LX/1ql;->c()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155150
    :goto_0
    return-object v0

    .line 155151
    :catch_0
    move-exception v0

    .line 155152
    const-class v1, Lcom/facebook/graphql/executor/GraphQLMutationService;

    const-string v2, "Failed to create WakeLock, continuing without it."

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 155153
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized b(Lcom/facebook/graphql/executor/GraphQLMutationService;IJ)V
    .locals 1

    .prologue
    .line 155140
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->h:I

    .line 155141
    iget v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->h:I

    if-nez v0, :cond_0

    .line 155142
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->j:LX/1ql;

    invoke-static {v0}, Lcom/facebook/graphql/executor/GraphQLMutationService;->a(LX/1ql;)V

    .line 155143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->j:LX/1ql;

    .line 155144
    invoke-direct {p0}, Lcom/facebook/graphql/executor/GraphQLMutationService;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155145
    :cond_0
    monitor-exit p0

    return-void

    .line 155146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 155139
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not meant to bind() to this service"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x1befe806

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 155136
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 155137
    invoke-static {p0, p0}, Lcom/facebook/graphql/executor/GraphQLMutationService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 155138
    const/16 v1, 0x25

    const v2, -0x1457171f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x69c3e4e1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 155134
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 155135
    const/16 v1, 0x25

    const v2, -0x2e198384

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x2

    const/16 v2, 0x24

    const v3, 0x7bcb0d81

    invoke-static {v6, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 155120
    if-nez p1, :cond_1

    .line 155121
    monitor-enter p0

    .line 155122
    :try_start_0
    iget v3, p0, Lcom/facebook/graphql/executor/GraphQLMutationService;->h:I

    if-nez v3, :cond_0

    :goto_0
    const-string v1, "Got null intent while processing a comment"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 155123
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155124
    invoke-virtual {p0, p3}, Lcom/facebook/graphql/executor/GraphQLMutationService;->stopSelf(I)V

    .line 155125
    const v0, 0x6ff69c8f

    invoke-static {v0, v2}, LX/02F;->d(II)V

    .line 155126
    :goto_1
    return v6

    :cond_0
    move v0, v1

    .line 155127
    goto :goto_0

    .line 155128
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const v1, 0x4c4d4cf2    # 5.3818312E7f

    invoke-static {v1, v2}, LX/02F;->d(II)V

    throw v0

    .line 155129
    :cond_1
    const-string v3, "MUTATION_ID_KEY"

    invoke-virtual {p1, v3, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 155130
    cmp-long v3, v4, v8

    if-eqz v3, :cond_2

    :goto_2
    const-string v1, "Missing extra in service Intent"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 155131
    invoke-direct {p0, p3, v4, v5}, Lcom/facebook/graphql/executor/GraphQLMutationService;->a(IJ)V

    .line 155132
    const v0, -0x7fe9eba3

    invoke-static {v0, v2}, LX/02F;->d(II)V

    goto :goto_1

    :cond_2
    move v0, v1

    .line 155133
    goto :goto_2
.end method
