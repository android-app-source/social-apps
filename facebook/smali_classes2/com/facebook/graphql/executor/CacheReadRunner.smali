.class public Lcom/facebook/graphql/executor/CacheReadRunner;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0zO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final c:LX/1kt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1kt",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final d:LX/0sg;

.field private final e:Ljava/util/concurrent/locks/ReadWriteLock;

.field private final f:LX/1kx;

.field private final g:LX/0tc;

.field private final h:LX/11H;

.field private final i:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final j:LX/0SI;

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final m:LX/0t2;

.field public final n:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final o:LX/0tA;

.field private final p:LX/03V;

.field public final q:LX/0Uh;

.field private final r:LX/0sT;

.field private final s:LX/1kv;

.field public final t:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 310761
    const-class v0, Lcom/facebook/graphql/executor/CacheReadRunner;

    sput-object v0, Lcom/facebook/graphql/executor/CacheReadRunner;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0sg;Ljava/util/concurrent/locks/ReadWriteLock;LX/1kx;LX/0tc;LX/11H;LX/0zO;LX/1kt;LX/0SI;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/util/Set;LX/0t2;LX/0sT;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0tA;LX/03V;LX/0Uh;LX/1kv;I)V
    .locals 2
    .param p9    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/iface/ConsistencyCacheFactory;",
            "Ljava/util/concurrent/locks/ReadWriteLock;",
            "LX/1kx;",
            "LX/0tc;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0zO",
            "<TT;>;",
            "LX/1kt",
            "<TT;>;",
            "LX/0SI;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;",
            "LX/0t2;",
            "LX/0sT;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0tA;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1kv;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 310740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310741
    iput-object p1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->d:LX/0sg;

    .line 310742
    iput-object p2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 310743
    iput-object p3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->f:LX/1kx;

    .line 310744
    iput-object p4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->g:LX/0tc;

    .line 310745
    iput-object p5, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->h:LX/11H;

    .line 310746
    iput-object p6, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310747
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->l:Lcom/google/common/util/concurrent/SettableFuture;

    .line 310748
    iput-object p7, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->c:LX/1kt;

    .line 310749
    iput-object p9, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 310750
    iput-object p8, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->j:LX/0SI;

    .line 310751
    iput-object p10, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->k:Ljava/util/Set;

    .line 310752
    iput-object p11, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->m:LX/0t2;

    .line 310753
    iput-object p12, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->r:LX/0sT;

    .line 310754
    iput-object p13, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 310755
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->o:LX/0tA;

    .line 310756
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->p:LX/03V;

    .line 310757
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->q:LX/0Uh;

    .line 310758
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->s:LX/1kv;

    .line 310759
    move/from16 v0, p18

    iput v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    .line 310760
    return-void
.end method

.method private a(LX/1NB;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 3

    .prologue
    .line 310550
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x310026    # 4.499993E-39f

    iget v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-virtual {p1, p2, v0, v1, v2}, LX/1NB;->a(Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 310551
    if-eq v0, p2, :cond_0

    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310552
    iget-object v2, v1, LX/0zO;->a:LX/0zS;

    move-object v2, v2

    .line 310553
    iget-boolean v2, v2, LX/0zS;->h:Z

    if-eqz v2, :cond_1

    .line 310554
    iget-object v2, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 310555
    sget-object p1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v2, p1, :cond_1

    iget-object v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->c:LX/1kt;

    instance-of v2, v2, LX/1ks;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 310556
    if-eqz v1, :cond_0

    .line 310557
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    invoke-static {v1, v0}, LX/1lN;->b(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 310558
    :cond_0
    return-object v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const v3, 0x310026    # 4.499993E-39f

    .line 310737
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310738
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v1, p1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 310739
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 310735
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/graphql/executor/CacheReadRunner;->a(Ljava/lang/String;I)V

    .line 310736
    return-void
.end method

.method private b()V
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v8, 0x0

    .line 310615
    const-string v0, "CacheReadRunner.tryFetch"

    const v1, -0x6a11eff4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 310616
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    invoke-virtual {v0}, LX/0zO;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310617
    invoke-static {p0}, Lcom/facebook/graphql/executor/CacheReadRunner;->g(Lcom/facebook/graphql/executor/CacheReadRunner;)V

    .line 310618
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->m:LX/0t2;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v11

    .line 310619
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310620
    iget-object v1, v0, LX/0zO;->m:LX/0gW;

    move-object v12, v1

    .line 310621
    iget-object v0, v12, LX/0gW;->f:Ljava/lang/String;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 310622
    :try_start_1
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    iget-object v1, v1, LX/0zO;->a:LX/0zS;

    iget-boolean v1, v1, LX/0zS;->g:Z

    if-eqz v1, :cond_19

    .line 310623
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->g:LX/0tc;

    const/4 v2, 0x0

    invoke-static {v11}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0tc;->b(ZLjava/util/Collection;)LX/1NB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v6

    .line 310624
    :try_start_2
    sget-object v7, LX/1NE;->DB_CACHE:LX/1NE;

    .line 310625
    invoke-virtual {v6, v7}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v1

    .line 310626
    if-eqz v1, :cond_2

    .line 310627
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected upstream cache lock exception"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 310628
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_1

    .line 310629
    :try_start_3
    iget-object v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310630
    iget v3, v2, LX/0zO;->v:I

    move v2, v3

    .line 310631
    invoke-virtual {v1, v2}, LX/1NB;->a(I)V

    :cond_1
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 310632
    :catchall_1
    move-exception v0

    const v1, 0x477923a7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 310633
    :cond_2
    :try_start_4
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->c:LX/1kt;

    iget-object v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->p:LX/03V;

    const v4, 0x310026    # 4.499993E-39f

    iget v5, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-static/range {v0 .. v5}, LX/1l8;->a(Ljava/lang/String;LX/1kt;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/03V;II)Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    move-object v3, v7

    move-object v1, v6

    .line 310634
    :goto_1
    if-nez v2, :cond_18

    :try_start_5
    iget-object v4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    iget-object v4, v4, LX/0zO;->a:LX/0zS;

    iget-boolean v4, v4, LX/0zS;->i:Z

    if-eqz v4, :cond_18

    .line 310635
    if-eqz v1, :cond_3

    .line 310636
    invoke-virtual {v1}, LX/1NB;->e()V

    .line 310637
    :cond_3
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->g:LX/0tc;

    const/4 v4, 0x1

    invoke-static {v11}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0tc;->b(ZLjava/util/Collection;)LX/1NB;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    move-result-object v7

    .line 310638
    :try_start_6
    sget-object v11, LX/1NE;->NETWORK:LX/1NE;

    .line 310639
    invoke-virtual {v7, v11}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v3

    .line 310640
    if-eqz v3, :cond_7

    .line 310641
    sget-object v1, LX/1NE;->MEMORY_CACHE:LX/1NE;

    move-object v4, v3

    move v3, v10

    .line 310642
    :goto_2
    if-eqz v4, :cond_4

    .line 310643
    invoke-virtual {v4}, LX/1NB;->f()V

    .line 310644
    invoke-virtual {v7, v1}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v4

    .line 310645
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 310646
    :cond_4
    iget-object v4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x310026    # 4.499993E-39f

    iget v6, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    const-string v11, "network_blocker_count"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v6, v11, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 310647
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    iget-object v3, v3, LX/0zO;->a:LX/0zS;

    iget-boolean v3, v3, LX/0zS;->g:Z

    if-eqz v3, :cond_17

    .line 310648
    sget-object v6, LX/1NE;->DB_CACHE:LX/1NE;

    .line 310649
    invoke-virtual {v7, v6}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v1

    .line 310650
    if-eqz v1, :cond_5

    .line 310651
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected upstream lock exception"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310652
    :catchall_2
    move-exception v0

    move-object v1, v7

    goto :goto_0

    .line 310653
    :cond_5
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->c:LX/1kt;

    iget-object v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->p:LX/03V;

    const v4, 0x310026    # 4.499993E-39f

    iget v5, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-static/range {v0 .. v5}, LX/1l8;->a(Ljava/lang/String;LX/1kt;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/03V;II)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 310654
    if-eqz v1, :cond_6

    .line 310655
    iget-object v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x310026    # 4.499993E-39f

    iget v4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    const-string v5, "network_dedupped"

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 310656
    :cond_6
    move-object v2, v6

    .line 310657
    :goto_3
    if-nez v1, :cond_16

    .line 310658
    sget-object v11, LX/1NE;->NETWORK:LX/1NE;

    .line 310659
    invoke-virtual {v7, v11}, LX/1NB;->a(LX/1NE;)LX/1NB;

    move-result-object v1

    .line 310660
    if-eqz v1, :cond_7

    .line 310661
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected upstream lock exception"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310662
    :cond_7
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->f:LX/1kx;

    iget-object v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->h:LX/11H;

    iget-object v4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x310026    # 4.499993E-39f

    iget v6, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-static/range {v1 .. v6}, LX/1l8;->a(LX/1kx;LX/0zO;LX/11H;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result-object v1

    move-object v2, v1

    move-object v1, v7

    .line 310663
    :goto_4
    if-nez v2, :cond_9

    .line 310664
    :try_start_7
    invoke-direct {p0}, Lcom/facebook/graphql/executor/CacheReadRunner;->c()V

    .line 310665
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->l:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v2, 0x0

    const v3, -0x5e264270

    invoke-static {v0, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 310666
    if-eqz v1, :cond_8

    .line 310667
    :try_start_8
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310668
    iget v2, v0, LX/0zO;->v:I

    move v0, v2

    .line 310669
    invoke-virtual {v1, v0}, LX/1NB;->a(I)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 310670
    :cond_8
    const v0, -0xbad262d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 310671
    :goto_5
    return-void

    .line 310672
    :cond_9
    :try_start_9
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->c:LX/1kt;

    invoke-interface {v3, v2}, LX/1kt;->c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v2

    .line 310673
    const v7, 0x310026    # 4.499993E-39f

    .line 310674
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-interface {v3, v7, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 310675
    invoke-static {v2}, LX/1lN;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/15i;

    move-result-object v3

    .line 310676
    if-eqz v3, :cond_a

    .line 310677
    invoke-virtual {v3}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 310678
    if-eqz v3, :cond_a

    .line 310679
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    .line 310680
    iget-object v4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v5, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    const-string v6, "buffer_capacity"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v7, v5, v6, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 310681
    :cond_a
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    invoke-virtual {v3}, LX/0zO;->h()Z

    move-result v3

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    invoke-virtual {v3}, LX/0zO;->v()Z

    move-result v3

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->r:LX/0sT;

    invoke-virtual {v3}, LX/0sT;->c()Z

    move-result v3

    if-eqz v3, :cond_10

    move v3, v9

    .line 310682
    :goto_6
    sget-object v4, LX/1NE;->NETWORK:LX/1NE;

    if-ne v11, v4, :cond_12

    .line 310683
    if-eqz v3, :cond_11

    invoke-virtual {v12}, LX/0gW;->t()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 310684
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 310685
    iget-object v4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->s:LX/1kv;

    iget-object v5, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    invoke-virtual {v4, v5, v2, v3}, LX/1kv;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/util/Collection;)LX/4VN;

    move-result-object v9

    .line 310686
    invoke-static {v2}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/1lO;->b(Ljava/util/Collection;)LX/1lO;

    move-result-object v2

    invoke-virtual {v2}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v2

    .line 310687
    invoke-direct {p0}, Lcom/facebook/graphql/executor/CacheReadRunner;->d()V

    .line 310688
    const-string v4, "num_tags"

    invoke-direct {p0, v4, v3}, Lcom/facebook/graphql/executor/CacheReadRunner;->a(Ljava/lang/String;Ljava/util/Collection;)V

    .line 310689
    const-string v3, "num_consistent_fields"

    .line 310690
    iget v4, v9, LX/4VN;->g:I

    move v4, v4

    .line 310691
    invoke-direct {p0, v3, v4}, Lcom/facebook/graphql/executor/CacheReadRunner;->a(Ljava/lang/String;I)V

    .line 310692
    :goto_7
    invoke-direct {p0, v1, v2}, Lcom/facebook/graphql/executor/CacheReadRunner;->a(LX/1NB;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v2

    .line 310693
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->l:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 310694
    iget-object v5, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->q:LX/0Uh;

    const/16 v6, 0x96

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v5

    move v5, v5

    .line 310695
    const v6, 0x310026    # 4.499993E-39f

    iget v7, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-static/range {v1 .. v7}, LX/1l8;->a(LX/1NB;Lcom/facebook/graphql/executor/GraphQLResult;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/quicklog/QuickPerformanceLogger;ZII)V

    .line 310696
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 310697
    :try_start_a
    iget-object v4, v3, LX/0zO;->j:LX/1NI;

    if-eqz v4, :cond_b

    .line 310698
    iget-object v4, v3, LX/0zO;->j:LX/1NI;

    invoke-virtual {v4, v2}, LX/1NI;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1NI;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 310699
    :cond_b
    :goto_8
    :try_start_b
    sget-object v3, LX/1NE;->NETWORK:LX/1NE;

    if-ne v11, v3, :cond_e

    .line 310700
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    iget-object v3, v3, LX/0zO;->a:LX/0zS;

    iget-boolean v3, v3, LX/0zS;->h:Z

    if-nez v3, :cond_c

    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    invoke-virtual {v3}, LX/0zO;->h()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 310701
    :cond_c
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    invoke-virtual {v3}, LX/0zO;->h()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 310702
    if-eqz v9, :cond_13

    .line 310703
    invoke-virtual {v9}, LX/4VN;->a()Z

    move-result v3

    if-nez v3, :cond_14

    .line 310704
    iget-object v3, v1, LX/1NB;->f:LX/1ND;

    check-cast v3, LX/1NC;

    invoke-virtual {v9}, LX/4VN;->d()LX/3Bq;

    move-result-object v4

    iput-object v4, v3, LX/1NC;->a:LX/3Bq;

    .line 310705
    invoke-virtual {v9}, LX/4VN;->d()LX/3Bq;

    move-result-object v8

    move-object v5, v9

    .line 310706
    :goto_9
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x310026    # 4.499993E-39f

    iget v6, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    const/16 v7, 0x97

    invoke-interface {v3, v4, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 310707
    if-eqz v8, :cond_d

    .line 310708
    invoke-static {}, LX/0tA;->a()Ljava/lang/String;

    move-result-object v3

    .line 310709
    iget-object v4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    iget-object v6, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->m:LX/0t2;

    .line 310710
    iget-object v7, v4, LX/0zO;->y:Ljava/lang/String;

    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1a

    .line 310711
    iget-object v7, v4, LX/0zO;->y:Ljava/lang/String;

    .line 310712
    :goto_a
    move-object v4, v7

    .line 310713
    iget-object v6, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->o:LX/0tA;

    invoke-virtual {v6, v3, v4, v8}, LX/0tA;->a(Ljava/lang/String;Ljava/lang/String;LX/3Bq;)V

    .line 310714
    :cond_d
    :goto_b
    iget-object v6, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->k:Ljava/util/Set;

    iget-object v7, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v8, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->p:LX/03V;

    const v9, 0x310026    # 4.499993E-39f

    iget v10, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    move-object v3, v0

    move-object v4, v1

    invoke-static/range {v3 .. v10}, LX/1l8;->a(Ljava/lang/String;LX/1NB;LX/2lk;Ljava/util/Set;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/03V;II)V

    .line 310715
    iget-object v6, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->c:LX/1kt;

    iget-object v7, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    iget-object v8, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v9, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->p:LX/03V;

    const v10, 0x310026    # 4.499993E-39f

    iget v11, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    move-object v3, v1

    move-object v4, v2

    invoke-static/range {v3 .. v11}, LX/1l8;->a(LX/1NB;Lcom/facebook/graphql/executor/GraphQLResult;LX/2lk;LX/1kt;LX/0zO;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/03V;II)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 310716
    :cond_e
    if-eqz v1, :cond_f

    .line 310717
    :try_start_c
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310718
    iget v2, v0, LX/0zO;->v:I

    move v0, v2

    .line 310719
    invoke-virtual {v1, v0}, LX/1NB;->a(I)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 310720
    :cond_f
    const v0, 0xe85486c

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_5

    :cond_10
    move v3, v10

    .line 310721
    goto/16 :goto_6

    .line 310722
    :cond_11
    :try_start_d
    iget-boolean v3, v2, Lcom/facebook/graphql/executor/GraphQLResult;->f:Z

    move v3, v3

    .line 310723
    if-eqz v3, :cond_12

    .line 310724
    invoke-virtual {v2}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v3

    .line 310725
    invoke-direct {p0}, Lcom/facebook/graphql/executor/CacheReadRunner;->d()V

    .line 310726
    const-string v4, "num_tags"

    invoke-direct {p0, v4, v3}, Lcom/facebook/graphql/executor/CacheReadRunner;->a(Ljava/lang/String;Ljava/util/Collection;)V

    .line 310727
    invoke-interface {v3}, Ljava/util/Set;->size()I

    :cond_12
    move-object v9, v8

    goto/16 :goto_7

    .line 310728
    :cond_13
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->d:LX/0sg;

    invoke-virtual {v3}, LX/0sg;->a()LX/2lk;

    move-result-object v5

    .line 310729
    invoke-static {v2, v5}, LX/1l8;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/2lk;)V

    .line 310730
    iget-object v3, v1, LX/1NB;->f:LX/1ND;

    check-cast v3, LX/1NC;

    invoke-interface {v5}, LX/2lk;->d()LX/3Bq;

    move-result-object v4

    iput-object v4, v3, LX/1NC;->a:LX/3Bq;

    .line 310731
    invoke-interface {v5}, LX/2lk;->d()LX/3Bq;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    move-result-object v8

    goto/16 :goto_9

    .line 310732
    :catchall_3
    move-exception v0

    move-object v1, v8

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    goto/16 :goto_0

    :cond_14
    move-object v5, v9

    goto/16 :goto_9

    :cond_15
    move-object v5, v8

    goto :goto_b

    :cond_16
    move-object v11, v2

    move-object v2, v1

    move-object v1, v7

    goto/16 :goto_4

    :cond_17
    move-object v13, v2

    move-object v2, v1

    move-object v1, v13

    goto/16 :goto_3

    :cond_18
    move-object v11, v3

    goto/16 :goto_4

    :cond_19
    move-object v2, v8

    move-object v3, v8

    move-object v1, v8

    goto/16 :goto_1

    .line 310733
    :catch_0
    :try_start_e
    move-exception v4

    .line 310734
    sget-object v5, LX/1l8;->a:Ljava/lang/Class;

    const-string v6, "Failed to subscribe request"

    invoke-static {v5, v6, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :cond_1a
    invoke-virtual {v4, v6}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_a
.end method

.method private c()V
    .locals 4

    .prologue
    .line 310613
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x310026    # 4.499993E-39f

    iget v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    const/16 v3, 0x14

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 310614
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 310611
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x310026    # 4.499993E-39f

    iget v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    const/16 v3, 0x96

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 310612
    return-void
.end method

.method private static g(Lcom/facebook/graphql/executor/CacheReadRunner;)V
    .locals 8

    .prologue
    const v3, 0x310026    # 4.499993E-39f

    .line 310605
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310606
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->r:LX/0sT;

    .line 310607
    iget-object v4, v0, LX/0sT;->b:LX/0W3;

    sget-wide v6, LX/0X5;->fc:J

    invoke-interface {v4, v6, v7}, LX/0W4;->c(J)J

    move-result-wide v4

    long-to-int v4, v4

    move v0, v4

    .line 310608
    if-ltz v0, :cond_0

    .line 310609
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    const-string v2, "spec_experiment"

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 310610
    :cond_0
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const v8, 0x310026    # 4.499993E-39f

    .line 310559
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    .line 310560
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->l:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0SQ;->cancel(Z)Z

    .line 310561
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-interface {v0, v8, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 310562
    :goto_0
    return-void

    .line 310563
    :cond_0
    const-string v0, "CacheReadRunner.run"

    const v1, 0x7698a26a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 310564
    const/4 v0, 0x2

    .line 310565
    :try_start_0
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310026    # 4.499993E-39f

    iget v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    const/16 v4, 0xf

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 310566
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310567
    iget-object v2, v1, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v2

    .line 310568
    if-eqz v1, :cond_4

    .line 310569
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->j:LX/0SI;

    iget-object v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310570
    iget-object v3, v2, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v2, v3

    .line 310571
    invoke-interface {v1, v2}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310572
    :cond_1
    :goto_1
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/graphql/executor/CacheReadRunner;->b()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 310573
    :try_start_2
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310574
    iget-object v2, v1, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v2

    .line 310575
    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v1, :cond_3

    .line 310576
    :cond_2
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->j:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    .line 310577
    :cond_3
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 310578
    :goto_2
    const v1, -0x61fefa53

    invoke-static {v1}, LX/02m;->a(I)V

    .line 310579
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-interface {v1, v8, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0

    .line 310580
    :cond_4
    :try_start_3
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v1, :cond_1

    .line 310581
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->j:LX/0SI;

    iget-object v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-interface {v1, v2}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 310582
    :catchall_0
    move-exception v1

    move v7, v0

    move-object v0, v1

    :goto_3
    const v1, -0x3efe99a

    invoke-static {v1}, LX/02m;->a(I)V

    .line 310583
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-interface {v1, v8, v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v0

    .line 310584
    :catch_0
    move-exception v2

    .line 310585
    const/4 v7, 0x3

    .line 310586
    :try_start_4
    invoke-direct {p0}, Lcom/facebook/graphql/executor/CacheReadRunner;->c()V

    .line 310587
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->l:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 310588
    const-string v0, "CacheReadRunner.run"

    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310589
    iget-object v3, v1, LX/0zO;->m:LX/0gW;

    move-object v1, v3

    .line 310590
    iget-object v3, v1, LX/0gW;->f:Ljava/lang/String;

    move-object v1, v3

    .line 310591
    iget-object v3, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->p:LX/03V;

    iget-object v4, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x310026    # 4.499993E-39f

    iget v6, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->t:I

    invoke-static/range {v0 .. v6}, LX/1l8;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 310592
    :try_start_5
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310593
    iget-object v1, v0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v1

    .line 310594
    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v0, :cond_6

    .line 310595
    :cond_5
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->j:LX/0SI;

    invoke-interface {v0}, LX/0SI;->f()V

    .line 310596
    :cond_6
    iget-object v0, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v0, v7

    .line 310597
    goto :goto_2

    .line 310598
    :catchall_1
    move-exception v1

    move v7, v0

    move-object v0, v1

    :goto_4
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->b:LX/0zO;

    .line 310599
    iget-object v2, v1, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v2

    .line 310600
    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v1, :cond_8

    .line 310601
    :cond_7
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->j:LX/0SI;

    invoke-interface {v1}, LX/0SI;->f()V

    .line 310602
    :cond_8
    iget-object v1, p0, Lcom/facebook/graphql/executor/CacheReadRunner;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 310603
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 310604
    :catchall_3
    move-exception v0

    goto :goto_4
.end method
