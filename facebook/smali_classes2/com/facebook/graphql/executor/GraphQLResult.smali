.class public Lcom/facebook/graphql/executor/GraphQLResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/fbservice/results/BaseResult;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/1NB;

.field public final transient b:J

.field public final transient c:J

.field public final d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final f:Z

.field public final g:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154931
    new-instance v0, LX/0tZ;

    invoke-direct {v0}, LX/0tZ;-><init>()V

    sput-object v0, Lcom/facebook/graphql/executor/GraphQLResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    .line 154953
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 154954
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->e:Ljava/lang/Class;

    .line 154955
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->e:Ljava/lang/Class;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 154956
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 154957
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 154958
    invoke-static {p1}, LX/4By;->c(Landroid/os/Parcel;)Ljava/util/Map;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    .line 154959
    :goto_1
    const-class v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v2

    .line 154960
    if-nez v2, :cond_4

    .line 154961
    sget-object v2, LX/0Re;->a:LX/0Re;

    move-object v2, v2

    .line 154962
    :goto_2
    iput-object v2, p0, Lcom/facebook/graphql/executor/GraphQLResult;->g:LX/0Rf;

    .line 154963
    const-class v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v2

    .line 154964
    if-nez v2, :cond_5

    move-object v2, v1

    :goto_3
    iput-object v2, p0, Lcom/facebook/graphql/executor/GraphQLResult;->h:Ljava/util/Set;

    .line 154965
    iput-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->a:LX/1NB;

    .line 154966
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->k:Ljava/util/Map;

    .line 154967
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->l:Ljava/util/Map;

    .line 154968
    iput-wide v4, p0, Lcom/facebook/graphql/executor/GraphQLResult;->b:J

    .line 154969
    iput-wide v4, p0, Lcom/facebook/graphql/executor/GraphQLResult;->c:J

    .line 154970
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->f:Z

    .line 154971
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    .line 154972
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->j:Ljava/lang/String;

    .line 154973
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->m:Z

    .line 154974
    return-void

    .line 154975
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->e:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    goto :goto_0

    .line 154976
    :cond_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 154977
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    goto :goto_1

    .line 154978
    :cond_2
    if-nez v2, :cond_3

    .line 154979
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    goto :goto_1

    .line 154980
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown value "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154981
    :cond_4
    invoke-static {v2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v2

    goto :goto_2

    .line 154982
    :cond_5
    invoke-static {v2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v2

    goto :goto_3
.end method

.method public constructor <init>(Ljava/lang/Object;LX/0ta;J)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 154951
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v7, v6

    move-object v8, v6

    invoke-direct/range {v1 .. v8}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;LX/1NB;Ljava/util/Map;)V

    .line 154952
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;LX/0ta;JLX/0Rf;Ljava/util/Set;LX/1NB;Ljava/util/Map;Ljava/util/Map;JJZZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p5    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/1NB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p16    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p17    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "LX/0ta;",
            "J",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1NB;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;JJZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 154935
    invoke-direct {p0, p2, p3, p4}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 154936
    iput-object p1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    .line 154937
    if-nez p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->e:Ljava/lang/Class;

    .line 154938
    if-nez p5, :cond_0

    invoke-static {}, LX/0Rf;->of()LX/0Rf;

    move-result-object p5

    :cond_0
    iput-object p5, p0, Lcom/facebook/graphql/executor/GraphQLResult;->g:LX/0Rf;

    .line 154939
    iput-object p6, p0, Lcom/facebook/graphql/executor/GraphQLResult;->h:Ljava/util/Set;

    .line 154940
    iput-object p7, p0, Lcom/facebook/graphql/executor/GraphQLResult;->a:LX/1NB;

    .line 154941
    iput-object p8, p0, Lcom/facebook/graphql/executor/GraphQLResult;->k:Ljava/util/Map;

    .line 154942
    iput-object p9, p0, Lcom/facebook/graphql/executor/GraphQLResult;->l:Ljava/util/Map;

    .line 154943
    iput-wide p10, p0, Lcom/facebook/graphql/executor/GraphQLResult;->c:J

    .line 154944
    iput-wide p12, p0, Lcom/facebook/graphql/executor/GraphQLResult;->b:J

    .line 154945
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->f:Z

    .line 154946
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->m:Z

    .line 154947
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    .line 154948
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->j:Ljava/lang/String;

    .line 154949
    return-void

    .line 154950
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto :goto_0
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;LX/0ta;JLX/0Rf;Ljava/util/Set;LX/1NB;Ljava/util/Map;Ljava/util/Map;JJZZLjava/lang/String;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 154934
    invoke-direct/range {p0 .. p17}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLX/0Rf;Ljava/util/Set;LX/1NB;Ljava/util/Map;Ljava/util/Map;JJZZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V
    .locals 9
    .param p5    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "LX/0ta;",
            "J",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 154932
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    move-object v8, v7

    invoke-direct/range {v1 .. v8}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;LX/1NB;Ljava/util/Map;)V

    .line 154933
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;LX/1NB;Ljava/util/Map;)V
    .locals 19
    .param p5    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1NB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "LX/0ta;",
            "J",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1NB;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154879
    invoke-static {}, LX/0Rf;->of()LX/0Rf;

    move-result-object v5

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const-wide/16 v12, -0x1

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v17}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLX/0Rf;Ljava/util/Set;LX/1NB;Ljava/util/Map;Ljava/util/Map;JJZZLjava/lang/String;Ljava/lang/String;)V

    .line 154880
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;LX/1NB;Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 19
    .param p5    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1NB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "LX/0ta;",
            "J",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1NB;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 154929
    invoke-static {}, LX/0Rf;->of()LX/0Rf;

    move-result-object v5

    const-wide/16 v10, -0x1

    const-wide/16 v12, -0x1

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v16, p9

    move-object/from16 v17, p10

    invoke-direct/range {v0 .. v17}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLX/0Rf;Ljava/util/Set;LX/1NB;Ljava/util/Map;Ljava/util/Map;JJZZLjava/lang/String;Ljava/lang/String;)V

    .line 154930
    return-void
.end method

.method public static a(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 154983
    if-nez p0, :cond_0

    .line 154984
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object p0, v0

    .line 154985
    :goto_0
    return-object p0

    .line 154986
    :cond_0
    instance-of v0, p0, Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 154987
    check-cast p0, Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    goto :goto_0

    .line 154988
    :cond_1
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_2

    .line 154989
    check-cast p0, Ljava/util/Collection;

    goto :goto_0

    .line 154990
    :cond_2
    invoke-static {p0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 154926
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->l:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 154927
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 154928
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->l:Ljava/util/Map;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 154925
    iget-boolean v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->f:Z

    return v0
.end method

.method public final c()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 154924
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    return-object v0
.end method

.method public final d()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 154923
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    invoke-static {v0}, Lcom/facebook/graphql/executor/GraphQLResult;->a(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 154922
    const/4 v0, 0x0

    return v0
.end method

.method public final declared-synchronized e()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 154903
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->h:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 154904
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    .line 154905
    if-nez v0, :cond_4

    .line 154906
    sget-object v1, LX/0Re;->a:LX/0Re;

    move-object v1, v1

    .line 154907
    :goto_0
    move-object v0, v1

    .line 154908
    iput-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->h:Ljava/util/Set;

    .line 154909
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->g:LX/0Rf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->g:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 154910
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->h:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154911
    :goto_1
    monitor-exit p0

    return-object v0

    .line 154912
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 154913
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->g:LX/0Rf;

    goto :goto_1

    .line 154914
    :cond_3
    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->h:Ljava/util/Set;

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->g:LX/0Rf;

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 154915
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 154916
    :cond_4
    instance-of v1, v0, Ljava/util/Map;

    if-nez v1, :cond_5

    instance-of v1, v0, Ljava/util/Collection;

    if-eqz v1, :cond_7

    .line 154917
    :cond_5
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 154918
    invoke-static {v0}, Lcom/facebook/graphql/executor/GraphQLResult;->a(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 154919
    invoke-static {v3}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    goto :goto_2

    .line 154920
    :cond_6
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    goto :goto_0

    .line 154921
    :cond_7
    invoke-static {v0}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v1

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 154881
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 154882
    iget-object v3, p0, Lcom/facebook/graphql/executor/GraphQLResult;->e:Ljava/lang/Class;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 154883
    iget-object v3, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    instance-of v3, v3, Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 154884
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 154885
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 154886
    :goto_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->g:LX/0Rf;

    if-nez v0, :cond_4

    move-object v0, v2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 154887
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->h:Ljava/util/Set;

    if-nez v0, :cond_5

    :goto_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 154888
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->k:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 154889
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->l:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 154890
    iget-boolean v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 154891
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154892
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154893
    return-void

    .line 154894
    :cond_0
    iget-object v3, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    instance-of v3, v3, Ljava/util/List;

    if-eqz v3, :cond_1

    .line 154895
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 154896
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    goto :goto_0

    .line 154897
    :cond_1
    iget-object v3, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    instance-of v3, v3, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v3, :cond_3

    :cond_2
    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 154898
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 154899
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 154900
    goto :goto_3

    .line 154901
    :cond_4
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->g:LX/0Rf;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    .line 154902
    :cond_5
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->h:Ljava/util/Set;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_2
.end method
