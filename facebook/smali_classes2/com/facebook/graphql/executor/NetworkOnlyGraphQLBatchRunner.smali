.class public Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/11E;

.field private final c:LX/11H;

.field public final d:LX/0v6;

.field public final e:LX/03V;

.field private final f:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final g:I

.field public h:I

.field public i:I

.field private final j:LX/11X;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170842
    const-class v0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;

    sput-object v0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0v6;LX/11E;LX/11H;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 2
    .param p1    # LX/0v6;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 170843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170844
    new-instance v0, LX/11W;

    invoke-direct {v0, p0}, LX/11W;-><init>(Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;)V

    iput-object v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->j:LX/11X;

    .line 170845
    iput-object p1, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->d:LX/0v6;

    .line 170846
    iput-object p2, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->b:LX/11E;

    .line 170847
    iput-object p3, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->c:LX/11H;

    .line 170848
    iget-object v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->d:LX/0v6;

    iget-object v1, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->j:LX/11X;

    .line 170849
    iput-object v1, v0, LX/0v6;->e:LX/11X;

    .line 170850
    iput-object p4, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->e:LX/03V;

    .line 170851
    iput-object p5, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 170852
    iget v0, p1, LX/0v6;->n:I

    move v0, v0

    .line 170853
    iput v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->g:I

    .line 170854
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 170855
    const-string v0, "GraphQLBatchRunner.tryFetch"

    const v1, -0x74b9f15d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 170856
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->c:LX/11H;

    iget-object v1, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->b:LX/11E;

    iget-object v2, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->d:LX/0v6;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 170857
    const-string v0, "network"

    iget-object v1, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x310027    # 4.499994E-39f

    iget v3, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->g:I

    invoke-static {v0, v1, v2, v3}, LX/1l8;->a(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 170858
    iget-object v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->d:LX/0v6;

    invoke-virtual {v0}, LX/0v6;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170859
    const v0, -0xc03ced6

    invoke-static {v0}, LX/02m;->a(I)V

    .line 170860
    return-void

    .line 170861
    :catchall_0
    move-exception v0

    const v1, 0x5f734250

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private b()V
    .locals 5

    .prologue
    const v4, 0x310027    # 4.499994E-39f

    .line 170862
    iget-object v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->g:I

    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170863
    iget-object v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->g:I

    const-string v2, "results_returned"

    iget v3, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->h:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v4, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 170864
    iget-object v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->g:I

    const-string v2, "errors_returned"

    iget v3, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->i:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v4, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 170865
    :cond_0
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v7, 0x3

    const/4 v1, 0x2

    const v8, 0x310027    # 4.499994E-39f

    .line 170866
    iget-object v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->g:I

    const/16 v3, 0xf

    invoke-interface {v0, v8, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 170867
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170868
    invoke-direct {p0}, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->b()V

    .line 170869
    iget-object v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->g:I

    invoke-interface {v0, v8, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 170870
    :goto_0
    return-void

    .line 170871
    :catch_0
    move-exception v2

    .line 170872
    :try_start_1
    iget v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->i:I

    iget-object v1, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->d:LX/0v6;

    invoke-virtual {v1, v2}, LX/0v6;->a(Ljava/lang/Throwable;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->i:I

    .line 170873
    const-string v0, "NetworkOnlyGraphQLBatchRunner.run"

    iget-object v1, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->d:LX/0v6;

    .line 170874
    iget-object v3, v1, LX/0v6;->c:Ljava/lang/String;

    move-object v1, v3

    .line 170875
    iget-object v3, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->e:LX/03V;

    iget-object v4, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x310027    # 4.499994E-39f

    iget v6, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->g:I

    invoke-static/range {v0 .. v6}, LX/1l8;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 170876
    invoke-direct {p0}, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->b()V

    .line 170877
    iget-object v0, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->g:I

    invoke-interface {v0, v8, v1, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0

    .line 170878
    :catchall_0
    move-exception v0

    :goto_1
    invoke-direct {p0}, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->b()V

    .line 170879
    iget-object v2, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v3, p0, Lcom/facebook/graphql/executor/NetworkOnlyGraphQLBatchRunner;->g:I

    invoke-interface {v2, v8, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v0

    .line 170880
    :catchall_1
    move-exception v0

    move v1, v7

    goto :goto_1
.end method
