.class public final enum Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum CREATING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum DEPRECATED_11:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum DRAFT:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum NO_CTA:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum PENDING_FUNDING_SOURCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 269712
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269713
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269714
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269715
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269716
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269717
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "REJECTED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269718
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "PENDING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269719
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "CREATING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->CREATING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269720
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269721
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "EXTENDABLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269722
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "DRAFT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->DRAFT:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269723
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "DEPRECATED_11"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->DEPRECATED_11:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269724
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "NO_CTA"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->NO_CTA:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269725
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const-string v1, "PENDING_FUNDING_SOURCE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PENDING_FUNDING_SOURCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269726
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->CREATING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->DRAFT:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->DEPRECATED_11:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->NO_CTA:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PENDING_FUNDING_SOURCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 269727
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .locals 1

    .prologue
    .line 269728
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 269729
    :goto_0
    return-object v0

    .line 269730
    :cond_1
    const-string v0, "INACTIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 269731
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269732
    :cond_2
    const-string v0, "DRAFT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 269733
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->DRAFT:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269734
    :cond_3
    const-string v0, "PENDING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 269735
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269736
    :cond_4
    const-string v0, "ACTIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 269737
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269738
    :cond_5
    const-string v0, "PAUSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 269739
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269740
    :cond_6
    const-string v0, "REJECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 269741
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269742
    :cond_7
    const-string v0, "FINISHED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 269743
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269744
    :cond_8
    const-string v0, "EXTENDABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 269745
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269746
    :cond_9
    const-string v0, "NO_CTA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 269747
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->NO_CTA:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269748
    :cond_a
    const-string v0, "CREATING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 269749
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->CREATING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269750
    :cond_b
    const-string v0, "ERROR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 269751
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 269752
    :cond_c
    const-string v0, "PENDING_FUNDING_SOURCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 269753
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PENDING_FUNDING_SOURCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto/16 :goto_0

    .line 269754
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .locals 1

    .prologue
    .line 269755
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .locals 1

    .prologue
    .line 269756
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    return-object v0
.end method
