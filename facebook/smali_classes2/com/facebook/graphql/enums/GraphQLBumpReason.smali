.class public final enum Lcom/facebook/graphql/enums/GraphQLBumpReason;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBumpReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_COMMENT_FROM_MUTIPLE_OBJECTS:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_FROM_NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_HOT_CONVERSATION:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_IMAGE_NOT_LOADED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_NEW_APP:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_NEW_ATTACHED_PHOTO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_NONE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_OUT_OF_TIME_RANGE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_PRIVACY_UPDATED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_SAVED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_SHARE_COMPOSER_ABANDON:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_SHORT_VPVD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_STORY_TIME:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_UNREAD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_UNREAD_ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_UNREAD_FROM_MUTIPLE_OBJECTS:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_VH_LIVE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum BUMP_VIDEO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 161538
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161539
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_NONE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161540
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_UNREAD"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161541
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_OUT_OF_TIME_RANGE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_OUT_OF_TIME_RANGE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161542
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_ACTION_TYPE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161543
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_STORY_TIME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_STORY_TIME:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161544
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_PRIVACY_UPDATED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_PRIVACY_UPDATED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161545
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_NEW_ATTACHED_PHOTO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_NEW_ATTACHED_PHOTO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161546
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_NEW_APP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_NEW_APP:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161547
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_COMMENT_FROM_MUTIPLE_OBJECTS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_COMMENT_FROM_MUTIPLE_OBJECTS:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161548
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_SHARE_COMPOSER_ABANDON"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_SHARE_COMPOSER_ABANDON:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161549
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_IMAGE_NOT_LOADED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_IMAGE_NOT_LOADED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161550
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_UNREAD_ACTION_TYPE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD_ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161551
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_SHORT_VPVD"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_SHORT_VPVD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161552
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_LIVE_VIDEO"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161553
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_VH_LIVE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_VH_LIVE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161554
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_SAVED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_SAVED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161555
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_HOT_CONVERSATION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_HOT_CONVERSATION:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161556
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_UNREAD_FROM_MUTIPLE_OBJECTS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD_FROM_MUTIPLE_OBJECTS:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161557
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_VIDEO"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_VIDEO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161558
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const-string v1, "BUMP_FROM_NOTIFICATION"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBumpReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_FROM_NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161559
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBumpReason;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_NONE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_OUT_OF_TIME_RANGE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_STORY_TIME:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_PRIVACY_UPDATED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_NEW_ATTACHED_PHOTO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_NEW_APP:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_COMMENT_FROM_MUTIPLE_OBJECTS:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_SHARE_COMPOSER_ABANDON:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_IMAGE_NOT_LOADED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD_ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_SHORT_VPVD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_VH_LIVE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_SAVED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_HOT_CONVERSATION:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD_FROM_MUTIPLE_OBJECTS:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_VIDEO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_FROM_NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBumpReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 161537
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBumpReason;
    .locals 1

    .prologue
    .line 161492
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161493
    :goto_0
    return-object v0

    .line 161494
    :cond_1
    const-string v0, "BUMP_NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 161495
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_NONE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161496
    :cond_2
    const-string v0, "BUMP_UNREAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 161497
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161498
    :cond_3
    const-string v0, "BUMP_OUT_OF_TIME_RANGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 161499
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_OUT_OF_TIME_RANGE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161500
    :cond_4
    const-string v0, "BUMP_ACTION_TYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 161501
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161502
    :cond_5
    const-string v0, "BUMP_STORY_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 161503
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_STORY_TIME:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161504
    :cond_6
    const-string v0, "BUMP_PRIVACY_UPDATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 161505
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_PRIVACY_UPDATED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161506
    :cond_7
    const-string v0, "BUMP_NEW_ATTACHED_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 161507
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_NEW_ATTACHED_PHOTO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161508
    :cond_8
    const-string v0, "BUMP_NEW_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 161509
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_NEW_APP:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161510
    :cond_9
    const-string v0, "BUMP_COMMENT_FROM_MUTIPLE_OBJECTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 161511
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_COMMENT_FROM_MUTIPLE_OBJECTS:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161512
    :cond_a
    const-string v0, "BUMP_SHARE_COMPOSER_ABANDON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 161513
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_SHARE_COMPOSER_ABANDON:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161514
    :cond_b
    const-string v0, "BUMP_IMAGE_NOT_LOADED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 161515
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_IMAGE_NOT_LOADED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto :goto_0

    .line 161516
    :cond_c
    const-string v0, "BUMP_UNREAD_ACTION_TYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 161517
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD_ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto/16 :goto_0

    .line 161518
    :cond_d
    const-string v0, "BUMP_SHORT_VPVD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 161519
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_SHORT_VPVD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto/16 :goto_0

    .line 161520
    :cond_e
    const-string v0, "BUMP_LIVE_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 161521
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto/16 :goto_0

    .line 161522
    :cond_f
    const-string v0, "BUMP_VH_LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 161523
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_VH_LIVE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto/16 :goto_0

    .line 161524
    :cond_10
    const-string v0, "BUMP_SAVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 161525
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_SAVED:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto/16 :goto_0

    .line 161526
    :cond_11
    const-string v0, "BUMP_HOT_CONVERSATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 161527
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_HOT_CONVERSATION:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto/16 :goto_0

    .line 161528
    :cond_12
    const-string v0, "BUMP_UNREAD_FROM_MUTIPLE_OBJECTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 161529
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD_FROM_MUTIPLE_OBJECTS:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto/16 :goto_0

    .line 161530
    :cond_13
    const-string v0, "BUMP_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 161531
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_VIDEO:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto/16 :goto_0

    .line 161532
    :cond_14
    const-string v0, "BUMP_FROM_NOTIFICATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 161533
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_FROM_NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto/16 :goto_0

    .line 161534
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBumpReason;
    .locals 1

    .prologue
    .line 161536
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBumpReason;
    .locals 1

    .prologue
    .line 161535
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBumpReason;

    return-object v0
.end method
