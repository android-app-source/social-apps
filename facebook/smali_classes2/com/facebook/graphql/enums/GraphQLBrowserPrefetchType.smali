.class public final enum Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

.field public static final enum HTML_ONLY:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

.field public static final enum MEDIA:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

.field public static final enum PRERENDER:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

.field public static final enum RENDER_BLOCKING:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 214330
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    .line 214331
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    const-string v1, "HTML_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->HTML_ONLY:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    .line 214332
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    const-string v1, "RENDER_BLOCKING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->RENDER_BLOCKING:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    .line 214333
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    const-string v1, "MEDIA"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    .line 214334
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    const-string v1, "PRERENDER"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->PRERENDER:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    .line 214335
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->HTML_ONLY:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->RENDER_BLOCKING:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->PRERENDER:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 214336
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;
    .locals 1

    .prologue
    .line 214337
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    .line 214338
    :goto_0
    return-object v0

    .line 214339
    :cond_1
    const-string v0, "HTML_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214340
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->HTML_ONLY:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    goto :goto_0

    .line 214341
    :cond_2
    const-string v0, "RENDER_BLOCKING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 214342
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->RENDER_BLOCKING:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    goto :goto_0

    .line 214343
    :cond_3
    const-string v0, "MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 214344
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    goto :goto_0

    .line 214345
    :cond_4
    const-string v0, "PRERENDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 214346
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->PRERENDER:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    goto :goto_0

    .line 214347
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;
    .locals 1

    .prologue
    .line 214348
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;
    .locals 1

    .prologue
    .line 214349
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    return-object v0
.end method
