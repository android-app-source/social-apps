.class public final enum Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

.field public static final enum FORMATTED_TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

.field public static final enum LEARNING_NUX_SECOND_STEP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

.field public static final enum LEARNING_NUX_SERP_SUCCESS:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

.field public static final enum LEARNING_TYPEAHEAD_TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

.field public static final enum TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 189076
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 189077
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const-string v1, "TOOLTIP"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 189078
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const-string v1, "LEARNING_NUX_SECOND_STEP"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SECOND_STEP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 189079
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const-string v1, "LEARNING_NUX_SERP_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SERP_SUCCESS:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 189080
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const-string v1, "FORMATTED_TOOLTIP"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->FORMATTED_TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 189081
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const-string v1, "LEARNING_TYPEAHEAD_TOOLTIP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_TYPEAHEAD_TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 189082
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SECOND_STEP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SERP_SUCCESS:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->FORMATTED_TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_TYPEAHEAD_TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 189075
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;
    .locals 1

    .prologue
    .line 189060
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    .line 189061
    :goto_0
    return-object v0

    .line 189062
    :cond_1
    const-string v0, "TOOLTIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 189063
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    goto :goto_0

    .line 189064
    :cond_2
    const-string v0, "LEARNING_NUX_SECOND_STEP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 189065
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SECOND_STEP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    goto :goto_0

    .line 189066
    :cond_3
    const-string v0, "LEARNING_NUX_SERP_SUCCESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 189067
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SERP_SUCCESS:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    goto :goto_0

    .line 189068
    :cond_4
    const-string v0, "FORMATTED_TOOLTIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 189069
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->FORMATTED_TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    goto :goto_0

    .line 189070
    :cond_5
    const-string v0, "LEARNING_TYPEAHEAD_TOOLTIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 189071
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_TYPEAHEAD_TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    goto :goto_0

    .line 189072
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;
    .locals 1

    .prologue
    .line 189074
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;
    .locals 1

    .prologue
    .line 189073
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    return-object v0
.end method
