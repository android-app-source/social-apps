.class public final enum Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

.field public static final enum DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

.field public static final enum FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

.field public static final enum OFFLINE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

.field public static final enum POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

.field public static final enum RETRYING_IN_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

.field public static final enum SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

.field public static final enum TRANSCODING_FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 264895
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 264896
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    const-string v1, "POSTING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 264897
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    const-string v1, "RETRYING_IN_BACKGROUND"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->RETRYING_IN_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 264898
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 264899
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    const-string v1, "OFFLINE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->OFFLINE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 264900
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    const-string v1, "FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 264901
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    const-string v1, "TRANSCODING_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->TRANSCODING_FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 264902
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    const-string v1, "DELETED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 264903
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->RETRYING_IN_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->OFFLINE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->TRANSCODING_FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 264904
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;
    .locals 1

    .prologue
    .line 264905
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 264906
    :goto_0
    return-object v0

    .line 264907
    :cond_1
    const-string v0, "POSTING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264908
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_0

    .line 264909
    :cond_2
    const-string v0, "RETRYING_IN_BACKGROUND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 264910
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->RETRYING_IN_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_0

    .line 264911
    :cond_3
    const-string v0, "SUCCESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 264912
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_0

    .line 264913
    :cond_4
    const-string v0, "OFFLINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 264914
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->OFFLINE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_0

    .line 264915
    :cond_5
    const-string v0, "FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 264916
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_0

    .line 264917
    :cond_6
    const-string v0, "TRANSCODING_FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 264918
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->TRANSCODING_FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_0

    .line 264919
    :cond_7
    const-string v0, "DELETED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 264920
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_0

    .line 264921
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;
    .locals 1

    .prologue
    .line 264922
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;
    .locals 1

    .prologue
    .line 264923
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    return-object v0
.end method
