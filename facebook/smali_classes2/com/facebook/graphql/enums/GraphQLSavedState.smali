.class public final enum Lcom/facebook/graphql/enums/GraphQLSavedState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSavedState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public static final enum ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public static final enum NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public static final enum NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public static final enum SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 312654
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 312655
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    const-string v1, "NOT_SAVABLE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSavedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 312656
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    const-string v1, "NOT_SAVED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSavedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 312657
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    const-string v1, "SAVED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSavedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 312658
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    const-string v1, "ARCHIVED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLSavedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 312659
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 312653
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 1

    .prologue
    .line 312660
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 312661
    :goto_0
    return-object v0

    .line 312662
    :cond_1
    const-string v0, "NOT_SAVABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 312663
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0

    .line 312664
    :cond_2
    const-string v0, "NOT_SAVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 312665
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0

    .line 312666
    :cond_3
    const-string v0, "SAVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 312667
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0

    .line 312668
    :cond_4
    const-string v0, "ARCHIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 312669
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0

    .line 312670
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 1

    .prologue
    .line 312651
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 1

    .prologue
    .line 312652
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method
