.class public final enum Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum AGGREGATED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum BEM_MERGEABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum BLENDED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum BLENDED_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum BLENDED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum BLENDED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum BOOKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum CARD_FADE_IN_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum CARD_NO_FADE_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum CELEBRITY_TOP_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum COMMERCE_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum COMPACT_POST_SETS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum DENSE_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum DENSE_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum EMPTY_RESULTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum ENTITY_COMPACT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum ENTITY_FULLWIDTH_FADEIN_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum ENTITY_FULLWIDTH_FADEIN_NONEXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum ENTITY_LARGER_FOOTER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum ENTITY_LARGER_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum ENTITY_LARGE_SNIPPET:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum FINITE_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum FULL_CARD_FOOTER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum FULL_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum GAMES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum LARGE_SPACING:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum LATEST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum MARKETPLACE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum MOVIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum MUSIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum NEWS_LINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum PAGES_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum PEOPLE_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum PUBLIC_POST_LIMITED_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum PUBLIC_POST_UNLIMITED_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum QUERY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum SONGS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum SUBHEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum TRENDING_FINITE_SERP_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum VIDEO_PERMALINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum VIDEO_SHARE_PERMALINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum VIDEO_STATE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public static final enum WIKIPEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 315362
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315363
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "USERS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315364
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "PAGES"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315365
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "BOOKS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BOOKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315366
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "MOVIES"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MOVIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315367
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "MUSIC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MUSIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315368
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "GROUPS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315369
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "STORIES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315370
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "AGGREGATED_STORIES"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->AGGREGATED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315371
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "PHOTOS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315372
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "VIDEOS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315373
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "VIDEOS_LIVE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315374
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "VIDEO_PUBLISHERS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315375
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "VIDEOS_WEB"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315376
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "VIDEO_PERMALINK"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PERMALINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315377
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "VIDEO_SHARE_PERMALINK"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_SHARE_PERMALINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315378
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "VIDEO_STATE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_STATE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315379
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "APPS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315380
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "GAMES"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GAMES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315381
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "PLACES"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315382
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "EVENTS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315383
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "WIKIPEDIA"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WIKIPEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315384
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "BLENDED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315385
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "QUERY"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->QUERY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315386
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "BLENDED_ENTITIES"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315387
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "BLENDED_VIDEOS"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315388
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "EMPTY_RESULTS"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EMPTY_RESULTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315389
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "FINITE_MODULE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FINITE_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315390
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "COMPACT_POST_SETS_MODULE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->COMPACT_POST_SETS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315391
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "LARGE_SPACING"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LARGE_SPACING:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315392
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "WEB"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315393
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "ENTITY_FULLWIDTH_FADEIN_EXPANDABLE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_FULLWIDTH_FADEIN_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315394
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "ENTITY_FULLWIDTH_FADEIN_NONEXPANDABLE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_FULLWIDTH_FADEIN_NONEXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315395
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "ENTITY_LARGE_SNIPPET"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_LARGE_SNIPPET:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315396
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "ENTITY_COMPACT"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_COMPACT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315397
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "ENTITY_LARGER_HEADER"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_LARGER_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315398
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "ENTITY_LARGER_FOOTER"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_LARGER_FOOTER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315399
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "ENTITY_HSCROLL"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315400
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "CARD_NO_FADE_EXPANDABLE"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->CARD_NO_FADE_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315401
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "CARD_FADE_IN_EXPANDABLE"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->CARD_FADE_IN_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315402
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "PUBLIC_POST_LIMITED_SOCIAL"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PUBLIC_POST_LIMITED_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315403
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "PUBLIC_POST_UNLIMITED_SOCIAL"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PUBLIC_POST_UNLIMITED_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315404
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "CELEBRITY_TOP_MEDIA"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->CELEBRITY_TOP_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315405
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "SALE_POST"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315406
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "LATEST"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LATEST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315407
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "TRENDING_FINITE_SERP_SEE_MORE"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->TRENDING_FINITE_SERP_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315408
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "MARKETPLACE"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MARKETPLACE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315409
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "DENSE_MEDIA"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315410
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "DENSE_STORIES"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315411
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "BLENDED_PHOTOS"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315412
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "NEWS_LINK"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->NEWS_LINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315413
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "COMMERCE_PRODUCTS"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->COMMERCE_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315414
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "SONGS"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SONGS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315415
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "FULL_CARD_HEADER"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FULL_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315416
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "FULL_CARD_FOOTER"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FULL_CARD_FOOTER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315417
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "SUBHEADER"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SUBHEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315418
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "PAGES_HSCROLL"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315419
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "BLENDED_MORE"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315420
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "BEM_MERGEABLE"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BEM_MERGEABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315421
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "BLENDED_STORIES"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315422
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const-string v1, "PEOPLE_POSTS"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PEOPLE_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315423
    const/16 v0, 0x3d

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BOOKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MOVIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MUSIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->AGGREGATED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PERMALINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_SHARE_PERMALINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_STATE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GAMES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WIKIPEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->QUERY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EMPTY_RESULTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FINITE_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->COMPACT_POST_SETS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LARGE_SPACING:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_FULLWIDTH_FADEIN_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_FULLWIDTH_FADEIN_NONEXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_LARGE_SNIPPET:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_COMPACT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_LARGER_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_LARGER_FOOTER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->CARD_NO_FADE_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->CARD_FADE_IN_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PUBLIC_POST_LIMITED_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PUBLIC_POST_UNLIMITED_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->CELEBRITY_TOP_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LATEST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->TRENDING_FINITE_SERP_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MARKETPLACE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->NEWS_LINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->COMMERCE_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SONGS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FULL_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FULL_CARD_FOOTER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SUBHEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BEM_MERGEABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PEOPLE_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 315424
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 2

    .prologue
    .line 315425
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 315426
    :goto_0
    return-object v0

    .line 315427
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 315428
    packed-switch v0, :pswitch_data_0

    .line 315429
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315430
    :pswitch_1
    const-string v0, "MOVIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 315431
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MOVIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315432
    :cond_2
    const-string v0, "VIDEO_SHARE_PERMALINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 315433
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_SHARE_PERMALINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315434
    :cond_3
    const-string v0, "AGGREGATED_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 315435
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->AGGREGATED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315436
    :cond_4
    const-string v0, "PUBLIC_POST_UNLIMITED_SOCIAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 315437
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PUBLIC_POST_UNLIMITED_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315438
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315439
    :pswitch_2
    const-string v0, "COMMERCE_PRODUCTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 315440
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->COMMERCE_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315441
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315442
    :pswitch_3
    const-string v0, "PAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 315443
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315444
    :cond_7
    const-string v0, "ENTITY_FULLWIDTH_FADEIN_EXPANDABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 315445
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_FULLWIDTH_FADEIN_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315446
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 315447
    :pswitch_4
    const-string v0, "PLACES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 315448
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315449
    :cond_9
    const-string v0, "PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 315450
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315451
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315452
    :pswitch_5
    const-string v0, "FULL_CARD_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 315453
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FULL_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315454
    :cond_b
    const-string v0, "FULL_CARD_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 315455
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FULL_CARD_FOOTER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315456
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315457
    :pswitch_6
    const-string v0, "BLENDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 315458
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315459
    :cond_d
    const-string v0, "ENTITY_FULLWIDTH_FADEIN_NONEXPANDABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 315460
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_FULLWIDTH_FADEIN_NONEXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315461
    :cond_e
    const-string v0, "ENTITY_LARGE_SNIPPET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 315462
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_LARGE_SNIPPET:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315463
    :cond_f
    const-string v0, "SONGS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 315464
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SONGS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315465
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315466
    :pswitch_7
    const-string v0, "USERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 315467
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315468
    :cond_11
    const-string v0, "STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 315469
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315470
    :cond_12
    const-string v0, "ENTITY_LARGER_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 315471
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_LARGER_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315472
    :cond_13
    const-string v0, "ENTITY_LARGER_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 315473
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_LARGER_FOOTER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315474
    :cond_14
    const-string v0, "ENTITY_HSCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 315475
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315476
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315477
    :pswitch_8
    const-string v0, "SALE_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 315478
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315479
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315480
    :pswitch_9
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 315481
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315482
    :cond_17
    const-string v0, "BLENDED_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 315483
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315484
    :cond_18
    const-string v0, "PEOPLE_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 315485
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PEOPLE_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315486
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315487
    :pswitch_a
    const-string v0, "SUBHEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 315488
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SUBHEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315489
    :cond_1a
    const-string v0, "BEM_MERGEABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 315490
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BEM_MERGEABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315491
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315492
    :pswitch_b
    const-string v0, "TRENDING_FINITE_SERP_SEE_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 315493
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->TRENDING_FINITE_SERP_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315494
    :cond_1c
    const-string v0, "NEWS_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 315495
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->NEWS_LINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315496
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315497
    :pswitch_c
    const-string v0, "FINITE_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 315498
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FINITE_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315499
    :cond_1e
    const-string v0, "DENSE_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 315500
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315501
    :cond_1f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315502
    :pswitch_d
    const-string v0, "MUSIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 315503
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MUSIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315504
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315505
    :pswitch_e
    const-string v0, "PAGES_HSCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 315506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315507
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315508
    :pswitch_f
    const-string v0, "APPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 315509
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315510
    :cond_22
    const-string v0, "LARGE_SPACING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 315511
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LARGE_SPACING:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315512
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315513
    :pswitch_10
    const-string v0, "VIDEO_PUBLISHERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 315514
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315515
    :cond_24
    const-string v0, "MARKETPLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 315516
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MARKETPLACE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315517
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315518
    :pswitch_11
    const-string v0, "BOOKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 315519
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BOOKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315520
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315521
    :pswitch_12
    const-string v0, "CARD_NO_FADE_EXPANDABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 315522
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->CARD_NO_FADE_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315523
    :cond_27
    const-string v0, "CARD_FADE_IN_EXPANDABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 315524
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->CARD_FADE_IN_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315525
    :cond_28
    const-string v0, "CELEBRITY_TOP_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 315526
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->CELEBRITY_TOP_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315527
    :cond_29
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315528
    :pswitch_13
    const-string v0, "COMPACT_POST_SETS_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 315529
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->COMPACT_POST_SETS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315530
    :cond_2a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315531
    :pswitch_14
    const-string v0, "EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 315532
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315533
    :cond_2b
    const-string v0, "WEB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 315534
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315535
    :cond_2c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315536
    :pswitch_15
    const-string v0, "GAMES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 315537
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GAMES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315538
    :cond_2d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315539
    :pswitch_16
    const-string v0, "VIDEO_PERMALINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 315540
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PERMALINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315541
    :cond_2e
    const-string v0, "GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 315542
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315543
    :cond_2f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315544
    :pswitch_17
    const-string v0, "VIDEOS_LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 315545
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315546
    :cond_30
    const-string v0, "VIDEO_STATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 315547
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_STATE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315548
    :cond_31
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315549
    :pswitch_18
    const-string v0, "QUERY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 315550
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->QUERY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315551
    :cond_32
    const-string v0, "BLENDED_VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 315552
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315553
    :cond_33
    const-string v0, "BLENDED_PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 315554
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315555
    :cond_34
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315556
    :pswitch_19
    const-string v0, "VIDEOS_WEB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 315557
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315558
    :cond_35
    const-string v0, "DENSE_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 315559
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315560
    :cond_36
    const-string v0, "BLENDED_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 315561
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315562
    :cond_37
    const-string v0, "PUBLIC_POST_LIMITED_SOCIAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 315563
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PUBLIC_POST_LIMITED_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315564
    :cond_38
    const-string v0, "LATEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 315565
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LATEST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315566
    :cond_39
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315567
    :pswitch_1a
    const-string v0, "WIKIPEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 315568
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WIKIPEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315569
    :cond_3a
    const-string v0, "BLENDED_ENTITIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 315570
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315571
    :cond_3b
    const-string v0, "EMPTY_RESULTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 315572
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EMPTY_RESULTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315573
    :cond_3c
    const-string v0, "ENTITY_COMPACT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 315574
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_COMPACT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    .line 315575
    :cond_3d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 1

    .prologue
    .line 315576
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 1

    .prologue
    .line 315577
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    return-object v0
.end method
