.class public final enum Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

.field public static final enum HIGH:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

.field public static final enum LOW:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 156829
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    .line 156830
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    .line 156831
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->LOW:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    .line 156832
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->HIGH:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    .line 156833
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->LOW:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->HIGH:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 156834
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;
    .locals 1

    .prologue
    .line 156835
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    .line 156836
    :goto_0
    return-object v0

    .line 156837
    :cond_1
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156838
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    goto :goto_0

    .line 156839
    :cond_2
    const-string v0, "LOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 156840
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->LOW:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    goto :goto_0

    .line 156841
    :cond_3
    const-string v0, "HIGH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 156842
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->HIGH:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    goto :goto_0

    .line 156843
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;
    .locals 1

    .prologue
    .line 156844
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;
    .locals 1

    .prologue
    .line 156845
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    return-object v0
.end method
