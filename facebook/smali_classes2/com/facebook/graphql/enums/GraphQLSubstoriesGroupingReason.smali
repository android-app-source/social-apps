.class public final enum Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

.field public static final enum BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

.field public static final enum COMPACTNESS:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

.field public static final enum CONTEXT:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

.field public static final enum POST_CHANNEL:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 263468
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    .line 263469
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    const-string v1, "CONTEXT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->CONTEXT:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    .line 263470
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    const-string v1, "COMPACTNESS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->COMPACTNESS:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    .line 263471
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    const-string v1, "POST_CHANNEL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->POST_CHANNEL:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    .line 263472
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    const-string v1, "BIRTHDAY"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    .line 263473
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->CONTEXT:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->COMPACTNESS:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->POST_CHANNEL:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 263487
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;
    .locals 1

    .prologue
    .line 263476
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    .line 263477
    :goto_0
    return-object v0

    .line 263478
    :cond_1
    const-string v0, "CONTEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 263479
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->CONTEXT:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    goto :goto_0

    .line 263480
    :cond_2
    const-string v0, "COMPACTNESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 263481
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->COMPACTNESS:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    goto :goto_0

    .line 263482
    :cond_3
    const-string v0, "POST_CHANNEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 263483
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->POST_CHANNEL:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    goto :goto_0

    .line 263484
    :cond_4
    const-string v0, "BIRTHDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 263485
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    goto :goto_0

    .line 263486
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;
    .locals 1

    .prologue
    .line 263475
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;
    .locals 1

    .prologue
    .line 263474
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    return-object v0
.end method
