.class public final enum Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

.field public static final enum BACKDATED:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

.field public static final enum CREATED:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

.field public static final enum DELEGATE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 263448
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    .line 263449
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    const-string v1, "BACKDATED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->BACKDATED:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    .line 263450
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    const-string v1, "CREATED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->CREATED:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    .line 263451
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    const-string v1, "DELEGATE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->DELEGATE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    .line 263452
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    .line 263453
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->BACKDATED:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->CREATED:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->DELEGATE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 263454
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;
    .locals 1

    .prologue
    .line 263455
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    .line 263456
    :goto_0
    return-object v0

    .line 263457
    :cond_1
    const-string v0, "BACKDATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 263458
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->BACKDATED:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    goto :goto_0

    .line 263459
    :cond_2
    const-string v0, "CREATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 263460
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->CREATED:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    goto :goto_0

    .line 263461
    :cond_3
    const-string v0, "DELEGATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 263462
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->DELEGATE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    goto :goto_0

    .line 263463
    :cond_4
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 263464
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    goto :goto_0

    .line 263465
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;
    .locals 1

    .prologue
    .line 263466
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;
    .locals 1

    .prologue
    .line 263467
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    return-object v0
.end method
