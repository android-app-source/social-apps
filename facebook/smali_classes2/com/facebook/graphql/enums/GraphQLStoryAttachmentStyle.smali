.class public final enum Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AIRLINE_BOARDING_PASS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AIRLINE_CHECKIN_REMINDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AIRLINE_CHECK_IN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AIRLINE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AIRLINE_FLIGHT_RESCHEDULE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AIRLINE_FLIGHT_RESCHEDULE_UPDATE_BUBBLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AIRLINE_ITINERARY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AIRLINE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ANIMATED_IMAGE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ANIMATED_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ANIMATED_IMAGE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ANIMATED_IMAGE_VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ANSWER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum APPLICATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ASK_FRIENDS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ATTACHED_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ATTRIBUTED_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AVATAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AVATAR_LARGE_COVER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AVATAR_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AVATAR_WITH_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AVATAR_WITH_EGO_ACTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AVATAR_WITH_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum AVATAR_WITH_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum BALLOT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum BOOK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum BUSINESS_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum BUSINESS_MESSAGE_ITEMS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum CANCELED_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum CENTERED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum CITY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum COMMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum COMMENT_PLACE_INFO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum COMMERCE_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum COMMERCE_PRODUCT_MINI:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum COMMERCE_PRODUCT_MINI_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum COMMERCE_STORE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum CONNECTION_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum CONNECTION_QUESTION_OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum COUPON:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum CULTURAL_MOMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum CULTURAL_MOMENT_HOLIDAY_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum DEPRECATED_10:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum DEPRECATED_214:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum DEPRECATED_23:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum DEPRECATED_234:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum DEPRECATED_46:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum DISCUSSION_COMMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum DISCUSSION_CONVERSATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum DYNAMIC_GAME_BOARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum DYNAMIC_MULTI_SHARE_ITEMS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EGO_HSCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ENDORSEMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ENHANCED_LINK_REDDIT_POST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ENHANCED_LINK_YELP_BUSINESS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EVENTS_PENDING_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EVENT_CALENDAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EVENT_REMINDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EVENT_TICKET:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EVENT_TOUR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EXPERIENCE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EXTERNAL_GALLERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EXTERNAL_OG_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum EXTERNAL_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FACEPILE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FILE_UPLOAD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FINANCIAL_BILL_PAYMENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FITNESS_COURSE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FRIEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FUNDRAISER_FOR_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FUNDRAISER_PAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FUNDRAISER_PERSON_FOR_PERSON:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum FUNDRAISER_PERSON_TO_CHARITY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GALLERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GAMES_INSTANT_PLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GAMETIME:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GAMETIME_LEAGUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GAMETIME_PLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GENIE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GIFT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GOODWILL_THROWBACK_ANNIVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GOODWILL_THROWBACK_FACEVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD_IPB:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GOODWILL_THROWBACK_FRIENDVERSARY_DATA_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD_IPB:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GOODWILL_THROWBACK_VIDEO_BASIC:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GOODWILL_WEATHER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GREETING_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GROUP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GROUP_ADD_MEMBERS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GROUP_JOIN_REQUEST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GROUP_MEMBER_ADDED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GROUP_PENDING_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GROUP_QUIZ:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GROUP_REPORTED_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum GROUP_SELL_PRODUCT_ITEM_MARK_AS_SOLD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum HIGH_SCORE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum H_SCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum IMPLICIT_PLACE_LIST_CONVERSION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum INSPIRATION_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum INSTANT_ARTICLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum INSTANT_ARTICLE_LEGACY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum INSTANT_ARTICLE_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum INSTANT_ARTICLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum INSTANT_GAMES_SHARE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum INSTANT_GAMES_SHARE_SCORE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum ISSUE_POSITION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum JOB_OFFER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum LARGE_IMAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum LIFE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum LIGHTBOX_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum LIGHTWEIGHT_ACTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum LIGHTWEIGHT_PLACE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum LIVE_VIDEO_SCHEDULE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum LOCAL_CONTEXT_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MAP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MDOTME_USER_LINK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MEDIA_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MEME_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSAGE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSAGE_LIVE_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSAGE_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSAGE_THREAD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSENGER_COMMERCE_COVER_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSENGER_GROUP_JOINABLE_LINK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSENGER_INVITE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSENGER_PLATFORM_BUTTON_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSENGER_PLATFORM_COMPACT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSENGER_PLATFORM_COVER_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSENGER_PLATFORM_ELEMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MESSENGER_TEAM_BOT_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MFS_BILL_PAY_AGENT_CASH_IN_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MFS_BILL_PAY_CREATION_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MFS_BILL_PAY_REFERENCE_CODE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MINUTIAE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MINUTIAE_UNIT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MOMENTS_APP_INVITATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MOMENTS_APP_PHOTO_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MOVIE_BOT_MOVIE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MOVIE_BOT_MOVIE_SHOWTIME_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MULTI_SHARE_FIXED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MULTI_SHARE_NON_LINK_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MULTI_SHARE_NON_LINK_VIDEO_AUTO_SCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MULTI_SHARE_NO_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MULTI_SHARE_SEARCH_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MULTI_VIDEOS_STITCHED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum MUSIC_AGGREGATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum NATIVE_COMPONENT_FLOW_BOOKING_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum NATIVE_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum NATIVE_TEMPLATES:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum NEW_ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum NOTE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum NOTE_COMPOSED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum NOTIFICATION_TARGET:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum OFFER_VIEW_LIVE_COUNTDOWN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum OG_COMPOSER_SIMPLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum OG_MAP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum OMNI_M_FLOW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum P2P_PAYMENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum P2P_TRANSFER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PAGES_PLATFORM_BOOKING_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PAGES_PLATFORM_LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PAGE_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PAGE_VIDEO_PLAYLIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PAYMENT_PLATFORM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PHOTO_LINK_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PLATFORM_INSTANT_APP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum POPULAR_OBJECTS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum POST_CHANNEL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PROFILE_MEDIA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PROMPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum PYMI_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum QUOTE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum QUOTED_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RESTAURANT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_AGENT_ITEM_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_AGENT_ITEM_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_CANCELLATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_NOW_IN_STOCK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_SHIPMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_SHIPMENT_FOR_SUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_SHIPMENT_FOR_UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_SHIPMENT_TRACKING_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_SHIPMENT_TRACKING_EVENT_DELAYED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_SHIPMENT_TRACKING_EVENT_DELIVERED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_SHIPMENT_TRACKING_EVENT_ETA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_SHIPMENT_TRACKING_EVENT_IN_TRANSIT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RETAIL_SHIPMENT_TRACKING_EVENT_OUT_FOR_DELIVERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RICH_MEDIA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RICH_MEDIA_COLLECTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RIDE_ORDERED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RIDE_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum RTC_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SCORE_LEADERBOARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SHARE_LARGE_IMAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SMS_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SOCIAL_SEARCH_CONVERSION_PROMPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SOUVENIR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SPORTS_MATCHUP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SQUARE_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum STICKER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum STREAM_PUBLISH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SUBTOPIC_CUSTOMIZATION_OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SUBTOPIC_CUSTOMIZATION_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SUPER_EMOJI:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum SURVEY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum TAROT_DIGEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum TEEM_COLLECTIONS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum TELEPHONE_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum TEXT_FOR_COLLAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum THIRD_PARTY_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum TOPIC:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum TRANSACTION_INVOICE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum TRAVEL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum TRIAL_AD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VERTICAL_ATTACHMENT_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VIDEO_CINEMAGRAPH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VIDEO_DIRECT_RESPONSE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VIDEO_DIRECT_RESPONSE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VIDEO_INLINE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VIDEO_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VIDEO_SHARE_HIGHLIGHTED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VIDEO_SHARE_YOUTUBE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VIDEO_SHOP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum VOTER_REGISTRATION_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum WELCOME_CARD_IMAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum YEAR_IN_REVIEW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field public static final enum YEAR_OVERVIEW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 257778
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257779
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FALLBACK"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257780
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SHARE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257781
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SHARE_LARGE_IMAGE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE_LARGE_IMAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257782
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257783
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "COVER_PHOTO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257784
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ALBUM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257785
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "NEW_ALBUM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NEW_ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257786
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "COUPON"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COUPON:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257787
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "DEPRECATED_9"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257788
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "DEPRECATED_10"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_10:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257789
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "QUESTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257790
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ANSWER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANSWER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257791
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "OPTION"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257792
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "OG_COMPOSER_SIMPLE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OG_COMPOSER_SIMPLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257793
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SPORTS_MATCHUP"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SPORTS_MATCHUP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257794
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GALLERY"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GALLERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257795
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "STREAM_PUBLISH"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->STREAM_PUBLISH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257796
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MUSIC_AGGREGATION"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MUSIC_AGGREGATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257797
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "LIST"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257798
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "HIGH_SCORE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->HIGH_SCORE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257799
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SCORE_LEADERBOARD"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SCORE_LEADERBOARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257800
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FRIEND_LIST"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257801
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "DEPRECATED_23"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_23:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257802
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "POPULAR_OBJECTS"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->POPULAR_OBJECTS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257803
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AVATAR_LIST"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257804
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AVATAR"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257805
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AVATAR_LARGE_COVER"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_LARGE_COVER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257806
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AVATAR_WITH_VIDEO"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257807
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EVENT"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257808
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EVENTS_PENDING_POST_QUEUE"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENTS_PENDING_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257809
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "CANCELED_EVENT"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CANCELED_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257810
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MINUTIAE_EVENT"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MINUTIAE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257811
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EXPERIENCE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXPERIENCE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257812
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "LIFE_EVENT"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIFE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257813
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GIFT"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GIFT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257814
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "IMAGE_SHARE"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257815
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ANIMATED_IMAGE_AUTOPLAY"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257816
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ANIMATED_IMAGE_SHARE"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257817
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "NOTE"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257818
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "TOPIC"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TOPIC:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257819
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FILE_UPLOAD"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FILE_UPLOAD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257820
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "NOTIFICATION_TARGET"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTIFICATION_TARGET:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257821
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "UNAVAILABLE"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257822
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PAGE_RECOMMENDATION"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGE_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257823
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PAGE_VIDEO_PLAYLIST"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGE_VIDEO_PLAYLIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257824
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "DEPRECATED_46"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_46:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257825
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VIDEO"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257826
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VIDEO_INLINE"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_INLINE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257827
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VIDEO_AUTOPLAY"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257828
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VIDEO_SHARE"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257829
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VIDEO_SHARE_HIGHLIGHTED"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHARE_HIGHLIGHTED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257830
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VIDEO_SHARE_YOUTUBE"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHARE_YOUTUBE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257831
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VIDEO_DIRECT_RESPONSE"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257832
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VIDEO_DIRECT_RESPONSE_AUTOPLAY"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257833
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VIDEO_SHOP"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHOP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257834
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MAP"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MAP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257835
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "OG_MAP"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OG_MAP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257836
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PRODUCT"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257837
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EXTERNAL_PRODUCT"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXTERNAL_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257838
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FITNESS_COURSE"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FITNESS_COURSE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257839
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "APPLICATION"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->APPLICATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257840
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "STICKER"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->STICKER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257841
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EXTERNAL_OG_PRODUCT"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXTERNAL_OG_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257842
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "TRAVEL_LOG"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TRAVEL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257843
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MULTI_SHARE"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257844
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MULTI_SHARE_NO_END_CARD"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NO_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257845
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MULTI_SHARE_NON_LINK_VIDEO"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NON_LINK_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257846
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MULTI_SHARE_SEARCH_END_CARD"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_SEARCH_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257847
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MULTI_SHARE_FIXED_TEXT"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_FIXED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257848
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "YEAR_IN_REVIEW"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->YEAR_IN_REVIEW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257849
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "COMMERCE_PRODUCT_ITEM"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257850
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "COMMERCE_STORE"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_STORE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257851
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "THIRD_PARTY_PHOTO"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->THIRD_PARTY_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257852
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PROMPT"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PROMPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257853
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "BIRTHDAY"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257854
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "DISCUSSION_CONVERSATION"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DISCUSSION_CONVERSATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257855
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "DISCUSSION_COMMENT"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DISCUSSION_COMMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257856
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GROUP_SELL_PRODUCT_ITEM"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257857
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GROUP_SELL_PRODUCT_ITEM_MARK_AS_SOLD"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM_MARK_AS_SOLD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257858
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GAMETIME"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMETIME:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257859
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GAMETIME_PLAY"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMETIME_PLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257860
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GROUP_REPORTED_POST_QUEUE"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_REPORTED_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257861
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GROUP_PENDING_POST_QUEUE"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_PENDING_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257862
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GROUP_JOIN_REQUEST_QUEUE"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_JOIN_REQUEST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257863
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GREETING_CARD"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GREETING_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257864
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "LEAD_GEN"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257865
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ATTACHED_STORY"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ATTACHED_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257866
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SOUVENIR"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SOUVENIR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257867
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "P2P_TRANSFER"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->P2P_TRANSFER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257868
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "P2P_PAYMENT_REQUEST"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->P2P_PAYMENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257869
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RIDE_ORDERED"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RIDE_ORDERED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257870
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RIDE_RECEIPT"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RIDE_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257871
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "BUSINESS_MESSAGE_ITEMS"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BUSINESS_MESSAGE_ITEMS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257872
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_CANCELLATION"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_CANCELLATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257873
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_PROMOTION"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257874
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_ITEM"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257875
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_RECEIPT"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257876
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_NOW_IN_STOCK"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_NOW_IN_STOCK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257877
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_SHIPMENT"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257878
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_SHIPMENT_FOR_SUPPORTED_CARRIER"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_FOR_SUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257879
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_SHIPMENT_FOR_UNSUPPORTED_CARRIER"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_FOR_UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257880
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_SHIPMENT_TRACKING_EVENT"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257881
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_SHIPMENT_TRACKING_EVENT_ETA"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_ETA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257882
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_SHIPMENT_TRACKING_EVENT_IN_TRANSIT"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_IN_TRANSIT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257883
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_SHIPMENT_TRACKING_EVENT_OUT_FOR_DELIVERY"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_OUT_FOR_DELIVERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257884
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_SHIPMENT_TRACKING_EVENT_DELAYED"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_DELAYED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257885
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_SHIPMENT_TRACKING_EVENT_DELIVERED"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_DELIVERED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257886
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "INSTANT_ARTICLE"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257887
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MOMENTS_APP_INVITATION"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOMENTS_APP_INVITATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257888
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MOMENTS_APP_PHOTO_REQUEST"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOMENTS_APP_PHOTO_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257889
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SURVEY"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SURVEY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257890
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSAGE_LOCATION"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257891
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSAGE_THREAD"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_THREAD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257892
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "JOB_OFFER"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->JOB_OFFER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257893
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GROUP_ADD_MEMBERS"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_ADD_MEMBERS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257894
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RTC_CALL_LOG"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RTC_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257895
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EVENT_CALENDAR"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_CALENDAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257896
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SLIDESHOW"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257897
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FACEPILE"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FACEPILE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257898
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ATTRIBUTED_SHARE"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ATTRIBUTED_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257899
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VIDEO_CINEMAGRAPH"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_CINEMAGRAPH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257900
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "LOCAL_CONTEXT_SHARE"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LOCAL_CONTEXT_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257901
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "H_SCROLL"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->H_SCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257902
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EGO_HSCROLL"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EGO_HSCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257903
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MEDIA_QUESTION"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MEDIA_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257904
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RICH_MEDIA"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RICH_MEDIA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257905
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SQUARE_IMAGE_SHARE"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SQUARE_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257906
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_AGENT_ITEM_SUGGESTION"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_AGENT_ITEM_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257907
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RETAIL_AGENT_ITEM_RECEIPT"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_AGENT_ITEM_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257908
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AIRLINE_FLIGHT_RESCHEDULE_UPDATE_BUBBLE"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_FLIGHT_RESCHEDULE_UPDATE_BUBBLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257909
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AIRLINE_CONFIRMATION"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257910
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AIRLINE_CHECK_IN"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_CHECK_IN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257911
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AIRLINE_BOARDING_PASS"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_BOARDING_PASS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257912
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AIRLINE_FLIGHT_RESCHEDULE_UPDATE"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_FLIGHT_RESCHEDULE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257913
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FINANCIAL_BILL_PAYMENT_REQUEST"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FINANCIAL_BILL_PAYMENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257914
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "NOTE_COMPOSED"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTE_COMPOSED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257915
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AVATAR_WITH_BIRTHDAY"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257916
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "LIGHTBOX_VIDEO"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTBOX_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257917
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GLOBALLY_DELETED_MESSAGE_PLACEHOLDER"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257918
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GENIE_MESSAGE"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GENIE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257919
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MOVIE_BOT_MOVIE_LIST"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOVIE_BOT_MOVIE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257920
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MOVIE_BOT_MOVIE_SHOWTIME_LIST"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOVIE_BOT_MOVIE_SHOWTIME_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257921
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AVATAR_WITH_EGO_ACTION"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_EGO_ACTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257922
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EVENT_TICKET"

    const/16 v2, 0x90

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_TICKET:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257923
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VERTICAL_ATTACHMENT_LIST"

    const/16 v2, 0x91

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VERTICAL_ATTACHMENT_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257924
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "INSTANT_ARTICLE_LEGACY"

    const/16 v2, 0x92

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_LEGACY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257925
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ANIMATED_IMAGE_VIDEO"

    const/16 v2, 0x93

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257926
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ANIMATED_IMAGE_VIDEO_AUTOPLAY"

    const/16 v2, 0x94

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257927
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FUNDRAISER_PAGE"

    const/16 v2, 0x95

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_PAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257928
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FUNDRAISER_PERSON_TO_CHARITY"

    const/16 v2, 0x96

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_PERSON_TO_CHARITY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257929
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD"

    const/16 v2, 0x97

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257930
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD_IPB"

    const/16 v2, 0x98

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD_IPB:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257931
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD"

    const/16 v2, 0x99

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257932
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD_IPB"

    const/16 v2, 0x9a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD_IPB:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257933
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GOODWILL_THROWBACK_FRIENDVERSARY_DATA_CARD"

    const/16 v2, 0x9b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_DATA_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257934
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GOODWILL_THROWBACK_VIDEO_BASIC"

    const/16 v2, 0x9c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_VIDEO_BASIC:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257935
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GOODWILL_THROWBACK_FACEVERSARY_COLLAGE_CARD"

    const/16 v2, 0x9d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FACEVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257936
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GOODWILL_THROWBACK_ANNIVERSARY_COLLAGE_CARD"

    const/16 v2, 0x9e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_ANNIVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257937
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "WELCOME_CARD_IMAGE"

    const/16 v2, 0x9f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->WELCOME_CARD_IMAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257938
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "LARGE_IMAGE_LIKE"

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LARGE_IMAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257939
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ASK_FRIENDS"

    const/16 v2, 0xa1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ASK_FRIENDS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257940
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "CULTURAL_MOMENT"

    const/16 v2, 0xa2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CULTURAL_MOMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257941
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "TELEPHONE_CALL_LOG"

    const/16 v2, 0xa3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TELEPHONE_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257942
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SUPER_EMOJI"

    const/16 v2, 0xa4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SUPER_EMOJI:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257943
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "BOOK"

    const/16 v2, 0xa5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BOOK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257944
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SUBTOPIC_CUSTOMIZATION_QUESTION"

    const/16 v2, 0xa6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SUBTOPIC_CUSTOMIZATION_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257945
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SUBTOPIC_CUSTOMIZATION_OPTION"

    const/16 v2, 0xa7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SUBTOPIC_CUSTOMIZATION_OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257946
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RESTAURANT"

    const/16 v2, 0xa8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RESTAURANT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257947
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "CONNECTION_QUESTION"

    const/16 v2, 0xa9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CONNECTION_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257948
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "CONNECTION_QUESTION_OPTION"

    const/16 v2, 0xaa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CONNECTION_QUESTION_OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257949
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PYMI_ITEM"

    const/16 v2, 0xab

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PYMI_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257950
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "CITY"

    const/16 v2, 0xac

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CITY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257951
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AVATAR_WITH_SHARE"

    const/16 v2, 0xad

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257952
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "LIGHTWEIGHT_PLACE"

    const/16 v2, 0xae

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTWEIGHT_PLACE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257953
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "QUOTED_SHARE"

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->QUOTED_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257954
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SMS_LOG"

    const/16 v2, 0xb0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SMS_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257955
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "CENTERED_TEXT"

    const/16 v2, 0xb1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CENTERED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257956
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSAGE_EVENT"

    const/16 v2, 0xb2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257957
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "POST_CHANNEL"

    const/16 v2, 0xb3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->POST_CHANNEL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257958
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "TEXT_FOR_COLLAGE"

    const/16 v2, 0xb4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TEXT_FOR_COLLAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257959
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "QUOTE"

    const/16 v2, 0xb5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->QUOTE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257960
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "VOTER_REGISTRATION_SHARE"

    const/16 v2, 0xb6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VOTER_REGISTRATION_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257961
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AIRLINE_ITINERARY"

    const/16 v2, 0xb7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_ITINERARY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257962
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AIRLINE_UPDATE"

    const/16 v2, 0xb8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257963
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PLACE_LIST"

    const/16 v2, 0xb9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257964
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "AIRLINE_CHECKIN_REMINDER"

    const/16 v2, 0xba

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_CHECKIN_REMINDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257965
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MINUTIAE_UNIT"

    const/16 v2, 0xbb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MINUTIAE_UNIT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257966
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "INSTANT_ARTICLE_PHOTO"

    const/16 v2, 0xbc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257967
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "INSTANT_ARTICLE_VIDEO"

    const/16 v2, 0xbd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257968
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PHOTO_LINK_SHARE"

    const/16 v2, 0xbe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO_LINK_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257969
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "BUSINESS_LOCATION"

    const/16 v2, 0xbf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BUSINESS_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257970
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GOODWILL_WEATHER"

    const/16 v2, 0xc0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_WEATHER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257971
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "TRANSACTION_INVOICE"

    const/16 v2, 0xc1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TRANSACTION_INVOICE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257972
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GROUP"

    const/16 v2, 0xc2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257973
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "COMMENT_PLACE_INFO"

    const/16 v2, 0xc3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMENT_PLACE_INFO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257974
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSAGE_LIVE_LOCATION"

    const/16 v2, 0xc4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_LIVE_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257975
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSENGER_INVITE"

    const/16 v2, 0xc5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_INVITE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257976
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GAMES_INSTANT_PLAY"

    const/16 v2, 0xc6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMES_INSTANT_PLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257977
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PROFILE_MEDIA"

    const/16 v2, 0xc7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PROFILE_MEDIA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257978
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "NATIVE_TEMPLATES"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NATIVE_TEMPLATES:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257979
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "COMMERCE_PRODUCT_MINI"

    const/16 v2, 0xc9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_PRODUCT_MINI:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257980
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "COMMERCE_PRODUCT_MINI_LIST"

    const/16 v2, 0xca

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_PRODUCT_MINI_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257981
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "COMMENT"

    const/16 v2, 0xcb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257982
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSENGER_TEAM_BOT_SHARE"

    const/16 v2, 0xcc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_TEAM_BOT_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257983
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "LIVE_VIDEO_SCHEDULE"

    const/16 v2, 0xcd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIVE_VIDEO_SCHEDULE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257984
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PRODUCT_ITEM"

    const/16 v2, 0xce

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257985
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PLATFORM_INSTANT_APP"

    const/16 v2, 0xcf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLATFORM_INSTANT_APP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257986
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "YEAR_OVERVIEW"

    const/16 v2, 0xd0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->YEAR_OVERVIEW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257987
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "NATIVE_COMPONENT_FLOW_BOOKING_REQUEST"

    const/16 v2, 0xd1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NATIVE_COMPONENT_FLOW_BOOKING_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257988
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FRIEND_REQUEST"

    const/16 v2, 0xd2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FRIEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257989
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSENGER_GROUP_JOINABLE_LINK"

    const/16 v2, 0xd3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_GROUP_JOINABLE_LINK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257990
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "SOCIAL_SEARCH_CONVERSION_PROMPT"

    const/16 v2, 0xd4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SOCIAL_SEARCH_CONVERSION_PROMPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257991
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MDOTME_USER_LINK"

    const/16 v2, 0xd5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MDOTME_USER_LINK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257992
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "DEPRECATED_214"

    const/16 v2, 0xd6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_214:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257993
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "DYNAMIC_MULTI_SHARE_ITEMS"

    const/16 v2, 0xd7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DYNAMIC_MULTI_SHARE_ITEMS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257994
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "RICH_MEDIA_COLLECTION"

    const/16 v2, 0xd8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RICH_MEDIA_COLLECTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257995
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EVENT_REMINDER"

    const/16 v2, 0xd9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_REMINDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257996
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "INSTANT_GAMES_SHARE_MESSAGE"

    const/16 v2, 0xda

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_GAMES_SHARE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257997
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSENGER_PLATFORM_COMPACT_ITEM"

    const/16 v2, 0xdb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_COMPACT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257998
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSENGER_PLATFORM_COVER_ITEM"

    const/16 v2, 0xdc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_COVER_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 257999
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GAMETIME_LEAGUE"

    const/16 v2, 0xdd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMETIME_LEAGUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258000
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "IMPLICIT_PLACE_LIST_CONVERSION"

    const/16 v2, 0xde

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->IMPLICIT_PLACE_LIST_CONVERSION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258001
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MULTI_SHARE_NON_LINK_VIDEO_AUTO_SCROLL"

    const/16 v2, 0xdf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NON_LINK_VIDEO_AUTO_SCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258002
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ENDORSEMENT"

    const/16 v2, 0xe0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ENDORSEMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258003
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "OFFER_VIEW_LIVE_COUNTDOWN"

    const/16 v2, 0xe1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OFFER_VIEW_LIVE_COUNTDOWN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258004
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ENHANCED_LINK_REDDIT_POST"

    const/16 v2, 0xe2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ENHANCED_LINK_REDDIT_POST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258005
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FUNDRAISER_FOR_STORY"

    const/16 v2, 0xe3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_FOR_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258006
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSENGER_COMMERCE_COVER_ITEM"

    const/16 v2, 0xe4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_COMMERCE_COVER_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258007
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ENHANCED_LINK_YELP_BUSINESS"

    const/16 v2, 0xe5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ENHANCED_LINK_YELP_BUSINESS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258008
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PAGES_PLATFORM_LEAD_GEN"

    const/16 v2, 0xe6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGES_PLATFORM_LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258009
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "INSPIRATION_PHOTO"

    const/16 v2, 0xe7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258010
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "INSPIRATION_VIDEO"

    const/16 v2, 0xe8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258011
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "NATIVE_STORY"

    const/16 v2, 0xe9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NATIVE_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258012
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "DEPRECATED_234"

    const/16 v2, 0xea

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_234:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258013
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "ISSUE_POSITION"

    const/16 v2, 0xeb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ISSUE_POSITION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258014
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MFS_BILL_PAY_CREATION_UPDATE"

    const/16 v2, 0xec

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MFS_BILL_PAY_CREATION_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258015
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PAGES_PLATFORM_BOOKING_MESSAGE"

    const/16 v2, 0xed

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGES_PLATFORM_BOOKING_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258016
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EXTERNAL_GALLERY"

    const/16 v2, 0xee

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXTERNAL_GALLERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258017
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MEME_SHARE"

    const/16 v2, 0xef

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MEME_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258018
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MFS_BILL_PAY_REFERENCE_CODE_UPDATE"

    const/16 v2, 0xf0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MFS_BILL_PAY_REFERENCE_CODE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258019
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSENGER_PLATFORM_ELEMENT"

    const/16 v2, 0xf1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_ELEMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258020
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "INSTANT_GAMES_SHARE_SCORE_MESSAGE"

    const/16 v2, 0xf2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_GAMES_SHARE_SCORE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258021
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MESSENGER_PLATFORM_BUTTON_LIST"

    const/16 v2, 0xf3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_BUTTON_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258022
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "BALLOT"

    const/16 v2, 0xf4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BALLOT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258023
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "LIGHTWEIGHT_ACTION"

    const/16 v2, 0xf5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTWEIGHT_ACTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258024
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "FUNDRAISER_PERSON_FOR_PERSON"

    const/16 v2, 0xf6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_PERSON_FOR_PERSON:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258025
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MFS_BILL_PAY_AGENT_CASH_IN_UPDATE"

    const/16 v2, 0xf7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MFS_BILL_PAY_AGENT_CASH_IN_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258026
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "TAROT_DIGEST"

    const/16 v2, 0xf8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TAROT_DIGEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258027
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GROUP_MEMBER_ADDED"

    const/16 v2, 0xf9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_MEMBER_ADDED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258028
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "CULTURAL_MOMENT_HOLIDAY_CARD"

    const/16 v2, 0xfa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CULTURAL_MOMENT_HOLIDAY_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258029
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "EVENT_TOUR"

    const/16 v2, 0xfb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_TOUR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258030
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "PAYMENT_PLATFORM"

    const/16 v2, 0xfc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAYMENT_PLATFORM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258031
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "GROUP_QUIZ"

    const/16 v2, 0xfd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_QUIZ:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258032
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "OMNI_M_FLOW"

    const/16 v2, 0xfe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OMNI_M_FLOW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258033
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "TRIAL_AD"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TRIAL_AD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258034
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "DYNAMIC_GAME_BOARD"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DYNAMIC_GAME_BOARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258035
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "MULTI_VIDEOS_STITCHED"

    const/16 v2, 0x101

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_VIDEOS_STITCHED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258036
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    const-string v1, "TEEM_COLLECTIONS"

    const/16 v2, 0x102

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TEEM_COLLECTIONS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258037
    const/16 v0, 0x103

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE_LARGE_IMAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NEW_ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COUPON:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_10:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANSWER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OG_COMPOSER_SIMPLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SPORTS_MATCHUP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GALLERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->STREAM_PUBLISH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MUSIC_AGGREGATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->HIGH_SCORE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SCORE_LEADERBOARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_23:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->POPULAR_OBJECTS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_LARGE_COVER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENTS_PENDING_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CANCELED_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MINUTIAE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXPERIENCE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIFE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GIFT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TOPIC:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FILE_UPLOAD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTIFICATION_TARGET:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGE_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGE_VIDEO_PLAYLIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_46:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_INLINE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHARE_HIGHLIGHTED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHARE_YOUTUBE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHOP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MAP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OG_MAP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXTERNAL_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FITNESS_COURSE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->APPLICATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->STICKER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXTERNAL_OG_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TRAVEL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NO_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NON_LINK_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_SEARCH_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_FIXED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->YEAR_IN_REVIEW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_STORE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->THIRD_PARTY_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PROMPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DISCUSSION_CONVERSATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DISCUSSION_COMMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM_MARK_AS_SOLD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMETIME:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMETIME_PLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_REPORTED_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_PENDING_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_JOIN_REQUEST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GREETING_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ATTACHED_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SOUVENIR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->P2P_TRANSFER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->P2P_PAYMENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RIDE_ORDERED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RIDE_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BUSINESS_MESSAGE_ITEMS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_CANCELLATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_NOW_IN_STOCK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_FOR_SUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_FOR_UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_ETA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_IN_TRANSIT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_OUT_FOR_DELIVERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_DELAYED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_DELIVERED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOMENTS_APP_INVITATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOMENTS_APP_PHOTO_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SURVEY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_THREAD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->JOB_OFFER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_ADD_MEMBERS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RTC_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_CALENDAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FACEPILE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ATTRIBUTED_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_CINEMAGRAPH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LOCAL_CONTEXT_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->H_SCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EGO_HSCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MEDIA_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RICH_MEDIA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SQUARE_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_AGENT_ITEM_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_AGENT_ITEM_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_FLIGHT_RESCHEDULE_UPDATE_BUBBLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_CHECK_IN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_BOARDING_PASS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_FLIGHT_RESCHEDULE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FINANCIAL_BILL_PAYMENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTE_COMPOSED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTBOX_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GENIE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOVIE_BOT_MOVIE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOVIE_BOT_MOVIE_SHOWTIME_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_EGO_ACTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_TICKET:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VERTICAL_ATTACHMENT_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_LEGACY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_PAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_PERSON_TO_CHARITY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD_IPB:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD_IPB:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_DATA_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_VIDEO_BASIC:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FACEVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_ANNIVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->WELCOME_CARD_IMAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LARGE_IMAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ASK_FRIENDS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CULTURAL_MOMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TELEPHONE_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SUPER_EMOJI:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BOOK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SUBTOPIC_CUSTOMIZATION_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SUBTOPIC_CUSTOMIZATION_OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RESTAURANT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CONNECTION_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CONNECTION_QUESTION_OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xab

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PYMI_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xac

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CITY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xad

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xae

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTWEIGHT_PLACE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->QUOTED_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SMS_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CENTERED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->POST_CHANNEL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TEXT_FOR_COLLAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->QUOTE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VOTER_REGISTRATION_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_ITINERARY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xba

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_CHECKIN_REMINDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MINUTIAE_UNIT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO_LINK_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BUSINESS_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_WEATHER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TRANSACTION_INVOICE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMENT_PLACE_INFO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_LIVE_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_INVITE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMES_INSTANT_PLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PROFILE_MEDIA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NATIVE_TEMPLATES:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_PRODUCT_MINI:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xca

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_PRODUCT_MINI_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_TEAM_BOT_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIVE_VIDEO_SCHEDULE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xce

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLATFORM_INSTANT_APP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->YEAR_OVERVIEW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NATIVE_COMPONENT_FLOW_BOOKING_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FRIEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_GROUP_JOINABLE_LINK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SOCIAL_SEARCH_CONVERSION_PROMPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MDOTME_USER_LINK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_214:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DYNAMIC_MULTI_SHARE_ITEMS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RICH_MEDIA_COLLECTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_REMINDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xda

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_GAMES_SHARE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_COMPACT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_COVER_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMETIME_LEAGUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xde

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->IMPLICIT_PLACE_LIST_CONVERSION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NON_LINK_VIDEO_AUTO_SCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ENDORSEMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OFFER_VIEW_LIVE_COUNTDOWN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ENHANCED_LINK_REDDIT_POST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_FOR_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_COMMERCE_COVER_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ENHANCED_LINK_YELP_BUSINESS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGES_PLATFORM_LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NATIVE_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xea

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DEPRECATED_234:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ISSUE_POSITION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xec

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MFS_BILL_PAY_CREATION_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xed

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGES_PLATFORM_BOOKING_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xee

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXTERNAL_GALLERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xef

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MEME_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MFS_BILL_PAY_REFERENCE_CODE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_ELEMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_GAMES_SHARE_SCORE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_BUTTON_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BALLOT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTWEIGHT_ACTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_PERSON_FOR_PERSON:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MFS_BILL_PAY_AGENT_CASH_IN_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TAROT_DIGEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_MEMBER_ADDED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CULTURAL_MOMENT_HOLIDAY_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_TOUR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAYMENT_PLATFORM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_QUIZ:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OMNI_M_FLOW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xff

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TRIAL_AD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x100

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DYNAMIC_GAME_BOARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x101

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_VIDEOS_STITCHED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x102

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TEEM_COLLECTIONS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 258655
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;
    .locals 2

    .prologue
    .line 258040
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258041
    :goto_0
    return-object v0

    .line 258042
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x7f

    .line 258043
    packed-switch v0, :pswitch_data_0

    .line 258044
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0

    .line 258045
    :pswitch_1
    const-string v0, "SQUARE_IMAGE_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 258046
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SQUARE_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0

    .line 258047
    :cond_2
    const-string v0, "MESSENGER_TEAM_BOT_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 258048
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_TEAM_BOT_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0

    .line 258049
    :cond_3
    const-string v0, "FRIEND_REQUEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 258050
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FRIEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0

    .line 258051
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0

    .line 258052
    :pswitch_2
    const-string v0, "AVATAR_LARGE_COVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 258053
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_LARGE_COVER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0

    .line 258054
    :cond_5
    const-string v0, "EVENT_CALENDAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 258055
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_CALENDAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0

    .line 258056
    :cond_6
    const-string v0, "EVENT_REMINDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 258057
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_REMINDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0

    .line 258058
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0

    .line 258059
    :pswitch_3
    const-string v0, "LIFE_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 258060
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIFE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_0

    .line 258061
    :cond_8
    const-string v0, "PROMPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 258062
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PROMPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258063
    :cond_9
    const-string v0, "DISCUSSION_COMMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 258064
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DISCUSSION_COMMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258065
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258066
    :pswitch_4
    const-string v0, "PRODUCT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 258067
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258068
    :cond_b
    const-string v0, "AIRLINE_FLIGHT_RESCHEDULE_UPDATE_BUBBLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 258069
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_FLIGHT_RESCHEDULE_UPDATE_BUBBLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258070
    :cond_c
    const-string v0, "AIRLINE_CHECK_IN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 258071
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_CHECK_IN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258072
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258073
    :pswitch_5
    const-string v0, "H_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 258074
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->H_SCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258075
    :cond_e
    const-string v0, "WELCOME_CARD_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 258076
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->WELCOME_CARD_IMAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258077
    :cond_f
    const-string v0, "MFS_BILL_PAY_CREATION_UPDATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 258078
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MFS_BILL_PAY_CREATION_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258079
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258080
    :pswitch_6
    const-string v0, "GOODWILL_WEATHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 258081
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_WEATHER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258082
    :cond_11
    const-string v0, "INSTANT_GAMES_SHARE_SCORE_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 258083
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_GAMES_SHARE_SCORE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258084
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258085
    :pswitch_7
    const-string v0, "PLACE_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 258086
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258087
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258088
    :pswitch_8
    const-string v0, "OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 258089
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258090
    :cond_14
    const-string v0, "AIRLINE_CONFIRMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 258091
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258092
    :cond_15
    const-string v0, "AIRLINE_CHECKIN_REMINDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 258093
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_CHECKIN_REMINDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258094
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258095
    :pswitch_9
    const-string v0, "STICKER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 258096
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->STICKER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258097
    :cond_17
    const-string v0, "RESTAURANT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 258098
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RESTAURANT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258099
    :cond_18
    const-string v0, "CONNECTION_QUESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 258100
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CONNECTION_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258101
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258102
    :pswitch_a
    const-string v0, "SOUVENIR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 258103
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SOUVENIR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258104
    :cond_1a
    const-string v0, "AVATAR_WITH_EGO_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 258105
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_EGO_ACTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258106
    :cond_1b
    const-string v0, "ISSUE_POSITION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 258107
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ISSUE_POSITION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258108
    :cond_1c
    const-string v0, "MFS_BILL_PAY_AGENT_CASH_IN_UPDATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 258109
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MFS_BILL_PAY_AGENT_CASH_IN_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258110
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258111
    :pswitch_b
    const-string v0, "RIDE_RECEIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 258112
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RIDE_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258113
    :cond_1e
    const-string v0, "MFS_BILL_PAY_REFERENCE_CODE_UPDATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 258114
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MFS_BILL_PAY_REFERENCE_CODE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258115
    :cond_1f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258116
    :pswitch_c
    const-string v0, "QUESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 258117
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258118
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258119
    :pswitch_d
    const-string v0, "RETAIL_RECEIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 258120
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258121
    :cond_21
    const-string v0, "TAROT_DIGEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 258122
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TAROT_DIGEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258123
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258124
    :pswitch_e
    const-string v0, "FILE_UPLOAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 258125
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FILE_UPLOAD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258126
    :cond_23
    const-string v0, "NOTIFICATION_TARGET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 258127
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTIFICATION_TARGET:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258128
    :cond_24
    const-string v0, "RETAIL_SHIPMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 258129
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258130
    :cond_25
    const-string v0, "MEDIA_QUESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 258131
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MEDIA_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258132
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258133
    :pswitch_f
    const-string v0, "PAGE_VIDEO_PLAYLIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 258134
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGE_VIDEO_PLAYLIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258135
    :cond_27
    const-string v0, "P2P_PAYMENT_REQUEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 258136
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->P2P_PAYMENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258137
    :cond_28
    const-string v0, "MESSAGE_LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 258138
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258139
    :cond_29
    const-string v0, "CONNECTION_QUESTION_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 258140
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CONNECTION_QUESTION_OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258141
    :cond_2a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258142
    :pswitch_10
    const-string v0, "MUSIC_AGGREGATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 258143
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MUSIC_AGGREGATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258144
    :cond_2b
    const-string v0, "VIDEO_SHOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 258145
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHOP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258146
    :cond_2c
    const-string v0, "FINANCIAL_BILL_PAYMENT_REQUEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 258147
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FINANCIAL_BILL_PAYMENT_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258148
    :cond_2d
    const-string v0, "POST_CHANNEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 258149
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->POST_CHANNEL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258150
    :cond_2e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258151
    :pswitch_11
    const-string v0, "BIRTHDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 258152
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258153
    :cond_2f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258154
    :pswitch_12
    const-string v0, "DYNAMIC_GAME_BOARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 258155
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DYNAMIC_GAME_BOARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258156
    :cond_30
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258157
    :pswitch_13
    const-string v0, "MESSAGE_LIVE_LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 258158
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_LIVE_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258159
    :cond_31
    const-string v0, "PLATFORM_INSTANT_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 258160
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLATFORM_INSTANT_APP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258161
    :cond_32
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258162
    :pswitch_14
    const-string v0, "MOMENTS_APP_INVITATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 258163
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOMENTS_APP_INVITATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258164
    :cond_33
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258165
    :pswitch_15
    const-string v0, "RETAIL_AGENT_ITEM_RECEIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 258166
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_AGENT_ITEM_RECEIPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258167
    :cond_34
    const-string v0, "NOTE_COMPOSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 258168
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTE_COMPOSED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258169
    :cond_35
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258170
    :pswitch_16
    const-string v0, "TRIAL_AD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 258171
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TRIAL_AD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258172
    :cond_36
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258173
    :pswitch_17
    const-string v0, "GLOBALLY_DELETED_MESSAGE_PLACEHOLDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 258174
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258175
    :cond_37
    const-string v0, "ASK_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 258176
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ASK_FRIENDS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258177
    :cond_38
    const-string v0, "IMPLICIT_PLACE_LIST_CONVERSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 258178
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->IMPLICIT_PLACE_LIST_CONVERSION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258179
    :cond_39
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258180
    :pswitch_18
    const-string v0, "RIDE_ORDERED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 258181
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RIDE_ORDERED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258182
    :cond_3a
    const-string v0, "VERTICAL_ATTACHMENT_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 258183
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VERTICAL_ATTACHMENT_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258184
    :cond_3b
    const-string v0, "OFFER_VIEW_LIVE_COUNTDOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 258185
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OFFER_VIEW_LIVE_COUNTDOWN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258186
    :cond_3c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258187
    :pswitch_19
    const-string v0, "RETAIL_SHIPMENT_TRACKING_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 258188
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258189
    :cond_3d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258190
    :pswitch_1a
    const-string v0, "COVER_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 258191
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258192
    :cond_3e
    const-string v0, "VIDEO_CINEMAGRAPH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 258193
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_CINEMAGRAPH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258194
    :cond_3f
    const-string v0, "NATIVE_COMPONENT_FLOW_BOOKING_REQUEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 258195
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NATIVE_COMPONENT_FLOW_BOOKING_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258196
    :cond_40
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258197
    :pswitch_1b
    const-string v0, "FUNDRAISER_FOR_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 258198
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_FOR_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258199
    :cond_41
    const-string v0, "NATIVE_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 258200
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NATIVE_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258201
    :cond_42
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258202
    :pswitch_1c
    const-string v0, "SUBTOPIC_CUSTOMIZATION_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 258203
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SUBTOPIC_CUSTOMIZATION_OPTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258204
    :cond_43
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258205
    :pswitch_1d
    const-string v0, "AVATAR_WITH_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 258206
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258207
    :cond_44
    const-string v0, "AIRLINE_BOARDING_PASS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 258208
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_BOARDING_PASS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258209
    :cond_45
    const-string v0, "OMNI_M_FLOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 258210
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OMNI_M_FLOW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258211
    :cond_46
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258212
    :pswitch_1e
    const-string v0, "SUBTOPIC_CUSTOMIZATION_QUESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 258213
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SUBTOPIC_CUSTOMIZATION_QUESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258214
    :cond_47
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258215
    :pswitch_1f
    const-string v0, "GROUP_ADD_MEMBERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 258216
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_ADD_MEMBERS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258217
    :cond_48
    const-string v0, "SLIDESHOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 258218
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258219
    :cond_49
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258220
    :pswitch_20
    const-string v0, "ANIMATED_IMAGE_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 258221
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258222
    :cond_4a
    const-string v0, "COMMENT_PLACE_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 258223
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMENT_PLACE_INFO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258224
    :cond_4b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258225
    :pswitch_21
    const-string v0, "RETAIL_SHIPMENT_TRACKING_EVENT_IN_TRANSIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 258226
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_IN_TRANSIT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258227
    :cond_4c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258228
    :pswitch_22
    const-string v0, "VIDEO_SHARE_HIGHLIGHTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 258229
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHARE_HIGHLIGHTED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258230
    :cond_4d
    const-string v0, "FACEPILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 258231
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FACEPILE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258232
    :cond_4e
    const-string v0, "FUNDRAISER_PERSON_TO_CHARITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 258233
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_PERSON_TO_CHARITY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258234
    :cond_4f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258235
    :pswitch_23
    const-string v0, "NEW_ALBUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 258236
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NEW_ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258237
    :cond_50
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258238
    :pswitch_24
    const-string v0, "INSPIRATION_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 258239
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258240
    :cond_51
    const-string v0, "INSPIRATION_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 258241
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258242
    :cond_52
    const-string v0, "VIDEO_AUTOPLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 258243
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258244
    :cond_53
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258245
    :pswitch_25
    const-string v0, "PYMI_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 258246
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PYMI_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258247
    :cond_54
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258248
    :pswitch_26
    const-string v0, "HIGH_SCORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 258249
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->HIGH_SCORE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258250
    :cond_55
    const-string v0, "NOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 258251
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258252
    :cond_56
    const-string v0, "ENHANCED_LINK_YELP_BUSINESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 258253
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ENHANCED_LINK_YELP_BUSINESS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258254
    :cond_57
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258255
    :pswitch_27
    const-string v0, "FITNESS_COURSE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 258256
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FITNESS_COURSE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258257
    :cond_58
    const-string v0, "INSTANT_ARTICLE_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 258258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258259
    :cond_59
    const-string v0, "INSTANT_ARTICLE_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 258260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258261
    :cond_5a
    const-string v0, "PRODUCT_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 258262
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258263
    :cond_5b
    const-string v0, "YEAR_OVERVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 258264
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->YEAR_OVERVIEW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258265
    :cond_5c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258266
    :pswitch_28
    const-string v0, "YEAR_IN_REVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 258267
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->YEAR_IN_REVIEW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258268
    :cond_5d
    const-string v0, "RETAIL_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 258269
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258270
    :cond_5e
    const-string v0, "FUNDRAISER_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 258271
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_PAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258272
    :cond_5f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258273
    :pswitch_29
    const-string v0, "MDOTME_USER_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 258274
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MDOTME_USER_LINK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258275
    :cond_60
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258276
    :pswitch_2a
    const-string v0, "SMS_LOG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 258277
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SMS_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258278
    :cond_61
    const-string v0, "PAYMENT_PLATFORM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 258279
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAYMENT_PLATFORM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258280
    :cond_62
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258281
    :pswitch_2b
    const-string v0, "RETAIL_SHIPMENT_TRACKING_EVENT_DELAYED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 258282
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_DELAYED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258283
    :cond_63
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258284
    :pswitch_2c
    const-string v0, "EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_64

    .line 258285
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258286
    :cond_64
    const-string v0, "RETAIL_SHIPMENT_TRACKING_EVENT_DELIVERED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 258287
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_DELIVERED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258288
    :cond_65
    const-string v0, "GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD_IPB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 258289
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD_IPB:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258290
    :cond_66
    const-string v0, "COMMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_67

    .line 258291
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258292
    :cond_67
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258293
    :pswitch_2d
    const-string v0, "GIFT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 258294
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GIFT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258295
    :cond_68
    const-string v0, "LARGE_IMAGE_LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 258296
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LARGE_IMAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258297
    :cond_69
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258298
    :pswitch_2e
    const-string v0, "AVATAR_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 258299
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258300
    :cond_6a
    const-string v0, "MULTI_SHARE_NON_LINK_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 258301
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NON_LINK_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258302
    :cond_6b
    const-string v0, "GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD_IPB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 258303
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD_IPB:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258304
    :cond_6c
    const-string v0, "LIGHTWEIGHT_PLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 258305
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTWEIGHT_PLACE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258306
    :cond_6d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258307
    :pswitch_2f
    const-string v0, "LOCAL_CONTEXT_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 258308
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LOCAL_CONTEXT_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258309
    :cond_6e
    const-string v0, "LIVE_VIDEO_SCHEDULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 258310
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIVE_VIDEO_SCHEDULE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258311
    :cond_6f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258312
    :pswitch_30
    const-string v0, "VIDEO_DIRECT_RESPONSE_AUTOPLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 258313
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258314
    :cond_70
    const-string v0, "RICH_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_71

    .line 258315
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RICH_MEDIA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258316
    :cond_71
    const-string v0, "PHOTO_LINK_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_72

    .line 258317
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO_LINK_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258318
    :cond_72
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258319
    :pswitch_31
    const-string v0, "ENDORSEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 258320
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ENDORSEMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258321
    :cond_73
    const-string v0, "VIDEO_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_74

    .line 258322
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258323
    :cond_74
    const-string v0, "CENTERED_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_75

    .line 258324
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CENTERED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258325
    :cond_75
    const-string v0, "GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_76

    .line 258326
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258327
    :cond_76
    const-string v0, "PROFILE_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_77

    .line 258328
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PROFILE_MEDIA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258329
    :cond_77
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258330
    :pswitch_32
    const-string v0, "CANCELED_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_78

    .line 258331
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CANCELED_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258332
    :cond_78
    const-string v0, "VIDEO_INLINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_79

    .line 258333
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_INLINE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258334
    :cond_79
    const-string v0, "EVENT_TICKET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7a

    .line 258335
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_TICKET:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258336
    :cond_7a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258337
    :pswitch_33
    const-string v0, "CULTURAL_MOMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 258338
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CULTURAL_MOMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258339
    :cond_7b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258340
    :pswitch_34
    const-string v0, "TEXT_FOR_COLLAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7c

    .line 258341
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TEXT_FOR_COLLAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258342
    :cond_7c
    const-string v0, "MESSENGER_GROUP_JOINABLE_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 258343
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_GROUP_JOINABLE_LINK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258344
    :cond_7d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258345
    :pswitch_35
    const-string v0, "MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 258346
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MAP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258347
    :cond_7e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258348
    :pswitch_36
    const-string v0, "EXTERNAL_PRODUCT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 258349
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXTERNAL_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258350
    :cond_7f
    const-string v0, "JOB_OFFER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 258351
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->JOB_OFFER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258352
    :cond_80
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258353
    :pswitch_37
    const-string v0, "GOODWILL_THROWBACK_VIDEO_BASIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 258354
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_VIDEO_BASIC:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258355
    :cond_81
    const-string v0, "TRANSACTION_INVOICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_82

    .line 258356
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TRANSACTION_INVOICE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258357
    :cond_82
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258358
    :pswitch_38
    const-string v0, "EXTERNAL_OG_PRODUCT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_83

    .line 258359
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXTERNAL_OG_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258360
    :cond_83
    const-string v0, "VIDEO_SHARE_YOUTUBE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_84

    .line 258361
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_SHARE_YOUTUBE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258362
    :cond_84
    const-string v0, "EGO_HSCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_85

    .line 258363
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EGO_HSCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258364
    :cond_85
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258365
    :pswitch_39
    const-string v0, "OG_MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_86

    .line 258366
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OG_MAP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258367
    :cond_86
    const-string v0, "BUSINESS_LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_87

    .line 258368
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BUSINESS_LOCATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258369
    :cond_87
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258370
    :pswitch_3a
    const-string v0, "VIDEO_DIRECT_RESPONSE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_88

    .line 258371
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258372
    :cond_88
    const-string v0, "LEAD_GEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_89

    .line 258373
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258374
    :cond_89
    const-string v0, "MESSAGE_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8a

    .line 258375
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258376
    :cond_8a
    const-string v0, "MINUTIAE_UNIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8b

    .line 258377
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MINUTIAE_UNIT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258378
    :cond_8b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258379
    :pswitch_3b
    const-string v0, "MINUTIAE_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 258380
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MINUTIAE_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258381
    :cond_8c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258382
    :pswitch_3c
    const-string v0, "RETAIL_SHIPMENT_TRACKING_EVENT_OUT_FOR_DELIVERY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8d

    .line 258383
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_OUT_FOR_DELIVERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258384
    :cond_8d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258385
    :pswitch_3d
    const-string v0, "VOTER_REGISTRATION_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8e

    .line 258386
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VOTER_REGISTRATION_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258387
    :cond_8e
    const-string v0, "COMMERCE_PRODUCT_MINI_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 258388
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_PRODUCT_MINI_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258389
    :cond_8f
    const-string v0, "PAGES_PLATFORM_BOOKING_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_90

    .line 258390
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGES_PLATFORM_BOOKING_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258391
    :cond_90
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258392
    :pswitch_3e
    const-string v0, "P2P_TRANSFER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_91

    .line 258393
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->P2P_TRANSFER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258394
    :cond_91
    const-string v0, "ENHANCED_LINK_REDDIT_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_92

    .line 258395
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ENHANCED_LINK_REDDIT_POST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258396
    :cond_92
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258397
    :pswitch_3f
    const-string v0, "DISCUSSION_CONVERSATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_93

    .line 258398
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DISCUSSION_CONVERSATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258399
    :cond_93
    const-string v0, "MOVIE_BOT_MOVIE_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_94

    .line 258400
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOVIE_BOT_MOVIE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258401
    :cond_94
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258402
    :pswitch_40
    const-string v0, "CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_95

    .line 258403
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CITY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258404
    :cond_95
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258405
    :pswitch_41
    const-string v0, "MULTI_SHARE_FIXED_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_96

    .line 258406
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_FIXED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258407
    :cond_96
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258408
    :pswitch_42
    const-string v0, "GREETING_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_97

    .line 258409
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GREETING_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258410
    :cond_97
    const-string v0, "LIGHTWEIGHT_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_98

    .line 258411
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTWEIGHT_ACTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258412
    :cond_98
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258413
    :pswitch_43
    const-string v0, "SPORTS_MATCHUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_99

    .line 258414
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SPORTS_MATCHUP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258415
    :cond_99
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258416
    :pswitch_44
    const-string v0, "MOMENTS_APP_PHOTO_REQUEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9a

    .line 258417
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOMENTS_APP_PHOTO_REQUEST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258418
    :cond_9a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258419
    :pswitch_45
    const-string v0, "RETAIL_SHIPMENT_TRACKING_EVENT_ETA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 258420
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_TRACKING_EVENT_ETA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258421
    :cond_9b
    const-string v0, "MESSENGER_PLATFORM_ELEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 258422
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_ELEMENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258423
    :cond_9c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258424
    :pswitch_46
    const-string v0, "RETAIL_PROMOTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9d

    .line 258425
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258426
    :cond_9d
    const-string v0, "FUNDRAISER_PERSON_FOR_PERSON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9e

    .line 258427
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_PERSON_FOR_PERSON:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258428
    :cond_9e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258429
    :pswitch_47
    const-string v0, "GALLERY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 258430
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GALLERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258431
    :cond_9f
    const-string v0, "PAGE_RECOMMENDATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 258432
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGE_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258433
    :cond_a0
    const-string v0, "GROUP_MEMBER_ADDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a1

    .line 258434
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_MEMBER_ADDED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258435
    :cond_a1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258436
    :pswitch_48
    const-string v0, "ATTACHED_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a2

    .line 258437
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ATTACHED_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258438
    :cond_a2
    const-string v0, "MOVIE_BOT_MOVIE_SHOWTIME_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a3

    .line 258439
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MOVIE_BOT_MOVIE_SHOWTIME_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258440
    :cond_a3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258441
    :pswitch_49
    const-string v0, "RETAIL_CANCELLATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 258442
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_CANCELLATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258443
    :cond_a4
    const-string v0, "MESSAGE_THREAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 258444
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSAGE_THREAD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258445
    :cond_a5
    const-string v0, "MESSENGER_PLATFORM_BUTTON_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 258446
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_BUTTON_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258447
    :cond_a6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258448
    :pswitch_4a
    const-string v0, "ALBUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a7

    .line 258449
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258450
    :cond_a7
    const-string v0, "STREAM_PUBLISH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a8

    .line 258451
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->STREAM_PUBLISH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258452
    :cond_a8
    const-string v0, "PAGES_PLATFORM_LEAD_GEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a9

    .line 258453
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PAGES_PLATFORM_LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258454
    :cond_a9
    const-string v0, "RICH_MEDIA_COLLECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_aa

    .line 258455
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RICH_MEDIA_COLLECTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258456
    :cond_aa
    const-string v0, "AIRLINE_ITINERARY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ab

    .line 258457
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_ITINERARY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258458
    :cond_ab
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258459
    :pswitch_4b
    const-string v0, "GAMETIME_PLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ac

    .line 258460
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMETIME_PLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258461
    :cond_ac
    const-string v0, "BOOK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ad

    .line 258462
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BOOK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258463
    :cond_ad
    const-string v0, "CULTURAL_MOMENT_HOLIDAY_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ae

    .line 258464
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CULTURAL_MOMENT_HOLIDAY_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258465
    :cond_ae
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258466
    :pswitch_4c
    const-string v0, "AVATAR_WITH_BIRTHDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_af

    .line 258467
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258468
    :cond_af
    const-string v0, "EXTERNAL_GALLERY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b0

    .line 258469
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXTERNAL_GALLERY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258470
    :cond_b0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258471
    :pswitch_4d
    const-string v0, "SOCIAL_SEARCH_CONVERSION_PROMPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b1

    .line 258472
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SOCIAL_SEARCH_CONVERSION_PROMPT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258473
    :cond_b1
    const-string v0, "MULTI_VIDEOS_STITCHED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b2

    .line 258474
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_VIDEOS_STITCHED:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258475
    :cond_b2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258476
    :pswitch_4e
    const-string v0, "ANIMATED_IMAGE_AUTOPLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 258477
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258478
    :cond_b3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258479
    :pswitch_4f
    const-string v0, "MULTI_SHARE_NO_END_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 258480
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NO_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258481
    :cond_b4
    const-string v0, "SCORE_LEADERBOARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b5

    .line 258482
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SCORE_LEADERBOARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258483
    :cond_b5
    const-string v0, "SURVEY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b6

    .line 258484
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SURVEY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258485
    :cond_b6
    const-string v0, "RETAIL_AGENT_ITEM_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b7

    .line 258486
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_AGENT_ITEM_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258487
    :cond_b7
    const-string v0, "GAMES_INSTANT_PLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b8

    .line 258488
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMES_INSTANT_PLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258489
    :cond_b8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258490
    :pswitch_50
    const-string v0, "FALLBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b9

    .line 258491
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258492
    :cond_b9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258493
    :pswitch_51
    const-string v0, "MULTI_SHARE_SEARCH_END_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ba

    .line 258494
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_SEARCH_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258495
    :cond_ba
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258496
    :pswitch_52
    const-string v0, "BUSINESS_MESSAGE_ITEMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bb

    .line 258497
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BUSINESS_MESSAGE_ITEMS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258498
    :cond_bb
    const-string v0, "RETAIL_SHIPMENT_FOR_SUPPORTED_CARRIER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bc

    .line 258499
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_FOR_SUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258500
    :cond_bc
    const-string v0, "ANIMATED_IMAGE_VIDEO_AUTOPLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bd

    .line 258501
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258502
    :cond_bd
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258503
    :pswitch_53
    const-string v0, "PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_be

    .line 258504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258505
    :cond_be
    const-string v0, "INSTANT_ARTICLE_LEGACY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bf

    .line 258506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_LEGACY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258507
    :cond_bf
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258508
    :pswitch_54
    const-string v0, "MULTI_SHARE_NON_LINK_VIDEO_AUTO_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c0

    .line 258509
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NON_LINK_VIDEO_AUTO_SCROLL:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258510
    :cond_c0
    const-string v0, "GROUP_SELL_PRODUCT_ITEM_MARK_AS_SOLD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c1

    .line 258511
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM_MARK_AS_SOLD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258512
    :cond_c1
    const-string v0, "RETAIL_SHIPMENT_FOR_UNSUPPORTED_CARRIER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c2

    .line 258513
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_SHIPMENT_FOR_UNSUPPORTED_CARRIER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258514
    :cond_c2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258515
    :pswitch_55
    const-string v0, "EXPERIENCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c3

    .line 258516
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EXPERIENCE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258517
    :cond_c3
    const-string v0, "GAMETIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 258518
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMETIME:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258519
    :cond_c4
    const-string v0, "AIRLINE_UPDATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c5

    .line 258520
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258521
    :cond_c5
    const-string v0, "DYNAMIC_MULTI_SHARE_ITEMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c6

    .line 258522
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->DYNAMIC_MULTI_SHARE_ITEMS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258523
    :cond_c6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258524
    :pswitch_56
    const-string v0, "COMMERCE_PRODUCT_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c7

    .line 258525
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258526
    :cond_c7
    const-string v0, "LIGHTBOX_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c8

    .line 258527
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTBOX_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258528
    :cond_c8
    const-string v0, "NATIVE_TEMPLATES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c9

    .line 258529
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NATIVE_TEMPLATES:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258530
    :cond_c9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258531
    :pswitch_57
    const-string v0, "COMMERCE_STORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ca

    .line 258532
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_STORE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258533
    :cond_ca
    const-string v0, "POPULAR_OBJECTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cb

    .line 258534
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->POPULAR_OBJECTS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258535
    :cond_cb
    const-string v0, "VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cc

    .line 258536
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258537
    :cond_cc
    const-string v0, "ATTRIBUTED_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cd

    .line 258538
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ATTRIBUTED_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258539
    :cond_cd
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258540
    :pswitch_58
    const-string v0, "GOODWILL_THROWBACK_FRIENDVERSARY_DATA_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ce

    .line 258541
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_DATA_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258542
    :cond_ce
    const-string v0, "AVATAR_WITH_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cf

    .line 258543
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258544
    :cond_cf
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258545
    :pswitch_59
    const-string v0, "GOODWILL_THROWBACK_FACEVERSARY_COLLAGE_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d0

    .line 258546
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FACEVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258547
    :cond_d0
    const-string v0, "GOODWILL_THROWBACK_ANNIVERSARY_COLLAGE_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d1

    .line 258548
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_ANNIVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258549
    :cond_d1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258550
    :pswitch_5a
    const-string v0, "IMAGE_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d2

    .line 258551
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258552
    :cond_d2
    const-string v0, "GENIE_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d3

    .line 258553
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GENIE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258554
    :cond_d3
    const-string v0, "COMMERCE_PRODUCT_MINI"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d4

    .line 258555
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_PRODUCT_MINI:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258556
    :cond_d4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258557
    :pswitch_5b
    const-string v0, "ANIMATED_IMAGE_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d5

    .line 258558
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258559
    :cond_d5
    const-string v0, "GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d6

    .line 258560
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258561
    :cond_d6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258562
    :pswitch_5c
    const-string v0, "GAMETIME_LEAGUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d7

    .line 258563
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMETIME_LEAGUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258564
    :cond_d7
    const-string v0, "GROUP_SELL_PRODUCT_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d8

    .line 258565
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258566
    :cond_d8
    const-string v0, "QUOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d9

    .line 258567
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->QUOTE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258568
    :cond_d9
    const-string v0, "TEEM_COLLECTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_da

    .line 258569
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TEEM_COLLECTIONS:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258570
    :cond_da
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258571
    :pswitch_5d
    const-string v0, "GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_db

    .line 258572
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258573
    :cond_db
    const-string v0, "MEME_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_dc

    .line 258574
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MEME_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258575
    :cond_dc
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258576
    :pswitch_5e
    const-string v0, "MULTI_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_dd

    .line 258577
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258578
    :cond_dd
    const-string v0, "SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_de

    .line 258579
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258580
    :cond_de
    const-string v0, "INSTANT_ARTICLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_df

    .line 258581
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258582
    :cond_df
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258583
    :pswitch_5f
    const-string v0, "BALLOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e0

    .line 258584
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BALLOT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258585
    :cond_e0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258586
    :pswitch_60
    const-string v0, "ANSWER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e1

    .line 258587
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANSWER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258588
    :cond_e1
    const-string v0, "AVATAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e2

    .line 258589
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258590
    :cond_e2
    const-string v0, "SUPER_EMOJI"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e3

    .line 258591
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SUPER_EMOJI:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258592
    :cond_e3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258593
    :pswitch_61
    const-string v0, "THIRD_PARTY_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e4

    .line 258594
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->THIRD_PARTY_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258595
    :cond_e4
    const-string v0, "TOPIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e5

    .line 258596
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TOPIC:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258597
    :cond_e5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258598
    :pswitch_62
    const-string v0, "GROUP_QUIZ"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e6

    .line 258599
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_QUIZ:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258600
    :cond_e6
    const-string v0, "TRAVEL_LOG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e7

    .line 258601
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TRAVEL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258602
    :cond_e7
    const-string v0, "RTC_CALL_LOG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e8

    .line 258603
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RTC_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258604
    :cond_e8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258605
    :pswitch_63
    const-string v0, "QUOTED_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e9

    .line 258606
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->QUOTED_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258607
    :cond_e9
    const-string v0, "MESSENGER_INVITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ea

    .line 258608
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_INVITE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258609
    :cond_ea
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258610
    :pswitch_64
    const-string v0, "EVENTS_PENDING_POST_QUEUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_eb

    .line 258611
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENTS_PENDING_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258612
    :cond_eb
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258613
    :pswitch_65
    const-string v0, "GROUP_PENDING_POST_QUEUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ec

    .line 258614
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_PENDING_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258615
    :cond_ec
    const-string v0, "GROUP_JOIN_REQUEST_QUEUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ed

    .line 258616
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_JOIN_REQUEST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258617
    :cond_ed
    const-string v0, "RETAIL_NOW_IN_STOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ee

    .line 258618
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_NOW_IN_STOCK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258619
    :cond_ee
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258620
    :pswitch_66
    const-string v0, "COUPON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ef

    .line 258621
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COUPON:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258622
    :cond_ef
    const-string v0, "UNAVAILABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f0

    .line 258623
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258624
    :cond_f0
    const-string v0, "GROUP_REPORTED_POST_QUEUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f1

    .line 258625
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_REPORTED_POST_QUEUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258626
    :cond_f1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258627
    :pswitch_67
    const-string v0, "LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f2

    .line 258628
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258629
    :cond_f2
    const-string v0, "OG_COMPOSER_SIMPLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f3

    .line 258630
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->OG_COMPOSER_SIMPLE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258631
    :cond_f3
    const-string v0, "AIRLINE_FLIGHT_RESCHEDULE_UPDATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f4

    .line 258632
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AIRLINE_FLIGHT_RESCHEDULE_UPDATE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258633
    :cond_f4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258634
    :pswitch_68
    const-string v0, "FRIEND_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f5

    .line 258635
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258636
    :cond_f5
    const-string v0, "MESSENGER_PLATFORM_COVER_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f6

    .line 258637
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_COVER_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258638
    :cond_f6
    const-string v0, "MESSENGER_COMMERCE_COVER_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f7

    .line 258639
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_COMMERCE_COVER_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258640
    :cond_f7
    const-string v0, "EVENT_TOUR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f8

    .line 258641
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT_TOUR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258642
    :cond_f8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258643
    :pswitch_69
    const-string v0, "APPLICATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f9

    .line 258644
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->APPLICATION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258645
    :cond_f9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258646
    :pswitch_6a
    const-string v0, "SHARE_LARGE_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_fa

    .line 258647
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE_LARGE_IMAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258648
    :cond_fa
    const-string v0, "TELEPHONE_CALL_LOG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_fb

    .line 258649
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->TELEPHONE_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258650
    :cond_fb
    const-string v0, "INSTANT_GAMES_SHARE_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_fc

    .line 258651
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_GAMES_SHARE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258652
    :cond_fc
    const-string v0, "MESSENGER_PLATFORM_COMPACT_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_fd

    .line 258653
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_PLATFORM_COMPACT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    .line 258654
    :cond_fd
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_19
        :pswitch_0
        :pswitch_0
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_0
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_0
        :pswitch_27
        :pswitch_28
        :pswitch_0
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_0
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_0
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_0
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_0
        :pswitch_0
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_0
        :pswitch_4a
        :pswitch_0
        :pswitch_4b
        :pswitch_4c
        :pswitch_0
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_0
        :pswitch_0
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_0
        :pswitch_0
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_62
        :pswitch_63
        :pswitch_64
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_69
        :pswitch_6a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;
    .locals 1

    .prologue
    .line 258039
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;
    .locals 1

    .prologue
    .line 258038
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    return-object v0
.end method
