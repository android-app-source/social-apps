.class public final enum Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

.field public static final enum HAS_PINNED_DUMMY_STORY_NOT_SHOWING:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

.field public static final enum HAS_PINNED_DUMMY_STORY_SHOWING:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

.field public static final enum NO_PINNED_DUMMY_STORY:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 329381
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    .line 329382
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    const-string v1, "HAS_PINNED_DUMMY_STORY_SHOWING"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->HAS_PINNED_DUMMY_STORY_SHOWING:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    .line 329383
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    const-string v1, "HAS_PINNED_DUMMY_STORY_NOT_SHOWING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->HAS_PINNED_DUMMY_STORY_NOT_SHOWING:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    .line 329384
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    const-string v1, "NO_PINNED_DUMMY_STORY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->NO_PINNED_DUMMY_STORY:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    .line 329385
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->HAS_PINNED_DUMMY_STORY_SHOWING:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->HAS_PINNED_DUMMY_STORY_NOT_SHOWING:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->NO_PINNED_DUMMY_STORY:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 329380
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;
    .locals 1

    .prologue
    .line 329371
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    .line 329372
    :goto_0
    return-object v0

    .line 329373
    :cond_1
    const-string v0, "HAS_PINNED_DUMMY_STORY_SHOWING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 329374
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->HAS_PINNED_DUMMY_STORY_SHOWING:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    goto :goto_0

    .line 329375
    :cond_2
    const-string v0, "HAS_PINNED_DUMMY_STORY_NOT_SHOWING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 329376
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->HAS_PINNED_DUMMY_STORY_NOT_SHOWING:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    goto :goto_0

    .line 329377
    :cond_3
    const-string v0, "NO_PINNED_DUMMY_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 329378
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->NO_PINNED_DUMMY_STORY:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    goto :goto_0

    .line 329379
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;
    .locals 1

    .prologue
    .line 329369
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;
    .locals 1

    .prologue
    .line 329370
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    return-object v0
.end method
