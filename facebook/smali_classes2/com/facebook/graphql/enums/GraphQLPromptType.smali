.class public final enum Lcom/facebook/graphql/enums/GraphQLPromptType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPromptType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum AIRPORT:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum CHECKIN:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum CULTURAL:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum DRAFT_RECOVERY:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum EVENT:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum EVERGREEN_FILTER:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum EVERGREEN_LOCATION:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum EVERGREEN_WEATHER:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum FRIENDS_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum FRIENDS_HOLIDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum GAMEFACE:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum INSPIRATION_EFFECT:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum LOCATION:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum MANUAL:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum MOVIE_EVERGREEN:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum MOVIE_HOLIDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum MPS:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum OG_MUSIC:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum PHOTO_AND_SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum PHOTO_WEEKEND:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum SOUVENIR:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum SPORTS:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum SPORTS_MLB:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum SPORTS_NBA:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum SPORTS_NFL:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum SPORTS_POSTGAME:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum SPORTS_PREGAME:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum TRENDING_SHARE:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum TV_SHOW:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 309785
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309786
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "MANUAL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->MANUAL:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309787
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "CULTURAL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->CULTURAL:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309788
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "SPORTS_PREGAME"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_PREGAME:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309789
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "SPORTS"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309790
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "SPORTS_POSTGAME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_POSTGAME:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309791
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "GAMEFACE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->GAMEFACE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309792
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "SPORTS_NBA"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_NBA:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309793
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "SPORTS_NFL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_NFL:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309794
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "SPORTS_MLB"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_MLB:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309795
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "LOCATION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->LOCATION:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309796
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "EVENT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309797
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "TV_SHOW"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->TV_SHOW:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309798
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "OG_MUSIC"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->OG_MUSIC:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309799
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "CHECKIN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309800
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "CLIPBOARD"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309801
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "SOUVENIR"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SOUVENIR:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309802
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "PHOTO"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309803
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "PHOTO_AND_SLIDESHOW"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO_AND_SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309804
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "PHOTO_WEEKEND"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO_WEEKEND:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309805
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "MPS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->MPS:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309806
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "AIRPORT"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->AIRPORT:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309807
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "BIRTHDAY"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309808
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "FRIENDS_BIRTHDAY"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->FRIENDS_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309809
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "MOVIE_HOLIDAY"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->MOVIE_HOLIDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309810
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "MOVIE_EVERGREEN"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->MOVIE_EVERGREEN:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309811
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "FEED_STORY"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309812
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "DRAFT_RECOVERY"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->DRAFT_RECOVERY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309813
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "TRENDING_SHARE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->TRENDING_SHARE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309814
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "EVERGREEN_LOCATION"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVERGREEN_LOCATION:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309815
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "EVERGREEN_WEATHER"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVERGREEN_WEATHER:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309816
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "EVERGREEN_FILTER"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVERGREEN_FILTER:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309817
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "INSPIRATION_EFFECT"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->INSPIRATION_EFFECT:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309818
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    const-string v1, "FRIENDS_HOLIDAY"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->FRIENDS_HOLIDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309819
    const/16 v0, 0x22

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPromptType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->MANUAL:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->CULTURAL:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_PREGAME:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_POSTGAME:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->GAMEFACE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_NBA:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_NFL:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_MLB:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->LOCATION:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->TV_SHOW:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->OG_MUSIC:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->SOUVENIR:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO_AND_SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO_WEEKEND:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->MPS:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->AIRPORT:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->FRIENDS_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->MOVIE_HOLIDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->MOVIE_EVERGREEN:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->DRAFT_RECOVERY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->TRENDING_SHARE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVERGREEN_LOCATION:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVERGREEN_WEATHER:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVERGREEN_FILTER:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->INSPIRATION_EFFECT:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->FRIENDS_HOLIDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 309820
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptType;
    .locals 2

    .prologue
    .line 309821
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 309822
    :goto_0
    return-object v0

    .line 309823
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 309824
    packed-switch v0, :pswitch_data_0

    .line 309825
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309826
    :pswitch_1
    const-string v0, "MOVIE_HOLIDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 309827
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->MOVIE_HOLIDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309828
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309829
    :pswitch_2
    const-string v0, "EVERGREEN_FILTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 309830
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVERGREEN_FILTER:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309831
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309832
    :pswitch_3
    const-string v0, "TV_SHOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 309833
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->TV_SHOW:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309834
    :cond_4
    const-string v0, "EVERGREEN_WEATHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 309835
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVERGREEN_WEATHER:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309836
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309837
    :pswitch_4
    const-string v0, "SPORTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 309838
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309839
    :cond_6
    const-string v0, "LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 309840
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->LOCATION:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309841
    :cond_7
    const-string v0, "PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 309842
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto :goto_0

    .line 309843
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309844
    :pswitch_5
    const-string v0, "MANUAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 309845
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->MANUAL:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309846
    :cond_9
    const-string v0, "INSPIRATION_EFFECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 309847
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->INSPIRATION_EFFECT:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309848
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309849
    :pswitch_6
    const-string v0, "CLIPBOARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 309850
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309851
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309852
    :pswitch_7
    const-string v0, "SOUVENIR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 309853
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SOUVENIR:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309854
    :cond_c
    const-string v0, "EVERGREEN_LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 309855
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVERGREEN_LOCATION:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309856
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309857
    :pswitch_8
    const-string v0, "GAMEFACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 309858
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->GAMEFACE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309859
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309860
    :pswitch_9
    const-string v0, "PHOTO_AND_SLIDESHOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 309861
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO_AND_SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309862
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309863
    :pswitch_a
    const-string v0, "MOVIE_EVERGREEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 309864
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->MOVIE_EVERGREEN:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309865
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309866
    :pswitch_b
    const-string v0, "SPORTS_NFL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 309867
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_NFL:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309868
    :cond_11
    const-string v0, "BIRTHDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 309869
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309870
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309871
    :pswitch_c
    const-string v0, "OG_MUSIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 309872
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->OG_MUSIC:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309873
    :cond_13
    const-string v0, "AIRPORT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 309874
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->AIRPORT:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309875
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309876
    :pswitch_d
    const-string v0, "EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 309877
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309878
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309879
    :pswitch_e
    const-string v0, "FEED_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 309880
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309881
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309882
    :pswitch_f
    const-string v0, "PHOTO_WEEKEND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 309883
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO_WEEKEND:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309884
    :cond_17
    const-string v0, "DRAFT_RECOVERY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 309885
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->DRAFT_RECOVERY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309886
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309887
    :pswitch_10
    const-string v0, "SPORTS_MLB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 309888
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_MLB:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309889
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309890
    :pswitch_11
    const-string v0, "SPORTS_PREGAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 309891
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_PREGAME:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309892
    :cond_1a
    const-string v0, "SPORTS_NBA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 309893
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_NBA:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309894
    :cond_1b
    const-string v0, "CHECKIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 309895
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309896
    :cond_1c
    const-string v0, "FRIENDS_HOLIDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 309897
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->FRIENDS_HOLIDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309898
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309899
    :pswitch_12
    const-string v0, "SPORTS_POSTGAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 309900
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SPORTS_POSTGAME:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309901
    :cond_1e
    const-string v0, "MPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 309902
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->MPS:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309903
    :cond_1f
    const-string v0, "FRIENDS_BIRTHDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 309904
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->FRIENDS_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309905
    :cond_20
    const-string v0, "TRENDING_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 309906
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->TRENDING_SHARE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309907
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309908
    :pswitch_13
    const-string v0, "CULTURAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 309909
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->CULTURAL:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    .line 309910
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_13
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptType;
    .locals 1

    .prologue
    .line 309911
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPromptType;
    .locals 1

    .prologue
    .line 309912
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPromptType;

    return-object v0
.end method
