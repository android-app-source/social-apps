.class public final enum Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

.field public static final enum BLOCKED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

.field public static final enum BLOCKED_AND_ALREADY_APPEALED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

.field public static final enum BLOCKED_AND_ALREADY_CONFIRMED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

.field public static final enum NOT_BLOCKED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 260308
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 260309
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    const-string v1, "NOT_BLOCKED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->NOT_BLOCKED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 260310
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    const-string v1, "BLOCKED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 260311
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    const-string v1, "BLOCKED_AND_ALREADY_APPEALED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED_AND_ALREADY_APPEALED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 260312
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    const-string v1, "BLOCKED_AND_ALREADY_CONFIRMED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED_AND_ALREADY_CONFIRMED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 260313
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->NOT_BLOCKED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED_AND_ALREADY_APPEALED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED_AND_ALREADY_CONFIRMED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 260314
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;
    .locals 1

    .prologue
    .line 260315
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 260316
    :goto_0
    return-object v0

    .line 260317
    :cond_1
    const-string v0, "NOT_BLOCKED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260318
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->NOT_BLOCKED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    goto :goto_0

    .line 260319
    :cond_2
    const-string v0, "BLOCKED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 260320
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    goto :goto_0

    .line 260321
    :cond_3
    const-string v0, "BLOCKED_AND_ALREADY_APPEALED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 260322
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED_AND_ALREADY_APPEALED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    goto :goto_0

    .line 260323
    :cond_4
    const-string v0, "BLOCKED_AND_ALREADY_CONFIRMED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 260324
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED_AND_ALREADY_CONFIRMED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    goto :goto_0

    .line 260325
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;
    .locals 1

    .prologue
    .line 260326
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;
    .locals 1

    .prologue
    .line 260327
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    return-object v0
.end method
