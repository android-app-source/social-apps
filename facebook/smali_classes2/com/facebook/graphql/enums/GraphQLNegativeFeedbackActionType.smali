.class public final enum Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum AD_ALREADY_OWNED:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum APP_INVITE_BLOCK_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum APP_INVITE_BLOCK_USER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum APP_SUPPORT_REDIRECT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum AWESOMIZER_REFOLLOW_CARD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum AWESOMIZER_UNFOLLOW_CARD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum BAN_USER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum BLOCK_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum BLOCK_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum BLOCK_MESSAGES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum CHANGE_CONTENT_FILTER_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum CLOSE_OLD_PROFILE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum COMPASSION_SUPPORT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum CONTACT_FORM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum DELETE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum DELETE_AND_BLOCK:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum DEPRECATED_73:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum DIRECT_SUPPORT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum EDIT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HACKED_NOTIFY:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HEAD_PUBLISHER_APP_MENTIONS_BAN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HEAD_PUBLISHER_APP_MENTIONS_HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HELP_CENTER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE_ADVERTISER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE_AD_WITH_AD_ALREADY_OWNED:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE_AD_WITH_REMOVE_ITEM_FROM_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE_FEED_TOPIC:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE_FROM_TIMELINE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE_RESEARCH_POLLS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE_TOPIC:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum HIDE_TOPIC_MISCLASSIFICATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum IGNORE_EVENT_INVITES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum LEAVE_GROUP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum LESS_FROM_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum LESS_FROM_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum LESS_FROM_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum LESS_FROM_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum LESS_FROM_DOMAIN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum LESS_FROM_THROWBACK:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum LESS_FROM_UPSETTING_CONCEPT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum LIMIT_FRIENDING_PRIVACY:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum MARK_AS_FALSE_NEWS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum MARK_AS_OBJECTIONABLE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum MARK_AS_SPAM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum MARK_MESSAGE_THREAD_AS_SPAM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum MESSAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum NEWSFEED_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum PAGES_LIKE_CHECKUP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum PHONE_FRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum RECOVER_PROFILE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REDIRECT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REDIRECT_ADS_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REMOVE_ADS_INTEREST:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REMOVE_EVENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REMOVE_ITEM_FROM_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REPORT_AD_IP_VIOLATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REPORT_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REPORT_CONTENT_EDUCATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REPORT_CONTENT_WITH_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REPORT_IP_VIOLATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REPORT_TO_GROUP_ADMIN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum REPORT_WORK_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum SAVE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum SPAM_CLEANUP_CHECKPOINT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum THROWBACK_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNFOLLOW_FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNFRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNLIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNSUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNSUBSCRIBE_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNSUBSCRIBE_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNSUBSCRIBE_OWNER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNSUBSCRIBE_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNSUBSCRIBE_RESHARER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum UNTAG_SPONSOR_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum VIDEO_CHANNEL_FOLLOW:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum VIDEO_CHANNEL_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum VOTING_REMINDER_OPT_OUT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public static final enum WRITE_IN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 248265
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248266
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "AD_ALREADY_OWNED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->AD_ALREADY_OWNED:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248267
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "APP_SUPPORT_REDIRECT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->APP_SUPPORT_REDIRECT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248268
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "BAN_USER"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BAN_USER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248269
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "BLOCK_ACTOR"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248270
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "BLOCK_APP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248271
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "BLOCK_MESSAGES"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_MESSAGES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248272
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "BLOCK_PAGE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248273
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "DELETE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DELETE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248274
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "DELETE_AND_BLOCK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DELETE_AND_BLOCK:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248275
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "DIRECT_SUPPORT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DIRECT_SUPPORT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248276
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "EDIT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->EDIT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248277
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HACKED_NOTIFY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HACKED_NOTIFY:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248278
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HELP_CENTER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HELP_CENTER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248279
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE_ADVERTISER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_ADVERTISER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248280
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE_AD"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248281
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE_TOPIC_MISCLASSIFICATION"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_TOPIC_MISCLASSIFICATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248282
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE_RESEARCH_POLLS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_RESEARCH_POLLS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248283
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE_APP"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248284
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "LEAVE_GROUP"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LEAVE_GROUP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248285
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "LIMIT_FRIENDING_PRIVACY"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LIMIT_FRIENDING_PRIVACY:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248286
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "MARK_AS_SPAM"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_SPAM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248287
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "MARK_AS_OBJECTIONABLE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_OBJECTIONABLE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248288
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "MARK_MESSAGE_THREAD_AS_SPAM"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_MESSAGE_THREAD_AS_SPAM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248289
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "MESSAGE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248290
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "PHONE_FRIEND"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->PHONE_FRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248291
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "CONTACT_FORM"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->CONTACT_FORM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248292
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "CLOSE_OLD_PROFILE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->CLOSE_OLD_PROFILE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248293
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "RECOVER_PROFILE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RECOVER_PROFILE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248294
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REDIRECT"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REDIRECT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248295
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REDIRECT_ADS_PREFERENCES"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REDIRECT_ADS_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248296
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REMOVE_ADS_INTEREST"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REMOVE_ADS_INTEREST:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248297
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REPORT_CONTENT"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248298
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REPORT_TO_GROUP_ADMIN"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_TO_GROUP_ADMIN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248299
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REPORT_WORK_CONTENT"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_WORK_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248300
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REPORT_CONTENT_EDUCATION"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT_EDUCATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248301
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REPORT_IP_VIOLATION"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_IP_VIOLATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248302
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REPORT_AD_IP_VIOLATION"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_AD_IP_VIOLATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248303
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "RESOLVE_PROBLEM"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248304
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "SPAM_CLEANUP_CHECKPOINT"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->SPAM_CLEANUP_CHECKPOINT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248305
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNFOLLOW_FRIEND_LIST"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNFOLLOW_FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248306
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNFRIEND"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNFRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248307
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNLIKE"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNLIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248308
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNSUBSCRIBE"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248309
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNSUBSCRIBE_DIRECTED_TARGET"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248310
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNSUBSCRIBE_RESHARER"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_RESHARER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248311
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNSUBSCRIBE_OWNER"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_OWNER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248312
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNSUBSCRIBE_ATTACHED_STORY_ACTOR"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248313
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNTAG"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248314
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNTAG_SPONSOR_PAGE"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG_SPONSOR_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248315
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248316
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE_FROM_TIMELINE"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FROM_TIMELINE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248317
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "DONT_LIKE"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248318
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "UNSUBSCRIBE_PAGE"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248319
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HEAD_PUBLISHER_APP_MENTIONS_BAN"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HEAD_PUBLISHER_APP_MENTIONS_BAN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248320
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HEAD_PUBLISHER_APP_MENTIONS_HIDE"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HEAD_PUBLISHER_APP_MENTIONS_HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248321
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE_TOPIC"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_TOPIC:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248322
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "LESS_FROM_ACTOR"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248323
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "LESS_FROM_DIRECTED_TARGET"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248324
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "LESS_FROM_ATTACHED_STORY_ACTOR"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248325
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "LESS_FROM_DOMAIN"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_DOMAIN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248326
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "LESS_FROM_APP"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248327
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "LESS_FROM_UPSETTING_CONCEPT"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_UPSETTING_CONCEPT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248328
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "NEWSFEED_SETTINGS"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->NEWSFEED_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248329
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "AWESOMIZER_UNFOLLOW_CARD"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->AWESOMIZER_UNFOLLOW_CARD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248330
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "AWESOMIZER_REFOLLOW_CARD"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->AWESOMIZER_REFOLLOW_CARD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248331
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "WRITE_IN"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->WRITE_IN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248332
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "IGNORE_EVENT_INVITES"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->IGNORE_EVENT_INVITES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248333
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REMOVE_EVENT"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REMOVE_EVENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248334
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "APP_INVITE_BLOCK_APP"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->APP_INVITE_BLOCK_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248335
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "APP_INVITE_BLOCK_USER"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->APP_INVITE_BLOCK_USER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248336
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "LESS_FROM_THROWBACK"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_THROWBACK:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248337
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "THROWBACK_PREFERENCES"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->THROWBACK_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248338
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "DEPRECATED_73"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DEPRECATED_73:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248339
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE_FEED_TOPIC"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FEED_TOPIC:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248340
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "PAGES_LIKE_CHECKUP"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->PAGES_LIKE_CHECKUP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248341
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "SAVE"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248342
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "VOTING_REMINDER_OPT_OUT"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->VOTING_REMINDER_OPT_OUT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248343
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "VIDEO_CHANNEL_FOLLOW"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->VIDEO_CHANNEL_FOLLOW:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248344
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "VIDEO_CHANNEL_SUBSCRIBE"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->VIDEO_CHANNEL_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248345
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "CHANGE_CONTENT_FILTER_SETTINGS"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->CHANGE_CONTENT_FILTER_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248346
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REMOVE_ITEM_FROM_AD"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REMOVE_ITEM_FROM_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248347
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "REPORT_CONTENT_WITH_TYPEAHEAD"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT_WITH_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248348
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "COMPASSION_SUPPORT"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->COMPASSION_SUPPORT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248349
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "MARK_AS_FALSE_NEWS"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_FALSE_NEWS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248350
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE_AD_WITH_AD_ALREADY_OWNED"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD_WITH_AD_ALREADY_OWNED:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248351
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v1, "HIDE_AD_WITH_REMOVE_ITEM_FROM_AD"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD_WITH_REMOVE_ITEM_FROM_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248352
    const/16 v0, 0x57

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->AD_ALREADY_OWNED:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->APP_SUPPORT_REDIRECT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BAN_USER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_MESSAGES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DELETE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DELETE_AND_BLOCK:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DIRECT_SUPPORT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->EDIT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HACKED_NOTIFY:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HELP_CENTER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_ADVERTISER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_TOPIC_MISCLASSIFICATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_RESEARCH_POLLS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LEAVE_GROUP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LIMIT_FRIENDING_PRIVACY:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_SPAM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_OBJECTIONABLE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_MESSAGE_THREAD_AS_SPAM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->PHONE_FRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->CONTACT_FORM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->CLOSE_OLD_PROFILE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RECOVER_PROFILE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REDIRECT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REDIRECT_ADS_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REMOVE_ADS_INTEREST:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_TO_GROUP_ADMIN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_WORK_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT_EDUCATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_IP_VIOLATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_AD_IP_VIOLATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->SPAM_CLEANUP_CHECKPOINT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNFOLLOW_FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNFRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNLIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_RESHARER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_OWNER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG_SPONSOR_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FROM_TIMELINE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HEAD_PUBLISHER_APP_MENTIONS_BAN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HEAD_PUBLISHER_APP_MENTIONS_HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_TOPIC:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_DOMAIN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_UPSETTING_CONCEPT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->NEWSFEED_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->AWESOMIZER_UNFOLLOW_CARD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->AWESOMIZER_REFOLLOW_CARD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->WRITE_IN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->IGNORE_EVENT_INVITES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REMOVE_EVENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->APP_INVITE_BLOCK_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->APP_INVITE_BLOCK_USER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_THROWBACK:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->THROWBACK_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DEPRECATED_73:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FEED_TOPIC:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->PAGES_LIKE_CHECKUP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->VOTING_REMINDER_OPT_OUT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->VIDEO_CHANNEL_FOLLOW:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->VIDEO_CHANNEL_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->CHANGE_CONTENT_FILTER_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REMOVE_ITEM_FROM_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT_WITH_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->COMPASSION_SUPPORT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_FALSE_NEWS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD_WITH_AD_ALREADY_OWNED:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD_WITH_REMOVE_ITEM_FROM_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 248353
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 2

    .prologue
    .line 248354
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 248355
    :goto_0
    return-object v0

    .line 248356
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x3f

    .line 248357
    packed-switch v0, :pswitch_data_0

    .line 248358
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248359
    :pswitch_1
    const-string v0, "HIDE_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 248360
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248361
    :cond_2
    const-string v0, "UNSUBSCRIBE_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 248362
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248363
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248364
    :pswitch_2
    const-string v0, "APP_SUPPORT_REDIRECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 248365
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->APP_SUPPORT_REDIRECT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248366
    :cond_4
    const-string v0, "HELP_CENTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 248367
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HELP_CENTER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248368
    :cond_5
    const-string v0, "COMPASSION_SUPPORT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 248369
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->COMPASSION_SUPPORT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248370
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248371
    :pswitch_3
    const-string v0, "UNTAG_SPONSOR_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 248372
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG_SPONSOR_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248373
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248374
    :pswitch_4
    const-string v0, "HEAD_PUBLISHER_APP_MENTIONS_HIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 248375
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HEAD_PUBLISHER_APP_MENTIONS_HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0

    .line 248376
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248377
    :pswitch_5
    const-string v0, "APP_INVITE_BLOCK_USER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 248378
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->APP_INVITE_BLOCK_USER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248379
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248380
    :pswitch_6
    const-string v0, "HIDE_ADVERTISER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 248381
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_ADVERTISER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248382
    :cond_a
    const-string v0, "APP_INVITE_BLOCK_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 248383
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->APP_INVITE_BLOCK_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248384
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248385
    :pswitch_7
    const-string v0, "REDIRECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 248386
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REDIRECT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248387
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248388
    :pswitch_8
    const-string v0, "LEAVE_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 248389
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LEAVE_GROUP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248390
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248391
    :pswitch_9
    const-string v0, "VIDEO_CHANNEL_SUBSCRIBE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 248392
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->VIDEO_CHANNEL_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248393
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248394
    :pswitch_a
    const-string v0, "LESS_FROM_ACTOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 248395
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248396
    :cond_f
    const-string v0, "LESS_FROM_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 248397
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248398
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248399
    :pswitch_b
    const-string v0, "REMOVE_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 248400
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REMOVE_EVENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248401
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248402
    :pswitch_c
    const-string v0, "HIDE_AD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 248403
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248404
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248405
    :pswitch_d
    const-string v0, "REPORT_CONTENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 248406
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248407
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248408
    :pswitch_e
    const-string v0, "AD_ALREADY_OWNED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 248409
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->AD_ALREADY_OWNED:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248410
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248411
    :pswitch_f
    const-string v0, "LESS_FROM_DOMAIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 248412
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_DOMAIN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248413
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248414
    :pswitch_10
    const-string v0, "REMOVE_ADS_INTEREST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 248415
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REMOVE_ADS_INTEREST:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248416
    :cond_16
    const-string v0, "REPORT_WORK_CONTENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 248417
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_WORK_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248418
    :cond_17
    const-string v0, "LESS_FROM_DIRECTED_TARGET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 248419
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248420
    :cond_18
    const-string v0, "WRITE_IN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 248421
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->WRITE_IN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248422
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248423
    :pswitch_11
    const-string v0, "PAGES_LIKE_CHECKUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 248424
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->PAGES_LIKE_CHECKUP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248425
    :cond_1a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248426
    :pswitch_12
    const-string v0, "LESS_FROM_UPSETTING_CONCEPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 248427
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_UPSETTING_CONCEPT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248428
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248429
    :pswitch_13
    const-string v0, "UNSUBSCRIBE_OWNER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 248430
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_OWNER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248431
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248432
    :pswitch_14
    const-string v0, "UNFOLLOW_FRIEND_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 248433
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNFOLLOW_FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248434
    :cond_1d
    const-string v0, "AWESOMIZER_UNFOLLOW_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 248435
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->AWESOMIZER_UNFOLLOW_CARD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248436
    :cond_1e
    const-string v0, "AWESOMIZER_REFOLLOW_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 248437
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->AWESOMIZER_REFOLLOW_CARD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248438
    :cond_1f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248439
    :pswitch_15
    const-string v0, "SPAM_CLEANUP_CHECKPOINT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 248440
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->SPAM_CLEANUP_CHECKPOINT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248441
    :cond_20
    const-string v0, "HIDE_TOPIC_MISCLASSIFICATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 248442
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_TOPIC_MISCLASSIFICATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248443
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248444
    :pswitch_16
    const-string v0, "REPORT_IP_VIOLATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 248445
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_IP_VIOLATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248446
    :cond_22
    const-string v0, "UNSUBSCRIBE_RESHARER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 248447
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_RESHARER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248448
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248449
    :pswitch_17
    const-string v0, "PHONE_FRIEND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 248450
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->PHONE_FRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248451
    :cond_24
    const-string v0, "LESS_FROM_ATTACHED_STORY_ACTOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 248452
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248453
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248454
    :pswitch_18
    const-string v0, "REPORT_TO_GROUP_ADMIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 248455
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_TO_GROUP_ADMIN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248456
    :cond_26
    const-string v0, "UNFRIEND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 248457
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNFRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248458
    :cond_27
    const-string v0, "HEAD_PUBLISHER_APP_MENTIONS_BAN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 248459
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HEAD_PUBLISHER_APP_MENTIONS_BAN:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248460
    :cond_28
    const-string v0, "VOTING_REMINDER_OPT_OUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 248461
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->VOTING_REMINDER_OPT_OUT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248462
    :cond_29
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248463
    :pswitch_19
    const-string v0, "REPORT_AD_IP_VIOLATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 248464
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_AD_IP_VIOLATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248465
    :cond_2a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248466
    :pswitch_1a
    const-string v0, "HACKED_NOTIFY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 248467
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HACKED_NOTIFY:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248468
    :cond_2b
    const-string v0, "REPORT_CONTENT_EDUCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 248469
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT_EDUCATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248470
    :cond_2c
    const-string v0, "UNSUBSCRIBE_DIRECTED_TARGET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 248471
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248472
    :cond_2d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248473
    :pswitch_1b
    const-string v0, "BLOCK_MESSAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 248474
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_MESSAGES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248475
    :cond_2e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248476
    :pswitch_1c
    const-string v0, "HIDE_AD_WITH_AD_ALREADY_OWNED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 248477
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD_WITH_AD_ALREADY_OWNED:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248478
    :cond_2f
    const-string v0, "REMOVE_ITEM_FROM_AD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 248479
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REMOVE_ITEM_FROM_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248480
    :cond_30
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248481
    :pswitch_1d
    const-string v0, "CONTACT_FORM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 248482
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->CONTACT_FORM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248483
    :cond_31
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248484
    :pswitch_1e
    const-string v0, "UNSUBSCRIBE_ATTACHED_STORY_ACTOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 248485
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248486
    :cond_32
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248487
    :pswitch_1f
    const-string v0, "HIDE_AD_WITH_REMOVE_ITEM_FROM_AD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 248488
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD_WITH_REMOVE_ITEM_FROM_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248489
    :cond_33
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248490
    :pswitch_20
    const-string v0, "DELETE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 248491
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DELETE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248492
    :cond_34
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248493
    :pswitch_21
    const-string v0, "BLOCK_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 248494
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248495
    :cond_35
    const-string v0, "HIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 248496
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248497
    :cond_36
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248498
    :pswitch_22
    const-string v0, "HIDE_RESEARCH_POLLS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 248499
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_RESEARCH_POLLS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248500
    :cond_37
    const-string v0, "DONT_LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 248501
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248502
    :cond_38
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248503
    :pswitch_23
    const-string v0, "DELETE_AND_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 248504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DELETE_AND_BLOCK:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248505
    :cond_39
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248506
    :pswitch_24
    const-string v0, "IGNORE_EVENT_INVITES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 248507
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->IGNORE_EVENT_INVITES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248508
    :cond_3a
    const-string v0, "LIMIT_FRIENDING_PRIVACY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 248509
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LIMIT_FRIENDING_PRIVACY:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248510
    :cond_3b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248511
    :pswitch_25
    const-string v0, "REPORT_CONTENT_WITH_TYPEAHEAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 248512
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT_WITH_TYPEAHEAD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248513
    :cond_3c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248514
    :pswitch_26
    const-string v0, "MARK_AS_FALSE_NEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 248515
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_FALSE_NEWS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248516
    :cond_3d
    const-string v0, "MARK_AS_SPAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 248517
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_SPAM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248518
    :cond_3e
    const-string v0, "NEWSFEED_SETTINGS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 248519
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->NEWSFEED_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248520
    :cond_3f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248521
    :pswitch_27
    const-string v0, "CHANGE_CONTENT_FILTER_SETTINGS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 248522
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->CHANGE_CONTENT_FILTER_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248523
    :cond_40
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248524
    :pswitch_28
    const-string v0, "CLOSE_OLD_PROFILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 248525
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->CLOSE_OLD_PROFILE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248526
    :cond_41
    const-string v0, "HIDE_TOPIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 248527
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_TOPIC:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248528
    :cond_42
    const-string v0, "MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 248529
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248530
    :cond_43
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248531
    :pswitch_29
    const-string v0, "SAVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 248532
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248533
    :cond_44
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248534
    :pswitch_2a
    const-string v0, "UNTAG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 248535
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248536
    :cond_45
    const-string v0, "VIDEO_CHANNEL_FOLLOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 248537
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->VIDEO_CHANNEL_FOLLOW:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248538
    :cond_46
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248539
    :pswitch_2b
    const-string v0, "HIDE_FEED_TOPIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 248540
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FEED_TOPIC:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248541
    :cond_47
    const-string v0, "RESOLVE_PROBLEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 248542
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248543
    :cond_48
    const-string v0, "LESS_FROM_THROWBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 248544
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->LESS_FROM_THROWBACK:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248545
    :cond_49
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248546
    :pswitch_2c
    const-string v0, "EDIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 248547
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->EDIT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248548
    :cond_4a
    const-string v0, "HIDE_FROM_TIMELINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 248549
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FROM_TIMELINE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248550
    :cond_4b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248551
    :pswitch_2d
    const-string v0, "UNLIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 248552
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNLIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248553
    :cond_4c
    const-string v0, "THROWBACK_PREFERENCES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 248554
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->THROWBACK_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248555
    :cond_4d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248556
    :pswitch_2e
    const-string v0, "REDIRECT_ADS_PREFERENCES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 248557
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REDIRECT_ADS_PREFERENCES:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248558
    :cond_4e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248559
    :pswitch_2f
    const-string v0, "BAN_USER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 248560
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BAN_USER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248561
    :cond_4f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248562
    :pswitch_30
    const-string v0, "BLOCK_ACTOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 248563
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248564
    :cond_50
    const-string v0, "BLOCK_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 248565
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248566
    :cond_51
    const-string v0, "MARK_MESSAGE_THREAD_AS_SPAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 248567
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_MESSAGE_THREAD_AS_SPAM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248568
    :cond_52
    const-string v0, "UNSUBSCRIBE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 248569
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248570
    :cond_53
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248571
    :pswitch_31
    const-string v0, "RECOVER_PROFILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 248572
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RECOVER_PROFILE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248573
    :cond_54
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248574
    :pswitch_32
    const-string v0, "MARK_AS_OBJECTIONABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 248575
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_OBJECTIONABLE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248576
    :cond_55
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248577
    :pswitch_33
    const-string v0, "DIRECT_SUPPORT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 248578
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DIRECT_SUPPORT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    .line 248579
    :cond_56
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_0
        :pswitch_1a
        :pswitch_1b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_0
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_0
        :pswitch_27
        :pswitch_28
        :pswitch_0
        :pswitch_0
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_0
        :pswitch_0
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 1

    .prologue
    .line 248263
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 1

    .prologue
    .line 248264
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    return-object v0
.end method
