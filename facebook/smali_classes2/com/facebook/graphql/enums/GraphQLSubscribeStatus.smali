.class public final enum Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public static final enum CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public static final enum CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public static final enum IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 188289
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 188290
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const-string v1, "CANNOT_SUBSCRIBE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 188291
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const-string v1, "IS_SUBSCRIBED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 188292
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const-string v1, "CAN_SUBSCRIBE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 188293
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 188288
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 1

    .prologue
    .line 188294
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 188295
    :goto_0
    return-object v0

    .line 188296
    :cond_1
    const-string v0, "CANNOT_SUBSCRIBE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188297
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0

    .line 188298
    :cond_2
    const-string v0, "IS_SUBSCRIBED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 188299
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0

    .line 188300
    :cond_3
    const-string v0, "CAN_SUBSCRIBE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 188301
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0

    .line 188302
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 1

    .prologue
    .line 188286
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 1

    .prologue
    .line 188287
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method
