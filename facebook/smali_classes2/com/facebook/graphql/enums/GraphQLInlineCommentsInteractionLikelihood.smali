.class public final enum Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

.field public static final enum HIGH:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

.field public static final enum LOW:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 270091
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    .line 270092
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    .line 270093
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->LOW:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    .line 270094
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->HIGH:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    .line 270095
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->LOW:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->HIGH:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 270096
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;
    .locals 1

    .prologue
    .line 270082
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    .line 270083
    :goto_0
    return-object v0

    .line 270084
    :cond_1
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 270085
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    goto :goto_0

    .line 270086
    :cond_2
    const-string v0, "LOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 270087
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->LOW:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    goto :goto_0

    .line 270088
    :cond_3
    const-string v0, "HIGH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 270089
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->HIGH:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    goto :goto_0

    .line 270090
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;
    .locals 1

    .prologue
    .line 270080
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;
    .locals 1

    .prologue
    .line 270081
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    return-object v0
.end method
