.class public final enum Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum AUTO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum CROWDSOURCING_TRANSLATE_TO_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum DEPRECATED_4:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum HIDE_AUTO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum HIDE_SEE_CONVERSION_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum HIDE_SEE_TRANSLATION_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum NO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum SEE_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 267166
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267167
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "NO_TRANSLATION"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->NO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267168
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "SEE_TRANSLATION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267169
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "AUTO_TRANSLATION"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->AUTO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267170
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "DEPRECATED_4"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->DEPRECATED_4:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267171
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "SEE_CONVERSION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267172
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "HIDE_SEE_TRANSLATION_LINK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->HIDE_SEE_TRANSLATION_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267173
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "HIDE_AUTO_TRANSLATION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->HIDE_AUTO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267174
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "HIDE_SEE_CONVERSION_LINK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->HIDE_SEE_CONVERSION_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267175
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "DEPRECATED_9"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267176
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const-string v1, "CROWDSOURCING_TRANSLATE_TO_LINK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->CROWDSOURCING_TRANSLATE_TO_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267177
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->NO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->AUTO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->DEPRECATED_4:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->HIDE_SEE_TRANSLATION_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->HIDE_AUTO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->HIDE_SEE_CONVERSION_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->CROWDSOURCING_TRANSLATE_TO_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 267178
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;
    .locals 1

    .prologue
    .line 267179
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 267180
    :goto_0
    return-object v0

    .line 267181
    :cond_1
    const-string v0, "NO_TRANSLATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 267182
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->NO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    goto :goto_0

    .line 267183
    :cond_2
    const-string v0, "SEE_TRANSLATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 267184
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    goto :goto_0

    .line 267185
    :cond_3
    const-string v0, "AUTO_TRANSLATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 267186
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->AUTO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    goto :goto_0

    .line 267187
    :cond_4
    const-string v0, "SEE_CONVERSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 267188
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    goto :goto_0

    .line 267189
    :cond_5
    const-string v0, "CROWDSOURCING_TRANSLATE_TO_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 267190
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->CROWDSOURCING_TRANSLATE_TO_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    goto :goto_0

    .line 267191
    :cond_6
    const-string v0, "HIDE_SEE_TRANSLATION_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 267192
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->HIDE_SEE_TRANSLATION_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    goto :goto_0

    .line 267193
    :cond_7
    const-string v0, "HIDE_AUTO_TRANSLATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 267194
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->HIDE_AUTO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    goto :goto_0

    .line 267195
    :cond_8
    const-string v0, "HIDE_SEE_CONVERSION_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 267196
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->HIDE_SEE_CONVERSION_LINK:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    goto :goto_0

    .line 267197
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;
    .locals 1

    .prologue
    .line 267198
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;
    .locals 1

    .prologue
    .line 267199
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    return-object v0
.end method
