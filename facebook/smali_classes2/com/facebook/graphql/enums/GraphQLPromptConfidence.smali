.class public final enum Lcom/facebook/graphql/enums/GraphQLPromptConfidence;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPromptConfidence;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

.field public static final enum HIGH:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

.field public static final enum LOW:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

.field public static final enum NEUTRAL:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

.field public static final enum VERY_HIGH:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

.field public static final enum VERY_LOW:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 309670
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    .line 309671
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    const-string v1, "VERY_LOW"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->VERY_LOW:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    .line 309672
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->LOW:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    .line 309673
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    const-string v1, "NEUTRAL"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->NEUTRAL:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    .line 309674
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->HIGH:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    .line 309675
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    const-string v1, "VERY_HIGH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->VERY_HIGH:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    .line 309676
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->VERY_LOW:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->LOW:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->NEUTRAL:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->HIGH:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->VERY_HIGH:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 309677
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptConfidence;
    .locals 1

    .prologue
    .line 309678
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    .line 309679
    :goto_0
    return-object v0

    .line 309680
    :cond_1
    const-string v0, "VERY_LOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 309681
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->VERY_LOW:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    goto :goto_0

    .line 309682
    :cond_2
    const-string v0, "LOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 309683
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->LOW:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    goto :goto_0

    .line 309684
    :cond_3
    const-string v0, "NEUTRAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 309685
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->NEUTRAL:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    goto :goto_0

    .line 309686
    :cond_4
    const-string v0, "HIGH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 309687
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->HIGH:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    goto :goto_0

    .line 309688
    :cond_5
    const-string v0, "VERY_HIGH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 309689
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->VERY_HIGH:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    goto :goto_0

    .line 309690
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptConfidence;
    .locals 1

    .prologue
    .line 309691
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPromptConfidence;
    .locals 1

    .prologue
    .line 309692
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    return-object v0
.end method
