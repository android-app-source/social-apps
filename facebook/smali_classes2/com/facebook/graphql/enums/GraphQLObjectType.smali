.class public Lcom/facebook/graphql/enums/GraphQLObjectType;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0Pm;
.implements Lcom/facebook/flatbuffers/Flattenable;
.implements Lcom/facebook/flatbuffers/MutableFlattenable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/enums/GraphQLObjectTypeDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/enums/GraphQLObjectTypeSerializer;
.end annotation

.annotation build Lcom/instagram/common/json/annotation/JsonType;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLObjectType;",
        ">;",
        "Lcom/facebook/flatbuffers/Flattenable;",
        "Lcom/facebook/flatbuffers/MutableFlattenable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 186120
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectTypeDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 186121
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectTypeSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186122
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType$1;

    invoke-direct {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType$1;-><init>()V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLObjectType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 186123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186124
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    .line 186125
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 186131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186132
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    .line 186133
    iput p1, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    .line 186134
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    .line 186135
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 186126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186127
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    .line 186128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    .line 186129
    iget v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    invoke-static {v0}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    .line 186130
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;)V
    .locals 1

    .prologue
    .line 186138
    iget v0, p1, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a:I

    invoke-direct {p0, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 186139
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 186140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186141
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    .line 186142
    invoke-direct {p0, p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(Ljava/lang/String;)V

    .line 186143
    iget v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    invoke-static {v0}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    .line 186144
    return-void
.end method

.method public static a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 2

    .prologue
    .line 186102
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 186103
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 186104
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 186105
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    .line 186106
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 186107
    :goto_0
    return-object v0

    .line 186108
    :cond_0
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 186136
    invoke-static {p1}, LX/38I;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    .line 186137
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 186115
    iget-object v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 186116
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 186117
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 186118
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 186119
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->f()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 186111
    iput v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    .line 186112
    invoke-virtual {p1, p2, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    .line 186113
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->f()Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 186114
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 2

    .prologue
    .line 186110
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "initFromFlatBuffer is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 186109
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonGetter;
        value = "name"
    .end annotation

    .prologue
    .line 186099
    iget v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 186100
    iget-object v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(Ljava/lang/String;)V

    .line 186101
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    invoke-static {v0}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 186096
    iget v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 186097
    iget-object v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(Ljava/lang/String;)V

    .line 186098
    :cond_0
    return-object p0
.end method

.method public final g()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 186095
    iget v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 186094
    const/4 v0, 0x0

    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186093
    const/4 v0, 0x0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186092
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 186091
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 186089
    iget v0, p0, Lcom/facebook/graphql/enums/GraphQLObjectType;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 186090
    return-void
.end method
