.class public final enum Lcom/facebook/graphql/enums/StoryVisibility;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/StoryVisibility;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/StoryVisibility;

.field public static final enum CONTRACTING:Lcom/facebook/graphql/enums/StoryVisibility;

.field public static final enum DISAPPEARING:Lcom/facebook/graphql/enums/StoryVisibility;

.field public static final enum GONE:Lcom/facebook/graphql/enums/StoryVisibility;

.field public static final enum HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

.field public static final enum VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 206278
    new-instance v0, Lcom/facebook/graphql/enums/StoryVisibility;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/StoryVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 206279
    new-instance v0, Lcom/facebook/graphql/enums/StoryVisibility;

    const-string v1, "CONTRACTING"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/StoryVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->CONTRACTING:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 206280
    new-instance v0, Lcom/facebook/graphql/enums/StoryVisibility;

    const-string v1, "DISAPPEARING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/StoryVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->DISAPPEARING:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 206281
    new-instance v0, Lcom/facebook/graphql/enums/StoryVisibility;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/StoryVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 206282
    new-instance v0, Lcom/facebook/graphql/enums/StoryVisibility;

    const-string v1, "GONE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/StoryVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 206283
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/StoryVisibility;

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->CONTRACTING:Lcom/facebook/graphql/enums/StoryVisibility;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->DISAPPEARING:Lcom/facebook/graphql/enums/StoryVisibility;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->$VALUES:[Lcom/facebook/graphql/enums/StoryVisibility;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 206274
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 206277
    const-class v0, Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/StoryVisibility;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 206284
    sget-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->$VALUES:[Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/StoryVisibility;

    return-object v0
.end method


# virtual methods
.method public final getRequestParamValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206276
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/StoryVisibility;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final isHiddenOrVisible()Z
    .locals 1

    .prologue
    .line 206275
    sget-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
