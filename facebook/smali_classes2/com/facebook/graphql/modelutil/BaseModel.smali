.class public abstract Lcom/facebook/graphql/modelutil/BaseModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a_:Z


# instance fields
.field public b:LX/49K;

.field public c:LX/15i;

.field public d:I

.field private final e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123345
    const/4 v0, 0x0

    sput-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 123342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123343
    iput p1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->e:I

    .line 123344
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 123339
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    if-eqz v0, :cond_0

    .line 123340
    shr-int/lit8 v0, p1, 0x3

    and-int/lit8 v1, p1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 123341
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/graphql/modelutil/BaseModel;Lcom/facebook/graphql/modelutil/BaseModel;I)V
    .locals 2

    .prologue
    .line 123329
    if-eqz p1, :cond_0

    .line 123330
    iget-object v0, p1, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    move-object v0, v0

    .line 123331
    if-eqz v0, :cond_0

    .line 123332
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    move-object v1, v1

    .line 123333
    if-eqz v1, :cond_0

    .line 123334
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    move-object v1, v1

    .line 123335
    iput-object v1, v0, LX/49K;->i:LX/49K;

    .line 123336
    const/4 p0, 0x0

    iput-object p0, v0, LX/49K;->h:Ljava/lang/String;

    .line 123337
    iput p2, v0, LX/49K;->k:I

    .line 123338
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/graphql/modelutil/BaseModel;LX/49J;)LX/49K;
    .locals 8

    .prologue
    .line 123310
    new-instance v0, LX/49K;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->e:I

    const/4 v4, 0x0

    .line 123311
    iget-object v2, p1, LX/49J;->c:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->toArray()[Ljava/lang/Object;

    move-result-object v6

    .line 123312
    array-length v7, v6

    move v3, v4

    move v5, v4

    :goto_0
    if-ge v3, v7, :cond_0

    aget-object v2, v6, v3

    .line 123313
    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v5, v2

    .line 123314
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 123315
    :cond_0
    if-lez v5, :cond_2

    .line 123316
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 123317
    aget-object v2, v6, v4

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123318
    const/4 v2, 0x1

    :goto_1
    array-length v4, v6

    if-ge v2, v4, :cond_1

    .line 123319
    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v6, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123320
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 123321
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 123322
    :goto_2
    move-object v2, v2

    .line 123323
    invoke-direct {v0, v1, v2, p0}, LX/49K;-><init>(ILjava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V

    iput-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    .line 123324
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    .line 123325
    iget-object v1, p1, LX/49J;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123326
    iput-object p1, v0, LX/49K;->j:LX/49J;

    .line 123327
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    .line 123328
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    return-object v0

    :cond_2
    const-string v2, ""

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/util/List;I)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123302
    invoke-direct {p0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(I)V

    .line 123303
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_2

    .line 123304
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    invoke-virtual {v0, v1, p2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object p1

    move-object v0, p1

    .line 123305
    :goto_0
    if-nez v0, :cond_0

    .line 123306
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 123307
    :cond_0
    instance-of v1, v0, LX/0Px;

    if-nez v1, :cond_1

    .line 123308
    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 123309
    :cond_1
    check-cast v0, LX/0Px;

    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public final a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;)",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 123232
    invoke-direct {p0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(I)V

    .line 123233
    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_3

    .line 123234
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    invoke-virtual {v0, v1, p2, p3}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object p1

    .line 123235
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    move-object v0, v0

    .line 123236
    if-eqz v0, :cond_0

    .line 123237
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    .line 123238
    invoke-static {p0, v0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Lcom/facebook/graphql/modelutil/BaseModel;Lcom/facebook/graphql/modelutil/BaseModel;I)V

    goto :goto_0

    .line 123239
    :cond_0
    move-object v0, p1

    .line 123240
    :goto_1
    if-nez v0, :cond_1

    .line 123241
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 123242
    :cond_1
    instance-of v1, v0, LX/0Px;

    if-nez v1, :cond_2

    .line 123243
    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 123244
    :cond_2
    check-cast v0, LX/0Px;

    return-object v0

    :cond_3
    move-object v0, p1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(TT;I",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 123297
    invoke-direct {p0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(I)V

    .line 123298
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_1

    .line 123299
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    invoke-virtual {v0, v1, p2, p3}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object p1

    move-object v0, p1

    .line 123300
    :goto_0
    if-nez v0, :cond_0

    .line 123301
    :goto_1
    return-object p4

    :cond_0
    move-object p4, v0

    goto :goto_1

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;I",
            "LX/16a;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 123287
    invoke-direct {p0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(I)V

    .line 123288
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    .line 123289
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    .line 123290
    invoke-virtual {v0, v1, p2}, LX/15i;->g(II)I

    move-result p1

    .line 123291
    if-eqz p1, :cond_1

    .line 123292
    invoke-virtual {v0, p1, p3}, LX/15i;->a(ILX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object p1

    .line 123293
    :goto_0
    move-object v1, p1

    .line 123294
    move-object v0, v1

    .line 123295
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-static {p0, v0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Lcom/facebook/graphql/modelutil/BaseModel;Lcom/facebook/graphql/modelutil/BaseModel;I)V

    move-object p1, v1

    .line 123296
    :cond_0
    return-object p1

    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;)TT;"
        }
    .end annotation

    .prologue
    .line 123282
    invoke-direct {p0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(I)V

    .line 123283
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    .line 123284
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    invoke-virtual {v0, v1, p2, p3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    move-object v0, v1

    .line 123285
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-static {p0, v0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Lcom/facebook/graphql/modelutil/BaseModel;Lcom/facebook/graphql/modelutil/BaseModel;I)V

    move-object p1, v1

    .line 123286
    :cond_0
    return-object p1
.end method

.method public final a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 123278
    invoke-direct {p0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(I)V

    .line 123279
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    .line 123280
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    invoke-virtual {v0, v1, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 123281
    :cond_0
    return-object p1
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 123273
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    if-eqz v0, :cond_0

    .line 123274
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    .line 123275
    iget-boolean v1, v0, LX/49K;->a:Z

    if-eqz v1, :cond_0

    .line 123276
    iget-object v1, v0, LX/49K;->b:[B

    aget-byte v2, v1, p1

    const/4 p0, 0x1

    shl-int/2addr p0, p2

    or-int/2addr v2, p0

    int-to-byte v2, v2

    aput-byte v2, v1, p1

    .line 123277
    :cond_0
    return-void
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 123271
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 123272
    return-void
.end method

.method public a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 123260
    iput-object p1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 123261
    iput p2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    .line 123262
    invoke-static {p1}, LX/0sR;->b(Ljava/lang/Object;)LX/49J;

    move-result-object v0

    .line 123263
    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    .line 123264
    invoke-static {p3}, LX/0sR;->b(Ljava/lang/Object;)LX/49J;

    move-result-object v0

    .line 123265
    if-eqz v0, :cond_0

    instance-of v1, p3, LX/15w;

    if-eqz v1, :cond_0

    .line 123266
    check-cast p3, LX/15w;

    invoke-virtual {p3}, LX/15w;->j()LX/12V;

    move-result-object v1

    invoke-static {v1}, LX/0sR;->a(LX/12V;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/49J;->a(Ljava/lang/String;)V

    .line 123267
    invoke-static {p1, v0}, LX/0sR;->a(Ljava/lang/Object;LX/49J;)Z

    .line 123268
    :cond_0
    if-eqz v0, :cond_1

    .line 123269
    invoke-static {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Lcom/facebook/graphql/modelutil/BaseModel;LX/49J;)LX/49K;

    .line 123270
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 123196
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 2

    .prologue
    .line 123197
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "initFromFlatBuffer is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/util/List;I)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;I)",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123198
    invoke-direct {p0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(I)V

    .line 123199
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_2

    .line 123200
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    invoke-virtual {v0, v1, p2}, LX/15i;->e(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object p1

    move-object v0, p1

    .line 123201
    :goto_0
    if-nez v0, :cond_0

    .line 123202
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 123203
    :cond_0
    instance-of v1, v0, LX/0Px;

    if-nez v1, :cond_1

    .line 123204
    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 123205
    :cond_1
    check-cast v0, LX/0Px;

    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public final b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "Ljava/util/List",
            "<TT;>;I",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 123206
    invoke-direct {p0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(I)V

    .line 123207
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_2

    .line 123208
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    invoke-virtual {v0, v1, p2, p3}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object p1

    move-object v0, p1

    .line 123209
    :goto_0
    if-nez v0, :cond_0

    .line 123210
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 123211
    :cond_0
    instance-of v1, v0, LX/0Px;

    if-nez v1, :cond_1

    .line 123212
    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 123213
    :cond_1
    check-cast v0, LX/0Px;

    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(TT;I",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 123214
    invoke-direct {p0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(I)V

    .line 123215
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_2

    .line 123216
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    invoke-virtual {v0, v1, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 123217
    if-nez v0, :cond_1

    .line 123218
    :cond_0
    :goto_0
    return-object p4

    .line 123219
    :cond_1
    :try_start_0
    invoke-static {v0}, LX/1fg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    move-object v0, p1

    .line 123220
    :goto_1
    if-eqz v0, :cond_0

    move-object p4, v0

    .line 123221
    goto :goto_0

    .line 123222
    :catch_0
    move-exception v0

    .line 123223
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Falling back to unset enum value"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move-object v0, p1

    goto :goto_1
.end method

.method public final c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "Ljava/util/List",
            "<TT;>;I",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 123224
    invoke-direct {p0, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(I)V

    .line 123225
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_2

    .line 123226
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    invoke-virtual {v0, v1, p2, p3}, LX/15i;->c(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object p1

    move-object v0, p1

    .line 123227
    :goto_0
    if-nez v0, :cond_0

    .line 123228
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 123229
    :cond_0
    instance-of v1, v0, LX/0Px;

    if-nez v1, :cond_1

    .line 123230
    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 123231
    :cond_1
    check-cast v0, LX/0Px;

    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 123245
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    if-eqz v0, :cond_0

    .line 123246
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    .line 123247
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/49K;->a:Z

    .line 123248
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 123249
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    if-eqz v0, :cond_0

    .line 123250
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    .line 123251
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/49K;->a:Z

    .line 123252
    :cond_0
    return-void
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 123253
    iget v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 123254
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 123255
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 123256
    :catch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot clone "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 123257
    instance-of v0, p0, LX/0jT;

    if-eqz v0, :cond_0

    .line 123258
    check-cast p0, LX/0jT;

    invoke-interface {p0}, LX/0jT;->f()I

    move-result v0

    invoke-static {v0}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/23C;->a(Ljava/lang/String;)S

    move-result v0

    .line 123259
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
