.class public final Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 268516
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 268530
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 268536
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 268537
    return-void
.end method

.method public constructor <init>(LX/4WO;)V
    .locals 1

    .prologue
    .line 268531
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 268532
    iget-object v0, p1, LX/4WO;->b:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->e:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    .line 268533
    iget-object v0, p1, LX/4WO;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 268534
    iget-object v0, p1, LX/4WO;->d:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->g:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    .line 268535
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 268517
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->g:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 268518
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->g:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->g:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    .line 268519
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->g:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 268520
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 268521
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 268522
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 268523
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 268524
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 268525
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 268526
    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->k()Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 268527
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 268528
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 268529
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->k()Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 268496
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 268497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 268498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    .line 268499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 268500
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    .line 268501
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->e:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    .line 268502
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 268503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 268504
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 268505
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    .line 268506
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 268507
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 268508
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 268509
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->e:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 268510
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->e:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->e:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    .line 268511
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->e:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 268512
    const v0, 0x1aa068cd

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 268513
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 268514
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 268515
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
