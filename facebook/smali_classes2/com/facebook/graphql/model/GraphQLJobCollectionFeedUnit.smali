.class public final Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/17w;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit$Serializer;
.end annotation


# instance fields
.field private A:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:J

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I

.field public r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 251797
    const-class v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 251802
    const-class v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 251803
    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 251804
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x32512483

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 251805
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->A:LX/0x2;

    .line 251806
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251807
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251808
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 251809
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251810
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251811
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 251812
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 251813
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->t:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251814
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->t:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->t:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 251815
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->t:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    return-object v0
.end method

.method private D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251816
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251817
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->u:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->u:Ljava/lang/String;

    .line 251818
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->u:Ljava/lang/String;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251819
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251820
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 251821
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    return-object v0
.end method

.method private F()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251822
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->w:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251823
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->w:Ljava/util/List;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->w:Ljava/util/List;

    .line 251824
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->w:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251825
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251826
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251827
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 251828
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->q:I

    .line 251829
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 251830
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 251831
    if-eqz v0, :cond_0

    .line 251832
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 251833
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 251864
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->o:Ljava/lang/String;

    .line 251865
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 251866
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 251867
    if-eqz v0, :cond_0

    .line 251868
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 251869
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 251834
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->p:Ljava/lang/String;

    .line 251835
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 251836
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 251837
    if-eqz v0, :cond_0

    .line 251838
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 251839
    :cond_0
    return-void
.end method

.method private s()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251840
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251841
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->f:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->f:Ljava/util/List;

    .line 251842
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private t()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251843
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251844
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->g:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->g:Ljava/util/List;

    .line 251845
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251846
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251847
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 251848
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251849
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251850
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 251851
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251852
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251853
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->m:Ljava/lang/String;

    .line 251854
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251855
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251856
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->o:Ljava/lang/String;

    .line 251857
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251858
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251859
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->p:Ljava/lang/String;

    .line 251860
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method

.method private z()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 251861
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 251862
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 251863
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->q:I

    return v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 251798
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251799
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251800
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->i:Ljava/lang/String;

    .line 251801
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 251641
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 251642
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 251643
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->l:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 251644
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 251645
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->A:LX/0x2;

    if-nez v0, :cond_0

    .line 251646
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->A:LX/0x2;

    .line 251647
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->A:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 251648
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 251649
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 251650
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 251651
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->s()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 251652
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->t()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 251653
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 251654
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->E_()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 251655
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 251656
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 251657
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->w()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 251658
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n()LX/0Px;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 251659
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->x()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 251660
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->y()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 251661
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 251662
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 251663
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->D()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 251664
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 251665
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->F()LX/0Px;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->d(Ljava/util/List;)I

    move-result v16

    .line 251666
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 251667
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->c()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 251668
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 251669
    const/16 v20, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 251670
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 251671
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 251672
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 251673
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 251674
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 251675
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 251676
    const/4 v3, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 251677
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 251678
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 251679
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 251680
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 251681
    const/16 v2, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->z()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 251682
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 251683
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 251684
    const/16 v3, 0xe

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->C()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 251685
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 251686
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 251687
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 251688
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 251689
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 251690
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 251691
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 251692
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 251693
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->C()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 251694
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 251695
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->s()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 251696
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->s()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 251697
    if-eqz v1, :cond_0

    .line 251698
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 251699
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->f:Ljava/util/List;

    .line 251700
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->t()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 251701
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->t()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 251702
    if-eqz v1, :cond_1

    .line 251703
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 251704
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 251705
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 251706
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 251707
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 251708
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 251709
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 251710
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 251711
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 251712
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 251713
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 251714
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 251715
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 251716
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 251717
    if-eqz v2, :cond_4

    .line 251718
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 251719
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n:Ljava/util/List;

    move-object v1, v0

    .line 251720
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 251721
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251722
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 251723
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 251724
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251725
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 251726
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 251727
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 251728
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 251729
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 251730
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 251731
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 251732
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 251733
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 251734
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 251735
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 251736
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 251737
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 251738
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 251739
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 251740
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 251741
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251742
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 251743
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 251744
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251745
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 251746
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 251747
    new-instance v0, LX/4X3;

    invoke-direct {v0, p1}, LX/4X3;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251748
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 251749
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->l:J

    .line 251750
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 251751
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 251752
    const/4 v0, 0x6

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->l:J

    .line 251753
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->q:I

    .line 251754
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 251755
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251756
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->x()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 251757
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 251758
    const/16 v0, 0x9

    iput v0, p2, LX/18L;->c:I

    .line 251759
    :goto_0
    return-void

    .line 251760
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251761
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 251762
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 251763
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 251764
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251765
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->z()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 251766
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 251767
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 251768
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 251769
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251770
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->a(Ljava/lang/String;)V

    .line 251771
    :cond_0
    :goto_0
    return-void

    .line 251772
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251773
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 251774
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251775
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251776
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251777
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->y:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->y:Ljava/lang/String;

    .line 251778
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251779
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251780
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 251781
    :goto_0
    return-object v0

    .line 251782
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 251783
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 251784
    const v0, -0x32512483

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251785
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251786
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->h:Ljava/lang/String;

    .line 251787
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1

    .prologue
    .line 251788
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251789
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251790
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n:Ljava/util/List;

    .line 251791
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251792
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251793
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251794
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final synthetic p()Ljava/util/List;
    .locals 1

    .prologue
    .line 251795
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 251796
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
