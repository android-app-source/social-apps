.class public final Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:I

.field private q:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 254353
    const-class v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 254348
    const-class v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 254354
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 254355
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x5a045f73

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 254356
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->q:LX/0x2;

    .line 254357
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 254358
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->p:I

    .line 254359
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 254360
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 254361
    if-eqz v0, :cond_0

    .line 254362
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 254363
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 254364
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n:Ljava/lang/String;

    .line 254365
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 254366
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 254367
    if-eqz v0, :cond_0

    .line 254368
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 254369
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 254370
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o:Ljava/lang/String;

    .line 254371
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 254372
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 254373
    if-eqz v0, :cond_0

    .line 254374
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 254375
    :cond_0
    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254376
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254377
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->j:Ljava/lang/String;

    .line 254378
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254379
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254380
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->m:Ljava/lang/String;

    .line 254381
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254382
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254383
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n:Ljava/lang/String;

    .line 254384
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n:Ljava/lang/String;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254400
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254401
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o:Ljava/lang/String;

    .line 254402
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method private x()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 254385
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 254386
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 254387
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->p:I

    return v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 254388
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254389
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254390
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->h:Ljava/lang/String;

    .line 254391
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 254392
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 254393
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 254394
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->i:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 254395
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 254396
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 254397
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->q:LX/0x2;

    if-nez v0, :cond_0

    .line 254398
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->q:LX/0x2;

    .line 254399
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->q:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 254350
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 254351
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 254352
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 254260
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 254261
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 254262
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 254263
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->E_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 254264
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 254265
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 254266
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 254267
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 254268
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->v()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 254269
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 254270
    const/16 v3, 0xb

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 254271
    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 254272
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 254273
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 254274
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 254275
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 254276
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 254277
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 254278
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 254279
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 254280
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 254281
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->x()I

    move-result v1

    invoke-virtual {p1, v0, v1, v12}, LX/186;->a(III)V

    .line 254282
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 254283
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 254284
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 254285
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254286
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    .line 254287
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 254288
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    .line 254289
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    .line 254290
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 254291
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254292
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 254293
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    .line 254294
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254295
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 254296
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 254297
    new-instance v0, LX/4Vw;

    invoke-direct {v0, p1}, LX/4Vw;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254298
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 254299
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->i:J

    .line 254300
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 254301
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 254302
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->i:J

    .line 254303
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->p:I

    .line 254304
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 254305
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254306
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 254307
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 254308
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    .line 254309
    :goto_0
    return-void

    .line 254310
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254311
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 254312
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 254313
    const/16 v0, 0x9

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 254314
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254315
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->x()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 254316
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 254317
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 254318
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 254319
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254320
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->a(Ljava/lang/String;)V

    .line 254321
    :cond_0
    :goto_0
    return-void

    .line 254322
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254323
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 254324
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254325
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254326
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254327
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254328
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->l:Ljava/lang/String;

    .line 254329
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254330
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254331
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 254332
    :goto_0
    return-object v0

    .line 254333
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 254334
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 254335
    const v0, 0x5a045f73

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254336
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254337
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->f:Ljava/lang/String;

    .line 254338
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 254339
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->I_()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16d;

    invoke-static {v0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 254340
    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254341
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254342
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    .line 254343
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254344
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254345
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254346
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 254347
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 254349
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
