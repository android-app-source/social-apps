.class public interface abstract Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/HideableUnit;


# virtual methods
.method public abstract o()Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end method

.method public abstract r()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end method

.method public abstract s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end method
