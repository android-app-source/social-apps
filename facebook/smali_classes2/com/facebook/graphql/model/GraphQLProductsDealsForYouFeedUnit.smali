.class public final Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:I

.field private u:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 252389
    const-class v0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 252390
    const-class v0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 252391
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 252392
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x7af1f23b

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 252393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->u:LX/0x2;

    .line 252394
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252395
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252396
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->s:Ljava/lang/String;

    .line 252397
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->s:Ljava/lang/String;

    return-object v0
.end method

.method private B()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 252398
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 252399
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 252400
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->t:I

    return v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 252401
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->t:I

    .line 252402
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 252403
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 252404
    if-eqz v0, :cond_0

    .line 252405
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 252406
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 252407
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r:Ljava/lang/String;

    .line 252408
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 252409
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 252410
    if-eqz v0, :cond_0

    .line 252411
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 252412
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 252413
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->s:Ljava/lang/String;

    .line 252414
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 252415
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 252416
    if-eqz v0, :cond_0

    .line 252417
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 252418
    :cond_0
    return-void
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252419
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252420
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->j:Ljava/lang/String;

    .line 252421
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252422
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252423
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->l:Ljava/lang/String;

    .line 252424
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252425
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252426
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252427
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252428
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252429
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->p:Ljava/lang/String;

    .line 252430
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method

.method private y()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 252431
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 252432
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 252433
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->q:I

    return v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252434
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252435
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r:Ljava/lang/String;

    .line 252436
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252437
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252438
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->i:Ljava/lang/String;

    .line 252439
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 252440
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252441
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252442
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->g:Ljava/lang/String;

    .line 252443
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 252444
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 252445
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 252446
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->h:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 252447
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 252385
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 252386
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->u:LX/0x2;

    if-nez v0, :cond_0

    .line 252387
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->u:LX/0x2;

    .line 252388
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->u:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 252275
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 252276
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 252277
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 18

    .prologue
    .line 252278
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 252279
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 252280
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->E_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 252281
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->C_()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 252282
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->u()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 252283
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 252284
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->v()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 252285
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 252286
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 252287
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->c()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 252288
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->x()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 252289
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->z()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 252290
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->A()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 252291
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 252292
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 252293
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 252294
    const/4 v3, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 252295
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 252296
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 252297
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 252298
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 252299
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 252300
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 252301
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 252302
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 252303
    const/16 v2, 0xc

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->y()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 252304
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 252305
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 252306
    const/16 v2, 0xf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->B()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 252307
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 252308
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 252309
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 252310
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252311
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    .line 252312
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 252313
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    .line 252314
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    .line 252315
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 252316
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252317
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 252318
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    .line 252319
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252320
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 252321
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252322
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 252323
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    .line 252324
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252325
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 252326
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 252327
    new-instance v0, LX/4YN;

    invoke-direct {v0, p1}, LX/4YN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252328
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->u()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 252329
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->h:J

    .line 252330
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 252331
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 252332
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->h:J

    .line 252333
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->q:I

    .line 252334
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->t:I

    .line 252335
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 252336
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252337
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->z()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252338
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252339
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    .line 252340
    :goto_0
    return-void

    .line 252341
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252342
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->A()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252343
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252344
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 252345
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252346
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->B()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252347
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252348
    const/16 v0, 0xf

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 252349
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 252350
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252351
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->a(Ljava/lang/String;)V

    .line 252352
    :cond_0
    :goto_0
    return-void

    .line 252353
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252354
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 252355
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252356
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252274
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252357
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252358
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o:Ljava/lang/String;

    .line 252359
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252360
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252361
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 252362
    :goto_0
    return-object v0

    .line 252363
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 252364
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 252365
    const v0, 0x7af1f23b

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252366
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252367
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->f:Ljava/lang/String;

    .line 252368
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252369
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252370
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252371
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    .line 252372
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 6

    .prologue
    .line 252373
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;)LX/0Px;

    move-result-object v2

    .line 252374
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 252375
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    .line 252376
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 252377
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 252378
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 252379
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 252380
    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 252381
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252382
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252383
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252384
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
