.class public abstract Lcom/facebook/graphql/model/BaseImpression;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public a:LX/183;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public b:LX/183;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public c:LX/183;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public d:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public e:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public f:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public g:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public h:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public j:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public k:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public l:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public m:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 203811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203812
    sget-object v0, LX/183;->NOT_LOGGED:LX/183;

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->a:LX/183;

    .line 203813
    sget-object v0, LX/183;->NOT_LOGGED:LX/183;

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->b:LX/183;

    .line 203814
    sget-object v0, LX/183;->NOT_LOGGED:LX/183;

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->c:LX/183;

    .line 203815
    iput-boolean v1, p0, Lcom/facebook/graphql/model/BaseImpression;->d:Z

    .line 203816
    iput-boolean v1, p0, Lcom/facebook/graphql/model/BaseImpression;->e:Z

    .line 203817
    iput-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->f:J

    .line 203818
    iput-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    .line 203819
    iput-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->h:J

    .line 203820
    iput v1, p0, Lcom/facebook/graphql/model/BaseImpression;->j:I

    .line 203821
    iput v1, p0, Lcom/facebook/graphql/model/BaseImpression;->k:I

    .line 203822
    iput v1, p0, Lcom/facebook/graphql/model/BaseImpression;->l:I

    .line 203823
    iput-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->m:J

    .line 203824
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->i:Ljava/lang/String;

    .line 203825
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 203855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203856
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/BaseImpression;->d:Z

    .line 203857
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/BaseImpression;->e:Z

    .line 203858
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/BaseImpression;->j:I

    .line 203859
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/BaseImpression;->k:I

    .line 203860
    sget-object v0, LX/183;->NOT_LOGGED:LX/183;

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->a:LX/183;

    .line 203861
    sget-object v0, LX/183;->NOT_LOGGED:LX/183;

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->b:LX/183;

    .line 203862
    sget-object v0, LX/183;->NOT_LOGGED:LX/183;

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->c:LX/183;

    .line 203863
    iput-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->f:J

    .line 203864
    iput-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    .line 203865
    iput-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->h:J

    .line 203866
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/graphql/model/BaseImpression;->l:I

    .line 203867
    iput-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->m:J

    .line 203868
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->i:Ljava/lang/String;

    .line 203869
    return-void
.end method

.method private c(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 203848
    invoke-virtual {p0}, Lcom/facebook/graphql/model/BaseImpression;->k()Z

    move-result v1

    if-nez v1, :cond_1

    .line 203849
    :cond_0
    :goto_0
    return v0

    .line 203850
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/BaseImpression;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203851
    iget-object v1, p0, Lcom/facebook/graphql/model/BaseImpression;->c:LX/183;

    sget-object v2, LX/183;->PENDING:LX/183;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/graphql/model/BaseImpression;->c:LX/183;

    sget-object v2, LX/183;->LOGGING:LX/183;

    if-eq v1, v2, :cond_0

    .line 203852
    iget-boolean v1, p0, Lcom/facebook/graphql/model/BaseImpression;->d:Z

    if-nez v1, :cond_0

    .line 203853
    iget-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    iget v1, p0, Lcom/facebook/graphql/model/BaseImpression;->k:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    cmp-long v1, v2, p1

    if-gtz v1, :cond_0

    .line 203854
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/3EA;ZJ)V
    .locals 3

    .prologue
    .line 203834
    if-eqz p2, :cond_2

    sget-object v0, LX/183;->LOGGED:LX/183;

    .line 203835
    :goto_0
    sget-object v1, LX/3EB;->a:[I

    invoke-virtual {p1}, LX/3EA;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 203836
    :goto_1
    sget-object v0, LX/3EA;->ORIGINAL:LX/3EA;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/3EA;->SUBSEQUENT:LX/3EA;

    if-ne p1, v0, :cond_1

    .line 203837
    :cond_0
    if-eqz p2, :cond_3

    .line 203838
    iput-wide p3, p0, Lcom/facebook/graphql/model/BaseImpression;->f:J

    .line 203839
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/graphql/model/BaseImpression;->m:J

    .line 203840
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/graphql/model/BaseImpression;->l:I

    .line 203841
    :cond_1
    :goto_2
    return-void

    .line 203842
    :cond_2
    sget-object v0, LX/183;->FAILED:LX/183;

    goto :goto_0

    .line 203843
    :pswitch_0
    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->a:LX/183;

    goto :goto_1

    .line 203844
    :pswitch_1
    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->b:LX/183;

    goto :goto_1

    .line 203845
    :pswitch_2
    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->c:LX/183;

    goto :goto_1

    .line 203846
    :cond_3
    iput-wide p3, p0, Lcom/facebook/graphql/model/BaseImpression;->m:J

    .line 203847
    iget v0, p0, Lcom/facebook/graphql/model/BaseImpression;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/graphql/model/BaseImpression;->l:I

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public abstract a()Z
.end method

.method public final a(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 203826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/BaseImpression;->k()Z

    move-result v2

    if-nez v2, :cond_1

    .line 203827
    :cond_0
    :goto_0
    return v0

    .line 203828
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/BaseImpression;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 203829
    if-lez p1, :cond_2

    iget v2, p0, Lcom/facebook/graphql/model/BaseImpression;->j:I

    if-lt p1, v2, :cond_2

    move v2, v1

    .line 203830
    :goto_1
    if-nez v2, :cond_0

    .line 203831
    iget-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 203832
    goto :goto_0

    :cond_2
    move v2, v0

    .line 203833
    goto :goto_1
.end method

.method public final a(IJZ)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    .line 203800
    iget v0, p0, Lcom/facebook/graphql/model/BaseImpression;->j:I

    if-lt p1, v0, :cond_1

    move v0, v1

    .line 203801
    :goto_0
    if-eqz v0, :cond_2

    .line 203802
    iget-wide v0, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 203803
    iput-wide p2, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    .line 203804
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/facebook/graphql/model/BaseImpression;->c(J)Z

    move-result v0

    .line 203805
    :goto_1
    return v0

    .line 203806
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 203807
    :cond_2
    iget-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    if-eqz p4, :cond_3

    .line 203808
    iput-boolean v1, p0, Lcom/facebook/graphql/model/BaseImpression;->e:Z

    .line 203809
    :cond_3
    invoke-direct {p0, p2, p3}, Lcom/facebook/graphql/model/BaseImpression;->c(J)Z

    move-result v0

    .line 203810
    iput-wide v4, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    goto :goto_1
.end method

.method public final b(J)J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 203798
    iget-wide v2, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_0

    iget-wide v0, p0, Lcom/facebook/graphql/model/BaseImpression;->g:J

    sub-long v0, p1, v0

    .line 203799
    :cond_0
    return-wide v0
.end method

.method public final b(LX/3EA;)V
    .locals 2

    .prologue
    .line 203870
    sget-object v0, LX/3EB;->a:[I

    invoke-virtual {p1}, LX/3EA;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 203871
    :goto_0
    return-void

    .line 203872
    :pswitch_0
    sget-object v0, LX/183;->LOGGING:LX/183;

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->a:LX/183;

    goto :goto_0

    .line 203873
    :pswitch_1
    sget-object v0, LX/183;->LOGGING:LX/183;

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->b:LX/183;

    goto :goto_0

    .line 203874
    :pswitch_2
    sget-object v0, LX/183;->LOGGING:LX/183;

    iput-object v0, p0, Lcom/facebook/graphql/model/BaseImpression;->c:LX/183;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public abstract b()Z
.end method

.method public abstract c()Z
.end method

.method public d()J
    .locals 2

    .prologue
    .line 203789
    const-wide v0, 0x7fffffffffffffffL

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 203797
    const/4 v0, 0x0

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 203796
    const-wide v0, 0x7fffffffffffffffL

    return-wide v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 203795
    const/4 v0, 0x0

    return v0
.end method

.method public abstract k()Z
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 203790
    iget-boolean v0, p0, Lcom/facebook/graphql/model/BaseImpression;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 203791
    iget-boolean v0, p0, Lcom/facebook/graphql/model/BaseImpression;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 203792
    iget v0, p0, Lcom/facebook/graphql/model/BaseImpression;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 203793
    iget v0, p0, Lcom/facebook/graphql/model/BaseImpression;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 203794
    return-void
.end method
