.class public final Lcom/facebook/graphql/model/GraphQLBoostedComponent;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLBoostedComponent$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLBoostedComponent$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLAdAccount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public i:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

.field public k:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:J

.field public s:J

.field public t:Z

.field public u:Z

.field public v:Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;",
            ">;"
        }
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262502
    const-class v0, Lcom/facebook/graphql/model/GraphQLBoostedComponent$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262503
    const-class v0, Lcom/facebook/graphql/model/GraphQLBoostedComponent$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 262504
    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 262505
    return-void
.end method

.method private A()Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262506
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->w:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262507
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->w:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->w:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 262508
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->w:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    return-object v0
.end method

.method private B()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262509
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->x:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262510
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->x:Ljava/util/List;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->x:Ljava/util/List;

    .line 262511
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->x:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private C()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262512
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262513
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->y:Ljava/util/List;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->y:Ljava/util/List;

    .line 262514
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->y:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262515
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->z:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262516
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->z:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->z:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 262517
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->z:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLAdAccount;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262518
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->e:Lcom/facebook/graphql/model/GraphQLAdAccount;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262519
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->e:Lcom/facebook/graphql/model/GraphQLAdAccount;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLAdAccount;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAdAccount;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->e:Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 262520
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->e:Lcom/facebook/graphql/model/GraphQLAdAccount;

    return-object v0
.end method

.method private l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262521
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262522
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262523
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->g:I

    return v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262524
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262525
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 262526
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->h:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262527
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->i:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262528
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->i:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->i:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 262529
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->i:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262530
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262531
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    .line 262532
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262533
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->k:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262534
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->k:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x6

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->k:Lcom/facebook/graphql/model/FeedUnit;

    .line 262535
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->k:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method private q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 262370
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262371
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262372
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->m:Z

    return v0
.end method

.method private r()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262499
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262500
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->n:Ljava/util/List;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->n:Ljava/util/List;

    .line 262501
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262367
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->o:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262368
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->o:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->o:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    .line 262369
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->o:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262373
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262374
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262375
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262376
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262377
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 262378
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method private v()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262379
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262380
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262381
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->r:J

    return-wide v0
.end method

.method private w()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262382
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262383
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262384
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->s:J

    return-wide v0
.end method

.method private x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262385
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262386
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262387
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->t:Z

    return v0
.end method

.method private y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262388
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262389
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262390
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->u:Z

    return v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262391
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->v:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262392
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->v:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->v:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 262393
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->v:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 262394
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262395
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->k()Lcom/facebook/graphql/model/GraphQLAdAccount;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 262396
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->a()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 262397
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->n()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 262398
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->p()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    sget-object v4, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v3, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v3

    .line 262399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 262400
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->r()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 262401
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 262402
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->u()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 262403
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->z()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 262404
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->B()LX/0Px;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/util/List;)I

    move-result v9

    .line 262405
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->C()LX/0Px;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->d(Ljava/util/List;)I

    move-result v10

    .line 262406
    const/16 v11, 0x16

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 262407
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 262408
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 262409
    const/4 v0, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->l()I

    move-result v1

    const/4 v11, 0x0

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 262410
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v0

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    if-ne v0, v11, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262411
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 262412
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262413
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 262414
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 262415
    const/16 v0, 0x8

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->q()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 262416
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 262417
    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->s()Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262418
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 262419
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 262420
    const/16 v1, 0xd

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->v()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 262421
    const/16 v1, 0xe

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->w()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 262422
    const/16 v0, 0xf

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->x()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 262423
    const/16 v0, 0x10

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->y()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 262424
    const/16 v0, 0x11

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 262425
    const/16 v1, 0x12

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->A()Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262426
    const/16 v0, 0x13

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 262427
    const/16 v0, 0x14

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 262428
    const/16 v1, 0x15

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->D()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    if-ne v0, v2, :cond_4

    const/4 v0, 0x0

    :goto_4
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262429
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262430
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 262431
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v0

    goto/16 :goto_0

    .line 262432
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v0

    goto/16 :goto_1

    .line 262433
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->s()Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v0

    goto :goto_2

    .line 262434
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->A()Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    move-result-object v0

    goto :goto_3

    .line 262435
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->D()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v0

    goto :goto_4
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 262436
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262437
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->k()Lcom/facebook/graphql/model/GraphQLAdAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262438
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->k()Lcom/facebook/graphql/model/GraphQLAdAccount;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 262439
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->k()Lcom/facebook/graphql/model/GraphQLAdAccount;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 262440
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 262441
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->e:Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 262442
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->z()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 262443
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->z()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 262444
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->z()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 262445
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 262446
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->v:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 262447
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->a()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 262448
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->a()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 262449
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->a()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 262450
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 262451
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->f:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 262452
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->n()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 262453
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->n()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 262454
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->n()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 262455
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 262456
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->i:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 262457
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->p()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 262458
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->p()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 262459
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->p()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 262460
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 262461
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->k:Lcom/facebook/graphql/model/FeedUnit;

    .line 262462
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 262463
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    .line 262464
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 262465
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 262466
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->l:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    .line 262467
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 262468
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->r()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 262469
    if-eqz v2, :cond_6

    .line 262470
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 262471
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->n:Ljava/util/List;

    move-object v1, v0

    .line 262472
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 262473
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262474
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 262475
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 262476
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262477
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->u()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 262478
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->u()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 262479
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->u()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 262480
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 262481
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->q:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 262482
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262483
    if-nez v1, :cond_9

    :goto_0
    return-object p0

    :cond_9
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262484
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->f:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262485
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->f:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->f:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 262486
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->f:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 262487
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 262488
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->g:I

    .line 262489
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->m:Z

    .line 262490
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->r:J

    .line 262491
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->s:J

    .line 262492
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->t:Z

    .line 262493
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->u:Z

    .line 262494
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 262495
    const v0, -0x2d08045

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262496
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->l:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262497
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->l:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->l:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    .line 262498
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->l:Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    return-object v0
.end method
