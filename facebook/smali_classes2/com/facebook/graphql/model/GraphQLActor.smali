.class public final Lcom/facebook/graphql/model/GraphQLActor;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLActor$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLActor$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/enums/GraphQLGender;

.field public B:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Z

.field public E:Z

.field public F:Z

.field public G:Z

.field public H:Z

.field public I:Z

.field public J:Z

.field public K:Z

.field public L:Z

.field public M:Z

.field public N:Z

.field public O:Z

.field public P:Z

.field public Q:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

.field public S:J

.field public T:D

.field public U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Z

.field public aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Z

.field public aE:D

.field public aF:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

.field public aG:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Lcom/facebook/graphql/model/GraphQLDate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Z

.field public aK:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aN:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:I

.field public aP:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

.field public aR:Z

.field public aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Z

.field public aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Z

.field public aZ:Z

.field public aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/graphql/model/GraphQLProfileBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:J

.field public ag:Z

.field public ah:Lcom/facebook/graphql/model/GraphQLProfileVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public al:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:Lcom/facebook/graphql/model/GraphQLName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:I

.field public au:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Z

.field public ax:Z

.field public ay:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:Z

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public s:D

.field public t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLEventsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 204314
    const-class v0, Lcom/facebook/graphql/model/GraphQLActor$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 204283
    const-class v0, Lcom/facebook/graphql/model/GraphQLActor$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 204281
    const/16 v0, 0x65

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 204282
    return-void
.end method

.method public constructor <init>(LX/3dL;)V
    .locals 2

    .prologue
    .line 204179
    const/16 v0, 0x65

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 204180
    iget-object v0, p1, LX/3dL;->b:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aM:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 204181
    iget-object v0, p1, LX/3dL;->c:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->f:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    .line 204182
    iget-object v0, p1, LX/3dL;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->g:Ljava/lang/String;

    .line 204183
    iget-object v0, p1, LX/3dL;->e:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 204184
    iget-object v0, p1, LX/3dL;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aG:Ljava/lang/String;

    .line 204185
    iget v0, p1, LX/3dL;->g:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aO:I

    .line 204186
    iget-object v0, p1, LX/3dL;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204187
    iget-object v0, p1, LX/3dL;->i:Lcom/facebook/graphql/model/GraphQLDate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aI:Lcom/facebook/graphql/model/GraphQLDate;

    .line 204188
    iget-object v0, p1, LX/3dL;->j:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->j:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 204189
    iget-boolean v0, p1, LX/3dL;->k:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->k:Z

    .line 204190
    iget-boolean v0, p1, LX/3dL;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->l:Z

    .line 204191
    iget-boolean v0, p1, LX/3dL;->m:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->m:Z

    .line 204192
    iget-boolean v0, p1, LX/3dL;->n:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->n:Z

    .line 204193
    iget-boolean v0, p1, LX/3dL;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->o:Z

    .line 204194
    iget-boolean v0, p1, LX/3dL;->p:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->p:Z

    .line 204195
    iget-boolean v0, p1, LX/3dL;->q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->q:Z

    .line 204196
    iget-object v0, p1, LX/3dL;->r:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->r:Ljava/util/List;

    .line 204197
    iget-wide v0, p1, LX/3dL;->s:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->s:D

    .line 204198
    iget-object v0, p1, LX/3dL;->t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 204199
    iget-object v0, p1, LX/3dL;->u:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->u:Lcom/facebook/graphql/model/GraphQLPage;

    .line 204200
    iget-boolean v0, p1, LX/3dL;->v:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aW:Z

    .line 204201
    iget-object v0, p1, LX/3dL;->w:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->v:Ljava/util/List;

    .line 204202
    iget-object v0, p1, LX/3dL;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->w:Ljava/lang/String;

    .line 204203
    iget-object v0, p1, LX/3dL;->y:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aK:Ljava/lang/String;

    .line 204204
    iget-object v0, p1, LX/3dL;->z:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->x:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    .line 204205
    iget-object v0, p1, LX/3dL;->A:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 204206
    iget-object v0, p1, LX/3dL;->B:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 204207
    iget-object v0, p1, LX/3dL;->C:Lcom/facebook/graphql/enums/GraphQLGender;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->A:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 204208
    iget-object v0, p1, LX/3dL;->D:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->B:Lcom/facebook/graphql/model/GraphQLPage;

    .line 204209
    iget-object v0, p1, LX/3dL;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->C:Ljava/lang/String;

    .line 204210
    iget-object v0, p1, LX/3dL;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aT:Ljava/lang/String;

    .line 204211
    iget-object v0, p1, LX/3dL;->G:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aF:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 204212
    iget-boolean v0, p1, LX/3dL;->H:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->D:Z

    .line 204213
    iget-boolean v0, p1, LX/3dL;->I:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->E:Z

    .line 204214
    iget-boolean v0, p1, LX/3dL;->J:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->F:Z

    .line 204215
    iget-boolean v0, p1, LX/3dL;->K:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->G:Z

    .line 204216
    iget-boolean v0, p1, LX/3dL;->L:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->H:Z

    .line 204217
    iget-boolean v0, p1, LX/3dL;->M:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aJ:Z

    .line 204218
    iget-boolean v0, p1, LX/3dL;->N:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->I:Z

    .line 204219
    iget-boolean v0, p1, LX/3dL;->O:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->J:Z

    .line 204220
    iget-boolean v0, p1, LX/3dL;->P:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->K:Z

    .line 204221
    iget-boolean v0, p1, LX/3dL;->Q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->L:Z

    .line 204222
    iget-boolean v0, p1, LX/3dL;->R:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->M:Z

    .line 204223
    iget-boolean v0, p1, LX/3dL;->S:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aR:Z

    .line 204224
    iget-boolean v0, p1, LX/3dL;->T:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->N:Z

    .line 204225
    iget-boolean v0, p1, LX/3dL;->U:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->O:Z

    .line 204226
    iget-boolean v0, p1, LX/3dL;->V:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->P:Z

    .line 204227
    iget-object v0, p1, LX/3dL;->W:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Q:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 204228
    iget-object v0, p1, LX/3dL;->X:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->R:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 204229
    iget-object v0, p1, LX/3dL;->Y:Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aN:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 204230
    iget-object v0, p1, LX/3dL;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204231
    iget-object v0, p1, LX/3dL;->aa:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204232
    iget-wide v0, p1, LX/3dL;->ab:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->S:J

    .line 204233
    iget-wide v0, p1, LX/3dL;->ac:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->T:D

    .line 204234
    iget-object v0, p1, LX/3dL;->ad:Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aL:Lcom/facebook/graphql/model/GraphQLUser;

    .line 204235
    iget-object v0, p1, LX/3dL;->ae:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->U:Ljava/lang/String;

    .line 204236
    iget-object v0, p1, LX/3dL;->af:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->V:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 204237
    iget-object v0, p1, LX/3dL;->ag:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->W:Ljava/lang/String;

    .line 204238
    iget-object v0, p1, LX/3dL;->ah:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->X:Ljava/util/List;

    .line 204239
    iget-object v0, p1, LX/3dL;->ai:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Y:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 204240
    iget-boolean v0, p1, LX/3dL;->aj:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aZ:Z

    .line 204241
    iget-object v0, p1, LX/3dL;->ak:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Z:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 204242
    iget-object v0, p1, LX/3dL;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aP:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204243
    iget-object v0, p1, LX/3dL;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204244
    iget-object v0, p1, LX/3dL;->an:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aQ:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 204245
    iget-object v0, p1, LX/3dL;->ao:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 204246
    iget-object v0, p1, LX/3dL;->ap:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204247
    iget-object v0, p1, LX/3dL;->aq:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ac:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 204248
    iget-object v0, p1, LX/3dL;->ar:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ad:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 204249
    iget-object v0, p1, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204250
    iget-wide v0, p1, LX/3dL;->at:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->af:J

    .line 204251
    iget-boolean v0, p1, LX/3dL;->au:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ag:Z

    .line 204252
    iget-object v0, p1, LX/3dL;->av:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ah:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 204253
    iget-object v0, p1, LX/3dL;->aw:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ai:Ljava/lang/String;

    .line 204254
    iget-object v0, p1, LX/3dL;->ax:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aj:Ljava/lang/String;

    .line 204255
    iget-object v0, p1, LX/3dL;->ay:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ak:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 204256
    iget-object v0, p1, LX/3dL;->az:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->al:Ljava/lang/String;

    .line 204257
    iget-object v0, p1, LX/3dL;->aA:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->am:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 204258
    iget-object v0, p1, LX/3dL;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204259
    iget-object v0, p1, LX/3dL;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204260
    iget-object v0, p1, LX/3dL;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204261
    iget-object v0, p1, LX/3dL;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ap:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204262
    iget-object v0, p1, LX/3dL;->aF:Lcom/facebook/graphql/model/GraphQLName;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aq:Lcom/facebook/graphql/model/GraphQLName;

    .line 204263
    iget-object v0, p1, LX/3dL;->aG:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ar:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 204264
    iget-object v0, p1, LX/3dL;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204265
    iget v0, p1, LX/3dL;->aI:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->at:I

    .line 204266
    iget-object v0, p1, LX/3dL;->aJ:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aV:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 204267
    iget-object v0, p1, LX/3dL;->aK:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->au:Ljava/lang/String;

    .line 204268
    iget-object v0, p1, LX/3dL;->aL:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->av:Ljava/lang/String;

    .line 204269
    iget-boolean v0, p1, LX/3dL;->aM:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aw:Z

    .line 204270
    iget-boolean v0, p1, LX/3dL;->aN:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ax:Z

    .line 204271
    iget-object v0, p1, LX/3dL;->aO:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ay:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 204272
    iget-boolean v0, p1, LX/3dL;->aP:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->az:Z

    .line 204273
    iget-boolean v0, p1, LX/3dL;->aQ:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aA:Z

    .line 204274
    iget-object v0, p1, LX/3dL;->aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204275
    iget-object v0, p1, LX/3dL;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204276
    iget-boolean v0, p1, LX/3dL;->aT:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aD:Z

    .line 204277
    iget-boolean v0, p1, LX/3dL;->aU:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aY:Z

    .line 204278
    iget-wide v0, p1, LX/3dL;->aV:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aE:D

    .line 204279
    iget-object v0, p1, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 204280
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 3

    .prologue
    .line 204173
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLActor;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 204174
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 204175
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 204176
    if-eqz v0, :cond_0

    .line 204177
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 204178
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;)V
    .locals 3

    .prologue
    .line 204167
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLActor;->R:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 204168
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 204169
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 204170
    if-eqz v0, :cond_0

    .line 204171
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x27

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 204172
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 3

    .prologue
    .line 204161
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLActor;->ak:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 204162
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 204163
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 204164
    if-eqz v0, :cond_0

    .line 204165
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x3a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 204166
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 3

    .prologue
    .line 204155
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLActor;->ar:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 204156
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 204157
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 204158
    if-eqz v0, :cond_0

    .line 204159
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x41

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 204160
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 204149
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLActor;->n:Z

    .line 204150
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 204151
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 204152
    if-eqz v0, :cond_0

    .line 204153
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 204154
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 204143
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLActor;->az:Z

    .line 204144
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 204145
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 204146
    if-eqz v0, :cond_0

    .line 204147
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x49

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 204148
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 204137
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLActor;->aA:Z

    .line 204138
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 204139
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 204140
    if-eqz v0, :cond_0

    .line 204141
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x4a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 204142
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204134
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->v:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204135
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->v:Ljava/util/List;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->v:Ljava/util/List;

    .line 204136
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->v:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204131
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204132
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->w:Ljava/lang/String;

    .line 204133
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final C()Lcom/facebook/graphql/model/GraphQLEventsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204128
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->x:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204129
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->x:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->x:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    .line 204130
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->x:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    return-object v0
.end method

.method public final D()Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204077
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204078
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 204079
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204122
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204123
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 204124
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/enums/GraphQLGender;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204119
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->A:Lcom/facebook/graphql/enums/GraphQLGender;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204120
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->A:Lcom/facebook/graphql/enums/GraphQLGender;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGender;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGender;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->A:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 204121
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->A:Lcom/facebook/graphql/enums/GraphQLGender;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204116
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->B:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204117
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->B:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->B:Lcom/facebook/graphql/model/GraphQLPage;

    .line 204118
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->B:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final H()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204113
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->C:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204114
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->C:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->C:Ljava/lang/String;

    .line 204115
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final I()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204110
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204111
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204112
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->D:Z

    return v0
.end method

.method public final J()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204107
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204108
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204109
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->E:Z

    return v0
.end method

.method public final K()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 204104
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204105
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204106
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->F:Z

    return v0
.end method

.method public final L()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204101
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204102
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204103
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->G:Z

    return v0
.end method

.method public final M()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204098
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204099
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204100
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->H:Z

    return v0
.end method

.method public final N()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204095
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204096
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204097
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->I:Z

    return v0
.end method

.method public final O()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204092
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204093
    const/4 v0, 0x3

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204094
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->J:Z

    return v0
.end method

.method public final P()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204089
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204090
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204091
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->K:Z

    return v0
.end method

.method public final Q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204086
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204087
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204088
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->L:Z

    return v0
.end method

.method public final R()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204083
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204084
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204085
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->M:Z

    return v0
.end method

.method public final S()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204080
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204081
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204082
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->N:Z

    return v0
.end method

.method public final T()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x4

    .line 204348
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204349
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204350
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->O:Z

    return v0
.end method

.method public final U()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204287
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204288
    const/4 v0, 0x4

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204289
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->P:Z

    return v0
.end method

.method public final V()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204351
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Q:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204352
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Q:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Q:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 204353
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Q:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    return-object v0
.end method

.method public final W()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204354
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->R:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204355
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->R:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->R:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 204356
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->R:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    return-object v0
.end method

.method public final X()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204357
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204358
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204359
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->S:J

    return-wide v0
.end method

.method public final Y()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204360
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204361
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204362
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->T:D

    return-wide v0
.end method

.method public final Z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204363
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204364
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->U:Ljava/lang/String;

    const/16 v1, 0x2a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->U:Ljava/lang/String;

    .line 204365
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->U:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 58

    .prologue
    .line 204631
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 204632
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 204633
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->k()Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 204634
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->l()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 204635
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->m()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 204636
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 204637
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->o()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 204638
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->w()LX/0Px;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 204639
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->y()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 204640
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 204641
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->A()LX/0Px;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/util/List;)I

    move-result v11

    .line 204642
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->B()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 204643
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->C()Lcom/facebook/graphql/model/GraphQLEventsConnection;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 204644
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->D()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 204645
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->G()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 204646
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 204647
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->V()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 204648
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->Z()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 204649
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 204650
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 204651
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ac()LX/0Px;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v21

    .line 204652
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ad()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 204653
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ae()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 204654
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->af()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 204655
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 204656
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ah()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 204657
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ai()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 204658
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 204659
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->am()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 204660
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->an()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 204661
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ao()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 204662
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aq()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 204663
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ar()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 204664
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 204665
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 204666
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->au()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 204667
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->av()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 204668
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ax()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 204669
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v39

    .line 204670
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aA()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 204671
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 204672
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 204673
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 204674
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aL()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    .line 204675
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 204676
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aN()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 204677
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aP()Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v47

    .line 204678
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aQ()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 204679
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aR()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 204680
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aS()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 204681
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 204682
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aX()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 204683
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aY()Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v53

    .line 204684
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v54

    .line 204685
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ba()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 204686
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 204687
    const/16 v57, 0x64

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 204688
    const/16 v57, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 204689
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 204690
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 204691
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 204692
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 204693
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 204694
    const/4 v2, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->p()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204695
    const/4 v2, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->q()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204696
    const/16 v2, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->r()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204697
    const/16 v2, 0x9

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->s()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204698
    const/16 v2, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->t()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204699
    const/16 v2, 0xb

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->u()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204700
    const/16 v2, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->v()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204701
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 204702
    const/16 v3, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->x()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 204703
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 204704
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 204705
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 204706
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 204707
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 204708
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 204709
    const/16 v3, 0x15

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 204710
    const/16 v3, 0x16

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->F()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 204711
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 204712
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204713
    const/16 v2, 0x19

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->I()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204714
    const/16 v2, 0x1a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->J()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204715
    const/16 v2, 0x1b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->K()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204716
    const/16 v2, 0x1c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->L()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204717
    const/16 v2, 0x1d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->M()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204718
    const/16 v2, 0x1e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->N()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204719
    const/16 v2, 0x1f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->O()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204720
    const/16 v2, 0x20

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->P()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204721
    const/16 v2, 0x21

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->Q()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204722
    const/16 v2, 0x22

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204723
    const/16 v2, 0x23

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->S()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204724
    const/16 v2, 0x24

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->T()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204725
    const/16 v2, 0x25

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->U()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204726
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204727
    const/16 v3, 0x27

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->W()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 204728
    const/16 v3, 0x28

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->X()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 204729
    const/16 v3, 0x29

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->Y()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 204730
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204731
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204732
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204733
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204734
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204735
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204736
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204737
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204738
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204739
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204740
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204741
    const/16 v3, 0x35

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ak()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 204742
    const/16 v2, 0x36

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->al()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204743
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204744
    const/16 v2, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204745
    const/16 v2, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204746
    const/16 v3, 0x3a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v2, v4, :cond_4

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 204747
    const/16 v2, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204748
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204749
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204750
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204751
    const/16 v2, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204752
    const/16 v2, 0x40

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204753
    const/16 v3, 0x41

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aw()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v2, v4, :cond_5

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 204754
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204755
    const/16 v2, 0x43

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ay()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 204756
    const/16 v2, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204757
    const/16 v2, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204758
    const/16 v2, 0x46

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204759
    const/16 v2, 0x47

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204760
    const/16 v2, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204761
    const/16 v2, 0x49

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204762
    const/16 v2, 0x4a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204763
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204764
    const/16 v2, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204765
    const/16 v2, 0x4d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aI()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204766
    const/16 v3, 0x4e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aJ()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 204767
    const/16 v3, 0x4f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aK()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    if-ne v2, v4, :cond_6

    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 204768
    const/16 v2, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204769
    const/16 v2, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204770
    const/16 v2, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204771
    const/16 v2, 0x53

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aO()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204772
    const/16 v2, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204773
    const/16 v2, 0x55

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204774
    const/16 v2, 0x56

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204775
    const/16 v2, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204776
    const/16 v2, 0x58

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aT()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 204777
    const/16 v2, 0x59

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204778
    const/16 v3, 0x5a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aV()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne v2, v4, :cond_7

    const/4 v2, 0x0

    :goto_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 204779
    const/16 v2, 0x5b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aW()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204780
    const/16 v2, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204781
    const/16 v2, 0x5d

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204782
    const/16 v2, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204783
    const/16 v2, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204784
    const/16 v2, 0x60

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->bb()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204785
    const/16 v2, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 204786
    const/16 v2, 0x62

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->bd()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204787
    const/16 v2, 0x63

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->be()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 204788
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 204789
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 204790
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 204791
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    goto/16 :goto_1

    .line 204792
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->F()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v2

    goto/16 :goto_2

    .line 204793
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->W()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v2

    goto/16 :goto_3

    .line 204794
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    goto/16 :goto_4

    .line 204795
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aw()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    goto/16 :goto_5

    .line 204796
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aK()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    move-result-object v2

    goto/16 :goto_6

    .line 204797
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLActor;->aV()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v2

    goto/16 :goto_7
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 204366
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 204367
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aR()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 204368
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aR()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 204369
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aR()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 204370
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204371
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aM:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 204372
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->k()Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 204373
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->k()Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    .line 204374
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->k()Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 204375
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204376
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->f:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    .line 204377
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->m()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 204378
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->m()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 204379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->m()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 204380
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204381
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 204382
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 204383
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204384
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 204385
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204386
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204387
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aN()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 204388
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aN()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDate;

    .line 204389
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aN()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 204390
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204391
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aI:Lcom/facebook/graphql/model/GraphQLDate;

    .line 204392
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->o()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 204393
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->o()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 204394
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->o()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 204395
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204396
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->j:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 204397
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->y()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 204398
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->y()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 204399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->y()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 204400
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204401
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 204402
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 204403
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 204404
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 204405
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204406
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->u:Lcom/facebook/graphql/model/GraphQLPage;

    .line 204407
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->C()Lcom/facebook/graphql/model/GraphQLEventsConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 204408
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->C()Lcom/facebook/graphql/model/GraphQLEventsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventsConnection;

    .line 204409
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->C()Lcom/facebook/graphql/model/GraphQLEventsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 204410
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204411
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->x:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    .line 204412
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->D()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 204413
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->D()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 204414
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->D()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 204415
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204416
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 204417
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->G()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 204418
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->G()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 204419
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->G()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 204420
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204421
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->B:Lcom/facebook/graphql/model/GraphQLPage;

    .line 204422
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->V()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 204423
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->V()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 204424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->V()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 204425
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204426
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->Q:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 204427
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aS()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 204428
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aS()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 204429
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aS()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 204430
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204431
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aN:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 204432
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 204433
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 204434
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 204435
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204436
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204437
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 204438
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 204439
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 204440
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204441
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204442
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aQ()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 204443
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aQ()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 204444
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aQ()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 204445
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204446
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aL:Lcom/facebook/graphql/model/GraphQLUser;

    .line 204447
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 204448
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 204449
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 204450
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204451
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->V:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 204452
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ad()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 204453
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ad()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 204454
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ad()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 204455
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204456
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->Y:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 204457
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ae()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 204458
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ae()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 204459
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ae()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 204460
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204461
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->Z:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 204462
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 204463
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204464
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 204465
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204466
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aP:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204467
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aX()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 204468
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aX()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204469
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aX()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 204470
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204471
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204472
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->af()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 204473
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->af()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 204474
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->af()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 204475
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204476
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 204477
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 204478
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 204479
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 204480
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204481
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204482
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ah()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 204483
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ah()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 204484
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ah()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 204485
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204486
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->ac:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 204487
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ai()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 204488
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ai()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 204489
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ai()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 204490
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204491
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->ad:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 204492
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 204493
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 204494
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 204495
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204496
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204497
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->am()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 204498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->am()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 204499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->am()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 204500
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204501
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->ah:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 204502
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ar()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 204503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ar()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 204504
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ar()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 204505
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204506
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->am:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 204507
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 204508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 204510
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204511
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204512
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 204513
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 204514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 204515
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204516
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204517
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 204518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 204519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 204520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204521
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204522
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->au()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 204523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->au()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 204524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->au()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 204525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204526
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->ap:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204527
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->av()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 204528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->av()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLName;

    .line 204529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->av()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 204530
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204531
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aq:Lcom/facebook/graphql/model/GraphQLName;

    .line 204532
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ax()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 204533
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ax()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ax()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 204535
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204536
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204537
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ba()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 204538
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ba()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 204539
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ba()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 204540
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204541
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aV:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 204542
    :cond_22
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 204543
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 204544
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 204545
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204546
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->ay:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 204547
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 204548
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204549
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 204550
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204551
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204552
    :cond_24
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 204553
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 204555
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 204556
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLActor;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204557
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 204558
    if-nez v1, :cond_26

    :goto_0
    return-object p0

    :cond_26
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 204559
    new-instance v0, LX/4Vn;

    invoke-direct {v0, p1}, LX/4Vn;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 204561
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 204562
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->k:Z

    .line 204563
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->l:Z

    .line 204564
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->m:Z

    .line 204565
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->n:Z

    .line 204566
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->o:Z

    .line 204567
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->p:Z

    .line 204568
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->q:Z

    .line 204569
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->s:D

    .line 204570
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->D:Z

    .line 204571
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->E:Z

    .line 204572
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->F:Z

    .line 204573
    const/16 v0, 0x1c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->G:Z

    .line 204574
    const/16 v0, 0x1d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->H:Z

    .line 204575
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->I:Z

    .line 204576
    const/16 v0, 0x1f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->J:Z

    .line 204577
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->K:Z

    .line 204578
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->L:Z

    .line 204579
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->M:Z

    .line 204580
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->N:Z

    .line 204581
    const/16 v0, 0x24

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->O:Z

    .line 204582
    const/16 v0, 0x25

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->P:Z

    .line 204583
    const/16 v0, 0x28

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->S:J

    .line 204584
    const/16 v0, 0x29

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->T:D

    .line 204585
    const/16 v0, 0x35

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->af:J

    .line 204586
    const/16 v0, 0x36

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ag:Z

    .line 204587
    const/16 v0, 0x43

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->at:I

    .line 204588
    const/16 v0, 0x46

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aw:Z

    .line 204589
    const/16 v0, 0x47

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ax:Z

    .line 204590
    const/16 v0, 0x49

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->az:Z

    .line 204591
    const/16 v0, 0x4a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aA:Z

    .line 204592
    const/16 v0, 0x4d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aD:Z

    .line 204593
    const/16 v0, 0x4e

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aE:D

    .line 204594
    const/16 v0, 0x53

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aJ:Z

    .line 204595
    const/16 v0, 0x58

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aO:I

    .line 204596
    const/16 v0, 0x5b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aR:Z

    .line 204597
    const/16 v0, 0x60

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aW:Z

    .line 204598
    const/16 v0, 0x62

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aY:Z

    .line 204599
    const/16 v0, 0x63

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aZ:Z

    .line 204600
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 204601
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204602
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->s()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 204603
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 204604
    const/16 v0, 0x9

    iput v0, p2, LX/18L;->c:I

    .line 204605
    :goto_0
    return-void

    .line 204606
    :cond_0
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204607
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 204608
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 204609
    const/16 v0, 0x15

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 204610
    :cond_1
    const-string v0, "live_video_subscription_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204611
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->W()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 204612
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 204613
    const/16 v0, 0x27

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 204614
    :cond_2
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 204615
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 204616
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 204617
    const/16 v0, 0x3a

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 204618
    :cond_3
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 204619
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aw()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 204620
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 204621
    const/16 v0, 0x41

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 204622
    :cond_4
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 204623
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 204624
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 204625
    const/16 v0, 0x49

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 204626
    :cond_5
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 204627
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 204628
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 204629
    const/16 v0, 0x4a

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 204630
    :cond_6
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 204330
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204331
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLActor;->a(Z)V

    .line 204332
    :cond_0
    :goto_0
    return-void

    .line 204333
    :cond_1
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204334
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLActor;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0

    .line 204335
    :cond_2
    const-string v0, "live_video_subscription_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 204336
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLActor;->a(Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;)V

    goto :goto_0

    .line 204337
    :cond_3
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 204338
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLActor;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    goto :goto_0

    .line 204339
    :cond_4
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 204340
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLActor;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto :goto_0

    .line 204341
    :cond_5
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 204342
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLActor;->b(Z)V

    goto :goto_0

    .line 204343
    :cond_6
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204344
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLActor;->c(Z)V

    goto :goto_0
.end method

.method public final aA()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204345
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->av:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204346
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->av:Ljava/lang/String;

    const/16 v1, 0x45

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->av:Ljava/lang/String;

    .line 204347
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->av:Ljava/lang/String;

    return-object v0
.end method

.method public final aB()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204327
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204328
    const/16 v0, 0x8

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204329
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aw:Z

    return v0
.end method

.method public final aC()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204324
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204325
    const/16 v0, 0x8

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204326
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ax:Z

    return v0
.end method

.method public final aD()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204321
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ay:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204322
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ay:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x48

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ay:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 204323
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ay:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final aE()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204318
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204319
    const/16 v0, 0x9

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204320
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->az:Z

    return v0
.end method

.method public final aF()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204315
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204316
    const/16 v0, 0x9

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204317
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aA:Z

    return v0
.end method

.method public final aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204311
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204312
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x4b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204313
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204308
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204309
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x4c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204310
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aI()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204305
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204306
    const/16 v0, 0x9

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204307
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aD:Z

    return v0
.end method

.method public final aJ()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204302
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204303
    const/16 v0, 0x9

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204304
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aE:D

    return-wide v0
.end method

.method public final aK()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204299
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aF:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204300
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aF:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    const/16 v1, 0x4f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aF:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 204301
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aF:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    return-object v0
.end method

.method public final aL()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204296
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aG:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204297
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aG:Ljava/lang/String;

    const/16 v1, 0x50

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aG:Ljava/lang/String;

    .line 204298
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aG:Ljava/lang/String;

    return-object v0
.end method

.method public final aM()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204293
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204294
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x51

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204295
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aN()Lcom/facebook/graphql/model/GraphQLDate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204290
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aI:Lcom/facebook/graphql/model/GraphQLDate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204291
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aI:Lcom/facebook/graphql/model/GraphQLDate;

    const/16 v1, 0x52

    const-class v2, Lcom/facebook/graphql/model/GraphQLDate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aI:Lcom/facebook/graphql/model/GraphQLDate;

    .line 204292
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aI:Lcom/facebook/graphql/model/GraphQLDate;

    return-object v0
.end method

.method public final aO()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204125
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204126
    const/16 v0, 0xa

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204127
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aJ:Z

    return v0
.end method

.method public final aP()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204284
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aK:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204285
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aK:Ljava/lang/String;

    const/16 v1, 0x54

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aK:Ljava/lang/String;

    .line 204286
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aK:Ljava/lang/String;

    return-object v0
.end method

.method public final aQ()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203941
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aL:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203942
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aL:Lcom/facebook/graphql/model/GraphQLUser;

    const/16 v1, 0x55

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aL:Lcom/facebook/graphql/model/GraphQLUser;

    .line 203943
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aL:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method public final aR()Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203983
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aM:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203984
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aM:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    const/16 v1, 0x56

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aM:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 203985
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aM:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    return-object v0
.end method

.method public final aS()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203980
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aN:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203981
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aN:Lcom/facebook/graphql/model/GraphQLLocation;

    const/16 v1, 0x57

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aN:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 203982
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aN:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method public final aT()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203977
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 203978
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 203979
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aO:I

    return v0
.end method

.method public final aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203974
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aP:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203975
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aP:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x59

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aP:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203976
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aP:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aV()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203971
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aQ:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203972
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aQ:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aQ:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 203973
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aQ:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    return-object v0
.end method

.method public final aW()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203968
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 203969
    const/16 v0, 0xb

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 203970
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aR:Z

    return v0
.end method

.method public final aX()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203965
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203966
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x5c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203967
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aY()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203962
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aT:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203963
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aT:Ljava/lang/String;

    const/16 v1, 0x5d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aT:Ljava/lang/String;

    .line 203964
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aT:Ljava/lang/String;

    return-object v0
.end method

.method public final aZ()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203989
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203990
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203991
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203959
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->V:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203960
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->V:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->V:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 203961
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->V:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    return-object v0
.end method

.method public final ab()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203956
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->W:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203957
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->W:Ljava/lang/String;

    const/16 v1, 0x2c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->W:Ljava/lang/String;

    .line 203958
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->W:Ljava/lang/String;

    return-object v0
.end method

.method public final ac()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203953
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->X:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203954
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->X:Ljava/util/List;

    const/16 v1, 0x2d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->X:Ljava/util/List;

    .line 203955
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->X:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final ad()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203950
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Y:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203951
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Y:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Y:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 203952
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Y:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    return-object v0
.end method

.method public final ae()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203947
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Z:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203948
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Z:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Z:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 203949
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->Z:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    return-object v0
.end method

.method public final af()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203944
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203945
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 203946
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final ag()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203899
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203900
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203901
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ah()Lcom/facebook/graphql/model/GraphQLProfileBadge;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203905
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ac:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203906
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ac:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfileBadge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileBadge;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ac:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 203907
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ac:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    return-object v0
.end method

.method public final ai()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203908
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ad:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203909
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ad:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ad:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 203910
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ad:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method public final aj()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203911
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203912
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203913
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ak()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203914
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 203915
    const/4 v0, 0x6

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 203916
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->af:J

    return-wide v0
.end method

.method public final al()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x6

    .line 203917
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 203918
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 203919
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ag:Z

    return v0
.end method

.method public final am()Lcom/facebook/graphql/model/GraphQLProfileVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203920
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ah:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203921
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ah:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfileVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ah:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 203922
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ah:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    return-object v0
.end method

.method public final an()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203923
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ai:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203924
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ai:Ljava/lang/String;

    const/16 v1, 0x38

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ai:Ljava/lang/String;

    .line 203925
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ai:Ljava/lang/String;

    return-object v0
.end method

.method public final ao()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203926
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aj:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203927
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aj:Ljava/lang/String;

    const/16 v1, 0x39

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aj:Ljava/lang/String;

    .line 203928
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aj:Ljava/lang/String;

    return-object v0
.end method

.method public final ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203929
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ak:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203930
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ak:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/16 v1, 0x3a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ak:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 203931
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ak:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method

.method public final aq()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203932
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->al:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203933
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->al:Ljava/lang/String;

    const/16 v1, 0x3b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->al:Ljava/lang/String;

    .line 203934
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->al:Ljava/lang/String;

    return-object v0
.end method

.method public final ar()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203935
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->am:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203936
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->am:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->am:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 203937
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->am:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    return-object v0
.end method

.method public final as()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203938
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->an:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203939
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->an:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203940
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->an:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final at()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203902
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203903
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203904
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final au()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204031
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ap:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204032
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ap:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ap:Lcom/facebook/graphql/model/GraphQLImage;

    .line 204033
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ap:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final av()Lcom/facebook/graphql/model/GraphQLName;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204074
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aq:Lcom/facebook/graphql/model/GraphQLName;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204075
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aq:Lcom/facebook/graphql/model/GraphQLName;

    const/16 v1, 0x40

    const-class v2, Lcom/facebook/graphql/model/GraphQLName;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLName;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aq:Lcom/facebook/graphql/model/GraphQLName;

    .line 204076
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aq:Lcom/facebook/graphql/model/GraphQLName;

    return-object v0
.end method

.method public final aw()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204071
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ar:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204072
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ar:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ar:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 204073
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->ar:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final ax()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204068
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204069
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204070
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final ay()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204065
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204066
    const/16 v0, 0x8

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204067
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->at:I

    return v0
.end method

.method public final az()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204062
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->au:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204063
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->au:Ljava/lang/String;

    const/16 v1, 0x44

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->au:Ljava/lang/String;

    .line 204064
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->au:Ljava/lang/String;

    return-object v0
.end method

.method public final ba()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204059
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aV:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204060
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aV:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    const/16 v1, 0x5f

    const-class v2, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aV:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 204061
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aV:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    return-object v0
.end method

.method public final bb()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204056
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204057
    const/16 v0, 0xc

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204058
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aW:Z

    return v0
.end method

.method public final bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204053
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204054
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x61

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204055
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final bd()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204050
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204051
    const/16 v0, 0xc

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204052
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aY:Z

    return v0
.end method

.method public final be()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204047
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204048
    const/16 v0, 0xc

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204049
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->aZ:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 204046
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204040
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 204041
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 204042
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLActor;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 204043
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 204044
    const/4 v0, 0x0

    .line 204045
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPageAdminInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204037
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->f:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204038
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->f:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->f:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    .line 204039
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->f:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204034
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204035
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->g:Ljava/lang/String;

    .line 204036
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203986
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203987
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 203988
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204028
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204029
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 204030
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204025
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->j:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204026
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->j:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->j:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 204027
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->j:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    return-object v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204022
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204023
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204024
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->k:Z

    return v0
.end method

.method public final q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204019
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204020
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204021
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->l:Z

    return v0
.end method

.method public final r()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204016
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204017
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204018
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->m:Z

    return v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 204013
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204014
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204015
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->n:Z

    return v0
.end method

.method public final t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204010
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204011
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204012
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->o:Z

    return v0
.end method

.method public final u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204007
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204008
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204009
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->p:Z

    return v0
.end method

.method public final v()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 204004
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 204005
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204006
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->q:Z

    return v0
.end method

.method public final w()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204001
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->r:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 204002
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->r:Ljava/util/List;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->r:Ljava/util/List;

    .line 204003
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->r:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final x()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203998
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 203999
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 204000
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->s:D

    return-wide v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203995
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203996
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 203997
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203992
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->u:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203993
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->u:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->u:Lcom/facebook/graphql/model/GraphQLPage;

    .line 203994
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActor;->u:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method
