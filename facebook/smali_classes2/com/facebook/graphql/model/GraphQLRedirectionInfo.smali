.class public final Lcom/facebook/graphql/model/GraphQLRedirectionInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLRedirectionInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLRedirectionInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 303583
    const-class v0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 303582
    const-class v0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 303580
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 303581
    return-void
.end method

.method public constructor <init>(LX/4YY;)V
    .locals 1

    .prologue
    .line 303575
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 303576
    iget-object v0, p1, LX/4YY;->b:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->e:Lcom/facebook/graphql/model/GraphQLNode;

    .line 303577
    iget-object v0, p1, LX/4YY;->c:Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->f:Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    .line 303578
    iget-object v0, p1, LX/4YY;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->g:Ljava/lang/String;

    .line 303579
    return-void
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303584
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->e:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303585
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->e:Lcom/facebook/graphql/model/GraphQLNode;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->e:Lcom/facebook/graphql/model/GraphQLNode;

    .line 303586
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->e:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLRedirectionReason;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303572
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->f:Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303573
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->f:Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLRedirectionReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->f:Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    .line 303574
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->f:Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 303562
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 303563
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 303564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 303565
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 303566
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 303567
    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->k()Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLRedirectionReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 303568
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 303569
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 303570
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 303571
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->k()Lcom/facebook/graphql/enums/GraphQLRedirectionReason;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 303554
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 303555
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 303556
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 303557
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 303558
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    .line 303559
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->e:Lcom/facebook/graphql/model/GraphQLNode;

    .line 303560
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 303561
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303551
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303552
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->g:Ljava/lang/String;

    .line 303553
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 303550
    const v0, -0x3ad262a6

    return v0
.end method
