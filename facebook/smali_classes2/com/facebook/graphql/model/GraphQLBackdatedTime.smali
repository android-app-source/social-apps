.class public final Lcom/facebook/graphql/model/GraphQLBackdatedTime;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLBackdatedTime$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLBackdatedTime$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLDateGranularity;

.field public f:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 260799
    const-class v0, Lcom/facebook/graphql/model/GraphQLBackdatedTime$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 260798
    const-class v0, Lcom/facebook/graphql/model/GraphQLBackdatedTime$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 260789
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 260790
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLDateGranularity;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260786
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBackdatedTime;->e:Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260787
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBackdatedTime;->e:Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDateGranularity;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLBackdatedTime;->e:Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    .line 260788
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLBackdatedTime;->e:Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 260791
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 260792
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 260793
    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBackdatedTime;->j()Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDateGranularity;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 260794
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBackdatedTime;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 260795
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 260796
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 260797
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLBackdatedTime;->j()Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260783
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260784
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260785
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLBackdatedTime;->f:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 260780
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 260781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 260782
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 260776
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 260777
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLBackdatedTime;->f:J

    .line 260778
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 260779
    const v0, 0x89d613c

    return v0
.end method
