.class public final Lcom/facebook/graphql/model/GraphQLInstantArticle;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLInstantArticle$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLInstantArticle$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 197249
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstantArticle$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 197250
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstantArticle$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 197159
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 197160
    return-void
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197251
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197252
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 197253
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197263
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->f:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197264
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->f:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->f:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197265
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->f:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197254
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->g:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197255
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->g:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->g:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 197256
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->g:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197257
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->i:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197258
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->i:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->i:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197259
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->i:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197260
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197261
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    .line 197262
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197243
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197244
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->l:Ljava/lang/String;

    .line 197245
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197246
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->m:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197247
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->m:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->m:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197248
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->m:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197240
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197241
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->n:Ljava/lang/String;

    .line 197242
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->n:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197237
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197238
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 197239
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197234
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197235
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->p:Ljava/lang/String;

    .line 197236
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 197206
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 197207
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 197208
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->m()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 197209
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->n()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 197210
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 197211
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->o()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 197212
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 197213
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->p()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 197214
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 197215
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->r()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 197216
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->s()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 197217
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v10

    invoke-static {p1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 197218
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->u()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 197219
    const/16 v12, 0xd

    invoke-virtual {p1, v12}, LX/186;->c(I)V

    .line 197220
    const/4 v12, 0x1

    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 197221
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 197222
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 197223
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 197224
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 197225
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 197226
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 197227
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 197228
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 197229
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 197230
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 197231
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 197232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 197233
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 197163
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 197164
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 197165
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 197166
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 197167
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 197168
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 197169
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->m()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 197170
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->m()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197171
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->m()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 197172
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 197173
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;->f:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197174
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->n()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 197175
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->n()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 197176
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->n()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 197177
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 197178
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;->g:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 197179
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->o()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 197180
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->o()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197181
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->o()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 197182
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 197183
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;->i:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197184
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 197185
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197186
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 197187
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 197188
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197189
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->p()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 197190
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->p()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    .line 197191
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->p()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 197192
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 197193
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    .line 197194
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 197195
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 197196
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 197197
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 197198
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 197199
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->r()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 197200
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->r()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197201
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->r()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 197202
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 197203
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticle;->m:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197204
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 197205
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    :cond_8
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 197161
    const v0, 0x5fcedbf5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197157
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->h:Ljava/lang/String;

    .line 197158
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197153
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 197154
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 197155
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    return-object v0
.end method
