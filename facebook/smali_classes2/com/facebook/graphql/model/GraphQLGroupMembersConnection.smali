.class public final Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupMembersConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupMembersConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupMembersEdge;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 306397
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 306396
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 306394
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 306395
    return-void
.end method

.method public constructor <init>(LX/4Wo;)V
    .locals 1

    .prologue
    .line 306388
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 306389
    iget v0, p1, LX/4Wo;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->e:I

    .line 306390
    iget-object v0, p1, LX/4Wo;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->f:Ljava/util/List;

    .line 306391
    iget-object v0, p1, LX/4Wo;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->g:Ljava/util/List;

    .line 306392
    iget-object v0, p1, LX/4Wo;->e:Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 306393
    return-void
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupMembersEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306385
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306386
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupMembersEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->f:Ljava/util/List;

    .line 306387
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306382
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306383
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 306384
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 306398
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 306399
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 306400
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 306371
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306372
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->k()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 306373
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 306374
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 306375
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 306376
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->a()I

    move-result v3

    invoke-virtual {p1, v4, v3, v4}, LX/186;->a(III)V

    .line 306377
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 306378
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 306379
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 306380
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306381
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 306353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306354
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 306355
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 306356
    if-eqz v1, :cond_0

    .line 306357
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 306358
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->f:Ljava/util/List;

    .line 306359
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 306360
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 306361
    if-eqz v1, :cond_1

    .line 306362
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 306363
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 306364
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 306365
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 306366
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 306367
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 306368
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 306369
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306370
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 306350
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 306351
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->e:I

    .line 306352
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 306349
    const v0, 0x6293c38

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306346
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306347
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->g:Ljava/util/List;

    .line 306348
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
