.class public final Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLAdAccount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I

.field public o:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public p:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262748
    const-class v0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262747
    const-class v0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 262745
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 262746
    return-void
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLAdAccount;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262742
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->e:Lcom/facebook/graphql/model/GraphQLAdAccount;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262743
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->e:Lcom/facebook/graphql/model/GraphQLAdAccount;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLAdAccount;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAdAccount;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->e:Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 262744
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->e:Lcom/facebook/graphql/model/GraphQLAdAccount;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262739
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262740
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 262741
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262736
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262737
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 262738
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262658
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->h:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262659
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->h:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x3

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->h:Lcom/facebook/graphql/model/FeedUnit;

    .line 262660
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->h:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method private n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262733
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262734
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262735
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->i:Z

    return v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262730
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262731
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->j:Ljava/lang/String;

    .line 262732
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262727
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262728
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262729
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->k:I

    return v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262655
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262656
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->l:Ljava/lang/String;

    .line 262657
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262661
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262662
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262663
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private s()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 262664
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262665
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262666
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->n:I

    return v0
.end method

.method private t()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262667
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262668
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262669
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->p:J

    return-wide v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x0

    .line 262670
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262671
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->j()Lcom/facebook/graphql/model/GraphQLAdAccount;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 262672
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->l()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 262673
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->m()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    sget-object v4, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v3, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v3

    .line 262674
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 262675
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 262676
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 262677
    const/16 v7, 0xc

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 262678
    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 262679
    const/4 v7, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->k()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v8, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v7, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262680
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 262681
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 262682
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->n()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 262683
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 262684
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->p()I

    move-result v2

    invoke-virtual {p1, v0, v2, v9}, LX/186;->a(III)V

    .line 262685
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 262686
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 262687
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->s()I

    move-result v2

    invoke-virtual {p1, v0, v2, v9}, LX/186;->a(III)V

    .line 262688
    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->a()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262689
    const/16 v1, 0xb

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->t()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 262690
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262691
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 262692
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->k()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    goto :goto_0

    .line 262693
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->a()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 262694
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262695
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->j()Lcom/facebook/graphql/model/GraphQLAdAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262696
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->j()Lcom/facebook/graphql/model/GraphQLAdAccount;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 262697
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->j()Lcom/facebook/graphql/model/GraphQLAdAccount;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 262698
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 262699
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->e:Lcom/facebook/graphql/model/GraphQLAdAccount;

    .line 262700
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->l()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 262701
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->l()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 262702
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->l()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 262703
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 262704
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->g:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 262705
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->m()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 262706
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->m()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 262707
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->m()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 262708
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 262709
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->h:Lcom/facebook/graphql/model/FeedUnit;

    .line 262710
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 262711
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262712
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 262713
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 262714
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262715
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262716
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262717
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->o:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262718
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->o:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->o:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 262719
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->o:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 262720
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 262721
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->i:Z

    .line 262722
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->k:I

    .line 262723
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->n:I

    .line 262724
    const/16 v0, 0xb

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;->p:J

    .line 262725
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 262726
    const v0, 0x14d92442

    return v0
.end method
