.class public final Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:J

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:I

.field public q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 251977
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 251978
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 251979
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 251980
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x5bdbec3a

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 251981
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->u:LX/0x2;

    .line 251982
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 251983
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->p:I

    .line 251984
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 251985
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 251986
    if-eqz v0, :cond_0

    .line 251987
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 251988
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 251989
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o:Ljava/lang/String;

    .line 251990
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 251991
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 251992
    if-eqz v0, :cond_0

    .line 251993
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 251994
    :cond_0
    return-void
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251995
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251996
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->h:Ljava/lang/String;

    .line 251997
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251998
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251999
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o:Ljava/lang/String;

    .line 252000
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method private x()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 252001
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 252002
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 252003
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->p:I

    return v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252054
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->t:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252055
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->t:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->t:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 252056
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->t:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method


# virtual methods
.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252004
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252005
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->s:Ljava/lang/String;

    .line 252006
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 252007
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252008
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252009
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->g:Ljava/lang/String;

    .line 252010
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 252011
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 252012
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 252013
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->n:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 252014
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 251942
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->u:LX/0x2;

    if-nez v0, :cond_0

    .line 251943
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->u:LX/0x2;

    .line 251944
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->u:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 252053
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 251945
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 251946
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 251947
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 251948
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 251949
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 251950
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->p()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 251951
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 251952
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 251953
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->s()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 251954
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->w()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 251955
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 251956
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    invoke-static {p1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 251957
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->C_()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 251958
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v12

    invoke-static {p1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 251959
    const/16 v13, 0x10

    invoke-virtual {p1, v13}, LX/186;->c(I)V

    .line 251960
    const/4 v13, 0x1

    invoke-virtual {p1, v13, v0}, LX/186;->b(II)V

    .line 251961
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 251962
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 251963
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 251964
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 251965
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 251966
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 251967
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 251968
    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 251969
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 251970
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->x()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 251971
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 251972
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 251973
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 251974
    const/16 v0, 0xf

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 251975
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 251976
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 252015
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 252016
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252017
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252018
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 252019
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 252020
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252021
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 252022
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 252023
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 252024
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 252025
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->t:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 252026
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 252027
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252028
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 252029
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 252030
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252031
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 252032
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252033
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 252034
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 252035
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252036
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 252037
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 252038
    if-eqz v2, :cond_4

    .line 252039
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 252040
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->j:Ljava/util/List;

    move-object v1, v0

    .line 252041
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 252042
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 252044
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 252045
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252046
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 252047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252048
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 252049
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 252050
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252051
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 252052
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 251888
    new-instance v0, LX/4XI;

    invoke-direct {v0, p1}, LX/4XI;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251889
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 251890
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->n:J

    .line 251891
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 251892
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 251893
    const/16 v0, 0x9

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->n:J

    .line 251894
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->p:I

    .line 251895
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 251896
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251897
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 251898
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 251899
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    .line 251900
    :goto_0
    return-void

    .line 251901
    :cond_0
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251902
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->x()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 251903
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 251904
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 251905
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 251906
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251907
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->a(Ljava/lang/String;)V

    .line 251908
    :cond_0
    :goto_0
    return-void

    .line 251909
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251910
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251911
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251912
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 251913
    :goto_0
    return-object v0

    .line 251914
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 251915
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 251916
    const v0, -0x5bdbec3a

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251917
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251918
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->f:Ljava/lang/String;

    .line 251919
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251920
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251921
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251922
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251923
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251924
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251925
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->j:Ljava/util/List;

    .line 251926
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251927
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251928
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251929
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251930
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251931
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251932
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251933
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251934
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->m:Ljava/lang/String;

    .line 251935
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251936
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251937
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251938
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251939
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251940
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251941
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
