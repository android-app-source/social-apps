.class public final Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 200385
    const-class v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 200386
    const-class v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 200387
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 200388
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x72442839

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 200389
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->t:LX/0x2;

    .line 200390
    return-void
.end method

.method public constructor <init>(LX/4Yf;)V
    .locals 2

    .prologue
    .line 200391
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 200392
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x72442839

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 200393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->t:LX/0x2;

    .line 200394
    iget-object v0, p1, LX/4Yf;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->f:Ljava/util/List;

    .line 200395
    iget-object v0, p1, LX/4Yf;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g:Ljava/lang/String;

    .line 200396
    iget-object v0, p1, LX/4Yf;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->h:Ljava/lang/String;

    .line 200397
    iget-wide v0, p1, LX/4Yf;->e:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->i:J

    .line 200398
    iget-object v0, p1, LX/4Yf;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->j:Ljava/lang/String;

    .line 200399
    iget-object v0, p1, LX/4Yf;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->k:Ljava/lang/String;

    .line 200400
    iget v0, p1, LX/4Yf;->h:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->l:I

    .line 200401
    iget-object v0, p1, LX/4Yf;->i:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->m:Ljava/util/List;

    .line 200402
    iget-object v0, p1, LX/4Yf;->j:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 200403
    iget-object v0, p1, LX/4Yf;->k:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->o:Ljava/util/List;

    .line 200404
    iget-object v0, p1, LX/4Yf;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200405
    iget-object v0, p1, LX/4Yf;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->q:Ljava/lang/String;

    .line 200406
    iget-object v0, p1, LX/4Yf;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200407
    iget-object v0, p1, LX/4Yf;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->s:Ljava/lang/String;

    .line 200408
    iget-object v0, p1, LX/4Yf;->p:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->t:LX/0x2;

    .line 200409
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 200410
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->l:I

    .line 200411
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 200412
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 200413
    if-eqz v0, :cond_0

    .line 200414
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 200415
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 200416
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->j:Ljava/lang/String;

    .line 200417
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 200418
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 200419
    if-eqz v0, :cond_0

    .line 200420
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 200421
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 200422
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->k:Ljava/lang/String;

    .line 200423
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 200424
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 200425
    if-eqz v0, :cond_0

    .line 200426
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 200427
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200428
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200429
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->q:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->q:Ljava/lang/String;

    .line 200430
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200431
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200432
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200433
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 200434
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200509
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200510
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->h:Ljava/lang/String;

    .line 200511
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 200435
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 200436
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 200437
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->i:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 200438
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 200439
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 200440
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->t:LX/0x2;

    if-nez v0, :cond_0

    .line 200441
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->t:LX/0x2;

    .line 200442
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->t:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 200443
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 200444
    invoke-static {p0}, LX/2ch;->c(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 200445
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 200446
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->s()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 200447
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 200448
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->E_()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 200449
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->t()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 200450
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->u()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 200451
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->w()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 200452
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 200453
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->y()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v12

    .line 200454
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 200455
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->A()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 200456
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 200457
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->c()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 200458
    const/16 v5, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 200459
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v2}, LX/186;->b(II)V

    .line 200460
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 200461
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 200462
    const/4 v3, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 200463
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 200464
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 200465
    const/4 v2, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->v()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 200466
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 200467
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 200468
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 200469
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 200470
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 200471
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 200472
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 200473
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 200474
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 200475
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 200476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->s()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 200477
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->s()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 200478
    if-eqz v1, :cond_0

    .line 200479
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 200480
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->f:Ljava/util/List;

    .line 200481
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->w()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 200482
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->w()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 200483
    if-eqz v1, :cond_1

    .line 200484
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 200485
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->m:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 200486
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 200487
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 200488
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 200489
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 200490
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 200491
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 200492
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 200493
    if-eqz v2, :cond_3

    .line 200494
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 200495
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->o:Ljava/util/List;

    move-object v1, v0

    .line 200496
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 200497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 200499
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 200500
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200501
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 200502
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 200504
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 200505
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200506
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 200507
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 200508
    new-instance v0, LX/4Yg;

    invoke-direct {v0, p1}, LX/4Yg;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200382
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 200383
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->i:J

    .line 200384
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 200302
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 200303
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->i:J

    .line 200304
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->l:I

    .line 200305
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 200306
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200307
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 200308
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 200309
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 200310
    :goto_0
    return-void

    .line 200311
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200312
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 200313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 200314
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 200315
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200316
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->v()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 200317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 200318
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 200319
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 200320
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200321
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->a(Ljava/lang/String;)V

    .line 200322
    :cond_0
    :goto_0
    return-void

    .line 200323
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200324
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 200325
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200326
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200327
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200328
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->s:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->s:Ljava/lang/String;

    .line 200329
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200330
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200331
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 200332
    :goto_0
    return-object v0

    .line 200333
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 200334
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 200335
    const v0, -0x72442839

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200336
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200337
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g:Ljava/lang/String;

    .line 200338
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 3

    .prologue
    .line 200339
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v0

    .line 200340
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->I_()I

    move-result v1

    .line 200341
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    if-ltz v1, :cond_1

    .line 200342
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 200343
    :goto_0
    move-object v0, v0

    .line 200344
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->l()Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v0, v0

    .line 200345
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()LX/0Px;
    .locals 1

    .prologue
    .line 200346
    invoke-static {p0}, LX/2ch;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/2ci;
    .locals 4

    .prologue
    .line 200347
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->s()LX/0Px;

    move-result-object v0

    .line 200348
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 200349
    :goto_0
    move-object v0, v0

    .line 200350
    const v1, 0x6352ea9a

    invoke-static {v0, v1}, LX/1VX;->a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 200351
    if-eqz v1, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v0}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200352
    :cond_0
    const/4 v0, 0x0

    .line 200353
    :goto_1
    move-object v0, v0

    .line 200354
    return-object v0

    :cond_1
    new-instance v0, LX/2ci;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/2ci;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->w()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic p()Ljava/util/List;
    .locals 1

    .prologue
    .line 200355
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->r()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q()Ljava/util/List;
    .locals 1

    .prologue
    .line 200356
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->n()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 1

    .prologue
    .line 200357
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final s()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200358
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200359
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->f:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->f:Ljava/util/List;

    .line 200360
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200361
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200362
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->j:Ljava/lang/String;

    .line 200363
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200364
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200365
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->k:Ljava/lang/String;

    .line 200366
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final v()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 200367
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 200368
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 200369
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->l:I

    return v0
.end method

.method public final w()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200370
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200371
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->m:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->m:Ljava/util/List;

    .line 200372
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200373
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200374
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 200375
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    return-object v0
.end method

.method public final y()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200376
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->o:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200377
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->o:Ljava/util/List;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->o:Ljava/util/List;

    .line 200378
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->o:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200379
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200380
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200381
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
