.class public final Lcom/facebook/graphql/model/GraphQLAttributionEntry;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAttributionEntry$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAttributionEntry$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLAttributionSource;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 322884
    const-class v0, Lcom/facebook/graphql/model/GraphQLAttributionEntry$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 322871
    const-class v0, Lcom/facebook/graphql/model/GraphQLAttributionEntry$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 322869
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 322870
    return-void
.end method

.method public constructor <init>(LX/4Vs;)V
    .locals 1

    .prologue
    .line 322863
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 322864
    iget-object v0, p1, LX/4Vs;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322865
    iget-object v0, p1, LX/4Vs;->c:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 322866
    iget-object v0, p1, LX/4Vs;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->g:Ljava/lang/String;

    .line 322867
    iget-object v0, p1, LX/4Vs;->e:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->h:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 322868
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLAttributionSource;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322860
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->h:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322861
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->h:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->h:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 322862
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->h:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 322872
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 322873
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 322874
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 322875
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 322876
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 322877
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 322878
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 322879
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 322880
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->l()Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 322881
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 322882
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 322883
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->l()Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 322847
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 322848
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 322849
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322850
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 322851
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAttributionEntry;

    .line 322852
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322853
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 322854
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 322855
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 322856
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAttributionEntry;

    .line 322857
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 322858
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 322859
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322844
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322845
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322846
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 322843
    const v0, -0x2cd1bcad

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322840
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322841
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 322842
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->f:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322837
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322838
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->g:Ljava/lang/String;

    .line 322839
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->g:Ljava/lang/String;

    return-object v0
.end method
