.class public final Lcom/facebook/graphql/model/GraphQLPrivacyScope;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyScope$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyScope$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLIcon;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262566
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262651
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 262649
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 262650
    return-void
.end method

.method public constructor <init>(LX/4YL;)V
    .locals 1

    .prologue
    .line 262635
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 262636
    iget-boolean v0, p1, LX/4YL;->b:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->e:Z

    .line 262637
    iget-object v0, p1, LX/4YL;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->f:Ljava/lang/String;

    .line 262638
    iget-object v0, p1, LX/4YL;->d:Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->g:Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    .line 262639
    iget-object v0, p1, LX/4YL;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->h:Ljava/lang/String;

    .line 262640
    iget-object v0, p1, LX/4YL;->f:Lcom/facebook/graphql/model/GraphQLIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->i:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 262641
    iget-object v0, p1, LX/4YL;->g:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262642
    iget-object v0, p1, LX/4YL;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k:Ljava/lang/String;

    .line 262643
    iget-object v0, p1, LX/4YL;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->l:Ljava/lang/String;

    .line 262644
    iget-object v0, p1, LX/4YL;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 262645
    iget-object v0, p1, LX/4YL;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 262646
    iget-boolean v0, p1, LX/4YL;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o:Z

    .line 262647
    iget-object v0, p1, LX/4YL;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->p:Ljava/lang/String;

    .line 262648
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 262609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262610
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 262611
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 262612
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 262613
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 262614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 262615
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 262616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 262617
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 262618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->r()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 262619
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 262620
    const/16 v10, 0xc

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 262621
    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->a()Z

    move-result v11

    invoke-virtual {p1, v10, v11}, LX/186;->a(IZ)V

    .line 262622
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 262623
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 262624
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 262625
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 262626
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 262627
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 262628
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 262629
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 262630
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 262631
    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->s()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 262632
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 262633
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262634
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 262581
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262582
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262583
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    .line 262584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 262585
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 262586
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->g:Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    .line 262587
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 262588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLIcon;

    .line 262589
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 262590
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 262591
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->i:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 262592
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 262593
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 262594
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 262595
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 262596
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262597
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 262598
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 262599
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 262600
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 262601
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 262602
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->r()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 262603
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->r()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 262604
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->r()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 262605
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 262606
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 262607
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262608
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 262577
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 262578
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->e:Z

    .line 262579
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o:Z

    .line 262580
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 262571
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->p:Ljava/lang/String;

    .line 262572
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 262573
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 262574
    if-eqz v0, :cond_0

    .line 262575
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 262576
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 262568
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262569
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262570
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->e:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 262567
    const v0, -0x1c648c34

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262652
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262653
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->f:Ljava/lang/String;

    .line 262654
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262536
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->g:Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262537
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->g:Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->g:Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    .line 262538
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->g:Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262539
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262540
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->h:Ljava/lang/String;

    .line 262541
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262542
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->i:Lcom/facebook/graphql/model/GraphQLIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262543
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->i:Lcom/facebook/graphql/model/GraphQLIcon;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->i:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 262544
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->i:Lcom/facebook/graphql/model/GraphQLIcon;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262545
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262546
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262547
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262548
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262549
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k:Ljava/lang/String;

    .line 262550
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262551
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262552
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->l:Ljava/lang/String;

    .line 262553
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262554
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262555
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 262556
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262557
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262558
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 262559
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    return-object v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262560
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262561
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262562
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o:Z

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262563
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262564
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->p:Ljava/lang/String;

    .line 262565
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->p:Ljava/lang/String;

    return-object v0
.end method
