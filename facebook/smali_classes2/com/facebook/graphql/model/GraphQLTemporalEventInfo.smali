.class public final Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTemporalEventInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTemporalEventInfo$Serializer;
.end annotation


# instance fields
.field public e:J

.field public f:Lcom/facebook/graphql/model/GraphQLEventThemePhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321046
    const-class v0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321045
    const-class v0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 321043
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321044
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 321034
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321035
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->j()Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 321036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 321037
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 321038
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 321039
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 321040
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 321041
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321042
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 321047
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321048
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321049
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->e:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 321026
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321027
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->j()Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 321028
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->j()Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 321029
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->j()Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 321030
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    .line 321031
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->f:Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 321032
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321033
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 321023
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 321024
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->e:J

    .line 321025
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 321022
    const v0, 0x54994d46

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLEventThemePhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321019
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->f:Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321020
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->f:Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->f:Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    .line 321021
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->f:Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321016
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321017
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->g:Ljava/lang/String;

    .line 321018
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->g:Ljava/lang/String;

    return-object v0
.end method
