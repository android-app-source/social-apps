.class public final Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I

.field public m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation
.end field

.field public s:I

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 252179
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 252162
    const-class v0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 252163
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 252164
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x2ca60061

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 252165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->w:LX/0x2;

    .line 252166
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 252167
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->l:I

    .line 252168
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 252169
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 252170
    if-eqz v0, :cond_0

    .line 252171
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 252172
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 252173
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->k:Ljava/lang/String;

    .line 252174
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 252175
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 252176
    if-eqz v0, :cond_0

    .line 252177
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 252178
    :cond_0
    return-void
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252180
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252181
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->k:Ljava/lang/String;

    .line 252182
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private x()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 252183
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 252184
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 252185
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->l:I

    return v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252186
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252187
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 252188
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252189
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252190
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->o:Ljava/lang/String;

    .line 252191
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252192
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252193
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->i:Ljava/lang/String;

    .line 252194
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 252195
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252196
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252197
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->g:Ljava/lang/String;

    .line 252198
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 252199
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 252200
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 252201
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->h:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 252202
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 252203
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->w:LX/0x2;

    if-nez v0, :cond_0

    .line 252204
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->w:LX/0x2;

    .line 252205
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->w:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 252206
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 20

    .prologue
    .line 252207
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 252208
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 252209
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->E_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 252210
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->C_()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 252211
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 252212
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->w()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 252213
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 252214
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 252215
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->z()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 252216
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 252217
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 252218
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->s()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v16

    .line 252219
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->c()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 252220
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 252221
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 252222
    const/16 v4, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 252223
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 252224
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 252225
    const/4 v3, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 252226
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 252227
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 252228
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 252229
    const/4 v2, 0x6

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->x()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 252230
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 252231
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 252232
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 252233
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 252234
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 252235
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 252236
    const/16 v2, 0xd

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->t()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 252237
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 252238
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 252239
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 252240
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 252241
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 252118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 252119
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252120
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252121
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 252122
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 252123
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252124
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 252125
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252126
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 252127
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 252128
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252129
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 252130
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252131
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 252132
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 252133
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252134
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 252135
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 252136
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 252137
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 252138
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 252139
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 252140
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252141
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 252142
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 252143
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252144
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 252145
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252146
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 252147
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 252148
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252149
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 252150
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252151
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 252152
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 252153
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252154
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->s()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 252155
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->s()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 252156
    if-eqz v2, :cond_7

    .line 252157
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 252158
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r:Ljava/util/List;

    move-object v1, v0

    .line 252159
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 252160
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    :cond_8
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 252161
    new-instance v0, LX/4XH;

    invoke-direct {v0, p1}, LX/4XH;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252089
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 252087
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->h:J

    .line 252088
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 252082
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 252083
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->h:J

    .line 252084
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->l:I

    .line 252085
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->s:I

    .line 252086
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 252072
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252073
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252074
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252075
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    .line 252076
    :goto_0
    return-void

    .line 252077
    :cond_0
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252078
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->x()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252079
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252080
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 252081
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 252067
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252068
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->a(Ljava/lang/String;)V

    .line 252069
    :cond_0
    :goto_0
    return-void

    .line 252070
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252071
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252066
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252063
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252064
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->t:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->t:Ljava/lang/String;

    .line 252065
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 252060
    :goto_0
    return-object v0

    .line 252061
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 252062
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 252057
    const v0, -0x2ca60061

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252090
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252091
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->f:Ljava/lang/String;

    .line 252092
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252093
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252094
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252095
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252096
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252097
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252098
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252099
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252100
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252101
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252102
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252115
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252116
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252117
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252103
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252104
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r:Ljava/util/List;

    .line 252105
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final t()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 252106
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 252107
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 252108
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->s:I

    return v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252109
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252110
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252111
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252112
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252113
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252114
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
