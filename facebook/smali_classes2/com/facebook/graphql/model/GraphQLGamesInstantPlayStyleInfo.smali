.class public final Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 324554
    const-class v0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 324553
    const-class v0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 324551
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 324552
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324548
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324549
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->e:Ljava/lang/String;

    .line 324550
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324545
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324546
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->f:Ljava/lang/String;

    .line 324547
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324542
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->g:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324543
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->g:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->g:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    .line 324544
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->g:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324539
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324540
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->h:Ljava/lang/String;

    .line 324541
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324536
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324537
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->i:Ljava/lang/String;

    .line 324538
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324555
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324556
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->j:Ljava/lang/String;

    .line 324557
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324486
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324487
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->k:Ljava/lang/String;

    .line 324488
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324492
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324493
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->l:Ljava/lang/String;

    .line 324494
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->l:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324495
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324496
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->m:Ljava/lang/String;

    .line 324497
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324498
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324499
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->n:Ljava/lang/String;

    .line 324500
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->n:Ljava/lang/String;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324489
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324490
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->o:Ljava/lang/String;

    .line 324491
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->o:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324501
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->p:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324502
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->p:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->p:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    .line 324503
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->p:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 324504
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 324505
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 324506
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 324507
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 324508
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 324509
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 324510
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 324511
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 324512
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 324513
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->r()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 324514
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->s()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 324515
    const/16 v11, 0xd

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 324516
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 324517
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 324518
    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->k()Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v0

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    if-ne v0, v11, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 324519
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 324520
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 324521
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 324522
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 324523
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 324524
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 324525
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 324526
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 324527
    const/16 v0, 0xc

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->t()Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 324528
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324529
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 324530
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->k()Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v0

    goto :goto_0

    .line 324531
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;->t()Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 324532
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 324533
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324534
    return-object p0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 324535
    const v0, -0x47391ef5

    return v0
.end method
