.class public final Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 319900
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 319899
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 319897
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 319898
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 319889
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 319890
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 319891
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 319892
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 319893
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->j()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319894
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 319895
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 319896
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->j()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 319886
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 319887
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 319888
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319883
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319884
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->e:Ljava/lang/String;

    .line 319885
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 319879
    const v0, -0x71c777aa

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319880
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->f:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319881
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->f:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->f:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 319882
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->f:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    return-object v0
.end method
