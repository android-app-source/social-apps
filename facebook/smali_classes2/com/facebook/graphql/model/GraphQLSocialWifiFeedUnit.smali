.class public final Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements Lcom/facebook/graphql/model/ItemListFeedUnit;
.implements LX/0jR;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 293769
    const-class v0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 293770
    const-class v0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 293771
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 293772
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x3369315c    # -7.90664E7f

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 293773
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->n:LX/0x2;

    .line 293774
    return-void
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293775
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293776
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->i:Ljava/lang/String;

    .line 293777
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293778
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293779
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293780
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 293792
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293781
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293782
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->g:Ljava/lang/String;

    .line 293783
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 293784
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 293785
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 293786
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->h:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 293787
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->n:LX/0x2;

    if-nez v0, :cond_0

    .line 293788
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->n:LX/0x2;

    .line 293789
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->n:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 293790
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 293791
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 293732
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 293733
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 293734
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 293735
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 293736
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->m()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 293737
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 293738
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 293739
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 293740
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 293741
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 293742
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 293743
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 293744
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 293745
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 293746
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 293747
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 293748
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 293749
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 293750
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 293751
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 293752
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->m()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 293753
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->m()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 293754
    if-eqz v1, :cond_3

    .line 293755
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;

    .line 293756
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->j:Ljava/util/List;

    move-object v1, v0

    .line 293757
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293758
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293759
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 293760
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;

    .line 293761
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293762
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 293763
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293764
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 293765
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;

    .line 293766
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293767
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 293768
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293717
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 293715
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->h:J

    .line 293716
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 293729
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 293730
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->h:J

    .line 293731
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293714
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293711
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293712
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->m:Ljava/lang/String;

    .line 293713
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293705
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293706
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 293707
    :goto_0
    return-object v0

    .line 293708
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 293709
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 293718
    const v0, -0x3369315c    # -7.90664E7f

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293719
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293720
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->f:Ljava/lang/String;

    .line 293721
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1

    .prologue
    .line 293722
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293723
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293724
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->j:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->j:Ljava/util/List;

    .line 293725
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293726
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293727
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293728
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final synthetic q()Ljava/util/List;
    .locals 1

    .prologue
    .line 293710
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method
