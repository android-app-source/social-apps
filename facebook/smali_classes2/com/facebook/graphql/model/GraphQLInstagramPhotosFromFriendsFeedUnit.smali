.class public final Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 201539
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 201531
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 201532
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 201533
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x1d51a5c6

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 201534
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->m:LX/0x2;

    .line 201535
    return-void
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201536
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201537
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->j:Ljava/lang/String;

    .line 201538
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201540
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201541
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201542
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 201543
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201544
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201545
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->g:Ljava/lang/String;

    .line 201546
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 201547
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201548
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201549
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->h:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 201550
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 201551
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->m:LX/0x2;

    if-nez v0, :cond_0

    .line 201552
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->m:LX/0x2;

    .line 201553
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->m:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 201554
    const/4 v0, 0x0

    move-object v0, v0

    .line 201555
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 201501
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 201502
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 201503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 201504
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->n()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 201505
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 201506
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 201507
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 201508
    const/4 v2, 0x7

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 201509
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 201510
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 201511
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 201512
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 201513
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 201514
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 201515
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 201516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 201517
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 201518
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 201519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 201520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 201521
    if-eqz v1, :cond_2

    .line 201522
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    .line 201523
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->i:Ljava/util/List;

    move-object v1, v0

    .line 201524
    :goto_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201525
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201526
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 201527
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    .line 201528
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201529
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 201530
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201475
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 201476
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->h:J

    .line 201477
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 201478
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 201479
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->h:J

    .line 201480
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201481
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201482
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201483
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->l:Ljava/lang/String;

    .line 201484
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201485
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201486
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 201487
    :goto_0
    return-object v0

    .line 201488
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 201489
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 201490
    const v0, -0x1d51a5c6

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201491
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201492
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->f:Ljava/lang/String;

    .line 201493
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1

    .prologue
    .line 201500
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201494
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201495
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->i:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->i:Ljava/util/List;

    .line 201496
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic p()Ljava/util/List;
    .locals 1

    .prologue
    .line 201497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 201498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 201499
    return-object v0
.end method
