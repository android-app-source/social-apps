.class public final Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 322368
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 322367
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 322365
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 322366
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 322369
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 322370
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->a()Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 322371
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 322372
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 322373
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 322374
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 322375
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 322376
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 322352
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 322353
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->a()Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 322354
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->a()Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    .line 322355
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->a()Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 322356
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 322357
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->e:Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    .line 322358
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 322359
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322360
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 322361
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 322362
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322363
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 322364
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322349
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->e:Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322350
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->e:Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->e:Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    .line 322351
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->e:Lcom/facebook/graphql/model/GraphQLVideoSocialContextActorsConnection;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 322348
    const v0, -0x74a6c9cb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322345
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322346
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322347
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
