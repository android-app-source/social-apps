.class public final Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo$Serializer;
.end annotation


# instance fields
.field public e:J

.field public f:Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321611
    const-class v0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321610
    const-class v0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 321608
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321609
    return-void
.end method

.method private j()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 321605
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321606
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321607
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;->e:J

    return-wide v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 321612
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321613
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 321614
    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;->j()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 321615
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;->a()Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 321616
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321617
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 321618
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;->a()Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 321602
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321603
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321604
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321599
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;->f:Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321600
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;->f:Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;->f:Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    .line 321601
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;->f:Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 321596
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 321597
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;->e:J

    .line 321598
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 321595
    const v0, -0x110bb90c    # -3.780006E28f

    return v0
.end method
