.class public final Lcom/facebook/graphql/model/GraphQLStorySaveInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStorySaveInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStorySaveInfo$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:I

.field public g:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

.field public h:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public i:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public j:Lcom/facebook/graphql/model/GraphQLSavable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 263350
    const-class v0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 263349
    const-class v0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 263347
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 263348
    return-void
.end method

.method public constructor <init>(LX/4Yv;)V
    .locals 1

    .prologue
    .line 263297
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 263298
    iget-object v0, p1, LX/4Yv;->b:Lcom/facebook/graphql/model/GraphQLSavable;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->j:Lcom/facebook/graphql/model/GraphQLSavable;

    .line 263299
    iget v0, p1, LX/4Yv;->c:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->e:I

    .line 263300
    iget v0, p1, LX/4Yv;->d:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->f:I

    .line 263301
    iget-object v0, p1, LX/4Yv;->e:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->g:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    .line 263302
    iget-object v0, p1, LX/4Yv;->f:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->h:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 263303
    iget-object v0, p1, LX/4Yv;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->i:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 263304
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 263344
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263345
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263346
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 263330
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 263331
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->n()Lcom/facebook/graphql/model/GraphQLSavable;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 263332
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 263333
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->a()I

    move-result v0

    invoke-virtual {p1, v4, v0, v4}, LX/186;->a(III)V

    .line 263334
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->j()I

    move-result v3

    invoke-virtual {p1, v0, v3, v4}, LX/186;->a(III)V

    .line 263335
    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->k()Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    if-ne v0, v4, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v3, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263336
    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->l()Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    if-ne v0, v4, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {p1, v3, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263337
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v3, v4, :cond_2

    :goto_2
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263338
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 263339
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 263340
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 263341
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->k()Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    move-result-object v0

    goto :goto_0

    .line 263342
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->l()Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    move-result-object v0

    goto :goto_1

    .line 263343
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    goto :goto_2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 263322
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 263323
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->n()Lcom/facebook/graphql/model/GraphQLSavable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 263324
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->n()Lcom/facebook/graphql/model/GraphQLSavable;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavable;

    .line 263325
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->n()Lcom/facebook/graphql/model/GraphQLSavable;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 263326
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 263327
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->j:Lcom/facebook/graphql/model/GraphQLSavable;

    .line 263328
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 263329
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 263318
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 263319
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->e:I

    .line 263320
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->f:I

    .line 263321
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 3

    .prologue
    .line 263312
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->i:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 263313
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 263314
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 263315
    if-eqz v0, :cond_0

    .line 263316
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 263317
    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 263311
    const v0, -0x3a83a440

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263308
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263309
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263310
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->f:I

    return v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263305
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->g:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263306
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->g:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->g:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    .line 263307
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->g:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLStorySaveType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263294
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->h:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263295
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->h:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->h:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 263296
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->h:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263291
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->i:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263292
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->i:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->i:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 263293
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->i:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLSavable;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263288
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->j:Lcom/facebook/graphql/model/GraphQLSavable;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263289
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->j:Lcom/facebook/graphql/model/GraphQLSavable;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLSavable;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavable;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->j:Lcom/facebook/graphql/model/GraphQLSavable;

    .line 263290
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->j:Lcom/facebook/graphql/model/GraphQLSavable;

    return-object v0
.end method
