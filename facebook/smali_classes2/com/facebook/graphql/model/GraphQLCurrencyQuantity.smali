.class public final Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCurrencyQuantity$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCurrencyQuantity$Serializer;
.end annotation


# instance fields
.field public e:D
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public f:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 322763
    const-class v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 322762
    const-class v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 322760
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 322761
    return-void
.end method

.method public constructor <init>(LX/4W3;)V
    .locals 2

    .prologue
    .line 322717
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 322718
    iget-wide v0, p1, LX/4W3;->b:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->e:D

    .line 322719
    iget v0, p1, LX/4W3;->c:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->f:I

    .line 322720
    iget-object v0, p1, LX/4W3;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->g:Ljava/lang/String;

    .line 322721
    iget-object v0, p1, LX/4W3;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->h:Ljava/lang/String;

    .line 322722
    iget v0, p1, LX/4W3;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->i:I

    .line 322723
    iget-object v0, p1, LX/4W3;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->j:Ljava/lang/String;

    .line 322724
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 322757
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322758
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322759
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->e:D

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 322744
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 322745
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 322746
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 322747
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 322748
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 322749
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->a()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 322750
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->j()I

    move-result v2

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 322751
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 322752
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 322753
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->m()I

    move-result v2

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 322754
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 322755
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 322756
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 322741
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 322742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 322743
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 322764
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 322765
    const-wide/16 v0, 0x0

    invoke-virtual {p1, p2, v2, v0, v1}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->e:D

    .line 322766
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->f:I

    .line 322767
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->i:I

    .line 322768
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 322740
    const v0, 0x2cee5bdc

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 322737
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322738
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322739
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->f:I

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322734
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322735
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->g:Ljava/lang/String;

    .line 322736
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322731
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322732
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->h:Ljava/lang/String;

    .line 322733
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322728
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322729
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322730
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->i:I

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322725
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322726
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->j:Ljava/lang/String;

    .line 322727
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->j:Ljava/lang/String;

    return-object v0
.end method
