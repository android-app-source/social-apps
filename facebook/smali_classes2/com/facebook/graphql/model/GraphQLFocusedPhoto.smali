.class public final Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFocusedPhoto$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFocusedPhoto$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLVect2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 304875
    const-class v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 304902
    const-class v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 304900
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 304901
    return-void
.end method

.method public constructor <init>(LX/4WQ;)V
    .locals 1

    .prologue
    .line 304896
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 304897
    iget-object v0, p1, LX/4WQ;->b:Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 304898
    iget-object v0, p1, LX/4WQ;->c:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->f:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 304899
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 304903
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 304904
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 304905
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 304906
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 304907
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 304908
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 304909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 304910
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 304883
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 304884
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 304885
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    .line 304886
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 304887
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304888
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 304889
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 304890
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 304891
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 304892
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304893
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->f:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 304894
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 304895
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLVect2;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304880
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304881
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLVect2;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 304882
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 304879
    const v0, 0x1da3a91b

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304876
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->f:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304877
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->f:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->f:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 304878
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->f:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method
