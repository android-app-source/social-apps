.class public final Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327359
    const-class v0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327402
    const-class v0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 327400
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 327401
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 327389
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 327390
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 327391
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 327392
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->k()Lcom/facebook/graphql/model/GraphQLQuantity;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 327393
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 327394
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 327395
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 327396
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 327397
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->l()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 327398
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327399
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 327376
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 327377
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 327378
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 327379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 327380
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;

    .line 327381
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 327382
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->k()Lcom/facebook/graphql/model/GraphQLQuantity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 327383
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->k()Lcom/facebook/graphql/model/GraphQLQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuantity;

    .line 327384
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->k()Lcom/facebook/graphql/model/GraphQLQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 327385
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;

    .line 327386
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->g:Lcom/facebook/graphql/model/GraphQLQuantity;

    .line 327387
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327388
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327373
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327374
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->e:Ljava/lang/String;

    .line 327375
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 327370
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 327371
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->h:J

    .line 327372
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 327369
    const v0, 0x77fba080

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327366
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327367
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 327368
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327363
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->g:Lcom/facebook/graphql/model/GraphQLQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327364
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->g:Lcom/facebook/graphql/model/GraphQLQuantity;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->g:Lcom/facebook/graphql/model/GraphQLQuantity;

    .line 327365
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->g:Lcom/facebook/graphql/model/GraphQLQuantity;

    return-object v0
.end method

.method public final l()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 327360
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 327361
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 327362
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;->h:J

    return-wide v0
.end method
