.class public final Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/1pT;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNewsFeedEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNewsFeedEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:D

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 329464
    const-class v0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 329467
    const-class v0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 329465
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 329466
    return-void
.end method

.method private q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 329405
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 329406
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 329407
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->j:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 329445
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 329446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 329447
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 329448
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 329449
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    sget-object v4, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v4

    .line 329450
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 329451
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 329452
    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->a()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v0

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    if-ne v0, v7, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v5, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 329453
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 329454
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 329455
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->l()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 329456
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 329457
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->q()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 329458
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 329459
    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->o()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 329460
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 329461
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 329462
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 329463
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->a()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 329437
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 329438
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 329439
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 329440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 329441
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;

    .line 329442
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->k:Lcom/facebook/graphql/model/FeedUnit;

    .line 329443
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 329444
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLBumpReason;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 329434
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 329435
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 329436
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 329429
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 329430
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->h:Z

    .line 329431
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->j:Z

    .line 329432
    const/4 v0, 0x7

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->l:D

    .line 329433
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 329468
    const v0, 0x379f888e

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 329426
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 329427
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->f:Ljava/lang/String;

    .line 329428
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 329423
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 329424
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->g:Ljava/lang/String;

    .line 329425
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 329420
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 329421
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 329422
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->h:Z

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 329417
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 329418
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->i:Ljava/lang/String;

    .line 329419
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 329414
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->k:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 329415
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->k:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x6

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->k:Lcom/facebook/graphql/model/FeedUnit;

    .line 329416
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->k:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final o()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 329411
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 329412
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 329413
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->l:D

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 329408
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 329409
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->m:Ljava/lang/String;

    .line 329410
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->m:Ljava/lang/String;

    return-object v0
.end method
