.class public final Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 324640
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 324639
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 324637
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 324638
    return-void
.end method

.method public constructor <init>(LX/4YC;)V
    .locals 1

    .prologue
    .line 324628
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 324629
    iget-object v0, p1, LX/4YC;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->e:Ljava/lang/String;

    .line 324630
    iget-object v0, p1, LX/4YC;->c:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->f:Lcom/facebook/graphql/model/GraphQLActor;

    .line 324631
    iget-object v0, p1, LX/4YC;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->g:Ljava/lang/String;

    .line 324632
    iget-object v0, p1, LX/4YC;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->h:Ljava/lang/String;

    .line 324633
    iget-object v0, p1, LX/4YC;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->i:Ljava/lang/String;

    .line 324634
    iget-object v0, p1, LX/4YC;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->j:Ljava/lang/String;

    .line 324635
    iget-object v0, p1, LX/4YC;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->k:Ljava/lang/String;

    .line 324636
    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324625
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324626
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->j:Ljava/lang/String;

    .line 324627
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 324595
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 324596
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 324597
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 324598
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 324599
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 324600
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 324601
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 324602
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 324603
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 324604
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 324605
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 324606
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 324607
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 324608
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 324609
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 324610
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 324611
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324612
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 324617
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 324618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 324619
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 324620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 324621
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    .line 324622
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->f:Lcom/facebook/graphql/model/GraphQLActor;

    .line 324623
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324624
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 324616
    const v0, 0x485bf711

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324613
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324614
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->e:Ljava/lang/String;

    .line 324615
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324592
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->f:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324593
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->f:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->f:Lcom/facebook/graphql/model/GraphQLActor;

    .line 324594
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->f:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324589
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324590
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->g:Ljava/lang/String;

    .line 324591
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324586
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324587
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->h:Ljava/lang/String;

    .line 324588
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324583
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324584
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->i:Ljava/lang/String;

    .line 324585
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324580
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324581
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->k:Ljava/lang/String;

    .line 324582
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->k:Ljava/lang/String;

    return-object v0
.end method
