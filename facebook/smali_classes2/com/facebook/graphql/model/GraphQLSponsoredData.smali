.class public final Lcom/facebook/graphql/model/GraphQLSponsoredData;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSponsoredData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSponsoredData$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public l:I

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field public t:I

.field public u:I

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 202433
    const-class v0, Lcom/facebook/graphql/model/GraphQLSponsoredData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 202434
    const-class v0, Lcom/facebook/graphql/model/GraphQLSponsoredData$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 202435
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 202436
    return-void
.end method

.method public constructor <init>(LX/4Ym;)V
    .locals 1

    .prologue
    .line 202437
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 202438
    iget-object v0, p1, LX/4Ym;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->v:Ljava/lang/String;

    .line 202439
    iget-object v0, p1, LX/4Ym;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->e:Ljava/lang/String;

    .line 202440
    iget v0, p1, LX/4Ym;->d:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->f:I

    .line 202441
    iget-object v0, p1, LX/4Ym;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->g:Ljava/lang/String;

    .line 202442
    iget-boolean v0, p1, LX/4Ym;->f:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->h:Z

    .line 202443
    iget-boolean v0, p1, LX/4Ym;->g:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->i:Z

    .line 202444
    iget-boolean v0, p1, LX/4Ym;->h:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->j:Z

    .line 202445
    iget-boolean v0, p1, LX/4Ym;->i:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->k:Z

    .line 202446
    iget v0, p1, LX/4Ym;->j:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->l:I

    .line 202447
    iget-boolean v0, p1, LX/4Ym;->k:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->m:Z

    .line 202448
    iget-boolean v0, p1, LX/4Ym;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->n:Z

    .line 202449
    iget-boolean v0, p1, LX/4Ym;->m:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->o:Z

    .line 202450
    iget-object v0, p1, LX/4Ym;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->p:Ljava/lang/String;

    .line 202451
    iget-boolean v0, p1, LX/4Ym;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->q:Z

    .line 202452
    iget-object v0, p1, LX/4Ym;->p:Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->r:Lcom/facebook/graphql/model/GraphQLUser;

    .line 202453
    iget-boolean v0, p1, LX/4Ym;->q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->s:Z

    .line 202454
    iget v0, p1, LX/4Ym;->r:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->t:I

    .line 202455
    iget v0, p1, LX/4Ym;->s:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->u:I

    .line 202456
    return-void
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202457
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202458
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->p:Ljava/lang/String;

    .line 202459
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final N_()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 202460
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202461
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202462
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->n:Z

    return v0
.end method

.method public final O_()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202406
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202407
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202408
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->q:Z

    return v0
.end method

.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202463
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202464
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202465
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->f:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 202466
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 202467
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 202468
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 202469
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 202470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->q()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 202471
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->r()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 202472
    const/16 v5, 0x12

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 202473
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 202474
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->a()I

    move-result v5

    invoke-virtual {p1, v0, v5, v6}, LX/186;->a(III)V

    .line 202475
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 202476
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->c()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 202477
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->d()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 202478
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->n()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 202479
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 202480
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->e()I

    move-result v1

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 202481
    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->g()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 202482
    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->N_()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 202483
    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->p()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 202484
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 202485
    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->O_()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 202486
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 202487
    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->j()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 202488
    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->k()I

    move-result v1

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 202489
    const/16 v0, 0x10

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 202490
    const/16 v0, 0x11

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 202491
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 202492
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 202493
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 202494
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->q()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202495
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->q()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 202496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->q()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 202497
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 202498
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSponsoredData;->r:Lcom/facebook/graphql/model/GraphQLUser;

    .line 202499
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 202500
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 202501
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 202502
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->f:I

    .line 202503
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->h:Z

    .line 202504
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->i:Z

    .line 202505
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->j:Z

    .line 202506
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->k:Z

    .line 202507
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->l:I

    .line 202508
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->m:Z

    .line 202509
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->n:Z

    .line 202510
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->o:Z

    .line 202511
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->q:Z

    .line 202512
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->s:Z

    .line 202513
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->t:I

    .line 202514
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->u:I

    .line 202515
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202516
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202517
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->g:Ljava/lang/String;

    .line 202518
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202427
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202428
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202429
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->h:Z

    return v0
.end method

.method public final d()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202430
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202431
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202432
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->i:Z

    return v0
.end method

.method public final e()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202390
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202391
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202392
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->l:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 202393
    const v0, 0x1ecd5063

    return v0
.end method

.method public final g()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202394
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202395
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202396
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->m:Z

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202397
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202398
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202399
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->s:Z

    return v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202400
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202401
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202402
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->t:I

    return v0
.end method

.method public final l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202403
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202404
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202405
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->u:I

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202409
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202410
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->e:Ljava/lang/String;

    .line 202411
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202412
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202413
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202414
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->j:Z

    return v0
.end method

.method public final o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 202415
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202416
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202417
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->k:Z

    return v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202418
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202419
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202420
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->o:Z

    return v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202421
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->r:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202422
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->r:Lcom/facebook/graphql/model/GraphQLUser;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->r:Lcom/facebook/graphql/model/GraphQLUser;

    .line 202423
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->r:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202424
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202425
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->v:Ljava/lang/String;

    .line 202426
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSponsoredData;->v:Ljava/lang/String;

    return-object v0
.end method
