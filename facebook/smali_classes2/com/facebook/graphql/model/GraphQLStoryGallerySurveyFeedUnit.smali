.class public final Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:J

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:I

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 254552
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 254553
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 254554
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 254555
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x30c82027

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 254556
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->v:LX/0x2;

    .line 254557
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 254558
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->o:I

    .line 254559
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 254560
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 254561
    if-eqz v0, :cond_0

    .line 254562
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 254563
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 254564
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->m:Ljava/lang/String;

    .line 254565
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 254566
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 254567
    if-eqz v0, :cond_0

    .line 254568
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 254569
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 254570
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->n:Ljava/lang/String;

    .line 254571
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 254572
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 254573
    if-eqz v0, :cond_0

    .line 254574
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 254575
    :cond_0
    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254576
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254577
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->l:Ljava/lang/String;

    .line 254578
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254548
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254549
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->m:Ljava/lang/String;

    .line 254550
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254579
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254580
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->n:Ljava/lang/String;

    .line 254581
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->n:Ljava/lang/String;

    return-object v0
.end method

.method private w()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 254582
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 254583
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 254584
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->o:I

    return v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254585
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254586
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->q:Ljava/lang/String;

    .line 254587
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254588
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254589
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254590
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254591
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254592
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->u:Ljava/lang/String;

    .line 254593
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->u:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 254594
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254595
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254596
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->i:Ljava/lang/String;

    .line 254597
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 254598
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 254599
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 254600
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->j:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 254601
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 254602
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->v:LX/0x2;

    if-nez v0, :cond_0

    .line 254603
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->v:LX/0x2;

    .line 254604
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->v:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 254551
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 18

    .prologue
    .line 254423
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 254424
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 254425
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 254426
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 254427
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->E_()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 254428
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 254429
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->t()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 254430
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->u()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 254431
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->v()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 254432
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 254433
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->x()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 254434
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 254435
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 254436
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->s()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 254437
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->z()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 254438
    const/16 v6, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 254439
    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v2}, LX/186;->b(II)V

    .line 254440
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 254441
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 254442
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 254443
    const/4 v3, 0x5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 254444
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 254445
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 254446
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 254447
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 254448
    const/16 v2, 0xa

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->w()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 254449
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 254450
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 254451
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 254452
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 254453
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 254454
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 254455
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 254456
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 254461
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 254462
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254463
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254464
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 254465
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 254466
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254467
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 254468
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254469
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 254470
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 254471
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254472
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 254473
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254474
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 254475
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 254476
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254477
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 254478
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254479
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 254480
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 254481
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254482
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 254483
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254484
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 254485
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 254486
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254487
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 254488
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254489
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 254490
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 254491
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254492
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 254493
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 254494
    new-instance v0, LX/4Yu;

    invoke-direct {v0, p1}, LX/4Yu;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254495
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 254496
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->j:J

    .line 254497
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 254457
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 254458
    const/4 v0, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->j:J

    .line 254459
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->o:I

    .line 254460
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 254498
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254499
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 254500
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 254501
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    .line 254502
    :goto_0
    return-void

    .line 254503
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254504
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 254505
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 254506
    const/16 v0, 0x9

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 254507
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254508
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->w()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 254509
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 254510
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 254511
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 254512
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254513
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->a(Ljava/lang/String;)V

    .line 254514
    :cond_0
    :goto_0
    return-void

    .line 254515
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254516
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 254517
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254518
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 254521
    :goto_0
    return-object v0

    .line 254522
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 254523
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 254524
    const v0, -0x30c82027

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254525
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254526
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->f:Ljava/lang/String;

    .line 254527
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 254528
    const/4 v0, 0x0

    move-object v0, v0

    .line 254529
    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254530
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254531
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254532
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254533
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254534
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254535
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254536
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254537
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254538
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254539
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254540
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254541
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254542
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254543
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254544
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254545
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254546
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->t:Ljava/lang/String;

    .line 254547
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->t:Ljava/lang/String;

    return-object v0
.end method
