.class public final Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesEdge;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 308935
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 308939
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 308895
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 308896
    return-void
.end method

.method private j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 308936
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 308937
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 308938
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->e:I

    return v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308929
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308930
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->f:Ljava/util/List;

    .line 308931
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308932
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308933
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 308934
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 308918
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 308919
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->k()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 308920
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 308921
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 308922
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 308923
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->j()I

    move-result v3

    invoke-virtual {p1, v4, v3, v4}, LX/186;->a(III)V

    .line 308924
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 308925
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 308926
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 308927
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 308928
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308915
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308916
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->g:Ljava/util/List;

    .line 308917
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 308897
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 308898
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 308899
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 308900
    if-eqz v1, :cond_0

    .line 308901
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    .line 308902
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->f:Ljava/util/List;

    .line 308903
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 308904
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 308905
    if-eqz v1, :cond_1

    .line 308906
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    .line 308907
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 308908
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 308909
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 308910
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 308911
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    .line 308912
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 308913
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 308914
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 308892
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 308893
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->e:I

    .line 308894
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 308891
    const v0, -0xce5e2c6

    return v0
.end method
