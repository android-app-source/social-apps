.class public final Lcom/facebook/graphql/model/GraphQLOfferView;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLOfferView$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLOfferView$Serializer;
.end annotation


# instance fields
.field public e:J

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLOffer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325297
    const-class v0, Lcom/facebook/graphql/model/GraphQLOfferView$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325296
    const-class v0, Lcom/facebook/graphql/model/GraphQLOfferView$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 325294
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 325295
    return-void
.end method

.method private l()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 325235
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 325236
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 325237
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->e:J

    return-wide v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLOffer;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325291
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->g:Lcom/facebook/graphql/model/GraphQLOffer;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325292
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->g:Lcom/facebook/graphql/model/GraphQLOffer;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLOffer;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOffer;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->g:Lcom/facebook/graphql/model/GraphQLOffer;

    .line 325293
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->g:Lcom/facebook/graphql/model/GraphQLOffer;

    return-object v0
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325288
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325289
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->h:Ljava/util/List;

    .line 325290
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325285
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325286
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->j:Ljava/lang/String;

    .line 325287
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325282
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325283
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->k:Ljava/util/List;

    .line 325284
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 325265
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325266
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 325267
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->m()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 325268
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->n()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v8

    .line 325269
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 325270
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 325271
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->p()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 325272
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 325273
    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->l()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 325274
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 325275
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 325276
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 325277
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 325278
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 325279
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 325280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325281
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 325242
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325243
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->m()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 325244
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->m()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOffer;

    .line 325245
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->m()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 325246
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOfferView;

    .line 325247
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOfferView;->g:Lcom/facebook/graphql/model/GraphQLOffer;

    .line 325248
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 325249
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 325250
    if-eqz v2, :cond_1

    .line 325251
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOfferView;

    .line 325252
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLOfferView;->h:Ljava/util/List;

    move-object v1, v0

    .line 325253
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 325254
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 325255
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 325256
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOfferView;

    .line 325257
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOfferView;->i:Lcom/facebook/graphql/model/GraphQLStory;

    .line 325258
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 325259
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 325260
    if-eqz v2, :cond_3

    .line 325261
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOfferView;

    .line 325262
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLOfferView;->k:Ljava/util/List;

    move-object v1, v0

    .line 325263
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325264
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325241
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOfferView;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 325238
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 325239
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->e:J

    .line 325240
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 325234
    const v0, -0x14a5a4ff

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325231
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325232
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->f:Ljava/lang/String;

    .line 325233
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325228
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->i:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325229
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->i:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->i:Lcom/facebook/graphql/model/GraphQLStory;

    .line 325230
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOfferView;->i:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method
