.class public final Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:I

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 201605
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 201606
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 201607
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 201608
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x65f211ca

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 201609
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->t:LX/0x2;

    .line 201610
    return-void
.end method

.method public constructor <init>(LX/4Wr;)V
    .locals 2

    .prologue
    .line 201611
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 201612
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x65f211ca

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 201613
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->t:LX/0x2;

    .line 201614
    iget-object v0, p1, LX/4Wr;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->f:Ljava/lang/String;

    .line 201615
    iget-object v0, p1, LX/4Wr;->c:Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    .line 201616
    iget-object v0, p1, LX/4Wr;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->g:Ljava/lang/String;

    .line 201617
    iget-wide v0, p1, LX/4Wr;->e:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->h:J

    .line 201618
    iget v0, p1, LX/4Wr;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->i:I

    .line 201619
    iget-object v0, p1, LX/4Wr;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->j:Ljava/util/List;

    .line 201620
    iget-object v0, p1, LX/4Wr;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201621
    iget-object v0, p1, LX/4Wr;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->l:Ljava/lang/String;

    .line 201622
    iget-object v0, p1, LX/4Wr;->j:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->m:Ljava/util/List;

    .line 201623
    iget-object v0, p1, LX/4Wr;->k:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->q:Ljava/util/List;

    .line 201624
    iget-object v0, p1, LX/4Wr;->l:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 201625
    iget-object v0, p1, LX/4Wr;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->n:Ljava/lang/String;

    .line 201626
    iget-object v0, p1, LX/4Wr;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201627
    iget-object v0, p1, LX/4Wr;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->p:Ljava/lang/String;

    .line 201628
    iget-object v0, p1, LX/4Wr;->p:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->t:LX/0x2;

    .line 201629
    return-void
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 201630
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201631
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201632
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->g:Ljava/lang/String;

    .line 201633
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 201634
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201635
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201636
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->h:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 201637
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 201638
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->t:LX/0x2;

    if-nez v0, :cond_0

    .line 201639
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->t:LX/0x2;

    .line 201640
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->t:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 201641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 201642
    if-eqz v0, :cond_0

    :goto_0
    move-object v0, v0

    .line 201643
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/186;)I
    .locals 18

    .prologue
    .line 201644
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 201645
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 201646
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->E_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 201647
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->n()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v8

    .line 201648
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 201649
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->r()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 201650
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 201651
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->t()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 201652
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 201653
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->c()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 201654
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->v()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v15

    .line 201655
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 201656
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 201657
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 201658
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 201659
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 201660
    const/4 v3, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 201661
    const/4 v2, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->k()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 201662
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 201663
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 201664
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 201665
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 201666
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 201667
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 201668
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 201669
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 201670
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201671
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201672
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 201673
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 201674
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 201675
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201676
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    .line 201677
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 201678
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    .line 201679
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    .line 201680
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 201681
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 201682
    if-eqz v2, :cond_1

    .line 201683
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    .line 201684
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->j:Ljava/util/List;

    move-object v1, v0

    .line 201685
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 201686
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201687
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 201688
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    .line 201689
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201690
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 201691
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 201692
    if-eqz v2, :cond_3

    .line 201693
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    .line 201694
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->m:Ljava/util/List;

    move-object v1, v0

    .line 201695
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 201696
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 201697
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 201698
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    .line 201699
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 201700
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 201701
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201702
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 201703
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    .line 201704
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201705
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 201706
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201707
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 201708
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->h:J

    .line 201709
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 201600
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 201601
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->h:J

    .line 201602
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->i:I

    .line 201603
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201604
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201561
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201562
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->p:Ljava/lang/String;

    .line 201563
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201556
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201557
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 201558
    :goto_0
    return-object v0

    .line 201559
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 201560
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 201564
    const v0, 0x65f211ca

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201565
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201566
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->f:Ljava/lang/String;

    .line 201567
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 201568
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201569
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201570
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->i:I

    return v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201571
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201572
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->j:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->j:Ljava/util/List;

    .line 201573
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201574
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201575
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201576
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 201577
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 201578
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201579
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201580
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->l:Ljava/lang/String;

    .line 201581
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final s()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201582
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201583
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->m:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->m:Ljava/util/List;

    .line 201584
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201585
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201586
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->n:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->n:Ljava/lang/String;

    .line 201587
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201588
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201589
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201590
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final v()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201591
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->q:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201592
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->q:Ljava/util/List;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->q:Ljava/util/List;

    .line 201593
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201594
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201595
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 201596
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201597
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201598
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    .line 201599
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    return-object v0
.end method
