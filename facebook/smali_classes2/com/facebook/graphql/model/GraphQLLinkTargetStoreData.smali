.class public final Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320553
    const-class v0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320552
    const-class v0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320550
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 320551
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320547
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320548
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->e:Ljava/lang/String;

    .line 320549
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320544
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320545
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320546
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320505
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320506
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320507
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->h:Z

    return v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320541
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320542
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320543
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 320529
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320530
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 320531
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 320532
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 320533
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 320534
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 320535
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 320536
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 320537
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->l()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 320538
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 320539
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320540
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 320516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320517
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320518
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320519
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 320520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    .line 320521
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320522
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 320523
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320524
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 320525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    .line 320526
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320527
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320528
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 320512
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 320513
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->g:Z

    .line 320514
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->h:Z

    .line 320515
    return-void
.end method

.method public final a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320509
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320510
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320511
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->g:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 320508
    const v0, 0x4b008220    # 8421920.0f

    return v0
.end method
