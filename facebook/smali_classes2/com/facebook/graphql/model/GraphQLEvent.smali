.class public final Lcom/facebook/graphql/model/GraphQLEvent;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEvent$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEvent$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:J

.field public D:J

.field public E:Lcom/facebook/graphql/model/GraphQLEventCategoryData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLEventCategoryData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public S:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLEventTicketProvider;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/enums/GraphQLEventType;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public aA:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:I

.field public aD:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aF:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:I

.field public aN:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public aQ:Z

.field public aR:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aZ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:I

.field public ab:I

.field public ac:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Z

.field public av:Z

.field public aw:Z

.field public ax:Z

.field public ay:Z

.field public az:Z

.field public bA:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public bB:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bC:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bD:I

.field public bE:Z

.field public bF:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bG:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bH:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bI:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ba:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bc:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bd:Z

.field public be:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bf:J

.field public bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bh:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bi:J

.field public bj:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bl:Z

.field public bm:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bn:Lcom/facebook/graphql/model/GraphQLEventTimeRange;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bo:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bq:I

.field public br:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bs:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bt:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public bu:Z

.field public bv:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public bw:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

.field public bx:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public by:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public bz:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public f:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLInlineActivity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public v:Z

.field public w:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 307501
    const-class v0, Lcom/facebook/graphql/model/GraphQLEvent$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 307502
    const-class v0, Lcom/facebook/graphql/model/GraphQLEvent$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 307503
    const/16 v0, 0x8f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 307504
    return-void
.end method

.method public constructor <init>(LX/4W8;)V
    .locals 2

    .prologue
    .line 307505
    const/16 v0, 0x8f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 307506
    iget-object v0, p1, LX/4W8;->b:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 307507
    iget-object v0, p1, LX/4W8;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 307508
    iget-object v0, p1, LX/4W8;->d:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->g:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 307509
    iget-object v0, p1, LX/4W8;->e:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->h:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 307510
    iget-object v0, p1, LX/4W8;->f:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->i:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307511
    iget-object v0, p1, LX/4W8;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->j:Ljava/util/List;

    .line 307512
    iget-object v0, p1, LX/4W8;->h:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->k:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 307513
    iget-object v0, p1, LX/4W8;->i:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->l:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 307514
    iget-object v0, p1, LX/4W8;->j:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->m:Lcom/facebook/graphql/model/GraphQLStory;

    .line 307515
    iget-boolean v0, p1, LX/4W8;->k:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->n:Z

    .line 307516
    iget-boolean v0, p1, LX/4W8;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->o:Z

    .line 307517
    iget-boolean v0, p1, LX/4W8;->m:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->p:Z

    .line 307518
    iget-boolean v0, p1, LX/4W8;->n:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->q:Z

    .line 307519
    iget-boolean v0, p1, LX/4W8;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->r:Z

    .line 307520
    iget-boolean v0, p1, LX/4W8;->p:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->s:Z

    .line 307521
    iget-boolean v0, p1, LX/4W8;->q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->t:Z

    .line 307522
    iget-boolean v0, p1, LX/4W8;->r:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->u:Z

    .line 307523
    iget-boolean v0, p1, LX/4W8;->s:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->v:Z

    .line 307524
    iget-object v0, p1, LX/4W8;->t:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307525
    iget-object v0, p1, LX/4W8;->u:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->x:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 307526
    iget-object v0, p1, LX/4W8;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->y:Ljava/lang/String;

    .line 307527
    iget-object v0, p1, LX/4W8;->w:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 307528
    iget-object v0, p1, LX/4W8;->x:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->A:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 307529
    iget-object v0, p1, LX/4W8;->y:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 307530
    iget-wide v0, p1, LX/4W8;->z:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->C:J

    .line 307531
    iget-wide v0, p1, LX/4W8;->A:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->D:J

    .line 307532
    iget-object v0, p1, LX/4W8;->B:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->E:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 307533
    iget-object v0, p1, LX/4W8;->C:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307534
    iget-object v0, p1, LX/4W8;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 307535
    iget-object v0, p1, LX/4W8;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->H:Ljava/lang/String;

    .line 307536
    iget v0, p1, LX/4W8;->F:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bD:I

    .line 307537
    iget-object v0, p1, LX/4W8;->G:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->I:Ljava/lang/String;

    .line 307538
    iget-object v0, p1, LX/4W8;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->J:Ljava/lang/String;

    .line 307539
    iget-object v0, p1, LX/4W8;->I:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->K:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 307540
    iget-object v0, p1, LX/4W8;->J:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->L:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 307541
    iget-object v0, p1, LX/4W8;->K:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->M:Lcom/facebook/graphql/model/GraphQLActor;

    .line 307542
    iget-object v0, p1, LX/4W8;->L:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->N:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 307543
    iget-object v0, p1, LX/4W8;->M:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 307544
    iget-object v0, p1, LX/4W8;->N:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->P:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    .line 307545
    iget-object v0, p1, LX/4W8;->O:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Q:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 307546
    iget-object v0, p1, LX/4W8;->P:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->R:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 307547
    iget-object v0, p1, LX/4W8;->Q:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->S:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 307548
    iget-object v0, p1, LX/4W8;->R:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->T:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307549
    iget-object v0, p1, LX/4W8;->S:Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->U:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 307550
    iget-object v0, p1, LX/4W8;->T:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->V:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 307551
    iget-object v0, p1, LX/4W8;->U:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->W:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 307552
    iget-object v0, p1, LX/4W8;->V:Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->X:Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    .line 307553
    iget-object v0, p1, LX/4W8;->W:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Y:Ljava/lang/String;

    .line 307554
    iget-object v0, p1, LX/4W8;->X:Lcom/facebook/graphql/enums/GraphQLEventType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Z:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 307555
    iget v0, p1, LX/4W8;->Y:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aa:I

    .line 307556
    iget v0, p1, LX/4W8;->Z:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ab:I

    .line 307557
    iget-object v0, p1, LX/4W8;->aa:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ac:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    .line 307558
    iget-object v0, p1, LX/4W8;->ab:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ad:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 307559
    iget-object v0, p1, LX/4W8;->ac:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ae:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307560
    iget-object v0, p1, LX/4W8;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307561
    iget-object v0, p1, LX/4W8;->ae:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ag:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 307562
    iget-object v0, p1, LX/4W8;->af:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ah:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 307563
    iget-object v0, p1, LX/4W8;->ag:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ai:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 307564
    iget-object v0, p1, LX/4W8;->ah:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aj:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 307565
    iget-object v0, p1, LX/4W8;->ai:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ak:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 307566
    iget-object v0, p1, LX/4W8;->aj:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->al:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307567
    iget-object v0, p1, LX/4W8;->ak:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bF:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307568
    iget-object v0, p1, LX/4W8;->al:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->am:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307569
    iget-object v0, p1, LX/4W8;->am:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->an:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307570
    iget-object v0, p1, LX/4W8;->an:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bG:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307571
    iget-object v0, p1, LX/4W8;->ao:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ao:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307572
    iget-object v0, p1, LX/4W8;->ap:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ap:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307573
    iget-object v0, p1, LX/4W8;->aq:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aq:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307574
    iget-object v0, p1, LX/4W8;->ar:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ar:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 307575
    iget-object v0, p1, LX/4W8;->as:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->as:Ljava/lang/String;

    .line 307576
    iget-object v0, p1, LX/4W8;->at:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307577
    iget-boolean v0, p1, LX/4W8;->au:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->au:Z

    .line 307578
    iget-boolean v0, p1, LX/4W8;->av:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->av:Z

    .line 307579
    iget-boolean v0, p1, LX/4W8;->aw:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aw:Z

    .line 307580
    iget-boolean v0, p1, LX/4W8;->ax:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ax:Z

    .line 307581
    iget-boolean v0, p1, LX/4W8;->ay:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ay:Z

    .line 307582
    iget-boolean v0, p1, LX/4W8;->az:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->az:Z

    .line 307583
    iget-object v0, p1, LX/4W8;->aA:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aA:Ljava/lang/String;

    .line 307584
    iget-object v0, p1, LX/4W8;->aB:Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aB:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 307585
    iget v0, p1, LX/4W8;->aC:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aC:I

    .line 307586
    iget-object v0, p1, LX/4W8;->aD:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bB:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 307587
    iget-object v0, p1, LX/4W8;->aE:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bC:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 307588
    iget-object v0, p1, LX/4W8;->aF:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aD:Ljava/lang/String;

    .line 307589
    iget-object v0, p1, LX/4W8;->aG:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aE:Ljava/util/List;

    .line 307590
    iget-object v0, p1, LX/4W8;->aH:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aF:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 307591
    iget-object v0, p1, LX/4W8;->aI:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aG:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 307592
    iget-object v0, p1, LX/4W8;->aJ:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aH:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 307593
    iget-object v0, p1, LX/4W8;->aK:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aI:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 307594
    iget-object v0, p1, LX/4W8;->aL:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aJ:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307595
    iget-object v0, p1, LX/4W8;->aM:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aK:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307596
    iget-object v0, p1, LX/4W8;->aN:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aL:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 307597
    iget v0, p1, LX/4W8;->aO:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aM:I

    .line 307598
    iget-object v0, p1, LX/4W8;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307599
    iget-object v0, p1, LX/4W8;->aQ:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aO:Ljava/lang/String;

    .line 307600
    iget-object v0, p1, LX/4W8;->aR:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aP:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 307601
    iget-boolean v0, p1, LX/4W8;->aS:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aQ:Z

    .line 307602
    iget-object v0, p1, LX/4W8;->aT:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aR:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 307603
    iget-object v0, p1, LX/4W8;->aU:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 307604
    iget-object v0, p1, LX/4W8;->aV:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307605
    iget-object v0, p1, LX/4W8;->aW:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307606
    iget-object v0, p1, LX/4W8;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307607
    iget-object v0, p1, LX/4W8;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aV:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307608
    iget-object v0, p1, LX/4W8;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aW:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307609
    iget-object v0, p1, LX/4W8;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307610
    iget-object v0, p1, LX/4W8;->bb:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aY:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 307611
    iget-object v0, p1, LX/4W8;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307612
    iget-object v0, p1, LX/4W8;->bd:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307613
    iget-object v0, p1, LX/4W8;->be:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307614
    iget-object v0, p1, LX/4W8;->bf:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307615
    iget-boolean v0, p1, LX/4W8;->bg:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bd:Z

    .line 307616
    iget-object v0, p1, LX/4W8;->bh:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307617
    iget-object v0, p1, LX/4W8;->bi:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->be:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 307618
    iget-wide v0, p1, LX/4W8;->bj:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bf:J

    .line 307619
    iget-object v0, p1, LX/4W8;->bk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 307620
    iget-object v0, p1, LX/4W8;->bl:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bh:Ljava/lang/String;

    .line 307621
    iget-wide v0, p1, LX/4W8;->bm:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bi:J

    .line 307622
    iget-object v0, p1, LX/4W8;->bn:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bj:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 307623
    iget-object v0, p1, LX/4W8;->bo:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 307624
    iget-boolean v0, p1, LX/4W8;->bp:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bl:Z

    .line 307625
    iget-boolean v0, p1, LX/4W8;->bq:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bE:Z

    .line 307626
    iget-object v0, p1, LX/4W8;->br:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bm:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307627
    iget-object v0, p1, LX/4W8;->bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 307628
    iget-object v0, p1, LX/4W8;->bt:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bn:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 307629
    iget-object v0, p1, LX/4W8;->bu:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bo:Ljava/lang/String;

    .line 307630
    iget-object v0, p1, LX/4W8;->bv:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bp:Ljava/lang/String;

    .line 307631
    iget v0, p1, LX/4W8;->bw:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bq:I

    .line 307632
    iget-object v0, p1, LX/4W8;->bx:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->br:Ljava/lang/String;

    .line 307633
    iget-object v0, p1, LX/4W8;->by:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bs:Ljava/lang/String;

    .line 307634
    iget-object v0, p1, LX/4W8;->bz:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bt:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 307635
    iget-boolean v0, p1, LX/4W8;->bA:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bu:Z

    .line 307636
    iget-object v0, p1, LX/4W8;->bB:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bv:Ljava/util/List;

    .line 307637
    iget-object v0, p1, LX/4W8;->bC:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bw:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 307638
    iget-object v0, p1, LX/4W8;->bD:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bx:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 307639
    iget-object v0, p1, LX/4W8;->bE:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->by:Ljava/util/List;

    .line 307640
    iget-object v0, p1, LX/4W8;->bF:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bz:Ljava/util/List;

    .line 307641
    iget-object v0, p1, LX/4W8;->bG:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bA:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 307642
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 307643
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bq:I

    .line 307644
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 307645
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 307646
    if-eqz v0, :cond_0

    .line 307647
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x77

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 307648
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 307649
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bf:J

    .line 307650
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 307651
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 307652
    if-eqz v0, :cond_0

    .line 307653
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x6c

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 307654
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 3

    .prologue
    .line 307655
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bt:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 307656
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 307657
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 307658
    if-eqz v0, :cond_0

    .line 307659
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x7b

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 307660
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)V
    .locals 3

    .prologue
    .line 307661
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bw:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 307662
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 307663
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 307664
    if-eqz v0, :cond_0

    .line 307665
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x7e

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 307666
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 3

    .prologue
    .line 307492
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bA:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 307493
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 307494
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 307495
    if-eqz v0, :cond_0

    .line 307496
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x82

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 307497
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 307670
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aD:Ljava/lang/String;

    .line 307671
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 307672
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 307673
    if-eqz v0, :cond_0

    .line 307674
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x50

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 307675
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 307676
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLEvent;->q:Z

    .line 307677
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 307678
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 307679
    if-eqz v0, :cond_0

    .line 307680
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 307681
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 307682
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLEvent;->av:Z

    .line 307683
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 307684
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 307685
    if-eqz v0, :cond_0

    .line 307686
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x47

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 307687
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 307688
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aw:Z

    .line 307689
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 307690
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 307691
    if-eqz v0, :cond_0

    .line 307692
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x48

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 307693
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 307694
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bu:Z

    .line 307695
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 307696
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 307697
    if-eqz v0, :cond_0

    .line 307698
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x7c

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 307699
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 307700
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307701
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307702
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->v:Z

    return v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307703
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->w:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307704
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->w:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307705
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->w:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307706
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->x:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307707
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->x:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->x:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 307708
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->x:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method public final D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307709
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307710
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->y:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->y:Ljava/lang/String;

    .line 307711
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307462
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307463
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 307464
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307435
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->A:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307436
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->A:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->A:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 307437
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->A:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307429
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->B:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307430
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->B:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 307431
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->B:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final H()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307438
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307439
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307440
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->C:J

    return-wide v0
.end method

.method public final I()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307441
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307442
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307443
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->D:J

    return-wide v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLEventCategoryData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307444
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->E:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307445
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->E:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->E:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 307446
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->E:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307447
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->F:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307448
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->F:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307449
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->F:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307450
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307451
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 307452
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307453
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307454
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->H:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->H:Ljava/lang/String;

    .line 307455
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final N()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307456
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307457
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->I:Ljava/lang/String;

    const/16 v1, 0x20

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->I:Ljava/lang/String;

    .line 307458
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307459
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307460
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->J:Ljava/lang/String;

    const/16 v1, 0x21

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->J:Ljava/lang/String;

    .line 307461
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Lcom/facebook/graphql/model/GraphQLEventCategoryData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307432
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->K:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307433
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->K:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->K:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 307434
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->K:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    return-object v0
.end method

.method public final Q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307465
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->L:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307466
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->L:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->L:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 307467
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->L:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method public final R()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307468
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->M:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307469
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->M:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->M:Lcom/facebook/graphql/model/GraphQLActor;

    .line 307470
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->M:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final S()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307471
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->N:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307472
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->N:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->N:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 307473
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->N:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    return-object v0
.end method

.method public final T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307474
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307475
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 307476
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final U()Lcom/facebook/graphql/model/GraphQLEventHostsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307477
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->P:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307478
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->P:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->P:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    .line 307479
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->P:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    return-object v0
.end method

.method public final V()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307480
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Q:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307481
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Q:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Q:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 307482
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Q:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    return-object v0
.end method

.method public final W()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307483
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->R:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307484
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->R:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->R:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 307485
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->R:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method

.method public final X()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307486
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->S:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307487
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->S:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->S:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 307488
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->S:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    return-object v0
.end method

.method public final Y()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307489
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->T:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307490
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->T:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->T:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307491
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->T:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308012
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->U:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308013
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->U:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->U:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 308014
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->U:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 99

    .prologue
    .line 307742
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 307743
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 307744
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->l()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 307745
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->m()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 307746
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->n()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 307747
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->o()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v7

    .line 307748
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->p()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 307749
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 307750
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->r()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 307751
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 307752
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->D()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 307753
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 307754
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->F()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 307755
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 307756
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->J()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 307757
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 307758
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 307759
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->M()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 307760
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->N()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 307761
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->O()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 307762
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->P()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 307763
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 307764
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->R()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 307765
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->S()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 307766
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 307767
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->U()Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 307768
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->V()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 307769
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->X()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 307770
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Y()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 307771
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Z()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 307772
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ac()Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 307773
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ad()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 307774
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ah()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 307775
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aj()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 307776
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 307777
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->al()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 307778
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->am()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 307779
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->an()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 307780
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ao()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 307781
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ap()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 307782
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aq()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 307783
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ar()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 307784
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->as()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 307785
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->at()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 307786
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->au()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 307787
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->av()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 307788
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aw()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 307789
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    .line 307790
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 307791
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aF()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v51

    .line 307792
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aG()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 307793
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aI()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v53

    .line 307794
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aJ()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v54

    .line 307795
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aK()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 307796
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aL()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 307797
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aM()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 307798
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aN()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v58

    .line 307799
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aO()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v59

    .line 307800
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aP()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 307801
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aQ()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v61

    .line 307802
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v62

    .line 307803
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aT()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v63

    .line 307804
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aW()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v64

    .line 307805
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v65

    .line 307806
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 307807
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v67

    .line 307808
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ba()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v68

    .line 307809
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bb()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v69

    .line 307810
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v70

    .line 307811
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bd()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v71

    .line 307812
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v72

    .line 307813
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v73

    .line 307814
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v74

    .line 307815
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bh()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v75

    .line 307816
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bj()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v76

    .line 307817
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bl()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v77

    .line 307818
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bm()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v78

    .line 307819
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bo()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v79

    .line 307820
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v80

    .line 307821
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->br()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v81

    .line 307822
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v82

    .line 307823
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bt()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v83

    .line 307824
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bu()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v84

    .line 307825
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bw()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v85

    .line 307826
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bx()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v86

    .line 307827
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bA()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v87

    .line 307828
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bD()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v88

    .line 307829
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bE()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v89

    .line 307830
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bG()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v90

    .line 307831
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bH()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v91

    .line 307832
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bK()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v92

    .line 307833
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bL()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v93

    .line 307834
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v94

    .line 307835
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bN()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v95

    .line 307836
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bO()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v96

    .line 307837
    const/16 v2, 0x8e

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 307838
    const/16 v97, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->j()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v2

    sget-object v98, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-object/from16 v0, v98

    if-ne v2, v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    move/from16 v1, v97

    invoke-virtual {v0, v1, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307839
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 307840
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 307841
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 307842
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 307843
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 307844
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 307845
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 307846
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 307847
    const/16 v2, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->s()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307848
    const/16 v2, 0xb

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->t()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307849
    const/16 v2, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->u()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307850
    const/16 v2, 0xd

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->v()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307851
    const/16 v2, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->w()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307852
    const/16 v2, 0xf

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->x()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307853
    const/16 v2, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307854
    const/16 v2, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->z()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307855
    const/16 v2, 0x12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->A()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307856
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 307857
    const/16 v3, 0x14

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307858
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 307859
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 307860
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 307861
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 307862
    const/16 v3, 0x19

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->H()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 307863
    const/16 v3, 0x1a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->I()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 307864
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307865
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307866
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307867
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307868
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307869
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307870
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307871
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307872
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307873
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307874
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307875
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307876
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307877
    const/16 v3, 0x29

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->W()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307878
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307879
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307880
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307881
    const/16 v3, 0x2d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aa()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307882
    const/16 v3, 0x2e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ab()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    if-ne v2, v4, :cond_4

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307883
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307884
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307885
    const/16 v3, 0x31

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ae()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    if-ne v2, v4, :cond_5

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307886
    const/16 v2, 0x32

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->af()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 307887
    const/16 v2, 0x33

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ag()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 307888
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307889
    const/16 v3, 0x35

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ai()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    if-ne v2, v4, :cond_6

    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307890
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307891
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307892
    const/16 v2, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307893
    const/16 v2, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307894
    const/16 v2, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307895
    const/16 v2, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307896
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307897
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307898
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307899
    const/16 v2, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307900
    const/16 v2, 0x40

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307901
    const/16 v2, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307902
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307903
    const/16 v2, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307904
    const/16 v2, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307905
    const/16 v2, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307906
    const/16 v2, 0x46

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->az()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307907
    const/16 v2, 0x47

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aA()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307908
    const/16 v2, 0x48

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aB()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307909
    const/16 v2, 0x49

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aC()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307910
    const/16 v2, 0x4a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aD()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307911
    const/16 v2, 0x4b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aE()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307912
    const/16 v2, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307913
    const/16 v2, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307914
    const/16 v2, 0x4f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aH()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 307915
    const/16 v2, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307916
    const/16 v2, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307917
    const/16 v2, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307918
    const/16 v2, 0x53

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307919
    const/16 v2, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307920
    const/16 v2, 0x55

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307921
    const/16 v2, 0x56

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307922
    const/16 v2, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307923
    const/16 v2, 0x58

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307924
    const/16 v2, 0x59

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aR()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 307925
    const/16 v2, 0x5a

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307926
    const/16 v2, 0x5b

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307927
    const/16 v3, 0x5c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aU()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-ne v2, v4, :cond_7

    const/4 v2, 0x0

    :goto_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307928
    const/16 v2, 0x5d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aV()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307929
    const/16 v2, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307930
    const/16 v2, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307931
    const/16 v2, 0x60

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307932
    const/16 v2, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307933
    const/16 v2, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307934
    const/16 v2, 0x63

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307935
    const/16 v2, 0x64

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307936
    const/16 v2, 0x65

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307937
    const/16 v2, 0x66

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307938
    const/16 v2, 0x67

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307939
    const/16 v2, 0x68

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307940
    const/16 v2, 0x69

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307941
    const/16 v2, 0x6a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bi()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307942
    const/16 v2, 0x6b

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307943
    const/16 v3, 0x6c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bk()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 307944
    const/16 v2, 0x6d

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307945
    const/16 v2, 0x6e

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307946
    const/16 v3, 0x6f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bn()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 307947
    const/16 v2, 0x70

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307948
    const/16 v2, 0x71

    move-object/from16 v0, p1

    move/from16 v1, v80

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307949
    const/16 v2, 0x72

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bq()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307950
    const/16 v2, 0x73

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307951
    const/16 v2, 0x74

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307952
    const/16 v2, 0x75

    move-object/from16 v0, p1

    move/from16 v1, v83

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307953
    const/16 v2, 0x76

    move-object/from16 v0, p1

    move/from16 v1, v84

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307954
    const/16 v2, 0x77

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bv()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 307955
    const/16 v2, 0x79

    move-object/from16 v0, p1

    move/from16 v1, v85

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307956
    const/16 v2, 0x7a

    move-object/from16 v0, p1

    move/from16 v1, v86

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307957
    const/16 v3, 0x7b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v2, v4, :cond_8

    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307958
    const/16 v2, 0x7c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bz()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307959
    const/16 v2, 0x7d

    move-object/from16 v0, p1

    move/from16 v1, v87

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307960
    const/16 v3, 0x7e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bB()Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    if-ne v2, v4, :cond_9

    const/4 v2, 0x0

    :goto_9
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307961
    const/16 v3, 0x7f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bC()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v4, :cond_a

    const/4 v2, 0x0

    :goto_a
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307962
    const/16 v2, 0x80

    move-object/from16 v0, p1

    move/from16 v1, v88

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307963
    const/16 v2, 0x81

    move-object/from16 v0, p1

    move/from16 v1, v89

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307964
    const/16 v3, 0x82

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v2, v4, :cond_b

    const/4 v2, 0x0

    :goto_b
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 307965
    const/16 v2, 0x83

    move-object/from16 v0, p1

    move/from16 v1, v90

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307966
    const/16 v2, 0x84

    move-object/from16 v0, p1

    move/from16 v1, v91

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307967
    const/16 v2, 0x85

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bI()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 307968
    const/16 v2, 0x88

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bJ()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 307969
    const/16 v2, 0x89

    move-object/from16 v0, p1

    move/from16 v1, v92

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307970
    const/16 v2, 0x8a

    move-object/from16 v0, p1

    move/from16 v1, v93

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307971
    const/16 v2, 0x8b

    move-object/from16 v0, p1

    move/from16 v1, v94

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307972
    const/16 v2, 0x8c

    move-object/from16 v0, p1

    move/from16 v1, v95

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307973
    const/16 v2, 0x8d

    move-object/from16 v0, p1

    move/from16 v1, v96

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 307974
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 307975
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 307976
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->j()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v2

    goto/16 :goto_0

    .line 307977
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    goto/16 :goto_1

    .line 307978
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->W()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v2

    goto/16 :goto_2

    .line 307979
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aa()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v2

    goto/16 :goto_3

    .line 307980
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ab()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v2

    goto/16 :goto_4

    .line 307981
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ae()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v2

    goto/16 :goto_5

    .line 307982
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ai()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v2

    goto/16 :goto_6

    .line 307983
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aU()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v2

    goto/16 :goto_7

    .line 307984
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    goto/16 :goto_8

    .line 307985
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bB()Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    move-result-object v2

    goto/16 :goto_9

    .line 307986
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bC()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    goto/16 :goto_a

    .line 307987
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    goto/16 :goto_b
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 308018
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 308019
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 308020
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 308021
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 308022
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308023
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 308024
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->l()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 308025
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->l()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 308026
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->l()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 308027
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308028
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->g:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 308029
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->m()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 308030
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->m()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308031
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->m()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 308032
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308033
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->h:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308034
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->n()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 308035
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->n()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->n()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 308037
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308038
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->i:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308039
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->p()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 308040
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->p()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 308041
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->p()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 308042
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308043
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->k:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 308044
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 308045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 308046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 308047
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308048
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->l:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 308049
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->r()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 308050
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->r()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 308051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->r()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 308052
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308053
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->m:Lcom/facebook/graphql/model/GraphQLStory;

    .line 308054
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 308055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 308057
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308058
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308059
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 308060
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 308061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 308062
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308063
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 308064
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->F()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 308065
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->F()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 308066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->F()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 308067
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308068
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->A:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 308069
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 308070
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 308071
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 308072
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308073
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 308074
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->J()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 308075
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->J()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 308076
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->J()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 308077
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308078
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->E:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 308079
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 308080
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 308082
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308083
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308084
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 308085
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 308086
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 308087
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308088
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 308089
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->P()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 308090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->P()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 308091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->P()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 308092
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308093
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->K:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 308094
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 308095
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 308096
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 308097
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308098
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->L:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 308099
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->R()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 308100
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->R()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 308101
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->R()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 308102
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308103
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->M:Lcom/facebook/graphql/model/GraphQLActor;

    .line 308104
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->S()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 308105
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->S()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 308106
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->S()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 308107
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308108
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->N:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 308109
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 308110
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 308111
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->T()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 308112
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308113
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->O:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 308114
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->U()Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 308115
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->U()Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    .line 308116
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->U()Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 308117
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308118
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->P:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    .line 308119
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->V()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 308120
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->V()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308121
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->V()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 308122
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308123
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->Q:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308124
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->X()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 308125
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->X()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308126
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->X()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 308127
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308128
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->S:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308129
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Y()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 308130
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Y()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308131
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Y()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 308132
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308133
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->T:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308134
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Z()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 308135
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Z()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 308136
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Z()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 308137
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308138
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->U:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 308139
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ac()Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 308140
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ac()Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    .line 308141
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ac()Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 308142
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308143
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->X:Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    .line 308144
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ah()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 308145
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ah()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    .line 308146
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ah()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 308147
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308148
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->ac:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    .line 308149
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aj()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 308150
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aj()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308151
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aj()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 308152
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308153
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->ae:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308154
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 308155
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308156
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 308157
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308158
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308159
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->al()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 308160
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->al()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 308161
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->al()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 308162
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308163
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->ag:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 308164
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->am()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 308165
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->am()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308166
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->am()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 308167
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308168
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->ah:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308169
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->an()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 308170
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->an()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308171
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->an()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 308172
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308173
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->ai:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308174
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ao()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 308175
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ao()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308176
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ao()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 308177
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308178
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aj:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308179
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ap()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 308180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ap()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ap()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 308182
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308183
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->ak:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308184
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aq()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 308185
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aq()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308186
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aq()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 308187
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308188
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->al:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308189
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bK()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 308190
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bK()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308191
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bK()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 308192
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308193
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bF:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308194
    :cond_22
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ar()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 308195
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ar()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308196
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ar()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 308197
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308198
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->am:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308199
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->as()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 308200
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->as()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308201
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->as()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 308202
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308203
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->an:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308204
    :cond_24
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bL()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 308205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bL()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308206
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bL()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 308207
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308208
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bG:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308209
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->at()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 308210
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->at()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308211
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->at()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 308212
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308213
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->ao:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308214
    :cond_26
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->au()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 308215
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->au()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->au()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 308217
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308218
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->ap:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308219
    :cond_27
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->av()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 308220
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->av()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308221
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->av()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 308222
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308223
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aq:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308224
    :cond_28
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aw()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 308225
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aw()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308226
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aw()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 308227
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308228
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->ar:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308229
    :cond_29
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 308230
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308231
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 308232
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308233
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308234
    :cond_2a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aG()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 308235
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aG()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 308236
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aG()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 308237
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308238
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aB:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 308239
    :cond_2b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bG()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 308240
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bG()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 308241
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bG()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 308242
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308243
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bB:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 308244
    :cond_2c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bH()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 308245
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bH()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 308246
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bH()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 308247
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308248
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bC:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 308249
    :cond_2d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aK()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 308250
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aK()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 308251
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aK()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 308252
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308253
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aF:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 308254
    :cond_2e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aL()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 308255
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aL()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 308256
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aL()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2f

    .line 308257
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308258
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aG:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 308259
    :cond_2f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aM()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 308260
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aM()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308261
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aM()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_30

    .line 308262
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308263
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aH:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308264
    :cond_30
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aN()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 308265
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aN()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308266
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aN()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_31

    .line 308267
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308268
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aI:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308269
    :cond_31
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aO()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 308270
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aO()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308271
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aO()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_32

    .line 308272
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308273
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aJ:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308274
    :cond_32
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aP()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 308275
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aP()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308276
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aP()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_33

    .line 308277
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308278
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aK:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308279
    :cond_33
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aQ()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 308280
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aQ()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 308281
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aQ()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_34

    .line 308282
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308283
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aL:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 308284
    :cond_34
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 308285
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308286
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_35

    .line 308287
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308288
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308289
    :cond_35
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aW()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 308290
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aW()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 308291
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aW()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_36

    .line 308292
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308293
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aR:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 308294
    :cond_36
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_37

    .line 308295
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 308296
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_37

    .line 308297
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308298
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 308299
    :cond_37
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 308300
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308301
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_38

    .line 308302
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308303
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308304
    :cond_38
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 308305
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308306
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_39

    .line 308307
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308308
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308309
    :cond_39
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3a

    .line 308310
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308311
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3a

    .line 308312
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308313
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308314
    :cond_3a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ba()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3b

    .line 308315
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ba()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308316
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ba()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3b

    .line 308317
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308318
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aV:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308319
    :cond_3b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bb()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 308320
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bb()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308321
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bb()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3c

    .line 308322
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308323
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aW:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308324
    :cond_3c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3d

    .line 308325
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308326
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3d

    .line 308327
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308328
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308329
    :cond_3d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bd()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_3e

    .line 308330
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bd()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 308331
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bd()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_3e

    .line 308332
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308333
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aY:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 308334
    :cond_3e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3f

    .line 308335
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308336
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3f

    .line 308337
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308338
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308339
    :cond_3f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 308340
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308341
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_40

    .line 308342
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308343
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308344
    :cond_40
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_41

    .line 308345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308346
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_41

    .line 308347
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308348
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308349
    :cond_41
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bh()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 308350
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bh()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308351
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bh()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_42

    .line 308352
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308353
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308354
    :cond_42
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bN()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_43

    .line 308355
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bN()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308356
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bN()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_43

    .line 308357
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308358
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308359
    :cond_43
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bj()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_44

    .line 308360
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bj()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 308361
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bj()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    if-eq v2, v0, :cond_44

    .line 308362
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308363
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->be:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 308364
    :cond_44
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bl()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_45

    .line 308365
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bl()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 308366
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bl()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_45

    .line 308367
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308368
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 308369
    :cond_45
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bo()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_46

    .line 308370
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bo()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 308371
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bo()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_46

    .line 308372
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308373
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bj:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 308374
    :cond_46
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_47

    .line 308375
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 308376
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_47

    .line 308377
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308378
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 308379
    :cond_47
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->br()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_48

    .line 308380
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->br()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 308381
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->br()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_48

    .line 308382
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308383
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bm:Lcom/facebook/graphql/model/GraphQLImage;

    .line 308384
    :cond_48
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bO()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 308385
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bO()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 308386
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bO()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_49

    .line 308387
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308388
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 308389
    :cond_49
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v0

    if-eqz v0, :cond_4a

    .line 308390
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 308391
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v2

    if-eq v2, v0, :cond_4a

    .line 308392
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308393
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEvent;->bn:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 308394
    :cond_4a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bA()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4b

    .line 308395
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bA()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 308396
    if-eqz v2, :cond_4b

    .line 308397
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308398
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEvent;->bv:Ljava/util/List;

    move-object v1, v0

    .line 308399
    :cond_4b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bD()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4c

    .line 308400
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bD()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 308401
    if-eqz v2, :cond_4c

    .line 308402
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308403
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEvent;->by:Ljava/util/List;

    move-object v1, v0

    .line 308404
    :cond_4c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bE()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4d

    .line 308405
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bE()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 308406
    if-eqz v2, :cond_4d

    .line 308407
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 308408
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEvent;->bz:Ljava/util/List;

    move-object v1, v0

    .line 308409
    :cond_4d
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 308410
    if-nez v1, :cond_4e

    :goto_0
    return-object p0

    :cond_4e
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 308411
    new-instance v0, LX/4W9;

    invoke-direct {v0, p1}, LX/4W9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308412
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 308413
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 308414
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->n:Z

    .line 308415
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->o:Z

    .line 308416
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->p:Z

    .line 308417
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->q:Z

    .line 308418
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->r:Z

    .line 308419
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->s:Z

    .line 308420
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->t:Z

    .line 308421
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->u:Z

    .line 308422
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->v:Z

    .line 308423
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->C:J

    .line 308424
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->D:J

    .line 308425
    const/16 v0, 0x32

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aa:I

    .line 308426
    const/16 v0, 0x33

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ab:I

    .line 308427
    const/16 v0, 0x46

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->au:Z

    .line 308428
    const/16 v0, 0x47

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->av:Z

    .line 308429
    const/16 v0, 0x48

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aw:Z

    .line 308430
    const/16 v0, 0x49

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ax:Z

    .line 308431
    const/16 v0, 0x4a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ay:Z

    .line 308432
    const/16 v0, 0x4b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->az:Z

    .line 308433
    const/16 v0, 0x4f

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aC:I

    .line 308434
    const/16 v0, 0x59

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aM:I

    .line 308435
    const/16 v0, 0x5d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aQ:Z

    .line 308436
    const/16 v0, 0x6a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bd:Z

    .line 308437
    const/16 v0, 0x6c

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bf:J

    .line 308438
    const/16 v0, 0x6f

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bi:J

    .line 308439
    const/16 v0, 0x72

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bl:Z

    .line 308440
    const/16 v0, 0x77

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bq:I

    .line 308441
    const/16 v0, 0x7c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bu:Z

    .line 308442
    const/16 v0, 0x85

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bD:I

    .line 308443
    const/16 v0, 0x88

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bE:Z

    .line 308444
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 308445
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->v()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308447
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308448
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    .line 308449
    :goto_0
    return-void

    .line 308450
    :cond_0
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308451
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->S()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v0

    .line 308452
    if-eqz v0, :cond_11

    .line 308453
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308454
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308455
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 308456
    :cond_1
    const-string v0, "event_invitees.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 308457
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->V()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    .line 308458
    if-eqz v0, :cond_11

    .line 308459
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308460
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308461
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 308462
    :cond_2
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 308463
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->X()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    .line 308464
    if-eqz v0, :cond_11

    .line 308465
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308466
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308467
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 308468
    :cond_3
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 308469
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Y()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    .line 308470
    if-eqz v0, :cond_11

    .line 308471
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308472
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308473
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308474
    :cond_4
    const-string v0, "event_watchers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 308475
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aj()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    .line 308476
    if-eqz v0, :cond_11

    .line 308477
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308478
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308479
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308480
    :cond_5
    const-string v0, "is_canceled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 308481
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aA()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308482
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308483
    const/16 v0, 0x47

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308484
    :cond_6
    const-string v0, "is_event_draft"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 308485
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aB()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308486
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308487
    const/16 v0, 0x48

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308488
    :cond_7
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 308489
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aI()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308490
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308491
    const/16 v0, 0x50

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308492
    :cond_8
    const-string v0, "scheduled_publish_timestamp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 308493
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bk()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308494
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308495
    const/16 v0, 0x6c

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308496
    :cond_9
    const-string v0, "time_range.end"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 308497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v0

    .line 308498
    if-eqz v0, :cond_11

    .line 308499
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308500
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308501
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308502
    :cond_a
    const-string v0, "time_range.start"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 308503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v0

    .line 308504
    if-eqz v0, :cond_11

    .line 308505
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308506
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308507
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308508
    :cond_b
    const-string v0, "time_range.timezone"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 308509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v0

    .line 308510
    if-eqz v0, :cond_11

    .line 308511
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308512
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308513
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308514
    :cond_c
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 308515
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bv()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308517
    const/16 v0, 0x77

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308518
    :cond_d
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 308519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308520
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308521
    const/16 v0, 0x7b

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308522
    :cond_e
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 308523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bz()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308524
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308525
    const/16 v0, 0x7c

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308526
    :cond_f
    const-string v0, "viewer_notification_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 308527
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bB()Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308528
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308529
    const/16 v0, 0x7e

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308530
    :cond_10
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 308531
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 308532
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 308533
    const/16 v0, 0x82

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 308534
    :cond_11
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 308535
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308536
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLEvent;->a(Z)V

    .line 308537
    :cond_0
    :goto_0
    return-void

    .line 308538
    :cond_1
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 308539
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->S()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v0

    .line 308540
    if-eqz v0, :cond_0

    .line 308541
    if-eqz p3, :cond_2

    .line 308542
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 308543
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->a(I)V

    .line 308544
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->N:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    goto :goto_0

    .line 308545
    :cond_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->a(I)V

    goto :goto_0

    .line 308546
    :cond_3
    const-string v0, "event_invitees.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 308547
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->V()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    move-result-object v0

    .line 308548
    if-eqz v0, :cond_0

    .line 308549
    if-eqz p3, :cond_4

    .line 308550
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308551
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->a(I)V

    .line 308552
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Q:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    goto :goto_0

    .line 308553
    :cond_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;->a(I)V

    goto :goto_0

    .line 308554
    :cond_5
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 308555
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->X()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v0

    .line 308556
    if-eqz v0, :cond_0

    .line 308557
    if-eqz p3, :cond_6

    .line 308558
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 308559
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->a(I)V

    .line 308560
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->S:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    goto :goto_0

    .line 308561
    :cond_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->a(I)V

    goto/16 :goto_0

    .line 308562
    :cond_7
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 308563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Y()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v0

    .line 308564
    if-eqz v0, :cond_0

    .line 308565
    if-eqz p3, :cond_8

    .line 308566
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 308567
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->a(I)V

    .line 308568
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->T:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    goto/16 :goto_0

    .line 308569
    :cond_8
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->a(I)V

    goto/16 :goto_0

    .line 308570
    :cond_9
    const-string v0, "event_watchers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 308571
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->aj()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v0

    .line 308572
    if-eqz v0, :cond_0

    .line 308573
    if-eqz p3, :cond_a

    .line 308574
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 308575
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->a(I)V

    .line 308576
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ae:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    goto/16 :goto_0

    .line 308577
    :cond_a
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->a(I)V

    goto/16 :goto_0

    .line 308578
    :cond_b
    const-string v0, "is_canceled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 308579
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLEvent;->b(Z)V

    goto/16 :goto_0

    .line 308580
    :cond_c
    const-string v0, "is_event_draft"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 308581
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLEvent;->c(Z)V

    goto/16 :goto_0

    .line 308582
    :cond_d
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 308583
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLEvent;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 308584
    :cond_e
    const-string v0, "scheduled_publish_timestamp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 308585
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/graphql/model/GraphQLEvent;->a(J)V

    goto/16 :goto_0

    .line 308586
    :cond_f
    const-string v0, "time_range.end"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 308587
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v0

    .line 308588
    if-eqz v0, :cond_0

    .line 308589
    if-eqz p3, :cond_10

    .line 308590
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 308591
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->a(Ljava/lang/String;)V

    .line 308592
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bn:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    goto/16 :goto_0

    .line 308593
    :cond_10
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 308594
    :cond_11
    const-string v0, "time_range.start"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 308595
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v0

    .line 308596
    if-eqz v0, :cond_0

    .line 308597
    if-eqz p3, :cond_12

    .line 308598
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 308599
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->b(Ljava/lang/String;)V

    .line 308600
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bn:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    goto/16 :goto_0

    .line 308601
    :cond_12
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 308602
    :cond_13
    const-string v0, "time_range.timezone"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 308603
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v0

    .line 308604
    if-eqz v0, :cond_0

    .line 308605
    if-eqz p3, :cond_14

    .line 308606
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 308607
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->c(Ljava/lang/String;)V

    .line 308608
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bn:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    goto/16 :goto_0

    .line 308609
    :cond_14
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 308610
    :cond_15
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 308611
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLEvent;->a(I)V

    goto/16 :goto_0

    .line 308612
    :cond_16
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 308613
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLEvent;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto/16 :goto_0

    .line 308614
    :cond_17
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 308615
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLEvent;->d(Z)V

    goto/16 :goto_0

    .line 308616
    :cond_18
    const-string v0, "viewer_notification_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 308617
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLEvent;->a(Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)V

    goto/16 :goto_0

    .line 308618
    :cond_19
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308619
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLEvent;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0
.end method

.method public final aA()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 308620
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 308621
    const/16 v0, 0x8

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 308622
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->av:Z

    return v0
.end method

.method public final aB()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 308623
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 308624
    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 308625
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aw:Z

    return v0
.end method

.method public final aC()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 308626
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 308627
    const/16 v0, 0x9

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 308628
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ax:Z

    return v0
.end method

.method public final aD()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 308653
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 308654
    const/16 v0, 0x9

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 308655
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ay:Z

    return v0
.end method

.method public final aE()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 308629
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 308630
    const/16 v0, 0x9

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 308631
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->az:Z

    return v0
.end method

.method public final aF()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308632
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aA:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308633
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aA:Ljava/lang/String;

    const/16 v1, 0x4c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aA:Ljava/lang/String;

    .line 308634
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aA:Ljava/lang/String;

    return-object v0
.end method

.method public final aG()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308635
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aB:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308636
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aB:Lcom/facebook/graphql/model/GraphQLLocation;

    const/16 v1, 0x4d

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aB:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 308637
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aB:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method public final aH()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 308638
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 308639
    const/16 v0, 0x9

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 308640
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aC:I

    return v0
.end method

.method public final aI()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308641
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aD:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308642
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aD:Ljava/lang/String;

    const/16 v1, 0x50

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aD:Ljava/lang/String;

    .line 308643
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aD:Ljava/lang/String;

    return-object v0
.end method

.method public final aJ()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308644
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aE:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308645
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aE:Ljava/util/List;

    const/16 v1, 0x51

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aE:Ljava/util/List;

    .line 308646
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aE:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final aK()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308647
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aF:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308648
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aF:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/16 v1, 0x52

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aF:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 308649
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aF:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    return-object v0
.end method

.method public final aL()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308015
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aG:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308016
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aG:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    const/16 v1, 0x53

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aG:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 308017
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aG:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    return-object v0
.end method

.method public final aM()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308650
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aH:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308651
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aH:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    const/16 v1, 0x54

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aH:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 308652
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aH:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    return-object v0
.end method

.method public final aN()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307715
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aI:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307716
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aI:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    const/16 v1, 0x55

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aI:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 307717
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aI:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    return-object v0
.end method

.method public final aO()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307718
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aJ:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307719
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aJ:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    const/16 v1, 0x56

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aJ:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307720
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aJ:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    return-object v0
.end method

.method public final aP()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307721
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aK:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307722
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aK:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    const/16 v1, 0x57

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aK:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307723
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aK:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    return-object v0
.end method

.method public final aQ()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307724
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aL:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307725
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aL:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x58

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aL:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 307726
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aL:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method public final aR()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307727
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307728
    const/16 v0, 0xb

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307729
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aM:I

    return v0
.end method

.method public final aS()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307730
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307731
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307732
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aT()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307733
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aO:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307734
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aO:Ljava/lang/String;

    const/16 v1, 0x5b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aO:Ljava/lang/String;

    .line 307735
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aO:Ljava/lang/String;

    return-object v0
.end method

.method public final aU()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307736
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aP:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307737
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aP:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/16 v1, 0x5c

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aP:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 307738
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aP:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method public final aV()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307739
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307740
    const/16 v0, 0xb

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307741
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aQ:Z

    return v0
.end method

.method public final aW()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307712
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aR:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307713
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aR:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aR:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 307714
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aR:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307988
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307989
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x5f

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 307990
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final aY()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307991
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307992
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x60

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307993
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aZ()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307994
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307995
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x61

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307996
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aa()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 307997
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->V:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307998
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->V:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->V:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 307999
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->V:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method

.method public final ab()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 308000
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->W:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308001
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->W:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->W:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 308002
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->W:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    return-object v0
.end method

.method public final ac()Lcom/facebook/graphql/model/GraphQLEventTicketProvider;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308003
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->X:Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308004
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->X:Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->X:Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    .line 308005
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->X:Lcom/facebook/graphql/model/GraphQLEventTicketProvider;

    return-object v0
.end method

.method public final ad()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308006
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308007
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Y:Ljava/lang/String;

    const/16 v1, 0x30

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Y:Ljava/lang/String;

    .line 308008
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Y:Ljava/lang/String;

    return-object v0
.end method

.method public final ae()Lcom/facebook/graphql/enums/GraphQLEventType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 308009
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Z:Lcom/facebook/graphql/enums/GraphQLEventType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308010
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Z:Lcom/facebook/graphql/enums/GraphQLEventType;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Z:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 308011
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->Z:Lcom/facebook/graphql/enums/GraphQLEventType;

    return-object v0
.end method

.method public final af()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307667
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307668
    const/4 v0, 0x6

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307669
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aa:I

    return v0
.end method

.method public final ag()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307498
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307499
    const/4 v0, 0x6

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307500
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ab:I

    return v0
.end method

.method public final ah()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307254
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ac:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307255
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ac:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ac:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    .line 307256
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ac:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    return-object v0
.end method

.method public final ai()Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 307260
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ad:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307261
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ad:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ad:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 307262
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ad:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    return-object v0
.end method

.method public final aj()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307263
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ae:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307264
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ae:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ae:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307265
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ae:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    return-object v0
.end method

.method public final ak()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307266
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->af:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307267
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->af:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307268
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->af:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final al()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307269
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ag:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307270
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ag:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    const/16 v1, 0x38

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ag:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 307271
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ag:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    return-object v0
.end method

.method public final am()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307272
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ah:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307273
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ah:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    const/16 v1, 0x39

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ah:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 307274
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ah:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    return-object v0
.end method

.method public final an()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307275
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ai:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307276
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ai:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    const/16 v1, 0x3a

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ai:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 307277
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ai:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    return-object v0
.end method

.method public final ao()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307278
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aj:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307279
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aj:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    const/16 v1, 0x3b

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aj:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 307280
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aj:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    return-object v0
.end method

.method public final ap()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307281
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ak:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307282
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ak:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ak:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 307283
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ak:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    return-object v0
.end method

.method public final aq()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307284
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->al:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307285
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->al:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->al:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307286
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->al:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    return-object v0
.end method

.method public final ar()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307257
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->am:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307258
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->am:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->am:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307259
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->am:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    return-object v0
.end method

.method public final as()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307290
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->an:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307291
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->an:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->an:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307292
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->an:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    return-object v0
.end method

.method public final at()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307293
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ao:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307294
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ao:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    const/16 v1, 0x40

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ao:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307295
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ao:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    return-object v0
.end method

.method public final au()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307296
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ap:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307297
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ap:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ap:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307298
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ap:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    return-object v0
.end method

.method public final av()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307299
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aq:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307300
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aq:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aq:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307301
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aq:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    return-object v0
.end method

.method public final aw()Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307302
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ar:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307303
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ar:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    const/16 v1, 0x43

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ar:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    .line 307304
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ar:Lcom/facebook/graphql/model/GraphQLEventInviteesConnection;

    return-object v0
.end method

.method public final ax()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307305
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->as:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307306
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->as:Ljava/lang/String;

    const/16 v1, 0x44

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->as:Ljava/lang/String;

    .line 307307
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->as:Ljava/lang/String;

    return-object v0
.end method

.method public final ay()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307308
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307309
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x45

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307310
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->at:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final az()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307311
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307312
    const/16 v0, 0x8

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307313
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->au:Z

    return v0
.end method

.method public final bA()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307287
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bv:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307288
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bv:Ljava/util/List;

    const/16 v1, 0x7d

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bv:Ljava/util/List;

    .line 307289
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bv:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bB()Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307197
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bw:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307198
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bw:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    const/16 v1, 0x7e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bw:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 307199
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bw:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    return-object v0
.end method

.method public final bC()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307203
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bx:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307204
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bx:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x7f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bx:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 307205
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bx:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final bD()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307206
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->by:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307207
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->by:Ljava/util/List;

    const/16 v1, 0x80

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->by:Ljava/util/List;

    .line 307208
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->by:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bE()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307209
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bz:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307210
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bz:Ljava/util/List;

    const/16 v1, 0x81

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bz:Ljava/util/List;

    .line 307211
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bz:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307212
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bA:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307213
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bA:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x82

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bA:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 307214
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bA:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final bG()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307215
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bB:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307216
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bB:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/16 v1, 0x83

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bB:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 307217
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bB:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method public final bH()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307218
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bC:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307219
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bC:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/16 v1, 0x84

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bC:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 307220
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bC:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method public final bI()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307221
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307222
    const/16 v0, 0x10

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307223
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bD:I

    return v0
.end method

.method public final bJ()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307224
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307225
    const/16 v0, 0x11

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307226
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bE:Z

    return v0
.end method

.method public final bK()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307227
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bF:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307228
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bF:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    const/16 v1, 0x89

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bF:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307229
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bF:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    return-object v0
.end method

.method public final bL()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307230
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bG:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307231
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bG:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    const/16 v1, 0x8a

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bG:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 307232
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bG:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    return-object v0
.end method

.method public final bM()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307233
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307234
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307235
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bN()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307236
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bI:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307237
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bI:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307238
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bI:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bO()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307239
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307240
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8d

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 307241
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final ba()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307242
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aV:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307243
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aV:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x62

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aV:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307244
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aV:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bb()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307245
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aW:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307246
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aW:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x63

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aW:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307247
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aW:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bc()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307248
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307249
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x64

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307250
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bd()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307251
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aY:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307252
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aY:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x65

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aY:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 307253
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aY:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method public final be()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307200
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307201
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x66

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307202
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bf()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307402
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307403
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x67

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307404
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bg()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307375
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307376
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x68

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307377
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bh()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307378
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307379
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x69

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307380
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bi()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307381
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307382
    const/16 v0, 0xd

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307383
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bd:Z

    return v0
.end method

.method public final bj()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307384
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->be:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307385
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->be:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    const/16 v1, 0x6b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->be:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 307386
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->be:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    return-object v0
.end method

.method public final bk()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307387
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307388
    const/16 v0, 0xd

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307389
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bf:J

    return-wide v0
.end method

.method public final bl()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307390
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307391
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x6d

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 307392
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final bm()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307393
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bh:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307394
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bh:Ljava/lang/String;

    const/16 v1, 0x6e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bh:Ljava/lang/String;

    .line 307395
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bh:Ljava/lang/String;

    return-object v0
.end method

.method public final bn()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307396
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307397
    const/16 v0, 0xd

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307398
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bi:J

    return-wide v0
.end method

.method public final bo()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307399
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bj:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307400
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bj:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x70

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bj:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 307401
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bj:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method public final bp()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307372
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307373
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x71

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 307374
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final bq()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307405
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307406
    const/16 v0, 0xe

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307407
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bl:Z

    return v0
.end method

.method public final br()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307408
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bm:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307409
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bm:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x73

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bm:Lcom/facebook/graphql/model/GraphQLImage;

    .line 307410
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bm:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bs()Lcom/facebook/graphql/model/GraphQLEventTimeRange;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307411
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bn:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307412
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bn:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    const/16 v1, 0x74

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bn:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 307413
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bn:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    return-object v0
.end method

.method public final bt()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307414
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bo:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307415
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bo:Ljava/lang/String;

    const/16 v1, 0x75

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bo:Ljava/lang/String;

    .line 307416
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bo:Ljava/lang/String;

    return-object v0
.end method

.method public final bu()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307417
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bp:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307418
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bp:Ljava/lang/String;

    const/16 v1, 0x76

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bp:Ljava/lang/String;

    .line 307419
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bp:Ljava/lang/String;

    return-object v0
.end method

.method public final bv()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307420
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307421
    const/16 v0, 0xe

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307422
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bq:I

    return v0
.end method

.method public final bw()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307423
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->br:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307424
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->br:Ljava/lang/String;

    const/16 v1, 0x79

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->br:Ljava/lang/String;

    .line 307425
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->br:Ljava/lang/String;

    return-object v0
.end method

.method public final bx()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307426
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bs:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307427
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bs:Ljava/lang/String;

    const/16 v1, 0x7a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bs:Ljava/lang/String;

    .line 307428
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bs:Ljava/lang/String;

    return-object v0
.end method

.method public final by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307342
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bt:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307343
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bt:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0x7b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bt:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 307344
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bt:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final bz()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307317
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307318
    const/16 v0, 0xf

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307319
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->bu:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 307320
    const v0, 0x403827a

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307321
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307322
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 307323
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307324
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307325
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 307326
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307327
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->g:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307328
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->g:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->g:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 307329
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->g:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307330
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->h:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307331
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->h:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->h:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 307332
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->h:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307333
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->i:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307334
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->i:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->i:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 307335
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->i:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307336
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307337
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->j:Ljava/util/List;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->j:Ljava/util/List;

    .line 307338
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLInlineActivity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307339
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->k:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307340
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->k:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->k:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 307341
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->k:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307314
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->l:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307315
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->l:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->l:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 307316
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->l:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307345
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->m:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307346
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->m:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->m:Lcom/facebook/graphql/model/GraphQLStory;

    .line 307347
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->m:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307348
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307349
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307350
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->n:Z

    return v0
.end method

.method public final t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307351
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307352
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307353
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->o:Z

    return v0
.end method

.method public final u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307354
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307355
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307356
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->p:Z

    return v0
.end method

.method public final v()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307357
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307358
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307359
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->q:Z

    return v0
.end method

.method public final w()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307360
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307361
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307362
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->r:Z

    return v0
.end method

.method public final x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307363
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307364
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307365
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->s:Z

    return v0
.end method

.method public final y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307366
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307367
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307368
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->t:Z

    return v0
.end method

.method public final z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 307369
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307370
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307371
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEvent;->u:Z

    return v0
.end method
