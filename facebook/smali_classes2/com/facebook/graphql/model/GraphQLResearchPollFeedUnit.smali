.class public final Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements LX/16e;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements Lcom/facebook/graphql/model/Sponsorable;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit$Serializer;
.end annotation


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Z

.field private G:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I

.field public r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Z

.field public y:Z

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 201446
    const-class v0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 201386
    const-class v0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 201387
    const/16 v0, 0x1d

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 201388
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x44774584

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 201389
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->G:LX/0x2;

    .line 201390
    return-void
.end method

.method private M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201391
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201392
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->o:Ljava/lang/String;

    .line 201393
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method private N()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201394
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201395
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->p:Ljava/lang/String;

    .line 201396
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method

.method private O()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 201397
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201398
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201399
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->q:I

    return v0
.end method

.method private P()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201400
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201401
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->v:Ljava/lang/String;

    .line 201402
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->v:Ljava/lang/String;

    return-object v0
.end method

.method private Q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201403
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201404
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w:Ljava/lang/String;

    .line 201405
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w:Ljava/lang/String;

    return-object v0
.end method

.method private R()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201406
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201407
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201408
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private S()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201409
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201410
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E:Ljava/lang/String;

    .line 201411
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 201412
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->q:I

    .line 201413
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 201414
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 201415
    if-eqz v0, :cond_0

    .line 201416
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 201417
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 201418
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->o:Ljava/lang/String;

    .line 201419
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 201420
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 201421
    if-eqz v0, :cond_0

    .line 201422
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 201423
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 201424
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->F:Z

    .line 201425
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 201426
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 201427
    if-eqz v0, :cond_0

    .line 201428
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1b

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 201429
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 201430
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->p:Ljava/lang/String;

    .line 201431
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 201432
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 201433
    if-eqz v0, :cond_0

    .line 201434
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 201435
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201436
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201437
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->m:Ljava/lang/String;

    .line 201438
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201469
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201470
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->n:Ljava/lang/String;

    .line 201471
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201439
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201440
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->s:Ljava/lang/String;

    .line 201441
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201466
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201467
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->l:Ljava/lang/String;

    .line 201468
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201463
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201464
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->t:Ljava/lang/String;

    .line 201465
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 201462
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201459
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->u:Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201460
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->u:Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->u:Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    .line 201461
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->u:Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201456
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201457
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->j:Ljava/lang/String;

    .line 201458
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final F()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 201472
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201473
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201474
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->x:Z

    return v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 201453
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201454
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201455
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->k:J

    return-wide v0
.end method

.method public final G()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 201450
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201451
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201452
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->y:Z

    return v0
.end method

.method public final H()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201447
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201448
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->z:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->z:Ljava/lang/String;

    .line 201449
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 201379
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201443
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201444
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->A:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->A:Ljava/lang/String;

    .line 201445
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 201442
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201380
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201381
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201382
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final K()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 201383
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201384
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201385
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->F:Z

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 201349
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->G:LX/0x2;

    if-nez v0, :cond_0

    .line 201350
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->G:LX/0x2;

    .line 201351
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->G:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 201348
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 201346
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 201347
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 25

    .prologue
    .line 201293
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 201294
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 201295
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 201296
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->y()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 201297
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->z()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 201298
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E_()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 201299
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->C_()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 201300
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->A()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 201301
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->B()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 201302
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->M()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 201303
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->N()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 201304
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 201305
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->C()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 201306
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->D()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 201307
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 201308
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->P()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 201309
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->Q()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 201310
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->H()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 201311
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->I()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 201312
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 201313
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->R()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 201314
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->c()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 201315
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->S()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 201316
    const/16 v7, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 201317
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v2}, LX/186;->b(II)V

    .line 201318
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 201319
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 201320
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 201321
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 201322
    const/4 v3, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 201323
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 201324
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 201325
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 201326
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 201327
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 201328
    const/16 v2, 0xc

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->O()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 201329
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 201330
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 201331
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 201332
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201333
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201334
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201335
    const/16 v2, 0x13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->F()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 201336
    const/16 v2, 0x14

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->G()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 201337
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201338
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201339
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201340
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201341
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201342
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201343
    const/16 v2, 0x1b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->K()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 201344
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 201345
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 201265
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 201266
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201267
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 201268
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 201269
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 201270
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 201271
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 201272
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 201273
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 201274
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 201275
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 201276
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 201277
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    .line 201278
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 201279
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 201280
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->u:Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    .line 201281
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 201282
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201283
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 201284
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 201285
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201286
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->R()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 201287
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->R()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201288
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->R()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 201289
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 201290
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201291
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 201292
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 201264
    new-instance v0, LX/4YZ;

    invoke-direct {v0, p1}, LX/4YZ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201263
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->A()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 201261
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->k:J

    .line 201262
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 201254
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 201255
    const/4 v0, 0x6

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->k:J

    .line 201256
    const/16 v0, 0xc

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->q:I

    .line 201257
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->x:Z

    .line 201258
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->y:Z

    .line 201259
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->F:Z

    .line 201260
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 201236
    const-string v0, "local_is_completed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201237
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->K()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 201238
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 201239
    const/16 v0, 0x1b

    iput v0, p2, LX/18L;->c:I

    .line 201240
    :goto_0
    return-void

    .line 201241
    :cond_0
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201242
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->M()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 201243
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 201244
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 201245
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201246
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->N()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 201247
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 201248
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 201249
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 201250
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->O()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 201251
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 201252
    const/16 v0, 0xc

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 201253
    :cond_3
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 201227
    const-string v0, "local_is_completed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201228
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->a(Z)V

    .line 201229
    :cond_0
    :goto_0
    return-void

    .line 201230
    :cond_1
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201231
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 201232
    :cond_2
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 201233
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 201234
    :cond_3
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201235
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201226
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201223
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201224
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->D:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->D:Ljava/lang/String;

    .line 201225
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201218
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201219
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 201220
    :goto_0
    return-object v0

    .line 201221
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 201222
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 201217
    const v0, -0x44774584

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201352
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201353
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g:Ljava/lang/String;

    .line 201354
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201355
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 201356
    invoke-static {p0}, LX/2dv;->a(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v0

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201357
    const/4 v0, 0x0

    move-object v0, v0

    .line 201358
    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 201359
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q()Ljava/util/List;
    .locals 1

    .prologue
    .line 201360
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->u()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 1

    .prologue
    .line 201361
    invoke-static {p0}, LX/1w8;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 1

    .prologue
    .line 201362
    invoke-static {p0}, LX/1w8;->a(LX/16e;)Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201363
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/model/SponsoredImpression;->a(Lcom/facebook/graphql/model/GraphQLSponsoredData;)Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    move-object v0, v0

    .line 201364
    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 1

    .prologue
    .line 201365
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 201366
    invoke-static {p0}, LX/18M;->a(Lcom/facebook/graphql/model/Sponsorable;)Z

    move-result v0

    return v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201367
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201368
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 201369
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201370
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201371
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 201372
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201373
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201374
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->h:Ljava/lang/String;

    .line 201375
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201376
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201377
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->i:Ljava/lang/String;

    .line 201378
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method
