.class public final Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 308890
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 308889
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 308887
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 308888
    return-void
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308884
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308885
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->f:Ljava/util/List;

    .line 308886
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 308835
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 308836
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 308837
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 308881
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 308882
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 308883
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 308871
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 308872
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->k()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 308873
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 308874
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 308875
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->a()I

    move-result v2

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 308876
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 308877
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 308878
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->j()I

    move-result v1

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 308879
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 308880
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 308858
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 308859
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 308860
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 308861
    if-eqz v1, :cond_2

    .line 308862
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    .line 308863
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->f:Ljava/util/List;

    move-object v1, v0

    .line 308864
    :goto_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 308865
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 308866
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 308867
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    .line 308868
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 308869
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 308870
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 308852
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->e:I

    .line 308853
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 308854
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 308855
    if-eqz v0, :cond_0

    .line 308856
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 308857
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 308848
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 308849
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->e:I

    .line 308850
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->h:I

    .line 308851
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 308842
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->h:I

    .line 308843
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 308844
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 308845
    if-eqz v0, :cond_0

    .line 308846
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 308847
    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 308841
    const v0, -0x7eb557ed

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 308838
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 308839
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 308840
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->h:I

    return v0
.end method
