.class public final Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

.field public B:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private K:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:J

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:J

.field public q:I

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:I

.field public w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 253173
    const-class v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 253070
    const-class v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 253071
    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 253072
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x681541e0

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 253073
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K:LX/0x2;

    .line 253074
    return-void
.end method

.method public constructor <init>(LX/4Xu;)V
    .locals 2

    .prologue
    .line 253075
    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 253076
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x681541e0

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 253077
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K:LX/0x2;

    .line 253078
    iget-object v0, p1, LX/4Xu;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->f:Ljava/util/List;

    .line 253079
    iget-object v0, p1, LX/4Xu;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g:Ljava/util/List;

    .line 253080
    iget-object v0, p1, LX/4Xu;->d:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 253081
    iget-object v0, p1, LX/4Xu;->e:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 253082
    iget-object v0, p1, LX/4Xu;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->j:Ljava/util/List;

    .line 253083
    iget-object v0, p1, LX/4Xu;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->k:Ljava/lang/String;

    .line 253084
    iget-wide v0, p1, LX/4Xu;->h:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->l:J

    .line 253085
    iget-object v0, p1, LX/4Xu;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->m:Ljava/lang/String;

    .line 253086
    iget-object v0, p1, LX/4Xu;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 253087
    iget-object v0, p1, LX/4Xu;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 253088
    iget-wide v0, p1, LX/4Xu;->l:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->p:J

    .line 253089
    iget v0, p1, LX/4Xu;->m:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->q:I

    .line 253090
    iget-object v0, p1, LX/4Xu;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->r:Ljava/lang/String;

    .line 253091
    iget-object v0, p1, LX/4Xu;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->s:Ljava/lang/String;

    .line 253092
    iget-object v0, p1, LX/4Xu;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->t:Ljava/lang/String;

    .line 253093
    iget-object v0, p1, LX/4Xu;->q:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->J:Ljava/util/List;

    .line 253094
    iget-object v0, p1, LX/4Xu;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u:Ljava/lang/String;

    .line 253095
    iget v0, p1, LX/4Xu;->s:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->v:I

    .line 253096
    iget-object v0, p1, LX/4Xu;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 253097
    iget-object v0, p1, LX/4Xu;->u:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 253098
    iget-object v0, p1, LX/4Xu;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 253099
    iget-object v0, p1, LX/4Xu;->w:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 253100
    iget-object v0, p1, LX/4Xu;->x:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->A:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 253101
    iget-object v0, p1, LX/4Xu;->y:Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 253102
    iget-object v0, p1, LX/4Xu;->z:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->C:Ljava/lang/String;

    .line 253103
    iget-object v0, p1, LX/4Xu;->A:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 253104
    iget-object v0, p1, LX/4Xu;->B:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->E:Ljava/util/List;

    .line 253105
    iget-object v0, p1, LX/4Xu;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 253106
    iget-object v0, p1, LX/4Xu;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 253107
    iget-object v0, p1, LX/4Xu;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->H:Ljava/lang/String;

    .line 253108
    iget-object v0, p1, LX/4Xu;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I:Ljava/lang/String;

    .line 253109
    iget-object v0, p1, LX/4Xu;->G:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K:LX/0x2;

    .line 253110
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 253111
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->v:I

    .line 253112
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 253113
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 253114
    if-eqz v0, :cond_0

    .line 253115
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 253116
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 253117
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->t:Ljava/lang/String;

    .line 253118
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 253119
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 253120
    if-eqz v0, :cond_0

    .line 253121
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 253122
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 253123
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u:Ljava/lang/String;

    .line 253124
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 253125
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 253126
    if-eqz v0, :cond_0

    .line 253127
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 253128
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 253129
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 253130
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 253131
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->q:I

    return v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253132
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253133
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->s:Ljava/lang/String;

    .line 253134
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253135
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253136
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->t:Ljava/lang/String;

    .line 253137
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253138
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253139
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->r:Ljava/lang/String;

    .line 253140
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253141
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253142
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u:Ljava/lang/String;

    .line 253143
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 253144
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 253145
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 253146
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 253147
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->v:I

    return v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253148
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253149
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->m:Ljava/lang/String;

    .line 253150
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253184
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253185
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 253186
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 253151
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 253152
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 253153
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->p:J

    return-wide v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253181
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253182
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 253183
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253178
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253179
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 253180
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 253177
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253174
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253175
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 253176
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 253172
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final J()Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 253169
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->A:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253170
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->A:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->A:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 253171
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->A:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253166
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253167
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLEntity;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 253168
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLEntity;

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253163
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->C:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253164
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->C:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->C:Ljava/lang/String;

    .line 253165
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 253160
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K:LX/0x2;

    if-nez v0, :cond_0

    .line 253161
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K:LX/0x2;

    .line 253162
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K:LX/0x2;

    return-object v0
.end method

.method public final M()Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253157
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253158
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 253159
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    return-object v0
.end method

.method public final N()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253154
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->E:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253155
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->E:Ljava/util/List;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->E:Ljava/util/List;

    .line 253156
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->E:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253064
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253065
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 253066
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253067
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253068
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 253069
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 253025
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253022
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253023
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I:Ljava/lang/String;

    .line 253024
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 253020
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 253021
    return-object v0
.end method

.method public final R()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253017
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->J:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253018
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->J:Ljava/util/List;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->J:Ljava/util/List;

    .line 253019
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->J:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 29

    .prologue
    .line 252955
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 252956
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->s()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 252957
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->t()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 252958
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 252959
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 252960
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->w()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 252961
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 252962
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->E_()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 252963
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 252964
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 252965
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->C_()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 252966
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->B()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 252967
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->C()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 252968
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->D()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 252969
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 252970
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 252971
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 252972
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 252973
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 252974
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->L()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 252975
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->M()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 252976
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->N()LX/0Px;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->d(Ljava/util/List;)I

    move-result v22

    .line 252977
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 252978
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 252979
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->c()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 252980
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->Q()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 252981
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->R()LX/0Px;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v27

    .line 252982
    const/16 v28, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 252983
    const/16 v28, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 252984
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 252985
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 252986
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 252987
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 252988
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 252989
    const/4 v3, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->x()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 252990
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 252991
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 252992
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 252993
    const/16 v3, 0xb

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 252994
    const/16 v2, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->A()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 252995
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 252996
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 252997
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 252998
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 252999
    const/16 v2, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->E()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 253000
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 253001
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253002
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253003
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253004
    const/16 v3, 0x16

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->J()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 253005
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253006
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253007
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253008
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253009
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253010
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253011
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253012
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253013
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 253014
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 253015
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 253016
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->J()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 252877
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 252878
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->s()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 252879
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->s()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 252880
    if-eqz v1, :cond_0

    .line 252881
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252882
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->f:Ljava/util/List;

    .line 252883
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->t()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 252884
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->t()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 252885
    if-eqz v1, :cond_1

    .line 252886
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252887
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 252888
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 252889
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 252890
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 252891
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252892
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 252893
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 252894
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 252895
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 252896
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252897
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 252898
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->w()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 252899
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->w()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 252900
    if-eqz v2, :cond_4

    .line 252901
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252902
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->j:Ljava/util/List;

    move-object v1, v0

    .line 252903
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 252904
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 252905
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 252906
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252907
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 252908
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 252909
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 252910
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 252911
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252912
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 252913
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 252914
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252915
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 252916
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252917
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252918
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 252919
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 252920
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 252921
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252922
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->x:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 252923
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 252924
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252925
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 252926
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252927
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252928
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 252929
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 252930
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 252931
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252932
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 252933
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 252934
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 252935
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 252936
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252937
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->B:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 252938
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->M()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 252939
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->M()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 252940
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->M()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 252941
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252942
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->D:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 252943
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 252944
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252945
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 252946
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252947
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252948
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 252949
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252950
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 252951
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 252952
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252953
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 252954
    if-nez v1, :cond_f

    :goto_0
    return-object p0

    :cond_f
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 252876
    new-instance v0, LX/4Xv;

    invoke-direct {v0, p1}, LX/4Xv;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252875
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 252873
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->p:J

    .line 252874
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 253058
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 253059
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->l:J

    .line 253060
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->p:J

    .line 253061
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->q:I

    .line 253062
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->v:I

    .line 253063
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 252859
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252860
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252861
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252862
    const/16 v0, 0xf

    iput v0, p2, LX/18L;->c:I

    .line 252863
    :goto_0
    return-void

    .line 252864
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252865
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->D()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252866
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252867
    const/16 v0, 0x10

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 252868
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252869
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->E()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252870
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252871
    const/16 v0, 0x11

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 252872
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 252852
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252853
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->a(Ljava/lang/String;)V

    .line 252854
    :cond_0
    :goto_0
    return-void

    .line 252855
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252856
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 252857
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252858
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252851
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252848
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252849
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->H:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->H:Ljava/lang/String;

    .line 252850
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252843
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252844
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 252845
    :goto_0
    return-object v0

    .line 252846
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 252847
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 252842
    const v0, -0x681541e0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253026
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253027
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->k:Ljava/lang/String;

    .line 253028
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253029
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 1

    .prologue
    .line 253030
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic p()Ljava/util/List;
    .locals 1

    .prologue
    .line 253031
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->r()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q()Ljava/util/List;
    .locals 1

    .prologue
    .line 253032
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->o()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 1

    .prologue
    .line 253033
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final s()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253034
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253035
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->f:Ljava/util/List;

    .line 253036
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final t()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253037
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253038
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g:Ljava/util/List;

    .line 253039
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253040
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253041
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 253042
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253043
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253044
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 253045
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final w()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253046
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253047
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->j:Ljava/util/List;

    .line 253048
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final x()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 253049
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 253050
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 253051
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->l:J

    return-wide v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253052
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253053
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 253054
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253055
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253056
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 253057
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    return-object v0
.end method
