.class public final Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323420
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323419
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 323417
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 323418
    return-void
.end method

.method private a()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323414
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;->e:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323415
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;->e:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;->e:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323416
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;->e:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 323408
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323409
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 323410
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 323411
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 323412
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323413
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 323399
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323400
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 323401
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323402
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 323403
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    .line 323404
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;->e:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323405
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323406
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 323407
    const v0, -0x75f09e38

    return v0
.end method
