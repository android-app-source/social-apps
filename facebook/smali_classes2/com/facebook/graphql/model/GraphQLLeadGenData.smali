.class public final Lcom/facebook/graphql/model/GraphQLLeadGenData;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenData$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenPage;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z

.field public n:Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320333
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320334
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenData$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320335
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 320336
    return-void
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320337
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320338
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->f:Ljava/lang/String;

    .line 320339
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 320340
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320341
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 320342
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 320343
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->j()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 320344
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->k()Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 320345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->m()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 320346
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 320347
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 320348
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->q()Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 320349
    const/16 v8, 0xa

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 320350
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 320351
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 320352
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 320353
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 320354
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->l()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 320355
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 320356
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 320357
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 320358
    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->p()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 320359
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 320360
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320361
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 320362
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320363
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320364
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    .line 320365
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 320366
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 320367
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenData;->e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    .line 320368
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 320369
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 320370
    if-eqz v2, :cond_1

    .line 320371
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 320372
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->g:Ljava/util/List;

    move-object v1, v0

    .line 320373
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->k()Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 320374
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->k()Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    .line 320375
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->k()Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 320376
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 320377
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenData;->h:Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    .line 320378
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->m()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 320379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->m()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 320380
    if-eqz v2, :cond_3

    .line 320381
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 320382
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->j:Ljava/util/List;

    move-object v1, v0

    .line 320383
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->q()Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 320384
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->q()Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    .line 320385
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->q()Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 320386
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 320387
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLLeadGenData;->n:Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    .line 320388
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320389
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320390
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320391
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    .line 320392
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 320328
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 320329
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->i:Z

    .line 320330
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->m:Z

    .line 320331
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 320332
    const v0, -0x4adb5402

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320325
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320326
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->g:Ljava/util/List;

    .line 320327
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320322
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->h:Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320323
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->h:Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->h:Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    .line 320324
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->h:Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320319
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320320
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320321
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->i:Z

    return v0
.end method

.method public final m()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320316
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320317
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->j:Ljava/util/List;

    .line 320318
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320313
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320314
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->k:Ljava/lang/String;

    .line 320315
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320310
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320311
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->l:Ljava/lang/String;

    .line 320312
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320307
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320308
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320309
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->m:Z

    return v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320304
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->n:Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320305
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->n:Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->n:Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    .line 320306
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenData;->n:Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    return-object v0
.end method
