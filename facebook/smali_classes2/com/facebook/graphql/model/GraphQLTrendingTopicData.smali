.class public final Lcom/facebook/graphql/model/GraphQLTrendingTopicData;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTrendingTopicData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTrendingTopicData$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327219
    const-class v0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327218
    const-class v0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 327216
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 327217
    return-void
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327213
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327214
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->k:Ljava/lang/String;

    .line 327215
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->k:Ljava/lang/String;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327210
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327211
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->m:Ljava/lang/String;

    .line 327212
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 327158
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 327159
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 327160
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 327161
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 327162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 327163
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 327164
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 327165
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->r()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 327166
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 327167
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->s()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 327168
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->q()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 327169
    const/16 v10, 0xb

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 327170
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 327171
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 327172
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 327173
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 327174
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 327175
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 327176
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 327177
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 327178
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 327179
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 327180
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327181
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 327192
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 327193
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 327194
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 327195
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 327196
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 327197
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 327198
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 327199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 327200
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 327201
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 327202
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 327203
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 327204
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 327205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 327206
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 327207
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 327208
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327209
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327191
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 327220
    const v0, -0x6f02fa4c

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327188
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327189
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->e:Ljava/lang/String;

    .line 327190
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327185
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327186
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->f:Ljava/lang/String;

    .line 327187
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327182
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327183
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 327184
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327155
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327156
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 327157
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327152
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327153
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 327154
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327149
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327150
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->j:Ljava/lang/String;

    .line 327151
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327146
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327147
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->l:Ljava/lang/String;

    .line 327148
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327143
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327144
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->n:Ljava/lang/String;

    .line 327145
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->n:Ljava/lang/String;

    return-object v0
.end method
